<?php 

	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
	
	$medication_id=$_GET['id'];
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<?php
include 'header.php'; ?>
<!-- header.......-->
<?php
include("admin/includes/host_conf.php");
include("admin/includes/mysql.lib.php");
include('admin/includes/ps_pagination.php');
$obj=new connect;
?>

<script type="text/javascript">
function chage_period_usage()
{
	var period_usage=document.getElementById('period_usage').value;
	if (period_usage=='Other than These')
	{
		document.getElementById("other_than_these").style.display="block";
	}
	 
	 if (period_usage!='Other than These')
	{
		document.getElementById("other_than_these").style.display="none";
	}
}

</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="220" valign="top">
<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<table width="220" border="0" cellspacing="0" cellpadding="0" class="s90phrmenus">
<tr><td><img src="images/phr_menustop.jpg" width="220" height="6" /></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="my-active-consultation.php">My Active Consultations</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="user_details.php">My Personal Information</a></td></tr>
<tr><th><div id="s90phrsubmenus2"><img src="images/dots2.gif" class="s90phrdotsarrow" /><a href="phr_healthinformation.php"><font color="color='EA0977'"><b>MY Health Information</b></font></a></div>
<div id="s90phrsubmenus">. <a href="phr_healthinformation.php">Health Profile (family and i)</a><br />
. <a href="phr_hi_mymedications.php"><font color="#ea0977"><strong>My Medications</strong></font></a><br />
. <a href="phr_hi_as.php">My Allergies & Surgeries</a></div>
</th></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="phr_reports.php">My Reports</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="phr_achistory.php">My Account History</a></td></tr>
<tr><td><img src="images/phr_menusbot.jpg" width="220" height="6" /></td></tr>
</table>
</td>

<td width="748" valign="top" class="s90phrcontent">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr><td width="484"><h1>My Medications</h1></td>
	<td width="264" bgcolor="#f1f1f1"><div style="color:#906; font-family:Arial; font-size:16px; font-weight:bold;">
    	Pinkwhale Card No: <?php echo $user_id ; ?></div></td>
</tr>
</table>

<table height="20"><tr><td></td></tr></table>



       <?php
$qry= "SELECT * FROM `my_medication` where `medication_id`='$medication_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
	
		$date1=$result['entered_date'];
?>	
    
<form name="medication_form" id="medication_form" action="actions/edit_medicine.php" method="post">
    <table width="700" border="0" cellspacing="10" cellpadding="2" align="center" style="border:1px solid #CCC;" >
        <tr>
        <input type="hidden" name="hidden_value" id="hidden_value" value="<?php echo $result['medication_id'] ?>"  />
         <td colspan="2" bgcolor="#DBEEF9"><div class="phr_usr_dtils">EDIT MEDICATIONS</div></td>
        </tr>
        <tr>
            <td><div class="lables">Name of Medicine </div></td>
            <td><input type="text" name="name_of_medicine" id="name_of_medicine" value="<?php echo $result['name_of_medicine'] ?>"/></td>
        </tr>
        <tr>
            <td><div class="lables">Complaint</div></td>
            <td><input type="text" name="complaint" id="complaint" value="<?php echo $result['complaint'] ?>"/></td>
        </tr>
        <tr>
            <td><div class="lables">Dosage</div></td>
            <td><input type="text" name="dosage" id="dosage" value="<?php echo $result['dosage'] ?>"/></td>
        </tr>
        <tr>
            <td><div class="lables">Medication Schedule</div></td>
            <td>
             <select  name="period_usage" id="period_usage" class="registetextbox"  onchange="chage_period_usage()"> 
			   <option value="" >-Select Period-</option>
               <option value="Morning-Afternoon-night">Morning-Afternoon-night</option>
               <option value="Mornig Only">Mornig Only</option>
               <option value="Night Only">Night Only</option>
               <option value="Every 4 Hours">Every 4 Hours</option>
                <option value="Other than These">Other than These</option>
  		</select>
        <script type="text/javascript">
		type='<?php echo $result['period_of_usage']; ?>';
		document.getElementById('period_usage').value=type;
		document.getElementById('period_usage').selected=true;
	</script>
            </td>
        </tr>
        
              <tr><td colspan="3">
         

<!--        Other than these div            -->
        <div id="other_than_these" style="display:none;">
       
        <table>
        <tr><td width="268">&nbsp;</td>
        <td width="354"><input type="text" name="other_than" id="other_than" /></td>
        </tr>
        </table>
         </div>
<!--         End Div-->
 </td></tr>
 
 
 
  <tr>
            <td colspan="1"><div class="lables">Period Of Medication</div></td>
            <td colspan="1"><input type="text" name="period_of_medication" id="period_of_medication" value="<?php echo $result['period_of_medication'] ?>"/></td>
        </tr>
 
  <tr>
            <td colspan="1"><div class="lables">Date :</div></td>
            <td colspan="1">
   <?php
	  $myCalendar = new tc_calendar("last_occured", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?> 
        </td>
        </tr>
 
 
 
    	<tr>
            <td colspan="1"><div class="lables">Remarks</div></td>
            <td colspan="1"><textarea name="medication_remarks" id="medication_remarks"><?php echo $result['medication_remarks'] ?> </textarea></td>
        </tr>
        
 
 
        <tr>
      
        	<td>&nbsp;</td>
            <td><input type="submit" name="medication_save" id="medication_save" value="Save"  /></td>
        </tr>
    </table>
</form>
  <?php
		}
		?>
</td></tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>