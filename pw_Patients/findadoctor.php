<?php include "../includes/start.php"; 

?>
	<?php include "../includes/header.php" ?>
	<link href="../css/findadoctor.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-followup"></div>
	<?php include "menu-patients.php" ?>
	
	
	<div class="banner-bg banner-bg-find-a-doctor">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Know just who to consult without <br>even reaching the hospital.</i></div>
		</div>
	</div>

	<div class="find-a-doctor-row">		
		<div class="content-area find-a-doctor-form-container">
			<div class="header">
				<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_FindDoctor.png">
				Find a Doctor
			</div>			
			<form class="form-inline" role="form">				
				<div class="form-group">
					<div class="input-group">
						<select class="form-control">
							<option>Pediatrician</option>
							<option>Pediatrician</option>					
						</select>										
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control">
							<option>Bangalore</option>
							<option>Mysore</option>
							<option>Ahmedabad</option>
							<option>Delhi</option>
							<option>Jammu</option>
						</select>										
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn pw-btn">Find Doctor</button>
				</div>										
			</form>
		</div>
	</div>

	
	<div class="page-row gradient-background find-a-doctor first-row">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-3 col-xs-12">
				<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Doctor_1.png">
				<div class="details visible-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>
					<div class="red-text idno visible-xs">ID No: 67382</div>
				</div>				
				<a href="doctorsdomain.php" class="doctor-button blue">View Profile</a><span class="hidden-sm hidden-xs"><br></span>
				<a href="book-online.php" class="doctor-button red">Book Online</a>
				<div class="clearfix"></div>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-9 col-xs-12">
				<div class="details hidden-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>					
					<div>
						<div class="message">
							Lorem Ipsum is simply dummy text of the printing and <br>
							typesetting industry. Lorem Ipsum has been the industry's <br>
							standard dummy text ever since the 1500s.
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">CHILDCARE CLINIC</span><br>
							MON - Thu <br>
							10am - 2pm<br>
							+91 80 43760903
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">VIKRAM HOSPITAL</span><br>
							MON - Thu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3pm - 5pm<br>
							Fri - Sat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10am - 2pm<br>
							+91 80 43768903
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar hidden-xs">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
					<span class="hidden-sm"><br></span>
					<div class="red-text idno">ID No: 67382</div>
					<div class="chart-container">
						<span class="arrow arrow-left" id="doctable1-arrow-left"></span>
						<span class="arrow arrow-right" id="doctable1-arrow-right"></span>
						<div class="chart">
							
							<table class="doctor-table table1">
								<tr>
									<!-- <th>MON</th><th>TUE</th><th>WED</th><th>THUR</th><th>FRI</th> <th>SAT</th> -->
									<?php for($i=0;$i<=29;$i++){?>
									<th data-ts="<?php echo $ts=time()+3600*24*$i;?>"><?php echo date('M j',$ts);?><br><?php echo date('D',$ts);?></th>
									<?php } ?>
								</tr>

								
								<?php for($x=0;$x<=5;$x++) {?>
									<tr>
										<?php for($i=0;$i<=29;$i++){ ?>
										<td>9:15am</td>
										<?php } ?>  															
									</tr>
								<?php } ?>

								<!--<tr>
									<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td></td>  													
								</tr>

								<tr>
									<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td></td>  														
								</tr>

								<tr>
									<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td></td> <td></td>  						
								</tr>

								<tr>
									<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td></td> <td></td>  								
								</tr>

								<tr>
									<td></td> <td>More..</td> <td>More..</td> <td></td> <td></td> <td></td>  								
								</tr> -->

							</table>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="page-row find-a-doctor">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-3 col-xs-12">
				<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Doctor_1.png">
				<div class="details visible-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>
					<div class="red-text idno visible-xs">ID No: 67382</div>
				</div>				
				<a href="doctorsdomain.php" class="doctor-button blue">View Profile</a><span class="hidden-sm hidden-xs"><br></span>
				<a href="book-online.php" class="doctor-button red">Book Online</a>
				<div class="clearfix"></div>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-9 col-xs-12">
				<div class="details hidden-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>					
					<div>
						<div class="message">
							Lorem Ipsum is simply dummy text of the printing and <br>
							typesetting industry. Lorem Ipsum has been the industry's <br>
							standard dummy text ever since the 1500s.
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">CHILDCARE CLINIC</span><br>
							MON - Thu <br>
							10am - 2pm<br>
							+91 80 43760903
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">VIKRAM HOSPITAL</span><br>
							MON - Thu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3pm - 5pm<br>
							Fri - Sat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10am - 2pm<br>
							+91 80 43768903
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar hidden-xs">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
					<span class="hidden-sm"><br></span>
					<div class="red-text idno">ID No: 67382</div>
					<div class="chart-container">
						<span class="arrow arrow-left" id="doctable2-arrow-left"></span>
						<span class="arrow arrow-right" id="doctable2-arrow-right"></span>
						<div class="chart">
							
							<table class="doctor-table table2">
								<tr>
									<!-- <th>MON</th><th>TUE</th><th>WED</th><th>THUR</th><th>FRI</th> <th>SAT</th> -->
									<?php for($i=0;$i<=29;$i++){?>
									<th data-ts="<?php echo $ts=time()+3600*24*$i;?>"><?php echo date('M j',$ts);?><br><?php echo date('D',$ts);?></th>
									<?php } ?>
								</tr>

								<?php for($x=0;$x<=5;$x++) {?>
									<tr>
										<?php for($i=0;$i<=29;$i++){ ?>
										<td>9:15am</td>
										<?php } ?>  															
									</tr>
								<?php } ?>

								<!-- <tr>
									<td></td> <td></td> <td>9:15am</td> <td></td> <td></td> <td></td>  															
								</tr>

								<tr>
									<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td></td>  													
								</tr>

								<tr>
									<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td></td>  														
								</tr>

								<tr>
									<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td></td> <td></td>  						
								</tr>

								<tr>
									<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td></td> <td></td>  								
								</tr>

								<tr>
									<td></td> <td>More..</td> <td>More..</td> <td></td> <td></td> <td></td>  								
								</tr>
 -->
							</table>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="page-row gradient-background find-a-doctor">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-3 col-xs-12">
				<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Doctor_1.png">
				<div class="details visible-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>
					<div class="red-text idno visible-xs">ID No: 67382</div>
				</div>				
				<a href="doctorsdomain.php" class="doctor-button blue">View Profile</a><span class="hidden-sm hidden-xs"><br></span>
				<a href="book-online.php" class="doctor-button red">Book Online</a>
				<div class="clearfix"></div>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-9 col-xs-12">
				<div class="details hidden-xs">
					<div class="name">Dr. Vanitha Rao Pangal</div>
					<div class="designation red-text">Paediatrician</div>					
					<div>
						<div class="message">
							Lorem Ipsum is simply dummy text of the printing and <br>
							typesetting industry. Lorem Ipsum has been the industry's <br>
							standard dummy text ever since the 1500s.
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">CHILDCARE CLINIC</span><br>
							MON - Thu <br>
							10am - 2pm<br>
							+91 80 43760903
						</div>
						<div class="timings col-xl-6 col-md-6 col-sm-6 col-xs-12">
							<span class="red-text">VIKRAM HOSPITAL</span><br>
							MON - Thu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3pm - 5pm<br>
							Fri - Sat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10am - 2pm<br>
							+91 80 43768903
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar hidden-xs">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="../img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
					<span class="hidden-sm"><br></span>
					<div class="red-text idno">ID No: 67382</div>
					<div class="chart-container">
						<span class="arrow arrow-left" id="doctable3-arrow-left"></span>
						<span class="arrow arrow-right" id="doctable3-arrow-right"></span>
						<div class="chart">
							
							<table class="doctor-table table3">
								<tr>
									<!-- <th>MON</th><th>TUE</th><th>WED</th><th>THUR</th><th>FRI</th> <th>SAT</th> -->
									<?php for($i=0;$i<=29;$i++){?>
									<th data-ts="<?php echo $ts=time()+3600*24*$i;?>"><?php echo date('M j',$ts);?><br><?php echo date('D',$ts);?></th>
									<?php } ?>
								</tr>

								<?php for($x=0;$x<=5;$x++) {?>
									<tr>
										<?php for($i=0;$i<=29;$i++){ ?>
										<td>9:15am</td>
										<?php } ?>  															
									</tr>
								<?php } ?>

								<!-- <tr>
									<td></td> <td></td> <td>9:15am</td> <td></td> <td></td> <td></td>  															
								</tr>

								<tr>
									<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td></td>  													
								</tr>

								<tr>
									<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td></td>  														
								</tr>

								<tr>
									<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>  							
								</tr>

								<tr>
									<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td></td> <td></td>  						
								</tr>

								<tr>
									<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td></td> <td></td>  								
								</tr>

								<tr>
									<td></td> <td>More..</td> <td>More..</td> <td></td> <td></td> <td></td>  								
								</tr> -->

							</table>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<style type="text/css">
	/*.profile-container .img-container img {max-width: 140px;}*/
	</style>
	<!-- <div class="row page-row">
		<div class="profile-container max-960">
			<div class="col-sm-3">
				<div class="img-container">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Doctor_1.png">
				</div>			
				<div><a href="doctorsdomain.php" class="pw-btn blue">View Profile</a></div>
				<div><a href="book-online.php" class="pw-btn red">Book Online</a></div>
			</div>
			<div class="col-sm-5">
				<div class="name">Dr. Vanitha Rao Pangal</div>
				<div class="designation">Paediatrician</div>
				<div class="description">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
					standard dummy text ever since the 1500s.
				</div>
				<div class="additional-fields-container">
					<div class="col-sm-6">
						<div class="heading">
							CHILDCARE CLINIC
						</div>
						<div class="info">						
							<div>MON - Thu</div>
							<div>10am - 2pm</div>
							<div>+91 80 43760903</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="heading">
							CHILDCARE CLINIC
						</div>											
						<div class="info">						
							<div>MON - Thu </div>
							<div>10am - 2pm</div>
							<div>+91 80 43760903</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				
			</div>
		</div>
	</div> -->



	<?php include "../includes/footer.php" ?>
	<?php include "../includes/include-js.php" ?>
	<script>
		$(function(){
			
			//Table1
			$('#doctable1-arrow-right').click(function(){
				if($('.doctor-table.table1').css('left') == '-2214px')
					return;
				var tablePos=$('.doctor-table.table1').position();
				$('.doctor-table.table1').css("left",tablePos.left-82);
			});
			$('#doctable1-arrow-left').click(function(){
				if($('.doctor-table.table1').css("left")=="0px")
					return;
				var tablePos=$('.doctor-table.table1').position();
				$('.doctor-table.table1').css("left",tablePos.left+82);
			});

			//Table2
			$('#doctable2-arrow-right').click(function(){
				if($('.doctor-table.table2').css('left') == '-2214px')
					return;
				var tablePos=$('.doctor-table.table2').position();
				$('.doctor-table.table2').css("left",tablePos.left-82);
			});
			$('#doctable2-arrow-left').click(function(){
				if($('.doctor-table.table2').css("left")=="0px")
					return;
				var tablePos=$('.doctor-table.table2').position();
				$('.doctor-table.table2').css("left",tablePos.left+82);
			});

			//Table3
			$('#doctable3-arrow-right').click(function(){
				if($('.doctor-table.table3').css('left') == '-2214px')
					return;
				var tablePos=$('.doctor-table.table3').position();
				$('.doctor-table.table3').css("left",tablePos.left-82);
			});
			$('#doctable3-arrow-left').click(function(){
				if($('.doctor-table.table3').css("left")=="0px")
					return;
				var tablePos=$('.doctor-table.table3').position();
				$('.doctor-table.table3').css("left",tablePos.left+82);
			});
		
		});
	</script>

<?php include "../includes/end.php" ?>
