<?php include "includes/start.php" ?>
<?php include "includes/header.php" ?>
<?php include "includes/db_connect.php" ?>
		<link href="css/doctorsdomain.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="meet-our-doctors" data-pink-name="pink-followup"></div>
	<?php include "includes/menu-patients.php" ?>
		<?php		
						$id=$_GET['doc_id'];
						$qry1= "SELECT * FROM `pw_doctors` WHERE `doc_id`='$id'";
							$qry_rslt1 = mysql_query($qry1);
							while($result1 = mysql_fetch_array($qry_rslt1))
							{								
						?>	
	
	<div class="banner-bg banner-bg-doctors-domain ">
		<div class="content-area ">
			<div class="red-text hidden-xs hidden-sm">
				<i>
					Polio Camp at <?php echo $result1['visit_clinic_name'];?><br>on <strong>24th Dec, 2014</strong><br><br>
					Come with your child and<br>
					make a difference to their lives
				</i>
			</div>
		</div>
	</div>


	
	<div class="page-row doctors-domain row-bg-1 hidden-xs">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-4 col-xs-12">
				<img src="<?php echo $result1['doc_photo'];?>" width="260" height="100" style="border-radius:100px;"><br>
				<span class="red-text">Health Tips from <?php echo $result1['doc_name'];?>!</span><br>
				Receive Useful Health Tips,<br>
				Updates & lots more via Email!<br><br>
				<!--a href="subscribe2.php" class="doctor-button blue">Subscribe</a-->
				<?php
				echo '<a href="subscribe2.php?doc_id='.$_GET['doc_id'].'" class="doctor-button blue">Subscribe</a>';
			?>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-8 col-xs-12">
				<div class="details">
					<div class="name"><?php echo $result1['doc_name'];?></div>
					<div class="designation red-text"><?php echo $result1['doc_specialities'];?></div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
				</div>
				<div class="clearfix"></div>
				<div class="message">
					<?php //echo $result1['doc_specialities'];?>
				</div>

				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Services.png">
						</div>
						<div class="info-data">
							<div class="red-text">SERVICES</div>
							<?php echo nl2br($result1['more_expertise']);?><br>
							
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image"></div>
						<div class="info-data second-info">
							<div class="red-text"><br></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Child-Clinic.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_clinic_name'];?></div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Hospital.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_hospital_name'];?></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-12 col-md-12 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Education.png">
						</div>
						<div class="info-data">
							<div class="red-text">EDUCATION</div>
							<?php echo $result1['doc_qualification'];?><br>
							
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Membership.png">
						</div>
						<div class="info-data">
							<div class="red-text">MEMBERSHIPS</div>
							Indian Academy of<br>
							Paediatrics (IAP)<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Registration.png">
						</div>
						<div class="info-data">
							<div class="red-text">REGISTRATIONS</div>
							85652 Karnataka Medical Council<br>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	

	<div class="page-row doctors-domain row-bg-1 visible-xs">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-12 col-xs-12">
				<img src="<?php echo $result1['doc_photo'];?>"><br>
				<div class="details">
					<div class="name"><?php echo $result1['doc_name'];?></div>
					<div class="designation red-text"><?php echo $result1['doc_specialities'];?></div>
					<div class="clearfix"></div>
				</div>								
			</div>
			<div class="clearfix"></div>
			<div class="calendar text-center">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
			</div>

			<div class="message">
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
				Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem
				Ipsum is simply dummy text of the printing and typesetting industry..
			</div>
			

			<div class="data-block col-xl-8 col-md-8 col-sm-12 col-xs-12">								
				<div class="clearfix"></div>				

				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Services.png">
						</div>
						<div class="info-data">
							<div class="red-text">SERVICES</div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image"></div>
						<div class="info-data second-info">
							<div class="red-text"><br></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Child-Clinic.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_clinic_name'];?></div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Hospital.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_hospital_name'];?></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-12 col-md-12 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Education.png">
						</div>
						<div class="info-data">
							<div class="red-text">EDUCATION</div>
							<?php echo $result1['doc_qualification'];?><br>
							
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Membership.png">
						</div>
						<div class="info-data">
							<div class="red-text">MEMBERSHIPS</div>
							Indian Academy of<br>
							Paediatrics (IAP)<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Registration.png">
						</div>
						<div class="info-data">
							<div class="red-text">REGISTRATIONS</div>
							85652 Karnataka Medical Council<br>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- <div class="page-tagline doctors-domain">
		<div class="tag">DR. VANITHA OFFERS YOU THESE<br>VIRTUAL SERVICES FOR ENHANCED CARE</div>
	</div> -->
	
	<div class="our-bouquet-container widgets-row">						
		<div class="content-area">
			<div class="header"><?php echo strtoupper($result1['doc_name']);?> OFFERS YOU THESE <span class="hidden-xs"><br></span> VIRTUAL SERVICES FOR ENHANCED CARE</div>
			<div class="clearfix"></div>

			<div class="widgets-image-container">
			<?php if ($result1['activate_online_query']!=''){?>
				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkQuery.php">
						<img src="img/home/patients/Patients_Icon_PinkQuery.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Query</strong></div>
						<div class="hr-container"><hr></div>
						<!-- <img src="img/home/patients/Patients_Icon_PinkLine_for Service Buttons.png" class="line"> -->
						<div class="widget-text">Our Doctors are available<br>now to answer your<br>questions and give advice.</div>
					</a>
				</div>
			<?php } if ($result1['appoint_flag']!='0'){?>
				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkAppoint.php">
						<img src="img/home/patients/Patients_Icon_pinkAppoint.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Appoint</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our synced calendar<br>makes it simple to<br>schedule doctor visits.</div>
					</a>
				</div>
			<?php } if ($result1['activate_online_consultation']!=''){?>
				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkfollowup-online.php">
						<img src="img/home/patients/Patients_Icon_pinkFollowUp.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>FollowUp</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our Virtual Follow-ups<br>make connecting with<br>your doctor fast & easy.</div>
					</a>
				</div>
			<?php } ?> 
				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkOpinion.php">
						<img src="img/home/patients/Patients_Icon_pinkOpinion.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Opinion</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our credible second<br>opinions help you make<br>the best health decisions.</div>
					</a>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>


	<div class="page-tagline doctors-domain">
		<div class="content-area">
			<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Timings.png" class="hidden-xs">
			<span class="tag">Check for <?php echo $result1['doc_name'];?>'s In-Clinic Consult Timings here.</span>
		</div>
		<div class="content-area">
			<div class="calendar-container">
				<table>
					<tr>
						<th>Mon</th><th>Tue</th><th>Wed</th><th>Thur</th><th>Fri</th> <th>Sat</th>
					</tr>

					<tr>
						<td></td> <td></td> <td>9:15am</td> <td></td> <td></td> <td>9:15am</td> 															
					</tr>
					<tr>
						<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td>9:30am</td> 															
					</tr>
					<tr>
						<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td>9:45am</td> 															
					</tr>
					<tr>
						<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>															
					</tr>
					<tr>
						<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>															
					</tr>
					<tr>
						<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td>10:30am</td> <td></td>															
					</tr>
					<tr>
						<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td>10:45am</td> <td></td>															
					</tr>
					<tr>
						<td></td> <td>More...</td> <td>More...</td> <td></td> <td>More...</td> <td></td>															
					</tr>
				</table>
			</div>
		</div>
	</div>

	<!-- <div class="image-block visible-xs text-center" style="margin-top: 20px;">							
		<span class="red-text">Health Tips from Dr. Vanitha!</span><br>
			Receive Useful Health Tips,<br>
			Updates & lots more via Email!<br><br>
		<a href="subscribe2.php" class="doctor-button blue">Subscribe</a>
	</div> -->
	<?php } ?>

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
