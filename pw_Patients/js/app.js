$( document ).ready(function() {

  $('.scrollLinks a').smoothScroll();


  $('.menu-'+$('.menu-data').data('main-page')).addClass('active-menu');
  

 if($('.menu-data').data('main-page') == "talktous"){
    $('.menu-aboutus span').css("border","0px");
  }

  if($('.menu-data').data('main-page') == "corporates"){
    $('.menu-doctors span').css("border","0px");
  }

  if($('.menu-data').data('main-page') == "doctors"){
    $('.menu-patients span').css("border","0px");
  }

  $('.sub-'+$('.menu-data').data('sub-page')).addClass('active'); 
  $('.pink-buttons.'+$('.menu-data').data('pink-name')).addClass('active');


 
    
    
  
});



function scrollSection(divId){
  $.smoothScroll({
    scrollElement: $('body'),
    scrollTarget: '#'+divId
  });
  return false;
}

$('.follow-up-carousel').owlCarousel({
        items:1,
        //loop:true,
        //center:true,
        margin:0,
        URLhashListener:true,
        //autoplayHoverPause:true,
        startPosition: 'zero',
        
        smartSpeed:1000
    });

function moveTableRight(htmlObject){
  var tableObject = $(htmlObject).parent().children().children('.doctor-table');
  var leftpos = tableObject.position();
  console.log(leftpos.left);
   if(leftpos.left <= -237 )
  {
    tableObject.css("left", "-242px");
    return;
  }

  if(leftpos.left > -242 )
  {
    tableObject.css("left", leftpos.left-80);
    return;
  }

   if(leftpos.left <= -241 && leftpos.left > -242 )
  {
    tableObject.css("left", "-323px");
    return;
  }
}


function moveTableLeft(htmlObject){
  var tableObject = $(htmlObject).parent().children().children('.doctor-table');
  var leftpos = tableObject.position();
  console.log(leftpos.left);
  if(leftpos.left >= 0){
    tableObject.css("left", "0px");
    return;
  }

  if( leftpos.left < 0 && leftpos.left > -4)
  {
    tableObject.css("left", "0px");
    return;
  }
  if( leftpos.left < 0 )
  {
    tableObject.css("left", leftpos.left+80+"px");
    return;
  }
}


function doctorContentToggle(htmlObject){
 
  $(htmlObject).removeClass('active'); 
  $('.doctors-clinic-home-page').fadeOut(function(){
     $('.doctors-doctor-home-page').fadeIn();
  });

  $('.clinic-home-button').removeClass('active'); 
  $('.doctor-home-button').addClass('active');
  
  $('.grey-row .content-area').html('Build stronger patient relationships without sacrificing care.<br>We enable you to upgrade your practice to the next level.');
}

function clinicContentToggle(htmlObject){
 
  $('.doctors-doctor-home-page').fadeOut(function(){
     $('.doctors-clinic-home-page').fadeIn();
  });  

  $('.doctor-home-button').removeClass('active'); 
  $('.clinic-home-button').addClass('active'); 
   $('.grey-row .content-area').html('Differentiate your Centre with our Virtual Services.<br>We make it easy for you to control disease management for your patients.');
}


function showLogin(){
  $('.modal-container').show();
  $('body').css("overflow","hidden");
}

function hideLogin(){
  $('.modal-container').hide();
  $('body').removeAttr("style");
}


function makeThisActive(htmlObject, bubbleCount){
  $(".link-box a").removeClass('active');
  $(htmlObject).addClass('active');
  $('.bubbles').removeClass('active-bubble');
  $('.bubble-'+bubbleCount).addClass('active-bubble');
}

var nav = responsiveNav(".nav-main-menu", { // Selector
  animate: true, // Boolean: Use CSS3 transitions, true or false
  transition: 500, // Integer: Speed of the transition, in milliseconds
  label: "Menu", // String: Label for the navigation toggle
  insert: "before", // String: Insert the toggle before or after the navigation
  customToggle: "", // Selector: Specify the ID of a custom toggle
  closeOnNavClick: false, // Boolean: Close the navigation when one of the links are clicked
  openPos: "relative", // String: Position of the opened nav, relative or static
  navClass: "nav-collapse", // String: Default CSS class. If changed, you need to edit the CSS too!
  navActiveClass: "js-nav-active", // String: Class that is added to <html> element when nav is active
  jsClass: "js", // String: 'JS enabled' class which is added to <html> element
  init: function(){}, // Function: Init callback
  open: function(){}, // Function: Open callback
  close: function(){} // Function: Close callback
});


var subNav = responsiveNav(".page-menu", { // Selector
  animate: true, // Boolean: Use CSS3 transitions, true or false
  transition: 500, // Integer: Speed of the transition, in milliseconds
  label: '<i class="fa fa-bars menu-icon" ></i>', // String: Label for the navigation toggle
  insert: "before", // String: Insert the toggle before or after the navigation
  customToggle: "", // Selector: Specify the ID of a custom toggle
  closeOnNavClick: false, // Boolean: Close the navigation when one of the links are clicked
  openPos: "relative", // String: Position of the opened nav, relative or static
  navClass: "nav-collapse", // String: Default CSS class. If changed, you need to edit the CSS too!
  navActiveClass: "js-nav-active", // String: Class that is added to <html> element when nav is active
  jsClass: "js", // String: 'JS enabled' class which is added to <html> element
  init: function(){}, // Function: Init callback
  open: function(){}, // Function: Open callback
  close: function(){} // Function: Close callback
});



$("#uploadBtn").onchange = function () {
    $("#uploadFile").text($(this).val());
}





var App = {
  Date: {},
  Contact: {}
};

App.Date.init = function(){
  this.initEventHandlers();
}.bind(App.Date);

App.Date.getNext = function(){
  
}.bind(App.Date);

App.Date.initEventHandlers = function(){
    //NEXT BUTTON EVENT HANDLER
    $('#arrow-right').on('click', function(){
      App.Date.getNext();
    });

}.bind(App.Date);


//CONTACT US 

// App.Contact.submit = function(){

// }.bind(App.Contact);

// App.Contact.initEventHandlers = function(){

// }


              /*$(function(){
                $('.menu-topup-li .menu-topup').click(function(){
                  $('.main-menu ul li a').each(function(){
                    $(this).removeClass("active-menu");
                  });
                  $(this).addClass("active-menu");
                  $('.topup-dropdown-container').toggle();
                  $('.menu-talktous span').css("border","0px");
                });
              });*/


/*$(document).on('click', function (e) {
    if ($(e.target).closest('.topup-dropdown-container').length === 0) {
        $('.topup-dropdown-container').hide();
    }
});*/


/*$(document).click(function(event){
    if ( !$(event.target).hasClass('topup-dropdown-container')) {
         $(".topup-dropdown-container").hide();
    }
});*/

/*$(function(){
  $('.topup-dropdown-container').click(function(e){
      e.stopPropagation();
      $('#consult-modal').modal();

  });
})*/


/*$(document).mouseup(function(e){
  var container=$('.topup-dropdown-container');
  if(!container.is(e.target) && container.has(e.target).length === 0){
    container.hide();
  }
});*/

/*$(document).click(function(event) {
    if ( !$(event.target).hasClass('.topup-dropdown-container')) {
         $(".topup-dropdown-container").hide();
    }
});*/


/*works*/
/*$('body').mouseup(function() {
    $('.topup-dropdown-container').hide();
});

$('.topup-dropdown-container').mouseup(function(e) {
    e.stopPropagation();
});
*/



/*Top up card menu background on hover*/
$(function(){
  $('.menu-topup-li').mouseenter(function(){
    $(this).addClass("sfHover");
  });
  $('.menu-topup-li').mouseleave(function(){
    $(this).removeClass("sfHover");
  });
});



   


/*Start Table right left arrow js*/

    $(document).ready(function(){
      $('.arrow-right-1').click(function(){
        if($('table.booking-table').css('left') == '-2160px')
          return;
        var tablePos=$('.booking-table').position();
        var thwidth=$('.booking-table tr th').width();
        $('.booking-table').css("left",tablePos.left-90);
      });
    });
  
  
    $(document).ready(function(){
      $('.arrow-left-1').click(function(){
        console.log($('table.booking-table'));
        console.log(document.getElementsByClassName('booking-table'));

        if($('table.booking-table').css('left') >= '0px' || $('table.booking-table').css('left') == 'auto')
          return;
        var tablePos=$('.booking-table').position();
        $('.booking-table').css("left",tablePos.left+90);
      });
    });

  /*End Table right left arrow js*/
  


/* start Getting picked time date from calender and highlighting selected text*/
  
    $(function(){
      
        styleActiveTime();
        getTimeDate();  
        
    });  
    function getTimeDate(){       
       $('.booking-table tbody tr .time-value').click(function(){
        var getTime=$(this).text();
        var getTs=$(this).data('ts');
        
          var d=new Date(getTs*1000);
        var getDate=d.getDate();
        var getMonth=d.getMonth();
        var getYear=d.getFullYear();
        var monthName=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
        
        $('#appointment-time').val(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);
        
        /*$('#bookonline-time-slot').text(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);*/
      });
    } 

    function styleActiveTime(){
      $('.booking-table tbody tr .time-block').click(function(){
        $(this).each(function(){
          $('.booking-table tbody tr .time-block').removeClass("active-time");
        });
        $(this).addClass("active-time");
      });
    }

    function getNewTimeDate(){        
       $('.booking-table tbody tr .new-time-value').click(function(){
        var getTime=$(this).text();
        var getTs=$(this).data('ts');
        
          var d=new Date(getTs);
        
        var getDate=d.getDate();
        var getMonth=d.getMonth();
        var getYear=d.getFullYear();
        var monthName=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
        
        $('#appointment-time').val(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);
        
        /*$('#bookonline-time-slot').text(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);*/
      });
    }   

  
    $(function(){

      $('.booking-table tbody tr:nth-child(6)').click(function(){
        /*$(this).css("display","none");*/
        $(this).addClass("row-hidden");
        var d=new Date();
        var tss=d.getTime();
        var html="";
        for(var j=0;j<=1;j++){
          html+='<tr class="new-row">';
          for(var i=0;i<=29;i++){
            var currentTs=tss+3600*24*1000*i;
            html+='<td class="new-time-value time-block" data-ts='+currentTs+'><span>9:15am</span></td>';
          }
          html+='</tr>';
        } 
        
        html+='<tr class="less-row">';
          for(var t=0;t<=29;t++){
          html+='<td><span class="less-value">Less..</span></td>';
        }
        html+='</tr>';

        $(html).appendTo(".booking-table");
        /*$(less).appendTo(".booking-table");*/
        styleActiveTime();
        getNewTimeDate();
        lessClick();
      });

    });


function lessClick(){
  $('.booking-table tr td .less-value').click(function(){
      $('.booking-table tr.less-row').addClass("row-hidden");
     $('.booking-table tr.new-row').addClass("row-hidden");
     $('.booking-table tr.more-row').removeClass("row-hidden");

  });
}




 /* End Getting picked time date from calender and highlighting selected text*/
 

 /*$(function(){
  $("#book").click(function(){
   
      var id=$('.booking-table .active-time').attr("id");
  
      var idd=id+"a";
     
   
    
  });
 });
*/


/*$(function(){
  $("#bookonline-overlay").on('loaded.bs.modal',function(){
      alert("test");
  });
});    
  */


/*Adding click event*/  

$(function(){
  $("#book").click(function(){
    $("#bookonline-overlay").modal('show');
      var id=$('.booking-table .active-time').attr("id");
      var idd="#"+id+"a";     
      /*console.log(idd);*/
      $(idd).click();
  });
})  
  


