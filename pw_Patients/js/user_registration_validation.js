

// JavaScript Document


	function add_user_register(form)
	{
		Validated = true;        
        document.getElementById("nameErrDiv").innerHTML = ""; 
		document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
		document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = "";         
        
        var name = form.name.value;
		var age = form.age.value;
        var gender = form.gender.value;
        var email = form.email.value;
        var password = form.password.value;
        var confirmpassword = form.cpassword.value;
        var mobile = form.mobile.value;
		var card = form.regcard.value
		
	if ( name =='' )
	{            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
		 if (name != "") {
	 if ( /[^A-Za-z +$]/.test(name)) {
	document.getElementById("nameErrDiv").innerHTML = "Please enter characters only";      	
	form.regname.focus();
	Validated = false;
    }
	}
	if ( age =='' )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
		Validated = false;
	}else if ( age < 18 )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
		Validated = false;
	}        
        if ( gender =='' )
	{            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
		Validated = false;
	}	
        if ( email =='' )
	{            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
		Validated = false;
	}
	if ( email !='' )
	{
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
			document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";       
			Validated = false;
	}
	}
        if ( password =='' )
	{            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
		Validated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
        if ( password != confirmpassword )
	{            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch Try again!";
		Validated = false;
		repswValidated = false;
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  Validated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  Validated = false;
		 } 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  Validated = false;
		 }
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 Validated = false;
		 }
			
	}
        if( mobile == "" )
	{            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
		Validated = false;
	} 
	if(Validated)
	{               
               // document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
               // document.getElementById("login_submit").disabled=true;
                document.getElementById("btnSubmit").value = "Processing....";            
                // -------------------ajax start--------------------- //                
                name = encodeURI(name);
		age = encodeURI(age);
                gender = encodeURI(gender);
		card = encodeURI(card);
                email = encodeURI(email);
                password = encodeURIComponent(password);
                mobile = encodeURI(mobile);                
                var paramString = 'name='+name+'&age='+age+'&gender='+gender+'&email='+email+'&password='+password+'&mobile='+mobile+'&regcard='+card;

                $.ajax({  
                        type: "POST",  
                        url: "../../actions/pw_regpink.php",  
                        data: paramString,  
                        success: function(response) {  
	                		//alert(response);            
                                if(response==true){
                                  
                                    //document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                   // document.getElementById("login_submit").disabled=false;
                                    
                                   // document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully Register<br />Please check your mail to complete registration.</div>";
                                    
                                }else{       
                                    
                                   // document.getElementById("emailErrDiv").innerHTML= response;
                                    document.getElementById("btnSubmit").value = "Register";
                                  //  document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                  //  document.getElementById("login_submit").disabled=false;                                    
                                }
                        }
                });
                return false;
                // -------------------ajax End--------------------- //                
		}		
		else
		{                
			return false;
		}	
	}

	 
	function valid_pswd(form)
	{
	var pswValidated = false;
	var repswValidated = false;
	var password = form.password.value;
    var confirmpassword = form.cpassword.value;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("passwordErrDiv").innerHTML = ""; 
    document.getElementById("repasswordErrDiv").innerHTML = ""; 
	if (password=='')
	{
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";		
    	pswValidated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(confirmpassword!='')
	{
		if(password!=confirmpassword)
		{
			document.getElementById("repasswordErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			 pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			 pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			 pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
			 pswValidated = true;
	}
	 	
}
function card_check(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("cardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: "../../check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        
                            if(response==true){
                                document.getElementById("cardErrDiv").innerHTML= "<font color='green'>Valid Card</font>";
                                return false;
                            }else{
                                document.getElementById("regcard").value= "";
                                document.getElementById("cardErrDiv").innerHTML= response;
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //        
}


function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}


function validate_consult(type){
    
    document.getElementById("login_nxt_page").value=type;
    document.getElementById("register_nxt_page").value=type;
        
    setTimeout(login_callpopup(true,0),200);
    
}
