<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<link href="css/pinkFollowUp.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-followup"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-pinkfollowup">
		<div class="content-area">
			<div class="red-text">Do I have to visit the clinic every time<br>I have a follow up session?</div>
		</div>
	</div>

	<?php include "includes/menu-pink-buttons.php" ?>
	
	<div class="page-tagline pink-page row-bg-1">
		<img src="img/ourservices/pinkfollowup/pink-followup_How-It-Works_Icon_pinkFollow up1.png" style="margin-top: 0px;"><i>pink</i><strong>FollowUp</strong>
		<div class="text">
			<i>
				Forget taking time out of your busy schedule to sit in traffic <br>
				and waiting rooms for a follow-up visit!<br><br>
				pinkFollowUp brings your doctor to you, wherever you are, <br>
				online or by phone.<br>
			</i>
			<br><br>
			<strong>
				With a pinkWhale card and your unique identification number, you can directly <br>
				access your doctor's calendar and schedule a virtual follow-up visit whenever <br>
				is convenient for you! Take the hassle out of seeing your doctor and stay in <br>
				the pink of health!<br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="followup-row">
		<div class="max-960">
			<div class = "link-box">
				<a href = "#zero" class="active" onclick="makeThisActive(this,'one');">Online FollowUp</a>	
				<a href = "#one" onclick="makeThisActive(this,'two');">Tele FollowUp</a>
			</div>
		</div>

		<div class="bubbles-container">
			<div class="bubble-one bubbles active-bubble"></div>
			<div class="bubble-two bubbles"></div>
			<div class="bubble-three bubbles"></div>
		</div>
		<div class="how-it-works">
			<div class="content-area">
				<img src="img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png">
				<div class="title">How does it work?</div>
			</div>
		</div>
		<div class="follow-up-carousel">
			<div class="work-bg work-pink-followup-online pink-follow-up " data-hash="zero"></div>
			<div class="work-bg work-pink-followup-tele pink-follow-up" data-hash="one"></div>
		</div>
	</div>

	
	<div class="row-before-video row-bg-2">
		SEE HOW <i>pink</i>QUERY<br>CAN HELP YOU IN TIME OF NEED
	</div>

	<div class="work-bg video-bg video-image">
		<iframe width="100%" height="480" src="//www.youtube.com/embed/sV6ff8UbjHk?list=PLu4bXNfdFdSxW0UGWQwSMkNBCOAorfB1E" frameborder="0" allowfullscreen></iframe>
	</div>

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<h1 class="form-title">STAY IN THE LOOP WITH<br>OUR HEALTH TIPS AND UPDATES</h1>
			<div class="form-left col-xl-8 col-md-8 col-sm-12 col-xs-12">
				<div class="form-header">
					<img src="img/Subscribe1_icon_Apply.png">
					<span class="form-tagline">Subscribe to our tips and latest updates</span>
				</div>
				<div class="form-elements">
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Name:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Email:</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Phone Number:</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6"></div><div class="input col-xl-8 col-md-6 col-sm-6"><button type="submit" class="submit">Continue</button></div>
				</div>
			</div>
			<div class="message col-xl-4 col-md-4 col-sm-12 col-xs-12">

				Like the idea of <b><i>pink</i>Whale</b> but aren't <br>
				sure of Enrolling yet?<br>
				Not a problem! <br><br>
				Drop in your Email ID and we'll send you <br>
				a newsletter with Updates &amp; Health Tips <br>
				to keep you in the Pink of Health all year 
				round.
			</div>
		</div>
	</div>

	
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
