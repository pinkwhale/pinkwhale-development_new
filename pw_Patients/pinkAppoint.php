<?php include "../includes/start.php" ?>
	<?php include "../includes/header.php" ?>
	<link href="../css/pinkAppoint.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-appoint"></div>
	<?php include "menu-patients.php" ?>
		
	<div class="banner-bg banner-bg-pinkappoint">
		<div class="content-area hidden-xs">
			<div class="banner-form">
				<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
				<form class="form-horizontal pw-form" action="findadoctor.php" role="form" style="max-width: 400px;">
					<div class="form-group">
						<label class="col-xs-5 control-label">Choose a Specialty:</label>
						<div class="col-xs-7">
							<select class="form-control">
								<option>Pediatrician</option>
								<option>Pediatrician</option>					
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-5 control-label">Choose a City:</label>
						<div class="col-xs-7">
							<select class="form-control">
								<option>Bangalore</option>
								<option>Mysore</option>
								<option>Ahmedabad</option>
								<option>Delhi</option>
								<option>Jammu</option>
							</select>
						</div>
					</div>  
					<div class="form-group">
						<div class="col-xs-offset-5 col-sm-7">
							<button type="submit" class="btn pw-btn">Find a Doctor</button>
						</div>
					</div>
				</form>
			</div>			
		</div>
	</div>


	<?php include "../includes/menu-pink-buttons.php" ?>


	<div class="banner-form visible-xs mobile-form">
		<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
		<form class="form-horizontal" action="findadoctor.php" role="form">
			<div class="form-group pw-form">
				<label class="col-xs-5 control-label">Choose a Specialty:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Pediatrician</option>
						<option>Pediatrician</option>					
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-5 control-label">Choose a City:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Bangalore</option>
						<option>Mysore</option>
						<option>Ahmedabad</option>
						<option>Delhi</option>
						<option>Jammu</option>
					</select>
				</div>
			</div>  
			<div class="form-group">
				<div class="col-xs-offset-5 col-sm-7">
					<button type="submit" class="btn pw-btn">Find a Doctor</button>
				</div>
			</div>
		</form>
	</div>
	
	<div class="page-tagline pink-page row-bg-1">
		<img src="../img/ourservices/pinkappoint/pinkAppoint_Icon_PinkAppointment1.png"><i>pink</i><strong>Appoint</strong>
		<div class="text">
			<i>
				Forgot to book an appointment during clinic hours?<br>
				Book an appointment here!<br><br>
				We're not trying to replace your face-to-face visits. <br>
				pinkAppoint allows you to book an appointment within a few clicks! <br><br>
			</i>
			<br>
			<strong>
				One in-clinic visit makes you eligible for virtual follow ups. We even make <span class="hidden-xs"><br></span>
				scheduling that visit simple. Our pinkAppoint Service syncs with the doctor's <span class="hidden-xs"><br></span>
				calendar so you can make an appointment when it's convenient for you. <br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="how-it-works">
		<div class="content-area">
			<img src="../img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png">
			<div class="title">How does it work?</div>
		</div>
	</div>

	<div class="how_it_works">
		<img src="img/ourservices/pinkappoint/pinkAppoint.png" style="max-width: 100%;">
	</div>

	<!-- <div class="work-bg work-pink-appoint"></div> -->

	<div class="row-before-video row-bg-2">
		SEE HOW <i>pink</i>Appoint<br>CAN HELP YOU WHEN YOU NEED IT MOST

	</div>

	<div class="work-bg video-bg video-image">
		<iframe width="100%" src="//www.youtube.com/embed/sV6ff8UbjHk?list=PLu4bXNfdFdSxW0UGWQwSMkNBCOAorfB1E" frameborder="0" allowfullscreen></iframe>		
	</div>

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<div class="form-header">
				<img src="../img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Register with pinkWhale Today!</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form">
						<div class="form-group">
							<label for="name" class="col-xs-5 control-label">Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="name" >
							</div>
						</div>	
						<div class="form-group">
							<label class="col-xs-5 control-label">Age/Gender:*</label>
							<div class="col-xs-2 col-md-2 pw-age">
								<input type="textbox" class="form-control" >
							</div>
							<div class="col-xs-5 col-md-5">
								<input type="textbox" class="form-control" >
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" >
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-xs-5 control-label">Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="password" >
							</div>
						</div>	
						<div class="form-group">
							<label for="cpassword" class="col-xs-5 control-label">Confirm Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="cpassword" >
							</div>
						</div>	
						<div class="form-group">
							<label for="report" class="col-xs-5 control-label">Upload Report:*</label>
							<div class="col-xs-7">
								<div class="fileUpload pw-btn">
								    <span>Choose File</span>
								    <input id="uploadBtn" type="file" class="upload upload-button" />
								</div>
								<div id="uploadFile" class="upload-file-path"></div>
								<div class="clearfix"></div>																					
							</div>							
						</div>	
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" value="Submit">
							</div>
						</div>												
					</form>
				</div>				
			</div>
			<div class="message form-right col-sm-4 hidden-xs">

				No need to wait on the phone to <br>
				schedule an appointment with <br>
				your doctor.<br><br>
				Schedule an in-clinic visit here to <br>
				become eligible for our other <br>
				virtual health services.<br>
			</div>
		</div>
	</div>
	

	<?php include "../includes/footer.php" ?>
	<?php include "../includes/include-js.php" ?>
<?php include "../includes/end.php" ?>
