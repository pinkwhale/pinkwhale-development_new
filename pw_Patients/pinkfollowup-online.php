<?php include "../includes/start.php" ?>
	<?php include "../includes/header.php" ?>
		<?php include "../includes/pw_db_connect.php"?>
	<link href="../css/pinkFollowUp.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="js/followup_subscribe_validation.js"></script>

	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-followup"></div>
	<?php include "menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-pinkfollowup">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text">Do I have to visit the clinic every time<br>I have a follow up session?</div>
			<div class="grey-text">Chat with your Doctor virtually<br>anytime, anywhere!</div>
			<div class="consult-btn-container"><button class="pw-consult-btn" data-toggle="modal" data-target="#consult-modal">Start a Consult</button></div>
		</div>
	</div>

	<?php include "../includes/menu-pink-buttons.php" ?>
	
	<div class="page-tagline pink-page row-bg-1">
		<img src="img/ourservices/pinkfollowup/pink-followup_How-It-Works_Icon_pinkFollow up1.png" style="margin-top: 0px;"><i>pink</i><strong>FollowUp</strong>
		<div class="text">
			<i>
				Forget taking time out of your busy schedule to sit in traffic <span class="hidden-xs"><br></span>
				and waiting rooms for a follow-up visit!<span class="hidden-xs"><br></span><br>
				pinkFollowUp brings your doctor to you, wherever you are, <span class="hidden-xs"><br></span>
				online or by phone.<span class="hidden-xs"><br></span>
			</i>
			<br><br>
			<strong>
				With a pinkWhale card and your unique identification number, you can directly <span class="hidden-xs"><br></span>
				access your doctor's calendar and schedule a virtual follow-up visit whenever <span class="hidden-xs"><br></span>
				is convenient for you! Take the hassle out of seeing your doctor and stay in <span class="hidden-xs"><br></span>
				the pink of health!<br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="followup-row">
		<div class="max-960 hidden-xs">
			<div class = "link-box">
				<a href = "#zero" class="active" onclick="makeThisActive(this,'one');">Online FollowUp</a>	
				<a href = "#one" onclick="makeThisActive(this,'two');" class="second-lnk">Tele FollowUp</a>
			</div>
		</div>

		<div class="bubbles-container">
			<div class="bubble-one bubbles active-bubble"></div>
			<div class="bubble-two bubbles"></div>
		</div>
		<div class="how-it-works">
			<div class="content-area">
				<img src="img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png">
				<div class="title">How does it work?</div>
			</div>
		</div>
		<div class="follow-up-carousel">
			<div class="" data-hash="zero"><img src="img/ourservices/pinkfollowup/OnlineFollowUp.png"></div>
			<div class="" data-hash="one"><img src="img/ourservices/pinkfollowup/TeleFollowUp.png"></div>
		</div>
	</div>

	<style type="text/css">
		.follow-up-carousel img{max-width: 100%;}
		.follow-up-carousel.owl-carousel .owl-item img{width: auto; margin: 0px auto;}
		.follow-up-carousel.owl-carousel .owl-item {}
		.followup-row .bubbles-container{bottom: -20px;}
		.follow-up-carousel.owl-carousel{margin-bottom: 40px;}
		.follow-up-carousel.owl-carousel {margin-bottom: 60px;} 
	</style>

	
	<div class="row-before-video row-bg-2">
		SEE HOW <i>pink</i>QUERY<br>CAN HELP YOU IN TIME OF NEED
	</div>

	<div class="work-bg video-bg video-image">
		<iframe width="100%" src="//www.youtube.com/embed/sV6ff8UbjHk?list=PLu4bXNfdFdSxW0UGWQwSMkNBCOAorfB1E" frameborder="0" allowfullscreen></iframe>
	</div>

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<h1 class="form-title">STAY IN THE LOOP WITH<br>OUR HEALTH TIPS AND UPDATES</h1>
			<div class="form-header hidden-xs">
				<img src="../img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Hear more from our trusted doctors here!</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">	
				<div class="pw-form-container">							
					<form class="form-horizontal pw-form" role="form" action="actions/followup_subscribe_action.php" method="POST" name="followup_subscribe"
					enctype="multipart/form-data" id="followup_subscribe">
						<div class="form-group">
							<label for="name" class="col-xs-6 control-label">Name:*</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="name" name="name" >
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="name_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="email" class="col-xs-6 control-label">Email:*</label>
							<div class="col-xs-6">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="email_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="phone_no" class="col-xs-6 control-label">Phone Number:</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="phone_no" name="phone_no">
							</div>
						</div>		
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="phone_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group text-left">
							<div class="col-xs-offset-6 col-xs-6">
								<input type="submit" class="pw-btn" value="Submit" onclick="return add_followup_subscription(followup_subscribe)"/>
							</div>
						</div>												
					</form>
				</div>				
			</div>			
			<div class="message form-right col-sm-4 hidden-xs">
				Like the idea of <b><i>pink</i>Whale</b> but aren't <span class="hidden-xs hidden-sm"><br></span>
				sure of Enrolling yet?<span class="hidden-xs hidden-sm"><br></span>
				Not a problem! <span class="hidden-xs hidden-sm"><br><br></span>
				Drop in your Email ID and we'll send you <span class="hidden-xs hidden-sm"><br></span>
				a newsletter with Updates &amp; Health Tips <span class="hidden-xs hidden-sm"><br></span>
				to keep you in the Pink of Health all year 
				round.
			</div>
		</div>
	</div>

	
	

	<?php include "../includes/footer.php" ?>
	<?php include "../includes/include-js.php" ?>
<?php include "../includes/end.php" ?>
