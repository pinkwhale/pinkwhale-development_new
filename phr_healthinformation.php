<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td  width="220" height="364" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />

<?php include 'user_left_menu.php'; ?>
</td>

<td width="748" rowspan="2" valign="top" class="s90phrcontent">
<?php include("name_card_no.php");?>

<table border="0">
	<tr>
		<td height='15px'></td>
	</tr>
</table>
<table border="0" height="30" width="700" cellspacing="0" cellpadding="0" class="tbl" >
	<tr>
		<td>
			<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;">My Health Information</span>
		</td>
	</tr>
</table>	
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
<tr>
<th colspan="3">My Health Information</th></tr>

<!--####### Get User Name ########-->

<?php
$qry= "SELECT * FROM `user_details` where `user_id`='$user_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
			$user_name=$result['user_name'];
?>	

<tr>
    <!--<td width="473" class="s90dbdtbls_drname"><span style="color:#f609ab;"><?php echo $result['user_name']; ?></span></td>-->
    <td width="473" class="s90dbdtbls_drname"></td>

<?php
}
?>
<?php
$qry= "SELECT * FROM `user_health_info` where `card_no`='$user_id' ";
	$qry_rslt = mysql_query($qry);
	if(mysql_num_rows($qry_rslt)<=0)
	{
?>	

    <td width="43" ><a href="add-user-healthinfo.php"><input type="button" name="cancel" value="Add Profile"/></a></td>
<?php
}    
    ?>
    <td width="64" class="s90dbdtbls_drname"><a href="edit-user-healthinfo_test.php"><input type="submit" name="cancel" value="Edit Profile"/></a></td>
</tr>

<!--####### Display user health info - only for `health_info_id`='1' ########-->

<?php
$qry= "SELECT * FROM `user_health_info` where `card_no`='$user_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
		$gdate1=strtotime($result['user_dob']);
		$dob=date("d M Y", $gdate1);
?>	
<tr>
<td class="s90dbdtbls_drdetails" colspan="3">
<table width="540" border="0" cellspacing="3" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
    <tr>
        <td width="105" align="right"><b>Date Of Birth : </b></td>
        <td width="165"><font color="#000000"><?php echo $dob; ?></font></td>
        <td width="155" align="right"><strong>Medical Device/Implants :</strong></td>
        <td width="100"><font color="#000000"><?php echo $result['medical_device'] ?></font></td>
    </tr>
    <tr>
        <td align="right"><b>Gender : </b></td>
        <td><font color="#000000"><?php echo $result['user_gender'] ?></font></td>
        <td align="right"><strong>Blood Group :</strong> </td>
        <td><font color="#000000"><?php echo $result['user_blood_group'] ?></font></td>
    </tr>
    <tr>
        <td align="right"><b>Height : </b></td>
        <td><font color="#000000"><?php echo $result['user_height'] ?> &nbsp;cm</font></td>
        <td align="right"><strong>Blood Sugar :</strong> </td>
        <td><font color="#000000"><?php echo $result['user_blood_sugar'] ?>&nbsp;mg/dL</font></td>
    </tr>
    <tr>
        <td align="right"><b>Weight : </b></td>
        <td><font color="#000000"><?php echo $result['user_weight'] ?>&nbsp;Kg</font></td>
        <td align="right"><strong>Cholestrol :</strong> </td>
        <td><font color="#000000"><?php echo $result['user_cholostrol'] ?>&nbsp;mmol/L</font></td>
    </tr>
    <tr>
        <td align="right"><b>Marital Status : </b></td>
        <td><font color="#000000"><?php echo $result['user_merital_status'] ?></font></td>
        <td align="right"><strong>Blood Pressure :</strong> </td>
        <td><font color="#000000"><?php echo $result['user_blood_pressure'] ?>&nbsp;mmHg</font></td>
    </tr>
</table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
<tr><th>Fathers Health Information</th></tr>

<tr>
<td class="s90dbdtbls_drdetails">
<table width="540" border="0" cellspacing="3" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
    <tr>
        <td width="105" align="right"><b>Name : </b></td>
        <td width="165"><font color="#000000"><?php echo $result['father_name'] ?></font></td>
        <td width="155" align="right"><strong>Medical Device/Implants :</strong></td>
        <td width="100"><font color="#000000"><?php echo $result['father_medicaldevice'] ?></font></td>
    </tr>
    <tr>
        <td width="105" align="right"><b>Age : </b></td>
        <td width="165"><font color="#000000"><?php echo $result['user_father_age'] ?></font></td>
        <td align="right"><strong>Blood Group :</strong> </td>
        <td><font color="#000000"><?php echo $result['father_blood_group'] ?></font></td>
    </tr>
    <tr>
        <td align="right"><strong>Blood Pressure :</strong> </td>
        <td><font color="#000000"><?php echo $result['father_blood_pressure'] ?>&nbsp;mmHg</font></td>
        <td align="right"><strong>Blood Sugar :</strong> </td>
        <td><font color="#000000"><?php echo $result['father_blood_suger'] ?>&nbsp;mg/dL</font></td>
    </tr>
    <tr>
    	<td align="right"><b>Major Illness : </b></td>
        <td><font color="#000000"><?php echo $result['user_father_illness'] ?></font></td>
        <td align="right"><strong>Cholestrol :</strong> </td>
        <td><font color="#000000"><?php echo $result['father_cholestrol'] ?>&nbsp;mmol/L</font></td>
    </tr>
        <tr><td colspan="0" align="right"><b>Remarks : </b></td>
        <td colspan="0"><font color="#000000"><?php echo $result['user_father_remarks'] ?></font></td>
    </tr>
</table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
    <tr><th>Mothers Health Info:</th></tr>
    <tr><td class="s90dbdtbls_drdetails">
    <table width="540" border="0" cellspacing="3" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
    <tr>
        <td width="105" align="right"><b>Name : </b></td>
        <td width="165"><font color="#000000"><?php echo $result['mother_name'] ?></font></td>
        <td width="155" align="right"><strong>Medical Device/Implants :</strong></td>
        <td width="100"><font color="#000000"><?php echo $result['mother_medical_device'] ?></font></td>
    </tr>
    <tr>
        <td width="105" align="right"><b>Age : </b></td>
        <td width="165"><font color="#000000"><?php echo $result['user_mother_age'] ?></font></td>
        <td align="right"><strong>Blood Group :</strong> </td>
        <td><font color="#000000"><?php echo $result['mother_blood_group'] ?></font></td>
    </tr>
    <tr>
    	<td align="right"><strong>Blood Pressure :</strong> </td>
        <td><font color="#000000"><?php echo $result['mother_blood_pressure'] ?>&nbsp;mmHg</font></td>
        <td align="right"><strong>Blood Sugar :</strong> </td>
        <td><font color="#000000"><?php echo $result['mother_blood_sugar'] ?>&nbsp;mg/dL</font></td>
    </tr>
    <tr>
        <td align="right"><b>Major Illness : </b></td>
        <td><font color="#000000"><?php echo $result['user_mother_illness'] ?></font></td>
        <td align="right"><strong>Cholestrol :</strong> </td>
        <td><font color="#000000"><?php echo $result['mother_cholestrol'] ?>&nbsp;mmol/L</font></td>
    </tr>
        <tr><td colspan="0" align="right"><b>Remarks : </b></td>
        <td colspan="0"><font color="#000000"><?php echo $result['user_mother_remarks'] ?></font></td>
    </tr>
</table>
</td></tr></table>


<?php 
}
?>
</td></tr>
<tr>
  <td height="370" valign="top"></td>
</tr>
</table>

</td></tr></table>

<script type="text/javascript">
document.getElementById("menu_text4").style.fontWeight="bold";
document.getElementById("sub_menu1").style.fontWeight="bold";
</script>

<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
