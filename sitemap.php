<?php include "includes/start.php" ?>
<?php include "includes/header.php" ?>
<?php include "includes/page-menu-start.php" ?>
<?php include "includes/page-menu-end.php" ?>
<?php include "includes/site_config.php" ?>
<hr/>
<div class="sitemap-content max-960">
	<div class="title">SITE MAP</div>
	<ul class="level-1">
		<li><a href="index.php">Patients</a>
			<ul class="level-2">
				<li><a href="meet-our-doctors.php">Meet Our Doctors</a></li>
				<li><a href="pinkQuery.php">Our Services</a>
					<ul class="level-3">
						<li><a href="pinkQuery.php">Ask Your Physician</a></li>
						<li><a href="pinkQuerySpecialists.php">Ask your Specialist</a></li>
						<li><a href="bookAppointment.php">Book An Appointment</a></li>
						<li><a href="pinkOpinion.php">Get A Second Opinion</a></li>
					</ul>
				</li>
				<li><a href="happy-patients.php">Happy Patients</a></li>
				<li><a href="patients-faqs.php">FAQ's</a></li>
				<li><a href="register.php">Register</a></li>
			</ul>
		</li>
		<li><a href="home-doctors.php">Doctors</a>
			<ul>
				<li><a href="joinus.php">Join Us</a></li>
			</ul>
		</li>
		<li><a href="home-corporates.php">Corporates</a>
			<ul>
				<li><a href="corporate-joinus.php">Join Us</a></li>
			</ul>
		</li>
		<li><a href="<?php echo $site_url;?>pinkblog">Blog</a></li>
		<li><a href="aboutus.php">About Us</a></li>
		<li><a href="talktous.php">Contact Us</a></li>
		<li><a href="sitemap.php">Sitemap</a></li>
		<li><a href="privacy-policy.php">Privacy Policy</a></li>
		<li><a href="terms.php">Terms Of Service</a></li>
	</ul>
</div>

<?php include "includes/footer.php" ?>
<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>