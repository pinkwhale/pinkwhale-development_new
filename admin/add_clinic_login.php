<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" id="mainBg" valign="top">
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else {
            if($_SESSION['error']!=""){
                echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";
            }
            
    ?>
    <form action="actions/add_clinic_login.php" method="post" name="clinic_login" id="clinic_login">
     <table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform">
        <tr><th colspan="2">Create Clinic Login</th></tr>
               
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Clinic<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic" >
                    <option value="" selected="selected">-----select Clinic-----</option>
                    <?php
                        $qry = "select clinic_id,name from clinic_details where status=0 and name<>'' and username is NULL and password is NULL";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">UserName<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="text" name="uname" id="uname" size="25" maxlength="20" /></td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="unameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Password<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="password" name="pwd" id="pwd" size="20" maxlength="15" /></td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Retype-Password<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="password" name="rpwd" id="rpwd" size="20" maxlength="15" /></td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="re_passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Save" tabindex="6" type="button"  onclick="add_clinic_login(clinic_login)"  /></td>        
        </tr>
    </table>
    </form>
    <?php
        }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
	enable_clinic_submenu();
</script>
</html>
<?php
ob_end_flush();
?>
