<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/appointment.js"></script>

<script type="text/javascript">
    function get_doctor_week_detail(){
        
        var doctor = encodeURI(document.getElementById("doctor").value);
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#report').load('get-weekly-doctor-appointment-details.php?doc_id='+doctor+'&val=0&date=');
        $("#loading").fadeOut('slow');
    }
    
    function get_doctor_next_day(val,date){
        
        var doctor = encodeURI(document.getElementById("doctor").value);
        
        if(val==0){
            $('#next-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }else{
            $('#pre-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }
    
    
        $('#report').load('get-weekly-doctor-appointment-details.php?doc_id='+doctor+'&val='+val+'&date='+date);
        
    }
    
    
    function get_clinic_week_details(){
        
        var clinic = encodeURI(document.getElementById("clinic").value);
        
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#report').load('get-weekly-clinic-appointment-details.php?clinic_id='+clinic+'&val=0&date=');
        $("#loading").fadeOut('slow');
    }
    
    function get_clinic_next_day(val,date){
        
        var clinic = encodeURI(document.getElementById("clinic").value);
       
        if(val==0){
            $('#next-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }else{
            $('#pre-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }
    
    
        $('#report').load('get-weekly-clinic-appointment-details.php?clinic_id='+clinic+'&val='+val+'&date='+date);
        
    }
</script>

<style>
    
#loading { 
width: 70%; 
position: absolute;
}
</style>

</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php 
include "admin_left_menu.php"; 
include "../includes/pw_db_connect.php"; 
?>
</td>
<td width="772" valign="top" id="mainBg" >
    <br /><br />
            <div id="doctor_dash">
                <table border="0" cellpadding="0" cellspacing="1" align="center">
                    <tr>
                        <td>
                            <select name="doctor" id="doctor" onchange='get_doctor_week_detail()'>
                                <option value="" selected="selected" disabled>-----select doctor-----</option>
                                
                                <?php
                                    $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and blocked<>'Y'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and blocked<>'Y' group by doc_id";                        
                                    $res = mysql_query($qry);
                                    while ($data = mysql_fetch_array($res)){
                                        echo "<option value='".$data['doc_id']."|".$data['doc_name']."'>".$data['doc_name']."</option>";                            
                                    }                        
                                ?>
                            </select>
                        </td>
			<td>OR</td>
                        <td>
                            <select name="clinic" id="clinic" onchange='get_clinic_week_details()'>
                            <option value="" selected="selected" disabled>-----select Clinic-----</option>
                            
                            <?php
                                $qry = "select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id where status=0 group by name union select c.clinic_id,name from clinic_details c inner join clinic_doctors_details d on c.clinic_id=d.clinic_id where status=0 group by name";
                                $res = mysql_query($qry);
                                while ($data = mysql_fetch_array($res)){
                                    echo "<option value='".$data['clinic_id']."|".$data['name']."'>".$data['name']."</option>";                            
                                }                        
                            ?>
                            </select>
                        </td>
                    </tr>
                </table>
                
                <br />
                <div id="loading" align="center"></div>
                <div id="report"></div>
            </div>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
 	
	enable_appointment_submenu();
        
        
</script>
   
</html>
<?php
ob_end_flush();
?>
