<?php
include 'phpexcel/Classes/PHPExcel/IOFactory.php';
include ("../db_connect.php");
  	include("Sajax.php");
	sajax_init();
	sajax_export('getdata');
	sajax_export('getquery');
	sajax_handle_client_request();        
	session_start();
	include ("../db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<link href="../calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cards.js"></script>
<script type="text/javascript" src="js/la_disable.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<style type="text/css">
.upload {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f2f2f2), color-stop(1, #FFFFFF) );
	background:-moz-linear-gradient( center top, #f2f2f2 5%, #FFFFFF 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f2f2f2', endColorstr='#FFFFFF');
	background-color:#f2f2f2;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	
	font-family:arial;
	font-size:12px;
	font-weight:bold;
	font-style:normal;
	height:25px;
	line-height:25px;
	width:100px;
	text-decoration:none;
	text-align:center;
	
}
.upload:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #FFFFFF), color-stop(1, #f2f2f2) );
	background:-moz-linear-gradient( center top, #FFFFFF 5%, #f2f2f2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFF', endColorstr='#f2f2f2');
	background-color:#FFFFFF;
}.upload:active {
	position:relative;
	top:1px;
}</style>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
#dvLoading

{
   background:#000 url(images/loader.gif) no-repeat center center;
   height: 100px;
   width: 100px;
   position: fixed;
   z-index: 1000;
   left: 50%;
   top: 50%;
   margin: -25px 0 0 -25px;
}

</style>

<script>function up_invoices($items)
{document.getElementById("dvLoading").style.display="block";
	$('#dvLoading').fadeOut(3000);
	var ids = new Array();
	var plans = new Array();
	var yns = new Array();
	var addons = new Array();
var i;
//alert( $items);
  for(i=1;i<=$items;i++){
	  
	  var quote_str =  "tick"+i;
	  var plan =  "pack"+i;
	  var yn =  "yn"+i;
	  var addon =  "addon"+i;
	  
	  ids[i-1]=document.getElementById(quote_str).value;
	  
	  plans[i-1]=document.getElementById(plan).value;
	  
	  yns[i-1]=document.getElementById(yn).value;
	  addons[i-1]=document.getElementById(addon).value;
	  
	  
	  
	  }
	  


	  $.ajax({  
                        type: "POST",  
                        url: "test.php",  
                        data: { ids : ids,plans :plans,yns:yns, addons:addons},  
                        success: function(response) {                                                      
                                if(response==true)
								{
                                alert("Mail sent");
								
								}else{
									alert("Error occoured while sending mail");}
                        }

                });
	  
	}
</script>
</head>
<body>
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?>

</td>
<td width="900" >

<?php
include ("../db_connect.php");
//Сheck that we have a file
if((!empty($_FILES["uploaded_file"])) && ($_FILES['uploaded_file']['error'] == 0)) {
	//Check if the file is JPEG image and it's size is less than 350Kb
	
	$inputFileName = pathinfo($_FILES['uploaded_file']['name']);
	$ext = $inputFileName['extension']; // get the extension of the file
	
	//if valid files
	if ($ext == "xls" || $ext == "xlsx") {
	
		$array = null;
		$errors = null;
		$objPHPExcel = null;
		$objReader = null;
		$date = date_create();
		$newFileName = date_timestamp_get($date).'.'.$ext;
		$destination = 'excel/'.$newFileName;
	
		//copy the file to destination folder
		if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$destination))) {
			try {
				$inputFileType = PHPExcel_IOFactory::identify($destination);
				//echo "TYPE".$inputFileType;
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($destination); // Load $inputFileName to a PHPExcel Object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}
		}
		//fetch the values in array
		$array=$objPHPExcel->getActiveSheet()->toArray();
		$errors = parseExcel($array);
		$ww="dd";
		if (sizeof($errors)>0){?>
			<div id="tot_doc_page" style="border: 1px solid; margin-left: 15px; margin-top: -260px; padding: 50px;">
								<font color="red" font-weight: "900";> <?php echo "File cant be uploaded due to following errors...";?>
								</font><br>
							
			<?php 
			foreach ($errors as $error){?>
				<font color="red"> <?php echo $error."<br/>";?>
			</font>
			<?php }
			?>
<div style="text-align:centre; margin-left:150px;">
			<form action="add-new-doctor.php">
		<button type="submit" class="upload" >Back</button>
	</form></div>
	 <?php 		
       }else
        {
        	add_doctor($array);
		}
	}
else {
echo "only excel files can be uploaded..";?>
				<form action="add-new-doctor.php">
					<button type="submit" class="Manually">Back</button>
				</form> <?php 

}
}

function parseExcel($array) {
	$errors = array();
	//$doctorDetails=array();
	for ($i=2; $i<sizeof($array); $i++){
		
		$doctorDetails = $array[$i];
		
			for ($docAttr=0; $docAttr<sizeof($doctorDetails); $docAttr++){
				switch ($docAttr){
					case 3:
						if (!isEmailValid($doctorDetails[3])){
							array_push($errors," Email address of ".$doctorDetails[1]." ".$doctorDetails[3]. "is invalid");
						}
						break;
					case 4:
						if (!isMobileValid($doctorDetails[4])){
							array_push($errors," Mobile number of ".$doctorDetails[1]." ".$doctorDetails[4]." is invalid");
						}
						break;
					case 15:
						if (!isMobileValid($doctorDetails[15])){
							array_push($errors,"Teleconsultation number of ".$doctorDetails[1]." ".$doctorDetails[15]."  is invalid");
						}
						break;
			    }
			 }
		 }
		return $errors;
	}

	function isMobileValid($mobile){
		if (!is_numeric ($mobile)){
			return false;
		}
		return true;
	}

	function isEmailValid($email){
		$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
		if (!preg_match($regex, $email)) {
			return false;
		}
		else
		{
			$qry7=mysql_query("SELECT * FROM `pw_doctors` WHERE `doc_email_id`='$email'");
			if(!$qry7){
				return false;
			}
		
			$num_rows = mysql_num_rows($qry7);
			if($num_rows>=1)
				return false;
		}
		return true;
	}

		function array_search2d($needle, $haystack) {
			for ($i = 0, $l = count($haystack); $i < $l; ++$i) {
				if (in_array($needle, $haystack[$i])) return $i;
			}
			return false;
		}
	function add_doctor($arrays)
	{
		$flag=0;
		$row = 0;
		$items = array();
		$port_arr=array();
		$port_f=0;
		foreach ($arrays as $doctorDetails){
        if($row>=2){
            $id_count=0;
			$port_arr[$port_f]=$doctorDetails[17];
			$port_arr1[$port_f]=$doctorDetails[19];
			$port_f++;
			do{
			
				$card_number = rand(1000,9999);
			
				$qry="SELECT `doc_id` FROM `pw_doctors` WHERE `doc_id`='$card_number'";
			
				if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
			
				if(mysql_fetch_array($qry_rslt)<1){
					$id_count++;
				}
			}
			
			while ($id_count<=0);
			 
			$ref_no=$card_number;
			$items[] = $ref_no;
			$qry=mysql_query("insert into pw_doctors(doc_id,doc_name,doc_gender,doc_email_id,doc_mobile_no,addressline_1,address_line2,city,doc_qualification,doc_category,doc_dept,doc_specialities,doc_timing,face_to_face_cnsltion_phno,tele_consult_phno,entered_date) values ('$ref_no','$doctorDetails[1]','$doctorDetails[2]','$doctorDetails[3]','$doctorDetails[4]','$doctorDetails[5]','$doctorDetails[6]','$doctorDetails[7]','$doctorDetails[8]','$doctorDetails[9]','$doctorDetails[10]','$doctorDetails[11]','$doctorDetails[14]','$doctorDetails[15]','$doctorDetails[16]',now())");
	        
	        if(!$qry){
	        	$flag=1;
	        }
	        
	       $package=$doctorDetails[17];
	       if($package=="pW-Tele")
	       {
	       	$qry1=mysql_query("insert into packages(doctor_id,type_of_doctor,3_min_charge,15_min_charge,30_min_charge) values ('$ref_no','$doctorDetails[9]','$doctorDetails[20]','$doctorDetails[21]','$doctorDetails[22]')");
	       	if(!$qry1){
	       		$flag=1;
	       	} 
	       
	       }
	       elseif($package=="pW-online")
	       {
	       	$qry2=mysql_query("insert into packages(doctor_id,type_of_doctor,1_email_session_charge,5_email_session_charge,10_email_session_charge) values ('$ref_no','$doctorDetails[9]','$doctorDetails[23]','$doctorDetails[24]','$doctorDetails[25]')");
	       	if(!$qry1){
	       		$flag=1;
	       	}
	       	
	       }
	       
	       else 
	       {
	       	$qry3=mysql_query("insert into packages(doctor_id,type_of_doctor,3_min_charge,15_min_charge,30_min_charge,1_email_session_charge,5_email_session_charge,10_email_session_charge) values ('$ref_no','$doctorDetails[9]','$doctorDetails[20]','$doctorDetails[21]','$doctorDetails[22]','$doctorDetails[23]','$doctorDetails[24]','$doctorDetails[25]')");
	       if(!$qry1){
	       $flag=1;
	       }
	       
	       }
	        
	      }else{
	     	$row++;
	     }
	    }
	 
if($flag==0)
{
    ?>
    <table width='700' border='0' cellpadding='4' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform' style="margin-top: -260px;">
		<!--  <tr><th colspan="7">Added Doctors</span></th></tr>-->
			<tr  bgcolor="#a6a6a6">
			<td align='left'></td>
			<td align='left'><strong>Sl.No</strong></td>
                <td align='left'><strong>Name</strong></td>
                <td align='left'><strong>Email id</strong></td>
                <td align='left'><strong>Mobile Number</strong></td>
                <td align='left'><strong>Type</strong></td>
                <td align='left'><strong>Department</strong></td>
                <td align='left'><strong>Speciality</strong></td>
                <td align='left'></td>
            </tr>
	
   
  <?php 
//  $items =array(8277,2135);
 // $arrays=array(5655,552);
   $matches = implode(',', $items);
   $qry4="SELECT * FROM pw_doctors WHERE doc_id IN ( $matches )";
   if(!$qry_rslt=mysql_query($qry4)) die(mysql_error());
   $cl_flag=1;
   $sl_no=1;
   while($qry_result=mysql_fetch_array($qry_rslt))
   {
   	$doc_id=$qry_result['doc_id'];
   $doctor_name=$qry_result['doc_name'];
   $email_id=$qry_result['doc_email_id'];
   $mobile=$qry_result['doc_mobile_no'];
   $type=$qry_result['doc_category'];
   $dept=$qry_result['doc_dept'];
   $speciality=$qry_result['doc_specialities'];
if($cl_flag==1){
	echo "<tr bgcolor='#f4f4f4'>";
	$cl_flag=0;
}else{
	echo "<tr bgcolor='#ececec'>";
	$cl_flag=1;
}
echo "<td><input type=\"checkbox\" value=\"$doc_id\" id=\"tick$sl_no\">";
echo "<td>$sl_no </td> ";
   echo "<td>$doctor_name </td> ";
   echo "<td>$email_id </td> ";
   echo "<td>$mobile </td> ";
   echo "<td>$type </td> ";
   echo "<td>$dept </td> ";
   echo "<td>$speciality </td> ";
$pos = array_search2d($email_id, $arrays);
   echo "<td style=\"display:none;\"><input type=\"text\" value=\"".$arrays[$pos][17]."\" id=\"pack$sl_no\" /></td>";
   echo "<td style=\"display:none;\"><input type=\"text\" value=\"".$arrays[$pos][18]."\" id=\"yn$sl_no\" /></td>";
   echo "<td style=\"display:none;\"><input type=\"text\" value=\"".$arrays[$pos][19]."\" id=\"addon$sl_no\"/></td>";
   echo "<td><a class=\"upload\">Upload Photo</a>";
   $sl_no++;

   }?> 
   <?php 
   $port_arr_js=json_encode($port_arr);
   $port_arr1_js=json_encode($port_arr1);
   ?>
   </table><div id="dvLoading" style="display:none"></div>
   <div style="text-align:centre; margin-left:150px;">
<a onclick="up_invoices(<?php echo $sl_no-1; ?>)" class="upload" style="width:150px; height:65px; line-height:30px; float:left; font-size:15px;" >Send invoices To Doctors</a>
<a class="upload" style="width:150px; height:65px;line-height:30px; font-size:15px;margin-left: 125px;">Send Templates To Printing Press</a>
</div>
</td>
</tr>
<?php }
else
{echo "error in inserting values";}
	}
?>

		
	
<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_cards_submenu();
</script>
</body></html>