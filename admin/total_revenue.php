<!-- new page -->
<?php 
ob_start();
error_reporting(E_PARSE);
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
<link href="../css/designstyles.css" media="screen, projection"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<link href="calendar/calendar.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="screen"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
	
<style type="text/css">
.uneditable-input {
	margin-left: 28px;
	margin-top: 4px;
}

.ui-datepicker-calendar {
	display: none;
}
.ui-widget-header{
background:#E10971;
}
.ui-state-default, .ui-widget-content .ui-state-default {
background:#E10971;
}
.ui-priority-secondary, .ui-priority-primary, .ui-widget-content .ui-priority-secondary  {
    color: #FFFFFF;
    }
.ui-priority-primary, .ui-widget-content .ui-priority-primary {
    color: #FFFFFF;
</style>

			</head>

			<body>
	
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td width="228" valign="top"
			style="border-right: 1px solid #4d4d4d; border-left: 1px solid #4d4d4d;">

			<?php include "admin_left_menu.php"; ?>
		</td>
		<td width="772" id="mainBg" valign="top"><?php
		require_once('calendar/classes/tc_calendar.php');
		?> <script language="javascript" src="calendar/calendar.js"></script>
			<script type="text/javascript">     
        function target_popup(form) {
        	  
     	
     	var str1 = $("#start_date").val();
     	var res1 = str1.split("/"); 
        var str2 = $("#end_date").val();
     	var res2 = str2.split("/"); 
     	if(str1=='' ||str2=='')
 		{alert("Please Select Months"); return false;}  
         if(res1[1]>res2[1])	
           {
            alert("start date is greater than end date");
            return false;
           }	
         else if((res1[1]==res2[1]) && (res1[0]>res2[0]))	
           {
                alert("start date is greater than end date");
                return false;
           }	
         else
          {
   	       window.open('', 'formpopup', 'width=900,height=600,resizeable,scrollbars');
	       form.target = 'formpopup';
	       return true;
	      }
       }
        	
        </script> <script
				src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
			<script type="text/javascript"
				src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>

			<script type="text/javascript">

$(function() {
	$('#start_date').datepicker( {
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		dateFormat: 'mm/yy',
		maxDate:'m', // restrict to show month greater than current month
		onClose: function(dateText, inst) {
			// set the date accordingly
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			//alert(month);
			//alert(year);
			$(this).datepicker('setDate', new Date(year, month, 1));
		},
		beforeShow : function(input, inst) {
			if ((datestr = $(this).val()).length > 0) {
				year = datestr.substring(datestr.length-4, datestr.length);
				month = jQuery.inArray(datestr.substring(0, datestr.length-5), $(this).datepicker('option', 'monthNames'));
				$(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
				$(this).datepicker('setDate', new Date(year, month, 1));
			}
		}
	});
});


$(function() {
	$('#end_date').datepicker( {
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		dateFormat: 'mm/yy',
		maxDate:'m', // restrict to show month greater than current month
		onClose: function(dateText, inst) {

			
			// set the date accordingly
			var month1 = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year1 = $("#ui-datepicker-div .ui-datepicker-year :selected").val();

			$("#end_date").datepicker('setDate', new Date(year1, month1, 1));

		},
		beforeShow : function(input, inst) {
			if ((datestr1 = $(this).val()).length > 0) {
				year1 = datestr1.substring(datestr1.length-4, datestr1.length);
				month1 = jQuery.inArray(datestr1.substring(0, datestr1.length-5), 
						$(this).datepicker('option', 'monthNames'));
				$(this).datepicker('option', 'defaultDate', new Date(year1, month1, 1));
				$(this).datepicker('setDate', new Date(year1, month1, 1));
			}
		},
	});
});

</script>
					<form id="form1" name="form1" method="get" action="generate_report_by_revenue.php" onsubmit="JavaScript:return target_popup(this);">
					<table width="435" border="0" cellspacing="0" cellpadding="0" align="center"
						style="margin-left: 191px; margin-top: 100px; border: 1px solid #cccccc;">
						<tr>
							<td colspan="3" align="center">
							<label style="font-family: arial; margin-top: 6px; font-size: 20px;">Total Revenue By Month</label>
							</td>
						</tr>

						<tr height="20px"></tr>
						<tr>
							<td align="center" colspan="3"><label style="font-family: arial; margin-top: 6px; font-size: 15px;">Between</label>
							</td>
						</tr>
						<tr height="20px"></tr>
						<tr>
							<td align="right" style="float: right;"><input name="start_date"
								id="start_date" class="date-picker" style="width: 50px;" />
								(mm/yyyy)</td>

							<td align="center"><label
								style="font-family: arial; margin-top: 6px;">And</label></td>
							
							<td align="left" style="float: left;"><input name="end_date"
								id="end_date" class="date-picker" style="width: 50px;" />
								(mm/yyyy)</td>
						</tr>



						<tr>
							<td colspan="3" align="center"><input type="submit" name="Submit"
								value="Generate" style="margin-top: 25px; margin-bottom: 20px;" />
							</td>
						</tr>
					</table>
				</form>

</table>
<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_cards_submenu();
</script>


</body>
</html>
