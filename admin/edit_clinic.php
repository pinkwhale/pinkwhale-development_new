<?php 
ob_start();
error_reporting(E_PARSE);
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}

if(!isset($_GET['id']))
{
	header("Location: clinic.php");
	exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>

</head>

<body>
	<link href="../css/designstyles.css" media="screen, projection"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
	<?php include "admin_head.php"; ?>
	<!-- side Menu -->
	<table width="1000" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="228" valign="top"
				style="border-right: 1px solid #4d4d4d; border-left: 1px solid #4d4d4d;">

				<?php include "admin_left_menu.php"; ?>
			</td>
			<td width="772"><?php
			if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else {
            if($_SESSION['error']!=""){
                echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";
            }

            $id = $_GET['id'];
            $qry = "select * from clinic_details where clinic_id=$id";
            $result = mysql_query($qry);
            $data = mysql_fetch_array($result);
             
            ?> <script type="text/javascript">
function chechclinicEmailid ()
{

        if(document.getElementById("email").value!=""){
            var paramString = 'email=' + document.getElementById("email").value;  


		$.ajax({  
			type: "POST",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) {  
				if(response == 'success')
				{
					document.getElementById('emailErrDiv').innerHTML ="";
				}
				else
				{
					document.getElementById('emailErrDiv').innerHTML ="Email address already exists";
				}	
			}
			  
		});  
                
        }

}
</script>
				<form action="actions/edit_clinic_action.php" method="post"
					name="add_clinic" id="add_clinic" enctype="multipart/form-data">
					<table border="0" cellpadding="0" cellspacing="1" width="500"
						align="center" class="s90registerform">
						<tr>
							<td colspan="2" align="left"><img height="100" width="100"
								src="../<?= $data['logo']; ?>" /></td>
						</tr>
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"><input type="hidden" name="card_no"
								value="<?= $data['clinic_id']; ?>" id="card_no" size="25"
								maxlength="30" /></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Name<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="cname" value="<?= $data['name']; ?>" id="cname" size="25"
								maxlength="30" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="clinicnameErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Address<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><textarea rows="3"
									cols="35" name="address" id="address"
									value="<?= $data['address']; ?>" maxlength="200">
									<?= $data['address']; ?>
								</textarea></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="addressErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Country<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><select
								name="country" id="country">
									<option value="">-----select Country-----</option>
									<option value="India" selected="selected">India</option>
							</select>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="countryRrrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<?php 

						$state_name = array("Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh","Chhatisgarh","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura","Uttaranchal","Uttar Pradesh","West Bengal");

						?>

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">State<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><select
								name="state" id="state">
									<option value="" selected="selected">-----select State-----</option>
									<?php
									$name=$data['state'];
									$count = count($state_name);
									for($k=0 ; $k <$count ;$k++){
                                if($state_name[$k]==$name){
                                    echo"<option value=".$state_name[$k]." selected>".$state_name[$k]."</option>";
                                }else{
                                    echo"<option value=".$state_name[$k].">".$state_name[$k]."</option>";
                                }
                            }
                            ?>
							</select>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="stateErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">City<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="city" id="city" value="<?= $data['city']; ?>"
								maxlength="30" />
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="cityErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Pin/Zip<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['pin_zip']; ?>" name="pin" id="pin" size="10"
								maxlength="8" onkeypress="return isNumberKey(event);"
								onkeyup="checkphoneno(this)" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="pinErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Phone No.<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['phone_no']; ?>" name="phone" id="phone"
								size="14" maxlength="12" onkeypress="return isNumberKey(event);"
								onkeyup="checkphoneno(this)" /></td>
						</tr>

						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="phoneErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Email ID.<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['email']; ?>" onchange="chechclinicEmailid()"
								name="email" id="email" size="40" maxlength="70" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="emailErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Latitude :</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['latitude']; ?>" name="late" id="late"
								size="10" maxlength="12" onkeypress="checkphoneno1(this)"
								onkeyup="checkname(this)" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="lateErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Longitude :</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['longitude']; ?>" name="longa" id="longa"
								size="10" maxlength="12" onkeypress="checkphoneno1(this)"
								onkeyup="checkname(this)" /></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="longaErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">No. of Doctors<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								value="<?= $data['num_of_doc']; ?>" name="num_doc" id="num_doc"
								size="3" maxlength="3" value="0"
								onkeypress="return isNumberKey(event);" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="num_of_doc_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Logo :</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="file"
								name="logo" id="logo" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="logo_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Type:</td>

							<td width="10%" align="left" bgcolor="#F5F5F5">Top Up</td>
							
							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="top_up" name="top_up" value="1" <?php if ($data['type'] == '0' || $data['type'] == '1' || $data['type'] == '4') echo "checked='checked'" ?>"/></td>

						</tr>

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							
							<td align="left" bgcolor="#F5F5F5"><div>Appointment Booking</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="appointment_booking" name="appointment_booking" <?php if ($data['type'] == '0' || $data['type'] == '4' || $data['type'] == '2' || $data['type'] == '5') echo "checked='checked'" ?> value="2" />
							</td>
						</tr>

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							<td align="left" bgcolor="#F5F5F5"><div>Diagnostic Consult</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="diagnostic_consultation" name="diagnostic_consultation" <?php if ($data['type'] == '0' || $data['type'] == '3' || $data['type'] == '5' || $data['type'] == '6') echo "checked='checked'" ?> 
								value="3" /></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							<td align="left" bgcolor="#F5F5F5"><div>All</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox" onchange="enable_all(all)"
								id="all" name="all"  <?php if ($data['type'] == '0' ) echo "checked='checked'" ?>  onchange="enable_all(type)" value="0" /></td>

						</tr>
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="clinic_type_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>



			
						<input type="hidden" name="clinic_id" value="<?php echo $id; ?>"
							id="cname" size="25" maxlength="30" />
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input
								onmouseover="this.style.cursor='pointer'" value="Save"
								tabindex="6" type="button"
								onclick="add_clinic_submit(add_clinic)" /></td>
						</tr>
					</table>
				</form> <?php
        }
        ?>
			</td>
		</tr>
	</table>
	<?php include 'admin_footer.php'; ?>
</body>

</html>
<?php
ob_end_flush();
?>
