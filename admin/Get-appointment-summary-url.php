<?php 
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    include "../includes/site_config.php";
	include "../actions/encdec.php";
	include "../modify_doctorname.php";
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
	
?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>pinkwhalehealthcare</title>
    <meta name="description" content="pinkwhalehealthcare">
    <link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
    <link href="css/my_css.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/add-doctor-validation.js"></script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>

<script>
</script>
</head>

<body >

<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
    <td width="772" valign="top" id="mainBg" align="center">
        <table border="0" cellpadding="0" cellspacing="1" style="width:100%;margin-left:10px;" align="left" class="s90registerform">
        <tr><th colspan="3">Appointment Summary URL</th></tr>
        <tr>
			<td><b>Doctor Name</b></td>
			<td><b>Url</b></td>
			<td><b>Action</b></td>
		</tr>
		<?php
                        $qry = "select doc_id,doc_name,doc_specialities,city from pw_doctors where appoint_flag=1 and blocked<>'Y'";
                        $result = mysql_query($qry);
                        while ($hold_data = mysql_fetch_array($result)){
						$doctor_id = $hold_data['doc_id'];
						$doctor_city = encode_city($hold_data['city']);
						$doctor_name_string = modifyname($hold_data['doc_name']);
						$doctor_spec_string = encode_city($hold_data['doc_specialities']);
						
		?>
		<tr>
		<td><?php echo $hold_data['doc_name']?></td>
		<td><a style="text-decoration:none !important;" href='<?php echo $sitePrefix."/doctors/appointments/".$doctor_spec_string."/".$doctor_city."/".$doctor_name_string.""?>'  target="_blank"> <?php echo $sitePrefix."/doctors/appointments/".$doctor_spec_string."/".$doctor_city."/".$doctor_name_string.""?></a></td>
		<td style="padding-right: 3px;">
		   <?php $parameters = $doctor_id."|".$doctor_name_string."|".$doctor_spec_string."|".$doctor_city."|".$hold_data['doc_name'];?>	
			<input value="Send Mail" tabindex="6" type="button" onclick="sendMail('<?php echo $parameters?>')"/>
		</td>
		</tr>
		<?php }?>	
		</table>
		<br />
		<center>
		<form action="actions/send_mail_url.php" name="sendMailForm" method="POST" style="display:none;">
				<input name="docid" id="docid" type="hidden"/>
				<input value="Send Mail" tabindex="6" type="submit" />
		</form>
		</center>
    </td>  		
</tr>
</table>

<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
function sendMail(val){
	document.getElementById("docid").value=val;
	document.sendMailForm.submit();
}	
</script>
</body></html>
