<?php
ob_start();
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
        header("Location: ../index.php");
        exit();
}

include ("../db_connect.php");

$clinic=urldecode($_GET['clinic_id']);

$clinic_details = explode("|", $clinic);
$clinic_id = $clinic_details[0];
$clinic_name = $clinic_details[1];

//$qry = "select p.doc_name,p.doc_id,p.blocked from pw_doctors p inner join clinic_doctors_details c on p.doc_id=c.doctor_id and clinic_id=$clinic_id where appoint_flag=1";
$qry = "select p.doc_name,p.doc_id,p.blocked from pw_doctors p inner join clinic_doctors_details c on p.doc_id=c.doctor_id and clinic_id=$clinic_id ";
$res = mysql_query($qry);
$num = mysql_num_rows($res);
if($num>0){
    $disp = "<table width='300' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'><tr><th colspan='2' align='center'><b>Doctor Details for clinic &nbsp;:&nbsp;$clinic_name</b></th></tr>";
    $disp .= "<tr><td align='center'><b>Doctor Name</td><td align='center'><b>Doctor ID</td></tr>";
    while($dat =  mysql_fetch_array($res)){
        if($dat['blocked']!='Y'){
            $disp .= "<tr><td align=\"center\" bgcolor=\"#F5F5F5\">".$dat['doc_name']."</td>";
            $disp .= "<td align=\"center\" bgcolor=\"#F5F5F5\">".$dat['doc_id']."</td></tr>";
        }else{
            $disp .= "<tr><td align=\"center\" bgcolor=\"#F5F5F5\"><font color='red'>".$dat['doc_name']."</font></td>";
            $disp .= "<td align=\"center\" bgcolor=\"#F5F5F5\"><font color='red'>".$dat['doc_id']."</font></td></tr>";
        }
    }
    $disp .= "</table>";
}

echo $disp;

ob_end_flush();
?>

