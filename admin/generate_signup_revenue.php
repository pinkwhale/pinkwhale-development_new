<?php

session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
$login= $_SESSION['login'];

$f_date1 = $_GET['start_date'];
$t_date1 = $_GET['end_date'];

list($f_part1, $f_part2) = explode('/', $f_date1);
list($t_part1, $t_part2) = explode('/', $t_date1);

$f_date=$f_part2."-".$f_part1."-"."01";

$lastday = date('t', strtotime($t_part2.'-'.$t_part1.'-01'));
$t_date=$t_part2."-".$t_part1."-".$lastday;

include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
require('pdf/fpdf.php');
$d=date('d_m_Y');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="css1/main.css" rel="stylesheet" type="text/css" />

<!-- add scripts -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="js1/highcharts.js"></script>
<script src="js1/gray.js"></script>

</head>
<body style="background: #ffffff;">
	<table style="width: 100%;">
		<tr>
			<td align="left" style="width: 50%;">
				<table style="width: 100%;">
					<tr>
						<td style="font-family: arial;">pinkWhale Healthcare Services Pvt.
							Ltd.</td>

					</tr>
					<tr>
						<td style="font-family: arial;">No. 275, 16th Cross 2nd Block,
							R.T. Nagar, Bangalore 560032</td>
					
					
					<tr>
						<td style="font-family: arial;">Phone: +91 - (080) 2333 1278.</td>
					</tr>
					<tr>
						<td style="font-family: arial;">Email:
							marketing@pinkwhalehealthcare.com</td>
					</tr>
				</table>
			</td>
			<td align="right" style="width: 50%;">
				<table style="width: 100%;" style="margin-top: 0px;">
					<tr>
						<td align="right"><img src="logo.jpg" width="380" height="125"
							title="Logo of a company" alt="Logo of a company" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<hr width="100%" />

	<div align="center" id="ab" style="display: none;">
		<label
			style="color: #800080; font-size: 21px; margin-top: 10px; font-family: arial;">Graphical
			Representation Of Number Of Doctors Signed Up By Month</label>
		<!-- Chart type switchers -->
		<!-- <div class="actions">
            <button class="switcher" id="column">column</button>
            <button class="switcher" id="area">area</button>
            <button class="switcher" id="line">line</button>
            <button class="switcher" id="spline">Spine</button>
            <button class="switcher" id="areaspline">areaspline</button>
        </div> -->

		<!-- two different charts -->
		<div id="chart_1" class="chart"></div>
	</div>


	<table style="width: 100%;">
		<tr>
			<td align="center"><label
				style="font-family: arial; font-weight: bolder;">Doctor SignUps By
					Revenue</label>
			</td>
		</tr>
				<tr height="12px;">
		</tr>
		<tr>
			<td align="center"><label style="font-family: arial;">Between</label>
			</td>
		</tr>
				<tr height="12px;">
		</tr>
		<tr>
			<td align="center"><label style="font-family: arial; color: #0000FF;"><?php echo $f_date?>
			</label> <label style="padding: 11px; font-family: arial;">And</label>
				<label style="padding: 11px; font-family: arial; color: #0000FF;"><?php echo $t_date?>
			</label>
			</td>
		</tr>
	</table>

	<?php 
	function getNextMonth($date)//date format 'Y-m-d'
	{
		$date_tmp = explode("-",$date);
		$next_date =mktime(0, 0, 0, $date_tmp[1]+1, $date_tmp[2], $date_tmp[0]);
		return date('Y-m-d',$next_date);
	}


	$months=Array();
	$year=Array();
	$i=1;
	$start = date($f_date);
	$end = date($t_date);
	$dateArray = Array();
	$dateArray[$i] = $start;
	$i++;
	while($start <= $end)
	{
		$curDate  = getNextMonth($start);
		$dateArray[$i] = $curDate;
		$start = $curDate;
		$i++;
	}
	?>

	<table style="width: 100%">
		<tr>
			<td align="center">
				<table style="width: 90%" border='0' cellpadding='0' cellspacing='1'
					bgcolor='#eeeeee' align='center' class='s90registerform'>


					<tr>
						<td align='center'
							style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>SL
								No</strong></td>
						<td align='center'
							style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>Month</strong>
						</td>

						<td align='center'
							style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>Total
								Revenue</strong></td>
						<td align='center'
							style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>SignUp
								Revenue</strong></td>
						<td align='center'
							style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>AddOn
								Revenue</strong></td>

					</tr>

					<?php
					$rowArray1=Array();
					$rowArray2=Array();
					$rowArray3=Array();
					for($j=1;$j<=sizeOf($dateArray)-1;$j++)
					{?>
					<tr bgcolor='#ffffff'>
						<?php
		
						$queryDate = $dateArray[$j];
						$month=date("F", strtotime($queryDate));
						$year1=date("Y",strtotime($queryDate));
							
						//echo $year1."  ".$month."<br/>";
						$revenue_signUp=mysql_query("SELECT SUM(sp_price) as sp,SUM(add_price) ap FROM  pw_reports  WHERE  MONTHNAME(date)='$month' AND YEAR(date)='$year1' GROUP BY MONTH(date)");
						$row1=(mysql_fetch_array($revenue_signUp));
						if(mysql_num_rows($revenue_signUp) == 0)
						{
							$row1['sp']=0;
							$row1['ap']=0;
						}
						$total=$row1['sp']+$row1['ap'];
						$rowArray1[$j]=$total;
						$rowArray2[$j]=$row1['sp'];
						$rowArray3[$j]=$row1['ap']
						?>
						<td align='center' style="font-family: arial;"><?php echo $j;?></td>
						<td align='center' style="font-family: arial;"><?php echo date("F, Y", strtotime($dateArray[$j]));?>
						</td>
						<td align='center' style="font-family: arial;"><?php echo $total."/-";?></td>
						<td align='center' style="font-family: arial;"><?php echo $row1['sp']."/-";?></td>
						<td align='center' style="font-family: arial;"><?php echo $row1['ap']."/-";?></td>
					</tr>

					<?php }
	if (!is_dir('../images/Reports'))
						// is_dir - tells whether the filename is a directory
					{
						//mkdir - tells that need to create a directory
						mkdir('../images/Reports');
					}
					?>


				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table align="center" style="width: 30%;">
					<?php 	$d=date('Y-m-d  H-i-s');?>
					<?php $p="Dr.Sign-Up-Revenue-";?>
					<tr>
						<td><a href="<?php echo "../images/Reports/".$p."$d.pdf";?>"><input type="button"
								name="download" value="Download Report"> </a></td>
						<td><input type="button" value="View Report" name="view" id="view">
						</td>
						<td><input type="button" value="Send Report" name="send"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<script>
	// Change Chart type function
	function ChangeChartType(chart, series, newType) {
		newType = newType.toLowerCase();
		for (var i = 0; i < series.length; i++) {
			var srs = series[0];
			try {
				srs.chart.addSeries({
					type: newType,
					stack: srs.stack,
					yaxis: srs.yaxis,
					name: srs.name,
					color: srs.color,
					data: srs.options.data
				},
				false);
					series[0].remove();
			} catch (e) {
			}
		}
	}
	
	// Two charts definition
	var chart1;
	
	// Once DOM (document) is finished loading
	$("#view").click(function() {
	//alert("f");
		$("#ab").show("slow");
		//alert("g");
		// First chart initialization
		chart1 = new Highcharts.Chart({
			chart: {
				renderTo: 'chart_1',
				type: 'column',
				height: 350,
			},
			title: {
				text: 'Doctors Signed Up By Revenue'
			},
			xAxis: {
				title: {
					text: 'Months'
				},
				
				categories: [<?php for($j=1;$j<=sizeof($dateArray);$j++) { echo "'".date("F, Y", strtotime($dateArray[$j]))."',";}?>] 
			},
			yAxis: {
				title: {
					text: 'Revenue'
				}
			},
			series: [{
				
				name: 'Revenue',
				data: [<?php for($j=1;$j<=sizeof($rowArray1);$j++){ echo $rowArray1[$j].",";}?>]
			}]
		});
	
	});
	</script>
	
	<?php 
	class PDF extends FPDF
	{

		function Header()
		{
			//Logo
			$this->SetFont('Arial','B',7);
			$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
			$this->Image('pdf/logo.jpg',160,6,40);
			$this->Ln(3);$this->SetFont('Arial','B',7);
			$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
			$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
			$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);

			$name="Export PDF";
			$this->SetFont('Arial','B',15);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->SetFont('Arial','B',9);
			//Line break
			$this->Line(0, 25, 300, 25);
			$this->Ln(4);
			//$this->SetFont('Arial','BU',12);
			$this->Cell(65);
			$this->SetFont('Arial','B',12);
			$this->Cell(90,10,"Doctor SignUps By Revenue",'C');
			$this->Ln(10);
			$this->SetFont('Arial','BU',9);
		}
		function doc_details($f_date,$t_date){
			$this->Cell(85);
			$this->SetFont('Arial','B',11);
			//$this->SetFont('Arial','B',12);
			$this->Cell(20,10,"Between");
			$this->Ln(10);
			$this->Cell(64);
			$this->SetTextColor(0,1,225);
			$this->Cell(25,10,$f_date);
			$this->SetTextColor(0,0,0);
			$this->Cell(15,10," And ");
			$this->SetTextColor(0,1,225);
			$this->Cell(20,10,$t_date);
			$this->Ln(15);
		}
		//Page footer
		function Footer()
		{

		}
		function reference_table($c_name,$c_address)
		{
			$this->Ln(10);
			$invoice_date=date("Y-m-d");
			$x=$this->GetX();
			$y=$this->GetY();
			$this->SetFont('Arial','B',7);
			$rand=rand(10000, 99999);
			$this->MultiCell(60, 4, "Invoice Number:\nINV$rand pW",1);
			$this->SetXY($x + 60, $y);
			$this->MultiCell(60, 4, "Invoice Date:\n$invoice_date",1);
			$this->SetXY($x + 120, $y);

			$this->MultiCell(60, 4, "Reference Number:\n49415",1);
			$this->MultiCell(180, 3, "Buyer:$c_name\n
	Address:\n$c_address\n",1);

	$this->Ln(10);
		}
		//Simple table
		function BasicTable($header,$f_date,$t_date,$dateArray,$rowArray1,$rowArray2,$rowArray3,$login)
		{

			$this->SetFillColor(242,39,201);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');

			$w=array(20,40,35,30,35);
			//Header
			$this->Cell(15);
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
			$this->Ln();
			//Data
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('');

			$count=1;
			for($k=1;$k<=sizeof($rowArray1);$k++)
			{
				$this->Cell(15);
			$this->Cell(20,6,$count,1);
			$this->Cell(40,6,date("F, Y", strtotime($dateArray[$k])),1);
			$this->Cell(35,6,$rowArray1[$k]."/-",1,'C',true);
			$this->Cell(30,6,$rowArray2[$k]."/-",1,'C',true);
			$this->Cell(35,6,$rowArray3[$k]."/-",1,'C',true);
			
			//$this->Cell(35,6,ucwords($eachResult["user_name"]),1);
			$this->Ln();
			$count++;
			}
				
			
			
			$this->SetFillColor(131,177,238);

			$this->Ln(8);
			$this->SetFont('Arial','BU',9);
			$this->Cell(80);
			$this->SetFillColor(242,39,201);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');

			$w=array(20,45);
			//Header
			$this->Cell(55);
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('');
			$date=date("d/m/Y");
			$this->Ln(10);
			$this->Cell(30,10,"Report Created On : ".$date);
			$this->Ln(10);
			$this->Cell(30,10,"Report Created By : ".$login);

			$this->Ln(10);
		}
		function b_details()
		{
			$this->Ln(2);
			//$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(15);
			//$this->MultiCell(70, 4,"Anita Shet\n
//Authorized Signatory
	//		");
		}


	}


	$pdf=new PDF();
	$header=array('Sl.No:','SignUp Date','Total Revenue','SignUp Revenue','Addon Revenue');
	$pdf->AddPage();
	$pdf->Ln(0);
	$pdf->doc_details($f_date,$t_date);
	$pdf->BasicTable($header,$f_date,$t_date,$dateArray,$rowArray1,$rowArray2,$rowArray3,$login);
	$pdf->b_details();
	$pdf->Output("../images/Reports/".$p."$d.pdf","F");

	?>
</body>
</html>
