<?php

  	include("Sajax.php");

	sajax_init();

	sajax_export('getdata');

	sajax_export('getquery');

	sajax_handle_client_request();        

	session_start();

	include ("../includes/pw_db_connect.php");

    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')

	{

		header("Location: ../index.php");

		exit();

	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>pinkwhalehealthcare</title>

<meta name="description" content="pinkwhalehealthcare">

<link href="../css/designstyles.css" rel="stylesheet" type="text/css">

<link href="../calendar/calendar.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/cards.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>

<script language="javascript" src="calendar/calendar.js"></script>

<style>	

a:hover{

	color:#e70976;

	text-decoration:underline;

}

</style>



<script type="text/javascript">

<?php sajax_show_javascript(); ?>



function check_spe_exp()

{



	var sel_doc=document.getElementById('doctor').value;

       // alert(sel_doc);

	if(sel_doc!="0" && sel_doc!="1"){

            var id=encodeURI(sel_doc);



            $('#doc_ctgory1').load('get-doc-catagory-list.php?doctor_id='+id+'&type=catagory');	

            $('#doc_qry1').load('get-doc-catagory-list.php?doctor_id='+id+'&type=query');

            setTimeout("set_spe_exp_doctor()", 1000);

        }else{

            document.getElementById("doc_ctgory1").innerHTML="<input type='hidden' id='doc_ctgory' name='doc_ctgory' value=''/>";

            document.getElementById("doc_qry1").innerHTML="<input type='hidden' id='doc_qry' name='doc_qry' value=''/>";

            setTimeout("set_spe_exp_doctor()", 1000);

        }

}



function set_spe_exp_doctor(){



	var sel_type=document.getElementById('doc_type').value;

	var sel_doc=document.getElementById('doctor').value;

	var doc_qry = document.getElementById('doc_qry').value;

	var doc_catgry=document.getElementById('doc_ctgory').value;



	//alert("sel_doc --> "+sel_doc+"  doc_qry --> "+doc_qry +" doc_catgry --> "+doc_catgry);



	document.getElementById('exp_doctors').value= "";

	document.getElementById('exp_doctors_hiden').value= "";

	document.getElementById('exp_doctors').selected=true;

	document.getElementById('exp_doctors').disabled=true;

	document.getElementById('Expert_1session').disabled=true;

	document.getElementById('Expert_2session').disabled=true;

	//document.getElementById('Expert_3session').disabled=true;

        //document.getElementById('Expert_4session').disabled=true;

	document.getElementById('spe_doctors').value= "";

	document.getElementById('spe_doctors_hiden').value= "";

	document.getElementById('spe_doctors').selected=true;

	document.getElementById('spe_doctors').disabled=true;

        document.getElementById('Specialist_3minutes').disabled=true;

	document.getElementById('Specialist_15minutes').disabled=true;

	document.getElementById('Specialist_30minutes').disabled=true;

	//document.getElementById('Specialist_60minutes').disabled=true;

	document.getElementById('Spcialist_1email').disabled=true;

	//document.getElementById('Spcialist_3email').disabled=true;

	document.getElementById('Spcialist_5email').disabled=true;

        document.getElementById('Spcialist_10email').disabled=true;

	document.getElementById('qry_doctors').value= "";

        document.getElementById('qry_doctors_hiden').value= "";

	document.getElementById('qry_doctors').selected=true;

	document.getElementById('qry_doctors').disabled=true;

	document.getElementById('Query_1session').disabled=true;

	document.getElementById('Query_3session').disabled=true;

	document.getElementById('Query_5session').disabled=true;



	if(sel_type=="Specialist/Expert" || doc_catgry=="Specialist/Expert"){

		//alert("Specialist/Expert");

		document.getElementById('exp_doctors').selected=true;

		document.getElementById('exp_doctors').value= sel_doc;

		document.getElementById('exp_doctors_hiden').value= sel_doc;

		document.getElementById('spe_doctors').selected=true;

		document.getElementById('spe_doctors').value= sel_doc;

		document.getElementById('spe_doctors_hiden').value= sel_doc;		

                document.getElementById('Specialist_3minutes').disabled=false;

		document.getElementById('Specialist_15minutes').disabled=false;

		document.getElementById('Specialist_30minutes').disabled=false;

		//document.getElementById('Specialist_60minutes').disabled=false;

		document.getElementById('Spcialist_1email').disabled=false;

		//document.getElementById('Spcialist_3email').disabled=false;

		document.getElementById('Spcialist_5email').disabled=false;

                document.getElementById('Spcialist_10email').disabled=false;

		document.getElementById('Expert_1session').disabled=false;

		document.getElementById('Expert_2session').disabled=false;

		//document.getElementById('Expert_3session').disabled=false;

                //document.getElementById('Expert_4session').disabled=false;

	}else if(sel_type=="Specialist" || doc_catgry=="Specialist"){

		//alert("Specialist");

		document.getElementById('spe_doctors').selected=true;

		document.getElementById('spe_doctors').value= sel_doc;

		document.getElementById('spe_doctors_hiden').value= sel_doc;

                document.getElementById('Specialist_3minutes').disabled=false;

		document.getElementById('Specialist_15minutes').disabled=false;

		document.getElementById('Specialist_30minutes').disabled=false;

		//document.getElementById('Specialist_60minutes').disabled=false;

		document.getElementById('Spcialist_1email').disabled=false;

		//document.getElementById('Spcialist_3email').disabled=false;

		document.getElementById('Spcialist_5email').disabled=false;

        	document.getElementById('Spcialist_10email').disabled=false;

	}else if(sel_type=="Expert" || doc_catgry=="Expert"){

		//alert("Expert");

		document.getElementById('qry_doctors').selected=true;

		document.getElementById('exp_doctors').value= sel_doc;

		document.getElementById('exp_doctors_hiden').value= sel_doc;

		document.getElementById('Expert_1session').disabled=false;

		document.getElementById('Expert_2session').disabled=false;

		//document.getElementById('Expert_3session').disabled=false;

        	//document.getElementById('Expert_4session').disabled=false;

	}else if(sel_type=="query"){

		//alert("Query");

                alert(sel_doc);

		document.getElementById('qry_doctors').selected=true;

		document.getElementById('qry_doctors').value= sel_doc;

                document.getElementById('qry_doctors_hiden').value= sel_doc;

		document.getElementById('Query_1session').disabled=false;

		document.getElementById('Query_3session').disabled=false;

		document.getElementById('Query_5session').disabled=false;

	}





}





</script>

</head>

<body>

<?php

require_once('calendar/classes/tc_calendar.php');

?>



<?php include "admin_head.php"; ?>

<!-- side Menu -->

<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >

<tr>

<td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>

</td>

<td width="850" >

<form method="post" name="add_packages" id="add_packages" action="actions/add-card-action_for_gencard.php">

<div id="doc_ctgory1" ><input type='hidden' id='doc_ctgory' name='doc_ctgory' value=''/></div>

<div id="doc_qry1" ><input type='hidden' id='doc_qry' name='doc_qry' value=''/></div>

    <table cellspacing="0" cellpadding="0" border="0"  width="726" align="center" class='s90registerform'>

    <tr><th colspan="6">Manage Cards</th></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr>

    	<td width="274" bgcolor="#F5F5F5">

        	<div class="postpropertytext">Number Of Cards:&nbsp;<font color="#FF0000">*</font></div>

        </td>

    	<td colspan="3"  bgcolor="#F5F5F5">

        	<input type="text" name="num_of_cards" class="input" onkeypress="return isNumberKey(event);"/>

        </td>

    </tr>

<!--  ERROR MSG-->

    <tr>

    	<td>&nbsp;</td>

    	<td colspan="3" align="left">

    		<div id="erroeCard" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

    	</td>

    </tr>
	
	<!-- select doc_type_cards-->
	 <tr>

    	<td width="274" bgcolor="#F5F5F5">

        	<div class="postpropertytext">Doctor type:&nbsp;<font color="#FF0000">*</font></div>

        </td>

    	<td width="30" bgcolor="#F5F5F5">Tele

    			<input type="checkbox" id="tele_type" name="tele_type"  value="tele" />

    		</td>
			
			<td width="30" bgcolor="#F5F5F5">Online

    			<input type="checkbox" id="online_type" name="online_type"  value="online" />

    		</td>
			
			<!--td width="30" bgcolor="#F5F5F5">Both

    			<input type="checkbox" id="both" name="both"  value="both" />

    		</td-->

    </tr>
	
	<!--  ERROR MSG-->

    <tr>

    	<td>&nbsp;</td>

    	<td colspan="3" align="left">

    		<div id="erroeCard" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

    	</td>

    </tr>

<!--  ERROR MSG-->

	<tr>

    <td width="274" bgcolor="#F5F5F5"><div class="postpropertytext">Select Group : </div></td>

    <td  bgcolor="#F5F5F5" colspan="3">

 	<select  name="group" id="group" class="registetextbox" onchange="enable_disable_grup(add_packages);"> 

    	<option value="">- Select Group -</option>

    	<?php

    	    $qry="SELECT `group_name` FROM groups";

    		if(!$qry_rslt=mysql_query($qry)) die(mysql_error());

       		while($qry_result=mysql_fetch_array($qry_rslt))

    	    {

    	?>

    		<option value="<?php echo $qry_result['group_name']; ?>"><?php echo $qry_result['group_name']; ?></option>

    	<?php } ?>

    </select>

    </td>

    </tr>

    <tr><td>&nbsp;</td><td align="center"><div style="text-align:center;"><strong> OR </strong></div></td></tr>

    <tr>

    <td bgcolor="#F5F5F5"><div class="postpropertytext">Select Doctor Type:&nbsp;</div></td>

    <td width="153" bgcolor="#F5F5F5">

    <select  name="doc_type" id="doc_type" class="registetextbox" onChange="setdoctor(),enable_disable_doc(add_packages);" > 

			   <option value="">-Select doctor type-</option>

               <option value="Specialist">Specialist </option>

               <option value="Expert">Expert</option>

               <option value="query">Query</option>

  		</select></td>

    <td width="115" bgcolor="#F5F5F5"><div class="postpropertytext"> Select Doctor: </div></td>

    <td width="188" bgcolor="#F5F5F5">

    <select  name="doctor" id="doctor" class="registetextbox" disabled="disabled" onchange="check_spe_exp(); " > 

    		<option value="">-Select Doctor-</option>

   			

    </select>

   </td>

    </tr>

<!--  ERROR MSG-->

    <tr>

    	<td colspan="2" align="center"><div id="GroupError" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

	</td><td>&nbsp;</td>

    	<td>

    		<div id="comError" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

        </td>

   </tr>

<!--  ERROR MSG-->

     

    <!--            Old Query               -->

    

    <!--

     <tr><td bgcolor="#F5F5F5" colspan="4">

    	<div style="font-family:Arial, Verdana; font-weight:bold;">Query Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

        <tr><td colspan="1" align="right" bgcolor="#F5F5F5" height="40">

      Query package (valid till) : &nbsp;

      </td>  

      <td colspan="3" align="left" bgcolor="#F5F5F5" height="40">

      <?php

	  $myCalendar = new tc_calendar("query_valid", true, false);

	  $myCalendar->setIcon("../calendar/images/iconCalendar.gif");

	  $myCalendar->setDate(date('d'), date('m'), date('Y'));

	  $myCalendar->setPath("../calendar/");

	  $myCalendar->setYearInterval(1910, 2015);

	  $myCalendar->dateAllow('1910-01-01', '2020-03-01');

	  $myCalendar->setDateFormat('j F Y');

	  //$myCalendar->setHeight(350);	  

	  //$myCalendar->autoSubmit(true, "form1");

	  $myCalendar->setAlignment('left', 'bottom');

	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');

	  $myCalendar->writeScript();

	  ?>

            </td></tr> 

   

    

    -->

    <!--            Old Query End              -->

    

    <!---- Query Start  --->

    

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

     <tr><td bgcolor="#F5F5F5" colspan="4">

    	<div style="font-family:Arial, Verdana; font-weight:bold;">Query Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    <tr>

        <td width="274" bgcolor="#F5F5F5" ><div class="postpropertytext">Select Doctor:&nbsp;</div></td>

        <td colspan="3" bgcolor="#F5F5F5" >

            <select name="qry_doctors" id="qry_doctors" class="registetextbox">

                <option value="">- Select Doctor Type -</option>

                <option value="0">General Physician</option>

                <option value="1">Specialist</option>

            </select>

            <input type="hidden" id="qry_doctors_hiden" name="qry_doctors_hiden"  />

        </td>    

    </tr>

    <tr><td colspan="4">

    <table width="725" border="0" cellpadding="0" cellspacing="0">

        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    	<tr>

    		<td width="172" bgcolor="#F5F5F5"><div class="postpropertytext1">1 Session</div></td>

    		<td width="30" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Query_1session" name="Query_1session"  value="1" />

    		</td>

    		<td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">3 Session</div></td>

   			 <td width="30" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Query_3session" name="Query_3session"  value="3"/>

    		</td>

    		<td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">5 Session</div></td>

    		<td width="133" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Query_5session" name="Query_5session"  value="5"/>

    		</td>

        </tr>

                

      <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

   	</table>

    </td></tr>

<!--  ERROR MSG-->

    <tr>

    	<td align="center">

    		<div id="errorQuery" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

        </td>

   </tr>

<!--  ERROR MSG-->

    <!--     Query End      --->

    

       <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    <tr><td bgcolor="#F5F5F5" colspan="4">

    	<div style="font-family:Arial, Verdana; font-weight:bold;">Experts Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    <tr>

    <td width="274" bgcolor="#F5F5F5" ><div class="postpropertytext">Select Doctor:&nbsp;</div></td>

    <td colspan="3" bgcolor="#F5F5F5" >

    <select name="exp_doctors" id="exp_doctors" class="registetextbox">

    	<option value="">-Select Doctor-</option>

    <?php

  	  	$qry="SELECT `doc_name`,doc_id FROM pw_doctors WHERE `doc_category`='Expert' || `doc_category`='Specialist/Expert'";

    	if(!$qry_rslt=mysql_query($qry)) die(mysql_error());

    	while($qry_result=mysql_fetch_array($qry_rslt))

    	{

    ?>

    	<option value="<?php echo $qry_result['doc_id']; ?>"><?php echo $qry_result['doc_name']; ?></option>

    <?php } ?>

    </select>

    <input type="hidden" id="exp_doctors_hiden" name="exp_doctors_hiden"  />

    </td></tr>

    <tr><td colspan="4">

    <table width="725" border="0" cellpadding="0" cellspacing="0">

        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    	<tr>

    		<td width="172" bgcolor="#F5F5F5"><div class="postpropertytext1">1 Session</div></td>

    		<td width="30" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Expert_1session" name="Expert_1session"  value="1" />

    		</td>

                

    		<td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">3 Session</div></td>

   			 <td width="30" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Expert_2session" name="Expert_2session"  value="3"/>

    		</td>

                <!--

    		<td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">5 Session</div></td>

    		<td width="133" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Expert_3session" name="Expert_3session"  value="5"/>

    		</td>-->                

        </tr>

        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

        <!--

        <tr>

                <td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">10 Session</div></td>

    		<td width="133" bgcolor="#F5F5F5">

    			<input type="checkbox" id="Expert_4session" name="Expert_4session"  value="5"/>

    		</td>

                <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

                <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    	</tr>

        -->



<!--  ERROR MSG-->

    <tr>

	<td>&nbsp</td><td>&nbsp</td>

    	<td align="center">

    		<div id="Experterror" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

        </td>

   </tr>

<!--  ERROR MSG-->        

      <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

   	</table>

    </td></tr>



    <tr><td bgcolor="#F5F5F5" colspan="4">

    	<div style="font-family:Arial, Verdana; font-weight:bold;">Specialist Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    

    

    <tr>

    <td bgcolor="#F5F5F5">

    	<div class="postpropertytext">Select Doctor:&nbsp;</div></td>

    <td colspan="3" bgcolor="#F5F5F5" >

     <select name="spe_doctors" id="spe_doctors"  onchange="changeSpeDiv(this.value)" class="registetextbox">

    	<option value="">- Select Doctor -</option>

    	<?php

    	    $qry="SELECT `doc_name`,doc_id FROM pw_doctors WHERE `doc_category`='Specialist' || `doc_category`='Specialist/Expert'";

    		if(!$qry_rslt=mysql_query($qry)) die(mysql_error());

    	    while($qry_result=mysql_fetch_array($qry_rslt))

   		    {

    	?>

    		<option value="<?php echo $qry_result['doc_id']; ?>"><?php echo $qry_result['doc_name']; ?></option>

    <?php } ?>

    </select>

    <input type="hidden" id="spe_doctors_hiden" name="spe_doctors_hiden"  />

    </td>

    </tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
	
    <tr id="specialist_div">

    <td colspan="4"><table width="725" border="0" cellpadding="0" cellspacing="0">

    <tr>

    <td width="172" bgcolor="#F5F5F5" ><div class="postpropertytext1">3 Minutes</div></td>

    <td  width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_3minutes" name="Specialist_3minutes"  value="3" />

    </td>

    <td width="172" bgcolor="#F5F5F5" ><div class="postpropertytext1">15 Minutes</div></td>

    <td  width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_15minutes" name="Specialist_15minutes"  value="15" />

    </td>

    <td  bgcolor="#F5F5F5" ><div class="postpropertytext1">30 Minutes</div></td>

    <td bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_30minutes" name="Specialist_30minutes"  value="30" />

    </td>

    <!--

    <td width="180" bgcolor="#F5F5F5" ><div class="postpropertytext1">60 Minutes</div></td>

    <td width="133" bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_60minutes" name="Specialist_60minutes"  value="60" />

    </td> 

    -->

    </tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    

    <!--<tr><td width="172" bgcolor="#F5F5F5" ><div class="postpropertytext1">120 Minutes </div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_120minutes" name="Specialist_120minutes"  value="120"/>

    </td>

    <td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">180 Minutes</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Specialist_180Minutes" name="Specialist_180Minutes"  value="180"/>

    </td><td width="180" bgcolor="#F5F5F5"></td>

    <td width="133" bgcolor="#F5F5F5"></td>

    </tr>

    <tr>

    <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0" /></td>

    </tr>-->

    <tr>

    <td bgcolor="#F5F5F5" ><div class="postpropertytext1">1 Email Session</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Spcialist_1email" name="Spcialist_1email"  value="1"/></td>

    <!--

    <td bgcolor="#F5F5F5" ><div class="postpropertytext1">3 Email Session</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Spcialist_3email" name="Spcialist_3email"  value="3"/></td>

    -->

    <td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">5 Email Session </div></td>

    <td width="133" bgcolor="#F5F5F5">

    <input type="checkbox" id="Spcialist_5email" name="Spcialist_5email"  value="5"/></td>

    <td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">10 Email Session </div></td>

    <td width="133" bgcolor="#F5F5F5">

    <input type="checkbox" id="Spcialist_10email" name="Spcialist_10email"  value="10"/></td>

        <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

        <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    </tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <!--

    <tr>

        <td width="180" bgcolor="#F5F5F5"><div class="postpropertytext1">10 Email Session </div></td>

    <td width="133" bgcolor="#F5F5F5">

    <input type="checkbox" id="Spcialist_10email" name="Spcialist_10email"  value="5"/></td>

        <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

        <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    </tr>

    -->

    </table></td>

    </tr>

<!--  ERROR MSG-->

    <tr>

	<td>&nbsp</td>

    	<td align="center">

    		<div id="Specialerror" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

        </td>

   </tr>

<!--  ERROR MSG-->   

   <!--  <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr><td bgcolor="#F5F5F5" colspan="4">

    <div style="font-family:Arial, Verdana; font-weight:bold;">Physician Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    <tr><td colspan="4">

    <table border="0" cellspacing="0" cellpadding="0" width="725">

    <tr>

    <td width="172" bgcolor="#F5F5F5">

    <div class="postpropertytext1">1 Chat Session</div>

    </td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="physician_1chat" name="physician_1chat"  value="1" />

    </td>

    <td width="180" bgcolor="#F5F5F5">

    <div class="postpropertytext1">3 Chat Session</div>

    </td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="physician_2chat" name="physician_2chat"  value="3"/>

    </td>

    <td width="180" bgcolor="#F5F5F5">

    <div class="postpropertytext1">5 Chat Session</div>

    </td>

    <td width="133" bgcolor="#F5F5F5">

    <input type="checkbox" id="physician_3chat" name="physician_3chat"  value="5"/>

    </td>

    </tr>

    </table>

    </td></tr> -->

    

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <!--tr><td bgcolor="#F5F5F5" colspan="4">

    <div style="font-family:Arial, Verdana; font-weight:bold;">Counsellor Packages :</div>

    </td></tr>

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr> 

    <tr><td colspan="4">

    <table border="0" cellspacing="0" cellpadding="0" width="725">

    <tr>

    <td width="173" bgcolor="#F5F5F5"><div class="postpropertytext1">60 Minutes</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_60min" name="Counsellor_60min"  value="60" />

    </td>

    <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    <!--<td width="178" bgcolor="#F5F5F5"><div class="postpropertytext1">120 Minutes </div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_120min" name="Counsellor_120min"  value="120"/>

    </td>

    <td width="179" bgcolor="#F5F5F5"><div class="postpropertytext1">180 Minutes</div></td>

    <td width="135" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_180min" name="Counsellor_180min"  value="180"/>

    </td>

    </tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr>

    <td width="173" bgcolor="#F5F5F5"><div class="postpropertytext1">1 Email Session</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_1email" name="Counsellor_1email"  value="1"/>

    </td>

    <td width="178" bgcolor="#F5F5F5"><div class="postpropertytext1">3 Email Session</div></td>

    <td width="30" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_3email" name="Counsellor_3email"  value="3"/>

    </td>

    <td width="179" bgcolor="#F5F5F5"><div class="postpropertytext1">5 Email Session</div></td>

    <td width="135" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_5email" name="Counsellor_5email"  value="5"/>

    </td>

    </tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr>

    <td width="179" bgcolor="#F5F5F5"><div class="postpropertytext1">10 Email Session</div></td>

    <td width="135" bgcolor="#F5F5F5">

    <input type="checkbox" id="Counsellor_10email" name="Counsellor_10email"  value="10"/>

    </td>

    <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    <td width="180" bgcolor="#F5F5F5"></td><td width="133" bgcolor="#F5F5F5"></td>

    </tr>

    </table>

     </td></tr-->

<!--  ERROR MSG-->

    <tr>

	<td>&nbsp</td>

    	<td align="center">

    		<div id="PackageError" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

        </td>

   </tr>

<!--  ERROR MSG--> 

    <tr><td colspan="4"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr><td align="center" colspan="5">

    		<input type="button" name="add_cards" value="Generate" id="add_cards" onclick="AddCards(add_packages)" />

    </td></tr>

    </table>

 </form>

</td></tr>

</table>

<script type="text/javascript">

 	enable_cards_submenu();

</script>

 <?php include 'admin_footer.php'; ?>



</body></html>


