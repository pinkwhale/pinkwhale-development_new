<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
        require_once('calendar/classes/tc_calendar.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<link href="../calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script language="javascript" src="js/appointment.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<script type="text/javascript">
    var clinic = false;
    var date = false;
    function get_booking_details(form){
        clinic =true;
        date = true;
        
        document.getElementById("clinicErrDiv").innerHTML="";
        document.getElementById("dateErrDiv").innerHTML="";
        
        if(document.getElementById("clinic").value==""){
            document.getElementById("clinicErrDiv").innerHTML="Please select Clinic";
            clinic =false;
        }
        
        if(document.getElementById("date1").value==""){
            document.getElementById("dateErrDiv").innerHTML="Please select Date";
            clinic =false;
        }
        
        if(clinic && date){
            document.getElementById("booking_details").submit();
        }else{
            return false;
        }
    }
</script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php 
require_once('calendar/classes/tc_calendar.php');
include "admin_left_menu.php"; 
include "get-who-booked-details.php";
?>
</td>
<td width="772" valign="top">
    <form action="" method="POST" name="booking_details" id="booking_details">
        <table border="0" width="400" cellpadding="0" cellspacing="1" align="center" class='s90registerform'>
            <tr>
                <th colspan="2">Patient Appointment Summary</th>
            </tr>
            <tr>
                <td align="right" bgcolor="#F5F5F5"><b>Clinic</b>&nbsp;&nbsp;:</td>
                <td align="left" bgcolor="#F5F5F5">
                    <select name="clinic" id="clinic" >
                        <option value="" selected="selected">-----select Clinic-----</option>
                        <?php
                            //$qry = "select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id and status=0 where name<>'' group by c.clinic_id union select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id and status=0 where name<>'' group by c.clinic_id";
                            $qry = "select clinic_id,name from clinic_details where status=0 and name<>''";
                            $res = mysql_query($qry);
                            while($data = mysql_fetch_array($res)){
                                echo "<option value='".$data['clinic_id']."|".$data['name']."' >".$data['name']."</option>";
                            }
                        ?>
                    </select>
                </td>        
            </tr>
            <!--    ERROR DIV -->
            <tr>
                 <td> </td>
                 <td  align="left" height="8">
                        <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                 </td>
            </tr>
            <!--  END ERROR DIV --> 
            <tr>
                <td align="right" bgcolor="#F5F5F5"> <b>Date</b>&nbsp; : </td>  
                <td  align="left" bgcolor="#F5F5F5">
                <?php
                      $myCalendar = new tc_calendar("date1", true, false);
                      $myCalendar->setIcon("../calendar/images/iconCalendar.gif");
                      $myCalendar->setDate(date('d'), date('m'), date('Y'));
                      $myCalendar->setPath("../calendar/");
                      $myCalendar->setYearInterval(1910, 2015);
                      //$myCalendar->dateAllow(date("Y-m-d", time()), '2020-03-01');
                      $myCalendar->setDateFormat('j F Y');
                      //$myCalendar->setHeight(350);	  
                      //$myCalendar->autoSubmit(true, "form1");
                      $myCalendar->setAlignment('left', 'bottom');
                      //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                      $myCalendar->writeScript();
                 ?>
                 </td>
            </tr>
            <!--    ERROR DIV -->
            <tr>
                 <td> </td>
                 <td  align="left" height="8">
                        <div id="dateErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                 </td>
            </tr>
            <!--  END ERROR DIV -->
            <tr>
                <td align="center" colspan="2" bgcolor="#F5F5F5">
                    <input type="button" value="Go" onclick="get_booking_details()"/>
                </td>        
            </tr>
        </table>
    </form>
    <?php
        if($_POST['clinic']!="" && $_POST['date1']!=""){
            $clinic = explode("|",$_POST['clinic']);
            $clinic_id = $clinic[0];
            $clinic_name = $clinic[1];
    ?>      
    <div id="clinic_dash" align="center"> 
            <table border="0" cellpadding="0" cellspacing="1" width="600" align="center" bgcolor='#eeeeee' class="s90registerform">
                <?php     
                    
                        $today_date = $_POST['date1'];
                        echo "<tr><th colspan=\"5\" align='center'>Doctor Wise Appointment Details for ". date("d M Y",strtotime($_POST['date1']))." </th></tr>";
                        echo "<tr><th colspan=\"5\" align='center'>Clinic Name &nbsp;:&nbsp;".$clinic_name."</th></tr>";
                ?>                
                
                <tr>
                    <td align='center'><b>Doctor Name</b></td>
                    <td align='center'><b>Appointment Time</b></td>
                    <td align='center'><b>Patient Name</b></td>
                    <td align='center'><b>Token-id</b></td>
                    <td align='center'><b>Booked By</b></td>
                </tr>
                <tr>
                <?php

                    

                    $qry = "select p.doc_name,p.doc_id from clinic_doctors_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.appoint_flag=1 and p.blocked<>'Y' union select p.doc_name,p.doc_id from doctor_clinic_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.appoint_flag=1 and p.blocked<>'Y'";
                    $res = mysql_query($qry);
                    $num = mysql_num_rows($res);
                    if($num==0){
                        echo "<script>document.getElementById(\"clinic_dash\").innerHTML=\"<font size=4 color=red>No Doctor's </font>\";</script>";
                    }else{
                        while($dat = mysql_fetch_array($res)){
                            $qry1 = "select admin_id,DATE_FORMAT(from_time,'%h:%i %p') as from_time,patient_name,email,mobile,gender,age,adress,token_id from Appointment_book_details where doc_id='".$dat['doc_id']."' and clinic_id='$clinic_id' and from_time like '$today_date%' and status=2 order by from_time";
                            $res1 = mysql_query($qry1);
                            $num1 = mysql_num_rows($res1);                            
                            if($num1==0){
                                echo "<td  align='left' bgcolor='#F5F5F5'>".$dat['doc_name']."</td>";
                                echo "<td colspan='4' align='center' bgcolor='#F5F5F5'>No Appointment's </td></tr><tr>";
                            }else{
                                echo "<td rowspan='$num1' align='left' bgcolor='#F5F5F5'>".$dat['doc_name']."</td>";
                                while($data1 = mysql_fetch_array($res1)){
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['from_time']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['patient_name']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['token_id']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".get_name_detals($data1['admin_id'])."</td></tr><tr>";
                                }                            
                            }
                        }
                    }
                ?>
                </tr></table>
    </div>
    <?php
        }        
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
 	
	enable_appointment_submenu();
        
</script>
</html>
<?php
ob_end_flush();
?>
