<?php
  	include("Sajax.php");
	sajax_init();
	sajax_export('block_fun');
	sajax_handle_client_request();
?>
<?php 

	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>pinkwhalehealthcare</title>
    <meta name="description" content="pinkwhalehealthcare">
    <link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
    <link href="css/my_css.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
    
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
<script type="text/javascript">
	<?php sajax_show_javascript(); ?>
	function fun_block_user(block_id)
	{
		x_block_fun(block_id,doc_details);
	}
	function doc_details(details) 
	{
		location.reload(true);
	}
</script>
</head>
<?php
	function block_fun($id)
	{        
		include ("../includes/pw_db_connect.php");
		$qry2 ="select `blocked` from `user_details` WHERE `user_id`='$id'";
		$res2 = mysql_query($qry2);
		if($result = mysql_fetch_array($res2))
		{
			$curr_status=$result['blocked'];
		}
		if($curr_status=='Y')
		{
			$qry ="update `user_details` set `blocked`='N' WHERE `user_id`='$id'";
		}
		else
		{
			$qry ="update `user_details` set `blocked`='Y' WHERE `user_id`='$id'";
		}
		$res = mysql_query($qry);
		return 0;
	}
?>
<body onLoad="init_table();">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" >
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
?>
		
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="7">Manage Cards</span></th></tr>
<?php		
		// Display all the data from the table 
			$sql="SELECT * , count( card_num )FROM `pw_cards` GROUP BY card_group, day(`generate_time`),Month(`generate_time`),year(`generate_time`),Hour(`generate_time`),Minute(`generate_time`) ORDER BY `generate_time` DESC";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) die(mysql_error());
			?>
		
			<tr>
                <td align='left'><strong>SI No.</strong></td>
                <td align='left'><strong>Group/Doctor Name</strong></td>
                <td align='left'><strong>No of Cards</strong></td>
                <td align='left'><strong> Generated Date</strong></td>
                <td align='left'><strong>Excel</strong></td>
            </tr>
	
			<?php	
			$count =0;
			while($row = mysql_fetch_assoc($rs)) 
			{
				$count ++;
				$imf_flag=0;
				$grp_name=$row['card_group'];
				$num_of_card=$row['count( card_num )'];
				$gen_date=$row['generate_time'];
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td>$count </td> ";
				echo "<td>$grp_name </td> ";
				echo "<td>$num_of_card </td> ";
				echo "<td>$gen_date </td> ";
				echo "<td align='center'>";
				?>
                	<a href="card_details_excel.php?grp_name=<?php echo $grp_name ; ?>&gen_time=<?php echo $gen_date ; ?>">
                		<img src="../images/excelLogo.gif" />
                    </a> 
				<?php 
				echo "</td>";
				echo "</tr>";
			
			}
			
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>', '</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
			<tr><td colspan='5' align='center'>
            <?php
            echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
        </table>
</td>
</tr>
</table>
<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_cards_submenu();
</script>
</body></html>