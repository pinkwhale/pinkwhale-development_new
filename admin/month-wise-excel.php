<?php
error_reporting(E_PARSE);
include '../db_connect.php';

$type = $_REQUEST['type'];
$year = $_REQUEST['year'];
$subval = $_REQUEST['subval'];
echo "";

if($year=="2011"){
    $date = "2011-06-05";    
    $count = 7;
}else{
    $date = $year."-01-05";
    $count = 12;
}

$current_year = strftime ( '%b-%y', (time()));

$filename = "$type-$year-Monthly-report".xls;

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$filename.'"');

include 'summary.report.class.php';

$obj = new SummaryReport;

$type_name = $obj->getTypeName($type);


$menu = "";

if($subval!=""){
   $menu = $subval;
}


if($type!="" && $year!=""){

    
    
    if($type=="spe_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where activate_online_consultation='online' and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
    }else if($type=="exp_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where ( doc_category='Specialist/Expert' || doc_category='Expert')";
    }else if($type=="query_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where activate_online_query='query'";
    }else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
        $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
    }else if($type=="spe_tele"){
        $doc_qry = "select doc_id,doc_name,doc_specialities from pw_doctors p inner join tele_consultation_summary t on doctor_id=doc_id where type='$type' group by doc_id ";
    }
    
    $doc_result = mysql_query($doc_qry);
    $doc_num = mysql_num_rows($doc_result);
    if($doc_num>0){
        
        
        
        $display =  $type_name." Data for Year - ".$year;
        
        echo  " <br /><br ><center><strong>$display<strong></center>";
        
        echo  "<table border='0' cellpadding='0' cellspacing='1' width='90%'  align='center' ><tr>";
        echo  "<th align='left'><b>Doctor Name</b></th>";
        
        for($i=0 ; $i<$count ; $i++){
            
            $dynamicyear = strftime("%b-%y",(strtotime($date." +$i months")));
            
            
            if($current_year==$dynamicyear){
                echo  "<th align='right'><b>".$dynamicyear."</b></td>";
                break;
            }else if($current_year!=$dynamicyear){
                echo  "<th align='right'><b>".$dynamicyear."</b></td>";
            }
        }
        echo "<th align='right'><b>Total</b></th>";
        echo  "</tr>";
        
        $total_bottom = array() ;
        
        while($doc_data = mysql_fetch_array($doc_result)){
            
            $doc_id = $doc_data['doc_id'];
            $doc_name = $doc_data['doc_name'];
            $doc_spe = $doc_data['doc_specialities'];
            echo  "<tr>";
            echo  "<td nowrap>$doc_name</td>";
            
            if($year=="2011"){
                $date = "2011-06-05";    
                $count = 7;
            }else{
                $date = $year."-01-05";
                $count = 12;
            }
            
            $total_count = 0;
            
            for($i=0 ; $i<$count ; $i++){
                
                $dynamicyear = strftime("%b-%y",(strtotime($date." +$i months")));
            
                if($current_year==$dynamicyear){
                    $dd = strftime("%m-%Y",(strtotime($date." +$i months")));
                    if($type=="spe_tele"){
                        $no = $obj->getmonthlyteleSummary($type, $dd, $doc_id, $subval);
                    }else{
                        if($type=="query_online"){
                            $no = $obj->getmonthlyquerySummary($subval, $dd, $doc_id);
                        }else{
                            $no = $obj->getmonthlyOnlineSummary($type, $dd, $doc_id);
                        }
                    }
                    $total_count +=$no;
                    $total_bottom[$i] +=$no;
                    echo  "<td align='center'>".$no."</td>";
                    break;
                }else if($current_year!=$dynamicyear){
                    $dd = strftime("%m-%Y",(strtotime($date." +$i months")));
                    if($type=="spe_tele"){
                        $no = $obj->getmonthlyteleSummary($type, $dd, $doc_id, $subval);
                    }else{
                        if($type=="query_online"){
                            $no = $obj->getmonthlyquerySummary($subval, $dd, $doc_id);
                        }else{
                            $no = $obj->getmonthlyOnlineSummary($type, $dd, $doc_id);
                        }
                    }
                    
                    $total_count +=$no;
                    $total_bottom[$i] +=$no;
                    echo  "<td align='center'>".$no."</td>";
                }
            }
            
            echo "<td align='center'><b>$total_count</b></td>";
            echo  "</tr>";
            
        }
        echo  "<tr>";
        $total_count = 0;
        echo "<td align='left' nowrap><b>Total # of Consults</b></td>";
        for($i=0;$i<sizeof($total_bottom);$i++){
            $total_count += $total_bottom[$i];
            echo "<td align='center'><b>$total_bottom[$i]</b></td>";
        }
        echo "<td align='center'><b>$total_count</b></td>";
        echo  "</tr>"; 
        
        echo  "</table>";
        
    }else{
        echo "No Records Found";
    }

}else{
    echo "No Records Found";
}




?>

