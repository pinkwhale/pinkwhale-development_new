<?php 
ob_start();
date_default_timezone_set('Asia/Calcutta');
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
	include ("../pwconstants.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>
<link href="calenda/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script language="javascript" src="js/appointment.js"></script>
<script language="javascript" src="js/book_appointment.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<script type="text/javascript">
 	
        function get_clinic(){
            
            var doc_id=encodeURI(document.getElementById('doctor').value); 
            
            $("#loading").fadeIn(900,0);
            $("#loading").html("<img src='../images/ajax-loader.gif' />");
            $('#clinic').load('get-clinic-details.php?doc='+doc_id); 
            $('#doc_clinic_details').load('get_doc_clinic_details.php?doctor_id='+doc_id,Hide_Load());
            
        }
        
        function get_doc_avail(){
       
            var doctor=encodeURI(document.getElementById('doctor').value);
            
            var clinic=encodeURI(document.getElementById('clinic').value);

            $('#doc_avail').load('get_doctor_avail.php?doctor_id='+doctor+'&clinic_id='+clinic); 

            //document.getElementById("hid").style.display="block";
            document.getElementById("doc_avail_time").innerHTML = "";


        }
        
        function change_category()
        {
     	   if(document.getElementById('category').value=="3")
     	   {
     		   document.getElementById("typelabel").style.display="block";
     		   document.getElementById("typ").style.display="block";
     		   }
     	   else{
     		   document.getElementById("typelabel").style.display="none";
     		   document.getElementById("typ").style.display="none";
        }
     	   get_time_slots();
        }
        
        
        function get_time_slots(){
/*****added by swathi******/
        	var category=document.getElementById('category').value;

		var type=document.getElementById('type').value;
/*****End added by swathi******/
            var doc_id=encodeURI(document.getElementById('doctor').value);
            
            var clinic=encodeURI(document.getElementById('clinic').value);

            var ddate=encodeURI(document.getElementById('date1').value);

            $('#doc_avail_time').load('get_appointment_booking_slots.php?doctor_id='+doc_id+'&date='+ddate+'&clinic_id='+clinic+'&category='+category+'&type='+type); 

            //document.getElementById("hid").style.display="block";
        
        }
        
        
        function proceed_detail(slot24,slot12){
        
            var doc_id=encodeURI(document.getElementById('doctor').value);

            var ddate=encodeURI(document.getElementById('date1').value);
            
            var clinic=encodeURI(document.getElementById('clinic').value);

            var slot24=encodeURI(slot24);

            var slot12=encodeURI(slot12);
            
            //alert(slot24);alert(slot12);

            $('#book_procceed').load('proceed_booking.php?doctor_id='+doc_id+'&date='+ddate+'&slot24='+slot24+'&slot12='+slot12+'&clinic_id='+clinic); 
        }

/****Added by swathi****/
        function check_error(val)
        {
            var count = 0;
        	$("input[type='checkbox']").each(function(){
        	  if($(this).is(":checked")==true){
        		count++;
               } 
        	});

        	if(count==0){
            	alert("Select atleast one checkbox");
            }
        	else
            	{document.book.submit();}
        }
/****End Added by swathi****/        
        function Hide_Load()
        {
            $("#loading").fadeOut('slow');
        };


     
        
</script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php 
//require_once('calendar/tc_calendar.php');
include "admin_left_menu.php"; 
require_once('calendar/tc_calendar.php');
?>
</td>
<td width="772" valign="top" id="mainBg">
    <?php
        if($_SESSION['msg']!=""){
            echo "<br /><center>".$_SESSION['msg']."</center>";
            $_SESSION['msg'] = "";
        }else if($_SESSION['error']!=""){
                echo "<br /><center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";        
        }else{
            
    ?>
    <div id="book_procceed">
    
        <table border="0" width="700" cellpadding="0" cellspacing="1" align="center" class='s90registerform'>
            <tr>
                <th colspan="2">Booked Appointment </th>
            </tr>
            <tr>
                <td align="right" bgcolor="#F5F5F5"><b>Doctor</b>&nbsp;&nbsp;:</td>
                <td align="left" bgcolor="#F5F5F5">
                    <select name="doctor" id="doctor" onchange="get_clinic()" >
                    <option value="" selected="selected" disabled>-----select doctor-----</option>
                        <?php
                            $qry = "select doc_id,doc_name from pw_doctors where appoint_flag=1 and blocked<>'Y'";
                            $res = mysql_query($qry);
                            while ($data = mysql_fetch_array($res)){
                                echo "<option value='".$data['doc_id']."|".$data['doc_name']."'>".$data['doc_name']."</option>";                            
                            }                        
                        ?>
                    </select>
                </td>        
            </tr>
             <!--    ERROR DIV -->
            <tr>
                 <td> </td>
                 <td  align="left" height="8">
                        <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                 </td>
            </tr>
            <!--  END ERROR DIV --> 
            
            <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
            <tr>
                <td width="30%" align="right" bgcolor="#F5F5F5"><b>Clinic</b>:</td>
                <td width="40%" align="left" bgcolor="#F5F5F5" >
                    <select name="clinic" id="clinic" onchange="get_doc_avail()" >
                        <option value="" selected="selected">-----select Clinic-----</option>
                    
                    </select>

                </td>        
            </tr>
            <!--    ERROR DIV -->
            <tr>
                 <td> </td>
                 <td  align="left" height="8">
                        <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                 </td>
            </tr>
            <!--  END ERROR DIV --> 
             <tr>
                <td width="30%" align="right" bgcolor="#F5F5F5"><b>Category</b><font color="#FF0000">*</font>:</td>
                                    <td width="43%" align="left" bgcolor="#F5F5F5">
                <div id="cate">
                <select id="category" name="category" onchange="change_category();">
                <option value='-1'>---Category---</option>
                				<?php  foreach ($categories as $key => $value) {
                                	echo "<option value='$key'>".$value."</option>";
                                 }?>
                </select>
                </div>
                </td>
                
                </tr>
                
                
                  <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <!--  END ERROR DIV --> 
               <tr>
               <td colspan="1" width="40%" align="right" bgcolor="#F5F5F5"><div id="typelabel" style="display:none"><b>Type : </b><font color="#FF0000">*</font>: </div></td>
                <td colspan="1" width="40%" align="left" bgcolor="#F5F5F5">
               <div id="typ" style="display:none">
                              <select id="type" name="type" onchange="get_time_slots()">
                                <?php  foreach ($patient_Types as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                          }?>
                                </select>
                                </div>
                         </td>                              
               </tr>
                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
      
            
            
            
            
            
            <tr>
                <td id="doc_avail" bgcolor="#F5F5F5" width="30%" colspan="2" align="center" ></td>
            </tr>
            
            <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
            
            <tr>
                <td align="right" bgcolor="#F5F5F5"> <b>Date</b>&nbsp; : </td>  
                <td  align="left" bgcolor="#F5F5F5">
                <?php
                      $myCalendar = new tc_calendar("date1");
                      $myCalendar->setIcon("calendar/images/iconCalendar.gif");
                      $myCalendar->setDate(date('d'), date('m'), date('Y'));
                      $myCalendar->setPath("calendar/");
                      $myCalendar->setYearInterval(1910, 2015);
                      $myCalendar->dateAllow(date("Y-m-d", time()), '2020-03-01');
                      $myCalendar->setDateFormat('j F Y');
                      $myCalendar->setOnChange("get_time_slots()");
                      //$myCalendar->setHeight(350);	  
                      //$myCalendar->autoSubmit(true, "form1");
                      $myCalendar->setAlignment('left', 'bottom');
                      //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                      $myCalendar->writeScript();
                 ?>
                 </td>
            </tr>
            <!--    ERROR DIV -->
            <tr>
                 <td> </td>
                 <td  align="left" height="8">
                        <div id="dateErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                 </td>
            </tr>
            <!--  END ERROR DIV -->
            
            <tr>
                    <td id="doc_avail_time" bgcolor="#F5F5F5" width="30%" colspan="2" align="center" ></td>
            </tr>
            
        </table>
    
    </div>
    <?php
            }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>
<script type="text/javascript">
 	
	enable_appointment_submenu();
</script>        
</html>
<?php
ob_end_flush();
?>
