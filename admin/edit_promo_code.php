<?php 
	session_start();
	include ("../db_connect.php");
     if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
	$promo_code_id=$_GET['promo_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/registration_validation.js"></script>
<script type="text/javascript" src="../js/enable-menu.js"></script>
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<script src=http://cdn.webrupee.com/js type="text/javascript"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>

<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?></td>
<td width="850" >
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
?>
<?php
	$qry= "SELECT * FROM `create_promo_code` where `promo_code_id`='$promo_code_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
?>
<form method="post" name="add_promo_code" id="add_promo_code" action="actions/edit_promcode.php">
<table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">
    <tr><th colspan="2">Edit Promo Code </th></tr>
    <input type="hidden" name="promo_id" id="promo_id" value="<?php echo $promo_code_id; ?>"/>
    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
    
    <tr>    
        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Doctor Type:</div></td>
        <td bgcolor="#F5F5F5" width="219">
<select  name="promo_doc_type" id="promo_doc_type" class="registetextbox" onChange="setdoctorpackages();"> 
			   <option value="" selected="selected" disabled>- Select Type -</option>
       		   <option value="Physician">Physician</option>
               <option value="Specialist">Specialist</option>
               <option value="Counseller">Counseller</option>
               <option value="Expert">Expert</option>
        </select>
        </td>        
	 <script type="text/javascript">
    type='<?php echo $result['promo_doctor_type']; ?>';
    document.getElementById('promo_doc_type').value=type;
    document.getElementById('promo_doc_type').selected=true;
	</script>
	</tr>
        
    <!--    ERROR DIV -->

		<tr><td> </td>
        <td  align="left">
	    <div id="doc_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
        </tr>
<!--  END ERROR DIV --> 
    
    <tr>
    <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
     <tr>    
        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Packages:</div></td>
        <td bgcolor="#F5F5F5" width="219">
		<select  name="promo_packages" id="promo_packages" class="registetextbox" > 
			 <option value="">Select Package</option>
       		  
        </select>
        </td>

    </tr> 
    
     <!--    ERROR DIV -->

		<tr><td> </td>
        <td  align="left">
	    <div id="package_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
        </tr>
<!--  END ERROR DIV --> 
      
     <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
     <tr>    
        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Promo Code:</div></td>
        <td bgcolor="#F5F5F5" width="219">
		 <input type="text" name="promocode" class="input" size="20" value="<?php echo $result['promo_code']; ?>" />
        </td>
    </tr>
    
     <!--    ERROR DIV -->

		<tr><td> </td>
        <td  align="left">
	    <div id="promocode_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
        </tr>
<!--  END ERROR DIV --> 
    
    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
     <tr>    
        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Discount: </div></td>
        <td bgcolor="#F5F5F5" width="219">
		 <input type="text" name="promo_discount" class="input" size="20" value="<?php  echo $result['promo_discount']; ?>" />&nbsp;%
        </td>
    </tr>
     <!--    ERROR DIV -->

		<tr><td> </td>
        <td  align="left">
	    <div id="discount_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
        </tr>
<!--  END ERROR DIV --> 
    
    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
     <tr>    
        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Expiry:</div></td>
        <td bgcolor="#F5F5F5" width="219">
		<?php
		$arr = explode('-', $result['promo_expiry']);
				$date = $arr[2].'-'.$arr[1].'-'.$arr[0];
				
	  $myCalendar = new tc_calendar("date5", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date)), date('m', strtotime($date)), date('Y', strtotime($date)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?>
        </td>
    </tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
    <tr>
       
    <td bgcolor="#F5F5F5">&nbsp;</td>
    <td bgcolor="#F5F5F5"><input type="button" value="Save" name="add_pomocode" id="add_pomocode" onclick="create_promo_code(add_promo_code)"/></td>
    </tr>
    </table>
    </form>
</td></tr>
</table>
<?php } ?>
<script type="text/javascript">
	
function setdoctorpackages()
 {

	var doc_type=encodeURI(document.getElementById('promo_doc_type').value);
	$('#promo_packages').load('get-doctor-packages.php?doctor_type='+doc_type);
}

var typeValidated = false;
var packageValidated = false;
var promocodeValidated = false;
var discountValidated = false;
var expiryValidated = false;
	
function create_promo_code(form)
	{

	typeValidated = true;
	packageValidated= true;
	promocodeValidated= true;
	discountValidated= true;
	
	document.getElementById("doc_typeErrDiv").innerHTML	= "";
	document.getElementById("package_typeErrDiv").innerHTML= "";
	document.getElementById("promocode_typeErrDiv").innerHTML = "";
	document.getElementById("discount_typeErrDiv").innerHTML = "";
	
	if (form.promo_doc_type.value=='')
	{
		document.getElementById("doc_typeErrDiv").innerHTML = "Doctor type cannot be blank";
    	typeValidated = false;
	}
	
	if (form.promo_packages.value=='')
	{
		document.getElementById("package_typeErrDiv").innerHTML = "Please select package";
    	packageValidated = false;
	}
	
	if (form.promocode.value=='')
	{
		document.getElementById("promocode_typeErrDiv").innerHTML = "Please enter promo code";
    	promocodeValidated = false;
	}
	
	if (form.promo_discount.value=='')
	{
		document.getElementById("discount_typeErrDiv").innerHTML = "Please enter discount (%)";
    	discountValidated = false;
	}
	
	if(typeValidated && packageValidated && promocodeValidated && discountValidated)
	{
		document.getElementById('add_promo_code').submit();
	}
	else
	{
		return false;
	}
	
}
</script>  
<?php include 'admin_footer.php'; ?>
</body></html>