<?php

session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}


$month1=$_POST['monthnum'];
$year1=$_POST['year'];
$name=$_POST['doc_id'];
echo $name;


$date=$year1."-".$month1."-"."1";
echo "date=".$date;

$f_date=$date;
echo "f_date=".$f_date;


$year=date('y',strtotime($date));
echo "year=".$year;
if((($year % 4) == 0) && ((($year % 100) != 0) || (($year %400) == 0))){$feb=28;}else{$feb=27;}
$month1=date('m',strtotime($date));
echo "month=".$month1;
if($month1<8)
{
	if($month1==2)
	{$adddate=$feb;}
	elseif($month1%2!=0)
		{$adddate=30;}
		else{$adddate=29;}
}
elseif($month1>=8)
{
	if($month1%2!=0)
	{$adddate=29;}
	else{$adddate=30;}
	
}
$t_date = date('Y-m-d',strtotime($date) + (24*3600*$adddate));
echo "to date=".$t_date;
$doc=$_POST['doc_id'];
$mode=$_POST['mode'];
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
require('pdf/fpdf.php');
$d=date('d_m_Y');
$doc_result=mysql_query("SELECT doc_name,pan_number FROM pw_doctors WHERE `doc_id`='$doc'");
while($doc_row=mysql_fetch_array($doc_result))
{	$doc_name=$doc_row['doc_name'];
$pan_number=$doc_row['pan_number'];}
$clinic_d=mysql_query("SELECT name,address FROM clinic_details a JOIN clinic_doctors_details b ON b.clinic_id=a.clinic_id WHERE b.doctor_id='$doc'");
while($clinic_row=mysql_fetch_array($clinic_d))
{
	$c_address=$clinic_row['address'];
	$c_name=$clinic_row['name'];
	
}

class PDF extends FPDF
{

	function Header()
	{
		//Logo
		$this->SetFont('Arial','B',5);
		$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
		$this->Image('pdf/logo.jpg',160,6,40);
		$this->Ln(3);$this->SetFont('Arial','B',5);
		$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
		$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
		$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);
		
		$name="Export PDF";
		$this->SetFont('Arial','B',15);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->SetFont('Arial','B',9);
		//Line break
	$this->Line(0, 25, 300, 25);
	$this->Ln(4);
	$this->SetFont('Arial','BU',9);
	$this->Cell(80);
		$this->Cell(80,10,"INVOICE");
		$this->Ln(15);
	}
	function doc_details($f_date,$t_date,$doc_name){
		$this->Cell(20,10,"From:");
		$this->Cell(20,10,$f_date);
		$this->Cell(20,10,"To:");
		$this->Cell(20,10,$t_date);
		$this->Ln(10);
		$this->Cell(30,10,"Doctor's Name:");
		$this->Cell(40,10,$doc_name);
	}
	//Page footer
	function Footer()
	{
		 
	}	
function reference_table($c_name,$c_address)
{$this->Ln(10);
$invoice_date=date("Y-m-d");
$x=$this->GetX();
$y=$this->GetY();
$this->SetFont('Arial','B',7);
$rand=rand(10000, 99999);
	$this->MultiCell(60, 4, "Invoice Number:\nINV$rand pW",1);
	$this->SetXY($x + 60, $y);
	$this->MultiCell(60, 4, "Invoice Date:\n$invoice_date",1);
	$this->SetXY($x + 120, $y);
	
	$this->MultiCell(60, 4, "Reference Number:\n49415",1);
	$this->MultiCell(180, 3, "Buyer:$c_name\n
	Address:\n$c_address\n",1);
	
	$this->Ln(10);
}
	//Simple table
	function BasicTable($header,$data,$f_date,$t_date,$doc_name,$p_account,$d_account,$c_address,$c_name)
	{
		/*$this->Cell(20,10,"From:");
		$this->Cell(20,10,$f_date);
		$this->Cell(20,10,"To:");
		$this->Cell(20,10,$t_date);
		$this->Ln(10);
		$this->Cell(30,10,"Doctor's Name:");
		$this->Cell(40,10,ucwords($doc_name));
		$this->Ln(20);*/
		$this->SetFillColor(242,39,201);
		$this->SetTextColor(255);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		//$this->SetFillColor(0,255,0);
		//$this->SetDrawColor(128,0,0);
		//$w=array(40,20,35,30,50,15,10,10,15,15,15,15,15);
		$w=array(20,20,35,30,30,25,25,10,15,15,15,15,15);
		//Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		//Data
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		$total=0;$total_pw=0;$total_doc=0;
		$count=1;
		foreach ($data as $eachResult)
			{
			//$this->Cell(40,6,$eachResult["user_name"],1);
			$this->Cell(20,6,$count,1);
			$this->Cell(20,6,$eachResult["top_up_amount"],1);
			$this->Cell(35,6,ucwords($eachResult["user_name"]),1);
			if($eachResult["package_details"]=="Clinic Added packages"){
				$this->Cell(30,6,"Clinic",1);
			}else{$this->Cell(30,6,"Online",1);}
			$trns_date=date('Y-m-d',strtotime($eachResult["payment_date"]));
			$this->Cell(30,6,$trns_date,1);
			if($eachResult["package_details"]=="Clinic Added packages")
			{$total_pw+=0;
			$total_doc+=$eachResult["top_up_amount"];
				$this->Cell(25,6,'0',1);
				$this->Cell(25,6,$eachResult["top_up_amount"],1);
			}else{
				$total_doc+=0;
				$total_pw+=$eachResult["top_up_amount"];
				$this->Cell(25,6,$eachResult["top_up_amount"],1);
				$this->Cell(25,6,'0',1);
				}
			/*if($eachResult["owed_pw"]==0){
				$this->Cell(30,6,"---",1);
			}else{
			$this->Cell(30,6,$eachResult["owed_pw"],1);}
			if($eachResult["owed_doc"]==0){
				$this->Cell(50,6,"---",1);}
				else
				{$this->Cell(50,6,$eachResult["owed_doc"],1);}*/
			$total+=$eachResult["top_up_amount"];
			/*$total_pw+=$eachResult["owed_pw"];
			$total_doc+=$eachResult["owed_doc"];*/
			$this->Ln();
			$count++;
	}$this->SetFillColor(131,177,238);
	//$this->SetFillColor(255,204,229);
	$this->Cell(20,6,"Total",1,0,'C',true);
	$this->Cell(20,6,$total,1);
	$this->Cell(35,6,"",1);
	$this->Cell(30,6,"",1);
	$this->Cell(30,6,"",1);
	$this->Cell(25,6,$total_pw,1);
	$this->Cell(25,6,$total_doc,1);$this->Ln();
	$this->SetFillColor(131,177,238);
	//$this->SetFillColor(255,204,229);
	$this->Cell(20,6,"Should Get",1,0,'C',true);
	$this->Cell(20,6,"",1);
	$this->Cell(35,6,"",1);
	$this->Cell(30,6,"",1);$this->Cell(30,6,"",1);
	$per_pw=($total*.2);
	$this->Cell(25,6,$per_pw,1);
	$per_doc=($total*.8);
	$this->Cell(25,6,$per_doc,1);$this->Ln();
	//$this->SetFillColor(131,177,238);
	$this->SetFillColor(255,204,229);
	$this->Cell(20,6,"Settlement",1,0,'C',true);
	$this->Cell(20,6,"",1);
	$this->Cell(35,6,"",1);
	$this->Cell(30,6,"",1);$this->Cell(30,6,"",1);
	
	$t_pw=($total_pw-$per_pw);
	$t_doc=($total_doc-$per_doc);
	if($t_pw>$t_doc){//$this->SetFillColor(77,230,97);
		$this->SetFillColor(255,204,229);
	$this->Cell(25,6,($per_pw-$total_pw),1,0,'C',true);
	$this->SetFillColor(255,204,229);
	$this->Cell(25,6,($per_doc-$total_doc),1,0,'C',true);
	}else{
	//$this->SetFillColor(255,0,0);
		$this->SetFillColor(255,204,229);
		$this->Cell(25,6,($per_pw-$total_pw),1,0,'C',true);
		$this->SetFillColor(255,204,229);
		//$this->SetFillColor(77,230,97);
		$this->Cell(25,6,($per_doc-$total_doc),1,0,'C',true);
	}//$this->Cell(15,6,"",1);
	$this->Ln(20);
	$this->SetFont('Arial','BU',9);
	$this->Cell(80);
	$this->Cell(80,10,"SUMMARY");$this->Ln(20);$this->SetFont('Arial','',9);
	/*$this->Cell(60,6,"Amount Payable to CDEC by pW:");
	$this->Cell(20,6,($per_doc-$total_doc));$this->Ln(10);
	$this->Cell(60,6,"Amount Payable to pW by CDEC:");
	$this->Cell(20,6,($per_pw-$total_pw));*/
	$this->Cell(60,6,"Amount Payable to CDEC by pW:",1);
	$this->Cell(20,6,($d_account),1);$this->Ln(6);
	$this->Cell(60,6,"Amount Payable to pW by CDEC:",1);
	$this->Cell(20,6,($p_account),1);
	$this->Ln(6);
	$this->Cell(60,6,"Amount That Pinkwhale Deserves:",1);
	$this->Cell(20,6,($per_pw),1);
	$this->Ln(6);
	/*if(($per_pw-$total_pw)>($per_doc-$total_doc)){
		$this->Cell(60,6,"Total settlement Amount to Pink Whale:");
		$total_settle=($per_pw-$total_pw)+($per_doc-$total_doc);
		$this->Cell(20,6,$total_settle);}else{$this->Cell(60,6,"Total settlement Amount to Doctor:");
		$total_settle=($per_pw-$total_pw)+($per_doc-$total_doc);
		$this->Cell(20,6,$total_settle);}*/
	if($p_account>$d_account){
		$this->Cell(60,6,"Total settlement Amount to Pink Whale:",1);
		$total_settle=$p_account-$d_account;
		$this->Cell(20,6,$total_settle,1);
	$total_settle_tds=$total_settle-($total_settle*.1);
	//$num_tds=$this->convert_number_to_words($total_settle_tds);
	$this->Ln(6);
		$this->Cell(60,6,"Total Amount payable after TDS :",1);
		$this->Cell(20,6,$total_settle_tds,1);$this->Ln(6);
		//$this->Cell(60,6,"Total Amount Payable by DOCTOR is Rupees $num_tds Only");
		$this->Ln(20);}
	else{$this->Cell(60,6,"Total settlement Amount to Doctor:",1);
		$total_settle=$d_account-$p_account;
		$this->Cell(20,6,$total_settle,1);
		$total_settle_tds=$total_settle-($total_settle*.1);
		//$num_tds=$this->convert_number_to_words($total_settle_tds);
	$this->Ln(6);
		$this->Cell(60,6,"Total Amount payable after TDS :",1);
		$this->Cell(20,6,$total_settle_tds,1);$this->Ln(6);
		//$this->Cell(60,6,"Total Amount Payable by pinkWHALE is Rupees $num_tds Only");
		$this->Ln(10);}
}
/*function settle($p_account,$d_account,$total_settle)
	{
		$this->Ln(20);
		$this->Cell(60,6,"Total amount owed to Doctor:");
		$this->Cell(20,6,$d_account);$this->Ln(10);
		$this->Cell(60,6,"Total amount owed to P.W:");
		$this->Cell(20,6,$p_account);
		$this->Ln(20);
		if($p_account>$d_account){
		$this->Cell(60,6,"Total settlement Amount to Pink Whale:");
		$total_settle=$p_account-$d_account;
		$this->Cell(20,6,$total_settle);}else{$this->Cell(60,6,"Total settlement Amount to Doctor:");
		$total_settle=$d_account-$p_account;
		$this->Cell(20,6,$total_settle);
		
		}
}*/
function b_details($pan_number)
{$this->Ln(2);
	$this->Cell(70,6,"Income Tax Permanent Account Number (PAN): $pan_number");$this->Ln(6);
	$this->Cell(70,6,"Service Tax Code (STC) Number: ");$this->Ln(20);
	$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(15);
	$this->MultiCell(70, 4,"Anita Shet\n
Authorized Signatory
			");
}


}

$pdf=new PDF();
$header=array('Sl.No:','Amount:','Patient Name','Mode','Date','With P.W','With Doctor');
//Data loading
//*** Load MySQL Data ***//
$sum=0;
if($mode="all"){
	$re_admin=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''" );

	$re1=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc AND package_details='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	$re2=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc AND package_details!='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	
}
$p_account=0;
$d_account=0;
for ($i=0;$i<mysql_num_rows($re1);$i++) {
	$result1 = mysql_fetch_array($re1);
	$p_account+=$result1['top_up_amount'];
}
$p_account=$p_account*.2;
for ($i=0;$i<mysql_num_rows($re2);$i++) {
	$result2 = mysql_fetch_array($re2);
	$d_account+=$result2['top_up_amount'];
}
$d_account=$d_account*.8;
$resultData = array();
for ($i=0;$i<mysql_num_rows($re_admin);$i++) {
	$result = mysql_fetch_array($re_admin);
	if($result['package_details']=='Clinic Added packages'){
	$result['owed_doc']=0;
	$amount=($result['top_up_amount']*.2);
	$result['owed_pw']=$amount;
	$result['recieved_at']="clinic";}
	else{
		$result['owed_pw']=0;
	$amount=($result['top_up_amount']*.8);
	$result['owed_doc']=$amount;
	$result['recieved_at']="P.W";}
	array_push($resultData,$result);
}
function forme()

{
	$d=date('d_m_Y');
	echo "PDF generated successfully. To download document click on the link >> <a href=".$d.".pdf>DOWNLOAD</a>";
}


//$pdf->SetFont('Arial','',6);
//*** Table 1 ***//
$pdf->AddPage();
$pdf->Ln(0);
$pdf->doc_details($f_date,$t_date,$doc_name);

$pdf->reference_table($c_name,$c_address);
$pdf->BasicTable($header,$resultData,$f_date,$t_date,$doc_name,$p_account,$d_account,$c_address,$c_name);
$pdf->b_details($pan_number);
//$pdf->AddPage();
/*$pdf->pink_amount($p_account);
$pdf->doc_amount($d_account,$total_settle);*/
//$pdf->settle($p_account,$d_account, $total_settle);
forme();
$pdf->Output("$d.pdf","F");

?>