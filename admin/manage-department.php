<?php 
error_reporting(E_PARSE);
session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
		{
			header("Location: ../index.php");
			exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>pinkwhalehealthcare</title>
    <meta name="description" content="pinkwhalehealthcare">
    <link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
    <link href="css/my_css.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/add-doctor-validation.js"></script>
    <script type="text/javascript" src="js/delete-speciality.js"></script>
    <script type="text/javascript" src="../js/registration_validation.js"></script>
    
    <style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
</head>
<body onLoad="init_table();">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" >
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
?>
	<form action="actions/delete_department.php" name="delete_speciality_form" id="delete_speciality_form" method="post">
		<table width='500' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="5" >Manage Departments</th></tr>
		<?php		

			$count = 0;
			// Display all the data from the table 
			$sql="SELECT * FROM `add_department`";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 10, 3, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) die(mysql_error());
		?>
		<tr>
            <td align='left'><strong>SI No:</strong> </td>
            <td align='left'><strong>Speciality</strong></td>
            <td align='left'><strong>Select</strong></td>
        </tr>
		<?php	
			while($row = mysql_fetch_assoc($rs)) 
			{
				$id=$row['department_id'];
				$added_speciality=$row['added_department'];
			     $count ++;
				echo "<tr bgcolor='#ffffff'>";
				echo "<td>$count </td> ";
				echo "<td>$added_speciality </td> ";
				echo "<td><input type='checkbox' id='delete_id' name='delete[]' value=". $id ." /></td>";
				echo "</tr>";             			
			}
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>','</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
            <tr><td colspan='5' align='center'>
			<?php
            echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
			</table>
     <div style="float:right; margin-right:150px; height:50px;">
      	<input name="delete_speciality" type="button" id="delete_speciality" onmouseover="this.style.cursor='pointer';" value="Delete" onclick="delete_specialities_msg();" />
     </div>
    
     </form>
</td>
</tr>
</table>
<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	//enable_doc_submenu();
        enable_department_submenu();
</script>
</body></html>
