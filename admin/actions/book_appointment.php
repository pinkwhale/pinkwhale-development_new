<?php
ob_start();
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin'){
        header("Location: ../../index.php");
        exit();
}

    include ("../../db_connect.php");
    
    include "send_mail.php";

    $logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/test/images/pinkwhale_logo.jpg'/>";
    $doc_id = mysql_escape_string(trim($_POST['doc_id']));
    $doctor_name = mysql_escape_string(($_POST['doc_name']));
    $patient_name =  mysql_escape_string(trim($_POST['p_name']));
    $age =  mysql_escape_string(trim($_POST['age']));
    $email =  mysql_escape_string(trim($_POST['email']));
    $mob = mysql_escape_string(trim($_POST['mob']));
    $gender = mysql_escape_string(trim($_POST['gender']));

    
    $appointment_time = $_POST['time_slot'];
    
    
    $display_time = $_POST['display_time_slot'];
    $app_time = $_POST['app_time'];
    $app_date = $_POST['app_date'];
    $app_date = strftime ( '%e %b %Y', strtotime($app_date) );
    $app_id = $_POST['app_id'];
    $new_patient = $_POST['new_patient'];
    $new_for_system = $_POST['new_for_system'];
    $clinic_id = $_POST['clinic_id']; 
    
    $time_slot_param_string = $_POST['time_slot_param'];
    
    $time_slot_param_array=explode(',', $time_slot_param_string);
    
    foreach ($time_slot_param_array as $key => $value) {
    	$time_slot_param .="'".$value."',";
    }
    
    $time_slot_param = trim($time_slot_param, ",");
    
    
    
    $token_count = 0;
    do{
        $token = rand(1111,5555);
        $qry  = "select token_id from Appointment_book_details where token_id='$token'";
        $res = mysql_query($qry);
        $dat = mysql_fetch_array($res);
        if($dat['token_id']==""){
            $token_count ++;
        }
        
    }while($token_count==1);
    
    
    
    $qqry = "select from_time from doctor_appointment_cancel where doc_id='$doc_id' and clinic_id='$clinic_id' and CONCAT(`cancel_date`, ' ', `from_time`) in ($time_slot_param) and status=1";
    $r = mysql_query($qqry);
    $d = mysql_num_rows($r);
    if($d==0){
        
        $qqry1 = "select status from Appointment_book_details where doc_id='$doc_id' and clinic_id='$clinic_id' and from_time in ($time_slot_param) and status in (2,1)";
        $r1 = mysql_query($qqry1);
        $d1 = mysql_num_rows($r1); 
        if($d1>0){
        
            $query = "update Appointment_book_details set new_patient=$new_patient,new_for_system=$new_for_system,patient_name='$patient_name',token_id='$token' , email='$email' , mobile='$mob', gender='$gender' , age='$age' ,status='2' where doc_id='$doc_id' and clinic_id='$clinic_id' and from_time in ($time_slot_param)";       
            $res = mysql_query($query);
            if($res){


                $display = "<table border='0' cellpadding='0' cellspacing='1' width='500' align='center' class='s90registerform'>";

                $display .=  "<tr><th colspan=\"2\" bgcolor=\"#F5F5F5\">Booked Details</th></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Doctor Name &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$doctor_name</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Appointment Time &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$display_time</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Clinic Address &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">";

                $qry = "select name,address,country,state,city,pin_zip,phone_no from clinic_details where clinic_id='$clinic_id'";

                $res = mysql_query($qry);

                if($dat = mysql_fetch_array($res)){

                    $display .= $dat['name']."<br />";

                    $display .= $dat['address']."<br />";

                    $display .= $dat['city']."-".$dat['pin_zip']."<br />";

                    $display .= $dat['state']."<br />";

                    $display .= $dat['country']."<br />";

                }

                $display .= "</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Patient Name &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$patient_name</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Token-id &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$token</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Age &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$age</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Gender &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$gender</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Email &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$email</td></tr>";

                $display .= "<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Phone No. &nbsp;:&nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\">$mob</td></tr>";

                $display .= "</table>";



                $details = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                            <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                            <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($patient_name).",</div>
                            <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Your Appointment Details</div>
                            <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Doctor Name :</div> {$doctor_name}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Appointment Time :</div> {$display_time}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Clinic Address :</div>{$dat['name']},{$dat['address']},{$dat['city']}-{$dat['pin_zip']}\n{$dat['state']}<br />{$dat['country']}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Token-id :</div> {$token}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>patient Name :</div> {$patient_name}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Age :</div> {$age}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Email :</div> {$email}</div>
                            <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Phone No. :</div> {$mob}</div>
                            <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>                        
                            </div>
                            <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                            <b>Pinkwhale Healthcare</b>
                            </div>";



	            sendmail($email, $details, "Appointment Booked Confirmation");

                    $url = "http://203.200.1.116/smsbroker/alertsms.php?";

                    $url .= "user=pinkwhale&pass=pinkwhale&";

                    $url .= "to=$mob";

                    $url .="&from=PWCARE&msg=Dear";                

                    $url .= "%20".str_replace(" ","%20",$patient_name).",%20ur%20Apt%20at%20".str_replace(" ","%20",$dat['name'])."%20with%20".str_replace(" ","%20",$doctor_name)."%20on%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$app_time)."%20is%20booked.%20Your%20Token%20no%20is%20".str_replace(" ","%20",$token).".";

                    $url .= "&msgopt=0&priority=2";

                    $fetched = file_get_contents($url);

                    $content = $fetched;

                
	


                $_SESSION['msg']=$display;             
                header("Location: ../book_patient_appointment.php");
            }else{
                $_SESSION['error']="Failed to Book appointment";             
                header("Location: ../book_patient_appointment.php");
            }
        }else{
            
        }
    }else{
         $_SESSION['error']="Appointent Time slots are cancelled ,Please book for another time slot ";             
         header("Location: ../book_patient_appointment.php");
    }
    
ob_end_flush();




?>

