<?php
error_reporting(E_PARSE); 
session_start();

if($_SESSION['username']!="" && $_SESSION['login']=="admin"){
    include '../../db_connect.php';

    $clinic_name = mysql_escape_string(trim($_POST['cname']));
    $clinic_address = mysql_escape_string(trim($_POST['address']));
    $country = mysql_escape_string($_POST['country']);
    $state = mysql_escape_string($_POST['state']);
    $city = mysql_escape_string($_POST['city']);
    $pin = mysql_escape_string($_POST['pin']);
    $no_of_doc = mysql_escape_string($_POST['num_doc']);
    $phone =mysql_escape_string( $_POST['phone']);
    $email = mysql_escape_string($_POST['email']);
    $late =mysql_escape_string( $_POST['late']);
    $longa = mysql_escape_string($_POST['longa']);
    $all = mysql_escape_string($_POST['all']);
    $top_up = mysql_escape_string($_POST['top_up']);
    $appointment_booking = mysql_escape_string($_POST['appointment_booking']);
    $diagnostic_consultation = mysql_escape_string($_POST['diagnostic_consultation']);
    if($top_up){
    	$type = $top_up;
    }
    
    if($appointment_booking){
    	$type = $appointment_booking;
    }
    
    if($diagnostic_consultation){
    	$type = $diagnostic_consultation;
    }
    
    if($all){
    	$type = $all;
    }
    
    if($top_up && $appointment_booking){
    	$type = '4';
    }
    
    if($appointment_booking && $diagnostic_consultation){
    	$type = '5';
    }
    
    if($diagnostic_consultation && $top_up){
    	$type = '6';
    }
    
    if($diagnostic_consultation && $top_up && $appointment_booking){
    	$type = '0';
    }
    
    /*-------------------------------    GENERATE DOCTOR ID NUMBER     ------------------------------------------*/
    $id_count=0;
    do
    {
             $card_number = rand(10000,99999);
             $qry="select clinic_id from clinic_details where clinic_id='$card_number'";             
             if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
             if(mysql_fetch_array($qry_rslt)<1)
             {
                     $id_count++;
             }
    }
    while ($id_count<=0);
    $ref_no=$card_number;

    /*-------------------------------    END GENERATE DOCTOR ID NUMBER     ------------------------------------------*/

    
    /*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&   upload photo  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/

	if(($_FILES["logo"]["name"]))
	{	
		$allowed_filetypes = array('.jpg','.gif','.bmp','.png','.jpeg','.JPG','.jfif');
		$filename1 = $_FILES["logo"]["name"];
		$ext1= substr($filename1, strpos($filename1,'.'), strlen($filename1)-1);
		$filetype1=explode(".", $filename1);
		if(!in_array($ext1,$allowed_filetypes) || $_FILES["logo"]["size"] > 5000000){                    
                        $_SESSION['error']="Improper file Type or Size of Upload image";
			header("Location: ../add_clinic.php");
			exit();
		}
		move_uploaded_file($_FILES["logo"]["tmp_name"],"../../images/clinic_logo/" . $ref_no."-photo.".$filetype1[1]);
		$upload_photo="images/clinic_logo/" . $ref_no."-photo.".$filetype1[1];
	}
    /*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  end   &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
   
	$qry = "insert into clinic_details(clinic_id,name,address,country,state,city,pin_zip,phone_no,email,latitude,longitude,num_of_doc,entered_date,logo,type) values('$ref_no','$clinic_name','$clinic_address','$country','$state','$city','$pin','$phone','$email','$late','$longa','$no_of_doc',now(),'$upload_photo','$type')"; 
    //echo $qry;
    
    $result = mysql_query($qry);
    if($result){
        $_SESSION['msg']="Successfully Added Clinic";
        header("Location: ../add_clinic.php");
    }else{
        $_SESSION['error']="Failed to add Clinic";
        header("Location: ../add_clinic.php");
    }

}else{    
    header("Location: ../../index.php");    
}

ob_end_flush();
?>

