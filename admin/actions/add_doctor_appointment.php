<?php
ob_start();
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin'){
        header("Location: ../../index.php");
        exit();
}

    include ("../../db_connect.php");
    
    $doc= mysql_escape_string($_POST['doctor']);
    $doctor_details = explode("|", $doc);
    $doc_id = $doctor_details[0];
    $doc_name = $doctor_details[1];
    
    $cli= mysql_escape_string($_POST['clinic']);
    $clinic_details = explode("|", $cli);
    $clinic_id = $clinic_details[0];
    $clinic_name = $clinic_details[1];
	
	$from_date=$_POST['date2'];
	$to_date=$_POST['date4'];
	
	// set the future flag here
		
	if($from_date!='0000-00-00' || $to_date !='0000-00-00'){
$future = 1;
}else{
$future = 0;
}

    
    $day = $_POST['day'];
    /** Admin Added Packages  **/
    $admin_id = 1;
    $msg = "";
    $msg_count = 0;
    
    //echo $msg_count;exit(1);
    $doc_appoint_count = 0;
        
    /*************************** Days loop for inserting AM time Slots ***********************************/
        
    for ($x=1; $x<=4; $x++)
    {
    	
    	if(isset($_POST['fromtime_slot'.$x])){
    		$ftime=$_POST['fromtime_slot'.$x];
    		if(strlen($ftime)==7){ $ftime="0".$_POST['fromtime_slot'.$x];}
    	}else{
    		$ftime=="";
    	}
    	
    	if(isset($_POST['totime_slot'.$x])){ 
	    	$ttime=$_POST['totime_slot'.$x];
	    	if(strlen($ttime)==7){ $ttime="0".$_POST['totime_slot'.$x];}
    	}else{
    		$ttime="";
    	}
    	
    	if(isset($_POST['category'.$x])){
    		$category = $_POST['category'.$x];
    	}else{
    		$category ="0";
    	}
    	
    	if(isset($_POST['app_duration_slot'.$x])){
    		$app_duration_slot=$_POST['app_duration_slot'.$x];
    	}else{
    		$app_duration_slot="0";
    	}
    	
    	if(isset($_POST['na_slot'.$x])){
    		$na_slot = $_POST['na_slot'.$x];
    	}else{
    		$na_slot ="";
    	}
    	
    	switch ($x){
    		case 1: $day_type="morning";break;
    		case 2: $day_type="beforenoon";break;
    		case 3: $day_type="afternoon";break;
    		case 4: $day_type="evening";break;
    	}
    
     if($ftime!="" && $ttime!="" && $category!=""){
        	
        	$day = $_POST['day'];
        	
        	/*added by swathi for saving category and type  */
        	
        		if($category=="3")
        		{
        			$apt_type=$_POST['app_type'.$x];
        		}else{
        			$apt_type="0";
        		}
        	
        	if($na_slot!=""){
        		
        		$remove_slots1_count=0;
        		
        		while (list ($key,$val_day) = @each ($day)) {
	        		$slots_qry = "select * from doctor_clinic_avail_time_slots where doc_id =".$doc_id." and 
					clinic_id=".$clinic_id." and Day=".$val_day." and day_type='$day_type'";
	        		
	        		$slots_res = mysql_query($slots_qry);
	        		$slots_data = mysql_fetch_array($slots_res);
	        		
	        		$doctor_slots_qry = "select abd.id,abd.from_time,patient_name,email from doctor_time_slots dts, 
					Appointment_book_details abd where abd.doc_id = dts.doc_id and abd.clinic_id=dts.clinic_id and
					dts.Day = abd.Day and DATE_FORMAT(dts.from_time,'%H:%i:%s')=DATE_FORMAT(abd.from_time,'%H:%i:%s')
					and abd.from_time > now() and day_type='$day_type' and dts.doc_id =".$doc_id." 
					and dts.clinic_id=".$clinic_id." and dts.Day=".$val_day." and 
					dts.day_type='".$slots_data['day_type']."' and dts.apt_category=".$slots_data['apt_category']." and
					dts.patient_apt_type=".$slots_data['patient_apt_type']." and abd.status in (1,2) group by abd.token_id";
					
	        		$doctor_slots_res = mysql_query($doctor_slots_qry);
	        		
	        		//$doctor_slots_data = mysql_fetch_array($doctor_slots_res);
	        		$num=mysql_num_rows($doctor_slots_res);
	        		
	        		if ($doctor_slots_data = mysql_fetch_array($doctor_slots_res)){
	        			$_SESSION['error']="Failed to update Doctor clinic Avail Slot 1 for ".getday($i).". There are appointment bookings.";
                		header("Location: ../add-appointment.php");
                		exit();
	        		}else{
	        			$delete_doctor_slots_qry = "delete from doctor_time_slots  where doc_id =".$doc_id." and
						clinic_id=".$clinic_id." and Day=".$val_day." and day_type='$day_type'";
	        			 
	        			$delete_doctor_slots_res = mysql_query($delete_doctor_slots_qry);
	        			
	        			if ($delete_doctor_slots_res)
	        			{
	        				 
	        			}
	        			
	        			$delete_doc_slots_qry = "delete from doctor_clinic_avail_time_slots  where doc_id =".$doc_id." 
						and clinic_id=".$clinic_id." and Day=".$val_day." and  day_type='$day_type'";
	        			
	        			$delete_doc_slots_qry_res = mysql_query($delete_doc_slots_qry);
	        			if ($delete_doc_slots_qry_res)
	        			{
	        			
	        			}
	        		}
        		}
        		
        		if($remove_slots1_count==0){
        			
        		}
        		
        	}else{
            
            /*End added by swathi for saving category and type  */
				while (list ($key,$val_day) = @each ($day)) {
                
                $qry1 = "select Day from doctor_clinic_avail_time_slots where doc_id='$doc_id' and clinic_id='$clinic_id' 
				and Day=$val_day and day_type='$day_type'";
                
                $res1 = mysql_query($qry1);
                
                $dataa = mysql_fetch_array($res1);
                                 
                
                if($dataa['Day']==''){

                    /* Inserting doctors timing ----Morning  */
                    $qry_doc_app_avail = "insert into doctor_clinic_avail_time_slots 
					(admin_id, doc_id, clinic_id, Day, from_time, to_time, entered_date,
					day_type,apt_category,patient_apt_type,appointment_duration, future_date_flag)
					values('$admin_id','$doc_id','$clinic_id','$val_day','".$ftime."','".$ttime."',now(),
					'$day_type','$category','$apt_type','$app_duration_slot','$future')";

                    $res_doc_app_avail = mysql_query($qry_doc_app_avail);					
						
                    if($res_doc_app_avail){
										

                    }else{
                         $_SESSION['error']="Failed to add Doctor clinic Avail Slot ";
                         header("Location: ../add-appointment.php");
                         exit();
                    }

                    /*  Loop for inserting morning time Slots  */

                    $time_slots_am= $_POST['slot_time'.$x];

                    while (list ($key,$val_time_slot_am) = @each ($time_slots_am)) { 

                        $time = get_time($val_time_slot_am);					
						

                        if($future==0){

                        $qry = "insert into doctor_time_slots (admin_id,doc_id,doc_name,clinic_id,clinic_name,Day,from_time,
						entered_date,day_type,apt_category,patient_apt_type,from_date, to_date, future_date_flag) 
						values('$admin_id','$doc_id','$doc_name','$clinic_id','$clinic_name','$val_day','".$time."',now(),
						'$day_type','$category','$apt_type','$from_date','$to_date','0')";

                        $res = mysql_query($qry) or die("error while inserting");
						
						if($res){
                            $doc_appoint_count ++;
                        }
						
						}
						if($future==1){
						 $qry = "insert into doctor_future_time_slots (admin_id,doc_id,doc_name,clinic_id,clinic_name,Day,
						 from_time,entered_date,day_type,apt_category,patient_apt_type,from_date, to_date, future_date_flag)
						 values('$admin_id','$doc_id','$doc_name','$clinic_id','$clinic_name','$val_day','".$time."',now(),
						 '$day_type','$category','$apt_type','$from_date','$to_date','1')";

                        $res = mysql_query($qry) or die("error while inserting");
						

                        if($res){
                            $doc_appoint_count ++;
                        }
						}
                    }
                    
                }else{
                	
                	$slots1_qry = "select * from doctor_clinic_avail_time_slots where doc_id='$doc_id' and 
					clinic_id='$clinic_id' and Day=$val_day and day_type='$day_type'";
                	 
                	$slots1_res = mysql_query($slots1_qry);
                	$slots1_data = mysql_fetch_array($slots1_res);
                	 
                	$doctor_slots1_qry = "select abd.id, abd.from_time, patient_name, email from doctor_time_slots dts,
					Appointment_book_details abd where abd.doc_id = dts.doc_id and abd.clinic_id=dts.clinic_id 
					and dts.Day = abd.Day and DATE_FORMAT(dts.from_time,'%H:%i:%s')=DATE_FORMAT(abd.from_time,'%H:%i:%s')
					and abd.from_time > now() and day_type='$day_type' and dts.doc_id =".$doc_id." and 
					dts.clinic_id=".$clinic_id." and dts.Day in ($val_day) and dts.day_type='".$slots1_data['day_type']."' 
					and dts.apt_category='".$slots1_data['apt_category']."' and 
					dts.patient_apt_type ='".$slots1_data['patient_apt_type']."' and abd.status in (1,2) group by abd.token_id";
					
                	$doctor_slots1_res = mysql_query($doctor_slots1_qry);
                	 
                	$num_slot1=mysql_num_rows($doctor_slots1_res);
                	 
                	//check if appointment has been booked
                	if($doctor_slots1_data = mysql_fetch_array($doctor_slots1_res)){
                		 
                		$_SESSION['error']="Failed to update Doctor clinic Avail Slot 1 for ".getday($val_day).". There are appointment bookings.";
                		header("Location: ../add-appointment.php");
                		exit();
                		 
                	}else{
                    
                    /* Updating doctors timing ----Morning  */
					 
                     $qry_doc_app_avail = "update doctor_clinic_avail_time_slots set admin_id='$admin_id',
					 doc_id='$doc_id' , clinic_id= '$clinic_id', Day= '$val_day', from_time='".$ftime."', to_time='".$ttime."',
					 entered_date=now(), apt_category='$category',patient_apt_type='$apt_type',appointment_duration ='$app_duration_slot'
					 where doc_id='$doc_id' and clinic_id= '$clinic_id' and Day= '$val_day' and day_type='$day_type' and 
					 future_date_flag='$future' ";

                     $res_doc_app_avail = mysql_query($qry_doc_app_avail);				
					
					//var_dump($id);exit;
					 
                    if($res_doc_app_avail){

                    }else{
                         $_SESSION['error']="Failed to update Doctor clinic Avail Slot ";
                         header("Location: ../add-appointment.php");
                         exit();
                    }  

						
					 // insert the future time slots in the new table
					 /*
					{
						
						$qry_doc_future_date = "insert into doctor_clinic_future_time_slots
							(doctor_clinic_avail_time_slots_id, from_time, to_time, from_date, to_date, entered_date)
							values ('$id','".$ftime."','".$ttime."','".$from_date."', '".$to_date."',now())";
							$res_qry_doc_future_date= mysql_query($qry_doc_future_date);
							//var_dump($res_qry_doc_future_date);exit;
						
					}
					*/
					

						
                    /*  Loop for Deleting Morning time Slots  */
                    
                    
                    //$qry = "delete from doctor_time_slots where doc_id='$doc_id' and clinic_id= '$clinic_id' and Day= '$val_day' and day_type='$day_type' ";

                    //$res = mysql_query($qry) or die("error while deleting");
					
					
                    /*  Loop for inserting morning time Slots  */

                    $time_slots_am= $_POST['slot_time'.$x];

                    while (list ($key,$val_time_slot_am) = @each ($time_slots_am)) { 

                        $time = get_time($val_time_slot_am);
						if($future==0){

                        $qry = "insert into doctor_time_slots (admin_id,doc_id,doc_name,clinic_id,clinic_name,Day,from_time,
						entered_date,day_type,apt_category,patient_apt_type,from_date, to_date, future_date_flag) 
						values('$admin_id','$doc_id','$doc_name','$clinic_id','$clinic_name','$val_day','".$time."',now(),
						'$day_type','$category','$apt_type','$from_date','$to_date','0')";

                        $res = mysql_query($qry) or die("error while inserting");
						
						if($res){
                            $doc_appoint_count ++;
                        }
						
						}
						if($future==1){
						 $qry = "insert into doctor_future_time_slots (admin_id,doc_id,doc_name,clinic_id,clinic_name,Day,
						 from_time,entered_date,day_type,apt_category,patient_apt_type,from_date, to_date, future_date_flag)
						 values('$admin_id','$doc_id','$doc_name','$clinic_id','$clinic_name','$val_day','".$time."',now(),
						 '$day_type','$category','$apt_type','$from_date','$to_date','1')";

                        $res = mysql_query($qry) or die("error while inserting");					
						

                        if($res){
                            $doc_appoint_count ++;
                        }
						}
                    }
                    
                	}
                }
              }
        	} 
        }
      }
         
        if($doc_appoint_count==0){
             $_SESSION['msg']="Updated Time Slot";             
             header("Location: ../add-appointment.php");
        }else{
             $_SESSION['msg']="Successfully Added Time Slot";
             header("Location: ../add-appointment.php");
        }
    
ob_end_flush();
//date formating code

function get_time($tt){
    //echo $tt."<br />";
    $t = explode(":",$tt);
    $h = "";
    $m = "";
    
    //echo strlen($t[0])." checkin ".$t[0]."<br />";
    
    if(strlen($t[0])==1){
         $h = "0".$t[0];
    }else{
        $h = $t[0];
    }
     
    if(strlen($t[1])==1){
         $m = "0".$t[1];
    }else{
        $m = $t[1];
    }
    
    $time = "0000-00-00 ".$h.":".$m.":"."00";   
    
    return $time;
}

function getday($i){
    $val = "";
    switch($i){
        case 1: $val = "Monday";break;
        case 2: $val = "Tuesday";break;
        case 3: $val = "Wednesday";break;
        case 4: $val = "Thursday";break;
        case 5: $val = "Friday";break;
        case 6: $val = "Saturday";break;
        case 7: $val = "Sunday";break;
    }
    return $val;
}
?>