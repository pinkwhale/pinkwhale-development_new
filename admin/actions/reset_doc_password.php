<?php
ob_start();
error_reporting(E_PARSE);
session_start();
include '../../db_connect.php';
include "../../actions/encdec.php";
require_once "send_mail.php";
$doc_type = $_POST['doc_type'];
$doctor_id = $_POST['doctor_list'];
$doc_emailid = $_POST['doc_emailid'];
$doc_password = generatePassword(8);
$regPassword = encode_base64($doc_password);

if ($doc_password == '') {
    header("Location: ../doc-login-control.php");
    exit;
}

$qry1 = mysql_query("UPDATE `pw_doctors` SET `doc_email_id`='$doc_emailid',`password`='$regPassword' where `doc_id`='$doctor_id' ");
if (!$qry1) {
    exit("error in inserting value");
} else {
    
    $result = mysql_query("select doc_name from pw_doctors where `doc_id`='$doctor_id'");
    $data  =mysql_fetch_array($result);
    $doc_name = $data['doc_name'];
    
    $logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/images/pinkwhale_logo.jpg'/>";
    
    $subject	= "Pinkwhalehealthcare login credentials";
    
    $details = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($doc_name).",</div>
                <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Your login credentials in www.pinkwhalehealthcare.com</div>
                <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Username :</div> <div style='float:left;width:18%;'>{$doc_emailid}</div></div>
                <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Password :</div> <div style='float:left;width:18%;'>{$doc_password}</div></div>
                <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>
                Please login to your account at <a href='http://pinkwhalehealthcare.com'>http://pinkwhalehealthcare.com</a>
                </div>
                <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                <b>Pinkwhale Healthcare</b>
                </div>";
    
            //$sent = sendmail($doc_emailid,$details,$subject);

	    //sleep(60);
	
	    $to = $doc_emailid;

	    $login_query = "select admin_email from login_user where admin_id=1";
	    $login_result = mysql_query($login_query);
	    $login_data = mysql_fetch_array($login_result);
	    $admin_email_id = $login_data['admin_email'];

	    $to	=",".$admin_email_id;

	    send_multimail($to,$details,$subject);
    
    
    header("Location: ../package_saved.php?data_var=Password Reset Successful,Password is sent to Doctors mail-id");
}

ob_end_flush();

function generatePassword($length) {

    // start with a blank password
    $password = "";

    // define possible characters - any character in this string can be
    // picked for use in the password, so if you want to put vowels back in
    // or add special characters such as exclamation marks, this is where
    // you should do it
    $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

    // we refer to the length of $possible a few times, so let's grab it now
    $maxlength = strlen($possible);

    // check for length overflow and truncate if necessary
    if ($length > $maxlength) {
        $length = $maxlength;
    }

    // set up a counter for how many characters are in the password so far
    $i = 0;

    // add random characters to $password until $length is reached
    while ($i < $length) {

        // pick a random character from the possible ones
        $char = substr($possible, mt_rand(0, $maxlength - 1), 1);

        // have we already used this character in $password?
        if (!strstr($password, $char)) {
            // no, so it's OK to add it onto the end of whatever we've already got...
            $password .= $char;
            // ... and increase the counter by one
            $i++;
        }
    }

    // done!
    return $password;
}

?>