<?php
ob_start();
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin'){
        header("Location: ../../index.php");
        exit();
}

include "../../db_connect.php";

include "send_mail.php";

$TOKEN = $_POST['token'];
$logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/test/images/pinkwhale_logo.jpg'/>";
$ID = $_POST['appid'];
$clinic_name=$_POST['clinic_name'];
$clinic_address=$_POST['clinic_add'];
$clinic_city=$_POST['clinic_city'];
$clinic_state=$_POST['clini_state'];
$clinic_country=$_POST['clini_country'];
$clinic_pin=$_POST['clini_pin'];
$clinic_email=$_POST['clini_email'];
$clinic_mob=$_POST['clini_mobile'];
$doc_name=$_POST['doc_name'];
$app_date=$_POST['app_date'];
//$app_date = strftime ( '%b %a %e', strtotime($appointment_date) );
$appointment_time=$_POST['app_time'];
$p_name=$_POST['p_name'];
$p_age=$_POST['p_age'];
$p_email=$_POST['p_email'];
$p_mob=$_POST['p_mobile'];
$pw_mobile = get_pw_moile();

$qry1 = "select status from Appointment_book_details where id='$ID' and token_id='$TOKEN'";

$resq = mysql_query($qry1);

$num = mysql_num_rows($resq);

if($num>0){
    
    $data = mysql_fetch_array($resq);
    
    if($data[0]==2){

        $qry = "update Appointment_book_details set status='3' where token_id='$TOKEN'";

        $res = mysql_query($qry);

        if($res){
            
            
            $details_patient = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($p_name).",</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Your Appointment Cancelled Details</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Doctor Name :</div> {$doc_name}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Appointment Time :</div>{$app_date} {$appointment_time}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Clinic Address :</div>{$clinic_name}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_address},</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_city}-{$clinic_pin}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_state}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_country}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Token-id :</div> {$TOKEN}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>patient Name :</div> {$p_name}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Age :</div> {$p_age}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Email :</div> {$p_email}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Phone No. :</div> {$p_mob}</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>                        
                        </div>
                        <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                        <b>Pinkwhale Healthcare</b>
                        </div>";
    
			/**************Patient Email *******************/
                    
                    sendmail($p_email, $details_patient, "Appointment Cancellation");
                    
                    /***********     Patient SMS URL  Start       *************/

                    $url = "http://203.200.1.116/smsbroker/alertsms.php?";

                    $url .= "user=pinkwhale&pass=pinkwhale&";

                    $url .= "to=$p_mob";

                    $url .="&from=PWCARE&msg=";                

                    $url .="Dear%20".str_replace(" ","%20",$p_name).",%20ur%20Apt%20at%20".str_replace(" ","%20",$clinic_name)."%20with%20".str_replace(" ","%20",$doc_name)."%20on%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time)."%20Token%20".str_replace(" ","%20",$TOKEN)."%20stands%20cancelled%20as%20per%20your%20request.&msgopt=0&priority=2"; 
                    
                    $url .= "&msgopt=0&priority=2";

                    $fetched = file_get_contents($url);

                    $content = $fetched;
                    
                    /***********     Patient SMS URL  End       *************/
                
                
                
                $details_clinic = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($clinic_name).",</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Appointment Cancelled Details for {$doc_name}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Doctor Name :</div> {$doc_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Appointment Time :</div>{$app_date} {$appointment_time}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Clinic Address :</div>{$clinic_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_address},</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_city}-{$clinic_pin}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_state}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_country}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Token-id :</div> {$TOKEN}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>patient Name :</div> {$p_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Age :</div> {$p_age}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Email :</div> {$p_email}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Phone No. :</div> {$p_mob}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>                        
                                    </div>
                                    <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                                    <b>Pinkwhale Healthcare</b>
                                    </div>";
                
                
                   /***********     Clinic Admin SMS URL  Start       *************/
                
               // if(sendmail($clinic_email, $details_clinic, "Appointment Cancellation")){                  
                 
                    $url = "http://203.200.1.116/smsbroker/alertsms.php?";

                    $url .= "user=pinkwhale&pass=pinkwhale&";

                    $url .= "to=$clinic_mob";

                    $url .="&from=PWCARE&msg=";                

                   // $url .="Dear%20".str_replace(" ","%20",$p_name).",%20ur%20Apt%20at%20".str_replace(" ","%20",$clinic_name)."%20with%20".str_replace(" ","%20",$doc_name)."%20on%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time)."%20Token%20".str_replace(" ","%20",$TOKEN)."%20stands%20cancelled%20as%20per%20your%20request.&msgopt=0&priority=2"; 
		    $url .="Dear%20Clinic%20Admin,FYR%20PW%20admin%20has%20cancelled%20appointment%20bearing%20token%20".str_replace(" ","%20",$TOKEN)."%20with%20".str_replace(" ","%20",$doc_name)."%20for%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time).".&msgopt=0&priority=2";                    
 
                    $url .= "&msgopt=0&priority=2";

                    $fetched = file_get_contents($url);

                    $content = $fetched;
               // }   
                    /***********     Clinic Admin SMS URL  End       *************/
                
                
                $details_admin = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear Pinkwhale Admin,</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Appointment Cancelled Details for {$doc_name}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Doctor Name :</div> {$doc_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Appointment Time :</div>{$app_date} {$appointment_time}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Clinic Address :</div>{$clinic_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_address},</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_city}-{$clinic_pin}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_state}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'></div>{$clinic_country}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Token-id :</div> {$TOKEN}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>patient Name :</div> {$p_name}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Age :</div> {$p_age}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Email :</div> {$p_email}</div>
                                    <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Phone No. :</div> {$p_mob}</div>
                                    <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>                        
                                    </div>
                                    <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                                    <b>Pinkwhale Healthcare</b>
                                    </div>";
                
                    
                    /***********     Pinkwhale admin SMS URL  Start       *************/
                    
               //if(sendmail("info@pinkwhalehealthcare.com", $details_admin, "Appointment Cancellation")){
                                    
                   
                    $url = "http://203.200.1.116/smsbroker/alertsms.php?";

                    $url .= "user=pinkwhale&pass=pinkwhale&";

                    $url .= "to=$pw_mobile";

                    $url .="&from=PWCARE&msg=";                

                   // $url .="Dear%20".str_replace(" ","%20",$p_name).",%20ur%20Apt%20at%20".str_replace(" ","%20",$clinic_name)."%20with%20".str_replace(" ","%20",$doc_name)."%20on%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time)."%20Token%20".str_replace(" ","%20",$TOKEN)."%20stands%20cancelled%20as%20per%20your%20request.&msgopt=0&priority=2"; 

		    $url .="Dear%20Admin,FYR%20admin%20has%20cancelled%20appointment%20bearing%20token%20".str_replace(" ","%20",$TOKEN)."%20with%20".str_replace(" ","%20",$doc_name)."%20for%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time).".&msgopt=0&priority=2"; 
                    
                    $url .= "&msgopt=0&priority=2";

                    $fetched = file_get_contents($url);

                    $content = $fetched;
                    
                    /***********     Pinkwhale Admin SMS URL  End       *************/


            $_SESSION['msg'] ="Successfully Cancelled Appointment"; 
            header("Location: ../cancel_patient_appointment.php");
            exit;

        }else{

            $_SESSION['error'] ="Failed to Cancel Appointment";
            header("Location: ../cancel_patient_appointment.php");
            exit;

        }
        
    }else{
            $_SESSION['error'] ="Invalid Token No.";
            header("Location: ../cancel_patient_appointment.php");
            exit;
    }

}else{
            $_SESSION['error'] ="Invalid Token No.";
            header("Location: ../cancel_patient_appointment.php");
            exit;
}

ob_flush();



function get_pw_moile(){
    include '../../db_connect.php';
    
    $qry = "select admin_Mobile from login_user";
    $res = mysql_query($qry);
    $dat = mysql_fetch_array($res);
    
    return $dat[0];
}
?>

