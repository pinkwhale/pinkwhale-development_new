
<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" id="mainBg" valign="top">
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            
    ?>
   
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Doctors Schedule</th></tr>
         
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Select Clinic<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic"   onchange="get_num()">
                    <option value="" selected="selected" disabled>-----select Clinic-----</option>
                    <?php
                        $qry = "select clinic_id,name from clinic_details where status=0 and name<>''";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id']."|".$data['name']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
		</table>
		<table align="center">    
        
        <tr>
            <td id="doc_id_1" width="40%" align="center" >
              
            </td>        
        </tr>
        
    </table>
    
    <?php
        }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
    enable_clinic_submenu();
    // enable_clinic_doctor_association();
    
    function get_num(){
        var doc_id=encodeURI(document.getElementById('clinic').value);
        //$('#doc_id_1').load('get_clinic_wise_doc_details.php?clinic_id='+doc_id);
        $('#doc_id_1').load('get_clinic_doc_details.php?clinic='+doc_id);
    }
</script>
</html>
<?php
ob_end_flush();
?>
