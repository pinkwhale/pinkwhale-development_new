<?php 

	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
	

?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>pinkwhalehealthcare</title>
    <meta name="description" content="pinkwhalehealthcare">
    <link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
    <link href="css/my_css.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/add-group-validation.js"></script>
    <script type="text/javascript" src="js/delete-group.js"></script>

<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>

</head>
<body onLoad="init_table();">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" >
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;

?>

		<form action="actions/delete_group.php" name="delete_group_form" id="delete_group_form" method="post">
		<table width='600' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="4">Manage Groups</span></th></tr>
<?php		
		// Display all the data from the table 
			$sql="SELECT * FROM `groups` order by group_id ASC";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 7, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) die(mysql_error());
			?>
		
				<tr>
                    <td align='left'><strong>SI No:</strong> </td>
                    <td align='left'><strong>Group Name</strong></td>
                    <td align='left'><strong>Group Type</strong></td>
                    <td align='left'><strong>Select</strong></td>
                </tr>
			
			
			<?php	
			$count = 0;
			while($row = mysql_fetch_assoc($rs)) 
			{
			   $count ++;
				$id=$row['group_id'];
				$groupname=$row['group_name'];
				$grouptype=$row['group_type'];
				echo "<tr bgcolor='#ffffff'>";
				echo "<td>$count </td> ";
				echo "<td>"; 
				?>
                
        	<a href="edit-group.php?id=<?php echo $id ?>" style="text-decoration:none;"> <?php echo $groupname ; ?></a></td>
				<!--<a href="manage-group.php" style="text-decoration:none;"> <?php echo $username ; ?></a></td> -->
                        <?php
				echo "<td>$grouptype </td> ";
				echo "<td><input type='checkbox' id='delete_id' name='delete[]' value=". $id ." /></td>";
				echo "</tr>";
			
			}
			
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>', '</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
            
	
			<tr><td colspan='5' align='center'>
			 
            <?php echo "$ps1"; ?> 
			
            <?php
            echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
       </table>

 <div style="float:right; margin-right:150px; height:50px;">
      	<input name="delete_group" type="button" id="delete_group" onmouseover="this.style.cursor='pointer';" value="Delete" onclick="delete_group_msg();" />
     </div>
      </form>
</td>
</tr>
</table>

 <?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_group_submenu();
</script>
</body></html>