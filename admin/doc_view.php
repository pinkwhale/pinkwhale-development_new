<?php
include 'phpexcel/Classes/PHPExcel/IOFactory.php';
include ("../db_connect.php");
include("Sajax.php");
sajax_init();
sajax_export('getdata');
sajax_export('getquery');
sajax_handle_client_request();
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<link href="../calendar/calendar.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/upclick-min.js"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<style type="text/css">
.Manually {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e10971), color-stop(1, #f40478) );
	background:-moz-linear-gradient( center top, #e10971 5%, #f40478 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#f40478');
	background-color:#e10971;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#FFFFFF;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:40px;
	line-height:40px;
	width:100px;
	text-decoration:none;
	text-align:center;
	
}
.Manually:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f40478), color-stop(1, #e10971) );
	background:-moz-linear-gradient( center top, #f40478 5%, #e10971 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f40478', endColorstr='#e10971');
	background-color:#f40478;
}.Manually:active {
	position:relative;
	top:1px;
}</style>
<style type="text/css">
#popup
{
    display:none;
    position: absolute;
    width:667px;
    height: 400px;
    top: 50%;
    left: 19%;
    margin-left:302px;
    margin-top:-15px;
    border:2px solid #E10971;
    border-top:4px solid #E10971;
    background-color:#DFDFDF;
    padding:30px;
    z-index:102;
    font-family:Verdana;
    font-size:10pt;
    border-radius:10px;
    -webkit-border-radius:20px;
    -moz-border-radius:20px;
    font-weight:bold;
}
#content
{
    height:auto;
    width:685px;
   /*  margin:-16px auto; */
}
#popupclose
{
    margin:35px 0 0 80px;
    width:50px;
    
}
#cancel
{
    margin:35px 0 0 80px;
    width:50px;
    
}
#popupOk
{
    margin:35px 0 0 80px;
    width:50px;
    
}
</style>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>

<script>
function check_plan(signup,id)
{
	var i=id;
	
	if(signup=="pW-Tele")
	{
		document.getElementById("1online"+i).disabled=true;	
		document.getElementById("5online"+i).disabled=true;	
		document.getElementById("15online"+i).disabled=true;	
		document.getElementById("3min"+i).disabled=false;	
		document.getElementById("15min"+i).disabled=false;	
		document.getElementById("30min"+i).disabled=false;	
		
	}
	else if(signup=="pW-online")
	{
		document.getElementById("3min"+i).disabled=z;	
		document.getElementById("15min"+i).disabled=true;	
		document.getElementById("30min"+i).disabled=true;	
		document.getElementById("1online"+i).disabled=false;	
		document.getElementById("5online"+i).disabled=false;	
		document.getElementById("15online"+i).disabled=false;	
		
	}
	else if(signup=="pW-combi")
	{
		document.getElementById("3min"+i).disabled=false;	
		document.getElementById("15min"+i).disabled=false;	
		document.getElementById("30min"+i).disabled=false;	
		document.getElementById("1online"+i).disabled=false;	
		document.getElementById("5online"+i).disabled=false;	
		document.getElementById("15online"+i).disabled=false;	
		
	}
	
	
}
function efun($u,$d_id,$row)
{
	var id=""+$u;
	var d_id=""+$d_id;
	var row=""+$row;
	var c_id=id+d_id;
$( "#d"+id+d_id ).addClass( "selected highlight" );
for(var j=0;j<row;j++)
{
	for(var i=0;i<6;i++)
	{var p=""+i+j;	
	if(c_id!=p)
		{
		$( "#d"+p ).removeClass('selected highlight');}

	}
}
}

function p_details($id,$row)
{
	var uid=$id;
    var row=$row;
    for(var k=1;k<=row+1;k++)
		hiding(k);
	$( "#pdetails"+uid ).fadeToggle("slow");
	
}
function address_details($id,$row)
{
	var uid=$id;
    var row=$row;
    for(var k=1;k<=row+1;k++)
		hiding(k);
	$( "#addressdetails"+uid ).fadeToggle("slow");
	
}
function plan_details($id,$row)
{
	var uid=$id;
    var row=$row;
	for(var k=1;k<=row+1;k++)
		hiding(k);
    
    $( "#plandetails"+uid ).fadeToggle("slow");
}
function pro_details($id,$row)
{
	var uid=$id;
    var row=$row;
    for(var k=1;k<=row+1;k++)
		hiding(k);		
	$( "#prodetails"+uid ).fadeToggle("slow");
	
}
function clinic_details($id,$row)
{
	var uid=$id;
    var row=$row;
	for(var k=1;k<=row+1;k++)
		hiding(k);
    $( "#clinicdetails"+uid ).fadeToggle("slow");

}
function pricing_details($id,$row)
{
    var uid=$id;
    var row=$row;
    for(var k=1;k<=row+1;k++)
        hiding(k);
    $( "#pricingdetails"+uid ).fadeToggle("slow");
    
}

function hiding(uid)
	{
	var id2=uid;
		
		$( "#pricingdetails"+id2 ).hide();
		$( "#addressdetails"+id2 ).hide();
		$( "#prodetails"+id2 ).hide();
		$( "#pdetails"+id2 ).hide();
		$( "#plandetails"+id2 ).hide();
		$( "#clinicdetails"+id2 ).hide();
		
		}
	</script>
	
	<script>
function disableButtons(row){
	var id=row;
	var j=0;
	var x = document.getElementById('view_listing');
	var y = x.getElementsByTagName('input');
	for (var i = 0; i < y.length; i++){
	if(y[i].disabled== true)
	{
	j++;
	}
}
if(j==id){document.getElementById("page_next").style.display='block';}
}
	function showEditPage()
	{
		document.getElementById("upload").style.display='block';
		 document.getElementById("listing").style.display='none';
		 document.getElementById("copy").style.display='block';
		 
		}
function data_save_call(k,row){
document.getElementById("popupclose").disabled=true;
document.getElementById("popupOk").disabled=true;
document.getElementById("cancel").disabled=true;
  var name=document.getElementById("name"+k).value;
  var gender=document.getElementById("gender"+k).value;
  var email=document.getElementById("email"+k).value;
  var mob_no=document.getElementById("mob_no"+k).value;
  var add1=document.getElementById("add1"+k).value;
  var add2=document.getElementById("add2"+k).value;
  var city=document.getElementById("city"+k).value;
  var qualification=document.getElementById("qualification"+k).value;
  var type=document.getElementById("type"+k).value;
  var department=document.getElementById("department"+k).value;
  var specialization=document.getElementById("specialization"+k).value;
  var c_name=document.getElementById("c_name"+k).value;
  var c_address=document.getElementById("c_address"+k).value;
  var time=document.getElementById("time"+k).value;
  var call_d_entity=document.getElementById("call_d_entity"+k).value;
  var tele_con_no=document.getElementById("tele_con_no"+k).value;
  var signup_plan=document.getElementById("signup_plan"+k).value;
  var sp_price=document.getElementById("sp_price"+k).value;
  var sp_no_cards=document.getElementById("sp_no_cards"+k).value; 
  var addon=document.getElementById("addon"+k).value; 
  var addon_price=document.getElementById("addon_price"+k).value;
  var addon_no_cards=document.getElementById("addon_no_cards"+k).value;
  var min3=document.getElementById("3min"+k).value; 
  var min15=document.getElementById("15min"+k).value; 
  var min30=document.getElementById("30min"+k).value;  
  var online1=document.getElementById("1online"+k).value;  
  var online5=document.getElementById("5online"+k).value;  
  var online15=document.getElementById("15online"+k).value; 
  var ratio=document.getElementById("ratio"+k).value; 
$.ajax({  
                        type: "POST",  
                        url: "doc_save.php",  
                        data: { name : name,gender :gender,email:email, mob_no:mob_no, add1:add1, add2:add2, city:city, qualification:qualification, type:type, department:department, specialization:specialization, c_name:c_name, time:time, c_address:c_address, call_d_entity:call_d_entity, tele_con_no:tele_con_no, signup_plan:signup_plan, sp_price:sp_price, sp_no_cards:sp_no_cards ,addon:addon, addon_price:addon_price, addon_no_cards:addon_no_cards, min3:min3, min15:min15, min30:min30, online1:online1, online5:online5, online15:online15 , ratio:ratio},  
                        success: function(response) { 
                           // alert(response);                                                     
                                if(response==false)
								{document.getElementById("popupclose").disabled=false;
document.getElementById("popupOk").disabled=false;
document.getElementById("cancel").disabled=false;
									alert(response);
								}else{ 
									//alert(row);
                                //alert(response);
								document.getElementById("report"+k).value=response;
								document.getElementById('reportlink'+k).href="reports.php?id_report="+response;
								document.getElementById('reportlink'+k).style.display="block";
                                document.getElementById("popuplink"+k).style.display="none";
                                document.getElementById("popuplink"+k).disabled=true;
								disableButtons(row);
								document.getElementById("popupclose").disabled=false;
document.getElementById("popupOk").disabled=false;
document.getElementById("cancel").disabled=false;
								closepopup();
									}
                        }

                });
}
		
	
function copyForm($row)
{
	 document.getElementById("upload").style.display='none';
	 document.getElementById("listing").style.display='block';
	 document.getElementById("copy").style.display='none';
	 var row=$row;
for(var k=0;k<row;k++)
{
	document.getElementById("sl1"+k).innerHTML=k+1;
   document.getElementById("name1"+k).innerHTML=document.getElementById("name"+k).value;
 document.getElementById("gender1"+k).innerHTML=document.getElementById("gender"+k).value;
   document.getElementById("email1"+k).innerHTML=document.getElementById("email"+k).value;
   document.getElementById("mob_no1"+k).innerHTML=document.getElementById("mob_no"+k).value;
   document.getElementById("type1"+k).innerHTML=document.getElementById("type"+k).value;
   document.getElementById("department1"+k).innerHTML=document.getElementById("department"+k).value;
   document.getElementById("specialization1"+k).innerHTML=document.getElementById("specialization"+k).value;
}


	}



</script>
	
<script>
$(function(){

    $('#popuplink').click(function(){
        $('#popup').show("slow");
    });
    
     $('#popupclose').click(function(){
        $('#popup').hide("slow");
    });
     $('#cancel').click(function(){
         $('#popup').hide("slow");
     });
	 
});
function closepopup(){
         $('#popup').hide("slow");
     }
function ShowPopUp(k,row)
{
  $('#doctor_id').val(k);	
document.getElementById('popupOk').onclick =function(){data_save_call(k,row)};
  var nameP=document.getElementById("name"+k).value;
  var genderP=document.getElementById("gender"+k).value;
  var emailP=document.getElementById("email"+k).value;
  var mob_noP=document.getElementById("mob_no"+k).value;
  var photoP=document.getElementById("FileID"+k).value;
  var add1P=document.getElementById("add1"+k).value;
  var add2P=document.getElementById("add2"+k).value;
  var cityP=document.getElementById("city"+k).value;
  var qualificationP=document.getElementById("qualification"+k).value;
  var typeP=document.getElementById("type"+k).value;
  var departmentP=document.getElementById("department"+k).value;
  var specializationP=document.getElementById("specialization"+k).value;
  var c_nameP=document.getElementById("c_name"+k).value;
  var c_addressP=document.getElementById("c_address"+k).value;
  var timeP=document.getElementById("time"+k).value;
  var call_d_entityP=document.getElementById("call_d_entity"+k).value;
  var tele_con_noP=document.getElementById("tele_con_no"+k).value;
  var signup_planP=document.getElementById("signup_plan"+k).value;
  var sp_priceP=document.getElementById("sp_price"+k).value;
 var sp_no_cardsP=document.getElementById("sp_no_cards"+k).value; 
 var addonP=document.getElementById("addon"+k).value; 
 var addon_priceP=document.getElementById("addon_price"+k).value;
 var addon_no_cardsP=document.getElementById("addon_no_cards"+k).value;
 var min3P=document.getElementById("3min"+k).value; 
 var min15P=document.getElementById("15min"+k).value; 
var min30P=document.getElementById("30min"+k).value;  
var online1P=document.getElementById("1online"+k).value;  
var online5P=document.getElementById("5online"+k).value;  
var online15P=document.getElementById("15online"+k).value;
var ratioP=document.getElementById("ratio"+k).value;
  //alert(nameP,genderP,emailP,mob_noP);
  //$('#name_pop').val(nameP);	
 $('#name_pop').html(nameP);
 $('#gender_pop').html(genderP);	
 $('#email_pop').html(emailP);	
 $('#mob_no_pop').html(mob_noP);	
 $('#photo_pop').html(photoP);	
 $('#add1_pop').html(add1P);	
 $('#add2_pop').html(add2P);	
 $('#city_pop').html(cityP);	
 $('#qualification_pop').html(qualificationP);
 $('#type_pop').html(typeP);
 $('#department_pop').html(departmentP);
 $('#specialization_pop').html(specializationP);
 $('#c_name_pop').html(c_nameP);
 $('#c_address_pop').html(c_addressP);
 $('#time_pop').html(timeP);
 $('#call_d_entity_pop').html(call_d_entityP);
 $('#tele_con_no_pop').html(tele_con_noP);
 $('#signup_plan_pop').html(signup_planP);
 $('#signup_price_pop').html(sp_priceP);
 $('#signup_cards_pop').html(sp_no_cardsP);
 $('#addon_pack_pop').html(addonP);
 $('#addon_price_pop').html(addon_priceP);
 $('#addon_cards_pop').html(addon_no_cardsP);
 $('#3min_pop').html(min3P);
 $('#15min_pop').html(min15P);
 $('#30min_pop').html(min30P);
 $('#1online_pop').html(online1P);
 $('#5online_pop').html(online5P);
 $('#15online_pop').html(online15P);
 $('#ratio_pop').html(ratioP);
 $('#popup').show("slow");

}


function getXMLHTTP() { 
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
	}
	
	
	
function getNumOfCards(strURL,i)
{
	
	var req = getXMLHTTP();		
	if (req) 
	{
		var i=i;
		//function to be called when state is changed
		req.onreadystatechange = function()
		{
			//when state is completed i.e 4
			if (req.readyState == 4) 
			{			
				// only if http status is "OK"
				if (req.status == 200)
				{						
					document.getElementById("addon_no_cards"+i).value=req.responseText;						
				} 
				else 
				{
					alert("There was a problem while using XMLHTTP:\n" + req.statusText);
				}
			}				
		 }			
		 req.open("GET", strURL, true);
		 req.send(null);
	}			
}

function getPrice(strURL,i)
{
	var req = getXMLHTTP();		
	if (req) 
	{
		//function to be called when state is changed
		req.onreadystatechange = function()
		{
			//when state is completed i.e 4
			if (req.readyState == 4) 
			{			
				// only if http status is "OK"
				if (req.status == 200)
				{						
					document.getElementById("addon_price"+i).value=req.responseText;						
				} 
				else 
				{
					alert("There was a problem while using XMLHTTP:\n" + req.statusText);
				}
			}				
		 }			
		 req.open("GET", strURL, true);
		 req.send(null);
	}			
}

function getSignUpPlan(strURL,i)
{
	
	var req = getXMLHTTP();		
	if (req) 
	{
		var i=i;
		//function to be called when state is changed
		req.onreadystatechange = function()
		{
			//when state is completed i.e 4
			if (req.readyState == 4) 
			{			
				// only if http status is "OK"
				if (req.status == 200)
				{						
					document.getElementById("sp_no_cards"+i).value=req.responseText;						
				} 
				else 
				{
					alert("There was a problem while using XMLHTTP:\n" + req.statusText);
				}
			}				
		 }			
		 req.open("GET", strURL, true);
		 req.send(null);
	}			
}

function getPriceof_plan(strURL,i)
{
	
	var req = getXMLHTTP();		
	if (req) 
	{
		var i=i;
		//function to be called when state is changed
		req.onreadystatechange = function()
		{
			//when state is completed i.e 4
			if (req.readyState == 4) 
			{			
				// only if http status is "OK"
				if (req.status == 200)
				{						
					document.getElementById("sp_price"+i).value=req.responseText;						
				} 
				else 
				{
					alert("There was a problem while using XMLHTTP:\n" + req.statusText);
				}
			}				
		 }			
		 req.open("GET", strURL, true);
		 req.send(null);
	}			
}

</script>
	
	<style>
 .s9registerform {
    border: 2px ridge grey;
    font-family: Arial,Verdana;
    margin-bottom: 10px;
    margin-top: 20px;
    padding: 0;
}


	/*This is how the text will look before mouse over*/
	.colc{
	font-family: san-serif;
  	color: blue;
  	font-size: 15px;
 	}

	/*This is how the text will look on mouse over. Note "hover" is the most important change here*/
	.colc:hover
	{
	font-family: Arial,Verdana;
  	color: magenta;
 	}
	


</style>
<body>

<form enctype="multipart/form-data" action="saveTodatabase.php" name="upload_photo" method="post">
<div id="popup" style="height: auto;">
<div id="content">
<table style="font-size:9px;"> <tr> <input id="doctor_id" name="doctor_id" type="hidden"/></tr>   
<tr><td><font color="red">Personal Details</font></td></tr>
<tr>
<td><label style="font-size:9px;">Name :</label><div id="name_pop" name="name_pop"/></td> 
<td><label>Gender :</label><div id="gender_pop" name="gender_pop"/></td>   
<td><label>Email :</label><div id="email_pop" name="email_pop"/> </td>
<td><label>Mobile</label><div id="mob_no_pop" name="mob_no_pop"/></td>

</tr>
<tr></tr>
<tr><td><font color="red">Address Details</font></td></tr>

<tr>
<td><label>AddressLine1</label><div id="add1_pop" name="add1_pop"/></td>
<td><label>AddressLine2</label><div id="add2_pop" name="add2_pop"/></td><td></td> 
<td><label>City</label><div id="city_pop" name="city_pop"/> </td></tr>
<tr></tr>
<tr><td><font color="red">Professional Details</font></td></tr>
<tr><td><label>Qualification</label><div id="qualification_pop" name="qualification_pop"/> </td>
<td><label>Type</label><div id="type_pop" name="type_pop"/> </td>
<td><label>Department</label><div id="department_pop" name="department_pop"/> </td>
<td><label>Specialization</label><div id="specialization_pop" name="specialization_pop"/></td></tr>
<tr></tr>
<tr><td><font color="red">Clinic Details</font></td></tr>
<tr>
<td><label>Clinic name</label><div id="c_name_pop" name="c_name_pop"/> </td>
<td><label>Clinic Address</label><div id="c_address_pop" name="c_address_pop"/> </td>
<td><label>Timing</label><div id="time_pop" name="time_pop"/> </td>
<td><label>Call Direct Entity</label><div id="call_d_entity_pop" name="call_d_entity_pop"/> </td>
<td><label>Tele consultation Number</label><div id="tele_con_no_pop" name="tele_con_no_pop"></td>
</tr>
<tr></tr>
<tr><td><font color="red">Plan Details</font></td></tr>
<tr>
<td><label>Sign Up plan</label><div id="signup_plan_pop" name="signup_plan_pop"></td>
<td><label>price</label><div id="signup_price_pop" name="signup_price_pop"/> </td>
<td><label>No of Cards</label><div id="signup_cards_pop" name="signup_cards_pop"/> </td>
</tr>
<tr>
<td><label>Addon Pack</label><div id="addon_pack_pop" name="addon_pack_pop"></td>
<td><label>price</label><div id="addon_price_pop" name="addon_price_pop"/> </td>
<td><label>No of Cards</label><div id="addon_cards_pop" name="addon_cards_pop"/> </td>
</tr>
<tr></tr>
<tr><td><font color="red">Pricing Details</font></td></tr>
<tr>
<td><label>3 minutes:</label><div id="3min_pop" name="3min_pop"></td>
<td><label>15 minutes:</label><div id="15min_pop" name="15min_pop"/> </td>
<td><label>30 minutes:</label><div id="30min_pop" name="30min_pop"/> </td>
</tr>
<tr>
<td><label>1 online consult :</label><div id="1online_pop" name="1online_pop"></td>
<td><label>5 online consult :</label><div id="5online_pop" name="5online_pop"/> </td>
<td><label>10 online consult :</label><div id="15online_pop" name="15online_pop"/> </td>
</tr>
<tr>
<td><label style="color:red;">Share Ratio Between Doctor and Pinkwhale :</label><div id="ratio_pop" name="ratio_pop"/> </td>
</tr>
<tr>
<td>
<input id="popupclose" type="Button" value="Edit" onclick="showEditPage()"/></td>
<td><input id="popupOk" type="Button" value="Save" onclick="data_save_call()" style="margin-left:62px;"/></td>
<td><input id="cancel" type="Button" value="Cancel"/></td></tr>
</table>    
    
 </div>   
    
</div>
</form>



<?php include "admin_head.php"; 
function parseExcel($array) {
	$errors = array();
	$doctorDetails=array();
	for ($i=2; $i<sizeof($array); $i++){

		$doctorDetails = $array[$i];
		
		for ($docAttr=0; $docAttr<sizeof($doctorDetails); $docAttr++){
$add=$doctorDetails[18];			
              switch ($docAttr){
         case 1:
	       if ($doctorDetails[1]==""){
	      	array_push($errors,"Please give doctor name for the row ".($i-1)."");
	       }
		break;

			case 3:
					if (isEmailValid($doctorDetails[3])==1){
						array_push($errors," Email address of ".$doctorDetails[1]." ".$doctorDetails[3]. " is invalid");
					}
					elseif (isEmailValid($doctorDetails[3])==2){
						array_push($errors," Email address of ".$doctorDetails[1]." ".$doctorDetails[3]. " already exist");
					}
					break;
				case 4:
					if (!isMobileValid($doctorDetails[4])){
						if($doctorDetails[4]=="")
							array_push($errors," Mobile number of ".$doctorDetails[1]." ".$doctorDetails[4]." is Empty");
						else
						array_push($errors," Mobile number of ".$doctorDetails[1]." ".$doctorDetails[4]." is invalid");
					}
					break;
    			case 15:
					if (!isMobileValid($doctorDetails[15])){
						if($doctorDetails[15]=="")
							array_push($errors,"Call Direct Entity number of ".$doctorDetails[1]." ".$doctorDetails[15]." is Empty");
						else
						array_push($errors,"Call Direct Entity number of ".$doctorDetails[1]." ".$doctorDetails[15]." is invalid");
					}
					break;
				case 16:
					if($doctorDetails[17]!="pW-online")
					{
					if (!isMobileValid($doctorDetails[16])){
                      if($doctorDetails[16]=="")
                        	array_push($errors,"Teleconsultation number of ".$doctorDetails[1]." ".$doctorDetails[16]."  is Empty");
					  else
                            array_push($errors,"Teleconsultation number of ".$doctorDetails[1]." ".$doctorDetails[16]."  is invalid");
					}
					}
					break;
				case 17:
						if ($doctorDetails[17]==""){
							array_push($errors,"Please select signup plan for the doctor ".$doctorDetails[1]."");
						}
						break;
							
				case 19:
					if($add=="")
					{
						array_push($errors,"Please select Yes Or No for column Adodn pack for  ".$doctorDetails[1]."");
					}
					if (($add=="No") && ($doctorDetails[19]!=""))
					{
						array_push($errors,"You have selected Addon pack column as No but you selected an Addon pack for  ".$doctorDetails[1]."");
					}
					elseif (($add=="Yes") && ($doctorDetails[19]=="")){
						array_push($errors,"You have selected Addon pack column as Yes but you haven't selected any Addon pack for  ".$doctorDetails[1]."");
					}
					else
					break;
			}
		}
	}
	return $errors;
}

function isMobileValid($mobile){
	if (!is_numeric ($mobile)){
		return false;
	}
	else
	return true;
}

function isEmailValid($email){
	$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
	if (!preg_match($regex, $email)) {
		return 1;
	}
	else
	{
		$qry7=mysql_query("SELECT * FROM `pw_doctors` WHERE `doc_email_id`='$email'");
		if(!$qry7){
			return false;
		}

		$num_rows = mysql_num_rows($qry7);
		if($num_rows>=1)
			return 2;
	}
	return 3;
}

?>

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?>

</td>
<td width="900" >
<?php
$inputFileName = pathinfo($_FILES['uploaded_file']['name']);
$ext = $inputFileName['extension']; // get the extension of the file

//if valid files
if ($ext == "xls" || $ext == "xlsx") {
	 
	$array = null;
	$errors = null;
	$objPHPExcel = null;
	$objReader = null;
	$date = date_create();
	$newFileName = date_timestamp_get($date).'.'.$ext;
	$destination = 'excel/'.$newFileName;

	//copy the file to destination folder
	if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$destination))) {
		try {
			$inputFileType = PHPExcel_IOFactory::identify($destination);
			//echo "TYPE".$inputFileType;
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($destination); // Load $inputFileName to a PHPExcel Object
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
		}
	}
	//fetch the values in array
	$array=$objPHPExcel->getActiveSheet()->toArray();
	//print_r($array);
	$errors = parseExcel($array);
		if (sizeof($errors)>0){?>
					<div id="tot_doc_page" class="message" style=" margin-left: 15px; margin-top: -260px; padding: 50px;">
										<font color="red" font-weight:900;> <?php echo "File cant be uploaded due to following errors...";?>
										</font><br>
									
					<?php 
					foreach ($errors as $error){?>
						<font color="red"> <?php echo $error."<br/>";?>
					</font>
					<?php }
					?></div>
		<div style="text-align:centre; margin-left:150px;">
					<form action="add-new-doctor.php">
				<button type="submit" class="backbtn" >Back</button>
			</form></div>
			 <?php 		
		       }else
		        { ?>
		

		        
<form name="upload" id="upload" class='s9registerform' style="margin-left:20px; border: 0 ridge #808080;">
<table><tr style="border: 2px ridge grey;"><td>
<table width='950' border='0' cellpadding='4' cellspacing='1' bgcolor='#eeeeee' align='left'  ><tr style="background:rosybrown;height: 40px;"><th style="font-family:Verdana;width: 50px;" align='left'><font color="black">SL NO</font></th><th style="float: left;font-family: Verdana;width: 180px;"><font color="black">NAME</font></th><th style="float: left;font-family: Verdana;width: 150px;"><font color="black">Qualification</font></th><th style="float: left;font-family: Verdana;width: 150px;"><font color="black">Type</font></th><th style="float: left;font-family: Verdana;width: 200px;"><font color="black">Department</font></th><th style="float: left;font-family: Verdana;width: 150px;"><font color="black">Specialization</font></th></tr></table>
<?php  $row=sizeof($array)-2;?>
<input type="hidden" name="rowSize" value="<?php echo $row;?>">
<?php 
	
	?>
<?php 
$j=1;
for($i=0;$i<$row;$i++)
{
	?>
	
	<table width='950' border='0' cellpadding='4' cellspacing='1' bgcolor='#eeeeee' align='center'  >
	<tr  style="background: none repeat scroll 0 0 silver;height: 40px;"></th>
	
	<th style="font-family:Verdana;text-align:left;padding-left:10px;" width="50"><font color="#E10971"><?php echo $j;?></font></th>
	<th style="font-family:Verdana" width="200"><font color="#E10971"><?php echo $array[$i+2][1]?></font></th>
        <th style="font-family:Verdana" width="150"><font color="#E10971"><?php echo $array[$i+2][8]?></font></th>
        <th style="font-family:Verdana" width="150"><font color="#E10971"><?php echo $array[$i+2][9]?></font></th>
        <th style="font-family:Verdana" width="250"><font color="#E10971"><?php echo $array[$i+2][10]?></font></th>
        <th style="font-family:Verdana" width="150"><font color="#E10971"><?php echo $array[$i+2][11]?></font></th><td></td></tr></table>
	
	
	
	
	
	<table width='950' border='0' cellpadding='4' cellspacing='1' bgcolor='#eeeeee' align='center'>
	<tr><td></td><td id="heading" class="colc" onclick="p_details(<?php echo $i+2 ?>,<?php echo $row?>)">Personal</td>
	<td id="heading" class="colc" onclick="address_details(<?php echo $i+2 ?>,<?php echo $row?>)">Address</td>
	<td id="heading" class="colc" onclick="pro_details(<?php echo $i+2 ?>,<?php echo $row?>)">Professional</td>
	<td id="heading" class="colc" onclick="clinic_details(<?php echo $i+2 ?>,<?php echo $row?>)"?>Clinic</td>
	<td id="heading" class="colc" onclick="plan_details(<?php echo $i+2 ?>,<?php echo $row?>)">Plan</td>
	<td id="heading" class="colc" onclick="pricing_details(<?php echo $i+2 ?>,<?php echo $row?>)">Pricing</td></tr></table>
   <!--  <td class="colc" onclick="summary_details( --><?php /*  echo $i+2 */ ?><?php /* echo $row */?><!-- )">Summary</td></tr> -->
	 	
	<table style="display: none; padding-left:63px; margin-left:49px;" id="pdetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Name:</font></td><td><input type="text" value="<?php echo $array[$i+2][1];?>" name="name<?php echo $i ?>" id="name<?php echo $i ?>"/></td>
	<td><font color="black">Gender:</font></td><td><select name="gender<?php echo $i;?>" id="gender<?php echo $i ?>"><option value="Female" <?php if ($array[$i+2][2]=='Female'){?> selected="selected" <?php }?>>Female</option><option value="Male" <?php if ($array[$i+2][2]!='Female'){?> selected="selected" <?php }?>>Male</option></select></td></tr>
	<tr><td><font color="black">Email :</font></td><td><input type="text" value="<?php echo $array[$i+2][3]?>" name="email<?php echo $i ?>" id="email<?php echo $i ?>"/></td>
	<td><font color="black">Mobile No:</font></td><td><input type="text" value="<?php echo $array[$i+2][4]?>" name="mob_no<?php echo $i ?>" id="mob_no<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black"></font></td><td><div id="fileInput" style="display:none"><label for="FileID<?php echo $i ?>"><img src="ImageURL"></label><input type="file" name="FileID<?php echo $i ?>" id="FileID<?php echo $i ?>">
	<script>
            document.getElementById("FileID<?php echo $i ?>").onchange = function(){var fileName=(document.getElementById('FileID<?php echo $i ?>').value); 
        	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
        	if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "doc" || ext == "png")
        	   return true;
           	else
        	 {
        	   alert("Upload Gif or Jpg or png images only");
        	   file.focus();
        	   document.getElementById('FileID<?php echo $i ?>').innerHTML="file not selected";
        	   return false;
        	 }
			}
     </script>
</div></td></tr>
	</table>

	<table  style="display: none; padding-left:63px;margin-left:49px;" id="addressdetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Address line1:</font></td><td><input type="text" value="<?php echo $array[$i+2][5];?>" name="add1<?php echo $i ?>" id="add1<?php echo $i ?>"/></td>
	<td><font color="black">Address line 2:</font></td><td><input type="text" value="<?php echo $array[$i+2][6]?>" name="add2<?php echo $i ?>" id="add2<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">city :</font></td><td><input type="text" value="<?php echo $array[$i+2][7]?>" name="city<?php echo $i ?>" id="city<?php echo $i ?>" /></td></tr>
	</table>
	
	<table  style="display: none; padding-left:63px; margin-left:49px;" id="prodetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Qualification:</font></td><td><input type="text" value="<?php echo $array[$i+2][8];?>" name="qualification<?php echo $i ?>" id="qualification<?php echo $i ?>"/></td>
	<td><font color="black">Type:</font></td><td><select name="type<?php echo $i;?>" id="type<?php echo $i;?>"><option value="Specialist" <?php if ($array[$i+2][9]=='Specialist'){?> selected="selected" <?php }?>>Specialist</option><option value="Expert" <?php if ($array[$i+2][9]=='Expert'){?> selected="selected" <?php }?>>Expert</option><option value="Query" <?php if ($array[$i+2][9]=='Query'){?> selected="selected" <?php }?>>Query</option><option value="Clinic Appointment" <?php if ($array[$i+2][9]=='Clinic Appointment'){?> selected="selected" <?php }?>>Clinic Appointment</option></select></td></tr>
	
	
	<tr><td><font color="black">Department :</font></td>
	<td>
	<select name="department<?php echo $i;?>" id="department<?php echo $i;?>">
	<?php
	$sql1=mysql_query("SELECT added_department FROM  add_department");	
	$dep_row=mysql_num_rows($sql1);
	for($dp_flag=0;$dp_flag<$dep_row;$dp_flag++){
       $dep_array=mysql_fetch_array($sql1);	
       ?>     
  	<option value="<?php  echo $dep_array['added_department'];?>" <?php if ($dep_array['added_department']==$array[$i+2][10]) echo "selected='selected'"?>><?php echo $dep_array['added_department'];?></option>
  	<?php }?>
	</select></td>
	
	<td><font color="black">Specialization:</font></td>
	<td>
	<select name="specialization<?php echo $i;?>" id="specialization<?php echo $i;?>">
	<?php
	$sql2=mysql_query("SELECT added_speciality FROM  add_specialities");	
	$dep_row2=mysql_num_rows($sql2);
	for($dp_flag1=0;$dp_flag1<$dep_row2;$dp_flag1++){
       $dep_array2=mysql_fetch_array($sql2);	
       ?>     
  	<option value="<?php  echo $dep_array2['added_speciality'];?>" <?php if ($dep_array2['added_speciality']==$array[$i+2][11]) echo "selected='selected'"?>><?php echo $dep_array2['added_speciality'];?></option>
  	<?php }?>
	</select></td>
	</tr>
	</table>
	
	<table  style="display: none; padding-left:63px; margin-left:49px;" id="clinicdetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Name:</font></td><td><input type="text" value="<?php echo $array[$i+2][12];?>" name="c_name<?php echo $i ?>" id="c_name<?php echo $i ?>"/></td>
	<td><font color="black">Address:</font></td><td><input type="text" value="<?php echo $array[$i+2][13]?>" name="c_address<?php echo $i ?>" id="c_address<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">Timing:</font></td><td><input type="text" value="<?php echo $array[$i+2][14]?>" name="time<?php echo $i ?>" id="time<?php echo $i ?>"/></td>
	<td><font color="black">Call Direct Entity:</font></td><td><input type="text" value="<?php echo $array[$i+2][15]?>" name="call_d_entity<?php echo $i ?>" id="call_d_entity<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">Tele consultation Number:</font></td><td><input type="text" value="<?php echo $array[$i+2][16]?>" name="tele_con_no<?php echo $i ?>" id="tele_con_no<?php echo $i ?>"/></td></tr>
	</table> 	
	
	<?php 
	$result=mysql_query("SELECT * FROM  pw_card_plan WHERE description='".$array[$i+2][17]."'");
	while ($rows=mysql_fetch_array($result)){
		$price=$rows['plan_price'];
		$no_cards=$rows['no_cards'];
		}
	if($array[$i+2][19]==""){
		$add_price=0;
        $add_no_cards=0;
	  }else{
   $result_addon=mysql_query("SELECT * FROM pw_addon_packs WHERE name='".$array[$i+2][19]."'");
     while ($rows1=mysql_fetch_array($result_addon)){
	   $add_price=$rows1['price'];
	   $add_no_cards=$rows1['no_of_cards'];
		
	   }
	
	}
	?>	
	<table  style="display: none; padding-left:30px; margin-left:49px;" id="plandetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Sign Up Plan:</font></td>
	<td><select name="signup_plan<?php echo $i;?>" id="signup_plan<?php echo $i;?>" onChange="getSignUpPlan('plan_numof_cards.php?signup_plan='+this.value,<?php echo $i;?>);getPriceof_plan('getPriceof_plan.php?signup_plan='+this.value,<?php echo $i;?>);changeTextbox(<?php echo $i;?>)">
	<?php
	$sql6=mysql_query("SELECT description FROM  pw_card_plan");	
	$signup_row=mysql_num_rows($sql6);
	for($dp_flag3=0;$dp_flag3<$signup_row;$dp_flag3++){
       $dep_array4=mysql_fetch_array($sql6);	
       ?>     
  	<option value="<?php  echo $dep_array4['description'];?>" <?php if ($dep_array4['description']==$array[$i+2][17]) echo "selected='selected'"?>><?php echo $dep_array4['description'];?></option>
   
   
   <?php }?>
	
	   </select></td>  
	       
	   
	
	<td><font color="black">Price:</font></td><td><input type="text" value="<?php echo $price; ?>" name="sp_price<?php echo $i ?>" id="sp_price<?php echo $i ?>"/></td>
	<td><font color="black">No of cards:</font></td><td><input type="text" value="<?php echo $no_cards; ?>" name="sp_no_cards<?php echo $i ?>" id="sp_no_cards<?php echo $i ?>"/></td></tr>
	
	<td><font color="black">Addon Pack:</font></td>
	<td>
	<select name="addon_pack<?php echo $i;?>" id="addon<?php echo $i;?>" onChange="getNumOfCards('add_num.php?addon='+this.value,<?php echo $i;?>);getPrice('add_price.php?addon='+this.value,<?php echo $i;?>)" > 
	<?php
	$sql3=mysql_query("SELECT name FROM   pw_addon_packs");	
	$dep_row3=mysql_num_rows($sql3);
	for($dp_flag2=0;$dp_flag2<$dep_row3;$dp_flag2++){
       $dep_array3=mysql_fetch_array($sql3);	
       ?>     
  	<option value="<?php  echo $dep_array3['name'];?>" <?php if ($dep_array3['name']==$array[$i+2][19]) echo "selected='selected'"?> ><?php echo $dep_array3['name'];?></option>
   
  	<?php }?>
  	<option value="none" <?php if ($array[$i+2][19]=="") echo "selected='selected'"?>>None</option>  
	</select></td>
	
	
	<td><font color="black">Price:</font></td><td>
	<?php 
	$addon=$array[$i+2][19];
	$sql4=mysql_query("SELECT * FROM  pw_addon_packs where name='$addon'");
	$dat = mysql_fetch_array($sql4);
	?>
			<input type="text" value="<?php  echo $dat['price'];?>" name="addon_price<?php echo $i ?>" id="addon_price<?php echo $i ?>"/>	
	
	</td>

	<td>No of cards:<td>
			<input type="text" value="<?php  echo $dat['no_of_cards'];?>" name="addon_no_cards<?php echo $i ?>" id="addon_no_cards<?php echo $i ?>"/>	
	
	
	</td></tr>
	</table>
	
	<table  style="display: none; padding-left:63px; margin-left:49px;" id="pricingdetails<?php echo $i+2 ?>">
	<tr><td><font color="black">3 minutes:</font></td><td><input type="text" value="<?php echo $array[$i+2][20];?>" name="3min<?php echo $i ?>" id="3min<?php echo $i ?>"/></td>
	<td><font color="black">15 minutes:</font></td><td><input type="text" value="<?php echo $array[$i+2][21]?>" name="15min<?php echo $i ?>" id="15min<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">30minutes:</font></td><td><input type="text" value="<?php echo $array[$i+2][22]?>" name="30min<?php echo $i ?>" id="30min<?php echo $i ?>" /></td></tr>
    <tr><td><font color="black">1 online consult :</font></td><td><input type="text" value="<?php echo $array[$i+2][23];?>" name="1online<?php echo $i ?>" id="1online<?php echo $i ?>"/></td>
    <td><font color="black">5 online consult :</font></td><td><input type="text" value="<?php echo $array[$i+2][24]?>" name="5online<?php echo $i ?>" id="5online<?php echo $i ?>"/></td></tr>
    <tr><td><font color="black">10 online consult :</font></td><td><input type="text" value="<?php echo $array[$i+2][25]?>" name="15online<?php echo $i ?>" id="15online<?php echo $i ?>" /></td></tr>
  
   <!-- display share ratio from excel -->
	
   <tr><td><font color="black">Share RatioBetween Doctor and Pinkwhale :</font></td><td><input type="text" value="<?php echo $array[$i+2][25+1];?>" name="ratio<?php echo $i ?>" id="ratio<?php echo $i ?>" onkeypress="return isNumberKey1(event);"/></td>
   </tr>
	<!--end display share ratio from excel -->
	
   
    </table>
    <script>
	function isNumberKey1(evt)
					{
						var charCode = (evt.which) ? evt.which : event.keyCode
						if (charCode > 31 && (charCode < 48 || charCode > 58))
						{
							return false; 
						}
						else
						{
							return true; 
						}
					}
	</script>
	
	    
	
		
    <table  style="display: none; padding-left:63px;" id="sdetails<?php echo $i+2 ?>">
	<tr><td><font color="black">Name:</font></td><td><input type="text"  name="sumName<?php echo $i ?>" id="sumName<?php echo $i ?>"/></td>
	<td><font color="black">Gender:</font></td><td><input type="text"  name="sumGender<?php echo $i ?>" id="sumGender<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">Email :</font></td><td><input type="text"  name="sumEmail<?php echo $i ?>" id="sumEmail<?php echo $i ?>"/></td>
	<td><font color="black">Mob num :</font></td><td><input type="text"  name="sumMob<?php echo $i ?>" id="sumMob<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">Addres Line1:</font></td><td><input type="text"  name="sumGender<?php echo $i ?>" id="sumAdd1<?php echo $i ?>"/></td>
	<td><font color="black">Address Line2 :</font></td><td><input type="text"  name="sumEmail<?php echo $i ?>" id="sumAdd2<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">City :</font></td><td><input type="text"  name="sumMob<?php echo $i ?>" id="sumCity<?php echo $i ?>"/></td>
	<td><font color="black">Qualification :</font></td><td><input type="text"  name="sumQualification<?php echo $i ?>" id="sumQualification<?php echo $i ?>"/></td></tr>
	<tr><td><font color="black">Type :</font></td><td><input type="text"  name="sumType<?php echo $i ?>" id="sumType<?php echo $i ?>"/></td>
    <td><font color="black">Department :</font></td><td><input type="text"  name="sumDepartment<?php echo $i ?>" id="sumDepartment<?php echo $i ?>"/></td></tr>
    <tr><td><font color="black">Specialization :</font></td><td><input type="text"  name="sumSpecialization<?php echo $i ?>" id="sumSpecialization<?php echo $i ?>"/></td></tr>
    	
	
	</table>
	<script type="text/javascript">
	   		 check_plan("<?php echo $array[$i+2][17]?>",<?php echo $i ?>)
	   		</script>

	  
	
	<?php $j++;
}
?></td></tr>


<tr>
<td><input value="<?php echo $row; ?>" name="no_docs" style="display: none;"/>
<input type="button" id="copy" value="next" class="Manually" style="margin-bottom:150px; margin-left:719px;" onclick="copyForm(<?php echo $row?>)">
</td></tr>

</table>
</form>

<!-- new form for showing the details --><div name="listing" id="listing" style="display:none">
<form >
<table id="view_listing" width='700' border='-1px'  cellpadding='8' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform' style="margin-top: 8px; margin-left: 119px; margin-bottom: 315px; border-color:black;">
		<!--  <tr><th colspan="7">Added Doctors</span></th></tr>-->
			<tr  bgcolor="#a6a6a6">

	<td align='left'><strong>Sl.No</strong></td>
                <td align='left'><strong>Name</strong></td>
                <td align='left'><strong>Gender</strong></td>
                <td align='left'><strong>Email id</strong></td>
                <td align='left'><strong>Mobile Number</strong></td>
                <td align='left'><strong>Type</strong></td>
                <td align='left'><strong>Department</strong></td>
                <td align='left'><strong>Speciality</strong></td>
                <td align='left'></td> 
            </tr>
            
<?php for($k=0;$k<$row;$k++)
{?>
<tr>
<td><div id="sl1<?php echo $k ?>" name="sl1<?php echo $k ?>"/></td>
<td><div id="name1<?php echo $k ?>" name="name1<?php echo $k ?>"/></td>
<td><div id="gender1<?php echo $k ?>" name="gender1<?php echo $k ?>"/></td>
<td><div id="email1<?php echo $k ?>" name="email1<?php echo $k ?>"/></td>
<td><div id="mob_no1<?php echo $k ?>" name="mob_no1<?php echo $k ?>"/></td>
<td><div id="type1<?php echo $k ?>" name="type1<?php echo $k ?>"/></td>
<td><div id="department1<?php echo $k ?>" name="department1<?php echo $k ?>"/></td>
<td><div id="specialization1<?php echo $k ?>" name="specialization1<?php echo $k ?>"/></td>
<td><input type="button" id="popuplink<?php echo $k ?>" value="View Details" onclick="ShowPopUp('<?php echo $k ?>','<?php echo $row ?>')"/><a href="" id="reportlink<?php echo $k ?>" style="display:none" target="_blank">View Report</a></td>
</tr>
<?php }?>
</table>
</form>
<form action="doc_add_reports" method="POST">
<?php for($s=0;$s<$row;$s++)
{?><input type="text" id="report<?php echo $s;?>" style="display:none;"/>
<?php }?>
<input type="button"  onclick="window.location.href='add-new-doctor.php'" value="Back" id="page_next" style="display:none; margin-top:-146px;margin-left:797px;margin-bottom:129px;"/>
</form>
</div>
<?php
}
?>
	<?php 
}
else {?><div id="tot_doc_page" class="message"><font color="red">
<?php 
echo "only excel files can be uploaded..";?></font></div>
<form action="add-new-doctor.php">
		<button type="submit" class="backbtn" >Back</button>
	</form></div>
			 <?php 

}
?>
</td>
</tr>
</table>
<script>
function changeTextbox(id)
{
	var i=id;
	var signup=document.getElementById("signup_plan"+i).value;
	if(signup=="pW-Tele")
	{
		if(document.getElementById("tele_con_no"+i).value=="")
		{alert("Tele consultation number should not be empty for pW-Tele");}
		
		document.getElementById("1online"+i).disabled=true;	
		document.getElementById("5online"+i).disabled=true;	
		document.getElementById("15online"+i).disabled=true;	
		document.getElementById("3min"+i).disabled=false;	
		document.getElementById("15min"+i).disabled=false;	
		document.getElementById("30min"+i).disabled=false;	
		
	}
	else if(signup=="pW-online")
	{
		if(document.getElementById("tele_con_no"+i).value!="")
		{alert("Tele consultation number should be empty for pW-online");}
		document.getElementById("3min"+i).disabled=true;	
		document.getElementById("15min"+i).disabled=true;	
		document.getElementById("30min"+i).disabled=true;	
		document.getElementById("1online"+i).disabled=false;	
		document.getElementById("5online"+i).disabled=false;	
		document.getElementById("15online"+i).disabled=false;	
		
	}
	else if(signup=="pW-combi")
	{
		if(document.getElementById("tele_con_no"+i).value=="")
		{alert("Tele consultation number should not be empty for pW-combi");}
	
		document.getElementById("3min"+i).disabled=false;	
		document.getElementById("15min"+i).disabled=false;	
		document.getElementById("30min"+i).disabled=false;	
		document.getElementById("1online"+i).disabled=false;	
		document.getElementById("5online"+i).disabled=false;	
		document.getElementById("15online"+i).disabled=false;	
		
	}
	
}

</script>

<?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_cards_submenu();
</script>


</body></html>
