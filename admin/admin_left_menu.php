<link href="css/MenuV2.css" rel="stylesheet" type="text/css" />
<script
	src="js/MenuV2.js" type="text/javascript"></script>
<div id="cssmenu" style="width: 228px;">

	<ul id="menuV2">
		<li class='has-sub'><a href="pw_admin.php" class="admin_left_menu">Package
				Management</a>
			<ul class="sub">

				<li><a href="specialist.php" class="admin_left_sub_menu">Specialist</a>
				</li>

				<li><a href="expert.php" class="admin_left_sub_menu">Expert</a></li>

				<li><a href="labtest.php" class="admin_left_sub_menu">Lab Test</a></li>

				<li><a href="dollarRate.php" class="admin_left_sub_menu">Dollar Rate</a>
				</li>

				<li><a href="createPromo.php" class="admin_left_sub_menu">Create
						Promo Code</a></li>

				<li><a href="query.php" class="admin_left_sub_menu">Query</a></li>

				<li><a href="addon_pack.php" class="admin_left_sub_menu">Addon Pack</a>
				</li>

				<li><a href="sign_up.php" class="admin_left_sub_menu">Sign up plan</a>
				</li>

				<!-- 7861 Ratio management-->
				<li><a href="manage_ratio.php" class="admin_left_sub_menu">Manage
						Ratio</a></li>
				<!--end  7861 -->
			</ul>
		</li>



		<!--   <li class='has-sub'>
            <a href="manage-doctor.php" class="admin_left_menu" >Manage Doctors</a>
        <ul><li> 
                <a href="add-new-doctor.php" class="admin_left_sub_menu">Add New Doctor</a>
            </li>

            
             <li>   <a href="doc-login-control.php" class="admin_left_sub_menu">Create Doctor Login</a></li>
           
            
            <li>
                <a href="doc-login-reset.php" class="admin_left_sub_menu">Reset Doctor Login</a></li>
           <li>
                <a href="add-appointment.php" class="admin_left_sub_menu">Create Doctor Schedule</a></li>
            <li>
                <a href="Get-appointment-summary-url.php" class="admin_left_sub_menu">Appointment Summary URL</a></li>
            <li><a href="manage_doctor_clinic_details.php" class="admin_left_sub_menu" >Manage Doctor-Clinic Association</a></li>
       
           <li>     <a href="add_clinic_doctor.php" class="admin_left_sub_menu">Associate Clinics2Doctor</a></li>
           

           
            
            <li>    <a href="doc_wise_clinic_details.php" class="admin_left_sub_menu">Doctor Schedule</a></li>
            </ul></li>
           -->







		<li class='has-sub'><a href="add_user_package.php"
			class="admin_left_menu">Add User Packages</a>
			<ul class="sub">
				<li><a class="admin_left_sub_menu" href="view_added_pakages.php">View
						Added Package</a></li>
			</ul>
		</li>
		<li class='has-sub'><a href="manage-specialities.php"
			class="admin_left_menu">manage Speciality</a>
			<ul class="sub">
				<li><a href="add-specialities.php" class="admin_left_sub_menu">Add
						Speciality</a></li>
			</ul></li>
		<li class='has-sub'><a href="manage-department.php"
			class="admin_left_menu">manage Department</a>

			<ul class="sub">
				<li><a href="add-department.php" class="admin_left_sub_menu">Add
						Department</a></li>
			</ul>
		</li>


		<!-- 7862 Invoice Main menu-->
		<li class='has-sub'><a class="admin_left_menu">Invoice</a>

			<ul class="sub">
				<li class='has-sub'><a class="admin_left_sub_menu">Signup</a>
					<ul class="sub">
						<li><a href="monthly_signup_invoice.php"
							class="admin_left_sub_menu">Monthly Frequency</a></li>
					</ul>
				</li>

				<li class='has-sub'><a href="" class="admin_left_sub_menu">TopUp</a>
					<ul class="sub">
						<li><a href="monthly_invoice_doctor.php"
							class="admin_left_sub_menu">Monthly Frequency(By Doctor)</a></li>

					</ul>
				</li>
			</ul>
		</li>

		<!-- End 7862-->



		<!-- Report Main menu-->
		<li class='has-sub'><a href="" class="admin_left_menu">Report</a>

			<ul class="sub">
				<li class='has-sub'><a href="" class="admin_left_sub_menu">Signup</a>
					<ul class="sub">
						<li><a href="signup_report.php" class="admin_left_sub_menu">Monthly
								Frequency</a></li>
						<li><a href="signup_by_revenue.php" class="admin_left_sub_menu">Monthly
								Revenue</a></li>
					</ul>
				</li>


				<li class='has-sub'><a href="" class="admin_left_sub_menu">Top-Up</a>

					<ul class="sub">
						<li><a href="topup_report_by_numberOf_topups.php"
							class="admin_left_sub_menu">Monthly Frequency</a>
						</li>

						<li><a href="topup_report_by_doctor.php"
							class="admin_left_sub_menu">Monthly Frequency(By Doctor)</a>
						</li>

						<li><a href="topup_revenue.php" class="admin_left_sub_menu">Monthly
								Revenue</a>
						</li>


						<li><a href="topup_revenue_by_doctor.php"
							class="admin_left_sub_menu">Monthly Revenue(By Doctor)</a>
						</li>





					</ul></li>

				<li class='has-sub'><a href="" class="admin_left_sub_menu">Revenue</a>
					<ul class="sub">
						<li><a href="total_revenue.php" class="admin_left_sub_menu">By
								Month</a>
						</li>
					</ul>
				</li>



			</ul>
		</li>
		<!-- End report menu-->



		<li class='has-sub'><a href="manage-doctor.php"
			class="admin_left_menu">Manage Doctors</a>
			<ul class="sub">
				<li><a href="add-new-doctor.php" class="admin_left_sub_menu">Add New
						Doctor</a>
				</li>


				<li><a href="doc-login-control.php" class="admin_left_sub_menu">Create
						Doctor Login</a></li>


				<li><a href="doc-login-reset.php" class="admin_left_sub_menu">Reset
						Doctor Login</a></li>
				<li><a href="add-appointment.php" class="admin_left_sub_menu">Create
						Doctor Schedule</a></li>
				<li><a href="Get-appointment-summary-url.php"
					class="admin_left_sub_menu">Appointment Summary URL</a></li>
				<li><a href="doc_wise_clinic_details.php"
					class="admin_left_sub_menu">Doctor Schedule</a></li>
			</ul></li>
		<li class='has-sub'><a href="clinic.php" class="admin_left_menu">Manage
				Clinics</a>
			<ul class="sub">
				<li><a href="add_clinic.php" class="admin_left_sub_menu">Add New
						Clinic</a></li>

				<li><a href="add_clinic_login.php" class="admin_left_sub_menu">Create
						Clinic Login</a></li>

				<li><a href="clinic-login-reset.php" class="admin_left_sub_menu">Reset
						Clinic Login</a>
				</li>

				<li><a href="add-appointment-clinic.php" class="admin_left_sub_menu">Create
						Doctors Schedule</a></li>


				<li><a href="manage_clinic_doctors.php" class="admin_left_sub_menu">Manage
						Clinic-Doctor Association</a></li>

				<li><a href="add_doctors_to_clinic.php" class="admin_left_sub_menu">Associate
						Doctors2Clinic</a></li>

				<li><a href="clinic_wise_doc_details.php"
					class="admin_left_sub_menu">Doctors Schedule</a></li>
			</ul></li>
		<li><a href="admin_profile.php" class="admin_left_menu">Admin Profile</a>
		</li>
		<li><a href="admin_changepassword.php" class="admin_left_menu">Change
				Password</a>
		</li>
		<li class='has-sub'><a href="manage_cards.php" class="admin_left_menu">Manage
				Cards</a>
			<ul class="sub">
				<li><a href="generate_cards.php" class="admin_left_sub_menu">Generate
						Cards</a></li>
			</ul></li>

		<li class='has-sub'><a href="manage-group.php" class="admin_left_menu">Manage
				Groups</a>
			<ul class="sub">
				<li><a href="add_group.php" class="admin_left_sub_menu">Add Group</a>
				</li>
			</ul></li>
		<li><a href="manage-user.php" class="admin_left_menu"> Manage Users</a>
		</li>
		<li class='has-sub'><a href="purchase_datails.php"
			class="admin_left_menu">Order Details </a>
			<ul class="sub">
				<li><a href="order_details.php" class="admin_left_sub_menu">Doctor
						Order Details</a></li>
				<li><a href="q_a_Counsellor.php" class="admin_left_sub_menu">Counsellor
						Query</a></li>
				<li><a href="q_a_Doctor.php" class="admin_left_sub_menu">Doctor
						Query</a></li>
				<li><a href="q_a_dietician.php" class="admin_left_sub_menu">
						Dietician Query</a></li>
			</ul></li>
		<li class='has-sub'><a href="user_mail.php" class="admin_left_menu">Manage
				Mails</a>
			<ul class="sub">
				<li><a href="saved_email.php" class="admin_left_sub_menu">Saved
						Mails</a></li>
				<li><a href="sent-mail.php" class="admin_left_sub_menu">Sent Mail</a>
				</li>
			</ul></li>
		<li class='has-sub'><a href="manage_sms.php" class="admin_left_menu">Manage
				SMS</a>

			<ul class="sub">
				<li><a href="sent_sms.php" class="admin_left_sub_menu">Sent SMS</a>
				</li>
			</ul></li>
		<li class='has-sub'><a href="pending_from_expert_doctors.php"
			class="admin_left_menu">Consultation Status & Reports</a>
			<ul class="sub">
				<li><a href="expert_consultation_details.php"
					class="admin_left_sub_menu">pinkOpinions</a></li>
				<li><a href="specialist_consultation_details.php"
					class="admin_left_sub_menu">pinkConsults</a></li>
					<li>
        <a href="diagnostic_consultation_details.php" class="admin_left_sub_menu" >pinkDiagnostics</a></li>
				<li><a href="query_consultation_details.php"
					class="admin_left_sub_menu">pinkQueries</a></li>

				<li><a href="online_summary_report.php" class="admin_left_sub_menu">Summary
						Report</a></li>

				<li><a href="month-wise-online-consultation.php"
					class="admin_left_sub_menu">Week wise Report</a></li>
				<li><a href="week-wise-online-report.php"
					class="admin_left_sub_menu">Day wise Report</a></li>

				<li><a href="monthly-online-graph.php" class="admin_left_sub_menu">Monthly
						Online Statistics</a></li>
				<li><a href="monthly-tele-graph.php" class="admin_left_sub_menu">Monthly
						Tele Statistics</a></li>


			</ul></li>

		<li class='has-sub'><a href="booked_appointment_details.php"
			class="admin_left_menu">Appointment Booking</a>
			<ul class="sub">
				<li class='has-sub'><a href="book_patient_appointment.php"
					class="admin_left_sub_menu">Book Appointment</a>
					<ul class="sub">
						<li><a href="book_patient_appointment.php"
							class="admin_left_sub_menu">Clinic</a></li>

						<li><a href="book_appointment_for_doctor.php"
							class="admin_left_sub_menu">Doctor</a></li>
					</ul></li>

				<li><a href="cancel_patient_appointment.php"
					class="admin_left_sub_menu">Cancel Appointment</a></li>
				<li><a href="weekly_appointment_details.php"
					class="admin_left_sub_menu">Appointment Summary</a></li>

				<li class='has-sub'><a href="Appointment_patient_details.php"
					class="admin_left_sub_menu">Patient Details</a>
					<ul class="sub">
						<li><a href="Appointment_patient_details.php"
							class="admin_left_sub_menu">Clinic</a></li>

						<li><a href="doc_Appointment_patient_details.php"
							class="admin_left_sub_menu">Doctor</a></li>
					</ul></li>
					
					
			</ul>
			
			<li><a href="content_manager_login.php" class="admin_left_menu"> Create Content Manager Login</a>
		</li>

</div>




