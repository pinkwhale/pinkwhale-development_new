<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
<style>
    
#loading { 
width: 70%; 
position: absolute;
}
</style>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top" id="mainBg">
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            
    ?>
    <form action="actions/add_doctors_clinic.php" method="post" name="doctor_to_clinic" id="doctor_to_clinic">
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Associate Doctors2Clinic</th></tr>
         
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Clinic<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic"   onchange="get_num()">
                    <option value="" selected="selected" disabled>-----select Clinic-----</option>
                    <?php
                        $qry = "select clinic_id,name from clinic_details where status=0 and name<>''";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id']."|".$data['name']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>        
         <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td id="doc_id_1" colspan="2" width="40%" align="center" bgcolor="#F5F5F5">
               Alloted No. of Doctors &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' readonly name='remaining_num_doc' value="0" id='remaining_num_doc' size='3' maxlength='2' />
            </td>        
        </tr>
         <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="num_docErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
              
        <tr>
            <td>
                <div id="loading" align="center"></div>
            </td>
                   
        </tr>
        
        <tr>
            <td colspan="2" id="added_doctor" >
               
            </td>
                   
        </tr>
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Doctor<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="doctor[]" id="doctor" multiple size="10" >
                    <option value="" selected="selected" disabled>-----select Doctor-----</option>
                    
                </select>
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Save" tabindex="6" type="button"  onclick="add_doctors_to_clinic(doctor_to_clinic)"  /></td>        
        </tr>
    </table>
    </form>
    <?php
        }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
    enable_clinic_submenu();

    // enable_clinic_doctor_association();
    
    function get_num(){
        var clinic_id=encodeURI(document.getElementById('clinic').value);
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#doc_id_1').load('get-clinic-num.php?clinic_id='+clinic_id);
        $('#doctor').load('get-clinic-doctors.php?clinic_id='+clinic_id);
        $('#added_doctor').load('added_doctors_to_clinic_details.php?clinic_id='+clinic_id,Hide_Load());
    }
    
    function Hide_Load()
    {
        $("#loading").fadeOut('slow');
    };
</script>
</html>
<?php
ob_end_flush();
?>
