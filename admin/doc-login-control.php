<?php
  	include("Sajax.php");
	sajax_init();
	sajax_export('getdata');
	sajax_handle_client_request();
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cards.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/doc-changepassword-validation.js"></script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<?php include "../includes/pw_db_connect.php"; ?>
<?php include "admin_head.php"; ?>
<script type="text/javascript">
<?php sajax_show_javascript(); ?>
</script>
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
<td width="850" valign="top" id="mainBg">

<form action="actions/edit_doc_password.php" method="post" name="changePassword" id="changePassword">
  <table width="521" border="0" cellspacing="0" cellpadding="0" align="center" >
  	<tr><td width="717"><table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform">
        <tr><th colspan="2" align="left">Create Doctor Login</th></tr>
        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
  		<tr></tr>
        	<td width="40%" align="right" bgcolor="#F5F5F5">Doctor Type<font color="#FF0000">*</font>:</td>
  			
            <td width="60%" align="left" bgcolor="#F5F5F5">
  				 <select  name="doc_type" id="doc_type" class="registetextbox" onChange="setdoctorlist();" > 
			   <option value="" selected="selected" disabled>-Select doctor type-</option>
               <option value="Specialist">Specialist </option>
               <option value="Expert">Expert</option>
               <option value="Counsellor">Counsellor</option>
	       <option value="appointment">Clinic Appointment</option>
               <option value="query">Query</option>
  		</select>
        	</td>
        </tr>
        
<tr>
    <td></td>
    <td height="10"><div id="doc_type_div" style="color: #F33;font-family:verdana;font-size:10px;"></div></td>
</tr> 

<tr>
	 <td align="right" bgcolor="#F5F5F5">Doctor Name<font color="#FF0000">*</font>:</td>
     <td nowrap="nowrap" bgcolor="#F5F5F5"><select  name="doctor_list" id="doctor_list" class="registetextbox" onChange="setdoctoremailid();"> 
			  <option value="" >- Select Doctor-</option>
               
        </select>
       </td>
</tr>

<tr>
     <td></td>
     <td height="10" >
     <div id="doc_name_div"  style="color: #F33;font-family:verdana;font-size:10px; "></div>
     </td>
 </tr> 

<tr>
        <td align="right" bgcolor="#F5F5F5">Doctor Email ID<font color="#FF0000">*</font>:</td>
  			
        <td bgcolor="#F5F5F5" id="doc_emailid"><input  size="30"  id="doc_emailid" name="doc_emailid" />
        <span style="color:green" id='result'></span></td>
</tr>
       
<tr>
 	<td></td>
 	<td height="10"><div id="doc_emailid_div" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
</tr>
 
<tr>
	<td align="right"  bgcolor="#F5F5F5">Password :</td>
	<td  bgcolor="#F5F5F5"><input type="password" size="30"  id="doc_password" name="doc_password" />
    </td></tr>
    
<tr>
	<td></td>
 	<td height="10"><div id="doc_password_div" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
 </tr>
 
 <tr>
	<td align="right"  bgcolor="#F5F5F5">Confirm Password :</td>
	<td  bgcolor="#F5F5F5"><input type="password" size="30"  id="confirm_doc_password" name="confirm_doc_password" />
    </td></tr>
 
 <tr>
	<td></td>
 	<td height="10"><div id="confi_password_div" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
 </tr> 
 
<tr>
 	<td></td>
 	<td><div id="repasswordErrDiv" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
 </tr> 

<tr>
     <td bgcolor="#F5F5F5">&nbsp;</td>
     <td bgcolor="#F5F5F5">
  				&nbsp;&nbsp;&nbsp;&nbsp;<input onmouseover="this.style.cursor='pointer'"  value="Save" tabindex="6" type="button" onclick="chechValidation(changePassword)"></td>
        </tr>
      </table>
   </td></tr> </table>
  </form>
</td></tr></table>

  <?php
include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_doc_submenu();
</script>
</body></html>
