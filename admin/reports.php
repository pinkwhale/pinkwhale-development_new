<?php
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
include("includes/host_conf.php");
include("includes/mysql.lib.php");


$id_report=$_GET['id_report'];

//$id_report=15;
$report_d=mysql_query("SELECT * FROM pw_reports a JOIN pw_doctors b ON b.doc_id=a.id_doc WHERE a.id_invoice=".$id_report."");
while($report_row=mysql_fetch_array($report_d))
{
	$doc_name=$report_row['doc_name'];
	$category=$report_row['doc_category'];
	$doc_dept=$report_row['doc_dept'];
	$doc_specialities=$report_row['doc_specialities'];
	$doc_qualification=$report_row['doc_qualification'];
	$city=$report_row['city'];
	$addressline_1=$report_row['addressline_1'];
	$address_line2=$report_row['address_line2'];
	$doc_mobile_no=$report_row['doc_mobile_no'];
	$doc_email_id=$report_row['doc_email_id'];
	$doc_gender=$report_row['doc_gender'];
	$doc_dept=$report_row['doc_dept'];
	$id_invoice=$report_row['id_invoice'];
	$sign_plan=$report_row['sign_plan'];
	$sp_price=$report_row['sp_price'];
	$sp_cards=$report_row['sp_cards'];
	$addon=$report_row['addon'];
	$add_price=$report_row['add_price'];
	$add_cards=$report_row['add_cards'];
	$date=$report_row['date'];
	
	
}
require('fpdf/fpdf.php');


class PDF extends FPDF
{

	function Header()
	{
		//Logo
		$this->SetFont('Arial','B',6);
		$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
		$this->Image('fpdf/logo.jpg',160,6,40);
		$this->Ln(3);$this->SetFont('Arial','B',5);
		$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
		$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
		$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);
		
		$name="Export PDF";
		$this->SetFont('Arial','B',15);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->SetFont('Arial','B',9);
		//Line break
	$this->Line(0, 25, 300, 25);
	$this->Ln(4);
	$this->SetFont('Arial','BU',9);
	$this->Cell(80);
		$this->Cell(80,10,"INVOICE");
		$this->Ln(15);
	}
	function doc_details($doc_name,$addressline_1,$address_line2,$city,$doc_mobile_no,$doc_email_id,$doc_gender,$category,$doc_dept,$doc_specialities,$doc_qualification,$id_report,$date,$sign_plan,$sp_price,$sp_cards,$addon,$add_price,$add_cards){
			$this->SetFont('Arial','B',6);
$this->Cell(185,50,"",1);
$x=$this->GetX();
$y=$this->GetY();
$this->SetX($this->GetX() - 180);
$this->Cell(15,7,"Name :");$this->Cell(20,7,"$doc_name");
/*$name_x=$this->GetX();
$name_y=$this->GetY();*/
$this->Ln(5);$this->Cell(5,7);$this->Cell(15,7,"Gender:");$this->Cell(20,7,"$doc_gender");
$this->Ln(5);
$this->Cell(5,7);
$this->Cell(15,7,"Address :");$this->Cell(20,7,"$addressline_1");$this->Ln(4);
$this->Cell(5,7);$this->Cell(15,7);$this->Cell(20,7,"$address_line2");
$this->Ln(5);$this->Cell(5,7);$this->Cell(15,7,"City:");$this->Cell(20,7,"$city");$this->Ln(5);
$this->Cell(5,7);$this->Cell(15,7,"Email Id:");$this->Cell(20,7,"$doc_email_id");
$this->Ln(5);$this->Cell(5,7);$this->Cell(15,7,"Mobile No:");$this->Cell(20,7,"$doc_mobile_no");
$x=$this->GetX();
$y=$this->GetY();
$this->SetY($this->GetY()-29);
$this->SetX($this->GetX()+80);
$this->Cell(15,7,"Category:");$this->Cell(20,7,"$category");
$this->Ln(5);
$this->SetX($this->GetX()+80);$this->Cell(15,7,"Department:");$this->Cell(20,7,"$doc_dept");
$this->Ln(5);
$this->SetX($this->GetX()+80);$this->Cell(15,7,"Speciality:");$this->Cell(20,7,"$doc_specialities");
$this->Ln(5);
$this->SetX($this->GetX()+80);$this->Cell(15,7,"Qualification:");$this->Cell(20,7,"$doc_qualification");
$x=$this->GetX();
$y=$this->GetY();
$this->SetY($this->GetY()-15);
$total=$sp_price+$add_price;
$this->SetX($this->GetX()+150);$this->Cell(15,7,"Invoice Date:");$this->Cell(20,7,"$date");$this->Ln(5);
$this->SetX($this->GetX()+150);$this->Cell(15,7,"Invoice No:");$this->Cell(20,7,"$id_report");$this->Ln(50);
$this->Cell(15,7,"sl no:",1);$this->Cell(25,7,"Plan Name",1);$this->Cell(25,7,"No of Cards",1);$this->Cell(25,7,"Price:",1);$this->Ln(7);
$this->Cell(15,7,"1",1);$this->Cell(25,7,"$sign_plan",1);$this->Cell(25,7,"$sp_cards",1);$this->Cell(25,7,"$sp_price",1);$this->Ln(7);
$this->Cell(15,7,"2",1);$this->Cell(25,7,"$addon",1);$this->Cell(25,7,"$add_cards",1);$this->Cell(25,7,"$add_price",1);$this->Ln(7);
$this->Cell(65,7,"Total :",1);$this->Cell(25,7,"$total",1);
/*$this->SetX($this->GetX()+20);
$this->SetY($this->GetY()-2);
//$this->Cell(20,15,"$addressline_1");
//$this->MultiCell(60, 5, "\nName: $doc_name\nAddress: $addressline_1\n$address_line2");
$this->Ln(4);
/*$address_x=$this->GetX();
$address_y=$this->GetY();
$this->SetX($this->GetX()+ 20);
$this->SetY($this->GetY()-58);
$this->Cell(20,10,"$doc_name");
/*$this->MultiCell(60, 4, "\nName: $doc_name\nAddress: $addressline_1\n$address_line2");
	$this->SetXY($x + 120, $y);
	
		$this->Cell(20,10,"Name: $doc_name");
		$this->Cell(20,10,$doc_name);
		$this->Cell(20,10,"Doctor's Name:");
		$this->Cell(20,10,$doc_name);
		$this->Ln(20);
		$this->Cell(30,10,"Doctor's Name:");
		$this->Cell(40,10,$doc_name);*/
	}
	//Page footer
	function Footer()
	{
		 
	}

	//Load data
	function LoadData($file)
	{
		//Read file lines
		$lines=file($file);
		$data=array();
		foreach($lines as $line)
			$data[]=explode(';',chop($line));
		return $data;
	}
function reference_table($id_invoice,$date)
{$this->Ln(4);
$x=$this->GetX();
$y=$this->GetY();
$this->SetFont('Arial','B',7);
	$this->MultiCell(60, 4, "Invoice Number:\n$id_invoice",1);
	$this->SetXY($x + 60, $y);
	$this->MultiCell(60, 4, "Invoice Date:\n$date",1);
	$this->SetXY($x + 120, $y);
	
	$this->MultiCell(60, 4, "Reference Number:\n49412",1);
	$this->MultiCell(180, 3, "Buyer:\n
	c_name\n
	Address:\n
	c_address\n",1);
	
	$this->Ln(10);
}
	//Simple table
	function BasicTable($header,$data,$f_date,$t_date,$doc_name,$p_account,$d_account,$c_address,$c_name)
	{
		$this->SetFillColor(242,39,201);
		$this->SetTextColor(255);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		//$this->SetFillColor(0,255,0);
		//$this->SetDrawColor(128,0,0);
		//$w=array(40,20,35,30,50,15,10,10,15,15,15,15,15);
		$w=array(20,20,35,30,30,25,25,10,15,15,15,15,15);
		//Header
		
}

function b_details()
{$this->Ln(20);
	$this->Cell(70,6,"Income Tax Permanent Account Number (PAN): ");$this->Ln(6);
	$this->Cell(70,6,"Service Tax Code (STC) Number: ");$this->Ln(20);
	$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(15);
	$this->MultiCell(70, 4,"Anita Shet\n
Authorized Signatory
			");
}





}
//$pdf = new FPDF();
$pdf=new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',5);
$pdf->doc_details($doc_name,$addressline_1,$address_line2,$city,$doc_mobile_no,$doc_email_id,$doc_gender,$category,$doc_dept,$doc_specialities,$doc_qualification,$id_report,$date,$sign_plan,$sp_price,$sp_cards,$addon,$add_price,$add_cards);
//$pdf->reference_table($id_invoice,$date);
//$pdf->Cell(40,10,''.$date.'');
$pdf->b_details();
$pdf->Output();

?>