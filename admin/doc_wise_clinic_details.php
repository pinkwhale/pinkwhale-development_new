
<?php
ob_start();
  	include("Sajax.php");
	sajax_init();
	sajax_export('block_fun');
	sajax_handle_client_request();
?>
<?php 
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />

<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript">
	<?php sajax_show_javascript(); ?>
	function fun_block_doc(block_id)
	{
		x_block_fun(block_id,pw_doctors);
	}
	function pw_doctors(details) 
	{
		location.reload(true);
	}
</script>

</head>
    
<body >
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" id="mainBg" valign="top"> 
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            
    ?>
   
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Doctor Schedule</th></tr>
         
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Select Doctor<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="docid" id="docid"   onchange="get_clinicdetails()">
                    <option value="" selected="selected" disabled>-----select Doctor-----</option>
                    <?php
                        $qry = "select doc_id,doc_name from pw_doctors where appoint_flag=1 and blocked<>'Y'";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['doc_id']."|".$data['doc_name']."'>".$data['doc_name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
		</table>
		<table >    
        
        <tr>
            <td id="doc_id_1" width="40%" align="center" >
              
            </td>        
        </tr>
        
    </table>
    
    <?php
        }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
   // enable_clinic_submenu();
    enable_doc_submenu();
    
    function get_clinicdetails(){
        var doc_id=encodeURI(document.getElementById('docid').value);
        $('#doc_id_1').load('get_doc_clinic_details.php?doctor_id='+doc_id);
    }
</script>
</html>
<?php
ob_end_flush();
?>
<script type="text/javascript">
 	//enable_doc_submenu();
	enable_manage_doc_clinic();
</script>
