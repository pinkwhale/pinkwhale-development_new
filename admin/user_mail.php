<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<?php include_once("cuteeditor_files/include_CuteEditor.php") ; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/add-new-doctor-validation.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="scripts/wysiwyg.js"></script>
	<script type="text/javascript" src="../nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}

.mailselectionbox1 {
    border-color: #666666 #DDDDDD #CCCCCC #333333;
    border-style: solid;
    border-width: 2px 2px 1px;
	font-size: 11px;
    color: #333333;
    font-family: Arial,Verdana;
    font-weight: bold;
    height: 22px;
    padding: 0 5px 3px;
    width: 200px;
	height:200px;
}

.mailselectionbox {
    border-color: #666666 #DDDDDD #CCCCCC #333333;
    border-style: solid;
    border-width: 2px 2px 1px;
	font-size: 11px;
    color: #333333;
    font-family: Arial,Verdana;
    font-weight: bold;
    height: 22px;
    padding: 0 5px 3px;
    width: 200px;
}

#BVQAFieldTextQuestionSummaryID {
    border-color: #666666 #DDDDDD #CCCCCC #333333;
    border-style: solid;
    border-width: 2px 2px 1px;
	font-size: 11px;
    color: #333333;
    font-family: Arial,Verdana;
    font-weight: bold;
    height: 22px;
    padding: 0 5px 3px;
    width: 186px;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?>
</td><td valign="top" style="margin-top: 20px;">
<form name="send_save_mail" id="send_save_mail" action="actions/send_save_email.php" method="post">
<table width='750' border='0' bordercolor="#FF0000" cellpadding='0' cellspacing='1' align='left' >
<tr><td height="40" colspan="2">&nbsp;</td></tr>
<?php if(isset($_SESSION['save_message'])) { ?>
<tr bgcolor="#008000"><td colspan="2" align="center"><span style="font-family: Arial,Verdana;font-size: 16px;color:#FFFFFF;"><b><?php 
echo $_SESSION['save_message'];

 ?></b></span></td></tr>

<?php }?>
<tr><td height="40" colspan="2" valign="top"><span style="font-family: Arial,Verdana;font-size: 16px;margin-left: 40px;color:#E70976;"><b>Manage Mails :</b></span></td></tr>
<tr><td width="141" align="right"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 40px;"><b>Select Users :</b></span></td>
<td width="600">&nbsp;<select name="customer_type" id="customer_type" class="mailselectionbox" onchange="setUsersList()">
<option value="" selected="selected">-- Select --</option>
<option value="all">All Users</option>
<option value="usersService">Users by service</option>
<option value="usersGroup">Users by group</option>
<option value="notactivated">Users not activated</option>
<option value="selectedusers">Selected Users</option>
</select></td></tr>

<?php if($_SESSION['customer_type']!="") 
	{?>
    
	 <script type="text/javascript">
    type1='<?php echo $_SESSION['customer_type']; ?>';
    document.getElementById('customer_type').value=type1;
    document.getElementById('customer_type').selected=true;
	</script>

<?php }?>

<?php if(isset($_SESSION['users_group'])) 
	{?>
<tr><td colspan="2">
<div id="users_group" style="display:block;">
<table style="margin-left: 60px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td valign="top"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 30px;"><b>Group :</b></span></td>
<td>&nbsp;<select name="users_group" id="users_group"  class="mailselectionbox">
<option value="">-- Select --</option>
<?php $qry="SELECT `group_name` FROM groups GROUP BY `group_name`";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{        
	?>	
<option value="<?php echo $qry_result['group_name']; ?>"><?php echo $qry_result['group_name']; ?></option>
<?php } 
?>
</select></td></tr>
</table>
</div>
</td></tr>
<?php
}
else {
?>

<tr><td colspan="2">
<div id="users_group" style="display:none;">
<table style="margin-left: 60px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td valign="top"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 30px;"><b>Group :</b></span></td>
<td>&nbsp;<select name="users_group" id="users_group"  class="mailselectionbox">
<option value="">-- Select --</option>
<?php $qry="SELECT `group_name` FROM groups GROUP BY `group_name`";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{        
	?>	
<option value="<?php echo $qry_result['group_name']; ?>"><?php echo $qry_result['group_name']; ?></option>
<?php } ?>
</select></td></tr>
</table>
</div>
</td></tr>
<?php } ?>
<tr><td colspan="2">
<?php if($_SESSION['users_by_service']!="") 
	{?>
    
<div id="users_services" style="display:block;">
<table style="margin-left: -14px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 45px;"><b>Users by service :</b></span></td>
<td>&nbsp;
<select name="users_by_service" id="users_by_service" class="mailselectionbox"  >
<option value="">-- Select --</option>
<option value="expertopinion">Expert opinion</option>
<option value="specialist">Specialist</option>
<option value="counseller">Counsellor</option>
</select>
</td>

</tr>
</table>
</div>    
	 <script type="text/javascript">
    type2='<?php echo $_SESSION['users_by_service']; ?>';
    document.getElementById('users_by_service').value=type2;
    document.getElementById('users_by_service').selected=true;
	</script>

<?php }else {   ?>

<div id="users_services" style="display:none;">
<table style="margin-left: -14px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 45px;"><b>Users by service :</b></span></td>
<td>&nbsp;
<select name="users_by_service" id="users_by_service" class="mailselectionbox" onchange="get_doc()">
<option value="">-- Select --</option>
<option value="expertopinion">Expert opinion</option>+-
<option value="specialist">Specialist</option>
<option value="counseller">Counsellor</option>
</select>
</td>

</tr>
</table>
</div>

<?php } ?>


<div id="doctor_id" style="display:none;">
<table style="margin-left: -14px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 45px;"><b>select Doctor :</b></span></td>
<td>&nbsp;
<select name="doctor" id="doctor" class="mailselectionbox" onchange="get_patient()">
<option value="">-- Select --</option>
</select>
</td>

</tr>
</table>
</div>

        
        
        
</td></tr>
<tr><td colspan="2">
<?php if($_SESSION['users_name']!="") 
	{ ?>   
		<div id="users_name" style="display:block;">
<table style="margin-left: 60px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td valign="top"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 45px;"><b>Users :</b></span></td>
<td>&nbsp;<select name="users_name[]" id="users_name"  multiple="multiple" class="mailselectionbox1" >
<option value="">-- Select --</option>
<?php $qry="SELECT * FROM user_details where user_email !=''  order by `user_name` ASC";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{        
	?>	
<option value="<?php echo $qry_result['user_id']; ?>"><?php echo $qry_result['user_name']; ?></option>
<?php } ?>
</select></td></tr>
</table>
</div>

	 <script type="text/javascript">
    type2='<?php echo $_SESSION['users_name']; ?>';
    document.getElementById('users_name').value=type2;
    document.getElementById('users_name').selected=true;
	</script>


<?php } 
else {
?>

<div id="users_name" style="display:none;">
<table style="margin-left: 60px;">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td valign="top"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 45px;"><b>Users :</b></span></td>
<td>&nbsp;<select name="users_name[]" id="users_name" multiple="multiple" class="mailselectionbox1" >
<option value="">-- Select --</option>
<?php $qry="SELECT * FROM user_details where user_email !='' order by `user_name` ASC ";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{        
	?>	
<option value="<?php echo $qry_result['user_id']; ?>"><?php echo $qry_result['user_name']; ?></option>
<?php } ?>
</select></td></tr>
</table>
</div>
<?php } ?>
</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td width="141" align="right"><span style="font-family: Arial,Verdana;font-size: 13px;margin-left: 40px;"><b>Subject :</b></span></td>
<td>&nbsp;


<?php if($_SESSION['mail_subject']!="")
{
	$value=$_SESSION['mail_subject'];
?>
<input id="BVQAFieldTextQuestionSummaryID" type="text" autocomplete="off"  name="mail_subject" value="<?php echo $value;?>"  />
<?php } 
elseif($_GET['mail_id']) {
	
	$mail_id=$_GET['mail_id'];
$qry="SELECT * FROM save_mail where mail_id ='$mail_id' ";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{ 
		
			$subject=$qry_result['mail_subject'];
			$contents=$qry_result['mail_contents'];
		
		}?>
			
	<input id="BVQAFieldTextQuestionSummaryID" type="text" autocomplete="off"  name="mail_subject" value="<?php echo $subject;?>"  />	
		
	<?php	
		 }
		 
	else {	 ?>
         <input id="BVQAFieldTextQuestionSummaryID" type="text" autocomplete="off"  name="mail_subject"  />
         <?php } ?>	
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2" align="center">&nbsp;
<table>
<tr><td><textarea name="area1" style="width:600px;height:200px;">
 <?php 
 if(isset($_SESSION['mail_editor']))
 {
	echo $_SESSION['mail_editor']; 
 }
 else if($_GET['mail_id'])  echo $contents;
 ?>
</textarea>
</td></tr>
</table>
		<?php
		
           /* $editor=new CuteEditor();
            $editor->ID="Editor1";
			if ($_SESSION['mail_editor']!=""){ 
			$text_content=$_SESSION['mail_editor'];
	$text_content=str_replace("%5C%22", "", $text_content);
$text_content=str_replace("\&quot;", "", $text_content);
$text_content=str_replace("\"", "", $text_content);
$text_content=str_replace("/Uploads", "http://www.pinkwhalehealthcare.com/Uploads", $text_content);
$text_content=str_replace("/test1/admin", "http://www.pinkwhalehealthcare.com/test1/admin", $text_content);
$text_content=stripslashes($text_content);
			
			$editor->Text=$text_content; }
			else if($_GET['mail_id']) { $editor->Text=$contents; }
			else {         $editor->Text="Type here"; }
            $editor->EditorBodyStyle="font:normal 12px arial;";
            $editor->EditorWysiwygModeCss="php.css";
            $editor->Draw();
            $editor=null;*/
            
            //use $_POST["Editor1"]to retrieve the data
        ?>
</td></tr>
<tr height="50"><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">
<table width="750" border="0"> 
<tr><td width="578" align="right"><input type="submit"  value="Save" name="save" onmouseover="this.style.cursor='pointer'" alt="Save"/></td>
<td width="156"><input type="submit" onmouseover="this.style.cursor='pointer'" value="Send" name="send"/></td>
</tr>
</table>
</td></tr>
</table>
</form>
</td></tr>
</table>
<script type="text/javascript">
function setUsersList()
{
	var user_type=encodeURI(document.getElementById('customer_type').value);

	if(user_type=='usersService')
	{
		document.getElementById("users_services").style.display="block";
		document.getElementById("users_name").style.display="none";
		document.getElementById("users_group").style.display="none";
	}
	else if(user_type=='selectedusers')
	{
		document.getElementById("users_services").style.display="none";
		document.getElementById("users_name").style.display="block";
		document.getElementById("users_group").style.display="none";	
	}
	else if(user_type=='usersGroup')
	{
		document.getElementById("users_services").style.display="none";
		document.getElementById("users_name").style.display="none";
		document.getElementById("users_group").style.display="block";			
	} 
	else 
	{
		document.getElementById("users_services").style.display="none";
		document.getElementById("users_name").style.display="none";
		document.getElementById("users_group").style.display="none";		
	}


}


function get_doc(){

        var doc_type = encodeURI(document.getElementById('users_by_service').value);
        
        if(document.getElementById('users_by_service').value=="specialist"){            
            document.getElementById("doctor_id").style.display="block";	
            $('#doctor').load('get-doctor-name.php?doctor_type='+doc_type);
        }
        else{
            document.getElementById("doctor").innerHTML="<option value='' selected='selected'></option>";
            document.getElementById("doctor_id").style.display="none";
        }
}



 function enable_savemail_submenu()
	{
		document.getElementById("submenu14").style.display="block";
		document.getElementById("submenu20").style.display="block";
	}
</script>
 <?php include 'admin_footer.php';
 if($_SESSION['customer_type']) unset($_SESSION['customer_type']);
if($_SESSION['users_by_service']) unset($_SESSION['users_by_service']);
if($_SESSION['mail_subject']) unset($_SESSION['mail_subject']);
if($_SESSION['mail_editor']) unset($_SESSION['mail_editor']);
if($_SESSION['users_name']) unset($_SESSION['users_name']); 
if($_SESSION['save_message']) unset($_SESSION['save_message']); 
if($_SESSION['users_group']) unset($_SESSION['users_group']);
ob_end_flush();
  ?>
<script type="text/javascript">
enable_savemail_submenu();
</script>
</body></html>
