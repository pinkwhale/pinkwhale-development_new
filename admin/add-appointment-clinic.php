<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../pwconstants.php");
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
        require_once('calendar/classes/tc_calendar.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/appointment.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css" />
<script type="text/javascript" src="js/timepicker/rainbow.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/rainbow.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<script type="text/javascript"> 
$(function() {
    
    $('#fromtime_slot1').timepicker({
	'minTime': '07:00am',
	'maxTime': '11:45am'
    });

    $('#fromtime_pm').timepicker({
	'minTime': '07:00am',
	'maxTime': '11:45pm'
    });

     $('#fromtime_slot2').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '11:45am'
     });

    $('#fromtime_slot3').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '11:45am'
     });
    
    $('#fromtime_slot4').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '11:45am'
    });
});
  
  
  function set_to_time(slot){
      var time = document.getElementById("fromtime_slot"+slot).value;
      if(time!=""){
              $('#totime_slot'+slot).timepicker({
                    'minTime': time,
                    'maxTime': '11:59pm'
          });
   document.getElementById("app_duration_slot"+slot).value="";
      }
  }
  
  
  function set_from_time(slot){
      var time = document.getElementById("totime_slot"+slot).value;
      var nextSlot = slot+1;
       $('#fromtime_slot'+nextSlot).timepicker({
                    'minTime': time,
                    'maxTime': '10:59pm'	
       });
       document.getElementById("app_duration_slot"+slot).value="";
  }
</script>
<style>
    
#loading { 
width: 70%; 
position: absolute;
}
</style>
<style type="text/css">
.ui-dialog { font-size: 11px; }
body {
        font-family: Tahoma;
        font-size: 12px;
}
#question {
        width: 300px!important;
        height: 60px!important;
        padding: 10px 0 0 10px;
}
#question img {
        float: left;
}
#question span {
        float: left;
        margin: 20px 0 0 10px;
}
.ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
</head>
    
<body >
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top" id="mainBg" >
    <?php
        if($_SESSION['msg']!=""){
            echo "<br /><center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else if($_SESSION['error']!=""){
            echo "<br /><center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
            $_SESSION['error'] = "";
        }
    ?>
    <form action="actions/add_clinic_appointment.php" method="post" name="doctor_appointment" id="doctor_appointment">
     <table border="0" cellpadding="0" cellspacing="1" width="772" align="center" class="s90registerform" style="margin-left:20px;">
        <tr><th colspan="2">Create Doctors Schedule</th></tr>
        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td  align="left" bgcolor="#F5F5F5"><b>Clinic</b><font color="#FF0000">*</font>:</td>
            <td  align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic"  onchange="get_doctors()">
                    <option value="" selected="selected">-----select Clinic-----</option>
                    <?php
                        $qry = "select clinic_id,name from clinic_details";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id']."|".$data['name']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
        
        <!--    ERROR DIV -->
  	    <tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr>
            <td>
                <div id="loading" align="center"></div>
            </td>
        </tr>
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr><td colspan="2"><div id="doc_clinic_details"></div></td></tr>
        <tr>
            <td colspan="2" id="doc_id_1" width="40%" align="center" >
              
            </td>        
        </tr>
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td align="left" bgcolor="#F5F5F5"><b>Doctor</b><font color="#FF0000">*</font>:</td>
            <td align="left" bgcolor="#F5F5F5">
                <select name="doctor" id="doctor"  onchange="get_doctors_slots()">
                    <option value="" selected="selected" disabled>-----select doctor-----</option>
                    
                </select>
                
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr>
            <td  align="left" bgcolor="#F5F5F5"><b>Appointment by Day</b><font color="#FF0000">*</font>:</td>
            <td  align="left" bgcolor="#F5F5F5">
           		 <?php  
						$cols = 3;
						$count = count($work_days);
						echo "<table border='0'>";
						echo "<tr><td><input type='checkbox' name='day' id='all' onclick='checkall()' value='all'/>All</td></tr>";
						foreach($work_days as $key => $td){
							if($key%$cols == 0) echo "<tr>";
							echo "<td><input type='checkbox' name='day[]' id='day' value='$key'/>$td</td>";
							if($key%$cols == ($cols - 1)) echo "</tr>";
							}
						echo "</table>";?> 
                
            </td>        
        </tr>        
        <!--    ERROR DIV -->
	    <tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="dayErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV -->
         
        <tr>
            <td colspan="2" align="center" bgcolor="#F5F5F5">
                <table border="0" >
                    <tr><td>
                        <table style="margin:2px;width:415px;" border="0" cellpadding="0" cellspacing="1"  align="center" class="s90registerform">
                            <tr><td colspan="2" align="center"><b>Slot 1</b></td></tr>
                            <tr>
                            	<td>
                            	  Category
	                                <select id="category1" name="category1" onchange="change_category(1)" style="margin:12px;">
	                                	<option value='-1'>---Category---</option>
		                                 <?php  foreach ($categories as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                                 }?>
	                                </select>
	                              
                                </td>
                                <td>
									<select name="app_type1" id="app_type1" style="display:none;">
										<?php  foreach ($patient_Types as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                                 }?>
		                            </select>								 
								</td>
                            </tr>
                            <tr>
                                <td nowrap>From Time :<input type="text" style="width: 70px;" readonly name="fromtime_slot1" id="fromtime_slot1" size="10" onchange="set_to_time(1)" maxlength="10" class="time" /></td>
                                <td nowrap> To Time : <input type="text" style="width: 70px;" readonly name="totime_slot1" id="totime_slot1" size="10" onchange="set_from_time(1)" maxlength="10" class="time" /></td>
                            </tr>
                            <!--    ERROR DIV -->
                            <tr>
                               <td colspan="2" align="center" height="8">
                                  <div id="from_to_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                               </td>
                            </tr>

                            <!--  END ERROR DIV --> 

                            <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                            <tr>
                                <td>Appointment Duration:</td>
                                <td>
                                    <select name="app_duration_slot1" id="app_duration_slot1" onchange="get_duration_slot(1)">
                                        <option value="" selected="selected" disabled>-- Duration --</option> 
                                        <?php
                                            for($i=5;$i<=60;){
                                                echo "<option value='$i'> $i min </option>";
                                                $i +=5;
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            
                            <!--    ERROR DIV -->
        
                            <tr>                                 
                                 <td colspan="2" align="center" height="8" >
                                        <div id="app_duration_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                 </td>
                            </tr>

                            <!--  END ERROR DIV --> 

                            <tr>
                                <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                    <div id="duration_slot1" style="display:none;"></div>
                                    <div id="duration_slot1_display" style="display:none;"></div>
                                </td>              
                            </tr>

                           <!--    ERROR DIV -->

                            <tr>
                                 <td  colspan="2" align="center" height="8">
                                        <div id="time_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                 </td>
                            </tr>
							<tr>
                                <td>NA :</td>
                                <td>
                                    <input type="checkbox" name="na_slot1" id="na_slot1" onchange="checkRemoveSlot();" value="1"/>
                                </td>
                            </tr>
                           
                            <!--  END ERROR DIV --> 
                        </table>
                        </td><td>
                            <table style="margin:2px;width: 415px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 2</b></td></tr>
                                <tr>
	                            	<td>
	                            	  Category
	                            	
		                                <select id="category2" name="category2" onchange="change_category(2)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td>
	                            	<td>
										<select name="app_type2" id="app_type2" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot2" id="fromtime_slot2" onchange="set_to_time(2)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot2" id="totime_slot2" onchange="set_from_time(2)" size="10" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot2" id="app_duration_slot2" onchange="get_duration_slot(2)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot2" style="display:none;">

                                                </div>
                                                <div id="duration_slot2_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->

                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                                <div id="time_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot2" id="na_slot2" onchange="checkRemoveSlot();" value="2"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>
                           </td>
                        </tr>
                        <tr>
					        <td>
								<table style="margin:2px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 3</b></td></tr>
                                <tr>
	                            	<td>Category
		                                <select id="category3" name="category3" onchange="change_category(3)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td><td>
										<select name="app_type3" id="app_type3" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot3" id="fromtime_slot3" onchange="set_to_time(3)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot3" id="totime_slot3" size="10" onchange="set_from_time(3)" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot3" id="app_duration_slot3" onchange="get_duration_slot(3)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot3" style="display:none;">

                                                </div>
                                                <div id="duration_slot3_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->

                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                                <div id="time_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot3" id="na_slot3" onchange="checkRemoveSlot();" value="1"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>					        
					        </td>
					        <td>
					           <table style="margin:2px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 4</b></td></tr>
                                <tr>
	                            	<td>Category
		                                <select id="category4" name="category4" onchange="change_category(4)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td>
	                                <td>
										<select name="app_type4" id="app_type4" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot4" id="fromtime_slot4" onchange="set_to_time(4)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot4" id="totime_slot4" size="10" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot4" id="app_duration_slot4" onchange="get_duration_slot(4)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot4" style="display:none;">

                                                </div>
                                                <div id="duration_slot4_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->
                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                            <div id="time_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot4" id="na_slot4" onchange="checkRemoveSlot();" value="4"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>
					        </td>
				        </tr>
                </table>
            </td>
        </tr>
        
        <!--    ERROR DIV -->

        <tr>
             <td colspan="2"  align="center" height="8">
                    <div id="AM_PM_time_ERROR" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        
        <!--  END ERROR DIV --> 
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Create Appointment Slot" tabindex="6" type="button"  onclick="add_doc_appointment(doctor_appointment)"  /></td>        
        </tr>
    </table>
    </form>
    
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
 	
//	enable_appointment_submenu();
		enable_clinic_submenu();
		function get_num(){
	            var doctor =encodeURI(document.getElementById('doctor').value);
	            var clinic=encodeURI(document.getElementById('clinic').value); 
	            $('#doc_id_1').load('get_clinic_doc_details.php?doctor_id='+doctor+'&clinic='+clinic);
	           
	     }
	     
        function get_doctors(){
        	
        	$("#doctor").html("");

            var clinic=encodeURI(document.getElementById('clinic').value); 
            
            $("#loading").fadeIn(900,0);
            $("#loading").html("<img src='../images/ajax-loader.gif' />");
            $('#doctor').load('select_clinic_doctors.php?clinic_id='+clinic,Hide_Load());
            get_num();
        }
        
        function Hide_Load()
        {
            $("#loading").fadeOut('slow');
        };
   
        </script>
<div id="appointment-confirm" title="Appointment slots Already Created !!!" style="display: none">
    <p><?php echo "Dear Admin,<br /> <p>Do you want to modify the appointment slots?</p>" ?></p>
</div> 
</html>
<?php
ob_end_flush();
?>
