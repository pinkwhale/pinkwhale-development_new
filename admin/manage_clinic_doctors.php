<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>
<style>
    
#loading { 
width: 70%; 
position: absolute;
}
</style>
<style type="text/css">
.ui-dialog { font-size: 11px; }
body {
        font-family: Tahoma;
        font-size: 12px;
}
#question {
        width: 300px!important;
        height: 60px!important;
        padding: 10px 0 0 10px;
}
#question img {
        float: left;
}
#question span {
        float: left;
        margin: 20px 0 0 10px;
}
.ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>

</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top"  id="mainBg">
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
        }
            
    ?>
    <table border="0" cellpadding="0" cellspacing="1" width="400" align="center" class="s90registerform">
        <tr><th colspan="4">Manage Clinic-Doctor Association</th></tr>
        <tr>
            <td width="40%" align="right" bgcolor="#F5F5F5">Clinic<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic" onchange="get_doctor_clin_detail()">
                    <option value="" selected="selected" disabled>-----select Clinic-----</option>
                    <?php
                        $qry = "select clinic_id,name from clinic_details where status=0 and name<>''";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id'].":".$data['name']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
        
           
    </table>
        <div id="loading" align="center"></div> 
        <div id="doc_clinic_d"></div>   
<div style="margin-left:200px;" id="clinic-confirm" title="Delete Doctor ?">
	
</div> 
        
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
    
	enable_clinic_submenu();
	//enable_clinic_doctor_association();
    
    function get_doctor_clin_detail(){
        
        var clinic = encodeURI(document.getElementById('clinic').value);
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#doc_clinic_d').load('clinic_doc_detail.php?clinic='+clinic,Hide_Load());
        
    }
    
    function clin_doc_delete(clinic,doctor){
        var clinic = encodeURI(clinic);
        var doctor = encodeURI(doctor);
        
        
        
        
     
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#clinic-confirm').load('actions/delete_clinic_doctor_details.php?doctor='+doctor+'&clinic='+clinic);
        $('#doc_clinic_d').load('clinic_doc_detail.php?clinic='+clinic,Hide_Load());
        
        
        
        
    }
    
    
    function Hide_Load()
    {
        $("#loading").fadeOut('slow');
    };
    
</script>
 

</html>
<?php
ob_end_flush();
?>
