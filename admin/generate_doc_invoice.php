<?php
session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}

include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
require('pdf/fpdf.php');
$d=date('d_m_Y');

$f_date = $_POST['start_date'];
$doc_id=$_POST['doc_id'];
$mode=$_POST['mode'];
//echo $mode;
//echo $f_date;
//echo $doc_id;
//echo "d.........";
list($f_part1, $f_part2) = explode('/', $f_date);

$f_date=$f_part2."-".$f_part1."-"."01";
$lastday = date('t', strtotime($f_date));
$t_date=$f_part2."-".$f_part1."-".$lastday;

$result1=mysql_query("SELECT * FROM pw_doctors WHERE doc_id='$doc_id'");
while($row=mysql_fetch_array($result1))
{
	
	$name1=$row['doc_name'];
	$pan_number=$row['pan_number'];
    $ratio=$row['pwDocRatio'];

}
if($ratio!='')
{
$res = explode(":", $ratio);
$ratio1=$res[0]/100;
$ratio2=$res[1]/100;

}
else 
{
	$ratio1=80/100;
	$ratio2=20/100;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="css1/main.css" rel="stylesheet" type="text/css" />

<!-- add scripts -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="js1/highcharts.js"></script>
<script src="js1/gray.js"></script>

</head>
<body  style="background: #ffffff;">
<table style="width: 100%;">
<tr>
<td align="left" style="width: 50%;">
<table style="width: 100%;">
<tr>
<td style="font-family: arial;">pinkWhale Healthcare
Services Pvt. Ltd.</td>

</tr>
<tr>
<td style="font-family: arial;">No. 275, 16th Cross
2nd Block, R.T. Nagar, Bangalore 560032</td>
</tr>
	
<tr>
<td style="font-family: arial;">Phone: +91 - (080) 2333 1278.</td>
</tr>
<tr>
<td style="font-family: arial;">Email:
marketing@pinkwhalehealthcare.com</td>
</tr>
</table>
</td>
<td align="right" style="width: 50%;">
<table style="width: 100%;" style="margin-top: 0px;">
<tr>
<td align="right"><img src="logo.jpg" width="380" height="125" title="Logo of a company" alt="Logo of a company" />
</td>
</tr>
</table>
</td>
</tr>
</table>
<hr width="100%" />

<table style="width:100%;">
		<tr>
			<td align="center"><label style="font-family: arial; font-weight: bolder;">TopUp Summary By Month</label>
			</td>
		</tr>
						<tr height="12px;">
		</tr>
		
	
		<tr>
			<td align="center"><label style="font-family: arial;">Between</label>
			</td>
		</tr>
					<tr height="12px;">
		</tr>
	
		<tr>
			<td align="center"><label style="font-family: arial; color: #0000FF;"><?php echo $f_date?>
			</label> <label style="padding: 11px; font-family: arial;">And</label>
				<label style="padding: 11px; font-family: arial; color: #0000FF;"><?php echo $t_date?>
			</label>
			</td>
		</tr>
							<tr height="12px;">
		<tr>
			<td align="center"><label style="font-family: arial;">By Doctor</label>
			</td>
		</tr>
							<tr height="12px;">
		<tr>
			<td align="center"><label style="font-family: arial;color: #0000FF;"><?php echo $name1;?></label>
			</td>
		</tr>
							<tr height="12px;">
	</table>


	
	
	
	
<?php 

//echo $f_date;
//echo "a..........";
//echo $t_date;


$clinic_d=mysql_query("SELECT name,address FROM clinic_details a JOIN clinic_doctors_details b ON b.clinic_id=a.clinic_id WHERE b.doctor_id='$doc_id'");
if(mysql_num_rows($clinic_d)<1)
{
	$c_address=" ";
	$c_name=" ";
}	
while($clinic_row=mysql_fetch_array($clinic_d))
{
	$c_address=$clinic_row['address'];
	$c_name=$clinic_row['name'];

}

$sum=0;
$resultData = array();
if($mode=="clinic")
{
	$p_account=0;
	$d_account=0;
	$re1=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id='$doc_id' AND package_details='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	for ($i=0;$i<mysql_num_rows($re1);$i++) {
		$result1 = mysql_fetch_array($re1);
		$p_account+=$result1['top_up_amount'];

	}
	$sum=$p_account;
	$re3=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id='$doc_id' AND package_details='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	$p_account=$p_account*$ratio2;
	$d_account=$p_account*$ratio1;
	for ($i=0;$i<mysql_num_rows($re3);$i++) {
		$result = mysql_fetch_array($re3);
		$result['owed_doc']=0;
		$amount=($result['top_up_amount']*$ratio2);
		$result['owed_pw']=$amount;
		$result['recieved_at']="clinic";
		array_push($resultData,$result);
	}
}
elseif($mode=="admin")
{
	///Added one condition AND package_details!='Admin Added packages' for filtering admin packages on 14-03-2014 after discussion with Anita
	$p_account=0;
	$d_account=0;
	$re2=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc_id AND package_details='Admin Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!='' AND package_details!='Admin Added packages' ");
	for ($i=0;$i<mysql_num_rows($re2);$i++) {
		$result2 = mysql_fetch_array($re2);
		$d_account+=$result2['top_up_amount'];
	}
	$sum=$d_account;
	$re4=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc_id AND package_details='Admin Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''  AND package_details!='Admin Added packages' ");
	$d_account=$d_account*$ratio1;
	$p_account=$d_account*$ratio2;
	for ($i=0;$i<mysql_num_rows($re4);$i++) {
		$result = mysql_fetch_array($re4);
		$result['owed_pw']=0;
		$amount=($result['top_up_amount']*$ratio1);
		$result['owed_doc']=$amount;
		$result['recieved_at']="P.W";
		array_push($resultData,$result);
	}
}
elseif($mode=="user")
{
	$p_account=0;
	$d_account=0;
	$re3=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc_id AND package_details!='Admin Added packages' AND package_details!='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	for ($i=0;$i<mysql_num_rows($re3);$i++) {
		$result2 = mysql_fetch_array($re3);
		$d_account+=$result2['top_up_amount'];
	}
	$sum=$d_account;
	$re6=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id=$doc_id AND package_details!='Admin Added packages' AND package_details!='Clinic Added packages'  AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!=''");
	$d_account=$d_account*$ratio1;
	$p_account=$d_account*$ratio2;
	for ($i=0;$i<mysql_num_rows($re6);$i++) {
		$result = mysql_fetch_array($re6);
		$result['owed_pw']=0;
		$amount=($result['top_up_amount']*$ratio1);
		$result['owed_doc']=$amount;
		$result['recieved_at']="P.W";
		array_push($resultData,$result);
	}
}
elseif($mode=="all"){
	///Added one condition AND package_details!='Admin Added packages' for filtering admin packages on 14-03-2014 after discussion with Anita 	
	$re_admin=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id='$doc_id' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!='' AND package_details!='Admin Added packages'" );
	$re1=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id='$doc_id' AND package_details='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!='' AND package_details!='Admin Added packages'");
	$re2=mysql_query("SELECT * FROM  card_topup_payment_report a JOIN user_details b ON a.pw_card_id=b.user_id WHERE payment_status='Sucess' AND doc_id='$doc_id' AND package_details!='Clinic Added packages' AND date(payment_date) BETWEEN '$f_date' AND '$t_date' AND user_name!='' AND package_details!='Admin Added packages'");
	$p_account=0;
	$d_account=0;
	for ($i=0;$i<mysql_num_rows($re1);$i++) {
		$result1 = mysql_fetch_array($re1);
		$p_account+=$result1['top_up_amount'];
	
	}
	$p_account=$p_account*$ratio2;
	for ($i=0;$i<mysql_num_rows($re2);$i++) {
		$result2 = mysql_fetch_array($re2);
		$d_account+=$result2['top_up_amount'];
		
}
$sum=$d_account+$p_account;
	$d_account=$d_account*$ratio1;

	for ($i=0;$i<mysql_num_rows($re_admin);$i++) {
		$result = mysql_fetch_array($re_admin);
		if($result['package_details']=='Clinic Added packages'){
			$result['owed_doc']=0;
			$amount=($result['top_up_amount']*$ratio2);
			$result['owed_pw']=$amount;
			$result['recieved_at']="clinic";}
			else{
				$result['owed_pw']=0;
				$amount=($result['top_up_amount']*$ratio1);
				$result['owed_doc']=$amount;
				$result['recieved_at']="P.W";}
				array_push($resultData,$result);
	}
}
if (!is_dir('../images/Invoices'))
		// is_dir - tells whether the filename is a directory
	{
		//mkdir - tells that need to create a directory
		mkdir('../images/Invoices');
	}
	
//$sum=$p_account+$d_account;
?>

<table style="width:100%">
	<tr>
		<td align="center">

	<table style="width:50%" border='1' cellpadding='0' cellspacing='1' align='center' class='s90registerform' >

		<tr>
			<td width="390px" align='center'
				style="font-family: arial;"><strong>Amount Payable to CDEC by pW:</strong></td>
			<td align='center'
				style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong><?php echo $d_account."/-";?></strong>
			</td>
		</tr>

		<tr>
			<td align='center'
				style="font-family: arial;"><strong>Amount Payable to pW by CDEC:</strong></td>
			<td align='center'
				style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong><?php echo $p_account."/-";?></strong>
			</td>
		</tr>

		<tr>
			<td align='center'
				style="font-family: arial;"><strong>Total Amount That PinkWhale SHould Get:</strong></td>
			<td align='center'
				style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong><?php echo ($sum*$ratio2)."/-";?></strong>
			</td>
		</tr>
		

		<tr>
			<td align='center'
				style="font-family: arial;"><strong>Total Amount That Doctor Should Get:</strong></td>
			<td align='center'
				style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong><?php echo ($sum*$ratio1)."/-";?></strong>
			</td>
		</tr>
		
		
		
	</table>

	<tr>
		<td align="center">
<table align="center" style="width:30%;">
<?php 	$d=date('Y-m-d H-i-s');?>
	<tr>
<?php $p="-TopUp-Invoice-";?>
	<td><a href="<?php echo "../images/Invoices/".$name1."$p$d.pdf";?>"><input type="button" name="download" value="Download Report"></a></td>
				<td><input type="button" value="Send Report" name="send"></td>
				</tr>
	</table>
			</td></tr>	
	</table>


<?php 
class PDF extends FPDF
{

	function Header()
	{
		//Logo
		$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
		$this->Image('pdf/logo.jpg',160,6,40);
		$this->Ln(3);$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
		$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
		$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);

		$name="Export PDF";
		$this->SetFont('Arial','B',15);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->SetFont('Arial','B',9);
		//Line break
		$this->Line(0, 25, 300, 25);
		$this->Ln(4);
		$this->SetFont('Arial','B',12);
		$this->Cell(60);
		$this->Cell(80,10,"Topup Invoice By Month By Doctor");
		$this->Ln(8);
	}
	function doc_details($f_date,$t_date,$doc_name){
		$this->SetFont('Arial','B',10);
		$this->Cell(85);
		$this->Cell(20,10,"BETWEEN");

		$this->Ln(5);
		$this->Cell(67);
		$this->SetTextColor(0,1,225);
		$this->Cell(20,10,$f_date);
		$this->Cell(5);
		$this->SetTextColor(0,0,0);
		$this->Cell(15,10,"AND");
		//$this->Cell(30);
		$this->SetTextColor(0,1,225);
		$this->Cell(20,10,$t_date);
		$this->Ln(10);
		$this->Cell(30,10,"Doctor's Name:");
		$this->Cell(40,10,$doc_name);
		$this->SetTextColor(0,0,0);
	}
	//Page footer
	function Footer()
	{
			
	}
	function reference_table($c_name,$c_address,$f_date)
	{$this->Ln(10);
	$invoice_date=date("Y-m-d");
	$x=$this->GetX();
	$y=$this->GetY();
	$this->SetFont('Arial','B',7);
	$rand=rand(10000, 99999);
	$this->MultiCell(60, 4, "Invoice Number:\n$invoice_date",1);
	$this->SetXY($x + 60, $y);
	$this->MultiCell(60, 4, "Invoice Date:\n$f_date",1);
	$this->SetXY($x + 120, $y);

	$this->MultiCell(60, 4, "\nReference Number",1);
	$this->MultiCell(180, 3, "Buyer:$c_name\n
			Address:\n$c_address\n",1);

	$this->Ln(10);
	}
	//Simple table
	function BasicTable($header,$data,$f_date,$t_date,$doc_name,$p_account,$d_account,$c_address,$c_name,$ratio1,$ratio2)
	{
		$this->SetFillColor(242,39,201);
		$this->SetTextColor(255);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		$w=array(15,20,35,30,30,25,25,10,15,15,15,15,15);
		//Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		//Data
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		$total=0;$total_pw=0;$total_doc=0;
		$count=1;
		foreach ($data as $eachResult)
		{

			$this->Cell(15,6,$count,1);
			$this->Cell(20,6,$eachResult["top_up_amount"],1);
			$this->Cell(35,6,ucwords($eachResult["user_name"]),1);
			if($eachResult["package_details"]=="Clinic Added packages")
			{
				$this->Cell(30,6,"Clinic",1);
			}
			elseif($eachResult["package_details"]=="Admin Added packages")
			{
				$this->Cell(30,6,"Online(admin)",1);
			}
			else
			{
				$this->Cell(30,6,"Online(user)",1);
			}
				
			$trns_date=date('Y-m-d',strtotime($eachResult["payment_date"]));
			$this->Cell(30,6,$trns_date,1);
			if($eachResult["package_details"]=="Clinic Added packages")
			{
			$total_pw+=0;
			$total_doc+=$eachResult["top_up_amount"];
			$this->Cell(25,6,'0',1);
			$this->Cell(25,6,$eachResult["top_up_amount"],1);
			}
			else
			{
				$total_doc+=0;
				$total_pw+=$eachResult["top_up_amount"];
				$this->Cell(25,6,$eachResult["top_up_amount"],1);
				$this->Cell(25,6,'0',1);
			}

			$total+=$eachResult["top_up_amount"];
			$this->Ln();
			$count++;
		}$this->SetFillColor(131,177,238);
		//$this->SetFillColor(255,204,229);
		$this->Cell(15,6,"Total",1,0,'C',true);
		$this->Cell(20,6,$total,1);
		$this->Cell(35,6,"",1);
		$this->Cell(30,6,"",1);
		$this->Cell(30,6,"",1);
		$this->Cell(25,6,$total_pw,1);
		$this->Cell(25,6,$total_doc,1);$this->Ln();
		
		$this->SetFillColor(131,177,238);
		//$this->SetFillColor(255,204,229);
		$this->Cell(15,6,"Share %",1,0,'C',true);
		$this->Cell(20,6,"",1);
		$this->Cell(35,6,"",1);
		$this->Cell(30,6,"",1);
		$this->Cell(30,6,"",1);
		$persnt1=$ratio1*100;
		$persnt2=$ratio2*100;
		$this->Cell(25,6,$persnt2."%",1);
		$this->Cell(25,6,$persnt1."%",1);$this->Ln();
		
		$this->SetFillColor(131,177,238);
		//$this->SetFillColor(255,204,229);
		$this->Cell(15,6,"Should Get",1,0,'C',true);
		$this->Cell(20,6,"",1);
		$this->Cell(35,6,"",1);
		$this->Cell(30,6,"",1);$this->Cell(30,6,"",1);
		$per_pw=($total*$ratio2);
		$this->Cell(25,6,$per_pw,1);
		$per_doc=($total*$ratio1);
		$this->Cell(25,6,$per_doc,1);$this->Ln();
		//$this->SetFillColor(131,177,238);
		$this->SetFillColor(255,204,229);
		$this->Cell(15,6,"Settlement",1,0,'C',true);
		$this->Cell(20,6,"",1);
		$this->Cell(35,6,"",1);
		$this->Cell(30,6,"",1);$this->Cell(30,6,"",1);

		$t_pw=($total_pw-$per_pw);
		$t_doc=($total_doc-$per_doc);
		if($t_pw>$t_doc){//$this->SetFillColor(77,230,97);
			$this->SetFillColor(255,204,229);
			$this->Cell(25,6,($per_pw-$total_pw),1,0,'C',true);
			$this->SetFillColor(255,204,229);
			$this->Cell(25,6,($per_doc-$total_doc),1,0,'C',true);
		}else{
			//$this->SetFillColor(255,0,0);
			$this->SetFillColor(255,204,229);
			$this->Cell(25,6,($per_pw-$total_pw),1,0,'C',true);
			$this->SetFillColor(255,204,229);
			//$this->SetFillColor(77,230,97);
			$this->Cell(25,6,($per_doc-$total_doc),1,0,'C',true);
		}//$this->Cell(15,6,"",1);
		$this->Ln(10);
		$this->SetFont('Arial','BU',9);
		$this->Cell(80);
		$this->Cell(80,10,"SUMMARY");$this->Ln(10);$this->SetFont('Arial','',9);
		$this->Cell(90,6,"Amount Payable to ".$c_name."(By PW):",1);
		$this->Cell(20,6,($d_account),1);$this->Ln(6);
		$this->Cell(90,6,"Amount Payable to pW(By clinic) :",1);
		$this->Cell(20,6,($p_account),1);
		$this->Ln(6);
		$this->Cell(90,6,"Total Amount That Pinkwhale Should Get :",1);
		$this->Cell(20,6,($per_pw),1);
		$this->Ln(6);
		$this->Cell(90,6,"Total Amount That Doctor Should Get :",1);
		$this->Cell(20,6,($per_doc),1);
		$this->Ln(6);
		if($p_account>$d_account){
			$this->Cell(90,6,"Total settlement Amount to Pink Whale :",1);
			$total_settle=$p_account-$d_account;
			$this->Cell(20,6,$total_settle,1);
			$total_settle_tds=$total_settle-($total_settle*.1);
			$this->Ln(6);
			$this->Cell(90,6,"Total Amount payable after TDS :",1);
			$this->Cell(20,6,$total_settle_tds,1);$this->Ln(6);

			$this->Ln(10);}
			else{$this->Cell(90,6,"Total settlement Amount to Doctor :",1);
			$total_settle=$d_account-$p_account;
			$this->Cell(20,6,$total_settle,1);
			$total_settle_tds=$total_settle-($total_settle*.1);
			$this->Ln(6);
			$this->Cell(90,6,"Total Amount payable after TDS :",1);
			$this->Cell(20,6,$total_settle_tds,1);$this->Ln(6);
			$this->Ln(8);
						
}
	}
	function b_details($pan_number)
	{$this->Ln(2);
	$this->Cell(60, 4,"Bank: HDFC");$this->Ln(5);
	$this->Cell(80, 4,"A/c Name: pinkWhale healthcare services pvt ltd");$this->Ln(5);
	$this->Cell(70, 4,"A/c # : 01402560001147");$this->Ln(5);
	$this->Cell(70, 4,"IFSC code : HDFC0000140");$this->Ln(10);
	
	$this->Cell(70,6,"Income Tax Permanent Account Number (PAN): $pan_number");$this->Ln(6);
	$this->Cell(70,6,"Service Tax Code (STC) Number: ");$this->Ln(8);
	$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(5);
	$this->MultiCell(70, 4,"Anita Shet\n
Authorized Signatory
			");
	}


}

$pdf=new PDF();
$header=array('Sl.No:','Amount:','Patient Name','Mode','Date','With P.W','With Doctor');
//Data loading
//*** Load MySQL Data ***//




function forme($doc_name,$c_name,$c_address)

{
	$d=date('d_m_Y');
	echo "PDF generated successfully..";
	?><br/><?php
	echo "Name of the Doctor: ".$doc_name;
	?><br/><?php 
	echo "Name of the Clinic:  ".$c_name;
	?><br/>
	<?php echo "Address of clinic:  ".$c_address?><br/><font color="blue"><?php 
	echo "<a href=".$d.".pdf>DOWNLOAD pdf </a>";?></font>
<?php }


$pdf->AddPage();
$pdf->Ln(0);
$pdf->doc_details($f_date,$t_date,$name1);

$pdf->reference_table($c_name,$c_address,$f_date);
$pdf->BasicTable($header,$resultData,$f_date,$t_date,$name1,$p_account,$d_account,$c_address,$c_name,$ratio1,$ratio2);
$pdf->b_details($pan_number);
//forme($name1,$c_name,$c_address);
$pdf->Output("../images/Invoices/".$name1."$p$d.pdf","F");

?>
</body>
</html>