<?php 
error_reporting(0);
/*
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}
 * 
 */
//for pagination and database connection
include ("../includes/pw_db_connect.php");
include 'summary.report.class.php';
include("class/FusionCharts.php");

$start_year = "2011";
$current_year = strftime ( '%Y', (time()));
$current_month = strftime ( '%m', (time()));

if($_REQUEST['year']!=""){
    $year = $_REQUEST['year'];
}else{
    $year = strftime ( '%Y', (time()));
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<link rel="stylesheet" type="text/css" href="../css/designstyles.css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/add-new-doctor-validation.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/consultation.js"></script>
<script type="text/javascript" src="js/FusionCharts.js">
    
</script>
<style>	
a:hover{
color:#e70976;
text-decoration:underline;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<?php include "admin_head.php"; ?>
 
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<!--Table1_start -->    
<table width="1000"  cellspacing="0" cellpadding="0" align="center" >
    <tr>
        <td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
            <?php include "admin_left_menu.php"; ?>
        </td>
        <td width="772" valign="top" >
            <br />
            <form>
            <table align="center">
                <tr>
                    <td>
                        <b>Select Year : </b>
                    </td>
                    <td>
                        <select name="year" id="year" onchange="this.form.submit();">
                            <?php
                                $self = $_SERVER[PHP_SELF];
                                for($i=$start_year ; $i<=$current_year ; $i++){
                                    echo "<option value='$i'><b>$i</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
            </form>
            <br />
            <br />
            
            
            <?php
                
                echo "<script>
                        document.getElementById(\"year\").value=\"$year\";
                        </script>";
                
                if($year=="2011"){
                    $date = "2011-05-05";    
                    $count = 6;
                }else{
                    $date = $year."-01-05";
                    $count = 12;
                }
                $current_year = strftime ( '%b-%y', (time()));
                
                $strXML = "<chart caption='Monthly Tele Consultation Statistics' xAxisName='Months' yAxisName='# of Consulting Minutes'>";
                
                $strXML1 = "<categories>";                
                for($i=0 ; $i<=$count ; $i++){
            
                    $dynamicyear = strftime("%b-%y",(strtotime($date)+(60*60*24*30*$i)));

                    if($current_year!=$dynamicyear){
                        $strXML1 .= "<category Label='".$dynamicyear."'/>";
                    }else{
                        break;
                    }
                }                
                $strXML1.="</categories>";
            
                $obj = new SummaryReport;
                
                $qry = "select distinct type from tele_consultation_summary";
                $result = mysql_query($qry);
                $num = mysql_num_rows($result);
                if($num>0){
                    
                    
                    
                    while($data = mysql_fetch_array($result)){

                        $dbtype = $data['type'];

                        if($year=="2011"){
                            $date = "2011-05-05";    
                            $count = 6;
                        }else{
                            $date = $year."-01-05";
                            $count = 12;
                        }
                        
                        $type_name = $obj->getTypeName($dbtype);
                        
                        $strXML1 .= "<dataset seriesName='$type_name'>";

                        $total_count = 0;

                        for($i=0 ; $i<=$count ; $i++){

                            $dynamicyear = strftime("%b-%y",(strtotime($date)+(60*60*24*30*$i)));

                            if($current_year!=$dynamicyear){
                                $dd = strftime("%m-%Y",(strtotime($date)+(60*60*24*30*$i)));
                                $no = $obj->getmonthteleSummary($dbtype, $dd);
                                $total_count +=$no;
                                $total_bottom[$i] +=$no;
                                $strXML1 .= "<set  value='$no' />";
                            }else{
                                break;
                            }                
                        }
                        
                        $strXML1 .= "</dataset>";
                        
                    }
                }
                
                $strXML .= $strXML1."</chart>";
                
                echo renderChartHTML("charts/MSColumn3D.swf", "", $strXML, "myNext", 800, 500, false);
            
            ?>
            
        </td>
    </tr>   
</table>
<script type="text/javascript">
enable_counsultation_submenu();
</script>
<?php include 'admin_footer.php'; ?>

</body></html>
