<?php 
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
//echo "plan:".$_GET['plan'].",";


$plan=$_GET['plan'];
$doc_id=$_GET['doc_id'];
$f_date = date("F j Y");
//echo "date:".$f_date.",";
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
require('fpdf/fpdf.php');
$d=date('d_m_Y');
$card_result=mysql_query("SELECT * FROM pw_card_plan WHERE id_plan=$plan");
while($card_row=mysql_fetch_array($card_result))
{	$no_cards=$card_row['no_cards'];
$plan_price=$card_row['plan_price'];
$description=$card_row['description'];
}


class PDF extends FPDF
{

	function Header()
	{
		//Logo
		$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
		$this->Image('fpdf/logo.jpg',160,6,40);
		$this->Ln(3);$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
		$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
		$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);

		$name="Export PDF";
		$this->SetFont('Arial','B',15);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->SetFont('Arial','B',9);
		//Line break
		$this->Line(0, 25, 300, 25);
		$this->Ln(4);
		$this->SetFont('Arial','BU',9);
		$this->Cell(80);
		$this->Cell(80,10,"INVOICE OF CURRENT TRANSACTION");
		$this->Ln(15);
	}
	
	//Page footer
	function Footer()
	{
			
	}
	
	//Simple table
function BasicTable($header,$doc_id,$plan)
{
	/*-----------------------*/
	$card_result=mysql_query("SELECT * FROM  pw_reports WHERE id_doc=$doc_id");
	while($card_row=mysql_fetch_array($card_result))
	{	$no_cards=$card_row['sp_cards'];
	$plan_price=$card_row['sp_price'];
	$description=$card_row['sign_plan'];
	$addname=$card_row['addon'];
	$add_num_cards=$card_row['add_cards'];
	$add_price=$card_row['add_price'];
	}
	
	$doc_result=mysql_query("SELECT * FROM pw_doctors WHERE doc_id=$doc_id");
	while($doc_row=mysql_fetch_array($doc_result))
	{	$doc_name=$doc_row['doc_name'];
	$pan_number=$doc_row['pan_number'];
	$d_address=$doc_row['addressline_1'];
	$d1_address=$doc_row['address_line2'];
	$ratio=$doc_row['pwDocRatio'];
	}
	$this->Ln(4);
	$invoice_date=date("Y-m-d");
	$x=$this->GetX();
	$y=$this->GetY();
	$this->SetFont('Arial','B',8);
	$rand=rand(10000, 99999);
	$this->MultiCell(60, 4, "Invoice Number:\nINV$rand pW",1);
	$this->SetXY($x + 60, $y);
	$this->MultiCell(60, 4, "Invoice Date:\n$invoice_date",1);
	$this->SetXY($x + 120, $y);
	
	$this->MultiCell(60, 4, "Reference Number:\n49412",1);
	$this->MultiCell(180, 5,
			"Address:\n
			$d_address\n$d1_address\n",1);
	
			$this->Ln(10);
	
			
	/*-----------------------*/
    //Header
  foreach($header as $col)
    {
    	
    	$this->SetFillColor(242,39,201);
    	$this->Cell(36,10,$col,1,0,'L',true);
    	//$this->SetFont('Arial','B',9);
    }
    
        $this->Ln(10);
		$this->Cell(36,7,"1",1);
		$this->Cell(36,7,$doc_name,1);
		$this->Cell(36,7,$description,1);
		$this->Cell(36,7,$no_cards,1);
		$this->Cell(36,7,$plan_price."/-",1);
		//$this->Cell(36,7,$ratio,1);
		
		$this->Ln(7);
		$this->Cell(36,7,"Addon Pack",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,$addname,1);
		$this->Cell(36,7,$add_num_cards,1);
		$this->Cell(36,7,$add_price."/-",1);
		//$this->Cell(36,7,$ratio,1);

		$plan_price=$add_price+$plan_price;
		
		$this->Ln(7);
		$this->SetFillColor(224,224,224);
		$this->Cell(36,7,"Amount",1,0,'L',true);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,$plan_price."/-",1);
		//$this->Ln(20);$this->SetFont('Arial','',9);
		
		$serTax=$plan_price*12.36/100;
		
		$this->Ln(7);
		$this->SetFillColor(224,224,224);
		$this->Cell(36,7,"Service Tax(12.36%)",1,0,'L',true);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,$serTax."/-",1);
		//$this->Ln(20);$this->SetFont('Arial','',9);
		$total=$plan_price+$serTax;
		$string=convert_number_to_words($total);
		$this->Ln(7);
		$this->SetFillColor(224,224,224);
		$this->Cell(36,7,"Total",1,0,'L',true);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,"",1);
		$this->Cell(36,7,$total."/-",1);
		
		
		$this->Ln(7);
		//$this->SetFillColor(224,224,224);
		$this->Cell(180,20,"Total amount payable to Pinkwhale =        ".$total."/-"."          ".$string." only ",1,0,'L');
		$this->Ln(7);
		
		$numbers = explode(":", $ratio);
		//echo $numbers[0]; 
		//echo $numbers[1]; 
		
		
		$this->Cell(180,20,"Share ratio between the pinkwhale and doctor for the service = ".$ratio."",1,0,'L');
		$this->Ln(7);
		$this->Cell(180,20,"Share for the Doctor = ".$numbers[0]."%",1,0,'L');
		$this->Ln(7);
		$this->Cell(180,20,"Share for the Pinkwhale = ".$numbers[1]."%");
		//$this->Cell(36,7,$total."/-",1);
		
		$this->Ln(20);$this->SetFont('Arial','',9);
		
		
	}
	
	
	
	function b_details($doc_id)
	{$doc_result=mysql_query("SELECT * FROM pw_doctors WHERE doc_id=$doc_id");
	while($doc_row=mysql_fetch_array($doc_result))
	{	$doc_name=$doc_row['doc_name'];
	$pan_number=$doc_row['pan_number'];
	$d_address=$doc_row['addressline_1'];
	
	}
		$this->Ln(2);

		$this->MultiCell(70, 4,"Bank: HDFC");$this->Ln(3);
		$this->MultiCell(80, 4,"A/c Name: pinkWhale healthcare services pvt ltd");$this->Ln(3);
		$this->MultiCell(70, 4,"A/c # : 01402560001147");$this->Ln(3);
		$this->MultiCell(70, 4,"IFSC code : HDFC0000140");$this->Ln(8);
		
		
		
		$this->Ln(2);
		//$this->Cell(70,6,"Income Tax Permanent Account Number (PAN): $pan_number",1);$this->Ln(6);
	//$this->Cell(70,6,"Service Tax Code (STC) Number: ");$this->Ln(20);
	$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(15);
	$this->MultiCell(70, 4,"Anita Shet\n
Authorized Signatory
			");
	}


}

$pdf=new PDF();
$header=array('Sl.No:','Doctor Name','Package Type','Number Of Cards','Price');
//Data loading
//*** Load MySQL Data ***//
$sum=0;

$p_account=0;
$d_account=0;


function forme()

{
	
	$d=date('d_m_Y H-i-s');
	$p="SignUp-Invoice-";
	echo "PDF generated successfully. To download document click on the link >> <a href='../images/Invoices/".$p."".$d.".pdf'>DOWNLOAD</a>";
}
function convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = '  ';
    $separator   = ' ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}


//$pdf->SetFont('Arial','',6);
//$pdf->doc_details($f_date,$doc_name);
//*** Table 1 ***//
$pdf->AddPage();
$pdf->Ln(0);
//$pdf->reference_table($d_address);
$pdf->BasicTable($header,$doc_id,$plan);
$serTax=$plan_price*12.36/100;
$number=$plan_price+$serTax;
//convert_number_to_words($number);

$pdf->b_details($doc_id);
//$pdf->AddPage();
/*$pdf->pink_amount($p_account);
 $pdf->doc_amount($d_account,$total_settle);*/
//$pdf->settle($p_account,$d_account, $total_settle);
forme();
$p="SignUp-Invoice-";
$d=date('d_m_Y H-i-s');
$pdf->Output("../images/Invoices/".$p."$d.pdf","F");

?>