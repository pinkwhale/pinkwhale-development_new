<?php 
error_reporting(E_PARSE);
session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}

//for pagination and database connection
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
include 'summary.report.class.php';
$obj=new connect;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
    <link rel="stylesheet" type="text/css" href="../css/designstyles.css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/add-new-doctor-validation.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/consultation.js"></script>
<script>
function getMonthWiseReport(type,year,subval){
    
    	var paramString = 'type='+type+'&year='+year+'&subval='+subval;  
	//alert(paramString);
        $.ajax({  
                type: "POST",  
                url: "monthlyconsultationreport.php",  
                data: paramString,  
                success: function(response) {  

                     document.getElementById('monthwise').innerHTML =response;

                }  
        });  

    
}

</script>
<style>	
a:hover{
color:#e70976;
text-decoration:underline;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<!--Table1_start -->    
<table width="1000"  cellspacing="0" cellpadding="0" align="center" >
    <tr>
        <td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
            <?php include "admin_left_menu.php"; ?>
        </td>
        <td width="772" valign="top" >
            <br />
            <?php
                $start_year = "2011";
                $current_year = strftime ( '%Y', (time()));
                $obj = new SummaryReport;
            
                echo "<center><strong>Summary Report<strong></center>";
                echo "<table border='0' cellpadding='0' cellspacing='1' width='60%' bgcolor='#eeeeee' align='center' class='s90registerform'><tr>";
                echo "<th bgcolor='#F5F5F5' align='left' colspan='2'><b>Type of Consultation</b></th>";
                for($i=$start_year ; $i<=$current_year ; $i++){
                    echo "<th bgcolor='#F5F5F5' align='center'><b>$i</b></th>";
                }
                echo "<th bgcolor='#F5F5F5' align='center'><b>Total</b></th>";
                echo "</tr>";

                $doc_qry = "select distinct type  from online_consultation_summary";
                $doc_result = mysql_query($doc_qry);
                $doc_num = mysql_num_rows($doc_result);
                if($doc_num>0){
                    
                    while($doc_data = mysql_fetch_array($doc_result)){
                    	
                        $type = $doc_data['type'];
                        $type_name = $obj->getTypeName($type);
                        $total_right = 0;
                        if($type=="query_online"){
                            echo "<tr>";
                            echo "<td align='left' rowspan='2'>$type_name</td>";
                            echo "<td align='left'>Physician</td>";
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyquerySummary("phy", $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'><a href='#' onclick=\"getMonthWiseReport('$type','$i','phy')\" title='Click here to see Month wise Data' >$count</a></td>";
                                }
                            }
                            echo "<td bgcolor='#F5F5F5' align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                            echo "<td align='left'>Specialist</td>";
                            
                            $total_right = 0;
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyquerySummary("spe", $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'><a href='#' onclick=\"getMonthWiseReport('$type','$i','spe')\" title='Click here to see Month wise Data' >$count</a></td>";
                                }
                            }
                            echo "<td bgcolor='#F5F5F5' align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                        }else{
                            echo "<tr>";
                            echo "<td align='left' colspan='2'>$type_name</td>";
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyOnlineSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'><a href='#' onclick=\"getMonthWiseReport('$type','$i')\" title='Click here to see Month wise Data' >$count</a></td>";
                                }
                            }
                            echo "<td bgcolor='#F5F5F5' align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                        }    
                    }
                }
                
                $doc_qry = "select distinct type  from tele_consultation_summary";
                $doc_result = mysql_query($doc_qry);
                $doc_num = mysql_num_rows($doc_result);
                if($doc_num>0){
                    
                    while($doc_data = mysql_fetch_array($doc_result)){
                        
                        $type = $doc_data['type'];
                        $type_name = $obj->getTypeName($type);
                        $total_right = 0;
                        echo "<tr>";
                        echo "<td align='left' rowspan='2'>$type_name</td>";
                        
                            echo "<td align='left'>Counts</td>";
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyTeleSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'><a href='#' onclick=\"getMonthWiseReport('$type','$i','count')\" title='Click here to see Month wise Data' >$count</a></td>";
                                }
                            }
                            echo "<td bgcolor='#F5F5F5' align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                            echo "<td align='left'>Secs</td>";
                            
                            $total_right = 0;
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyTelepulseSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'><a href='#' onclick=\"getMonthWiseReport('$type','$i','pulse')\" title='Click here to see Month wise Data' >$count</a></td>";
                                }
                            }
                            echo "<td bgcolor='#F5F5F5' align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                        
                    }
                    
                }
                
                
                echo "</table>";
            ?>
		<center><a href="yearly-summary-excel.php">Export Report <img src="../images/excelLogo.gif" /></a></center>            
            
            <br /><br />
            <div id="monthwise"></div>
            
            
        </td>
    </tr>   
</table>
<script type="text/javascript">
enable_counsultation_submenu();
</script>
<?php include 'admin_footer.php'; ?>

</body></html>
