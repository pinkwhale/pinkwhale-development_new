<?php
ob_start ();
error_reporting ( E_PARSE );
session_start ();
include ("../includes/pw_db_connect.php");
if (! isset ( $_SESSION ['username'] ) || $_SESSION ['login'] != 'admin') {
	header ( "Location: ../index.php" );
	exit ();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/add_content_manager_validation.js"></script>
<link href="../css/designstyles.css" media="screen, projection"	rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
</head>
<script type="text/javascript">                     
                function chechclinicEmailid()
                {                        
                        if(document.getElementById("regemail").value!=""){
                            var paramString = 'email=' + document.getElementById("regemail").value + '&docId=""';  

                                $.ajax({  
                                        type: "POST",  
                                        url: "check_email.php",  
                                        data: paramString,  
                                        success: function(response) {  
                                                //alert(response);	
                                                if(response== "true")
                                                {
                                                        document.getElementById('emailErrDiv').innerHTML ="";
                                                }
                                                else
                                                {
                                                        document.getElementById('emailErrDiv').innerHTML ="Email ID address already exists";
                                                }	
                                        }
                                });
                        }
                }
</script>
<body>	
	<?php include "admin_head.php"; ?>
	<!-- side Menu -->
	<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="228" valign="top"
				style="border-right: 1px solid #4d4d4d; border-left: 1px solid #4d4d4d;">

				<?php include "admin_left_menu.php"; ?>
			</td>
			<td width="772" id="mainBg" valign="top">
			<?php
			if ($_SESSION ['msg'] != "") {
				echo "<center><font color='green' size='3'>" . $_SESSION ['msg'] . "</font></center>";
				$_SESSION ['msg'] = "";
			} else {
				if ($_SESSION ['error'] != "") {
					echo "<center><font color='red' size='3'>" . $_SESSION ['error'] . "</font></center><br />";
					$_SESSION ['error'] = "";
				}
				?>
				
				<form action="actions/add_content_manager_action.php" method="post"
					name="add_content" id="add_content" enctype="multipart/form-data">
					<table border="0" cellpadding="0" cellspacing="1" width="500"
						align="center" class="s90registerform">
						<tr>
							<td width="40%" align="right">Your Name<font color="#FF0000">*</font>:
							</td>
							<td width="60%" align="left"><input size="20" type="text"
								maxlength="40" name="regname" id="regname" value=""
								class="s90regformtext"></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="nameErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td width="40%" align="right">Age<font color="#FF0000">*</font>:
							</td>
							<td width="60%" align="left"><input size="20" type="text"
								maxlength="2" onkeypress="return isNumberKey(event);"
								name="regage" id="regage" value="" class="s90regformtext"></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="ageErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td width="40%" align="right">Gender<font color="#FF0000">*</font>:
							</td>
							<td width="60%" align="left"><select name="reggender"
								id="reggender">
									<option value="">Select Gender</option>
									<option value="M">Male</option>
									<option value="F">Female</option>
							</select></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="genderErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td align="right">Your Email<font color="#FF0000">*</font>:</td>
							<td><input class="s90regformtext" type="text"
								maxlength="45" size="20" id="regemail" name="regemail" onchange="chechclinicEmailid();" value="">
							</td>

						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="emailErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td align="right">Password<font color="#FF0000">*</font>:</td>
							<td><input type="password" class="s90regformtext" 
								id="regPassword" onkeyup="validate_pswd(form)" name="regPassword" value=""></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="passwordErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td align="right">Confirm Password<font color="#FF0000">*</font>:
							</td>
							<td><input type="password" class="s90regformtext" 
								name="regConfirmPassword" id="regConfirmPassword"
								value="" onkeyup="validate_pswd(form)"></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="repasswordErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td align="right" style="line-height: 20px;">Mobile #<font
								color="#FF0000">*</font>:
							</td>
							<td style="line-height: 30px;"><input type="text"
								class="s90regformtext" maxlength="10" name="regPhone1"
								id="regPhone1" value="" onkeypress="return isNumberKey(event);"></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="mobileErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						
						<tr>						
						<td  width="40%" align="right" >
								<input  type="radio" name="is_approver" id="is_approver" value="1" >Approver </td>
							<td  width="40%" align="center" >	<input type="radio" name="is_approver" id="content_writer" value="0">Content Writer </td>
						</tr>	

						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="approverErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						
						<tr>
							<td width="40%" align="center" ></td>
							<td align="left" ><div>FAQ</div>
							</td>

							<td align="left" ><input type="checkbox"
								id="faq" name="faq"	value="1" /></td>
						</tr>
						
						<tr>
							<td width="40%" align="center" ></td>
							<td align="left" ><div>Tip</div>
							</td>

							<td align="left" ><input type="checkbox"
								id="tip" name="tip"	value="1" /></td>
						</tr>

						<tr>
							<td width="40%" align="center" ></td>
							<td align="left" ><div>SMS Tip</div>
							</td>

						<td align="left" ><input type="checkbox" id="sms_tip" name="sms_tip" value="1"/></td>
						</tr>
						<tr>
							<td width="40%" align="center"></td>
							<td align="left" ><div>Email</div>
							</td>

							<td align="left" ><input type="checkbox"
								id="email" name="email" value="1" /></td>
						</tr>
						
						<tr>
							<td width="10%" align="center" ></td>
							<td align="left" ><div>All</div>
							</td>

							<td align="left" ><input type="checkbox"
								id="all" name="all" onchange="enable_all(all)" value="1" /></td>
						</tr>
						
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="optionsErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						
						<tr>
							<td>
								<div class="reg_popupbtn">
									<input class="btn btn-primary" type="submit" name="register" 
										id="register" value="Register" onclick="return register_content(add_content)"/>
							</div>							
							</td>
						</tr>
					</table>
				</form> 
				<?php
			}
			?>
				</td>
		</tr>
	</table>
	<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
function isNumberKey(event)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (event.which) ? event.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
</script>
</html>
<?php
ob_end_flush ();
?>					