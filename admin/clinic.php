<?php
ob_start();
  	include("Sajax.php");
	sajax_init();
	sajax_export('block_fun');
	sajax_handle_client_request();
?>
<?php 

	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>

<script type="text/javascript">
    function block_clinic(val){
       
        var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
          {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                
                //document.getElementById("serialclick").innerHTML=xmlhttp.responseText;
                location.reload(true);
            }
          }


        xmlhttp.open("GET","change_clinic_status.php?id="+val,true);
        xmlhttp.send();
            
      
    }
</script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; 
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;


?>
</td>
<td width="772" valign="top"  id="mainBg" >
     <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else {
            if($_SESSION['error']!=""){
                echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";
            }
     ?>
    <div id="clin">
     <table width='750' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
           <tr><th colspan="6">Manage Clinics</th></tr>
           
           <?php
                $count = 0;
                $qry="select logo,clinic_id,name,address,country,state,city,pin_zip,num_of_doc,entered_date,status,email,phone_no from clinic_details where status in (0,1) and name<>''";
                $obj->query($qry);
                $pager = new PS_Pagination($dbcnx, $qry, 10, 1, "param1=valu1&param2=value2");
                $pager->setDebug(true);
                $rs = $pager->paginate();
                if(!$rs) die(mysql_error());?>
                <tr>
               <td width="10%" align="left" bgcolor="#F5F5F5" ><b>Clinic Id</b></td>
               <td width="20%" align="left" bgcolor="#F5F5F5" ><b>Clinic Logo</b></td>
               <td width="25%" align="left" bgcolor="#F5F5F5"><b>Clinic Name</b></td>
               <td width="40%" align="left" bgcolor="#F5F5F5"><b>Address</b></td>
               <td width="40%" align="left" bgcolor="#F5F5F5" nowrap><b>No.of Doctors</b></td>
               <td width="40%" align="left" bgcolor="#F5F5F5"><b>Active/Inactive</b></td>
           </tr>
                <?php 
                while($data = mysql_fetch_array($rs)){
                    $count +=1;
		    $mob = $data['phone_no'];
		    $email = $data['email'];
                    if($data['status']==0){                        
                        echo "<tr><td width='10%' align='left' bgcolor='#ffffff' >".$data['clinic_id']."</td>";
                        echo "<td width='20%' align='center' bgcolor='#ffffff' ><img height=30 width=30 src='../".$data['logo']."'/></td>";
                        echo "<td width='25%' align='left' bgcolor='#ffffff' style='text-decoration:none;' style='color:#FF0000;'><a href='edit_clinic.php?id=".$data['clinic_id']."'>".$data['name']."</a></td>";
                        echo "<td width='40%' align='left' bgcolor='#ffffff' >".$data['address']."<br />".$data['city']." - ".$data['pin_zip']."<br />".$data['state']." , ".$data['country']."<br /><strong>Email</strong> : $email<br /><strong>Mobile</strong> : $mob</td>";
                        echo "<td width='40%' align='center' bgcolor='#ffffff' >".$data['num_of_doc']."</td>";                    
                        echo "<td width='40%' align='center' bgcolor='#ffffff' ><a href='javascript:block_clinic(".$data['clinic_id'].")'><img src='../images/block.gif' /></a></td></tr>";
                    }else if($data['status']==1){
                        echo "<tr><td width='10%' align='left' bgcolor='#ffffff' style='color:#FF0000;'>".$data['clinic_id']."</td>";
                        echo "<td width='20%' align='center' bgcolor='#ffffff' ><img height=30 width=30 src='../".$data['logo']."'/></td>";
                        echo "<td width='25%' align='left' bgcolor='#ffffff' style='text-decoration:none;'><a href='edit_clinic.php?id=".$data['clinic_id']."'>".$data['name']."</a></td>";
                        echo "<td width='40%' align='left' bgcolor='#ffffff' style='color:#FF0000;'>".$data['address']."<br />".$data['city']." - ".$data['pin_zip']."<br />".$data['state']." , ".$data['country']."<br /><strong>Email</strong> : $email<br /><strong>Mobile</strong> : $mob</td>";
                        echo "<td width='40%' align='center' bgcolor='#ffffff' style='color:#FF0000;'>".$data['num_of_doc']."</td>";                                            
                        echo "<td width='40%' align='center' bgcolor='#ffffff' ><a href='javascript:block_clinic(".$data['clinic_id'].")'><img src='../images/block1.gif' /></a></td></tr>";
                    }
                }
                $ps1 = $pager->renderFirst();
				$ps2 = $pager->renderPrev();
				$ps3 = $pager->renderNav('<span>','</span>');
				$ps4 = $pager->renderNext();
				$ps5 = $pager->renderLast();
           ?>
            	<tr><td colspan='6' align='center'>
				<?php
                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr>"; 
                ?>
        
    </table>
    </div>
        <?php
        }
        ?>
</td></tr></table>
<?php 
if($count==0){
    echo "<script>document.getElementById('clin').innerHTML = '' </script>";
}
include 'admin_footer.php'; 
?>
</body>

<script type="text/javascript">
    enable_clinic_submenu();
</script>
</html>
<?php
ob_end_flush();
?>
