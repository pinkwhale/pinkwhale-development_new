<?php
ob_start();
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
        header("Location: ../index.php");
        exit();
}

include ("../db_connect.php");

$clinic=urldecode($_GET['clinic']);

$clinic_details = explode("|", $clinic);
$clinic_id = $clinic_details[0];
$clinic_name = $clinic_details[1];

$display = "<option value='' selected='selected' disabled>Select Doctor</option>";


$qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id";                        
$res = mysql_query($qry);
$num = mysql_num_rows($res);
if($num>0){
    while ($data = mysql_fetch_array($res)){
       $display .=  "<option value='".$data['doc_id']."|".$data['doc_name']."'>".$data['doc_name']."</option>";   
    }
}else{
    $display = "<option value='' selected='selected' disabled>No Doctors</option>";
}
echo $display;
ob_end_flush();
?>

