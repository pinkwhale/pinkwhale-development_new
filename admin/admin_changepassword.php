<?php 
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
     if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="../text/javascript" src="js/registration_validation.js"></script>
<script type="text/javascript" src="../js/changePassword.js"></script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<?php include "../includes/pw_db_connect.php"; ?>
<?php include "admin_head.php"; ?>

<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
<td width="850" valign="top">
<table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform1">
        <tr><th><?php 
		if($_SESSION['chg_pass']!=""){
			echo $_SESSION['chg_pass'];
			$_SESSION['chg_pass'] = "";
		}

	 ?></th></tr>
</table>
<br />
<form action="actions/change_password.php" method="post" name="changePassword" id="changePassword">
  <table width="521" border="0" cellspacing="0" cellpadding="0" align="center" >
  	<tr><td width="717"><table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform">
        <tr><th colspan="2" align="left">Change Password</th></tr>
        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
  		<tr></tr>
        	<td width="40%" align="right" bgcolor="#F5F5F5">User Name<font color="#FF0000">*</font>:</td>
  			<td width="60%" align="left" bgcolor="#F5F5F5">
  				<input size="30" type="text" disabled="disabled" maxlength="40" name="username" value="<?php echo $_SESSION['username']; ?>" class="input"">
        	</td>
        </tr>
        
        
<tr>
    <td></td>
    <td height="10">
    <div id="username_div" style="color: #F33;font-family:verdana;font-size:10px;"></div>
    </td>
</tr> 

  		<tr>
            <td align="right" bgcolor="#F5F5F5">New Password<font color="#FF0000">*</font>:</td>
            <td nowrap="nowrap" bgcolor="#F5F5F5">
            	<input class="input"" type="password" maxlength="45" size="30" name="new_pswd" id="new_pswd" onkeyup="validate_pswd(form)" autocomplete="off">
            </td>
        </tr>
        
        
 <tr>
     <td></td>
     <td height="10" >
     <div id="pswErrDiv"  style="color: #F33;font-family:verdana;font-size:10px; "></div>
     </td>
 </tr> 

  		<tr>
        	<td align="right" bgcolor="#F5F5F5">Confirm Password<font color="#FF0000">*</font>:</td>
  			<td bgcolor="#F5F5F5">
  			<input type="password" class="input"" maxlength="12" size="30"  name="cnfm_pswd" id="cnfm_pswd" onkeyup="validate_pswd(form)"><span style="color:green" id='result'></span>
         	</td>
       </tr>



 <tr>
 	<td></td>
 	<td height="10"><div id="repswErrDiv" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
 </tr>
  
 
		
        <tr>
        <tr>
        	<td bgcolor="#F5F5F5">&nbsp;</td>
        	<td bgcolor="#F5F5F5">
  				&nbsp;&nbsp;&nbsp;&nbsp;<input onmouseover="this.style.cursor='pointer'"  value="Save" tabindex="6" type="button"
              onclick="return psw_val(changePassword)"  
                >
        	</td>
        </tr>
      </table>
</td></tr></table></table>
</form>
  <?php
include 'admin_footer.php'; ?>
</p>
</body></html>
