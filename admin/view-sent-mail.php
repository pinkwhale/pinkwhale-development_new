<?php 
	error_reporting(E_PARSE);
	session_start();
	include ("../db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
	

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>pinkwhalehealthcare</title>
    <meta name="description" content="pinkwhalehealthcare">
    <link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
    <link href="css/my_css.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/add-group-validation.js"></script>
    <script type="text/javascript" src="js/delete-group.js"></script>

<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>

</head>
<body onLoad="init_table();">
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top">
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
$sent_mail_id=$_GET['sent-mail-id'];
?><table align="center">
<?php 
		$qry="SELECT * FROM send_mail where send_mail_id ='$sent_mail_id' ";
		$qry_rslt=mysql_query($qry);
		while($qry_result=mysql_fetch_array($qry_rslt))
		{ 
			$mail_to=$qry_result['send_mail_to'];
			$mail_subject=$qry_result['send_mail_subject'];
			$mail_content=$qry_result['send_mail_content'];
			$mail_date=$qry_result['send_mail_date'];
		}
				$old_date_timestamp = strtotime($mail_date);
				$sent_date = date('j-m-y H:i', $old_date_timestamp);
	if($mail_to=="all") $to="All Users";
	else if($mail_to=="usersService") $to="Users by service";
	else if($mail_to=="usersGroup") $to="Users by group";
	else if($mail_to=="notactivated") $to="Users not activated";
	else if($mail_to=="selectedusers") $to="Selected Users";			
?>
<tr><td>
<table width='600' border='0' cellpadding='0' cellspacing='15'>
<tr>
<td width="72" align="left"><b style="font-size: 14px;">To :</b></td>
<td width="483" align="left"><b><?php echo $to; ?></b></td>
</tr>
<tr>
<td align="left"><b style="font-size: 14px;">Subject :</b></td>
<td align="left"><b><?php echo $mail_subject; ?></b></td>
</tr>
<tr>
<td align="right" colspan="2"><b><?php echo $sent_date; ?></b></td>
</tr>
<tr>
<td align="left" colspan="2" style="border: 1px solid #999;"><span style="margin: 5px 5px 5px 5px;"><?php echo $mail_content; ?></span></td>
</tr>
</table>
</td></tr>
</table>
</td></tr>
</table>

 <?php include 'admin_footer.php'; ?>
<script type="text/javascript">
 function enable_savemail_submenu()
	{
		document.getElementById("submenu14").style.display="block";
		document.getElementById("submenu20").style.display="block";

	}

enable_savemail_submenu();
</script>
</body></html>