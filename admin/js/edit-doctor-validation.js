var typeValidated = false;
var cons_typeValidated = false;
var nameValidated = false;
var mobileValidated = false;
var num_of_clinics = false;
var emailValidated = false;
function edit_doctor_validate(form)
{
	typeValidated = true;
	cons_typeValidated= true;
	nameValidated= true;
	mobileValidated = true;
        num_of_doc_Validated = true;
        emailValidated = true;
        
	document.getElementById("typeErrDiv").innerHTML	= "";
	document.getElementById("cityError").innerHTML	= "";
	document.getElementById("cons_typeErrDiv").innerHTML= "";
	document.getElementById("nameErrDiv").innerHTML = "";
	document.getElementById("mobileErrDiv").innerHTML = "";
        document.getElementById("num_of_clinicErrDiv").innerHTML = "";
        document.getElementById("emailError").innerHTML	= "";
        
        if(form.doctor_app.checked==true && form.num_clinic.value==0){
            document.getElementById("num_of_clinicErrDiv").innerHTML = "Please enter Number of clinics.";
            num_of_doc_Validated  = false;
        }
        
	if (form.doctor_spe.checked==false  && form.doctor_exp.checked==false && form.doctor_app.checked==false && form.doctor_qry.checked==false)
	{
		document.getElementById("typeErrDiv").innerHTML = "Please select Doctor type";
				typeValidated  = false;
	}
	
	//if (form.doctor_phy.checked==false && form.doctor_spe.checked==false && form.doctor_con.checked==false && form.doctor_exp.checked==false && form.doctor_app.checked==false && form.doctor_qry.checked==false )
        if (form.doctor_spe.checked==false && form.doctor_exp.checked==false && form.doctor_app.checked==false && form.doctor_qry.checked==false )
	{
		document.getElementById("cons_typeErrDiv").innerHTML = "Please select Consultation type";
		cons_typeValidated = false;
	}
	
	if (form.doc_name.value=='')
	{
		alert("Please Enter the name");
		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
   		nameValidated = false;
	}
	if (form.city.value=='')
	{
        alert("Please Select City");        
   		document.getElementById("cityError").innerHTML = "City cannot be blank";
   		nameValidated = false;
	}
	if (form.doc_mobile.value=='')
	{
		alert("Please Enter Mobile number");
		document.getElementById("mobileErrDiv").innerHTML = "Mobile Number cannot be blank";		
		mobileValidated = false;
	}
	if(form.doc_email_id.value=='')
		{
		alert("Please Enter Email id");
		document.getElementById("emailError").innerHTML = "Email id cannot be blank";		
		emailValidated = false;
		}
	
	
	if(typeValidated && cons_typeValidated && nameValidated && mobileValidated && num_of_doc_Validated && emailValidated)
	{
		
		var paramString = 'docId='+ form.doctor_id.value + '&email=' + form.doc_email_id.value;   
			
		$.ajax({  
			type: "POST",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) {  

				if(response == true)
				{
					document.getElementById('edit_doc_form').submit();

				}
				else {
					document.getElementById('emailError').innerHTML ="Email address already exists";
				}
alert("Successfully saved");
			}  
		});  
		
	}
	else
	{
		return false;
	}

	
}







function pageback()
{
	window.location.href="manage-doctor.php";
}
function disable_all()
{
	   	//document.getElementById("doctor_phy").disabled="true";
		document.getElementById("doctor_spe").disabled="true";
		//document.getElementById("doctor_con").disabled="true";
		document.getElementById("doctor_exp").disabled="true";
}
function disable_two()
{
	//document.getElementById("doctor_phy").disabled="true";
	//document.getElementById("doctor_con").disabled="true";
}
function edit_enable(type)
{
	if (type.value=='Expert' && type.checked == true)
	{
		document.getElementById("expert_rate").style.display="block";
		document.getElementById("spe_contact_type").style.display="block";
	}
	else if (type.value=='Specialist' && type.checked == false)
	{
		document.getElementById("spe_contact_type").style.display="none";
		document.getElementById("specilist_rate1").style.display="none";
		document.getElementById("specilist_rate2").style.display="none";
		document.getElementById("specilist_rate6").style.display="none";
		document.getElementById("spe_tele_type1").style.display="none";
		document.getElementById("doctor_tele_con").checked = false;
		document.getElementById("doctor_online_con").checked = false;
		document.getElementById("doctor_diag_con").checked = false;
	}
	else if (type.value=='Expert' && type.checked == false)
	{
		document.getElementById("expert_rate").style.display="none";
		document.getElementById("spe_contact_type").style.display="none";
	}
	else if (type.value=='Specialist' && type.checked == true)
	{
		document.getElementById("spe_contact_type").style.display="block";
	
	}else if (type.value=='Appointment' && type.checked == true)
	{
   		document.getElementById("Appoint_type").style.display="block";
	}else if (type.value=='Appointment' && type.checked == false)
	{
   		document.getElementById("Appoint_type").style.display="none";
	}
        
        var doc_query =  document.getElementById("doctor_qry").checked;
        var doc_spe = document.getElementById("doctor_spe").checked;
        var dpc_exp = document.getElementById("doctor_exp").checked;
        
        if (doc_spe==true && doc_query==true)
	{   		
                document.getElementById("query_rate_2").style.display="block";
                document.getElementById("query_rate_1").style.display="none";
	}else if (dpc_exp==true && doc_query==true)
	{   		
                document.getElementById("query_rate_2").style.display="block";
                document.getElementById("query_rate_1").style.display="none";
	}else if (doc_query==true)
	{   		
                document.getElementById("query_rate_1").style.display="block";
                document.getElementById("query_rate_2").style.display="none";
	}
        
        if (type.value=='Query' && type.checked == false)
	{
   		document.getElementById("query_rate_1").style.display="none";
                document.getElementById("query_rate_2").style.display="none";
	}
}
function enable_tele(valu)
{
	if (valu.checked == true)
	{
		document.getElementById("spe_tele_type1").style.display="block";
		document.getElementById("specilist_rate1").style.display="block";
		
	}
	else if (valu.checked == false)
	{
		document.getElementById("specilist_rate1").style.display="none";
		document.getElementById("spe_tele_type1").style.display="none";
	}
}
	function enable_online(valu)
{
	if (valu.checked == true)
	{
		document.getElementById("specilist_rate2").style.display="block";
	}
	else if (valu.checked == false)
	{
		document.getElementById("specilist_rate2").style.display="none";
	}

}
	
	function enable_diag(valu)
	{
		if (valu.checked == true)
		{
			//document.getElementById("specilist_head").style.display="block";
			document.getElementById("specilist_rate6").style.display="block";
		}
		else if (valu.checked == false)
		{
			document.getElementById("specilist_rate6").style.display="none";
		}

	}

	
	function enable_none(valu)
	{
		if (valu.checked == true)
			{
			
				document.getElementById("specilist_rate2").style.display="none";
				document.getElementById("specilist_rate1").style.display="none";
				document.getElementById("specilist_rate6").style.display="none";
				document.getElementById("doctor_online_con").disabled="true";
				document.getElementById("doctor_tele_con").disabled="true";
				document.getElementById("doctor_diag_con").disabled="true";
				document.getElementById("spe_cont_type").style.display="none";
			}
		else {
			document.getElementById("doctor_online_con").disabled=false;
				document.getElementById("doctor_tele_con").disabled=false;
				document.getElementById("doctor_diag_con").disabled=false;
		}
	
	}
	
function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57 ))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
