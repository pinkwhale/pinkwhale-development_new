var clinic_name = false;
var address = false;
var country = false;
var state = false;
var city = false;
var pin_zip = false;
var num_of_doc = false;
var logo = false;
var email = false;
var phone = false;
var typeValidated  = false;

function add_clinic_submit(form){
    clinic_name = true;
    address = true;
    country = true;
    state = true;
    city = true;
    pin_zip = true;
    num_of_doc = true;
    logo = true;
    email = true;
    phone = true;
    typeValidated  = true;
    
    document.getElementById("clinicnameErrDiv").innerHTML= "";
    document.getElementById("addressErrDiv").innerHTML = "";
    document.getElementById("countryRrrDiv").innerHTML = "";
    document.getElementById("stateErrDiv").innerHTML= "";
    document.getElementById("cityErrDiv").innerHTML= "";
    document.getElementById("pinErrDiv").innerHTML= "";
    document.getElementById("num_of_doc_ErrDiv").innerHTML= "";
    document.getElementById("logo_ErrDiv").innerHTML= "";
    document.getElementById("phoneErrDiv").innerHTML= "";
    document.getElementById("emailErrDiv").innerHTML= "";
    document.getElementById("clinic_type_ErrDiv").innerHTML= "";
    
    var top_up =  document.getElementById("top_up").checked;
    var appointment_booking = document.getElementById("appointment_booking").checked;
    var diagnostic_consultation = document.getElementById("diagnostic_consultation").checked;
    var all = document.getElementById("all").checked;
    
    if (form.cname.value=='')
    {
        document.getElementById("clinicnameErrDiv").innerHTML = "Clinic Name cannot be blank";
        clinic_name = false;
    }
    
    if (form.address.value=='')
    {
        document.getElementById("addressErrDiv").innerHTML = "Address Name cannot be blank";
        address = false;
    }
    
    if (form.country.value=='')
    {
        document.getElementById("countryRrrDiv").innerHTML = "Country Name cannot be blank";
        country = false;
    }
    
    if (form.state.value=='')
    {
        document.getElementById("stateErrDiv").innerHTML = "State Name cannot be blank";
        state = false;
    }
     
    if (form.city.value=='')
    {
        document.getElementById("cityErrDiv").innerHTML = "City Name cannot be blank";
        city = false;
    }
     
    if (form.pin.value=='')
    {
        document.getElementById("pinErrDiv").innerHTML = "Pin/Zip Name cannot be blank";
        pin_zip = false;
    }
    
    if (form.phone.value=='')
    {
        document.getElementById("phoneErrDiv").innerHTML = "Phone number cannot be blank";
        phone = false;
    }

    if(top_up == false && appointment_booking == false && diagnostic_consultation == false && all == false){
    	document.getElementById("clinic_type_ErrDiv").innerHTML = "Clinic type cannot be blank";
    	typeValidated  = false;
    }
    
//    if(top_up == true && appointment_booking== true){  
//    	document.getElementById("clinic_type").value = 4;
//    }
//    
//    if(appointment_booking == true && diagnostic_consultation== true){  
//    	document.getElementById("clinic_type").value = 5;
//    }
//    
//    if(diagnostic_consultation == true && top_up== true){  
//    	document.getElementById("clinic_type").value = 6;
//    }
//    
//    if(diagnostic_consultation == true){
//    	document.getElementById("clinic_type").value = 3;
//    }
//    
//    if(appointment_booking == true){
//    	document.getElementById("clinic_type").value = 2;
//    }
//    
//    if(top_up == true){
//    	document.getElementById("clinic_type").value = 1;
//    }
//    
//    if(all == true){
//    	document.getElementById("clinic_type").value = 0;
//    }
//    
    if (form.email.value=='')
    {
        document.getElementById("emailErrDiv").innerHTML = "Email-Id cannot be blank";
        email = false;
    }else if(form.email.value!=''){
        
        var paramString = 'email=' + form.email.value + '&docId=""';
        
                $.ajax({  
			type: "clinic_type",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) {  
				if(response == true)
				{
					document.getElementById('emailErrDiv').innerHTML ="";				
				}
				else
				{
					document.getElementById('emailErrDiv').innerHTML ="Email address already exists";
                                        email = false;
				}	
			}
			  
		});  
    }

     
    if (form.num_doc.value==0)
    {
        document.getElementById("num_of_doc_ErrDiv").innerHTML = "Provide Number of doctors";
        num_of_doc = false;
    }
    
        
    if(clinic_name && address && country && state && city && pin_zip && num_of_doc && phone && email){
        
        document.getElementById('add_clinic').submit();
        
    }else{
        
        return false;
        
    }
}

function enable_all(valu)
{
	if (valu.checked == true)
		{
			document.getElementById("top_up").checked=true;
			document.getElementById("appointment_booking").checked=true;
			document.getElementById("diagnostic_consultation").checked=true;
		}else{
			document.getElementById("top_up").checked=false;
			document.getElementById("appointment_booking").checked=false;
			document.getElementById("diagnostic_consultation").checked=false;
		}
}

function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}

function checkphoneno(strSpace){
    var iChars = "0123456789";
    for(var i=0;i<strSpace.value.length;i++){
        if(iChars.indexOf(strSpace.value.charAt(i))==-1){
           //alert("These Characters are not allowed ,Try again");
           //strSpace.focus();
           //strSpace.value="";
           return false;
        }
    }
}

var doctor = false;
var clinic = false;
var num_of_clinic =false;
function add_clinic_doctor(form){
    
    doctor = true;
    clinic = true;
    num_of_clinic = true;
    
    document.getElementById("doctorErrDiv").innerHTML= "";
    document.getElementById("num_clinicErrDiv").innerHTML = "";
    document.getElementById("clinicErrDiv").innerHTML = "";
    
    if(form.doctor.value==""){
        document.getElementById("doctorErrDiv").innerHTML= "Please select Doctor";
        doctor = false;
    }
    
    var num = form.num_clinic.value;
    var remaining_num =form.remaining_num_clinic.value;
    if(remaining_num==0 || remaining_num==""){
        
        document.getElementById("num_clinicErrDiv").innerHTML= num+" clinics are already added";
        doctor = false;
    }
   
     
    var totalChecked = 0;
    for (i = 0; i < form.clinic.length; i++) {
      if (form.clinic[i].selected && form.clinic[i].value!="") {          
          totalChecked++;
      }
    }
    
    if(totalChecked==0 && remaining_num!=0){
        document.getElementById("clinicErrDiv").innerHTML= "Please select clinics";
        clinic = false;
    }else if(totalChecked > remaining_num){
        document.getElementById("clinicErrDiv").innerHTML= "selected clinic are more than alloted No. of Clinics";
        clinic = false;
    }
    
    if(doctor && clinic &&  num_of_clinic){
        document.getElementById('AddClinicDoctor').submit();
    }else{
        return false;
    }
}

var clinic = false;
var uname = false;
var password = false ;
var re_password = false;

function add_clinic_login(form){
    clinic = true;
    uname = true;
    password = true;
    repassword = true;
    
    document.getElementById("clinicErrDiv").innerHTML= "";
    document.getElementById("unameErrDiv").innerHTML = "";
    document.getElementById("passwordErrDiv").innerHTML = "";
    document.getElementById("re_passwordErrDiv").innerHTML= "";
    
    if(form.clinic.value==''){
        document.getElementById("clinicErrDiv").innerHTML= "Please select Clinic";
        clinic = false;
    }
    
    if(form.uname.value==''){
        document.getElementById("unameErrDiv").innerHTML= "User Name cannot be blank";
        uname = false;
    }else if(form.uname.value.length<5){
        document.getElementById("unameErrDiv").innerHTML= "User Name should contain minimun 5 characters";
        uname = false;
    }
    
    if(form.pwd.value==''){
        document.getElementById("passwordErrDiv").innerHTML= "Password cannot be blank";
        password = false;
    }else if(form.pwd.value.length<6 || form.pwd.value.length>16){
        document.getElementById("passwordErrDiv").innerHTML= "Password should contain minimum 5 charaters and Maximum 15 characters";
        password = false;
    }else if(form.pwd.value!=form.rpwd.value){
        document.getElementById("re_passwordErrDiv").innerHTML= "Password and Retype password are not same";
        repassword = false;
    }
    if(clinic && uname && password && repassword){
        document.getElementById('clinic_login').submit();
    }else{
        return false;
    }
}

function enable_clinic_submenu()
{
	document.getElementById("submenu23").style.display="block";   
        document.getElementById("submenu24").style.display="block";  
        document.getElementById("submenu32").style.display="block";
        document.getElementById("submenu27").style.display="block";  
        document.getElementById("submenu30").style.display="block";
        document.getElementById("submenu33").style.display="block";
	document.getElementById("submenu55").style.display="block";
}

function enable_clinic_doctor_association(){
        document.getElementById("submenu27").style.display="block";  
        document.getElementById("submenu30").style.display="block";
}



var doctor = false;
var clinic = false;
var num_of_doc =false;
function add_doctors_to_clinic(form){
    doctor = true;
    clinic = true;
    num_of_clinic = true;
    
    document.getElementById("clinicErrDiv").innerHTML= "";
    document.getElementById("num_docErrDiv").innerHTML = "";
    document.getElementById("doctorErrDiv").innerHTML = "";
    
    if(form.clinic.value==""){
        document.getElementById("clinicErrDiv").innerHTML= "Please select Clinic";
        clinic = false;
    }
    
    var num = form.num_doc.value;
    var remaining_doc = form.remaining_num_doc.value;
    if(remaining_doc==0 || remaining_doc==""){
        document.getElementById("num_docErrDiv").innerHTML= num+"  Doctors are already added";
        num_of_clinic = false;
    }
    
    var totalChecked = 0;
    for (i = 0; i < form.doctor.length; i++) {
      if (form.doctor[i].selected && form.doctor[i].value!="") {          
          totalChecked++;
      }
    }
    
    if(totalChecked==0 && remaining_doc!=0){
        document.getElementById("doctorErrDiv").innerHTML= "Please select Doctors";
        doctor = false;
    }else if(totalChecked > remaining_doc){
        document.getElementById("doctorErrDiv").innerHTML= "selected Doctors are more than alloted No. of doctors";
        doctor = false;
    }
        
    if(doctor && clinic && num_of_clinic){
        
        document.getElementById("doctor_to_clinic").submit();
    }else{
        return false;
    }
    
    
}
