	function register_content(form)
	{
		Validated = true;        
        document.getElementById("nameErrDiv").innerHTML = ""; 
		document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";		
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = "";  
		document.getElementById("approverErrDiv").innerHTML = ""; 
		document.getElementById("optionsErrDiv").innerHTML = ""; 
        
        var name = form.regname.value;
		var age = form.regage.value;
        var gender = form.reggender.value;
        var email_id = form.regemail.value;
        var password = form.regPassword.value;
        var confirmpassword = form.regConfirmPassword.value;
        var mobile = form.regPhone1.value;
		var approver = form.is_approver.value;
		var content = form.content_writer.value;
		//var tips = form.tip.value;
		//var sms_tips = form.sms_tip.value;
		//var emails = form.email.value;
		//var alls = form.all.value;
		
		
	if ( name =='' )
	{            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
		 if (name != "") {
	 if ( /[^A-Za-z +$]/.test(name)) {
	document.getElementById("nameErrDiv").innerHTML = "Please enter characters only";      	
	//form.regname.focus();
	Validated = false;
    }
	}
	if ( age =='' )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
		Validated = false;
	}else if ( age < 18 )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
		Validated = false;
	}        
        if ( gender =='' )
	{            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
		Validated = false;
	}	
        if ( email_id =='' )
	{            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
		Validated = false;
	}
	if ( email_id !='' )
	{
		var atpos = email_id.indexOf("@");
		var dotpos = email_id.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email_id.length) {
			document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";       
			Validated = false;
	}
	}
        if ( password =='' )
	{            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
		Validated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password cannot be blank";		
		Validated = false;
	}
        if ( password != confirmpassword )
	{            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch Try again!";
		Validated = false;
		//repswValidated = false;
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  Validated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  Validated = false;
		 } 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  Validated = false;
		 }
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 Validated = false;
		 }
			
	}
        if( mobile == "" )
	{            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
		Validated = false;
	} 
	
	if( approver == "" || content =="" )
	{            
   		document.getElementById("approverErrDiv").innerHTML = "Please select Approver/Content_Writer";
		Validated = false;
	} 
	
	if (form.tip.checked==false && form.sms_tip.checked==false && form.email.checked==false && form.all.checked==false )
	{
		document.getElementById("optionsErrDiv").innerHTML = "Please select option";
		Validated  = false;
	}
	
	
	if(Validated)
	{               
               document.getElementById('add_content').submit();                
		}		
		else
		{                
			return false;
		}	
	}
	


	function enable_all(valu)
{
	if (valu.checked == true)
		{
			document.getElementById("tip").checked=true;
			document.getElementById("sms_tip").checked=true;
			document.getElementById("email").checked=true;
			document.getElementById("faq").checked=true;
		}else{
			document.getElementById("tip").checked=false;
			document.getElementById("sms_tip").checked=false;
			document.getElementById("email").checked=false;	
			document.getElementById("faq").checked=false;	
		}
}
	 
	function validate_pswd(form)
{
	var pswValidated = false;
	var repswValidated = false;
	var password = form.regPassword.value;
    var confirmpassword = form.regConfirmPassword.value;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("passwordErrDiv").innerHTML = ""; 
    document.getElementById("repasswordErrDiv").innerHTML = ""; 
	if (password=='')
	{
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";		
    	pswValidated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(confirmpassword!='')
	{
		if(password!=confirmpassword)
		{
			document.getElementById("repasswordErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			 pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			 pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			 pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
			 pswValidated = true;
	}
	 	
}