var docTypeValidated = false;
var docNameValidated = false;
var docEmailValidated = false;
var docpasswordValidated = false;
var docconfirmValidated = false;
var confirmPasswordValidated = false;
function chechValidation(form)
{
    docTypeValidated = true;
    docNameValidated = true;
    docEmailValidated = true;
    docpasswordValidated = true;
    docconfirmValidated = true;
    confirmPasswordValidated = true;
	
    document.getElementById("doc_type_div").innerHTML	= "";
    document.getElementById("doc_name_div").innerHTML = "";
    document.getElementById("doc_emailid_div").innerHTML	= "";
    document.getElementById("doc_password_div").innerHTML = "";
    document.getElementById("confi_password_div").innerHTML	= "";

    if (form.doc_type.value=='')
    {
        document.getElementById("doc_type_div").innerHTML = "Doctor type cannot be blank";
        docTypeValidated = false;
    }
	
    if (form.doctor_list.value=='')
    {
        document.getElementById("doc_name_div").innerHTML = "Name cannot be blank";
        docNameValidated = false;
    }
	
    if (form.doc_emailid.value=='')
    {
        document.getElementById("doc_emailid_div").innerHTML = "Email ID cannot be blank";
        docEmailValidated = false;
    }
	
    if (form.doc_password.value=='')
    {
        document.getElementById("doc_password_div").innerHTML = "Password cannot be blank";
        docpasswordValidated = false;
    }
	
    if (form.confirm_doc_password.value=='')
    {
        document.getElementById("confi_password_div").innerHTML = "Password cannot be blank";
        docconfirmValidated = false;
    }
	
	
    if(form.doc_password.value!=form.confirm_doc_password.value)
	
    {
        document.getElementById("confi_password_div").innerHTML = "Password mismatch Try again!";
        confirmPasswordValidated = false;
    }
	
    if(docTypeValidated && docNameValidated && docEmailValidated && docpasswordValidated && docconfirmValidated && confirmPasswordValidated)
    {
		
        var paramString = 'docId='+ form.doctor_list.value + '&email=' + form.doc_emailid.value;  
        $.ajax({  
            type: "POST",  
            url: "check_email.php",  
            data: paramString,  
            success: function(response) {  
                if(response == 'success')
                    document.getElementById('changePassword').submit();
                else
                    document.getElementById('doc_emailid_div').innerHTML ="Email address already exists";
					
            }  
        });  
		
		
    }
    else
    {
        return false;
    }
}
/*
function enable_doc_submenu()
{
	
	document.getElementById("submenu6").style.display="block";
	document.getElementById("submenu8").style.display="block";
	document.getElementById("submenu9").style.display="block";
	document.getElementById("submenu11").style.display="block";
        document.getElementById("submenu25").style.display="block";
        document.getElementById("submenu26").style.display="block";
	document.getElementById("submenu34").style.display="block";
}
*/	


function doc_password_reset(form){
    docTypeValidated = true;
    docNameValidated = true;
    docEmailValidated = true;
		
	
    document.getElementById("doc_type_div").innerHTML	= "";
    document.getElementById("doc_name_div").innerHTML = "";
    document.getElementById("doc_emailid_div").innerHTML	= "";

    if (form.doc_type.value=='')
    {
        document.getElementById("doc_type_div").innerHTML = "Doctor type cannot be blank";
        docTypeValidated = false;
    }
	
    if (form.doctor_list.value=='')
    {
        document.getElementById("doc_name_div").innerHTML = "Name cannot be blank";
        docNameValidated = false;
    }
	
    if (form.doc_emailid.value=='')
    {
        document.getElementById("doc_emailid_div").innerHTML = "Email ID cannot be blank";
        docEmailValidated = false;
    }
	
    if(docTypeValidated && docNameValidated && docEmailValidated )
    {
        
        $(function() {
	    $('#dialog-confirm').dialog('open');
            $( "#dialog-confirm" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Reset": function() {
			$( this ).dialog( "close" );
			form.but.disabled=true;
			form.but.value = "Processing";
                        document.changePassword.submit();
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });
        });	
    }
    else
    {
        return false;
    }
}
