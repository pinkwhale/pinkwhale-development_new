var addCards = false;
var doctorValidation = false;
function AddCards(form)
{
	addCards = true;
	doctorValidation = true;
	
	document.getElementById("erroeCard").innerHTML	= "";
	document.getElementById("comError").innerHTML	= "";
	document.getElementById("GroupError").innerHTML = "";
	document.getElementById("errorQuery").innerHTML = "";
	document.getElementById("Experterror").innerHTML = "";
	document.getElementById("Specialerror").innerHTML = "";
	document.getElementById("PackageError").innerHTML = "";

	if (form.num_of_cards.value=='')
	{
   		document.getElementById("erroeCard").innerHTML = "Number of cards cannot be blank";
    		addCards = false;
	}

	if (form.group.value=='' && form.doc_type.value=='')
	{
   		document.getElementById("GroupError").innerHTML = "Please select the Doctor Type or Group ";
    		doctorValidation = false;
		
	}

	if (form.doc_type.value!='' && form.doctor.value=='')
	{
   		document.getElementById("comError").innerHTML = "Please select the Doctor";
    		doctorValidation = false;
		
	} 

	if (form.spe_doctors.value=='' && form.exp_doctors.value=='' && form.qry_doctors.value=='' )
	{
   		document.getElementById("PackageError").innerHTML = "Please select Packages ";
    		doctorValidation = false;
		
	}

	if (form.qry_doctors.value!='' && form.Query_1session.checked==false && form.Query_3session.checked==false && form.Query_5session.checked==false)
	{
   		document.getElementById("errorQuery").innerHTML = "Please select session for Query Package ";
    		doctorValidation = false;
		
	}

	if (form.exp_doctors.value!='' && form.Expert_1session.checked==false && form.Expert_2session.checked==false && form.Expert_3session.checked==false && form.Expert_4session.checked==false)
	{
   		document.getElementById("Experterror").innerHTML = "Please select session for Expert Package ";
    		doctorValidation = false;
		
	}

	if (form.spe_doctors.value!='' && (( form.Specialist_3minutes.checked==false && form.Specialist_15minutes.checked==false && form.Specialist_30minutes.checked==false ) && ( form.Spcialist_1email.checked==false  && form.Spcialist_3email.checked==false  && form.Spcialist_5email.checked==false && form.Spcialist_10email.checked==false )))
	{
   		document.getElementById("Specialerror").innerHTML = "Please select session for Spcialist Package ";
    		doctorValidation = false;
		
	}
	
	if(addCards && doctorValidation)
	{
		document.getElementById('add_packages').submit();
	}
		
	else
	{
		return false;
	}
	
}
function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
function setdoctor() 
{	
        
	var doc_type=encodeURI(document.getElementById('doc_type').value);
	$('#doctor').load('get-doctor-list.php?doctor_type='+doc_type);
	
}
function setdoctorlist() 
{	

	var doc_type=encodeURI(document.getElementById('doc_type').value);
	$('#doctor_list').load('get-login-doctor-list.php?doctor_type='+doc_type);
	
}
function setdoctoremailid()
{	
	var doc_type=encodeURI(document.getElementById('doctor_list').value);
	$('#doc_emailid').load('get-login-doctor-emailid.php?doctor_name='+doc_type);
}


function enable()
 {
	var openloc=$('#area').val();
	if(openloc=='others') 
	{
			$('#locality').attr({'disabled':false}); 
	}
	else 
	{
			$('#locality').attr({'disabled':true}); 
	}
}
function enable_disable_doc(form)
{

	if(form.doc_type.value !='')
	{
		form.group.disabled=true;
		form.doctor.disabled=false;
	}
	else
	{
		form.group.disabled=false;
		form.doctor.disabled=true;
		form.physician_1chat.disabled=false;
		form.physician_2chat.disabled=false;
		form.physician_3chat.disabled=false;
		form.Counsellor_60min.disabled=false;
		form.Counsellor_120min.disabled=false;
		form.Counsellor_180min.disabled=false;
		form.Counsellor_1email.disabled=false;
		form.Counsellor_3email.disabled=false;
		form.Counsellor_5email.disabled=false;
		form.Specialist_15minutes.disabled=false;
		form.Specialist_30minutes.disabled=false;
		form.Specialist_60minutes.disabled=false;
		form.Specialist_120minutes.disabled=false;
		form.Specialist_180Minutes.disabled=false;
		form.Spcialist_1email.disabled=false;
		form.Spcialist_3email.disabled=false;
		form.Spcialist_5email.disabled=false;
		form.spe_doctors.disabled=false;
	}
	if(form.doc_type.value=='Specialist')
	{
		form.physician_1chat.disabled=true;
		form.physician_2chat.disabled=true;
		form.physician_3chat.disabled=true;
		form.Counsellor_60min.disabled=true;
		form.Counsellor_120min.disabled=true;
		form.Counsellor_180min.disabled=true;
		form.Counsellor_1email.disabled=true;
		form.Counsellor_3email.disabled=true;
		form.Counsellor_5email.disabled=true;
		form.Specialist_15minutes.disabled=false;
		form.Specialist_30minutes.disabled=false;
		form.Specialist_60minutes.disabled=false;
		form.Specialist_120minutes.disabled=false;
		form.Specialist_180Minutes.disabled=false;
		form.Spcialist_1email.disabled=false;
		form.Spcialist_3email.disabled=false;
		form.Spcialist_5email.disabled=false;
		form.spe_doctors.disabled=false;
		document.getElementById('exp_doctors').value='';
		document.getElementById('spe_doctors').value='';
		document.getElementById('exp_doctors').disabled=false;
		
	}
	else if(form.doc_type.value=='Expert')
	{
		form.Specialist_15minutes.disabled=true;
		form.Specialist_30minutes.disabled=true;
		form.Specialist_60minutes.disabled=true;
		form.Specialist_120minutes.disabled=true;
		form.Specialist_180Minutes.disabled=true;
		form.Spcialist_1email.disabled=true;
		form.Spcialist_3email.disabled=true;
		form.Spcialist_5email.disabled=true;
		form.physician_1chat.disabled=true;
		form.physician_2chat.disabled=true;
		form.physician_3chat.disabled=true;
		form.Counsellor_60min.disabled=true;
		form.Counsellor_120min.disabled=true;
		form.Counsellor_180min.disabled=true;
		form.Counsellor_1email.disabled=true;
		form.Counsellor_3email.disabled=true;
		form.Counsellor_5email.disabled=true;
		form.spe_doctors.disabled=true;
		document.getElementById('spe_doctors').value='';
		document.getElementById('exp_doctors').value='';
		
	}
}
function enable_disable_grup(form)
{
	if(form.group.value !='')
	{
		form.doc_type.disabled=true;
	}
	else
	{
		form.doc_type.disabled=false;
	}

}
function enable_cards_submenu()
{
	document.getElementById("submenu10").style.display="block";
}


