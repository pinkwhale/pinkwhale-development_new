function delete_specialities_msg() 
{
	var chks = document.getElementsByName('delete[]');
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].checked==true)
		{
			var confrm=confirm("Are you sure of deleting the  Records  (By clicking Ok, You are permanently deleting the record )");
			if (confrm)
			{
				document.getElementById('delete_speciality_form').submit();
				return true;
			}
		}
	}
	alert("Please Select the Speciality !");
}