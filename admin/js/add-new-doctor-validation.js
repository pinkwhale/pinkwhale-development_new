var tele_cons_typeValidated =false;
var nameValidated = false;
//var mobileValidated = false;
var typeValidated = false;
var cons_typeValidated = false;
var genderValidate = false;
//var emailValidate = false;
var qualiValidation = false;
var specialityValidation = false;
var timingValidation = false;
var add1Validation = false;
var telconsulnumvalidation = false;
var aboutValidation = false;
var num_of_doc_Validated = false;

function add_doctor_validate(form)
{
	tele_cons_typeValidated = true;
	nameValidated = true;
	//mobileValidated = true;
	typeValidated = true;
	cons_typeValidated = true;
	genderValidate = true;
	//emailValidate = true;
	qualiValidation = true;
	specialityValidation = true;
	timingValidation = true;
	add1Validation = true;
	telconsulnumvalidation = true;
	aboutValidation = true;
    num_of_doc_Validated = true;
        
	document.getElementById("nameErrDiv").innerHTML	= "";
	document.getElementById("cityError").innerHTML	= "";
	//document.getElementById("mobileErrDiv").innerHTML = "";
	document.getElementById("typeErrDiv").innerHTML = "";
	document.getElementById("cons_typeErrDiv").innerHTML	= "";
	document.getElementById("genderError").innerHTML = "";
	//document.getElementById("emailError").innerHTML = "";
	document.getElementById("speError").innerHTML = "";
	document.getElementById("qliError").innerHTML = "";
	document.getElementById("timeError").innerHTML = "";
	document.getElementById("add1Error").innerHTML = "";
	document.getElementById("telconnumError").innerHTML = "";
	document.getElementById("aboutError").innerHTML = "";
        document.getElementById("num_of_clinicErrDiv").innerHTML = "";
	/*if (form.about_doc.value=='')
	{
   		document.getElementById("aboutError").innerHTML = "Please Enter About Doctor";
    	aboutValidation = false;
	}*/
	if (form.doc_address1.value=='')
	{
   		document.getElementById("add1Error").innerHTML = "Please Enter Address 1";
    	add1Validation = false;
	}
	if (form.doc_qlifiction.value=='')
	{
   		document.getElementById("qliError").innerHTML = "Please Enter Qualification";
    	qualiValidation = false;
	}
	if (form.doc_spclitis.value=='' && (form.doctor_spe.checked==true || form.doctor_exp.checked==true || (form.doctor_spe.checked==true && form.doctor_exp.checked==true)))
	{
   		document.getElementById("speError").innerHTML = "Please Select Specialities";
                specialityValidation = false;
	}
	/*if (form.doc_email_id.value=='')
	{
   		document.getElementById("emailError").innerHTML = "Please Enter Email ID";
    	emailValidate = false;
	}*/
	if (form.doc_gender.value=='')
	{
   		document.getElementById("genderError").innerHTML = "Please Select Gender";
    	genderValidate = false;
	}
	if (form.city.value=='')
	{
   		document.getElementById("cityError").innerHTML = "Please enter city name";
                genderValidate = false;
	}
	if (form.doc_name.value=='')
	{
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
    	nameValidated = false;
	}
	/*if (form.doc_mobile.value=='')
	{
		document.getElementById("mobileErrDiv").innerHTML = "Mobile Number cannot be blank";
		mobileValidated = false;
	}*/
	//if (form.doctor_phy.checked==false && form.doctor_spe.checked==false && form.doctor_con.checked==false && form.doctor_exp.checked==false && form.doctor_app.checked==false && form.doctor_qry.checked==false )
        if (form.doctor_spe.checked==false && form.doctor_exp.checked==false && form.doctor_app.checked==false && form.doctor_qry.checked==false && form.doctor_app_not_signed.checked==false && form.doctor_tele_qry.checked==false )
	{
		document.getElementById("typeErrDiv").innerHTML = "Please select Doctor type";
		typeValidated  = false;
	}
       
        if(form.doctor_app.checked==true && form.num_clinic.value==0){
            document.getElementById("num_of_clinicErrDiv").innerHTML = "Please enter Number of clinics.";
            num_of_doc_Validated  = false;
        }
        
	if (form.doctor_spe.checked==true && (form.doctor_tele_con.checked==false && form.doctor_online_con.checked==false && form.doctor_diag_con.checked==false && form.doctor_none_con.checked==false ))
	{
		document.getElementById("cons_typeErrDiv").innerHTML = "Please select Consultation type";
		cons_typeValidated = false;
	}
	
	

	if (form.doctor_tele_con.checked==true && form.doc_tele_number.value=='')
	{
		
		document.getElementById("telconnumError").innerHTML = "Please Enter Tele Consultation Number";
		tele_cons_typeValidated = false;
	}
	if (form.doctor_tele_con.checked==true && form.doc_timings.value=='')
	{
		document.getElementById("timeError").innerHTML = "Please Enter Timings";
		timingValidation = false;
	}

	if( add1Validation   && qualiValidation && specialityValidation && genderValidate && nameValidated && typeValidated && cons_typeValidated  && tele_cons_typeValidated && timingValidation)
	{

			var paramString = 'email=' + form.doc_email_id.value + '&docId=""';  
			
		$.ajax({  
			type: "POST",  
			url: "check_email.php",  
			data: paramString,  
			success: function(response) {  
				if(response == "true")
				{
					document.getElementById('emailError').innerHTML ="";
					document.getElementById('add_doc_form').submit();
				}
				else {
					document.getElementById('emailError').innerHTML ="Email address already exists";
				}
			}  
		});  
		
	}
	else
	{
		return false;
	}
	
}
