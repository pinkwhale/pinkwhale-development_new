<?php 
	session_start();

	include ("../includes/pw_db_connect.php");

     if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')

	{

		header("Location: ../index.php");

		exit();

	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>pinkwhalehealthcare</title>

<meta name="description" content="pinkwhalehealthcare">

<link href="../css/designstyles.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">

<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />

<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/registration_validation.js"></script>

<script type="text/javascript" src="../js/enable-menu.js"></script>

<script src=http://cdn.webrupee.com/js type="text/javascript"></script>

<script language="javascript" src="calendar/calendar.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>

</head>

<body>

<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">

<style>	

a:hover{

	color:#e70976;

	text-decoration:underline;

}

</style>

<?php include "admin_head.php"; ?>

<!-- side Menu -->

<?php

require_once('calendar/classes/tc_calendar.php');

?>
<?php

include("includes/host_conf.php");

include("includes/mysql.lib.php");

include('includes/ps_pagination.php');

$obj=new connect;

?>



<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >

<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">



<?php include "admin_left_menu.php"; ?>

<td width="850" >

<!-- ///////////////////       Create Promo Code        /////////////////////// -->



<form method="post" name="add_promo_code" id="add_promo_code" action="actions/create_promocode.php">

<table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">

    <tr><th colspan="2">Create Promo Code </th></tr>

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    

    <tr>    

        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Doctor Type:</div></td>

        <td bgcolor="#F5F5F5" width="219">

<select  name="promo_doc_type" id="promo_doc_type" class="registetextbox" onChange="setdoctorpackages();"> 

			   <option value="" selected="selected" disabled>- Select Doctor Type -</option>

       		   <option value="Physician">Physician</option>

               <option value="Specialist">Specialist</option>

               <option value="Counseller">Counseller</option>

               <option value="Expert">Expert</option>
               

        </select>

        </td>

    </tr>

    

    <!--    ERROR DIV -->



		<tr><td> </td>

        <td  align="left">

	    <div id="doc_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

        </tr>

<!--  END ERROR DIV --> 

    

    <tr>

    <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

     <tr>    

        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Packages:</div></td>

        <td bgcolor="#F5F5F5" width="219">

		<select  name="promo_packages" id="promo_packages" class="registetextbox" > 

			 <option value="">Select Package</option>

       		  

        </select>

        </td>

    </tr> 

    

     <!--    ERROR DIV -->



		<tr><td> </td>

        <td  align="left">

	    <div id="package_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

        </tr>

<!--  END ERROR DIV --> 

      

     <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

     <tr>    

        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Promo Code:</div></td>

        <td bgcolor="#F5F5F5" width="219">

		 <input type="text" name="promocode" class="input" size="20" />

        </td>

    </tr>

    

     <!--    ERROR DIV -->



		<tr><td> </td>

        <td  align="left">

	    <div id="promocode_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

        </tr>

<!--  END ERROR DIV --> 

    

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

     <tr>    

        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Discount: </div></td>

        <td bgcolor="#F5F5F5" width="219">

		 <input type="text" name="promo_discount" class="input" size="20" />&nbsp;%

        </td>

    </tr>

     <!--    ERROR DIV -->



		<tr><td> </td>

        <td  align="left">

	    <div id="discount_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

        </tr>

<!--  END ERROR DIV --> 

    

    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

     <tr>    

        <td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">Expiry:</div></td>

        <td bgcolor="#F5F5F5" width="219">

		<?php

	  $myCalendar = new tc_calendar("date5", true, false);

	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");

	  $myCalendar->setDate(date('d'), date('m'), date('Y'));

	  $myCalendar->setPath("calendar/");

	  $myCalendar->setYearInterval(1910, 2015);

	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');

	  $myCalendar->setDateFormat('j F Y');

	  //$myCalendar->setHeight(350);	  

	  //$myCalendar->autoSubmit(true, "form1");

	  $myCalendar->setAlignment('left', 'bottom');

	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');

	  $myCalendar->writeScript();

	  ?>

        </td>

    </tr>



    <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

    <tr>

       

    <td bgcolor="#F5F5F5">&nbsp;</td>

    <td bgcolor="#F5F5F5"><input type="button" value="Add" name="add_pomocode" id="add_pomocode" onclick="create_promo_code(add_promo_code)"/></td>

    </tr>

    </table>

    </form>








<form action="actions/delete_group.php" name="delete_group_form" id="delete_group_form" method="post">

		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>

		<tr><th colspan="7">Promo Code Details</span></th></tr>

<?php		

		// Display all the data from the table 

			$sql="SELECT * FROM `create_promo_code` order by promo_code_id ASC";

			$obj->query($sql);	

			

			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");

			$pager->setDebug(true);

			$rs = $pager->paginate();

			if(!$rs) { ?>

				<tr>

                    <td align='center' colspan="7"><strong>Promocode details are empty</strong></td>

				

              <?php  } 

			  

			  else {

			  ?>

				<tr>

                    <td align='left'><strong>Doctor Type</strong></td>

                    <td align='left'><strong>Package</strong></td>

                    <td align='left'><strong>Promo Code</strong></td>

                    <td align='left'><strong>Discount</strong></td>

                    <td align='left'><strong>Expiry</strong></td>

                    <td align='center'><strong>Edit</strong></td>

                    <td align='center'><strong>Delete</strong></td>                   

                    </tr>

			

			

			<?php	

			$count = 0;

			while($row = mysql_fetch_assoc($rs)) 

			{

			   $count ++;

				$promo_doctor_type=$row['promo_doctor_type'];

				$promo_package=$row['promo_package'];

				$promo_code=$row['promo_code'];

				$promo_discount=$row['promo_discount'];

				$promo_expiry=$row['promo_expiry'];

				$arr = explode('-', $promo_expiry);

				$date = $arr[2].'-'.$arr[1].'-'.$arr[0];

				echo "<tr bgcolor='#ffffff'>";

				echo "<td>$promo_doctor_type</td>"; 

				?>

                

        	<!--<a href="edit-group.php?id=<?php echo $id ?>" style="text-decoration:none;"> <?php echo $groupname ; ?></a></td>-->

			<!--<a href="manage-group.php" style="text-decoration:none;"> <?php echo $username ; ?></a></td> -->

          

		  

		  <?php

		  		if($promo_package=="spe_15_minutes") $pakage="15 Minutes";

				elseif($promo_package=="spe_30_minutes") $pakage="30 Minutes";

				elseif($promo_package=="spe_60_minutes") $pakage="60 Minutes";

				elseif($promo_package=="spe_120_minutes") $pakage="120 Minutes";

				elseif($promo_package=="spe_180_minutes") $pakage="180 Minutes";

				elseif($promo_package=="spe_1_emails_session") $pakage="1 Email Session";

				elseif($promo_package=="spe_3_mails_session") $pakage="3 Email Session";

				elseif($promo_package=="spe_5_email_session") $pakage="5 Email Session";
                                
                                elseif($promo_package=="spe_10_email_session") $pakage="10 Email Session";

				

				elseif($promo_package=="phy_1_chat_session") $pakage="1 Chat Session";

				elseif($promo_package=="phy_3_chat_session") $pakage="3 Chat Session";

				elseif($promo_package=="phy_5_chat_session") $pakage="5 Chat Session";

				

				elseif($promo_package=="exp_1_email_session") $pakage="1 Email Session";

				elseif($promo_package=="exp_3_email_session") $pakage="3 Email Session";

				elseif($promo_package=="exp_5_email_session") $pakage="5 Email Session";
                                
                                elseif($promo_package=="exp_10_email_session") $pakage="10 Email Session";

				

				elseif($promo_package=="cun_60_minutes") $pakage="60 Minutes";

				elseif($promo_package=="cun_120_minutes") $pakage="120 Minutes";

				elseif($promo_package=="cun_180_minutes") $pakage="180 Minutes";

				elseif($promo_package=="cun_1_email_session") $pakage="1 Email Session";

				elseif($promo_package=="cun_3_email_session") $pakage="3 Email Session";

				elseif($promo_package=="cun_5_email_session") $pakage="5 Email Session";
                                
                                elseif($promo_package=="cun_10_email_session") $pakage="10 Email Session";

		  

				echo "<td>$pakage</td> ";

				echo "<td>$promo_code</td>";

				echo "<td>$promo_discount%</td>";

				echo "<td>$date</td>"; ?>

                

				<td align="center"><a href="edit_promo_code.php?promo_id=<?php echo $row['promo_code_id'];?>"> <img src="images/EditIcon.gif"  /></a></td>

				<td align="center"> <a href="actions/delete_promo_code.php?promo_id=<?php echo $row['promo_code_id'];?>"><img src="images/delete_icon_16.gif" /></a></td>

				</tr>

			<?php } 

			

			$ps1 = $pager->renderFirst();

			$ps2 = $pager->renderPrev();

			$ps3 = $pager->renderNav('<span>', '</span>');

			$ps4 = $pager->renderNext();

			$ps5 = $pager->renderLast();

			?>            

	

			<tr><td colspan='7' align='center'>



			

            <?php echo "$ps1"; ?> 

			

            <?php

            echo "$ps2";

			echo "$ps3";

			echo "$ps4";

			echo "$ps5";

			echo "</td></tr>"; 

			}?>

       </table>



      </form>





</table>



<script type="text/javascript">

	

function setdoctorpackages()

 {

	var doc_type=encodeURI(document.getElementById('promo_doc_type').value);

	$('#promo_packages').load('get-doctor-packages.php?doctor_type='+doc_type);

}



/*var typeValidated = false;

var packageValidated = false;

var promocodeValidated = false;

var discountValidated = false;

var expiryValidated = false;
*/
//i commented............................
	

function create_promo_code(form)

	{

//	alert("Test");

	typeValidated = true;

	packageValidated= true;

	promocodeValidated= true;

	discountValidated= true;

	

	document.getElementById("doc_typeErrDiv").innerHTML	= "";

	document.getElementById("package_typeErrDiv").innerHTML= "";

	document.getElementById("promocode_typeErrDiv").innerHTML = "";

	document.getElementById("discount_typeErrDiv").innerHTML = "";

	

	if (form.promo_doc_type.value=='')

	{

		document.getElementById("doc_typeErrDiv").innerHTML = "Doctor type cannot be blank";

    	typeValidated = false;

	}

	

	if (form.promo_packages.value=='')

	{

		document.getElementById("package_typeErrDiv").innerHTML = "Please select package";

    	packageValidated = false;

	}

	

	if (form.promocode.value=='')

	{

		document.getElementById("promocode_typeErrDiv").innerHTML = "Please enter promo code";

    	promocodeValidated = false;

	}

	

	if (form.promo_discount.value=='')

	{

		document.getElementById("discount_typeErrDiv").innerHTML = "Please enter discount (%)";

    	discountValidated = false;

	}

	

	if(typeValidated && packageValidated && promocodeValidated && discountValidated)

	{

		document.getElementById('add_promo_code').submit();

	}

	else

	{

		return false;

	}

	

}

</script>    

<script type="text/javascript">

 	enable_submenu();

</script>

 <?php include 'admin_footer.php'; ?>

</body></html>

