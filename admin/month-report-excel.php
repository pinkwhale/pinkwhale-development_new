<?php 

error_reporting(E_PARSE);
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}
//for pagination and database connection

$start_year = "2011";
$current_year = strftime ( '%Y', (time()));
$current_month = strftime ( '%m', (time()));


$type = $_REQUEST['type']; 
$doc = $_REQUEST['doc'];
$month = $_REQUEST['month'];
if($month==""){
     $month=$current_month;
}
$year = $_REQUEST['year']; 
if($year==""){
    $year = $current_year;
}
if($type==""){
    $type="spe_online";
}


$filename = $type."-$month-$year-month-report.xls"; 

header('Content-Type: application/vnd.msexcel');
header('Content-Disposition: ; filename="'.$filename.'"');

include 'summary.report.class.php';
$report = new SummaryReport;
$type_name = $report->getTypeName($type);
 ?>
<table  cellspacing="0" cellpadding="0" align="center" >
    <tr>
        <td valign="top"  >
            
            
            <br /><br />
                
                 <?php       
                    
                    $extend = "";
                    
                    if($doc!="all" && $doc!=""){
                        $extend = " and doc_id='$doc'";
                    }
                    
                    
                    
                    if($month=="" || $year==""){
                        $datetmp = strftime ( '%Y-%m-%d', (time() - (60*60*24*6))); 
                    }else{
                        $datetmp = "$year-$month-01";    
                    }
                    
                    
                    
                    
                    $display =  $type_name." Data for ".$month."-".$year;
                    
                    echo  "<strong>$display<strong>";
                    
                    $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                    
                    if($type=="spe_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_online_consultation='online' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }else if($type=="exp_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where  blocked<>'Y' $extend and ( doc_category='Specialist/Expert' || doc_category='Expert')";
                    }else if($type=="query_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' $extend and activate_online_query='query'";
                    }else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
                        $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
                    }else if($type=="spe_tele"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_tele_Consultation='tele' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }
                    
                    $doc_result = mysql_query($doc_qry);
                    $doc_num = mysql_num_rows($doc_result);
                    if($doc_num>0){
                        
                        
                        if($type=="spe_tele"){

                            echo "<table border='0' cellpadding='1' cellspacing='2' width='90%'  align='center'><tr>";
                            echo "<th  align='left' rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th  align='left'  rowspan='2'></th>";

                            for($i=0;$i<$daysinmonth;$i++){
                                $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
                                $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
                                $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                echo "<th  align='left' colspan=2><b>$day<br/>$date</b></th>";
                            }
                            
                            echo "<th  align='left'  rowspan='2'></th>";
                            
                            echo "</tr><tr>";
                            
                            for($i=0;$i<$daysinmonth;$i++){
                                $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
                                $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
                                $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                echo "<th  align='left' nowrap><b>Calls</th><th>Secs</b></th>";
                            }

                            

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";
                                echo "<td></td>";

                                for($i=0;$i<7;$i++){                            
                                    $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                    $count = $report->getdatewiseTeleSummary($type,$date,$doc_id,'count');
                                    echo "<td  align='center'>$count</td>";
                                    $count = $report->getdatewiseTeleSummary($type,$date,$doc_id,'pulse');
                                    echo "<td  align='center'>$count</td>";
                                }

                                echo "<td></td>";
                                echo "</tr>";
                            }

                            echo "</table>";                            
                        }else if($type=="query_online"){

                            echo "<table border='0' cellpadding='1' cellspacing='2' width='90%'  align='center'><tr>";
                            echo "<th  align='left' rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th  align='left' rowspan='2'>
                            
                            </th>";

                            for($i=0;$i<$daysinmonth;$i++){
                                $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
                                $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
                                $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                echo "<th  align='left' colspan='2'><b>$day<br/>$date</b></th>";
                            }

                            echo "<th  align='left' rowspan='2'>
                            
                            </th>";

                            echo "</tr><tr>";
                            
                            for($i=0;$i<$daysinmonth;$i++){
                                $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
                                $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
                                $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                echo "<th  align='left' nowrap><b>Physician</th><th nowrap>Specialist</b></th>";
                            }

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";
                                echo "<td></td>";

                                for($i=0;$i<$daysinmonth;$i++){                            
                                    $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                    $count = $report->getdatewisequerySummary('phy',$date,$doc_id);
                                    echo "<td  align='center'>$count</td>";
                                    $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                    $count = $report->getdatewisequerySummary('spe',$date,$doc_id);
                                    echo "<td  align='center'>$count</td>";
                                }

                                echo "<td></td>";
                                echo "</tr>";
                            }

                            echo "</table>";                            
                        }else{

                            echo "<table border='0' cellpadding='1' cellspacing='2' width='90%'  align='center'><tr>";
                            echo "<th  align='left'><b>Doctor Name</b></th>";
                            echo "<th  align='left'>
                            
                            </th>";

                            for($i=0;$i<$daysinmonth;$i++){
                                $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
                                $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
                                $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                echo "<th  align='left'><b>$day<br/>$date</b></th>";
                            }

                            echo "<th  align='left'>
                            
                            </th>";

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";
                                echo "<td></td>";

                                for($i=0;$i<$daysinmonth;$i++){                            
                                    $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                                    $count = $report->getdatewiseOnlineSummary($type,$date,$doc_id);
                                    echo "<td  align='center'>$count</td>";
                                }

                                echo "<td></td>";
                                echo "</tr>";
                            }

                            echo "</table>";
                        }
                        
                    }else{
                        echo "No Records Found";
                    }
                ?>
                
             
            
        </td>
    </tr>   
</table>
