<?php
  	include("Sajax.php");
	sajax_init();
	sajax_export('getdata');
	sajax_handle_client_request();
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cards.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/doc-changepassword-validation.js"></script>
</head>
<body style="font-size:62.5%;">
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<?php include "../includes/pw_db_connect.php"; ?>
<?php include "admin_head.php"; ?>
<script type="text/javascript">
<?php sajax_show_javascript(); ?>
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
	
<script>
      
        function doc_password_reset(form){
            docTypeValidated = true;
            docNameValidated = true;
            docEmailValidated = true;


            document.getElementById("doc_type_div").innerHTML	= "";
            document.getElementById("doc_name_div").innerHTML = "";
            document.getElementById("doc_emailid_div").innerHTML	= "";

            if (form.doc_type.value=='')
            {
                document.getElementById("doc_type_div").innerHTML = "Doctor type cannot be blank";
                docTypeValidated = false;
            }

            if (form.doctor_list.value=='')
            {
                document.getElementById("doc_name_div").innerHTML = "Name cannot be blank";
                docNameValidated = false;
            }

            if (form.doc_emailid.value=='')
            {
                document.getElementById("doc_emailid_div").innerHTML = "Email ID cannot be blank";
                docEmailValidated = false;
            }

            if(docTypeValidated && docNameValidated && docEmailValidated )
            {
/*
                $(function() {
                    $('#dialog-confirm').dialog('open');
                    $( "#dialog-confirm" ).dialog({
                        resizable: false,
                        modal: true,
                        buttons: {
                            "Reset": function() {
                                $( this ).dialog( "close" );
                                form.but.disabled=true;
                                form.but.value = "Processing";
                                document.changePassword.submit();
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                });	
*/
 		    var val = confirm("Are You Sure.");
                    if(val){
                        form.but.disabled=true;
                        form.but.value = "Processing";
                        document.changePassword.submit();
                    }else{
                        return false;
                    }
            }
            else
            {
                return false;
            }
        }



  
  </script>
   
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}

</style>
<style type="text/css">
	.ui-dialog { font-size: 11px; }
	body {
		font-family: Tahoma;
		font-size: 12px;
	}
	#question {
		width: 300px!important;
		height: 60px!important;
		padding: 10px 0 0 10px;
	}
	#question img {
		float: left;
	}
	#question span {
		float: left;
		margin: 20px 0 0 10px;
	}
        .ui-widget-header { border: 1px solid #E10971; background: #E10971 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
<td width="850" valign="top" id="mainBg">

<form action="actions/reset_doc_password.php" method="post" name="changePassword" id="changePassword">
  <table width="521" border="0" cellspacing="0" cellpadding="0" align="center" >
  	<tr><td width="717"><table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform">
        <tr><th colspan="2" align="left">Reset Doctor Login</th></tr>
        <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
  		<tr></tr>
        	<td width="40%" align="right" bgcolor="#F5F5F5">Doctor Type<font color="#FF0000">*</font>:</td>
  			
            <td width="60%" align="left" bgcolor="#F5F5F5">
  				 <select  name="doc_type" id="doc_type" class="registetextbox" onChange="setdoctorlist();" > 
			   <option value="" selected="selected" disabled>-Select doctor type-</option>
               <option value="Specialist">Specialist </option>
               <option value="Expert">Expert</option>
               <option value="Counsellor">Counsellor</option>
	       <option value="appointment">Clinic Appointment</option>
               <option value="query">Query</option>
  		</select>
        	</td>
        </tr>
        
<tr>
    <td></td>
    <td height="10"><div id="doc_type_div" style="color: #F33;font-family:verdana;font-size:10px;"></div></td>
</tr> 

<tr>
	 <td align="right" bgcolor="#F5F5F5">Doctor Name<font color="#FF0000">*</font>:</td>
     <td nowrap="nowrap" bgcolor="#F5F5F5"><select  name="doctor_list" id="doctor_list" class="registetextbox" onChange="setdoctoremailid();"> 
			  <option value="" >- Select Doctor-</option>
               
        </select>
       </td>
</tr>

<tr>
     <td></td>
     <td height="10" >
     <div id="doc_name_div"  style="color: #F33;font-family:verdana;font-size:10px; "></div>
     </td>
 </tr> 

<tr>
        <td align="right" bgcolor="#F5F5F5">Doctor Email ID<font color="#FF0000">*</font>:</td>
  			
        <td bgcolor="#F5F5F5" id="doc_emailid"><input disabled="disabled" size="30"  id="doc_emailid" name="doc_emailid" />
        <span style="color:green" id='result'></span></td>
</tr>
       
<tr>
 	<td></td>
 	<td height="10"><div id="doc_emailid_div" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>
</tr>
 


<tr>
     <td bgcolor="#F5F5F5">&nbsp;</td>
     <td bgcolor="#F5F5F5">&nbsp;&nbsp;&nbsp;&nbsp;<input onmouseover="this.style.cursor='pointer'" id="but"  value="Reset" tabindex="6" type="button" onclick="doc_password_reset(changePassword)"></td>
        </tr>
      </table>
   </td></tr> </table>
  </form>
</td></tr></table>

  <?php
include 'admin_footer.php'; ?>
<script type="text/javascript">
 	enable_doc_submenu();
</script>
<div id="dialog-confirm" title="Reset Doctor Password !!" style="display:none" >
    <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Dear Admin,<br /> Are You Sure ?</p>
</div>
</body></html>
