<?php

session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}

$login=$_SESSION['login'];

$f_date1 = $_GET['start_date'];
$t_date1 = $_GET['end_date'];

list($f_part1, $f_part2) = explode('/', $f_date1);
list($t_part1, $t_part2) = explode('/', $t_date1);

$f_date=$f_part2."-".$f_part1."-"."01";

$lastday = date('t', strtotime($t_part2.'-'.$t_part1.'-01'));
$t_date=$t_part2."-".$t_part1."-".$lastday;

include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
require('pdf/fpdf.php');
$d=date('d_m_Y');
$doc_id=$_GET['doc_name'];
//echo $doc_id;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="css1/main.css" rel="stylesheet" type="text/css" />

<!-- add scripts -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="js1/highcharts.js"></script>
<script src="js1/gray.js"></script>

</head>
<body style="background: #ffffff;">
	<table style="width: 100%;">
		<tr>
			<td align="left" style="width: 50%;">
				<table style="width: 100%;">
					<tr>
						<td style="font-family: arial;">pinkWhale Healthcare
							Services Pvt. Ltd.</td>

					</tr>
					<tr>
						<td style="font-family: arial;">No. 275, 16th Cross
							2nd Block, R.T. Nagar, Bangalore 560032</td>
					</tr>
					
					<tr>
						<td style="font-family: arial;">Phone: +91 - (080) 2333 1278.</td>
					</tr>
					<tr>
						<td style="font-family: arial;">Email:
							marketing@pinkwhalehealthcare.com</td>
					</tr>
				</table>
			</td>
			<td align="right" style="width: 50%;">
				<table style="width: 100%;" style="margin-top: 0px;">
					<tr>
						<td align="right"><img src="logo.jpg" width="380" height="125" title="Logo of a company" alt="Logo of a company" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		<hr width="100%" />

	<div id="ab" align="center" style="display: none;">
		<label
			style="color: #800080; font-family: arial; font-size: 21px; margin-top: 10px;">Graphical
			Representation Of Number Of TopUps Up By Doctor By Month</label>
		<div id="chart_1" class="chart"></div>
	</div>



<?php 

$result1=mysql_query("SELECT doc_name FROM pw_doctors  WHERE doc_id='$doc_id'");
while($row=mysql_fetch_array($result1))
{
	$name1=$row['doc_name'];
}
?>
<table style="width:100%;">
	<tr>
		<td align="center"><label style="font-family: arial; font-weight: bolder;">Number Of
				TopUps By Doctor By Month</label>
		</td>
	</tr>
			<tr height="12px;">
		</tr>
	<tr>
		<td align="center"><label
			style="font-family: arial; font-weight: bolder; color: #0000FF;"><?php echo $name1;?>
		</label>
		</td>
	</tr>
				<tr height="12px;">
		</tr>
	<tr>
		<td align="center"><label style="font-family: arial;">Between</label>
		</td>
	</tr>
				<tr height="12px;">
		</tr>
	<tr>
		<td align="center"><label style="font-family: arial; color: #0000FF;"><?php echo $f_date?>
		</label> <label style="padding: 11px; font-family: arial;">And</label>
			<label style="padding: 11px; font-family: arial; color: #0000FF;"><?php echo $t_date?>
		</label>
		</td>
	</tr>
</table>

<?php 
function getNextMonth($date)//date format 'Y-m-d'
{
	$date_tmp = explode("-",$date);
	$next_date =mktime(0, 0, 0, $date_tmp[1]+1, $date_tmp[2], $date_tmp[0]);
	return date('Y-m-d',$next_date);
}


$months=Array();
$year=Array();
$i=1;
$start = date($f_date);
$end = date($t_date);
$dateArray = Array();
$dateArray[$i] = $start;
$i++;
while($start <= $end)
{
	$curDate  = getNextMonth($start);
	$dateArray[$i] = $curDate;
	$start = $curDate;
	$i++;
}
?>

<table style="width:100%">
	<tr>
		<td align="center">
<table style="width:90%" border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>

	<tr>
		<td align='center'
			style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>SL
				No</strong></td>
		<td align='center'
			style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>Payment
				Month</strong></td>
		<td align='center'
			style="background: #e10971; font-family: arial; color: #FFFFFF;"><strong>Number
				Of Topups</strong></td>

	</tr>



	<?php 
	$rowArray1=Array();
	for($j=1;$j<=sizeOf($dateArray)-1;$j++)
{?>
	<tr bgcolor='#ffffff'>
		<?php

		$queryDate = $dateArray[$j];
		//$month=date("m",strtotime($queryDate));
		$month=date("F", strtotime($queryDate));
		$year1=date("Y",strtotime($queryDate));
		//echo $year1."  ".$month."<br/>";
///Added one condition AND package_details!='Admin Added packages' for filtering admin packages on 14-03-2014 after discussion with Anita
		$TopUp_by_doc=mysql_query("SELECT b.doc_name as name,count(*) as cn FROM card_topup_payment_report a JOIN pw_doctors b ON b.doc_id=a.doc_Id  WHERE a.doc_Id='$doc_id' AND MONTHNAME(a.payment_date)='$month' AND YEAR(a.payment_date)='$year1' AND package_details!='Admin Added packages' GROUP BY MONTH(a.payment_date)");
		$row1=(mysql_fetch_array($TopUp_by_doc));
		if(mysql_num_rows($TopUp_by_doc) == 0)
			$row1['cn']=0;
		$rowArray1[$j]=$row1['cn'];
		?>
		<td align='center' style="font-family: arial;"><?php echo $j;?></td>
		<td align='center' style="font-family: arial;"><?php echo date("F, Y", strtotime($dateArray[$j]));?>
		</td>
		<td align='center' style="font-family: arial;"><?php echo $row1['cn'];?></td>
	</tr>

	<?php }
		if (!is_dir('../images/Reports'))
		// is_dir - tells whether the filename is a directory
	{
		//mkdir - tells that need to create a directory
		mkdir('../images/Reports');
	}

	?>

	</table>
	</td></tr>
	<tr>
		<td align="center">
<table align="center" style="width:30%;">
<?php 	$d=date('Y-m-d H-i-s');?>
<?php $p="-TopUp-By-Month-";?>
	<tr>
	
	<td><a href="<?php echo "../images/Reports/".$name1."$p.$d.pdf";?>"><input type="button" name="download" value="Download Report"></a></td>

	<td>
				<input type="button" value="View Report" name="view" id="view"></td>
				<td><input type="button" value="Send Report" name="send"></td>
				</tr>
	</table>
	</td></tr>
	


</table>
</body>
<script>
	// Change Chart type function
	function ChangeChartType(chart, series, newType) {
		newType = newType.toLowerCase();
		for (var i = 0; i < series.length; i++) {
			var srs = series[0];
			try {
				srs.chart.addSeries({
					type: newType,
					stack: srs.stack,
					yaxis: srs.yaxis,
					name: srs.name,
					color: srs.color,
					data: srs.options.data
				},
				false);
					series[0].remove();
			} catch (e) {
			}
		}
	}
	
	// Two charts definition
	var chart1;
	
	// Once DOM (document) is finished loading
	$("#view").click(function() {
	//alert("f");
		$("#ab").show("slow");
		//alert("g");
		// First chart initialization
		chart1 = new Highcharts.Chart({
			chart: {
				renderTo: 'chart_1',
				type: 'column',
				height: 350,
			},
			title: {
				text: 'TopUps By <?php echo $name1?> By Month'
			},
			xAxis: {
				title: {
					text: 'Months'
				},
			
				categories: [<?php for($j=1;$j<=sizeof($dateArray);$j++) { echo "'".date("F, Y", strtotime($dateArray[$j]))."',";}?>] 
			},
			yAxis: {
				title: {
					text: 'Number of Topups'
				}
			},
			series: [{
				
				name: 'Number of Topups',
				data: [<?php for($j=1;$j<=sizeof($rowArray1);$j++){ echo $rowArray1[$j].",";}?>]
			}]
		});
	
	
	
			// Switchers (of the Chart1 type) - onclick handler
			$('.switcher').click(function () {
				var newType = $(this).attr('id');
				ChangeChartType(chart1, chart1.series, newType);
			});
	});
	</script>
</html>
<?php 
class PDF extends FPDF
{

	function Header()
	{
		//Logo
		$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"pinkWhale Healthcare Services Pvt. Ltd.");
		$this->Image('pdf/logo.jpg',160,6,40);
		$this->Ln(3);$this->SetFont('Arial','B',7);
		$this->Cell(50,6,"No. 275, 16th Cross 2nd Block, R.T. Nagar, Bangalore 560032");$this->Ln(3);
		$this->Cell(50,6,"Phone: +91 - (080) 2333 1278.");$this->Ln(3);
		$this->Cell(50,6,"Email: marketing@pinkwhalehealthcare.com");$this->Ln(3);

		$name="Export PDF";
		$this->SetFont('Arial','B',15);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->SetFont('Arial','B',9);
		//Line break
		$this->Line(0, 25, 300, 25);
		$this->Ln(10);
		$this->SetFont('Arial','B',12);
		$this->Cell(50);
		$this->Cell(80,10,"Number Of Topups By Doctor By Month",'C');
		$this->Ln(10);
	}
	function doc_details($f_date,$t_date,$name){
		$this->SetFont('Arial','B',11);
		$this->Cell(65);
		$this->SetTextColor(0,1,225);
		$this->Cell(30,10,$name);
		$this->Ln(15);
		$this->Cell(80);
		$this->SetTextColor(0,0,0);
		$this->Cell(30,10,"Between");
		$this->Ln(10);
		$this->Cell(61);
		$this->SetTextColor(0,1,225);
		$this->Cell(23,10,$f_date);
		$this->SetTextColor(0,0,0);
		$this->Cell(14,10,"And");
		$this->SetTextColor(0,1,225);
		$this->Cell(20,10,$t_date);
		//$this->Cell(20,10,"Doctor Name: ");

		$this->Ln(20);
		}
		//Page footer
		function Footer()
		{
				
		}
		function reference_table($c_name,$c_address)
		{
			$this->Ln(10);
			$invoice_date=date("Y-m-d");
			$x=$this->GetX();
			$y=$this->GetY();
			$this->SetFont('Arial','B',7);
			$rand=rand(10000, 99999);
			$this->MultiCell(60, 4, "Invoice Number:\nINV$rand pW",1);
			$this->SetXY($x + 60, $y);
			$this->MultiCell(60, 4, "Invoice Date:\n$invoice_date",1);
			$this->SetXY($x + 120, $y);

			$this->MultiCell(60, 4, "Reference Number:\n49415",1);
			$this->MultiCell(180, 3, "Buyer:$c_name\n
	Address:\n$c_address\n",1);

	$this->Ln(10);
		}
		//Simple table
		function BasicTable($header,$f_date,$t_date,$dateArray,$rowArray1,$doc_id,$login)
		{
			$date=date('d/m/Y');
			$this->SetFillColor(242,39,201);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');
			$this->Cell(40);
			$w=array(20,40,40,35);
			//Header
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
			$this->Ln();
			//Data
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('');

			//$this->Cell(20,6,"",1);
			$count=1;
			for($k=1;$k<=sizeof($rowArray1);$k++)
			{
				$this->Cell(40);
				$this->Cell(20,6,$count,1);
				$this->Cell(40,6,date("F, Y", strtotime($dateArray[$k])),1);
				$this->Cell(40,6,$rowArray1[$k],1,'C',true);
					
				//$this->Cell(35,6,ucwords($eachResult["user_name"]),1);
				$this->Ln();
				$count++;
			}
				

			$this->SetFillColor(131,177,238);
			$this->Ln(10);
			$this->SetFont('Arial','B',9);
			$this->Cell(80);
			//$this->Cell(80,10,"SUMMARY");$this->Ln(10);$this->SetFont('Arial','',9);
			$this->Ln(6);
			$this->Cell(70,6,"Report Created On : ".$date);$this->Ln(6);
			$this->Cell(70,6,"Report Created By : ".$login);$this->Ln(6);
			$this->SetFillColor(242,39,201);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');

			$w=array(20,45);
			//Header


			$this->Ln(10);
		}
		function b_details()
		{
			$this->Ln(2);
			//	$this->Cell(70,6,"Income Tax Permanent Account Number (PAN): $pan_number");$this->Ln(6);
			//$this->Cell(70,6,"Service Tax Code (STC) Number: ");$this->Ln(20);
			$this->MultiCell(70, 4,"We thank you and appreciate your business.\nFor  pinkWhale Healthcare Services Pvt. Ltd.");$this->Ln(15);
			$this->MultiCell(70, 4,"Anita Shet\n
Authorized Signatory
			");
		}

}





$pdf=new PDF();
$header=array('Sl.No:','Payment Month','Number Of Topups');
//Data loading
//*** Load MySQL Data ***//



$pdf->AddPage();
$pdf->Ln(0);

$resultData = array();
		///Added one condition AND package_details!='Admin Added packages' for filtering admin packages on 14-03-2014 after discussion with Anita
$clinic_d=mysql_query("SELECT b.doc_name as name,MONTHNAME(a.payment_date) as dt,count(*) as cn FROM card_topup_payment_report a JOIN pw_doctors b ON b.doc_id=a.doc_Id  WHERE a.doc_Id='$doc_id' AND date(a.payment_date) BETWEEN '$f_date' AND '$t_date' AND package_details!='Admin Added packages' GROUP BY MONTH(a.payment_date)");
for ($i=0;$i<mysql_num_rows($clinic_d);$i++)
{
	$result = mysql_fetch_array($clinic_d);

	$result['name']=$result['name'];
	$result['dt']=$result['dt'];
	$result['cn']=$result['cn'];
	array_push($resultData,$result);
}
$pdf->doc_details($f_date,$t_date,$name1);
$pdf->BasicTable($header,$f_date,$t_date,$dateArray,$rowArray1,$doc_id,$login);
//forme($f_date,$t_date,$name1,$dateArray,$rowArray1);
$pdf->b_details();
$pdf->Output("../images/Reports/".$name1."$p.$d.pdf","F");


?>




