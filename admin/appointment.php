<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/appointment.js"></script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">

<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" >
    <table width='700' border='0' cellpadding='0' cellspacing='1' >
        <tr>
            <td align="right" width="600">
                <span style="font-size: 12px;font-weight: bold;">Sorted By :</span>
            </td>
            <td>
                <select name="sort_appointment" id="sort_appointment" onchange="window.location=this.value;">
                    <option value="#">Clinic</option>
                    <option value="#hp">Doctor</option>
                </select>
            </td>
        </tr>
    </table>
    <table width='750' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
           <tr><th colspan="5">Appointment Details</th></tr>
           <tr>
               <td width="10%" align="left" bgcolor="#F5F5F5" ><b>Sl.No.</b></td>
               <td width="20%" align="left" bgcolor="#F5F5F5"><b>Patient Name</b></td>
               <td width="20%" align="left" bgcolor="#F5F5F5"><b>Doctor Name</b></td>
               <td width="40%" align="left" bgcolor="#F5F5F5"><b>Clinic Address</b></td>
               <td width="40%" align="left" bgcolor="#F5F5F5" nowrap><b>Time</b></td>
               
           </tr>
           <tr>
               <td width="10%" align="left" bgcolor="#ffffff" >1</td>
               <td width="20%" align="left" bgcolor="#ffffff" >XYZ</td>
               <td width="20%" align="left" bgcolor="#ffffff" >Dr. ABC</td>
               <td width="40%" align="left" bgcolor="#ffffff" >Demo Clinic,Kormanagala,Bangalore</td>
               <td width="40%" align="center" bgcolor="#ffffff" >11.00AM</td>
               
           </tr>
           <tr>
               <td width="10%" align="left" bgcolor="#ffffff" >2</td>
               <td width="20%" align="left" bgcolor="#ffffff" >aaa</td>
               <td width="20%" align="left" bgcolor="#ffffff" >Dr. ABC</td>
               <td width="40%" align="left" bgcolor="#ffffff" >Demo Clinic,Kormanagala,Bangalore</td>
               <td width="40%" align="center" bgcolor="#ffffff" >12.00PM</td>
               
           </tr>
        
    </table>
  
</td></tr></table>

<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
 	enable_appointment_submenu();
</script>
</html>
<?php
ob_end_flush();
?>
