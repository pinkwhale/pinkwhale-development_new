<?php

include '../db_connect.php';
include 'summary.report.class.php';

$type = $_REQUEST['type'];
$year = $_REQUEST['year'];
$subval = $_REQUEST['subval'];
$output = "";

if($year=="2011"){
    $date = "2011-06-05";    
    $count = 7;
}else{
    $date = $year."-01-05";
    $count = 12;
}

$current_year = strftime ( '%b-%y', (time()));

if($type!="" && $year!=""){

    $obj = new SummaryReport;
    
    if($type=="spe_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where activate_online_consultation='online' and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
    }else if($type=="exp_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where ( doc_category='Specialist/Expert' || doc_category='Expert')";
    }else if($type=="query_online"){
        $doc_qry = "select doc_id,doc_name,doc_category,blocked,doc_specialities from pw_doctors where activate_online_query='query'";
    }else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
        $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
    }else if($type=="spe_tele"){
        $doc_qry = "select doc_id,doc_name,doc_specialities from pw_doctors p inner join tele_consultation_summary t on doctor_id=doc_id where type='$type' group by doc_id ";
    }
    
    $doc_result = mysql_query($doc_qry);
    $doc_num = mysql_num_rows($doc_result);
    if($doc_num>0){
        
        $type_name = $obj->getTypeName($type);
        
        $display =  $type_name." Data for Year - ".$year;
        
        $output .= "<center><strong>$display<strong></center>";
        
        $output .= "<table border='0' cellpadding='0' cellspacing='1' width='90%' bgcolor='#eeeeee' align='center' class='s90registerform'><tr>";
        $output .= "<th bgcolor='#F5F5F5' align='left'><b>Doctor Name</b></th>";
        
        for($i=0 ; $i<$count ; $i++){
            
            $dynamicyear = strftime("%b-%y",(strtotime($date." +$i months")));
            
            
            if($current_year==$dynamicyear){
                $output .= "<th bgcolor='#F5F5F5' align='right'><b>".$dynamicyear."</b></td>";
                break;
            }else if($current_year!=$dynamicyear){
                $output .= "<th bgcolor='#F5F5F5' align='right'><b>".$dynamicyear."</b></td>";
            }
        }
        $output .="<th bgcolor='#F5F5F5' align='right'><b>Total</b></th>";
        $output .= "</tr>";
        
        $total_bottom = array() ;
        
        while($doc_data = mysql_fetch_array($doc_result)){
            
            $doc_id = $doc_data['doc_id'];
            $doc_name = $doc_data['doc_name'];
            $doc_spe = $doc_data['doc_specialities'];

            $output .= "<tr>";
            $output .= "<td nowrap>$doc_name</td>";
            
            if($year=="2011"){
                $date = "2011-06-05";    
                $count = 7;
            }else{
                $date = $year."-01-05";
                $count = 12;
            }
            
            $total_count = 0;
            
            for($i=0 ; $i<$count ; $i++){
                
                $dynamicyear = strftime("%b-%y",(strtotime($date." +$i months")));
            
                if($current_year==$dynamicyear){
                    $dd = strftime("%m-%Y",(strtotime($date." +$i months")));
                    if($type=="spe_tele"){
                        $no = $obj->getmonthlyteleSummary($type, $dd, $doc_id, $subval);
                    }else{
                        if($type=="query_online"){
                            $no = $obj->getmonthlyquerySummary($subval, $dd, $doc_id);
                        }else{
                            $no = $obj->getmonthlyOnlineSummary($type, $dd, $doc_id);
                        }
                    }
                    $total_count +=$no;
                    $total_bottom[$i] +=$no;
                    $output .= "<td bgcolor='#F5F5F5' align='center'>".$no."</td>";
                    break;
                }else if($current_year!=$dynamicyear){
                    $dd = strftime("%m-%Y",(strtotime($date." +$i months")));
                    if($type=="spe_tele"){
                        $no = $obj->getmonthlyteleSummary($type, $dd, $doc_id, $subval);
                    }else{
                        if($type=="query_online"){
                            $no = $obj->getmonthlyquerySummary($subval, $dd, $doc_id);
                        }else{
                            $no = $obj->getmonthlyOnlineSummary($type, $dd, $doc_id);
                        }
                    }
                    
                    $total_count +=$no;
                    $total_bottom[$i] +=$no;
                    $output .= "<td bgcolor='#F5F5F5' align='center'> ".$no."</td>";
                }
            }
            
            $output .="<td bgcolor='#F5F5F5' align='center'><b>$total_count</b></td>";
            $output .= "</tr>";
            
        }
        $output .= "<tr>";
        $total_count = 0;
        $output .="<td bgcolor='#F5F5F5' align='left' nowrap><b>Total # of Consults</b></td>";
        for($i=0;$i<sizeof($total_bottom);$i++){
            $total_count += $total_bottom[$i];
            $output .="<td bgcolor='#F5F5F5' align='center'><b>$total_bottom[$i]</b></td>";
        }
        $output .="<td bgcolor='#F5F5F5' align='center'><b>$total_count</b></td>";
        $output .= "</tr>"; 
        
        $output .= "</table>";

	
        $output .="<center><a href=\"month-wise-excel.php?type=$type&year=$year&subval=$subval\">Excel Export<img src=\"../images/excelLogo.gif\" /></a></center>";
        
        
    }else{
        $output = "No Records Found";
    }

}else{
    $output = "No Records Found";
}

echo $output;

?>

