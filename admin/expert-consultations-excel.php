<?php
error_reporting(E_PARSE); 
require_once('class/excel.class.php');
include ("../db_connect.php");

$p_name= $_GET['p-name'];
$p_id= $_GET['p-id'];
$group= $_GET['group'];
$d_name= $_GET['d-name'];
$start_date= $_GET['start-date'];
$end_date= $_GET['end-date'];
$satus= $_GET['satus'];

// data array
$array = array();

// 1 string
$tr = array();
// 1 colum
$tr[] = 'Patient';
$tr[] = 'Card#';
// 2 colum
$tr[] = 'Group';
$tr[] = 'Doctor';
$tr[] = 'Started';
$tr[] = 'Last Consultation';
$tr[] = 'Status';
$array[] = $tr;
// 2 string
$tr = array();
// 1 colum
$tr[] = $p_name;
$tr[] = $p_id;
// 2 colum
$tr[] = $group;
$tr[] = $d_name;
$tr[] = $start_date;
$tr[] = $end_date;
$tr[] = $satus;
$array[] = $tr;	
	

// load file
$NY_excel_simple->LoadFile($array,'consultation-details.xls');

// OR file data
$DataFile = $NY_excel_simple->CreatFile($array);
?>