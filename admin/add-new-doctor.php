<?php
session_start();
include ("../includes/pw_db_connect.php");
if (!isset($_SESSION['username']) || $_SESSION['login'] != 'admin') {
	header("Location: ../index.php");
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/add-new-doctor-validation.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/add_helper.js"></script>
<style type="text/css">
.Manually {
	-moz-box-shadow: inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: inset 0px 1px 0px 0px #ffffff;
	box-shadow: inset 0px 1px 0px 0px #ffffff;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e10971
		), color-stop(1, #f40478) );
	background: -moz-linear-gradient(center top, #e10971 5%, #f40478 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed',
		endColorstr='#f40478' );
	background-color: #e10971;
	-webkit-border-top-left-radius: 6px;
	-moz-border-radius-topleft: 6px;
	border-top-left-radius: 6px;
	-webkit-border-top-right-radius: 6px;
	-moz-border-radius-topright: 6px;
	border-top-right-radius: 6px;
	-webkit-border-bottom-right-radius: 6px;
	-moz-border-radius-bottomright: 6px;
	border-bottom-right-radius: 6px;
	-webkit-border-bottom-left-radius: 6px;
	-moz-border-radius-bottomleft: 6px;
	border-bottom-left-radius: 6px;
	text-indent: 0;
	border: 1px solid #dcdcdc;
	display: inline-block;
	color: #FFFFFF;
	font-family: arial;
	font-size: 15px;
	font-weight: bold;
	font-style: normal;
	height: 40px;
	line-height: 40px;
	width: 100px;
	text-decoration: none;
	text-align: center;
}

.Manually:hover {
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f40478
		), color-stop(1, #e10971) );
	background: -moz-linear-gradient(center top, #f40478 5%, #e10971 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f40478',
		endColorstr='#e10971' );
	background-color: #f40478;
}

.Manually:active {
	position: relative;
	top: 1px;
}
</style>
<script type="text/javascript">
                     
                function chechclinicEmailid()
                {
                        
                        if(document.getElementById("doc_email_id").value!=""){
                            var paramString = 'email=' + document.getElementById("doc_email_id").value + '&docId=""';  

                            //alert(document.getElementById("doc_email_id").value);

                                $.ajax({  
                                        type: "POST",  
                                        url: "check_email.php",  
                                        data: paramString,  
                                        success: function(response) {  
                                                //alert(response);	
                                                if(response== "true")
                                                {
                                                        document.getElementById('emailError').innerHTML ="";
                                                }
                                                else
                                                {
                                                        document.getElementById('emailError').innerHTML ="Email address already exists";
                                                }	
                                        }

                                });  

                        }

                }
                
                </script>

<script type="text/javascript">
            function form_disp()
             {
                document.getElementById("add_doc_form").style.display="block";
                document.getElementById("add_doc_section").style.display="none";
                }       
        </script>
<style>
a:hover {
	color: #e70976;
	text-decoration: underline;
}
</style>
</head>
<body>
	<link href="../css/designstyles.css" media="screen, projection"
		rel="stylesheet" type="text/css">

	<?php include "admin_head.php"; ?>

	<!-- side Menu -->

	<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />

	<table width="1000" border="0" cellspacing="0" cellpadding="0"
		align="center">

		<tr>
			<td width="228" valign="top" style=""><?php include "admin_left_menu.php"; ?>
				<script>
					function b_act()
					{
						//alert(document.getElementById("uploaded_file").value);
						var filename=document.getElementById("uploaded_file").value;
						var extension=filename.substr(filename.lastIndexOf('.')+1).toLowerCase();
					    if(extension=='xlsx' || extension=='xls') {
					    	
						document.getElementById("upload").disabled=false; }
					
					    else {
					        alert('Only Excel file can be uploaded!');
					      
					    }
					}
					    
					</script>
			
			<td valign="top">
				<div id="tot_doc_page"
					style="border: 1px solid; margin-left: 15px; margin-top: 20px;">
					<div id="add_doc_section">

						<div style="height: 140px; margin-top: 40px; text-align: center;">
							<form enctype="multipart/form-data" id="upload_form"
								action="doc_view.php" method="post">
								<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
								<table cellspacing="0" cellpadding="0" border="0" width="750"
									align="center" class=''>
									<div style="font-family: Arial; font-size: 12px; color: #F00;">Choose
										a file to upload:</div>
									<input name="uploaded_file" id="uploaded_file" type="file"
										onchange="b_act()" />
									<input type="submit" id="upload" value="Upload"
										class="Manually" disabled />
								</table>
							</form>
							<a href="PHPExcel_1.7.9_doc/template.xlsx">Download Excel sheet</a>
						</div>
						<hr>
						<div style="width: 100%; text-align: center">
							<b>OR</b>
						</div>
						<div style="height: 150px; margin-top: 80px; text-align: center;">

							<a class="Manually" onclick="form_disp()">Manually</a>
						</div>

					</div>

					<form method="post" style="display: none" name="add_doc_form"
						id="add_doc_form" action="actions/add-doctor-action.php"
						enctype="multipart/form-data">

						<table cellspacing="0" cellpadding="0" border="0" width="750"
							align="center" class='s90registerform'>

							<tr>
								<th colspan="8">Add New Doctor</th>
							</tr>

							<tr>
								<td><img src="../images/blank.gif" width="1" height="6" alt=""
									border="0"></td>
							</tr>

							<tr>
								<td colspan="4">

									<table width="760" border="0" cellspacing="0" cellpadding="0">

										<tr>
											<td width="57" bgcolor="#F5F5F5" rowspan="2">

												<div class="postpropertytext">
													Service Type:&nbsp;<font color="#FF0000">*</font>
												</div>
											</td>

											<!--<td width="142" bgcolor="#F5F5F5"><div class="postpropertytext1">Physician:</div></td>

                                                                <td width="31" bgcolor="#F5F5F5">

                                                                    <input type="checkbox" id="doctor_phy" name="doctor_phy" onchange="js_select_type(doctor_phy)" value="Physician" /></td>-->

											<td width="138" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Specialist :</div></td>

											<td width="38" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_spe" name="doctor_spe"
												onchange="js_select_type(doctor_spe);changeRatio();"
												value="Specialist" /></td>

											<!--<td width="135" bgcolor="#F5F5F5"><div class="postpropertytext1">counsellor:</div></td>

                                                                <td width="33" bgcolor="#F5F5F5">

                                                                    <input type="checkbox" id="doctor_con" name="doctor_con" onchange="js_select_type(doctor_con)" value="Counsellor"/></td>-->

											<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Experts:</div></td>

											<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_exp" name="doctor_exp"
												onchange="js_select_type(doctor_exp);changeRatio();"
												value="Expert" /></td>
												
												<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Tele Query:</div></td>

											<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_tele_qry" name="doctor_tele_qry"												
												value="Tele_Query" /></td>
										</tr>

										<tr>

											<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Clinic Appointment Signed:</div></td>

											<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_app" name="doctor_app"
												onchange="js_select_type(doctor_app)" value="Appointment" />
											</td>
											
											<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Clinic Appointment not Signed:</div></td>

											<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_app_not_signed" name="doctor_app_not_signed"
												 value="1" />
											</td>

											<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Online Query:</div></td>

											<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_qry" name="doctor_qry"
												onchange="js_select_type(doctor_qry);changeRatio();"
												value="Query" /></td>

										</tr>

									</table>

								</td>
							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td align="center" height="8" colspan="8">

									<div id="typeErrDiv" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>
							</tr>

							<!--  END ERROR DIV -->
							<!-- Manual ratio management -->
							<tr>
								<td colspan="6">
									<div id="spe_ratio" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td width="308" bgcolor="#F5F5F5">Share Ratio between
													Specialist and PW :&nbsp;<font color="#FF0000">*</font>
												</td>

												<td width="119" bgcolor="#F5F5F5"><select id="sp_ratio"
													name="sp_ratio" onChange="otherRatio();">
														<?php $qry4="SELECT * from pw_doctor_default_share_ratio where doc_category='Specialist'";
														if(!$qry_rslt4=mysql_query($qry4)) die(mysql_error());

														while($qry_result4=mysql_fetch_array($qry_rslt4))

														{
															?>
														<option value="<?php echo $qry_result4['ratio'];?>">
															<?php echo $qry_result4['ratio'];?>
														</option>

														<?php }?>
														<option value="other">Other</option>

												</select>
												
												<td>
													<div id="otherdiv" style="display: none;">
														Type Doctor:PW Share ratio here-> <input type="text"
															name="other_ratio" id="other_ratio" style="width: 86px;"
															onkeypress="return isNumberKey1(event);">
													</div>
												</td>
												<script>
											   function otherRatio()
											   {
                                               var ratio=document.getElementById('sp_ratio').value;
                                               if(ratio=='other')
                                               {
                                            	   document.getElementById("otherdiv").style.display="block"  
                                                	   document.getElementById('sp_ratio').value=document.getElementById('other_ratio').value;
                                            	   alert(document.getElementById('sp_ratio').value);
      											   }
                                               else
                                            	   document.getElementById("otherdiv").style.display="none"
											   }
												</script>

												</td>
											</tr>

											<!--    ERROR DIV -->


											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<!--  END ERROR DIV -->

										</table>

									</div>
								</td>
							</tr>

							<tr>
								<td colspan="6">
									<div id="exp_ratio" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td width="308" bgcolor="#F5F5F5">Share Ratio between Expert
													and PW :&nbsp;<font color="#FF0000">*</font>
												</td>

												<td width="136" bgcolor="#F5F5F5"><select id="ex_ratio"
													name="ex_ratio">
														<?php $qry5="SELECT * from pw_doctor_default_share_ratio where doc_category='Expert'";
														if(!$qry_rslt5=mysql_query($qry5)) die(mysql_error());

														while($qry_result5=mysql_fetch_array($qry_rslt5))

														{
															?>
														<option value="<?php echo $qry_result5['ratio'];?>">
															<?php echo $qry_result5['ratio'];?>
														</option>

														<?php }?>


												</select>
												</td>
											</tr>


											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>


										</table>

									</div>

								</td>


							</tr>
							<tr>
								<td colspan="6">
									<div id="qry_ratio" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td width="308" bgcolor="#F5F5F5">Share Ratio between Query
													doctor and PW :&nbsp;<font color="#FF0000">*</font>
												</td>

												<td width="136" bgcolor="#F5F5F5"><select id="qr_ratio"
													name="qr_ratio">
														<?php $qry6="SELECT * from pw_doctor_default_share_ratio where doc_category='Query'";
														if(!$qry_rslt6=mysql_query($qry6)) die(mysql_error());

														while($qry_result6=mysql_fetch_array($qry_rslt6))

														{
															?>
														<option value="<?php echo $qry_result6['ratio'];?>">
															<?php echo $qry_result6['ratio'];?>
														</option>

														<?php }?>


												</select>
												</td>
											</tr>


											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>


										</table>

									</div>

								</td>


								</td>

							</tr>


							<!-- End Manual ratio management-->

							<tr>
								<td colspan="7">

									<div id="Appoint_type" style="display: none;">

										<table width="772" border="0" cellspacing="0" cellpadding="0">

											<tr>

												<td width="100" align="left" bgcolor="#F5F5F5">No.of
													clinics&nbsp;&nbsp;&nbsp;<font color="#FF0000">*&nbsp;&nbsp;:&nbsp;&nbsp;</font><input
													type="text" name="num_clinic" id="num_clinic" value="1"
													size="4" maxlength="2" />
												</td>
											</tr>

											<!--    ERROR DIV -->



											<tr>
												<td width="100" align="center">

													<div id="num_of_clinicErrDiv" class="error"
														style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

												</td>

											</tr>

											<!--  END ERROR DIV -->

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

										</table>

									</div>

									<div id="spe_contact_type" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td width="168" bgcolor="#F5F5F5">Consultation Type:&nbsp;<font
													color="#FF0000">*</font>
												</td>

												<td width="136" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Tele-Consultation:</div></td>

												<td width="26" bgcolor="#F5F5F5"><input type="checkbox"
													id="doctor_tele_con" name="doctor_tele_con"
													onchange="enable_tele(doctor_tele_con);enable_teledetails(doctor_tele_con);abc(doctor_tele_con);"
													value="tele" /></td>

												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Online-Consultation:</div></td>

												<td width="72" bgcolor="#F5F5F5"><input type="checkbox"
													id="doctor_online_con" name="doctor_online_con"
													onchange="enable_online(doctor_online_con);enable_onlinedetails(doctor_online_con);abc(doctor_online_con);"
													value="online" />
												</td>
												
												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Diagnostic-Consultation:</div></td>

												<td width="72" bgcolor="#F5F5F5"><input type="checkbox"
													id="doctor_diag_con" name="doctor_diag_con"
													onchange="enable_diag(doctor_diag_con);abc(doctor_diag_con);"
													value="diagnostic" />
												</td>

												<td width="48" align="left" bgcolor="#F5F5F5"><div>None:</div>
												</td>

												<td width="172" align="left" bgcolor="#F5F5F5"><input
													type="checkbox" id="doctor_none_con" name="doctor_none_con"
													onchange="enable_none(doctor_none_con)" value="none" /></td>



											</tr>

											<!--    ERROR DIV -->



											<tr>
												<td colspan="2"></td>

												<td align="left" height="8" colspan="4">

													<div id="cons_typeErrDiv" class="error"
														style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
												</td>

											</tr>

											<!--  END ERROR DIV -->

										</table>

									</div>

								</td>
							</tr>

							<!-- div for showing plan details -->

							<tr>
								<td colspan="6">
									<div id="spe_cont_type" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td width="168" bgcolor="#F5F5F5">Plan :&nbsp;<font
													color="#FF0000">*</font>
												</td>

												<td width="136" bgcolor="#F5F5F5"></td>

												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Price of plan:</div></td>

												<td width="26" bgcolor="#F5F5F5"><input type="text"
													id="plan_price" name="plan_price" /></td>

												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Number Of Cards:</div></td>

												<td width="72" bgcolor="#F5F5F5"><input type="text"
													id="num_of_cards" name="num_of_cards" />
												</td>

												<script>
function abc(value)
{
		
<?php $qry1="SELECT * FROM pw_card_plan WHERE description='pW-Tele'";
	
	if (!$qry_rslt1 = mysql_query($qry1))
	
		die(mysql_error());
	
	while ($qry_result1 = mysql_fetch_array($qry_rslt1)) {
	
		$price = $qry_result1['plan_price'];
	
		$num = $qry_result1['no_cards'];
		
	}

	 $qry2="SELECT * FROM pw_card_plan WHERE description='pW-online'";
	
	if (!$qry_rslt2 = mysql_query($qry2))
	
		die(mysql_error());
	
	while ($qry_result2 = mysql_fetch_array($qry_rslt2)) {
	
		$price2 = $qry_result2['plan_price'];
	
		$num2 = $qry_result2['no_cards'];
	
	}
	$qry3="SELECT * FROM pw_card_plan WHERE description='pW-combi'";
	
	if (!$qry_rslt3 = mysql_query($qry3))
	
		die(mysql_error());
	
	while ($qry_result3 = mysql_fetch_array($qry_rslt3)) {
	
		$price3 = $qry_result3['plan_price'];
	
		$num3 = $qry_result3['no_cards'];
	
	}
	
	$qry4="SELECT * FROM pw_card_plan WHERE description='pW-Diagnostics'";
	
	if (!$qry_rslt4 = mysql_query($qry4))
	
		die(mysql_error());
	
	while ($qry_result4 = mysql_fetch_array($qry_rslt4)) {
	
		$price4 = $qry_result4['plan_price'];
	
		$num4 = $qry_result4['no_cards'];
	
	}
	
	
?>
if(document.getElementById("doctor_tele_con").checked==true && document.getElementById("doctor_online_con").checked==false)
{	
	var price='<?php echo $price;?>';
	var num='<?php echo $num;?>';		 
}
else if(document.getElementById("doctor_tele_con").checked==false && document.getElementById("doctor_online_con").checked==true)
{
	var price='<?php echo $price2;?>';
	var num='<?php echo $num2;?>';		 
}
else if(document.getElementById("doctor_tele_con").checked==true && document.getElementById("doctor_online_con").checked==true)
{
	var price='<?php echo $price3;?>';
	var num='<?php echo $num3;?>';		 
}
else if(document.getElementById("doctor_diag_con").checked==true && document.getElementById("doctor_online_con").checked==false && document.getElementById("doctor_tele_con").checked==false) 
{
	var price='<?php echo $price4;?>';
	var num='<?php echo $num4;?>';		 
}
else if(document.getElementById("doctor_diag_con").checked==true && document.getElementById("doctor_online_con").checked==false)
{
	var price='<?php echo $price4;?>';
	var num='<?php echo $num4;?>';		 
}
else if(document.getElementById("doctor_diag_con").checked==true && document.getElementById("doctor_online_con").checked==true)
{
	var price='<?php echo $price3;?>';
	var num='<?php echo $num3;?>';		 
}

document.getElementById("plan_price").value=price;
document.getElementById("num_of_cards").value=num;

}
					</script>


											</tr>

											<!--    ERROR DIV -->


											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<!--  END ERROR DIV -->

										</table>

									</div>

								</td>
							</tr>
							<!-- end div for showing plan details -->
							<tr>

								<td colspan="6">

									<div id="addon">

										<table width="772" border="0" cellspacing="0" cellpadding="0">

											<tr>

 												<td width="58" align="left" bgcolor="#F5F5F5">Do you Want AddOn Pack?</td>
												<td width="50" align="left" bgcolor="#F5F5F5">
												<select id="want_addon" name="want_addon" onchange="list_addons();">
														<option disabled>-select-</option>
														<option value="no" selected>No</option>
														<option value="yes">Yes</option>
														
												</select>
																								
												</td>

											</tr>
											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

										</table>

									</div>

								</td>

							</tr>

							
								<tr>

								<td colspan="6">

									<div id="select_addon" style="display:none">

										<table width="772" border="0" cellspacing="0" cellpadding="0">

											<tr>

 												<td width="68" align="left" bgcolor="#F5F5F5">Select AddOn Pack</td>
												<td width="50" align="left" bgcolor="#F5F5F5">
												<select id="select_Addonpack" name="select_Addonpack" onchange="add_details();">
														<option selected>-Select addOn Pack-</option>
														<option value="Silver">Silver</option>
														<option value="Gold">Gold</option>
														<option value="Platinum">Platinum</option>
												</select>
												<script>
                                                 function list_addons()
                                                 {
                                                     var val=document.getElementById("want_addon").value;
                                                   if(val=="yes")
                                                   {
                                                          document.getElementById("select_addon").style.display = "block";
                                                          document.getElementById("show_addondetails").style.display = "block";
                                                          }
                                                   else if(val=="no")
                                                   {
                                                	   document.getElementById("select_addon").style.display = "none";
                                                	   document.getElementById("addon_price").value=0;
                                                	   document.getElementById("num_of_addons").value=0;
                                                	   document.getElementById("show_addondetails").style.display = "none";
                                                   }
                                                 } 
												</script>
												
												
												</td>

											</tr>
											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

										</table>

									</div>

								</td>

							</tr>

							
							<tr>
								<td colspan="6">
									<div id="show_addondetails" style="display:none">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

											
												<td width="136" bgcolor="#F5F5F5"></td>

												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Price of AddOn Pack:</div></td>

												<td width="26" bgcolor="#F5F5F5"><input type="text"
													id="addon_price" name="addon_price" /></td>

												<td width="138" bgcolor="#F5F5F5"><div
														class="postpropertytext1">Number Of Cards:</div></td>

												<td width="72" bgcolor="#F5F5F5"><input type="text"
													id="num_of_addons" name="num_of_addons" />
												</td>


<script>
function add_details()
{
var addon=document.getElementById("select_Addonpack").value;
	$.ajax ({
        type: "POST",
	      url: "get_addon_name.php",
        data: { addon : addon }, 
     success: function( response ) {
         	//alert(response);
         	resultObj = eval (response);
        	document.getElementById("addon_price").value=resultObj[1];
        	document.getElementById("num_of_addons").value=resultObj[0];
         	
     }
	});
}
</script>
											</tr>

											<!--    ERROR DIV -->


											<tr>
												<td colspan="4"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<!--  END ERROR DIV -->

										</table>

									</div>

								</td>
							</tr>							
							
							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										Name:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td width="185" bgcolor="#F5F5F5"><input type="text"
									name="doc_name" /></td>

								<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">
										Mobile Number:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td width="199" bgcolor="#F5F5F5"><input type="text"
									name="doc_mobile" onkeypress="return isNumberKey(event);" /></td>

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td colspan="2" align="center">

									<div id="nameErrDiv" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

								<td align="center" height="8" colspan="2">

									<div id="mobileErrDiv" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

							</tr>

							<!--  END ERROR DIV -->

							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										Gender:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td bgcolor="#F5F5F5"><select name="doc_gender" id="doc_gender"
									class="registetextbox">

										<option value="">-Select-</option>

										<option value="Male">Male</option>

										<option value="Female">Female</option>

								</select>
								</td>

								<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">
										Email Id:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_email_id"
									id="doc_email_id" onchange="chechclinicEmailid();" /></td>

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td colspan="2" align="center" height="8">

									<div id="genderError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

								<td align="center" height="8" colspan="2">

									<div id="emailError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

							</tr>

							<!--  END ERROR DIV -->

							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>
								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">Department:&nbsp;</div>
								</td>

								<td bgcolor="#F5F5F5"><select name="doc_spclitis"
									id="doc_spclitis" class="registetextbox"
									onchange="get_patient_details()">

										<option value="" selected="selected" disabled>- Select
											Department -</option>

										<?php

										$qry = "SELECT `department_id`,`added_department` FROM add_department";

										if (!$qry_rslt = mysql_query($qry))

											die(mysql_error());

										while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                ?>
										<option value="<?php echo $qry_result['added_department']; ?>">
											<?php echo $qry_result['added_department']; ?>
										</option>
										<?php } ?>

								</select>
								</td>

								<!--td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">
										Qualification:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_qlifiction" />
								</td-->

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td align="center" height="8" colspan="2">

									<div id="speError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

								<!--td align="center" height="8" colspan="2">

									<div id="qliError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td-->

							</tr>

							<!--  END ERROR DIV -->

							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">Specialization:</div>
								</td>

								<td bgcolor="#F5F5F5"><select name="doc_spciliztion"
									id="doc_spciliztion" class="registetextbox">

										<option value="" selected="selected" disabled>- Select
											Specialities -</option>


								</select>
								</td>

								<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">More
										Expertise :&nbsp;</div></td>

								<td bgcolor="#F5F5F5"><input type="text"
									name="doc_more_expertise" id="doc_more_expertise"
									maxlength="200" /><br /> <font color="red">Note- : Separate
										Expertise names by semi-colon ' ; '</font></td>

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td align="center" height="8" colspan="2">

									<div id="specializationError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

								</td>

							</tr>

							<!--  END ERROR DIV -->

							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">PAN
										Number:</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_pan_no" /></td>

								<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Account
										Number:</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_acount_no" />
								</td>

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td align="center" height="8" colspan="2">

									<div id="panError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

								</td>

								<td align="center" height="8" colspan="2">

									<div id="accountnumberError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

							</tr>

							<!--  END ERROR DIV -->


							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										Address Line 1:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_address1" />
								</td>

								<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Line
										2:</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="doc_address2" />
								</td>

							</tr>

							<!--    ERROR DIV -->

							<tr>
								<td align="center" height="8" colspan="2">

									<div id="add1Error" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

								<td align="center" height="8" colspan="2">

									<div id="add2Error" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

							</tr>

							<!--  END ERROR DIV -->

							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>

								<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										<div class="postpropertytext">
											City <font color="#FF0000">*</font>:
										</div>
									</div></td>

								<td bgcolor="#F5F5F5"><input type="text" name="city" id="city"
									maxlength="50" /></td>

								<td bgcolor="#F5F5F5" width="186">

									<div class="postpropertytext">Face to face consultation Number:</div>
								</td>

								<td bgcolor="#F5F5F5"><input type="text" name="face_to_face"
									onkeypress="return isNumberKey(event);" /></td>

							</tr>

							<!--    ERROR DIV -->

							<tr>


								<td align="center" height="8" colspan="2">

									<div id="cityError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>

								<td align="center" height="8" colspan="2">

									<div id="connumError" class="error"
										style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
								</td>



							</tr>

							<!--  END ERROR DIV -->

							<tr>
								<td colspan="4">

									<div id="specilist_online_cnsltion" style="display: none;">

										<table width="760" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="6" alt="" border="0"></td>
											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="184">

													<div class="postpropertytext">
														Tele consultation Number:&nbsp;<font color="#FF0000">*</font>
													</div>

												</td>

												<td width="187" bgcolor="#F5F5F5"><input type="text"
													name="doc_tele_number"
													onkeypress="return isNumberKey(event);" /></td>

												<td bgcolor="#F5F5F5" width="197"><div
														class="postpropertytext">
														Timings:&nbsp;<font color="#FF0000">*</font>
													</div></td>

												<td width="192" bgcolor="#F5F5F5"><input type="text"
													name="doc_timings" /></td>

											</tr>

											<!--    ERROR DIV -->

											<tr>

												<td align="center" height="8" colspan="2">

													<div id="telconnumError" class="error"
														style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

												</td>

												<td align="center" height="8" colspan="2">

													<div id="timeError" class="error"
														style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

												</td>

											</tr>

											<!--  END ERROR DIV -->

										</table>

									</div>

								</td>
							</tr>

							<tr>
								<td colspan="3"><img src="../images/blank.gif" width="1"
									height="6" alt="" border="0"></td>
							</tr>

							<tr>
								<td colspan="4">

									<table width="760" border="0" cellspacing="0" cellpadding="0">

										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Visiting Clinics:</div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="visiting_clinics" class="registetextbox1"></textarea><br />
												<font color="red">Note- : Separate Clinic names by
													semi-colon ' ; '</font>
											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="visiting_clinicError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
											<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Visiting Clinic Address:</div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="visiting_clinic_address" class="registetextbox1"></textarea>
												
											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="visiting_clinic_addr_Error" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Visiting Hospitals:</div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="visiting_hospitals" class="registetextbox1"></textarea><br />
												<font color="red">Note- : Separate hospital names by
													semi-colon ' ; '</font>
											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="visiting_hospitalsError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
											<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Visiting Hospital Address:</div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="visiting_hospital_address" class="registetextbox1"></textarea>
											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="visiting_hospital_addr_Error" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Clinical Address:</div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="clinical_address" class="registetextbox1"></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="cliaddError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>
										
													<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
												<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Education:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="doc_qlifiction" class="registetextbox1"></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="qliError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>
										
													<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Memberships:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="memberships" class="registetextbox1"></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="membershipError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>
										
													<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Registrations:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="registrations" class="registetextbox1"></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="registrationError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
										<!--tr>
										<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										Name:&nbsp;<font color="#FF0000">*</font>
									</div></td>

								<td width="185" bgcolor="#F5F5F5"><input type="text"
									name="doc_name" /></td>
										</tr>
										
										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr-->

										<tr>

											<td bgcolor="#F5F5F5" valign="top"><div
													class="postpropertytext">
													About Doctor:&nbsp;<font color="#FF0000">*</font>
												</div></td>

											<!--td bgcolor="#F5F5F5">
												<!-- <textarea name="about_doc" class="registetextbox1" style="height:150px;" ></textarea>-->


												<!--textarea id="about_doc" name="about_doc" cols="60"
													rows="10"></textarea> <script type="text/javascript">
                                                    var editor = CKEDITOR.replace('about_doc');
                                                    CKFinder.setupCKEditor( editor, 'ckfinder/' );
                                                    ste.init();
                                                </script--> <!--  <textarea id="body" name="body" cols="60" rows="10">

        </textarea>

        <script type="text/javascript">

				var ste = new SimpleTextEditor("body", "ste");

				ste.init();

        </script>-->

											<!--/td-->
											<td width="575" bgcolor="#F5F5F5"><textarea
													id="about_doc" name="about_doc" cols="60"
													rows="10" class="registetextbox1"></textarea>

											</td>
										</tr>



										<!--    ERROR DIV -->

										<tr>

											<td align="center" height="8" colspan="2">

												<div id="aboutError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
										<tr>
										<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
										Consulting Fee:&nbsp;&#8377;
									</div></td>

								<td width="185" bgcolor="#F5F5F5"><input type="text"
									name="consulting_fee" /></td>
										</tr>
										
										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

										<tr>

											<td bgcolor="#F5F5F5"><div class="postpropertytext">Upload
													Photo :</div></td>

											<td bgcolor="#F5F5F5"><input type="file" id="photo"
												name="photo" onclick="photo_Extension_check();">
											</td>

										</tr>

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

									</table>

								</td>
							</tr>

							<tr>
								<td colspan="4">

									<div id="query_rate_1" style="display: none;">

										<!-- ############################### Query rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>

												<td colspan="6" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Query
														Packages :</div>

												</td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="139"><div
														class="postpropertytext">1 Query Session:</div></td>

												<?php

												include "../db_connect.php";

												$qry = "select 1_email_session_charge,3_email_session_charge,5_email_session_charge from packages where type_of_doctor='query' and doctor_id=0";

												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                        $result1 = $qry_result['1_email_session_charge'];

                                                                        $result2 = $qry_result['3_email_session_charge'];

                                                                        $result3 = $qry_result['5_email_session_charge'];

                                                                    }

                                                                    ?>



												<td width="128" bgcolor="#F5F5F5"><input type="text"
													name="features2" value="<?php echo $result1; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="119"><div
														class="postpropertytext">3 Query Session:</div></td>

												<td width="116" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result2; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="118"><div
														class="postpropertytext">5 Query Session:</div></td>

												<td width="140" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result3; ?>"
													disabled="disabled" class="txtbox1" /></td>


											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

										</table>

										<!-- ##################################### stop ######################3############# -->

									</div>

									<div id="query_rate_2" style="display: none;">

										<!-- ############################### Query rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>

												<td colspan="6" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Query
														Packages :</div>

												</td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="139"><div
														class="postpropertytext">1 Query Session:</div></td>

												<?php

												include "../db_connect.php";

												$qry = "select 1_email_session_charge,3_email_session_charge,5_email_session_charge from packages where type_of_doctor='query' and doctor_id=1";

												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                        $result1 = $qry_result['1_email_session_charge'];

                                                                        $result2 = $qry_result['3_email_session_charge'];

                                                                        $result3 = $qry_result['5_email_session_charge'];

                                                                    }

                                                                    ?>



												<td width="128" bgcolor="#F5F5F5"><input type="text"
													name="features2" value="<?php echo $result1; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="119"><div
														class="postpropertytext">3 Query Session:</div></td>

												<td width="116" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result2; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="118"><div
														class="postpropertytext">5 Query Session:</div></td>

												<td width="140" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result3; ?>"
													disabled="disabled" class="txtbox1" /></td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

										</table>

										<!-- ##################################### stop ######################3############# -->

									</div>

									<div id="physician_rate" style="display: none;">

										<!-- ############################### physician rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>

												<td colspan="6" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Physician
														Packages :</div>

												</td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="139"><div
														class="postpropertytext">1 Online Session:</div></td>

												<?php

												include "../db_connect.php";

												$qry = "SELECT 1_chat_session_charge,3_chat_session_charge,5_chat_session_charge FROM packages WHERE `type_of_doctor`='Physician' AND `doctor_id`='0'";

												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                        $result1 = $qry_result['1_chat_session_charge'];

                                                                        $result2 = $qry_result['3_chat_session_charge'];

                                                                        $result3 = $qry_result['5_chat_session_charge'];

                                                                    }

                                                                    ?>


												<td width="128" bgcolor="#F5F5F5"><input type="text"
													name="features2" value="<?php echo $result1; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="119"><div
														class="postpropertytext">3 Online Session:</div></td>

												<td width="116" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result2; ?>"
													disabled="disabled" class="txtbox1" /></td>

												<td bgcolor="#F5F5F5" width="118"><div
														class="postpropertytext">5 Online Session:</div></td>

												<td width="140" bgcolor="#F5F5F5"><input type="text"
													name="features7" value="<?php echo $result3; ?>"
													disabled="disabled" class="txtbox1" /></td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

										</table>

										<!-- ##################################### stop ######################3############# -->

									</div>
									
									
									<!-- specialist header div -->
									
									<div id="specilist_head" style="display: none;">
									
									<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>
												<td colspan="10" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Specialist
														Packages :</div>

												</td>

											</tr>
											
											</table>
											</div>
									
									<!-- specialist header div -->

									<div id="specilist_rate1" style="display: none;">

										<!-- ############################### Specialist phone rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>
												<td colspan="10"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<?php

												include "../db_connect.php";

												$qry = "SELECT 3_min_charge,15_min_charge,30_min_charge,60_min_charge,120_min_charge,180_min_charge,1_email_session_charge,3_email_session_charge,5_email_session_charge,10_email_session_charge,
												1_diagnostic_consultation,5_diagnostic_consultation,10_diagnostic_consultation FROM packages WHERE `type_of_doctor`='Specialist' AND `doctor_id`='0'";

												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

									$result_3_min = $qry_result['3_min_charge'];

									$result10 = $qry_result['15_min_charge'];

									$result11 = $qry_result['30_min_charge'];

									$result12 = $qry_result['60_min_charge'];

									$result13 = $qry_result['120_min_charge'];

									$result14 = $qry_result['180_min_charge'];

									$result15 = $qry_result['1_email_session_charge'];

									$result16 = $qry_result['3_email_session_charge'];

									$result17 = $qry_result['5_email_session_charge'];

									$result_spe_10email = $qry_result['10_email_session_charge'];
									
									$result20 = $qry_result['1_diagnostic_consultation'];
									
									$result21 = $qry_result['5_diagnostic_consultation'];
									
									$result22 = $qry_result['10_diagnostic_consultation'];

                                                                    }

                                                                    ?>

												<td bgcolor="#F5F5F5" width="78"><div
														class="postpropertytext">3 Mins :</div></td>

												<td bgcolor="#F5F5F5" width="77"><input type="text"
													name="Minutes_3" class="txtbox"
													value="<?php echo $result_3_min; ?> " /></td>

												<td bgcolor="#F5F5F5" width="78"><div
														class="postpropertytext">15 Mins :</div></td>

												<td bgcolor="#F5F5F5" width="77"><input type="text"
													name="Minutes_15" class="txtbox"
													value="<?php echo $result10; ?> " /></td>

												<td bgcolor="#F5F5F5" width="74"><div
														class="postpropertytext">30 Mins :</div></td>

												<td bgcolor="#F5F5F5" width="75"><input type="text"
													name="Minutes_30" class="txtbox"
													value="<?php echo $result11; ?> " /></td>
												<!--
                                                                    <td bgcolor="#F5F5F5" width="75"><div class="postpropertytext">60 Mins :</div></td>

                                                                    <td bgcolor="#F5F5F5" width="75">-->
												<input type="hidden" name="Minutes_60" class="txtbox"
													value="<?php echo $result12; ?> " />
												<!--</td>-->

												<!--<td bgcolor="#F5F5F5" width="78"><div class="postpropertytext">120 Mins:</div></td>

                                                                    <td width="75" bgcolor="#F5F5F5">-->
												<input type="hidden" name="Minutes_120" class="txtbox"
													value="<?php echo $result13; ?> " />
												<!--</td>

                                                                    <td bgcolor="#F5F5F5" width="77"><div class="postpropertytext">180 Mins:</div></td>

                                                                    <td width="76" bgcolor="#F5F5F5">-->
												<input type="hidden" name="Minutes_180" class="txtbox"
													value="<?php echo $result14; ?> " />
												<!--</td>-->

											</tr>

											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>

											</tr>

										</table>

										<!-- ##################################### stop #################################### -->

									</div>

									<div id="specilist_rate2" style="display: none;">

										<!-- ############################### Specialist email rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">
										
											<tr>

												<td bgcolor="#F5F5F5" width="126"><div
														class="postpropertytext">1 Online Consult:</div></td>

												<td width="132" bgcolor="#F5F5F5"><input type="text"
													name="Email_Session_1" class="txtbox1"
													value="<?php echo $result15; ?> " /></td>
												<!--
                                                                    <td bgcolor="#F5F5F5" width="121"><div class="postpropertytext">3 Email Session:</div></td>

                                                                    <td width="128" bgcolor="#F5F5F5"><input type="text" name="Email_Session_3" class="txtbox1" value="<?php echo $result16; ?> "/></td>
-->
												<td bgcolor="#F5F5F5" width="122"><div
														class="postpropertytext">5 Online Consults:</div></td>

												<td width="131" bgcolor="#F5F5F5"><input type="text"
													name="Email_Session_5" class="txtbox1"
													value="<?php echo $result17; ?> " /></td>

												<td bgcolor="#F5F5F5" width="122" nowrap><div
														class="postpropertytext">10 Online Consults:</div></td>

												<td width="131" bgcolor="#F5F5F5"><input type="text"
													name="Email_Session_10" class="txtbox1"
													value="<?php echo $result_spe_10email; ?> " /></td>

											</tr>
											<!--
                                                                <tr>

                                                                    <td bgcolor="#F5F5F5" width="122" nowrap><div class="postpropertytext">10 Email Session:</div></td>

                                                                    <td width="131" bgcolor="#F5F5F5"><input type="text" name="Email_Session_10" class="txtbox1" value="<?php echo $result_spe_10email; ?> "/></td>

                                                                    <td bgcolor="#F5F5F5" width="126"></td><td width="132" bgcolor="#F5F5F5"></td><td bgcolor="#F5F5F5" width="126"></td><td width="132" bgcolor="#F5F5F5"></td>

                                                                </tr>
							-->
											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>

											</tr>

										</table>

										<!-- ##################################### stop #################################### -->

									</div>
									
									<div id="specilist_rate3" style="display: none;">

										<!-- ############################### Specialist diagnostic consult rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">
										
										

											<tr>

												<td bgcolor="#F5F5F5" width="126"><div
														class="postpropertytext">1 Diagnostic Consult:</div></td>

												<td width="132" bgcolor="#F5F5F5"><input type="text"
													name="Diagnostic_Consult_1" class="txtbox1"
													value="<?php echo $result20; ?> " /></td>
												<!--
                                                                    <td bgcolor="#F5F5F5" width="121"><div class="postpropertytext">3 Email Session:</div></td>

                                                                    <td width="128" bgcolor="#F5F5F5"><input type="text" name="Email_Session_3" class="txtbox1" value="<?php echo $result16; ?> "/></td>
-->
												<td bgcolor="#F5F5F5" width="122"><div
														class="postpropertytext">5 Diagnostic Consult:</div></td>

												<td width="131" bgcolor="#F5F5F5"><input type="text"
													name="Diagnostic_Consult_5" class="txtbox1"
													value="<?php echo $result21; ?> " /></td>

												<td bgcolor="#F5F5F5" width="122" nowrap><div
														class="postpropertytext">10 Diagnostic Consult:</div></td>

												<td width="131" bgcolor="#F5F5F5"><input type="text"
													name="Diagnostic_Consult_10" class="txtbox1"
													value="<?php echo $result22; ?> " /></td>

											</tr>
											<!--
                                                                <tr>

                                                                    <td bgcolor="#F5F5F5" width="122" nowrap><div class="postpropertytext">10 Email Session:</div></td>

                                                                    <td width="131" bgcolor="#F5F5F5"><input type="text" name="Email_Session_10" class="txtbox1" value="<?php echo $result_spe_10email; ?> "/></td>

                                                                    <td bgcolor="#F5F5F5" width="126"></td><td width="132" bgcolor="#F5F5F5"></td><td bgcolor="#F5F5F5" width="126"></td><td width="132" bgcolor="#F5F5F5"></td>

                                                                </tr>
							-->
											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>

											</tr>

										</table>

										<!-- ##################################### stop #################################### -->

									</div>

									<div id="cousilling_rate" style="display: none;">

										<!-- ############################### counsellor telephonic rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>

												<td colspan="10" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">counsellor
														Packages :</div>

												</td>

											</tr>

											<tr>
												<td colspan="10"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<?php

												include "../db_connect.php";

												$qry = "SELECT `60_min_charge`,`120_min_charge`,`180_min_charge`,`1_email_session_charge`,`3_email_session_charge`,`5_email_session_charge`,`10_email_session_charge` FROM `packages` WHERE `type_of_doctor`='Counsellor' AND `doctor_id`='0'";



												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                        $result4 = $qry_result['60_min_charge'];

                                                                        $result5 = $qry_result['120_min_charge'];

                                                                        $result6 = $qry_result['180_min_charge'];

                                                                        $result7 = $qry_result['1_email_session_charge'];

                                                                        $result8 = $qry_result['3_email_session_charge'];

                                                                        $result9 = $qry_result['5_email_session_charge'];

                                                                        $result_con_10email = $qry_result['10_email_session_charge'];

                                                                    }

                                                                    ?>

												<td bgcolor="#F5F5F5" width="115"><div
														class="postpropertytext">60 Mins :</div></td>

												<td bgcolor="#F5F5F5" width="136"><input type="text"
													name="features1" value="<?php echo $result4; ?>"
													disabled="disabled" class="txtbox1" />
												</td>

												<!--<td bgcolor="#F5F5F5" width="115"><div class="postpropertytext">120 Mins:</div></td>

                                                                    <td width="135" bgcolor="#F5F5F5">-->

												<input type="hidden" name="features6"
													value="<?php echo $result5; ?>" disabled="disabled"
													class="txtbox1" />

												<!--</td>

                                                                    <td bgcolor="#F5F5F5" width="115"><div class="postpropertytext">180 Mins:</div></td>

                                                                    <td width="144" bgcolor="#F5F5F5">-->

												<input type="hidden" name="features6"
													value="<?php echo $result6; ?>" disabled="disabled"
													class="txtbox1" />

												<!--</td>-->

											</tr>

											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>

											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="115"><div
														class="postpropertytext">1 Online Consult:</div></td>

												<td bgcolor="#F5F5F5"><input type="text" name="features2"
													value="<?php echo $result7; ?>" disabled="disabled"
													class="txtbox1" />
												</td>

												<td bgcolor="#F5F5F5" width="115"><div
														class="postpropertytext">3 Online Consults:</div></td>

												<td bgcolor="#F5F5F5"><input type="text" name="features7"
													value="<?php echo $result8; ?>" disabled="disabled"
													class="txtbox1" />
												</td>

												<td bgcolor="#F5F5F5" width="115"><div
														class="postpropertytext">5 Online Consults:</div></td>

												<td bgcolor="#F5F5F5"><input type="text" name="features7"
													value="<?php echo $result9; ?>" disabled="disabled"
													class="txtbox1" />
												</td>

											</tr>

											<tr>

												<td bgcolor="#F5F5F5" width="115" nowrap><div
														class="postpropertytext">10 Online Consults:</div></td>

												<td bgcolor="#F5F5F5"><input type="text" name="features7"
													value="<?php echo $result_con_10email; ?>"
													disabled="disabled" class="txtbox1" />
												</td>

												<td bgcolor="#F5F5F5" width="126"></td>
												<td width="132" bgcolor="#F5F5F5"></td>
												<td bgcolor="#F5F5F5" width="126"></td>
												<td width="132" bgcolor="#F5F5F5"></td>

											</tr>

											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>

											</tr>

										</table>

										<!-- ##################################### stop #################################### -->

									</div>

									<div id="expert_rate" style="display: none;">

										<!-- ############################### Expert email Rates start ################################ -->

										<table cellspacing="0" cellpadding="0" border="0" width="760">

											<tr>

												<td colspan="6" bgcolor="#F5F5F5">

													<div
														style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Expert
														Packages :</div>

												</td>

											</tr>

											<tr>
												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>

											<tr>

												<?php

												include "../db_connect.php";

												$qry = "SELECT `exprt_email_1_session`,`exprt_email_3_session`,`exprt_email_5_session`,`exprt_email_10_session` FROM `packages` WHERE `type_of_doctor`='Expert' AND `doctor_id`='0'";



												if (!$qry_rslt = mysql_query($qry))

													die(mysql_error());

												while ($qry_result = mysql_fetch_array($qry_rslt)) {

                                                                        $result18 = $qry_result['exprt_email_1_session'];

                                                                        $result19 = $qry_result['exprt_email_3_session'];

                                                                        $result20 = $qry_result['exprt_email_5_session'];

                                                                        $result_con_10 = $qry_result['exprt_email_10_session'];

                                                                    }

                                                                    ?>

												<td bgcolor="#F5F5F5" width="125"><div
														class="postpropertytext">1 Online Consult:</div></td>

												<td width="134" bgcolor="#F5F5F5"><input type="hidden"
													name="exp_Email_Session_1" class="txtbox1"
													value="<?php echo $result18; ?> " /><input type="text"
													disabled="disabled" name="exp_Email_Session_1_tmp"
													class="txtbox1" value="<?php echo $result18; ?> " /></td>

												<td bgcolor="#F5F5F5" width="122"><div
														class="postpropertytext">3 Online Consults:</div></td>

												<td width="125" bgcolor="#F5F5F5"><input type="hidden"
													name="exp_Email_Session_2" class="txtbox1"
													value="<?php echo $result19; ?> " /><input type="text"
													disabled="disabled" name="exp_Email_Session_2_tmp"
													class="txtbox1" value="<?php echo $result19; ?> " /></td>
												<!--
                                                                    <td bgcolor="#F5F5F5" width="123"><div class="postpropertytext">5 Email Session:</div></td>

                                                                    <td width="131" bgcolor="#F5F5F5"><input type="hidden" name="exp_Email_Session_3" class="txtbox1" value="<?php echo $result20; ?> "/><input type="text" disabled="disabled" name="exp_Email_Session_3_tmp" class="txtbox1" value="<?php echo $result20; ?> "/></td>
-->
											</tr>
											<!--
                                                                    <td bgcolor="#F5F5F5" width="125"><div class="postpropertytext">10 Email Session:</div></td>

                                                                    <td width="134" bgcolor="#F5F5F5"><input type="hidden" name="exp_Email_Session_4" class="txtbox1" value="<?php echo $result_con_10; ?> "/><input type="text" disabled="disabled" name="exp_Email_Session_4_tmp" class="txtbox1" value="<?php echo $result_con_10; ?> "/></td>

                                                                    <td bgcolor="#F5F5F5" width="122"></td>

                                                                    <td width="125" bgcolor="#F5F5F5"></td>

                                                                    <td bgcolor="#F5F5F5" width="123"></td>

                                                                    <td width="131" bgcolor="#F5F5F5"></td>

                                                                <tr>
-->
											</tr>

											<tr>

												<td colspan="3"><img src="../images/blank.gif" width="1"
													height="3" alt="" border="0"></td>
											</tr>
										</table>
										<!-- ##################################### stop #################################### -->

									</div>
								</td>
							</tr>
							<tr>
								<td colspan="4" align="center" bgcolor="#F5F5F5"><input
									type="button" name="doc_save" value="Save" id="doc_save"
									onclick="add_doctor_validate(add_doc_form)" />
								</td>
							</tr>

							<tr>
								<td colspan="3"><img src="images/blank.gif" width="1" height="6"
									alt="" border="0"></td>
							</tr>
						</table>
						<script>
					function changeRatio()
					{
						if(document.getElementById("doctor_spe").checked==true)
						document.getElementById("spe_ratio").style.display="block";
						else
							document.getElementById("spe_ratio").style.display="none";
						if(document.getElementById("doctor_exp").checked==true)
							document.getElementById("exp_ratio").style.display="block";
							else
								document.getElementById("exp_ratio").style.display="none";
						if(document.getElementById("doctor_qry").checked==true)
							document.getElementById("qry_ratio").style.display="block";
							else
								document.getElementById("qry_ratio").style.display="none";
							
				    }
					function isNumberKey1(evt)
					{

						var charCode = (evt.which) ? evt.which : event.keyCode
						if (charCode > 31 && (charCode < 48 || charCode > 58))
						{
							return false; 
						}
						else
						{
							return true; 
						}
					}
				    
					</script>
					</form>
					<!--    <form id="add_doc_form">
 </form>-->
				</div>
			</td>	
	</table>
	<script type="text/javascript">
                                        enable_doc_submenu();
                                    </script>

	<?php include 'admin_footer.php'; ?>
</body>
</html>
