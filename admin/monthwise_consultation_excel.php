<?php 
error_reporting(E_PARSE);
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}

//for pagination and database connection

$start_year = "2011";
$current_year = strftime ( '%Y', (time()));
$current_month = strftime ( '%m', (time()));

$type = $_REQUEST['type']; 
$doc = $_REQUEST['doc'];
$month = $_REQUEST['month'];
$year = $_REQUEST['year']; 
if($type==""){
    $type="spe_online";
}

$filename = "$type-Weekly-report-".$month."-".$year.".xls";

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$filename.'"');


include 'summary.report.class.php';
$report = new SummaryReport;
 ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<!--Table1_start -->    
<table cellspacing="0" cellpadding="0" align="center" >
    <tr>        
        <td valign="top" >
            
            
                <?php       
                    
                    $extend = "";
                    
                    if($doc!="all" && $doc!=""){
                        $extend = " and doc_id='$doc'";
                    }
                    
                    if($month=="" || $year==""){
                        $month = strftime ('%m', (time()));
                        $year =  strftime ('%Y', (time()));
                    }
                    
                    $type_name = $report->getTypeName($type);
                    
                    $display =  $type_name." Data for $month/$year";
                    
                    echo  "<center><strong>$display<strong></center>";
                    
                    /*
                    $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                    $lastdayofdate = "$daysinmonth-$month-$year";
                    $firstdayofmoth =  "01-$month-$year";
                    
                    $prevyear = strftime ('%Y', strtotime($firstdayofmoth)-(60*60*24));
                    $prevmonth = strftime ('%m', strtotime($firstdayofmoth)-(60*60*24));
                    $nextyear = strftime ('%Y', strtotime($lastdayofdate)+(60*60*24));
                    $nextmonth = strftime ('%m', strtotime($lastdayofdate)+(60*60*24));
                    */
                    
                    if($type=="spe_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_online_consultation='online' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }else if($type=="exp_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where  blocked<>'Y' $extend and ( doc_category='Specialist/Expert' || doc_category='Expert')";
                    }else if($type=="query_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' $extend and activate_online_query='query'";
                    }else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
                        $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
                    }else if($type=="spe_tele"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_tele_Consultation='tele' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }
                    
                    $doc_result = mysql_query($doc_qry);
                    $doc_num = mysql_num_rows($doc_result);
                    if($doc_num>0){
                        
                        
                        if($type=="spe_tele"){
                            echo "<table border='0' cellpadding='0' cellspacing='2' align='center'><tr>";
                            echo "<th  align='left' rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th  align='left' rowspan='2'></th>";

                            $weekwisetag = $report->getMonthTeleetag($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthTeleDatatag($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }else if($type=="query_online"){
                            echo "<table border='0' cellpadding='0' cellspacing='2'   align='center'><tr>";
                            echo "<th  align='left' rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th  align='left' rowspan='2'></th>";

                            $weekwisetag = $report->getMonthonlinetagquery($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthonlineDatatagquery($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }else{
                            echo "<table border='0' cellpadding='0' cellspacing='2'   align='center'><tr>";
                            echo "<th  align='left' ><b>Doctor Name</b></th>";
                            echo "<th  align='left' ></th>";

                            $weekwisetag = $report->getMonthonlinetag($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthonlineDatatag($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }
                        echo "</table>";
                        
                    }else{
                        echo "No Records Found";
                    }
                ?>
                
                
            </div>
            
            
        </td>
    </tr>   
</table>

