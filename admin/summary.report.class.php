<?php


class SummaryReport{

       function getYearlyOnlineSummary($type,$year){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select sum(consults) count from online_consultation_summary where type='$type' and entered_date like '$year%'";
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['count'];
           if($count==""){
               $count = 0;
           }
           
           
           return $count;
           
       }

       function getYearlyquerySummary($subtype,$year){
           
           include "../db_connect.php";
           
           $count = 0;
           if($subtype=="phy"){
                $qry = "select sum(consults) count from online_consultation_summary where type='query_online' and entered_date like '$year%' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_specialities='physician')";
           }else if($subtype=="spe"){
                $qry = "select sum(consults) count from online_consultation_summary where type='query_online' and entered_date like '$year%' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_specialities<>'physician')";
           }
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['count'];
           if($count==""){
               $count = 0;
           }
           
           
           return $count;
           
       }
       
       function getYearlyTeleSummary($type,$year){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select count(1) count from tele_consultation_summary where type='$type' and doctor_id<>'' and entered_date like '$year%'";
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['count'];
           if($count==""){
               $count = 0;
           }
           
           
           return $count;
           
       }
       
       function getYearlyTelepulseSummary($type,$year){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select sum(secs) min from tele_consultation_summary where type='$type'  and doctor_id<>'' and entered_date like '$year%'";
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['min'];
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
       
       function getTypeName($type){
           
           $name = "";
           
           switch ($type){
               case "spe_online"        :   $name = "Specialist (Online)";
                                            break;
               case "exp_online"        :   $name = "Expert";
                                            break;
               case "query_online"      :   $name = "Query";
                                            break;
               case "Counsellor"        :   $name = "Counsellor";
                                            break;
               case "Dietician"         :   $name = "Dietician";
                                            break;
               case "Doctor"            :   $name = "Physician";
                                            break;
               case "spe_tele"          :   $name = "Specialist (Tele)";
                                            break;
               default                  :   $name = "Consultation";
                                            break;                               
           }           
           return $name;
       }
       
       function getmonthlyOnlineSummary($type,$monthyear,$doc_id){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select sum(consults) count from online_consultation_summary where type='$type' and doctor_id='$doc_id' and  date_format(entered_date,'%m-%Y')='$monthyear'";
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
      
       function getmonthlyquerySummary($type,$monthyear,$doc_id){
           
           include "../db_connect.php";
           
           $count = 0;
           if($type=="phy"){
                $qry = "select sum(consults) count from online_consultation_summary where type='query_online' and doctor_id='$doc_id' and date_format(entered_date,'%m-%Y')='$monthyear' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_id='$doc_id' and doc_specialities='physician')";
           }else{
                $qry = "select sum(consults) count from online_consultation_summary where type='query_online' and doctor_id='$doc_id' and date_format(entered_date,'%m-%Y')='$monthyear' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_id='$doc_id' and doc_specialities<>'physician')";
           }
    
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
          //echo $qry."----->".$count."<br />"; 
           return $count;
           
       }
 

       function getmonthOnlineSummary($type,$monthyear){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select sum(consults) count from online_consultation_summary where type='$type' and date_format(entered_date,'%m-%Y')='$monthyear'";
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
       function getmonthlyteleSummary($type,$monthyear,$doc_id,$subval){
           
           include "../db_connect.php";
           
           $count = 0;
           if($subval=="count"){
                $qry = "select count(1) count from tele_consultation_summary where type='$type'  and doctor_id='$doc_id' and date_format(entered_date,'%m-%Y')='$monthyear'";
           }else{
               $qry = "select sum(secs) count from tele_consultation_summary where type='$type'  and doctor_id='$doc_id' and date_format(entered_date,'%m-%Y')='$monthyear'";
           }
           //echo $qry."<br />";
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
       
       function getmonthteleSummary($type,$monthyear){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select ceil(sum(secs)/60) count from tele_consultation_summary where doctor_id<>'' and type='$type' and date_format(entered_date,'%m-%Y')='$monthyear'";
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
       
       function getdatewiseOnlineSummary($type,$date,$doc_id){
           
           include "../db_connect.php";
           
           $count = 0;
           
           $qry = "select sum(consults) count from online_consultation_summary where type='$type' and doctor_id='$doc_id' and  entered_date like '$date%'";
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }
       
       
       function getMonthonlinetag($month,$year){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            

            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
            $i=1;

            while($i<=$noofweeks){

                //$output .="<th>".$dayone." - ".$daylast."</th>";
                $output .="<th nowrap>&nbsp;&nbsp;&nbsp;&nbsp;Week $i<br />$dayone - $daylast</th>";

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                $i++;
            }

	     $output .="<th bgcolor='#F5F5F5' align='left'><b>Total</b></th>";
            
            return $output;
          
       }
       
       function getMonthonlineDatatag($month,$year,$doc_id,$type){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
            $i=1;
            
            $count_rgt_total = 0;

            while($i<=$noofweeks){
                
                $count = $this->getOnlinData($fromdate, $todate, $doc_id, $type);
                
                $count_rgt_total +=$count;

                $output .="<td align='center'>$count</td>";

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $fromdate =  strftime ( '%Y-%m-%d', strtotime($date)+(60*60*24));
                $todate =  strftime ( '%Y-%m-%d', strtotime($date)+((60*60*24*$remainingdays)));
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                
                $i++;
            }
            
            $output .="<td align='center'>$count_rgt_total</td>";
            
            return $output;
          
       }

     
       function getMonthTeleetag($month,$year){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            

            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
            $i=1;

            while($i<=$noofweeks){

                //$output .="<th>".$dayone." - ".$daylast."</th>";
                $output .="<th nowrap colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;Week $i<br />$dayone - $daylast</th>";                

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                $i++;
            }
            
            $i=1;
            
            $output .="<th bgcolor='#F5F5F5' align='center' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Total</b></th>";
            $output .="</tr><tr>";

            while($i<=$noofweeks){

                //$output .="<th>".$dayone." - ".$daylast."</th>";
                $output .="<th >Calls</th><th >Secs</th>";                

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                $i++;
            }
            
            $output .="<th bgcolor='#F5F5F5' align='left' ><b>Calls</b></th><th bgcolor='#F5F5F5' align='left' ><b>Secs</b></th>";
            
            
            return $output;
          
       }


	function getMonthTeleDatatag($month,$year,$doc_id,$type){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
            $fromdate = strftime ( '%Y-%m-%d', strtotime($dayoneofmoth) );
            $todate = strftime ( '%Y-%m-%d', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $i=1;
            
            $count_rgt_total = 0;
            $pulse_rgt_total = 0;

            while($i<=$noofweeks){
                
                $count = $this->getTeleData($fromdate, $todate, $doc_id, $type,'count');
                
                $count_rgt_total +=$count;
                
                $pulse = $this->getTeleData($fromdate, $todate, $doc_id, $type,'pulse');
                
                $pulse_rgt_total +=$pulse;
                
                $output .="<td align='center'>$count</td><td align='center'>$pulse</td>";

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $fromdate =  strftime ( '%Y-%m-%d', strtotime($date)+(60*60*24));
                $todate =  strftime ( '%Y-%m-%d', strtotime($date)+((60*60*24*$remainingdays)));
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                
                $i++;
            }
            
            
            $output .="<td align='center' >$count_rgt_total</td><td align='center' >$pulse_rgt_total</td>";
            
            return $output;
          
       }


       function getMonthonlinetagquery($month,$year){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            

            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
           
            
            $i=1;

            while($i<=$noofweeks){

                //$output .="<th>".$dayone." - ".$daylast."</th>";
                $output .="<th nowrap colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;Week $i<br />$dayone - $daylast</th>";                

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                $i++;
            }
            
            $i=1;
            
            $output .="<th bgcolor='#F5F5F5' align='center' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total</b></th>";
            $output .="</tr><tr>";

            while($i<=$noofweeks){

                //$output .="<th>".$dayone." - ".$daylast."</th>";
                $output .="<th >Physician</th><th >Specialist</th>";                

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                $i++;
            }
            
            $output .="<th >Physician</th><th >Specialist</th>";
            
            
            return $output;
          
       }



	function getMonthonlineDatatagquery($month,$year,$doc_id,$type){

            $output = "";
           
            $currentmonth = strftime ('%m', (time()));
            $currentyear =  strftime ('%Y', (time()));
            if($currentmonth==$month && $currentyear==$year){
                $daysinmonth = strftime ('%d', (time()));
                $lastdayofdate = strftime ('%d-%m-%Y', (time()));
                $dayoneofmoth =  "01-$month-$year";
            }else{
                $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                $lastdayofdate = "$daysinmonth-$month-$year";
                $dayoneofmoth =  "01-$month-$year";                
            }

            $firstday =  strftime ( '%w', strtotime($dayoneofmoth) );
            
            if($daysinmonth<7){
                $daysinfirstweek = $daysinmonth-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = 1;
            }else{
                $daysinfirstweek = 6-$firstday;
                $remainingdays = $daysinmonth-$daysinfirstweek;
                $noofweeks = round($remainingdays/7)+1;
            }
            
            $dayone = strftime ( '%d/%m', strtotime($dayoneofmoth) );
            $daylast = strftime ( '%d/%m', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $date = strftime ( '%d-%m-%Y', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            $daysinmonth = $daysinmonth-$daysinfirstweek;
            
            $fromdate = strftime ( '%Y-%m-%d', strtotime($dayoneofmoth) );
            $todate = strftime ( '%Y-%m-%d', strtotime($dayoneofmoth)+(60*60*24*$daysinfirstweek) );
            
            $i=1;
            
            $count_phy_total = 0;
            
            $count_spe_total = 0;
            
            $count_phy = 0;
            
            $count_spe = 0;
            
            $query_type = $this->findquery($doc_id);

            while($i<=$noofweeks){
                
                if($query_type=="phy"){
                
                    $count_phy = $this->getOnlinData($fromdate, $todate, $doc_id, $type);
                
                    $count_phy_total +=$count_phy;
                
                }
                
                if($query_type=="spe"){
                
                    $count_spe = $this->getOnlinData($fromdate, $todate, $doc_id, $type);
                
                    $count_spe_total +=$count_spe;
                
                }
                

                $output .="<td align='center'>$count_phy</td><td align='center'>$count_spe</td>";

                if($daysinmonth > 8){
                    $remainingdays = 7;
                }else{
                    $remainingdays = $daysinmonth-1;
                }
                
                $daysinmonth = $daysinmonth-$remainingdays;
                
                $fromdate =  strftime ( '%Y-%m-%d', strtotime($date)+(60*60*24));
                $todate =  strftime ( '%Y-%m-%d', strtotime($date)+((60*60*24*$remainingdays)));
                $dayone = strftime ( '%d/%m', strtotime($date)+(60*60*24));
                $daylast = strftime ( '%d/%m', strtotime($date)+(60*60*24*$remainingdays));
                $date = strftime ( '%d-%m-%Y', strtotime($date)+(60*60*24*$remainingdays) );
                
                $dayoneofmoth = $daylast;
                
                $i++;
            }
            
            $output .="<td align='center'>$count_phy_total</td><td align='center'>$count_spe_total</td>";
            
            return $output;
          
       }
	
       
       function getOnlinData($fromdate,$todate,$doc_id,$type){
           
           include '../db_connect.php';
           
           $qry = "select sum(consults) count from online_consultation_summary where type='$type' and doctor_id='$doc_id' and  date(entered_date)>='$fromdate' and date(entered_date)<='$todate'";
           //echo "<br />".$qry;         
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['count'];         
           if($count==""){
               $count = 0;
           }
           return   $count;
               
           
       }
    
	function getTeleData($fromdate,$todate,$doc_id,$type,$subval){
           
           include '../db_connect.php';
           
           if($subval=="pulse"){
                $qry = "select sum(secs) count from tele_consultation_summary where type='$type' and doctor_id='$doc_id' and  date(entered_date)>='$fromdate' and date(entered_date)<='$todate'";
           }else {
               $qry = "select count(1) count from tele_consultation_summary where type='$type' and doctor_id='$doc_id' and  date(entered_date)>='$fromdate' and date(entered_date)<='$todate'";
           }
                
           $result = mysql_query($qry);
           $data = mysql_fetch_array($result);
           $count = $data['count'];         
           if($count==""){
               $count = 0;
           }
           return   $count;
               
           
       }

	function findquery($docid){
           
           include '../db_connect.php';
           
           $qry = "select doc_id from pw_doctors where activate_online_query='query' and doc_specialities='physician' and doc_id=$docid";
           $result = mysql_query($qry);
           $num = mysql_num_rows($result);
           if($num>0){
               return 'phy';
           }else{
               return 'spe';
           }
           
       }

	function getdatewisequerySummary($type,$date,$doc_id){
           
           include "../db_connect.php";
           
           $count = 0;
           
           if($type=="phy"){
                $qry = "select sum(consults) count from online_consultation_summary where type='$type' and doctor_id='$doc_id' and  entered_date like '$date%' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_specialities='physician')";
           }else{
                $qry = "select sum(consults) count from online_consultation_summary where type='$type' and doctor_id='$doc_id' and  entered_date like '$date%' and doctor_id in(select doc_id from pw_doctors where activate_online_query='query' and doc_specialities<>'physician')";
           }
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }


	function getdatewiseTeleSummary($type,$date,$doc_id,$subval){
           
           include "../db_connect.php";
           
           $count = 0;
           if($subval=='count'){
               $qry = "select count(1) count from tele_consultation_summary where type='$type'  and doctor_id='$doc_id' and  entered_date like '$date%'";
           }else{
               $qry = "select sum(secs) count from tele_consultation_summary where type='$type'  and doctor_id='$doc_id' and  entered_date like '$date%'";
           }
           $result = mysql_query($qry);
           if($data = mysql_fetch_array($result)){
               $count = $data['count'];
           }               
           
           if($count==""){
               $count = 0;
           }
           
           return $count;
           
       }

}

?>

