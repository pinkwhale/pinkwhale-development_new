<?php 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
		{
			header("Location: ../index.php");
			exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/registration_validation.js"></script>
<script type="text/javascript" src="../js/enable-menu.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<style>	
a:hover{
	color:#e70976;
	text-decoration:underline;
}
</style>
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
</head>
<body>
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="169"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
<?php include "admin_left_menu.php"; ?>
<td width="850" >
<div id="phy_menu" style="display:block;">
		<form method="post" name="add_department" id="add_department" action="actions/add_deptment_action.php">
		<table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">
    		<tr><th colspan="2">Add Departments :</th></tr>
    		<tr><td><img src="../images/blank.gif" width="1" height="12" alt="" border="0"></td></tr>
    		<tr>
   			<td bgcolor="#F5F5F5" width="181"><div class="postpropertytext">New Department :&nbsp;<font color="#FF0000">*</font></div></td>
        	<td bgcolor="#F5F5F5" width="219"><input type="text" name="new_department" class="input" size="20"/></td>
    		</tr>
<!--    ERROR DIV -->
		<tr>
             <td align="center" height="8" colspan="2">
             	<div id="dprtmntErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
    	<tr>
    		<td bgcolor="#F5F5F5">&nbsp;</td>
    		<td bgcolor="#F5F5F5"><input type="button" value="Save" onclick="add_deptment_validate(add_department)" /></td>
    	</tr>
    	</table>
    	</form>
    	<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
    	<tr height="200"><td>&nbsp; </td></tr>
		</table>
</div>
</table>
<script type="text/javascript">
 	//enable_doc_submenu();
        enable_department_submenu();
</script>
 <?php include 'admin_footer.php'; ?>
</body></html>
