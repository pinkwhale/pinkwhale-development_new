<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/admin_profile.js"></script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top" id="mainBg" >
    <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else {
            if($_SESSION['error']!=""){
                echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";
            }
    ?>

    <?php
    include "../includes/pw_db_connect.php";
    
    $qry = "select * from login_user";
    $res = mysql_query($qry);
    if($dat = mysql_fetch_array($res)){
        
        $user_name = $dat['admin_name'];
        $user_email = $dat['admin_email'];
        $user_mob = $dat['admin_Mobile'];
    }
    
    ?>
    
    
    <form action="actions/add_admin_profile.php" method="post" name="add_admin_profile" id="add_admin_profile" enctype="multipart/form-data">
     <table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform">
        <tr><th colspan="2">Admin Profile</th></tr>
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">User Name&nbsp;&nbsp;:&nbsp;&nbsp;</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="text" name="uname" id="uname" size="20" value="<?= $user_name ?>" maxlength="30" disabled /></td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="unameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Email ID.<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="text" onchange="chechclinicEmailid()" value="<?= $user_email ?>"  name="email" id="email" size="40" maxlength="70" /><input name="dupl" id="dulp" type="hidden" value="0"/></td>        
        </tr>
         <!--    ERROR DIV -->
         <tr>
             <td> </td>
             <td  align="left" height="8">
                 <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
         <!--  END ERROR DIV --> 
         <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Phone No.<font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"  name="phone" id="phone" size="14" value="<?= $user_mob ?>" maxlength="12"  onkeypress="return isNumberKey(event);" onkeyup="checkphoneno(this)"/></td>        
        </tr>
        
         <!--    ERROR DIV -->
         <tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="phoneErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Save" tabindex="6" type="button"  onclick="add_profile(add_admin_profile)"  /></td>        
        </tr>
    </table>
    </form>
    <?php
        }
    ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>
</html>
<?php
ob_end_flush();
?>
