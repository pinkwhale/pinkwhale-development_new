<?php 
error_reporting(E_PARSE);
session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}

$filename = "Summary-report.xls"; 
    
header('Content-Type: application/vnd.msexcel');
header('Content-Disposition: attachment; filename="'.$filename.'"');


//for pagination and database connection
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
include 'summary.report.class.php';
$obj=new connect;

?>
  
<table  cellspacing="0" border="0" cellpadding="0" align="center" >
    <tr>
        
        <td  >
            <br />
            <?php
                $start_year = "2011";
                $current_year = strftime ( '%Y', (time()));
                $obj = new SummaryReport;
            
                echo "<center><strong>Summary Report<strong></center>";
                echo "<table border='0' cellpadding='0' cellspacing='1' width='60%' align='center'><tr>";
                echo "<th  align='left' colspan='2'><b>Type of Consultation</b></th>";
                for($i=$start_year ; $i<=$current_year ; $i++){
                    echo "<th  align='center'><b>$i</b></th>";
                }
                echo "<th  align='center'><b>Total</b></th>";
                echo "</tr>";

                $doc_qry = "select distinct type  from online_consultation_summary";
                $doc_result = mysql_query($doc_qry);
                $doc_num = mysql_num_rows($doc_result);
                if($doc_num>0){
                    
                    while($doc_data = mysql_fetch_array($doc_result)){
                        
                        $type = $doc_data['type'];
                        $type_name = $obj->getTypeName($type);
                        $total_right = 0;
                        if($type=="query_online"){
                            echo "<tr>";
                            echo "<td align='left' rowspan='2'>$type_name</td>";
                            echo "<td align='left'>Physician</td>";
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyquerySummary("phy", $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'>$count</td>";
                                }
                            }
                            echo "<td  align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                            echo "<td align='left'>Specialist</td>";
                            
                            $total_right = 0;
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyquerySummary("spe", $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'>$count</td>";
                                }
                            }
                            echo "<td  align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                        }else{
                            echo "<tr>";
                            echo "<td align='left' colspan='2'>$type_name</td>";
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyOnlineSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'>$count</td>";
                                }
                            }
                            echo "<td  align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                        }
                    }
                }
                
                $doc_qry = "select distinct type  from tele_consultation_summary";
                $doc_result = mysql_query($doc_qry);
                $doc_num = mysql_num_rows($doc_result);
                if($doc_num>0){
                    
                    while($doc_data = mysql_fetch_array($doc_result)){
                        
                        $type = $doc_data['type'];
                        $type_name = $obj->getTypeName($type);
                        $total_right = 0;
                        echo "<tr>";
                        echo "<td align='left' rowspan='2'>$type_name</td>";
                        
                            echo "<td align='left'>Calls</td>";
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyTeleSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'>$count</td>";
                                }
                            }
                            echo "<td  align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                            echo "<td align='left'>Secs</td>";
                            
                            $total_right = 0;
                            
                            for($i=$start_year ; $i<=$current_year ; $i++){
                                $count = $obj->getYearlyTelepulseSummary($type, $i);
                                $total_right +=$count;
                                if($count==0){
                                    echo "<td align='center'>$count</td>";
                                }else{
                                    echo "<td align='center'>$count</td>";
                                }
                            }
                            echo "<td  align='center'><b>$total_right</b></td>";
                            echo "</tr>";
                            
                        
                    }
                    
                }
                
                
                echo "</table>";
            ?>
            
            
            <br /><br />            
        </td>
    </tr>   
</table>
