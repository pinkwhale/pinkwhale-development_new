<?php 
ob_start();
error_reporting(E_PARSE);
session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/clinic_query.js"></script>
</head>

<body>
	<link href="../css/designstyles.css" media="screen, projection"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
	<?php include "admin_head.php"; ?>
	<!-- side Menu -->
	<table width="1000" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="228" valign="top"
				style="border-right: 1px solid #4d4d4d; border-left: 1px solid #4d4d4d;">

				<?php include "admin_left_menu.php"; ?>
			</td>
			<td width="772" id="mainBg" valign="top"><?php
			if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else {
            if($_SESSION['error']!=""){
                echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                $_SESSION['error'] = "";
            }
            ?> <script type="text/javascript">
function chechclinicEmailid ()
{

        if(document.getElementById("email").value!=""){
            var paramString = 'email=' + document.getElementById("email").value;  


		$.ajax({  
			type: "POST",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) {  
				//alert(response);	
				if(response==true)
				{
					document.getElementById('emailErrDiv').innerHTML ="";
				}
				else
				{
					document.getElementById('emailErrDiv').innerHTML ="Email address already exists";
				}	
			}
			  
		});  
                
        }

}
</script>
				<form action="actions/add_clinic_action.php" method="post"
					name="add_clinic" id="add_clinic" enctype="multipart/form-data">
					<table border="0" cellpadding="0" cellspacing="1" width="500"
						align="center" class="s90registerform">
						<tr>
							<th colspan="2">Add New Clinic</th>
						</tr>
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Name<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="cname" id="cname" size="25" maxlength="30" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="clinicnameErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Address<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><textarea rows="3"
									cols="35" name="address" id="address" maxlength="200"></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="addressErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Country<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><select
								name="country" id="country">
									<option value="">-----select Country-----</option>
									<option value="India" selected="selected">India</option>
							</select>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="countryRrrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">State<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><select
								name="state" id="state">
									<option value="" selected="selected">-----select State-----</option>
									<option value="Andhra Pradesh">Andhra Pradesh</option>
									<option value="Arunachal Pradesh">Arunachal Pradesh</option>
									<option value="Assam">Assam</option>
									<option value="Bihar">Bihar</option>
									<option value="Chandigarh">Chandigarh</option>
									<option value="Chhatisgarh">Chhatisgarh</option>
									<option value="Delhi">Delhi</option>
									<option value="Goa">Goa</option>
									<option value="Gujarat">Gujarat</option>
									<option value="Haryana">Haryana</option>
									<option value="Himachal Pradesh">Himachal Pradesh</option>
									<option value="Jammu and Kashmir">Jammu and Kashmir</option>
									<option value="Jharkhand">Jharkhand</option>
									<option value="Karnataka">Karnataka</option>
									<option value="Kerala">Kerala</option>
									<option value="Madhya Pradesh">Madhya Pradesh</option>
									<option value="Maharashtra">Maharashtra</option>
									<option value="Manipur">Manipur</option>
									<option value="Meghalaya">Meghalaya</option>
									<option value="Mizoram">Mizoram</option>
									<option value="Nagaland">Nagaland</option>
									<option value="Orissa">Orissa</option>
									<option value="Punjab">Punjab</option>
									<option value="Rajasthan">Rajasthan</option>
									<option value="Sikkim">Sikkim</option>
									<option value="Tamil Nadu">Tamil Nadu</option>
									<option value="Uttaranchal">Uttaranchal</option>
									<option value="Uttar Pradesh">Uttar Pradesh</option>
									<option value="West Bengal">West Bengal</option>
							</select>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="stateErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">City<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="city" id="city" maxlength="30" />
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="cityErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Pin/Zip<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="pin" id="pin" size="10" maxlength="8"
								onkeypress="return isNumberKey(event);"
								onkeyup="checkphoneno(this)" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="pinErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Phone No.<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="phone" id="phone" size="14" maxlength="12"
								onkeypress="return isNumberKey(event);"
								onkeyup="checkphoneno(this)" /></td>
						</tr>

						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="phoneErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Email ID.<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input
								autocomplete='off' type="text" onchange="chechclinicEmailid()"
								name="email" id="email" size="40" maxlength="70" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="emailErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Latitude :</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="late" id="late" size="10" maxlength="12"
								onkeypress="checkphoneno1(this)" onkeyup="checkname(this)" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="lateErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Longitude :</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="longa" id="longa" size="10" maxlength="12"
								onkeypress="checkphoneno1(this)" onkeyup="checkname(this)" /></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="longaErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">No. of Doctors<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="text"
								name="num_doc" id="num_doc" size="3" maxlength="3" value="0"
								onkeypress="return isNumberKey(event);" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="num_of_doc_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Logo:</td>
							<td width="40%" align="left" bgcolor="#F5F5F5"><input type="file"
								name="logo" id="logo" /></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="logo_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<!--        <tr>
            <td width="40%" align="center" bgcolor="#F5F5F5">Type:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="type" id="type">
                    <option value="0" >All</option> 
                    <option value="1" selected="selected">Top Up</option> 
                    <option value="2" >Appointment Booking</option>
                    <option value="3" >Diagnostic Consult</option>
                </select>
            </td>        
        </tr>   -->

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5">Clinic Type:</td>

							<td width="10%" align="left" bgcolor="#F5F5F5">Top Up</td>
							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="top_up" name="top_up"
								value="1" /></td>

						</tr>

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							<td align="left" bgcolor="#F5F5F5"><div>Appointment Booking</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="appointment_booking" name="appointment_booking"
								value="2" /></td>
						</tr>

						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							<td align="left" bgcolor="#F5F5F5"><div>Diagnostic Consult</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="diagnostic_consultation" name="diagnostic_consultation"
								value="3" /></td>
						</tr>
						<tr>
							<td width="40%" align="center" bgcolor="#F5F5F5"></td>
							<td align="left" bgcolor="#F5F5F5"><div>All</div>
							</td>

							<td align="left" bgcolor="#F5F5F5"><input type="checkbox"
								id="all" name="all" onchange="enable_all(all)" value="0" /></td>

						</tr>
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="clinic_type_ErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr> 


						
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""
								border="0"></td>
						</tr>
						<tr>
							<td align="center" colspan="5" bgcolor="#F5F5F5"><input
								onmouseover="this.style.cursor='pointer'" value="Save"
								tabindex="6" type="button"
								onclick="add_clinic_submit(add_clinic)" /></td>
						</tr>
					</table>
				</form> <?php
}
?></td>
		</tr>
	</table>
	<?php include 'admin_footer.php'; ?>
</body>

<script type="text/javascript">
 	enable_clinic_submenu();
</script>
</html>
<?php
ob_end_flush();
?>
