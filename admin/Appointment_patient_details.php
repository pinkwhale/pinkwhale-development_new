<?php 
ob_start();
	error_reporting(E_PARSE);
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
	{
		header("Location: ../index.php");
		exit();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
    <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/appointment.js"></script>
<script type="text/javascript">
    
    
        var display = false;
        
        
        function validate_details(form){
            
            display = true;
            
            document.getElementById("clinicErrDiv").innerHTML="";
            document.getElementById("doctorErrDiv").innerHTML="";
            
        
            
            if(document.getElementById("clinic").value==""){
                document.getElementById("clinicErrDiv").innerHTML="Please select Clinic";
                display = false;
            }
            
            if(document.getElementById("doctor").value==""){
                document.getElementById("doctorErrDiv").innerHTML="Please select Doctor";
                display = false;
            }
            
            if(display){
                document.getElementById('get_clinic_patient').submit();
            }else{
                return false;
            }
        
            
        }
    
    
    

        function get_doctors(){
            
            var clinic=encodeURI(document.getElementById('clinic').value); 
            
            $("#loading").fadeIn(900,0);
            $("#loading").html("<img src='../images/ajax-loader.gif' />");
            $('#doctor').load('get-doctor-details.php?clinic='+clinic); 
            
            
        }

</script>
</head>
    
<body >
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<?php include "admin_head.php"; ?>
<!-- side Menu -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" >
<tr><td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">

<?php include "admin_left_menu.php"; ?>
</td>
<td width="772" valign="top" id="mainBg">
    <form action="" method="GET" name="get_clinic_patient"  id="get_clinic_patient">
        <table border="0" width="700" cellpadding="0" cellspacing="1" align="center" class='s90registerform'>
                <tr>
                    <th colspan="2">Patient Details for Clinic</th>
                </tr>
                <tr>
                    <td align="right" bgcolor="#F5F5F5"><b>Clinic</b>&nbsp;&nbsp;:</td>
                    <td align="left" bgcolor="#F5F5F5">
                        <select name="clinic" id="clinic" onchange="get_doctors()">
                            <option value="" selected="selected">-----select Clinic-----</option>
                            <?php
                                //$qry = "select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id and status=0 where name<>'' group by c.clinic_id union select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id and status=0 where name<>'' group by c.clinic_id";
                                $qry = "select clinic_id,name from clinic_details where status=0 and name<>''";
                                $res = mysql_query($qry);
                                while($data = mysql_fetch_array($res)){
                                    echo "<option value='".$data['clinic_id']."|".$data['name']."' >".$data['name']."</option>";
                                }
                            ?>
                        </select>
                    </td>        
                </tr>
                <!--    ERROR DIV -->
                <tr>
                     <td> </td>
                     <td  align="left" height="8">
                            <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                     </td>
                </tr>
                <!--  END ERROR DIV --> 

                <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <td width="30%" align="right" bgcolor="#F5F5F5"><b>Doctor</b>:</td>
                    <td width="40%" align="left" bgcolor="#F5F5F5">
                        <select name="doctor" id="doctor"  >
                            <option value="" selected="selected" disabled>-----select doctor-----</option>

                        </select>

                    </td>        
                </tr>
                <!--    ERROR DIV -->
                <tr>
                     <td> </td>
                     <td  align="left" height="8">
                            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                     </td>
                </tr>
                <!--  END ERROR DIV -->

                <tr><td colspan="2" align="center"><input type="button" onclick="validate_details('get_clinic_patient')" name="button" id="button" value="Go"  /></td></tr>            
        </table>
    </form>
    
    
    
    
        <?php
        
        if($_REQUEST['doctor']!="" && $_REQUEST['clinic']!=""){
        
            
            $clinic = $_REQUEST['clinic'];
            $clinic_details = explode("|", $clinic);
            $clinic_id = $clinic_details[0];
            $clinic_name = $clinic_details[1];
            
            $doctor = $_REQUEST['doctor'];
            $doctor_details = explode("|", $doctor);
            $doctor_id = $doctor_details[0];
            $doctor_name = $doctor_details[1];
            
            
            include("includes/host_conf.php");
            include("includes/mysql.lib.php");
            include('includes/ps_pagination.php');
            $obj=new connect;
            ?>

		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="6" > Patient Details for Clinic : <?= $clinic_name ?></th></tr>
                <tr><th colspan="6" > Doctor : <?= $doctor_name ?></th></tr>
		<?php	
			$count =0;			
			$sql="SELECT `patient_name`,`mobile`,`email` FROM `Appointment_book_details` WHERE doc_id=$doctor_id and clinic_id=$clinic_id and new_patient=0 and patient_name<>''";
			
			$obj->query($sql);	
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "clinic=$clinic&doctor=$doctor");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) 
			{
				echo htmlForNoRecords("700", true);
			} 
			else if(mysql_num_rows($rs) > 0)
			{
				?>
				<tr>
					<td align='left'><strong>SI No:</strong> </td>
					<td align='left'><strong>Patient Name</strong></td>
					<td align='left'><strong>Phone No.</strong></td>
					<td align='left'><strong>Email</strong></td>
				 </tr>
				<?php	
				while($row = mysql_fetch_assoc($rs)) 
				{
					$count ++;
					$user_dob="";
					$gender="";
					$age="";
						$user_id=$row['user_id'];
						$user_name=$row['patient_name'];
						$mail_id=$row['email'];
                                                $phone=$row['mobile'];
                                               
					
					$gdate1=strtotime($user_dob);
					$final_date1=date("d M Y", $gdate1);
					$age=$final_date1;
					echo "<tr bgcolor='#ffffff'>";
						echo "<td>$count</td> ";
						echo "<td>$user_name </td>"; 
						echo "<td>$phone</td> ";
						echo "<td>$mail_id</td> ";
					echo "</tr>";          			
				}
                                }
				
				$ps1 = $pager->renderFirst();
				$ps2 = $pager->renderPrev();
				$ps3 = $pager->renderNav('<span>','</span>');
				$ps4 = $pager->renderNext();
				$ps5 = $pager->renderLast();
				?>
            	<tr><td colspan='5' align='center'>
				<?php
                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr>"; 
            
		 
		?>
			</table>
    
            <?php
            
            } 
            
            ?>
</td></tr></table>
<?php include 'admin_footer.php'; ?>
</body>
</html>
<script>
	enable_appointment_submenu();
</script>
<?php
ob_end_flush();
?>
