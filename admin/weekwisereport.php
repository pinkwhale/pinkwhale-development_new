<?php

include '../db_connect.php';
include 'summary.report.class.php';

$param  = $_REQUEST['param'];
$type = $_REQUEST['type']; 
$doc = $_REQUEST['doc'];
$datetmp = $_REQUEST['date'];   


$month = strftime ( '%m', strtotime($datetmp));
$year = strftime ( '%Y', strtotime($datetmp));


if($param==1){
    $datetmp = strftime ( '%Y-%m-%d', strtotime($datetmp));
}else{
    $datetmp = strftime ( '%Y-%m-%d', strtotime($datetmp)-(60*60*24*7));
}

$extend = "";

if($doc!="all" && $doc!=""){
    $extend = " and doc_id='$doc'";
}

$report = new SummaryReport;
$type_name = $report->getTypeName($type);

$display =  $type_name." Data ";

$output = "<center><strong>$display<strong></center>";

if($type=="spe_online"){
    $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_online_consultation='online' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
}else if($type=="exp_online"){
    $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where  blocked<>'Y' $extend and ( doc_category='Specialist/Expert' || doc_category='Expert')";
}else if($type=="query_online"){
    $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' $extend and activate_online_query='query'";
}else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
    $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
}else if($type=="spe_tele"){
    $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_tele_Consultation='tele' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
}

$doc_result = mysql_query($doc_qry);
$doc_num = mysql_num_rows($doc_result);
if($doc_num>0){
    if($type=="spe_tele"){

        $output .="<table border='0' cellpadding='1' cellspacing='2' width='90%' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
        $output .="<th bgcolor='#F5F5F5' align='left' rowspan='2'><b>Doctor Name</b></th>";
        $output .="<th bgcolor='#F5F5F5' align='left'  rowspan='2'>
        <div style='cursor:pointer' id=\"pre-id\" onclick=\"load_weekReport('0','$datetmp','$type','$doc')\">
        <img  src=\"../images/back_img.png\"><br>
        </div>
        </th>";

        for($i=0;$i<7;$i++){
            $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
            $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
            $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
            $output .="<th bgcolor='#F5F5F5' align='left' colspan=2><b>$day<br/>$date</b></th>";
        }

        $output .="<th bgcolor='#F5F5F5' align='left'  rowspan='2'>
        <div style='cursor:pointer' id=\"next-id\" onclick=\"load_weekReport('1','$transferdate','$type','$doc')\">
        <img  src=\"../images/next_img.png\"><br>
        </div>
        </th>";

        $output .="</tr><tr>";

        for($i=0;$i<7;$i++){
            $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
            $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
            $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
            $output .="<th  align='left' nowrap><b>Calls</th><th>Secs</b></th>";
        }



        $output .="</tr>";

        while($doc_data = mysql_fetch_array($doc_result)){
            $doc_id = $doc_data['doc_id'];
            $doc_name = $doc_data['doc_name'];

            $output .="<tr>";
            $output .="<td nowrap>$doc_name</td>";
            $output .="<td></td>";

            for($i=0;$i<7;$i++){                            
                $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                $count = $report->getdatewiseTeleSummary($type,$date,$doc_id,'count');
                $output .="<td  align='center'>$count</td>";
                $count = $report->getdatewiseTeleSummary($type,$date,$doc_id,'pulse');
                $output .="<td  align='center'>$count</td>";
            }

            $output .="<td></td>";
            $output .="</tr>";
        }

        $output .="</table>";                            
    }else if($type=="query_online"){

        $output .="<table border='0' cellpadding='1' cellspacing='2' width='90%' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
        $output .="<th bgcolor='#F5F5F5' align='left' rowspan='2'><b>Doctor Name</b></th>";
        $output .="<th bgcolor='#F5F5F5' align='left' rowspan='2'>
        <div style='cursor:pointer' id=\"pre-id\" onclick=\"load_weekReport('0','$datetmp','$type','$doc')\">
        <img  src=\"../images/back_img.png\"><br>
        </div>
        </th>";

        for($i=0;$i<7;$i++){
            $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
            $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
            $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
            $output .="<th bgcolor='#F5F5F5' align='left' colspan='2'><b>$day<br/>$date</b></th>";
        }

        $output .="<th bgcolor='#F5F5F5' align='left' rowspan='2'>
        <div style='cursor:pointer' id=\"next-id\" onclick=\"load_weekReport('1','$transferdate','$type','$doc')\">
        <img  src=\"../images/next_img.png\"><br>
        </div>
        </th>";

        $output .="</tr><tr>";

        for($i=0;$i<7;$i++){
            $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
            $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
            $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
            $output .="<th  align='left' nowrap><b>Physician</th><th nowrap>Specialist</b></th>";
        }

        $output .="</tr>";

        while($doc_data = mysql_fetch_array($doc_result)){
            $doc_id = $doc_data['doc_id'];
            $doc_name = $doc_data['doc_name'];

            $output .="<tr>";
            $output .="<td nowrap>$doc_name</td>";
            $output .="<td></td>";

            for($i=0;$i<7;$i++){                            
                $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                $count = $report->getdatewisequerySummary('phy',$date,$doc_id);
                $output .="<td  align='center'>$count</td>";
                $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                $count = $report->getdatewisequerySummary('spe',$date,$doc_id);
                $output .="<td  align='center'>$count</td>";
            }

            $output .="<td></td>";
            $output .="</tr>";
        }

        $output .="</table>";                            
    }else{

        $output .="<table border='0' cellpadding='1' cellspacing='2' width='90%' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
        $output .="<th bgcolor='#F5F5F5' align='left'><b>Doctor Name</b></th>";
        $output .="<th bgcolor='#F5F5F5' align='left'>
        <div style='cursor:pointer' id=\"pre-id\" onclick=\"load_weekReport('0','$datetmp','$type','$doc')\">
        <img  src=\"../images/back_img.png\"><br>
        </div>
        </th>";

        for($i=0;$i<7;$i++){
            $day = strftime ( '%a', strtotime($datetmp)+(60*60*24*$i) );
            $date = strftime ( '%d/%m/%y', strtotime($datetmp)+(60*60*24*$i) );
            $transferdate = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
            $output .="<th bgcolor='#F5F5F5' align='left'><b>$day<br/>$date</b></th>";
        }

        $output .="<th bgcolor='#F5F5F5' align='left'>
        <div style='cursor:pointer' id=\"next-id\" onclick=\"load_weekReport('1','$transferdate','$type','$doc')\">
        <img  src=\"../images/next_img.png\"><br>
        </div>
        </th>";

        $output .="</tr>";

        while($doc_data = mysql_fetch_array($doc_result)){
            $doc_id = $doc_data['doc_id'];
            $doc_name = $doc_data['doc_name'];

            $output .="<tr>";
            $output .="<td nowrap>$doc_name</td>";
            $output .="<td></td>";

            for($i=0;$i<7;$i++){                            
                $date = strftime ( '%Y-%m-%d', strtotime($datetmp)+(60*60*24*$i) );
                $count = $report->getdatewiseOnlineSummary($type,$date,$doc_id);
                $output .="<td  align='center'>$count</td>";
            }

            $output .="<td></td>";
            $output .="</tr>";
        }

        $output .="</table>";
    }

	$output .= "<br /><center><a href=\"month-report-excel.php?type=$type&year=$year&month=$month&doc=$doc\">Excel Export<img src=\"../images/excelLogo.gif\" /></a></center>";

}else{
    $output = "No Records Found";
}


echo $output;
?>

