<?php 

session_start();
include ("../db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
	header("Location: ../index.php");
	exit();
}
include('../db_connect.php');


$doc_id=$_GET['id'];


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<link href="../css/designstyles.css" media="screen, projection"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/registration_validation.js"></script>
<script type="text/javascript" src="js/edit-doctor-validation.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/add_helper.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script>
<script type="text/javascript">

        function chechclinicEmailid ()
        {
        	document.getElementById('emailError').innerHTML="";
        	var email=document.getElementById("doc_email_id").value;
                if(document.getElementById("doc_email_id").value!=""){
		     
                	var atpos=email.indexOf("@");
                	var dotpos=email.lastIndexOf(".");
                	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
                	  {
                	  alert("Not a valid e-mail address");
                	  document.getElementById('emailError').innerHTML ="Email address is not valid";
                	  return false;
                	  }
                	else
                    	return true;
                }
        }
function hide()
{
	document.getElementById("spe_ratio").style.display="none";
	$("#doc_save").hide();
	$("#gen_invoice").show();
	
}
</script>
</head>
<body>

	<?php include "admin_head.php"; ?>
	<!-- side Menu -->


	<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="228" valign="top" style="border-right: 1px solid #4d4d4d; border-left: 1px solid #4d4d4d;">

				<?php include "admin_left_menu.php"; ?>
			
			<td width="772">
			<?php
			$edit_id=$_GET['id'] ;
			$qry= "SELECT * FROM `pw_doctors` where `doc_id`='$edit_id' ";
			$qry_rslt = mysql_query($qry);
			if($result = mysql_fetch_array($qry_rslt))
			{
				$doc_spe = $result['doc_specialities'];
				$doc_dept = $result['doc_dept'];
				$doc_ratio= $result['pwDocRatio'];
				?>
				<form method="post" name="edit_doc_form" id="edit_doc_form" action="actions/edit-doctor-action.php"
					enctype="multipart/form-data">
					<input type="hidden" id="doctor_id" name="doctor_id" value="<?php echo $edit_id; ?>" /> 
					<input type="hidden" id="doctor_photo" name="doctor_photo" value="<?php echo $result['doc_photo']; ?>" />
					<table cellspacing="0" cellpadding="0" border="0" width="750" align="center">
						<tr>
							<td><img src="../images/blank.gif" width="1" height="10" alt=""	border="0"></td>
						</tr>
						<tr>
							<td rowspan="2" align="center"><img	src="../<?php echo $result['doc_photo']; ?>" width="150" height="150" alt="" border="1">
							</td>
							<td height="116" colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4"><input type="file" name="photo" id="photo" /></td>
						</tr>
						<tr>
							<td><img src="../images/blank.gif" width="1" height="10" alt="" border="0"></td>
						</tr>
						<tr>
							<td colspan="4">
								<table width="760" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="57" bgcolor="#F5F5F5" rowspan="2">
											<div class="postpropertytext">
												Type:&nbsp;<font color="#FF0000">*</font>
											</div>
										</td>

										<td width="112" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Specialist :</div></td>
										<td width="24" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_spe" name="doctor_spe" value="Specialist"
											onclick="edit_enable(doctor_spe);changeRatio();" />
										</td>


										<td width="110" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Experts:</div></td>
										<td width="52" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_exp" name="doctor_exp" value="Expert"
											onclick="edit_enable(doctor_exp);changeRatio();" />
										</td>
										
										<td width="110" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Tele Query:</div></td>
										<td width="52" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_tele_qry" name="doctor_tele_qry" value="Tele_Query"
											 />
										</td>
									</tr>
									<tr>
										<td width="135" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Clinic Appointment Signed:</div></td>
										<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_app" name="doctor_app"
											onclick="edit_enable(doctor_app)" value="Appointment" />
										</td>
										
										<td width="135" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Clinic Appointment not Signed:</div></td>
										<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_app_not_signed" name="doctor_app_not_signed"
											 value="1" />
										</td>
										
										
										<td width="135" bgcolor="#F5F5F5"><div
												class="postpropertytext1">Online Query:</div></td>
										<td width="51" bgcolor="#F5F5F5"><input type="checkbox"
											id="doctor_qry" name="doctor_qry"
											onclick="edit_enable(doctor_qry);changeRatio()" value="Query" />
	<script type="text/javascript">
	var type='<?php echo $result['doc_category']; ?>';
	var tele_no='<?php echo $result['activate_tele_Consultation']; ?>';
	var online_no='<?php echo $result['activate_online_consultation']; ?>';
	var diag_no='<?php echo $result['activate_diagnostic_consultation']; ?>';
    var appointment = '<?php echo $result['appoint_flag']; ?>';
    var query = '<?php echo $result['activate_online_query']; ?>';
	 var doc_tele_query = '<?php echo $result['doc_tele_query']; ?>';
	  var appoint_not_signed = '<?php echo $result['appoint_not_signed']; ?>';
        
        if(doc_tele_query=="Tele_Query"){
            document.getElementById("doctor_tele_qry").checked = true;
        }
		if(query=="Query"){
            document.getElementById("doctor_qry").checked = true;
        }
		if(appoint_not_signed=="1"){
            document.getElementById("doctor_app_not_signed").checked = true;
        }
        
	</script>
											
											</td>
									</tr>

								</table>
							</td>
						</tr>




						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="8">
								<div id="typeErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<!-- ##################################   Start Specialist second menu   ###############################-->
						<tr>
							<td colspan="7">
								<div id="Appoint_type" style="display: none;"
									class="postpropertytext">
									<table width="772" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100" align="center" bgcolor="#F5F5F5">No.of
												clinics&nbsp;&nbsp;&nbsp;<font color="#FF0000">*&nbsp;&nbsp;:&nbsp;&nbsp;</font><input
												type="text" name="num_clinic" id="num_clinic"
												value="<?=$result['number_of_clinic']; ?>" size="4"
												maxlength="2" />
											</td>
											
										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td width="100" align="center">
												<div id="num_of_clinicErrDiv" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
											</td>
										</tr>
										<!--  END ERROR DIV -->
										<tr>
											<td colspan="4"><img src="../images/blank.gif" width="1"
												height="6" alt="" border="0"></td>
										</tr>
									</table>
								</div>
								<div id="spe_contact_type" style="display: none;">
									<table width="760" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td colspan="4"><img src="../images/blank.gif" width="1"
												height="6" alt="" border="0"></td>
										</tr>
										<tr>
											<td width="150" bgcolor="#F5F5F5"><div
													class="postpropertytext1">
													Consultation Type:&nbsp;<font color="#FF0000">*</font>
												</div></td>
											<td width="146" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Tele-Consultation:</div></td>
											<td width="28" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_tele_con" name="doctor_tele_con"
												onchange="enable_tele(doctor_tele_con)" value="tele" />
											</td>
											<td width="168" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Online-Consultation:</div></td>
											<td width="24" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_online_con" name="doctor_online_con"
												onchange="enable_online(doctor_online_con)" value="online" />
											</td>
											
											<td width="168" bgcolor="#F5F5F5"><div
													class="postpropertytext1">Diagnostic-Consultation:</div></td>
											<td width="24" bgcolor="#F5F5F5"><input type="checkbox"
												id="doctor_diag_con" name="doctor_diag_con"
												onchange="enable_diag(doctor_diag_con)" value="diagnostic" />
											</td>
											
											
											<td width="135" bgcolor="#F5F5F5"><div
													class="postpropertytext1">None:</div></td>
											<td width="109" align="left" bgcolor="#F5F5F5"><input
												type="checkbox" id="doctor_none_con" name="doctor_none_con"
												onchange="enable_none(doctor_none_con)" value="none" /> <input
												type="hidden" name="doc_category_change"
												id="doc_category_change"
												value="<?php echo $result['doc_category'];?>" />
											</td>
										</tr>
										<!--    ERROR DIV -->
										<tr>
											<td align="center" height="8" colspan="5">
												<div id="cons_typeErrDiv" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
											</td>
										</tr>
										<!--  END ERROR DIV -->
									</table>
								</div> <!-- Manual ratio management -->

								<table>
									<tr>
										<td colspan="6">
											<div id="spe_ratio" style="display: none;">

												<table width="760" border="0" cellspacing="0"
													cellpadding="0">

													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>

													<tr>

														<td width="308" bgcolor="#F5F5F5"><div
																class="postpropertytext1"
																style="width: 276px; float: left;">
																Share Ratio between Specialist and PW :&nbsp;<font
																	color="#FF0000">*</font>
															</div>
														</td>

														<td width="106" bgcolor="#F5F5F5"><select id="sp_ratio"
															name="sp_ratio" onChange="otherRatio();">
                                                    							<option value="" <?php if ($doc_ratio=="") echo "selected='selected'"?>>No Share selected</option>
                                                         <?php if ($doc_ratio!=""){?>
                                                           <option value="<?php echo $doc_ratio;?>" selected='selected'><?php echo $doc_ratio;?></option><?php }?>
															<?php $qry4="SELECT * from pw_doctor_default_share_ratio where doc_category='Specialist' and ratio!='$doc_ratio'";
																if(!$qry_rslt4=mysql_query($qry4)) die(mysql_error());
																while($qry_result4=mysql_fetch_array($qry_rslt4))
																{
																	?>																
																<option value="<?php  echo $qry_result4['ratio'];?>">
																	<?php echo $qry_result4['ratio'];?>
																</option>

																<?php }?>
																<option value="other">Other</option>
														</select>
														
														<td>
															<div id="otherdiv" style="display: none;">
																<font size="2px"> Type Doctor:PW Share ratio here-</font>
																<input type="text" name="other_ratio" id="other_ratio"
																	style="width: 86px;" onkeypress="return isNumberKey1(event);">
															</div>
											<script>
											   function otherRatio()
											   {
                                               var ratio=document.getElementById('sp_ratio').value;
                                               if(ratio=='other')
                                               {
                                            	   document.getElementById("otherdiv").style.display="block";  
                                                	   document.getElementById('sp_ratio').value=document.getElementById('other_ratio').value;
    										   }
                                               else
                                            	   document.getElementById("otherdiv").style.display="none";
											   }
											</script>
														</td>
													</tr>

													<!--    ERROR DIV -->


													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>

													<!--  END ERROR DIV -->

												</table>

											</div>
										</td>
									</tr>

									<tr>
										<td colspan="6">
											<div id="exp_ratio" style="display: none;">

												<table width="760" border="0" cellspacing="0"
													cellpadding="0">

													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>

													<tr>

														<td width="308" bgcolor="#F5F5F5"><div
																class="postpropertytext1"
																style="width: 276px; float: left;">
																Share Ratio between Expert and PW :&nbsp;<font
																	color="#FF0000">*</font>
															</div>
														</td>

														<td width="136" bgcolor="#F5F5F5"><select id="ex_ratio"
															name="ex_ratio">
																<?php $qry5="SELECT * from pw_doctor_default_share_ratio where doc_category='Expert'";
																if(!$qry_rslt5=mysql_query($qry5)) die(mysql_error());

																while($qry_result5=mysql_fetch_array($qry_rslt5))

																{
																	?>

																<option value="<?php  echo $qry_result5['ratio'];?>"
																<?php if ($qry_result5['ratio']==$doc_ratio) echo "selected='selected'"?>>
																	<?php echo $qry_result5['ratio'];?>
																</option>

																<option value="<?php echo $qry_result5['ratio'];?>">
																	<?php echo $qry_result5['ratio'];?>
																</option>

																<?php }?>


														</select>
														</td>
													</tr>


													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>


												</table>

											</div>

										</td>




									</tr>



									<tr>
										<td colspan="6">
											<div id="qry_ratio" style="display: none;">

												<table width="760" border="0" cellspacing="0"
													cellpadding="0">

													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>

													<tr>

														<td width="308" bgcolor="#F5F5F5"><div
																class="postpropertytext1"
																style="width: 276px; float: left;">
																Share Ratio between Query doctor and PW :&nbsp;<font
																	color="#FF0000">*</font>
															</div>
														</td>

														<td width="136" bgcolor="#F5F5F5"><select id="qr_ratio"
															name="qr_ratio">
																<?php $qry6="SELECT * from pw_doctor_default_share_ratio where doc_category='Query'";
																if(!$qry_rslt6=mysql_query($qry6)) die(mysql_error());

																while($qry_result6=mysql_fetch_array($qry_rslt6))

																{
																	?>
																<option value="<?php echo $doc_ratio;?>" selected>
																	<?php echo $doc_ratio;?>
																</option>
																<option value="<?php echo $qry_result6['ratio'];?>">
																	<?php echo $qry_result6['ratio'];?>
																</option>

																<?php }?>


														</select>
														</td>
													</tr>


													<tr>
														<td colspan="4"><img src="../images/blank.gif" width="1"
															height="6" alt="" border="0"></td>
													</tr>


												</table>

											</div>

										</td>




									</tr>

								</table> <!-- End Manual ratio management--> <!-- ##################################   Stop Specialist second menu   ###############################-->
								<table width="760" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td bgcolor="#F5F5F5"><div class="postpropertytext">
												Name:&nbsp;<font color="#FF0000">*</font>
											</div></td>
										<td width="185" bgcolor="#F5F5F5"><input type="text"
											name="doc_name" value="<?php echo $result['doc_name'] ;?>" />
										</td>
										<td bgcolor="#F5F5F5" width="186"><div
												class="postpropertytext">
												Mobile Number:&nbsp;<font color="#FF0000">*</font>
											</div></td>
										<td width="199" bgcolor="#F5F5F5"><input type="text"
											name="doc_mobile"
											value="<?php echo  $result['doc_mobile_no'] ;?>"
											onkeypress="return isNumberKey(event);" />
										</td>
									</tr>
									<!--    ERROR DIV -->
									<tr>
										<td colspan="2" align="center">
											<div id="nameErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
										<td align="center" height="8" colspan="2">
											<div id="mobileErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>
									<!--  END ERROR DIV -->



									<tr>
										<td colspan="3"><img src="../images/blank.gif" width="1"
											height="6" alt="" border="0"></td>
									</tr>
									<tr>
										<td bgcolor="#F5F5F5" width="190"><div
												class="postpropertytext">Gender:</div></td>
										<td bgcolor="#F5F5F5"><select name="doc_gender"
											id="doc_gender" class="registetextbox">
												<option value="Male">Male</option>
												<option value="Female">Female</option>

										</select>
	<script type="text/javascript">
		gndr='<?php echo $result['doc_gender']; ?>';
		document.getElementById('doc_gender').value=gndr;
		document.getElementById('doc_gender').selected=true;
	</script>
										</td>
								
										<td bgcolor="#F5F5F5" width="186"><div
												class="postpropertytext">Email Id:</div></td>
										<td bgcolor="#F5F5F5"><input type="text" name="doc_email_id"
											id="doc_email_id" onchange="chechclinicEmailid();"
											value="<?php echo  $result['doc_email_id']; ?>" /></td>
									</tr>
									<!--    ERROR DIV -->
									<tr>
										<td colspan="2" align="center" height="8">
											<div id="genderError" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
										<td align="center" height="8" colspan="2">
											<div id="emailError" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>

									<!--  END ERROR DIV -->
									<tr>
										<td colspan="3"><img src="../images/blank.gif" width="1"
											height="6" alt="" border="0"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>


							<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">PAN
									Number:</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_pan_no"
								value="<?php echo  $result['pan_number']; ?>" /></td>
							<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Account
									Number:</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_acount_no"
								value="<?php echo  $result['account_number']; ?>" /></td>



						</tr>



						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="2">
								<div id="speError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
							<td align="center" height="8" colspan="2">
								<div id="qliError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->


						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<tr>



							<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">Department:</div>
							</td>
							<td bgcolor="#F5F5F5"><select name="doc_spclitis"
								id="doc_spclitis" class="registetextbox">

									<option value="" selected="selected" disabled>- Select
										Department -</option>

									<?php

									$qry = "SELECT `department_id`,`added_department` FROM add_department";

									if (!$qry_rslt = mysql_query($qry))

										die(mysql_error());

									while ($qry_result = mysql_fetch_array($qry_rslt)) {

            ?>
									<option value="<?php echo $qry_result['added_department']; ?>">
										<?php echo $qry_result['added_department']; ?>
									</option>

									<?php } ?>

							</select> <script type="text/javascript">
		spclst='<?php echo $doc_dept; ?>';
		document.getElementById('doc_spclitis').value=spclst;
		document.getElementById('doc_spclitis').selected=true;
	</script>
							</td>


							<!--td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Qualification:</div>
							</td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_qlifiction"
								value="<?php //echo  $result['doc_qualification']; ?>" /></td-->







						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="2">
								<div id="panError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
							<td align="center" height="8" colspan="2">
								<div id="accountnumberError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<tr>
							<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">Specialization:</div>
							</td>
							<td bgcolor="#F5F5F5"><input name="selected_spe"
								id="selected_spe" type="hidden" value="<?php echo $doc_spe; ?>" /> <select
								name="doc_spciliztion" id="doc_spciliztion"
								class="registetextbox">

									<option value="" disabled>- Select Specialities -</option>
									<?php

									$qry2 = "SELECT `speciality_id`,`added_speciality` FROM add_specialities";

									if (!$qry_rslt2 = mysql_query($qry2))

										die(mysql_error());

									while ($qry_result2 = mysql_fetch_array($qry_rslt2)) {

            ?>
									<option value="<?php echo $qry_result2['added_speciality']; ?>">
										<?php echo $qry_result2['added_speciality']; ?>
									</option>

									<?php } ?>

							</select> <script>
    var spe = "<?php echo $doc_spe; ?>";
    document.getElementById("doc_spciliztion").value=spe;
   document.getElementById("doc_spciliztion").selected=true;              
    </script>
							</td>


							<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Services :&nbsp;</div></td>
							<td bgcolor="#F5F5F5"><textarea 
								name="doc_more_expertise" class="registetextbox1"								
								id="doc_more_expertise"><?php echo  $result['more_expertise']; ?></textarea><br /> <font
								color="red">Note- : Separate Expertise names by semi-colon ' ; '</font>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="2">
								<div id="specializationError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<tr>
							<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">Address
									Line 1:</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_address1"
								value="<?php echo  $result['addressline_1']; ?>" /></td>
							<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Line
									2:</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_address2"
								value="<?php echo  $result['address_line2']; ?>" /></td>
						</tr>

						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="2">
								<div id="add1Error" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
							<td align="center" height="8" colspan="2">
								<div id="add2Error" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<tr>
							<td bgcolor="#F5F5F5" width="190"><div class="postpropertytext">
									<div class="postpropertytext">
										City <font color="#FF0000">*</font>:
									</div>
								</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="city" id="city"
								maxlength="50" value="<?php echo $result['city'] ?>" /></td>
							<td bgcolor="#F5F5F5" width="186"><div class="postpropertytext">Face
									to face consultation Number:</div></td>
							<td bgcolor="#F5F5F5"><input type="text" name="doc_tele_no"
								value="<?php echo  $result['face_to_face_cnsltion_phno']; ?>"
								onkeypress="return isNumberKey(event);" />
							</td>
						</tr>

						<!--    ERROR DIV -->
						<tr>
							<td align="center" height="8" colspan="2">
								<div id="cityError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
							<td align="center" height="8" colspan="2">
								<div id="connumError" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>

						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>

						<tr>
							<td colspan="4">
								<div id="spe_tele_type1" style="display: none;">
									<table width="760" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td colspan="4"><img src="../images/blank.gif" width="1"
												height="6" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5"><div class="postpropertytext">Tele
													consultation Number:</div></td>
											<td bgcolor="#F5F5F5"><input type="text" name="doc_online_no"
												value="<?php echo  $result['tele_consult_phno']; ?>"
												onkeypress="return isNumberKey(event);" />
											</td>
											<td bgcolor="#F5F5F5" width="186"><div
													class="postpropertytext">Timings:</div></td>
											<td bgcolor="#F5F5F5"><input type="text" name="doc_timings"										s"
												value="<?php echo  $result['doc_timing']; ?>" /></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="3"><img src="../images/blank.gif" width="1"
								height="6" alt="" border="0"></td>
						</tr>
						<tr>
							<td colspan="4">
								<table width="760" border="0" cellspacing="0" cellpadding="0">
								<tr>
											
											<td bgcolor="#F5F5F5" width="186"><div
													class="postpropertytext">Clinic Timings:</div></td>
											<td bgcolor="#F5F5F5"><input type="text" name="doc_clinic_timing"
												value="<?php echo  $result['doc_clinic_timing']; ?>" /></td>
										</tr>
										<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									<tr>
											
											<td bgcolor="#F5F5F5" width="186"><div
													class="postpropertytext">Hospital Timings:</div></td>
											<td bgcolor="#F5F5F5"><input type="text" name="doc_hospital_timing"
												value="<?php echo  $result['doc_hospital_timing']; ?>" /></td>
										</tr>
										<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									<tr>
										<td width="190" bgcolor="#F5F5F5"><div
												class="postpropertytext">Visiting Clinics:</div></td>
										<td width="570" bgcolor="#F5F5F5"><textarea
												name="visiting_clinics_name" class="registetextbox1">
												<?php echo  $result['visit_clinic_name']; ?>
											</textarea><br /> <font color="red" size="2">Note- : Separate
												Clinic names by semi-colon ' ; '</font>
										</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									
										<tr>
										<td width="190" bgcolor="#F5F5F5"><div
												class="postpropertytext">Visiting Clinic Address:</div></td>
										<td width="570" bgcolor="#F5F5F5"><textarea
												name="visiting_clinic_address" class="registetextbox1">
												<?php echo  $result['visit_clinic_address']; ?>
											</textarea>
										</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>

									<tr>
										<td width="190" bgcolor="#F5F5F5"><div
												class="postpropertytext">Visiting Hospitals:</div></td>
										<td width="570" bgcolor="#F5F5F5"><textarea
												name="visiting_hospitals" class="registetextbox1">
												<?php echo  $result['visit_hospital_name']; ?>
											</textarea><br /> <font color="red" size="2">Note- : Separate
												hospital names by semi-colon ' ; '</font>
										</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									<tr>
										<td width="190" bgcolor="#F5F5F5"><div
												class="postpropertytext">Visiting Hospital Address:</div></td>
										<td width="570" bgcolor="#F5F5F5"><textarea
												name="visiting_hospital_address" class="registetextbox1">
												<?php echo  $result['visit_hospital_address']; ?>
											</textarea>
										</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									<tr>
										<td width="190" bgcolor="#F5F5F5"><div
												class="postpropertytext">Clinical Address:</div></td>
										<td width="570" bgcolor="#F5F5F5"><textarea
												name="clinical_address" class="registetextbox1">
												<?php echo  $result['doc_clinical_address']; ?>
											</textarea>
										</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									
									<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Education:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="doc_qlifiction" class="registetextbox1">
													<?php echo  $result['doc_qualification']; ?></textarea>

											</td>

										</tr>
										
										<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
									
													<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Memberships:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="memberships" class="registetextbox1">
													<?php echo  $result['memberships']; ?></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="membershipError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>
										
													<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>

										<tr>

											<td width="185" bgcolor="#F5F5F5"><div
													class="postpropertytext">Registrations:&nbsp;<font color="#FF0000">*</font></div></td>

											<td width="575" bgcolor="#F5F5F5"><textarea
													name="registrations" class="registetextbox1">
													<?php echo  $result['registrations']; ?></textarea>

											</td>

										</tr>

										<!--    ERROR DIV -->

										<tr>
											<td align="center" height="8" colspan="2">

												<div id="registrationError" class="error"
													style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>

											</td>

										</tr>

										<!--  END ERROR DIV -->

										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
										<tr>
										<td bgcolor="#F5F5F5"><div class="postpropertytext">
												Clinic Consulting Fee:&nbsp;&#8377;
											</div></td>
										<td width="185" bgcolor="#F5F5F5"><input type="text"
											name="consulting_fee" value="<?php echo $result['consulting_fee'] ;?>" />
										</td>
										</tr>
										
										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
										<tr>
										<td bgcolor="#F5F5F5"><div class="postpropertytext">
												Hospital Consulting Fee:&nbsp;&#8377;
											</div></td>
										<td width="185" bgcolor="#F5F5F5"><input type="text"
											name="hospital_consulting_fee" value="<?php echo $result['hospital_consulting_fee'] ;?>" />
										</td>
										</tr>
										
										<tr>
											<td><img src="../images/blank.gif" width="1" height="6"
												alt="" border="0"></td>
										</tr>
										
									<tr>
										<td bgcolor="#F5F5F5"><div class="postpropertytext">About
												Doctor:</div></td>
										<!--td bgcolor="#F5F5F5">
											<!--        <textarea name="about_doc" class="registetextbox1" style="height:150px;">
          <?php echo  $result['about_doctor']; ?>
          </textarea>--> <!--textarea id="about_doc" name="about_doc"
												cols="60" rows="10">
												<?php //echo  $result['about_doctor']; ?>
											</textarea> <script type="text/javascript">
                var editor = CKEDITOR.replace('about_doc');
                CKFinder.setupCKEditor( editor, 'ckfinder/' );
                ste.init();
          </script-->
										<!--/td-->
										<td width="575" bgcolor="#F5F5F5"><textarea
													id="about_doc" name="about_doc" cols="60"
													rows="10" class="registetextbox1">
													<?php echo  $result['about_doctor']; ?></textarea>

											</td>
									</tr>
									<tr>
										<td><img src="../images/blank.gif" width="1" height="6" alt=""
											border="0"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><input type="hidden" name="doc_type"
								value="<?php echo  $result['doc_category']; ?>" /></td>
						</tr>
						<?php
						$doc_ctgry= $result['doc_category'];
			}
			$phone_charge_3min="";
			$phone_charge_15min="";
			$phone_charge_30min="";
			$phone_charge_60min= "";
			$phone_charge_120min= "";
			$phone_charge_180min= "";

			$email_1_session= "";
			$email_3_session= "";
			$email_5_session= "";
			$email_10_session= "";

			$exp_email_1_session= "";
			$exp_email_3_session= "";
			$exp_email_5_session= "";
			$exp_email_10_session= "";
			
			$diagnostic_1= "";
			$diagnostic_5= "";
			$diagnostic_10= "";
			
			

			$qry3= "SELECT * FROM `packages` where `doctor_id`='$edit_id' ";
			$qry_rslt3 = mysql_query($qry3);


			if(mysql_fetch_row($qry_rslt3)<1)
			{


				$qry4= "SELECT * FROM `packages` where `doctor_id`='0' && type_of_doctor='$doc_ctgry' ";
				$qry_rslt4 = mysql_query($qry4);
				while($result4 = mysql_fetch_array($qry_rslt4))
				{
					$phone_charge_3min= $result4['3_min_charge'];
					$phone_charge_15min= $result4['15_min_charge'];
					$phone_charge_30min= $result4['30_min_charge'];
					$phone_charge_60min= $result4['60_min_charge'];
					$phone_charge_120min= $result4['120_min_charge'];
					$phone_charge_180min= $result4['180_min_charge'];

					$email_1_session= $result4['1_email_session_charge'];
					$email_3_session= $result4['3_email_session_charge'];
					$email_5_session= $result4['5_email_session_charge'];
					$email_10_session= $result4['10_email_session_charge'];

					$exp_email_1_session= $result4['exprt_email_1_session'];
					$exp_email_3_session= $result4['exprt_email_3_session'];
					$exp_email_5_session= $result4['exprt_email_5_session'];
					$exp_email_10_session= $result4['exprt_email_10_session'];
					
					$diagnostic_1= $result4['1_diagnostic_consultation'];
					$diagnostic_5= $result4['5_diagnostic_consultation'];
					$diagnostic_10= $result4['10_diagnostic_consultation'];
					
				}
			}
			else
			{
				$qry5= "SELECT * FROM `packages` where `doctor_id`='$edit_id' ";
				$qry_rslt5 = mysql_query($qry5);
				while($result5 = mysql_fetch_array($qry_rslt5))
				{
					$phone_charge_3min= $result5['3_min_charge'];
					$phone_charge_15min= $result5['15_min_charge'];
					$phone_charge_30min= $result5['30_min_charge'];
					$phone_charge_60min= $result5['60_min_charge'];
					$phone_charge_120min= $result5['120_min_charge'];
					$phone_charge_180min= $result5['180_min_charge'];

					$email_1_session= $result5['1_email_session_charge'];
					$email_3_session= $result5['3_email_session_charge'];
					$email_5_session= $result5['5_email_session_charge'];
					$email_10_session= $result5['10_email_session_charge'];
					$exp_email_1_session= $result5['exprt_email_1_session'];
					$exp_email_3_session= $result5['exprt_email_3_session'];
					$exp_email_5_session= $result5['exprt_email_5_session'];
					$exp_email_10_session= $result5['exprt_email_10_session'];
					
					$diagnostic_1= $result5['1_diagnostic_consultation'];
					$diagnostic_5= $result5['5_diagnostic_consultation'];
					$diagnostic_10= $result5['10_diagnostic_consultation'];
					
				}
				if($exp_email_1_session =='' && $exp_email_3_session =='' && $exp_email_5_session =='')
				{
					$qry6= "SELECT * FROM `packages` where `doctor_id`='0' && type_of_doctor='Expert' ";
					$qry_rslt6 = mysql_query($qry6);
					while($result6 = mysql_fetch_array($qry_rslt6))
					{
						$exp_email_1_session= $result6['exprt_email_1_session'];
						$exp_email_3_session= $result6['exprt_email_3_session'];
						$exp_email_5_session= $result6['exprt_email_5_session'];
						$exp_email_10_session= $result6['exprt_email_10_session'];
					}
				}
				if($email_1_session =='' && $email_3_session =='' && $email_5_session =='')
				{
					$qry7= "SELECT * FROM `packages` where `doctor_id`='0' && type_of_doctor='Specialist' ";
					$qry_rslt7 = mysql_query($qry7);
					while($result7 = mysql_fetch_array($qry_rslt7))
					{
						//$phone_charge_3min= $result7['3_min_charge'];
						//$phone_charge_15min= $result7['15_min_charge'];
						//$phone_charge_30min= $result7['30_min_charge'];
						//$phone_charge_60min= $result7['60_min_charge'];
						//$phone_charge_120min= $result7['120_min_charge'];
						//$phone_charge_180min= $result7['180_min_charge'];
						$email_1_session= $result7['1_email_session_charge'];
						$email_3_session= $result7['3_email_session_charge'];
						$email_5_session= $result7['5_email_session_charge'];
						$email_10_session= $result7['10_email_session_charge'];
					}
				}

			}
			$qry8= "SELECT * FROM `packages` where `type_of_doctor`='Physician' ";
			$qry_rslt8 = mysql_query($qry8);
			while($result8 = mysql_fetch_array($qry_rslt8))
			{

				$chat_1_session= $result8['1_chat_session_charge'];
				$chat_3_session= $result8['3_chat_session_charge'];
				$chat_5_session= $result8['5_chat_session_charge'];

			}
			$qry9= "SELECT * FROM `packages` where `type_of_doctor`='Counsellor' ";
			$qry_rslt9 = mysql_query($qry9);
			while($result9 = mysql_fetch_array($qry_rslt9))
			{
				$con_phone_charge_60min= $result9['60_min_charge'];
				$con_phone_charge_120min= $result9['120_min_charge'];
				$con_phone_charge_180min= $result9['180_min_charge'];
				$coun_email_1_session=$result9['1_email_session_charge'];
				$coun_email_3_session=$result9['3_email_session_charge'];
				$coun_email_5_session=$result9['5_email_session_charge'];
				$coun_email_10_session=$result9['10_email_session_charge'];
			}
			?>
						<tr>
							<td colspan="4">
								<div id="query_rate_1" style="display: none;">

									<!-- ############################### Query rates start ################################ -->

									<table cellspacing="0" cellpadding="0" border="0" width="760">

										<tr>

											<td colspan="6" bgcolor="#F5F5F5">

												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Query
													Packages :</div>

											</td>

										</tr>

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>

										<tr>

											<td bgcolor="#F5F5F5" width="139"><div
													class="postpropertytext">1 Query Session:</div></td>

											<?php

											include "../db_connect.php";

											$qry = "select 1_email_session_charge,3_email_session_charge,5_email_session_charge from packages where type_of_doctor='query' and doctor_id=0";

											if (!$qry_rslt = mysql_query($qry))

												die(mysql_error());

											while ($qry_result = mysql_fetch_array($qry_rslt)) {

                $result1 = $qry_result['1_email_session_charge'];

                $result2 = $qry_result['3_email_session_charge'];

                $result3 = $qry_result['5_email_session_charge'];

            }

            ?>



											<td width="128" bgcolor="#F5F5F5"><input type="text"
												name="features2" value="<?php echo $result1; ?>"
												disabled="disabled" class="txtbox1" /></td>

											<td bgcolor="#F5F5F5" width="119"><div
													class="postpropertytext">3 Query Session:</div></td>

											<td width="116" bgcolor="#F5F5F5"><input type="text"
												name="features7" value="<?php echo $result2; ?>"
												disabled="disabled" class="txtbox1" /></td>

											<td bgcolor="#F5F5F5" width="118"><div
													class="postpropertytext">5 Query Session:</div></td>

											<td width="140" bgcolor="#F5F5F5"><input type="text"
												name="features7" value="<?php echo $result3; ?>"
												disabled="disabled" class="txtbox1" /></td>



										</tr>

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>

									</table>

									<!-- ##################################### stop ######################3############# -->

								</div>

								<div id="query_rate_2" style="display: none;">

									<!-- ############################### Query rates start ################################ -->

									<table cellspacing="0" cellpadding="0" border="0" width="760">

										<tr>

											<td colspan="6" bgcolor="#F5F5F5">

												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Query
													Packages :</div>

											</td>

										</tr>

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>

										<tr>

											<td bgcolor="#F5F5F5" width="139"><div
													class="postpropertytext">1 Query Session:</div></td>

											<?php

											include "../db_connect.php";

											$qry = "select 1_email_session_charge,3_email_session_charge,5_email_session_charge from packages where type_of_doctor='query' and doctor_id=1";

											if (!$qry_rslt = mysql_query($qry))

												die(mysql_error());

											while ($qry_result = mysql_fetch_array($qry_rslt)) {

                $result1 = $qry_result['1_email_session_charge'];

                $result2 = $qry_result['3_email_session_charge'];

                $result3 = $qry_result['5_email_session_charge'];

            }

            ?>



											<td width="128" bgcolor="#F5F5F5"><input type="text"
												name="features2" value="<?php echo $result1; ?>"
												disabled="disabled" class="txtbox1" /></td>

											<td bgcolor="#F5F5F5" width="119"><div
													class="postpropertytext">3 Query Session:</div></td>

											<td width="116" bgcolor="#F5F5F5"><input type="text"
												name="features7" value="<?php echo $result2; ?>"
												disabled="disabled" class="txtbox1" /></td>

											<td bgcolor="#F5F5F5" width="118"><div
													class="postpropertytext">5 Query Session:</div></td>

											<td width="140" bgcolor="#F5F5F5"><input type="text"
												name="features7" value="<?php echo $result3; ?>"
												disabled="disabled" class="txtbox1" /></td>



										</tr>

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>

									</table>

									<!-- ##################################### stop ######################3############# -->

								</div>


								<div id="physician_rate" style="display: none;">
									<!-- ############################### physician rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td colspan="6" bgcolor="#F5F5F5">
												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Physician
													Packages :</div>
											</td>
										</tr>
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="139"><div
													class="postpropertytext">1 Online Session:</div></td>
											<td width="128" bgcolor="#F5F5F5"><input type="text"
												name="1_chat_session"
												value="<?php echo  $chat_1_session; ?>" disabled="disabled"
												class="txtbox1" />
											</td>
											<td bgcolor="#F5F5F5" width="119"><div
													class="postpropertytext">3 Online Session:</div></td>
											<td width="116" bgcolor="#F5F5F5"><input type="text"
												name="3_chat_session"
												value="<?php echo  $chat_3_session; ?>" disabled="disabled"
												class="txtbox1" />
											</td>
											<td bgcolor="#F5F5F5" width="118"><div
													class="postpropertytext">5 Online Session:</div></td>
											<td width="140" bgcolor="#F5F5F5"><input type="text"
												name="5_chat_session"
												value="<?php echo  $chat_5_session; ?>" disabled="disabled"
												class="txtbox1" />
											</td>
										</tr>
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									<!-- ##################################### stop ######################3############# -->
								</div>
								<div id="specilist_rate1" style="display: none;">
									<!-- ############################### Specialist phone rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td colspan="10" bgcolor="#F5F5F5">
												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Specialist
													Packages :</div>
											</td>
										</tr>
										<tr>
											<td colspan="10"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="78"><div
													class="postpropertytext">3 Mins :</div></td>
											<td bgcolor="#F5F5F5" width="77"><input type="text"
												name="3_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_3min; ?>" />
											</td>
											<td bgcolor="#F5F5F5" width="78"><div
													class="postpropertytext">15 Mins :</div></td>
											<td bgcolor="#F5F5F5" width="77"><input type="text"
												name="15_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_15min; ?>" />
											</td>
											<td bgcolor="#F5F5F5" width="74"><div
													class="postpropertytext">30 Mins :</div></td>
											<td bgcolor="#F5F5F5" width="75"><input type="text"
												name="30_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_30min; ?>" />
											</td>
											<!--<td bgcolor="#F5F5F5" width="75"><div class="postpropertytext">60 Mins :</div></td>
            <td bgcolor="#F5F5F5" width="75">-->
										
										
										<tr>
											<td><input type="hidden" name="60_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_60min; ?>" /></td>
										</tr>
										<!--</td>-->
										<!--<td bgcolor="#F5F5F5" width="78"><div class="postpropertytext">120 Mins:</div></td>
            <td width="75" bgcolor="#F5F5F5">-->
										<tr>
											<td><input type="hidden" name="120_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_120min; ?>" /></td>
										</tr>
										<!--</td>
            <td bgcolor="#F5F5F5" width="77"><div class="postpropertytext">180 Mins:</div></td>
            <td width="76" bgcolor="#F5F5F5">-->
										<tr>
											<td><input type="hidden" name="180_min_charge" class="txtbox"
												value="<?php echo  $phone_charge_180min; ?>" /></td>
										</tr>
										<!--</td>-->

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									<!-- ##################################### stop #################################### -->
								</div>
								<div id="specilist_rate2" style="display: none;">
									<!-- ############################### Specialist email rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td bgcolor="#F5F5F5" width="126"><div
													class="postpropertytext">1 Online Consult:</div></td>
											<td width="132" bgcolor="#F5F5F5"><input type="text"
												name="spe_1_email_session" class="txtbox1"
												value="<?php echo  $email_1_session ; ?>" />
											</td>
											<!--
            <td bgcolor="#F5F5F5" width="121"><div class="postpropertytext">3 Email Session:</div></td>
            <td width="128" bgcolor="#F5F5F5">
            	<input type="text" name="spe_3_email_session" class="txtbox1" value="<?php echo  $email_3_session ; ?>"/>
            </td>
-->
											<td bgcolor="#F5F5F5" width="122"><div
													class="postpropertytext">5 Online Consult:</div></td>
											<td width="131" bgcolor="#F5F5F5"><input type="text"
												name="spe_5_email_session" class="txtbox1"
												value="<?php echo  $email_5_session ;?>" />
											</td>
											<td bgcolor="#F5F5F5" width="122"><div
													class="postpropertytext">10 Online Consult:</div></td>
											<td width="131" bgcolor="#F5F5F5"><input type="text"
												name="spe_10_email_session" class="txtbox1"
												value="<?php echo  $email_10_session ;?>" />
											</td>

										</tr>
										<!--
        <tr>
            <td bgcolor="#F5F5F5" width="122"><div class="postpropertytext">10 Email Session:</div></td>
            <td width="131" bgcolor="#F5F5F5">
            	<input type="text" name="spe_10_email_session" class="txtbox1" value="<?php echo  $email_10_session ;?>"/>
            </td>
        </tr>
-->
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									<!-- ##################################### stop #################################### -->
								</div>
								
								
								<!-- new div for diagnostic packages added by shoaib -->
								
									<div id="specilist_rate6" style="display: none;">
									<!-- ############################### Specialist email rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td bgcolor="#F5F5F5" width="126"><div
													class="postpropertytext">1 Diagnostic Consult:</div></td>
											<td width="132" bgcolor="#F5F5F5"><input type="text"
												name="1_diagnostic_consult" class="txtbox1"
												value="<?php echo  $diagnostic_1 ; ?>" />
											</td>
											
											<td bgcolor="#F5F5F5" width="122"><div
													class="postpropertytext">5 Diagnostic Consult:</div></td>
											<td width="131" bgcolor="#F5F5F5"><input type="text"
												name="5_diagnostic_consult" class="txtbox1"
												value="<?php echo  $diagnostic_5 ;?>" />
											</td>
											<td bgcolor="#F5F5F5" width="122"><div
													class="postpropertytext">10 Diagnostic Consult:</div></td>
											<td width="131" bgcolor="#F5F5F5"><input type="text"
												name="10_diagnostic_consult" class="txtbox1"
												value="<?php echo  $diagnostic_10 ;?>" />
											</td>

										</tr>
										
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									
								</div>
								<!-- end diagnostic packages - shoaib -->
								<!-- ##################################### stop #################################### -->
								
								<div id="cousilling_rate" style="display: none;">
									<!-- ############################### counsellor telephonic rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td colspan="10" bgcolor="#F5F5F5">
												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">counsellor
													Packages :</div>
											</td>
										</tr>
										<tr>
											<td colspan="10"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="115"><div
													class="postpropertytext">60 Mins :</div></td>
											<td bgcolor="#F5F5F5" width="144"><input type="text"
												name="coun_60_min_charge"
												value="<?php echo  $con_phone_charge_60min ; ?>"
												disabled="disabled" />
											</td>
											<!--<td bgcolor="#F5F5F5" width="107"><div class="postpropertytext">120 Mins:</div></td>
    <td width="144" bgcolor="#F5F5F5">-->
										
										
										<tr>
											<td><input type="hidden" name="coun_120_min_charge"
												value="<?php echo  $con_phone_charge_120min ; ?>"
												disabled="disabled" /></td>
										</tr>
										<!--</td>
    <td bgcolor="#F5F5F5" width="106"><div class="postpropertytext">180 Mins:</div></td>
    <td width="144" bgcolor="#F5F5F5">-->

										<tr>
											<td><input type="hidden" name="coun_180_min_charge"
												value="<?php echo  $con_phone_charge_180min ; ?>"
												disabled="disabled" /></td>
										</tr>
										<!--</td>-->

										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="115"><div
													class="postpropertytext">1 Online Consult:</div></td>
											<td bgcolor="#F5F5F5"><input type="text"
												name="coun_1_email_session"
												value="<?php echo  $coun_email_1_session ; ?>"
												disabled="disabled" />
											</td>
											<td bgcolor="#F5F5F5" width="107"><div
													class="postpropertytext">3 Online Consults:</div></td>
											<td bgcolor="#F5F5F5"><input type="text"
												name="coun_3_email_session"
												value="<?php echo  $coun_email_3_session ; ?>"
												disabled="disabled" />
											</td>
											<td bgcolor="#F5F5F5" width="106"><div
													class="postpropertytext">5 Online Consults:</div></td>
											<td bgcolor="#F5F5F5"><input type="text"
												name="coun_5_email_session"
												value="<?php echo  $coun_email_5_session ; ?>"
												disabled="disabled" />
											</td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="106"><div
													class="postpropertytext">10 Online Consults:</div></td>
											<td bgcolor="#F5F5F5"><input type="text"
												name="coun_10_email_session"
												value="<?php echo  $coun_email_10_session ; ?>"
												disabled="disabled" />
											</td>
										</tr>
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									<!-- ##################################### stop #################################### -->
								</div>
								<div id="expert_rate" style="display: none;">
									<!-- ############################### Expert email Rates start ################################ -->
									<table cellspacing="0" cellpadding="0" border="0" width="760">
										<tr>
											<td colspan="6" bgcolor="#F5F5F5">
												<div
													style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Expert
													Packages :</div>
											</td>
										</tr>
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
										<tr>
											<td bgcolor="#F5F5F5" width="125"><div
													class="postpropertytext">1 Online Consult:</div></td>
											<td width="134" bgcolor="#F5F5F5"><input type="hidden"
												name="email_session_1" class="txtbox1"
												value="<?php echo  $exp_email_1_session ; ?>" /> <input
												type="text" name="email_session_1_tmp" disabled="disabled"
												class="txtbox1"
												value="<?php echo  $exp_email_1_session ; ?>" />
											</td>
											<td bgcolor="#F5F5F5" width="122"><div
													class="postpropertytext">3 Online Consults:</div></td>
											<td width="125" bgcolor="#F5F5F5"><input type="hidden"
												name="email_session_3" class="txtbox1"
												value="<?php echo  $exp_email_3_session ; ?>" /> <input
												type="text" name="email_session_3_tmp" disabled="disabled"
												class="txtbox1"
												value="<?php echo  $exp_email_3_session ; ?>" />
											</td>
											<!--
    <td bgcolor="#F5F5F5" width="123"><div class="postpropertytext">5 Email Session:</div></td>
    <td width="131" bgcolor="#F5F5F5">
    	<input type="hidden" name="email_session_5" class="txtbox1" value="<?php echo  $exp_email_5_session ; ?>"/>
	<input type="text" name="email_session_5_tmp" disabled="disabled" class="txtbox1" value="<?php echo  $exp_email_5_session ; ?>"/>
    </td>
-->
										</tr>
										<!--
  <tr>
      <td bgcolor="#F5F5F5" width="123"><div class="postpropertytext">10 Email Session:</div></td>
    <td width="131" bgcolor="#F5F5F5">
    	<input type="hidden" name="email_session_10" class="txtbox1" value="<?php echo  $exp_email_10_session ; ?>"/>
	<input type="text" name="email_session_10_tmp" disabled="disabled" class="txtbox1" value="<?php echo  $exp_email_10_session ; ?>"/>
    </td>
  </tr>
-->
										<tr>
											<td colspan="3"><img src="../images/blank.gif" width="1"
												height="3" alt="" border="0"></td>
										</tr>
									</table>
									<!-- ##################################### stop #################################### -->
								</div>
							</td>
						</tr>

						<tr>
							<td colspan="1" align="right" style="margin-left: 10px;"><input
								type="submit" name="doc_save" value="Save" id="doc_save"
								onclick="edit_doctor_validate(edit_doc_form);" />
							</td>
							<td colspan="1" align="right" style="margin-right: 10px;"><input
								type="button" name="cancel" value="Cancel" onclick="pageback()" />
							</td>

							<td colspan="1" align="right" style="margin-left: 10px;"><input
								type="button" name="gen_invoice" value="Generate Invoice"
								id="gen_invoice" style="display: none"
								onclick="getInvoice()" />
							</td>

						</tr>
						<tr>
							<td colspan="3"><img src="images/blank.gif" width="1" height="6"
								alt="" border="0"></td>
						</tr>
						<?php if(isset($_GET['saved']))
						{

							echo '<script> hide(); </script>';
							 }   ?>

					</table>
					<script>
					function changeRatio()
					{
						if(document.getElementById("doctor_spe").checked==true)
						document.getElementById("spe_ratio").style.display="block";
						else
							document.getElementById("spe_ratio").style.display="none";
						if(document.getElementById("doctor_exp").checked==true)
							document.getElementById("exp_ratio").style.display="block";
							else
								document.getElementById("exp_ratio").style.display="none";
						if(document.getElementById("doctor_qry").checked==true)
							document.getElementById("qry_ratio").style.display="block";
							else
								document.getElementById("qry_ratio").style.display="none";
							
				    }

					</script>

				</form>
			</td>
	
	</table>
	<script>
function getInvoice()
{
var	docId=document.getElementById("doctor_id").value;

if ((document.getElementById('doctor_tele_con').checked)&&(document.getElementById('doctor_online_con').checked)) {
    var planId=3;
}

else if (document.getElementById('doctor_tele_con').checked) {
	 var planId=1;
}
else if (document.getElementById('doctor_online_con').checked) {
	 var planId=2;
}
else if (document.getElementById('doctor_diag_con').checked) {
	 var planId=2;
}

document.location.href="edit-doctor-invoice.php?plan="+planId+"&doc_id="+docId;
 
}
</script>


	<script type="text/javascript">
	  if(appointment==1){
          //alert("appointment");
          document.edit_doc_form.doctor_app.checked = true;
          document.getElementById("Appoint_type").style.display="block";
      }else if(appointment==0){
          document.getElementById("Appoint_type").style.display="none";
      }
      
      if(type=='Specialist')
	{
		document.getElementById("spe_contact_type").style.display="block";
		document.edit_doc_form.doctor_spe.checked = true;
		disable_two();
		document.getElementById("spe_ratio").style.display="block";
		if(online_no=='online')
		{
			document.edit_doc_form.doctor_online_con.checked = true;
			document.getElementById("specilist_rate2").style.display="block";
		}
		if(tele_no=='tele')
		{
			document.edit_doc_form.doctor_tele_con.checked = true;
			document.getElementById("specilist_rate1").style.display="block";
			document.getElementById("spe_tele_type1").style.display="block";
		}
		if(diag_no=='diagnostic')
		{
			document.edit_doc_form.doctor_diag_con.checked = true;
			document.getElementById("specilist_rate6").style.display="block";
			
		}
		
		if(tele_no=='' && online_no=='' && diag_no=='' )
		{
			document.edit_doc_form.doctor_none_con.checked = true;
			document.getElementById("specilist_rate1").style.display="none";
			document.getElementById("spe_tele_type1").style.display="none";
			document.getElementById("specilist_rate2").style.display="none";
			document.getElementById("specilist_rate6").style.display="none";
		}
	}
	else 
			document.getElementById("spe_ratio").style.display="none";
       
        if(type=='Expert')
	{
		document.getElementById("expert_rate").style.display="block";
		document.edit_doc_form.doctor_exp.checked = true;
		disable_two();
			document.getElementById("exp_ratio").style.display="block";
			
	}
        else if(type=='Query')
        {
        	document.getElementById("expert_rate").style.display="block";
    		document.edit_doc_form.doctor_exp.checked = true;
    		disable_two();
    		document.getElementById("qry_ratio").style.display="block";
        }
		else if(type=='Tele_Query')
        {
        	//document.getElementById("expert_rate").style.display="block";
    		document.edit_doc_form.doctor_tele_qry.checked = true;
    		//disable_two();
    		//document.getElementById("qry_ratio").style.display="block";
        }
		else if(type=='1')
        {
        	//document.getElementById("expert_rate").style.display="block";
    		document.edit_doc_form.doctor_app_not_signed.checked = true;
    		//disable_two();
    		//document.getElementById("qry_ratio").style.display="block";
        }
	else if(type=='Specialist/Expert')
	{
		document.getElementById("spe_contact_type").style.display="block";
		document.edit_doc_form.doctor_spe.checked = true;
		disable_two();
		if(online_no=='online')
		{
			document.edit_doc_form.doctor_online_con.checked = true;
			document.getElementById("specilist_rate2").style.display="block";
		}
		if(diag_no=='diagnostic')
		{
			document.edit_doc_form.doctor_diag_con.checked = true;
			document.getElementById("specilist_rate6").style.display="block";
			
		}
		if(tele_no=='tele')
		{
			document.edit_doc_form.doctor_tele_con.checked = true;
			document.getElementById("specilist_rate1").style.display="block";
			document.getElementById("spe_tele_type1").style.display="block";
		}
		
		document.getElementById("expert_rate").style.display="block";
		document.edit_doc_form.doctor_exp.checked = true;
		
	}
        
        setTimeout(update_query(), 10000);
        
        
      
        
        function update_query(){
            
            var doc_query =  document.getElementById("doctor_qry").checked;
            var doc_spe = document.getElementById("doctor_spe").checked;
            var dpc_exp = document.getElementById("doctor_exp").checked;



            if (doc_spe==true && doc_query==true)
            {   		
                    document.getElementById("query_rate_2").style.display="block";
                    document.getElementById("query_rate_1").style.display="none";
            }else if (dpc_exp==true && doc_query==true)
            {   		
                    document.getElementById("query_rate_2").style.display="block";
                    document.getElementById("query_rate_1").style.display="none";
            }else if (doc_query==true)
            {   		
                    document.getElementById("query_rate_1").style.display="block";
                    document.getElementById("query_rate_2").style.display="none";
            }
        }

		function isNumberKey1(evt)
		{

			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 58))
			{
				return false; 
			}
			else
			{
				return true; 
			}
		}
        
</script>
	<?php include 'admin_footer.php'; ?>

</body>
</html>
