<?php 
error_reporting(E_PARSE);
session_start();
include ("../includes/pw_db_connect.php");
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='admin')
{
header("Location: ../index.php");
exit();
}
//for pagination and database connection
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
include 'summary.report.class.php';
$obj=new connect;

$report = new SummaryReport;
$start_year = "2011";
$current_year = strftime ( '%Y', (time()));
$current_month = strftime ( '%m', (time()));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
    <link rel="stylesheet" type="text/css" href="../css/designstyles.css">
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/add-new-doctor-validation.js"></script>
<script type="text/javascript" src="js/add-doctor-validation.js"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/consultation.js"></script>
<script>


function load_weeklyReport(type,year,month,doc){
        
        var paramString = 'type='+type+'&year='+year+'&month='+month+'&doc='+doc;  
        alert(paramString);
        $.ajax({  
                type: "POST",  
                url: "month-wise-online-consultation-details.php",  
                data: paramString,  
                success: function(response) {  
                    
                    $().html(response);

                }  
        });
    
}


function load_weekReport(param,date,type,doc){
        
        var paramString = 'param='+param+'&date='+date+'&type='+type+'&doc='+doc;  
        
        $.ajax({  
                type: "POST",  
                url: "weekwisereport.php",  
                data: paramString,  
                success: function(response) {  
                    
                     document.getElementById('monthwise').innerHTML =response;

                }  
        });
    
}


function load_reportdoc(type,doc){
        
        var paramString = 'type='+type+'&doc='+doc;  
        
        $.ajax({  
                type: "POST",  
                url: "getdoc_report.php",  
                data: paramString,  
                success: function(response) {  
                    
                     document.getElementById('doc').innerHTML =response;

                }  
        });
    
}


</script>
<style>	
a:hover{
color:#e70976;
text-decoration:underline;
}
</style>
</head>
<body>
<link href="../css/designstyles.css" media="screen, projection" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<?php include "admin_head.php"; ?>
    
<?php
    $type = $_POST['type']; 
    $doc = $_POST['doc'];
    $month = $_POST['month'];
    $year = $_POST['year']; 
    if($type==""){
        $type="spe_online";
    }
 ?>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<!--Table1_start -->    
<table width="1000"  cellspacing="0" cellpadding="0" align="center" >
    <tr>
        <td width="228"  valign="top" style="border-right:1px solid #4d4d4d; border-left:1px solid #4d4d4d;">
            <?php include "admin_left_menu.php"; ?>
        </td>
        <td width="772" valign="top" >
            <br />
            
            <center><strong>Week Wise Report<strong></center>
                        
            <form method="post">
                <table border='0' cellpadding='1' cellspacing='0' width='50%' align='center' >
                    <tr>
                        <td>
                            Consultation Type : 
                        </td>
                        <td>
                            <select name="type" id="type" onchange="load_reportdoc(this.value,'<?php echo $doc; ?>')">                                
                                <?php
                                
                                    $type_qry = "select distinct type from online_consultation_summary";
                                    $type_result = mysql_query($type_qry);
                                    while($typq_data = mysql_fetch_array($type_result)){
                                        $type1 = $typq_data['type'];
                                        $type_name = $report->getTypeName($type1);
                                        echo "<option value='$type1'>$type_name</option>";
                                    }
                                    
                                    $type_qry = "select distinct type from tele_consultation_summary";
                                    $type_result = mysql_query($type_qry);
                                    while($typq_data = mysql_fetch_array($type_result)){
                                        $type1 = $typq_data['type'];
                                        $type_name = $report->getTypeName($type1);
                                        echo "<option value='$type1'>$type_name</option>";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Doctor Name : 
                        </td>
                        <td>
                            <select name="doc" id="doc"> 
                                <?php
                                $doc_qry = "select doc_id,doc_name from pw_doctors where blocked<>'Y' and activate_online_consultation='online' and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                                $result = mysql_query($doc_qry);
                                echo "<option value='all'>All</option>";
                                while($data = mysql_fetch_array($result)){
                                        echo  "<option value='".$data['doc_id']."'>".$data['doc_name']."</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Month/Year : 
                        </td>
                        <td>
                            <select name="month" id="month"> 
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                            <select name="year" id="year">
                                <?php
                                    for($i=$start_year ; $i<=$current_year ; $i++){
                                        echo "<option value='$i'><b>$i</option>";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input name="submit" type="submit"  value='submit' /></td>
                    </tr>
                </table>                
            </form>
                        
            
            
            <br /><br />
            <?php
                    if($_POST['submit']=="submit"){
                ?>
                    <script>
                        document.getElementById("type").value="<?php echo $type; ?>";
                        document.getElementById("month").value="<?php echo $month; ?>";
                        document.getElementById("year").value="<?php echo $year; ?>";
                    </script>
                 <?php
                    }else{
                 ?>       
                      <script>
                        document.getElementById("month").value="<?php echo $current_month; ?>";
                        document.getElementById("year").value="<?php echo $current_year; ?>";
                      </script>  
                 <?php
                    }
                 ?>
            <div id="monthwise">
                <?php       
                    
                    $extend = "";
                    
                    if($doc!="all" && $doc!=""){
                        $extend = " and doc_id='$doc'";
                    }
                    
                    if($month=="" || $year==""){
                        $month = strftime ('%m', (time()));
                        $year =  strftime ('%Y', (time()));
                    }
                    
                    $type_name = $report->getTypeName($type);
                    
                    $display =  $type_name." Data for $month/$year";
                    
                    echo  "<center><strong>$display<strong></center>";
                    
                    /*
                    $daysinmonth = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                    $lastdayofdate = "$daysinmonth-$month-$year";
                    $firstdayofmoth =  "01-$month-$year";
                    
                    $prevyear = strftime ('%Y', strtotime($firstdayofmoth)-(60*60*24));
                    $prevmonth = strftime ('%m', strtotime($firstdayofmoth)-(60*60*24));
                    $nextyear = strftime ('%Y', strtotime($lastdayofdate)+(60*60*24));
                    $nextmonth = strftime ('%m', strtotime($lastdayofdate)+(60*60*24));
                    */
                    
                    if($type=="spe_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_online_consultation='online' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }else if($type=="exp_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where  blocked<>'Y' $extend and ( doc_category='Specialist/Expert' || doc_category='Expert')";
                    }else if($type=="query_online"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' $extend and activate_online_query='query'";
                    }else if($type=="Doctor" || $type=="Dietician" || $type=="Counsellor"){
                        $doc_qry = "select distinct type doc_name,doctor_id doc_id from online_consultation_summary where type='$type'";
                    }else if($type=="spe_tele"){
                        $doc_qry = "select doc_id,doc_name,doc_category,blocked from pw_doctors where blocked<>'Y' and activate_tele_Consultation='tele' $extend and ( doc_category='Specialist/Expert' || doc_category='Specialist')";
                    }
                    
                    $doc_result = mysql_query($doc_qry);
                    $doc_num = mysql_num_rows($doc_result);
                    if($doc_num>0){
                        
                        
                        if($type=="spe_tele"){
                            echo "<table border='0' cellpadding='0' cellspacing='2' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
                            echo "<th bgcolor='#F5F5F5' align='left'rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th bgcolor='#F5F5F5' align='left' rowspan='2'></th>";

                            $weekwisetag = $report->getMonthTeleetag($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthTeleDatatag($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }else if($type=="query_online"){
                            echo "<table border='0' cellpadding='0' cellspacing='2' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
                            echo "<th bgcolor='#F5F5F5' align='left' rowspan='2'><b>Doctor Name</b></th>";
                            echo "<th bgcolor='#F5F5F5' align='left' rowspan='2' ></th>";

                            $weekwisetag = $report->getMonthonlinetagquery($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthonlineDatatagquery($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }else{
                            echo "<table border='0' cellpadding='0' cellspacing='2' bgcolor='#eeeeee'  class='s90registerform' align='center'><tr>";
                            echo "<th bgcolor='#F5F5F5' align='left' ><b>Doctor Name</b></th>";
                            echo "<th bgcolor='#F5F5F5' align='left' ></th>";

                            $weekwisetag = $report->getMonthonlinetag($month, $year);

                            echo $weekwisetag;

                            echo "</tr>";

                            while($doc_data = mysql_fetch_array($doc_result)){
                                $doc_id = $doc_data['doc_id'];
                                $doc_name = $doc_data['doc_name'];

                                echo "<tr>";
                                echo "<td nowrap>$doc_name</td>";

                                $counts = $report->getMonthonlineDatatag($month, $year, $doc_id, $type);
                                echo "<td>$counts</td>";

                                echo "</tr>";
                            }
                        }
                        echo "</table>";
                        
                    }else{
                        echo "No Records Found";
                    }
                ?>
		
		<center><a href="monthwise_consultation_excel.php?type=<?php echo $type; ?>&doc=<?php echo $doc; ?>&month=<?php echo $month; ?>&year=<?php echo $year; ?>">Export Report <img src="../images/excelLogo.gif" /></a></center>                
                
            </div>
            
            
        </td>
    </tr>   
</table>
<script type="text/javascript">
enable_counsultation_submenu();
</script>
<?php include 'admin_footer.php'; ?>

</body></html>
