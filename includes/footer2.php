<?php include("site_config.php");?>
<div class="clearfix"></div>
<div class="pw-footer-container">
	<div class="row max-960 row-one">
		<div class="col-sm-12 col-xs-12 left-nav">
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="home-patients.php">Patients</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>pw_Patients/meet-our-doctors.php">Meet our Doctors</a></li>
					<li><a href="<?php echo $site_url;?>pw_Patients/pinkQuery.php">Our Services</a></li>
					<li><a href="<?php echo $site_url;?>pw_Patients/happy-patients.php">Happy Patients</a></li>
					<li><a href="<?php echo $site_url;?>pw_Patients/register.php">Register</a></li>
					<li><a href="<?php echo $site_url;?>pw_Patients/patients-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="home-doctors.php">Doctors</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>pw_Doctors/happy-doctors.php">Happy Doctors</a></li>
					<li><a href="<?php echo $site_url;?>pw_Doctors/joinus.php">Join Us</a></li>
					<li><a href="<?php echo $site_url;?>pw_Doctors/doctors-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="home-corporates.php">Corporates</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>pw_Corporates/corporate-joinus.php">Join Us</a></li>
					<li><a href="<?php echo $site_url;?>pw_Corporates/corporate-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-12 right-content">
				<a href="<?php echo $site_url;?>subscribe.php" class="pw-btn">Subscribe</a>
			</div>
		</div>		
	</div>	
	<div class="row max-960 row-two">
		<div class="social-share">
			<div class="share facebook">
				<i class="fa fa-facebook"></i>
			</div>
			<div class="share youtube">
				<i class="fa fa-youtube-play"></i>
			</div>
		</div>		
	</div>
	<div class="row max-960 row-three">
		<div class="left-block">
			<div class="nav">
				<a href="privacy-policy.php">Privacy Policy</a>
			</div>
			<div class="nav">
				<a href="terms.php">Terms of Service</a>
			</div>
			<div class="nav">
				<span class="copyright"><i class="fa fa-copyright"></i> Copyright 2014 www.pinkwhalehealthcare.com</span>
			</div>
			<div class="designed-by">			
				<span>Design by: </span> 
				<a href="http://connectcreative.in/">Connect Creative</a>			
			</div>
		</div>		
	</div>
</div>

<!-- <div class="footer-container col-md-12">
	<div class="footer">
		<div class="content-area max-960">
			<div class="left col-xl-8 col-md-8 col-sm-8 col-xs-12 hidden-xs">	
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-patients.php">Patients</a></div>
					<a href="meet-our-doctors.php">Meet our Doctors</a>
					<a href="pinkQuery.php">Our Services</a>
					<a href="happy-patients.php">Happy Patients</a>
					<a href="register.php">Register</a>
					<a href="patients-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-doctors.php">Doctors</a></div>
					<a href="happy-doctors.php">Happy Doctors</a>
					<a href="joinus.php">Join Us</a>
					<a href="doctors-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-corporates.php">Corporates</a></div>
					<a href="corporate-joinus.php">Join Us</a>
					<a href="corporate-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="clearfix"></div>							
			</div>
			<div class="right col-xl-4 col-md-4 col-sm-4 col-xs-12">
				<a href="subscribe.php" class="subscribe">subscribe</a>
			</div>
			<div class="clearfix"></div>
			<div class="footer-bottom col-xl-12 col-md-12 col-sm-12">
				<span class="designby">
					<div class="sns-icon facebook"></div>					
					<div class="sns-icon youtube"></div>
				</span>
				<div class="clearfix"></div>
				
				<span><a href="privacy-policy.php">Privacy Policy</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span><a href="terms.php">Terms of Service</a></span>
				
				
				<span class="copyright"><span class="noa">&copy; Copyright 2014 www.pinkwhalehealthcare.com</span></span>				
				<span class="designby">
					<span>Design by:</span> 
					<a href="http://connectcreative.in/">Connect Creative</a>
				</span>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div> -->

<div class="modal-container">
	<div class="modal-box">
		<div class="times" onclick="hideLogin();">&times;</div>
		<div class="clearfix"></div>
		<h2 class="modal-title">Sign In To Explore <br > pinkWhale Healthcare!</h2>
		<div class="login-form-box">
			<div class="col-xl-3 col-md-3 field-label">Username :</div><div class="col-xl-9 col-md-9 field-value"><input class="input-fields" type="text" name="username"></div>
			<div class="col-xl-3 col-md-3 field-label">Password :</div><div class="col-xl-9 col-md-9 field-value"><input class="input-fields" type="password" name="username"></div>
			<div class="col-xl-3 col-md-3"></div><div class="col-xl-9 col-md-9 field-value">Forgot your Password? <a href="">Click Here.</a></div>
			<div class="col-xl-3 col-md-3"></div><div class="col-xl-9 col-md-9 field-value"><button class="submit-button" type="submit">Submit</button></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>



<!-- Modal -->
<div class="modal fade pw-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign In To Explore pinkWhale Healthcare!</h2>
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container row">
					<div class="col-md-6">
						<div class="user-title">Existing Users</div>
						<form class="form-horizontal" role="form" name="consult_signin" id="consult_signin" method="POST" 
						action="<?php echo $site_url;?>/pw_login_action.php" enctype="multipart/formdata">
						<div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
							<input name="login_nxt_page" id="login_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="loginUser" class="col-sm-5 control-label">Username:</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="username" id="username"  autocomplete="off">
								</div>								
							</div>
							<div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							<div class="form-group">
								<label for="loginPass" class="col-sm-5 control-label">Password:</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" name="password" id="password" autocomplete="off" >
								</div>
							</div>	
							<div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" id="login_submit" type="submit" onclick="return consult_LoginSignUp(consult_signin)">Sign In</button>
								</div>
							</div>											
						</form>
						<div class="form-group forgot">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									Forgot your Password? <a href="">Click Here.</a>
								</div>
							</div>	
					</div>
					<div class="col-md-6 right-form">
						<div class="newuser-title">New Users</div>
						<form class="form-horizontal" role="form" name="reg_form" id="reg_form">
						<input name="register_nxt_page" id="register_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="name" class="col-sm-5 control-label">Your Name:*</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="regname" id="regname" value="" autocomplete="off">
								</div>
							</div>
							 <!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                                
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="age" class="col-sm-5 control-label">Age:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" id="age" autocomplete="off" name="regage" id="regage" onkeypress="return isNumberKey(event);" value="">
								</div>
							</div>	
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="gender" class="col-sm-5 control-label">Gender:*</label>
								<div class="col-sm-7">
									<!--input type="text" class="form-control" id="gender" autocomplete="off" -->
									<select name="reggender" id="reggender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
								</div>
							</div>
								 <!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                              
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="cardno" class="col-sm-5 control-label">Card No.:</label>
								<div class="col-sm-7">
									<input type="number" class="form-control"  autocomplete="off" onblur="return card_check(this.value)" id="regcard" name="regcard" value="" >
								</div>
							</div>	
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="eid" class="col-sm-5 control-label">Email ID:*</label>
								<div class="col-sm-7">
									<input type="email" class="form-control" id="eid" autocomplete="off" id="regemail" name="regemail" value="" >
								</div>
							</div>	
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                              
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="password" class="col-sm-5 control-label">Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" autocomplete="off"  id="regPassword" name="regPassword" value="" onkeyup="validate_pswd(form)" >
								</div>
							</div>
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="password2" class="col-sm-5 control-label">Confirm Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" name="regConfirmPassword" id="regConfirmPassword" value="" onkeyup="validate_pswd(form)" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="mobno" class="col-sm-5 control-label">Mobile No.:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" name="regPhone1" id="regPhone1"  value="" onkeypress="return isNumberKey(event);" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                                </div>
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" type="submit" name="btnSubmit" id="btnSubmit" onclick="return register_pink(reg_form)" >Register</button>
								</div>
							</div>											
						</form>
				    </div>
				

				</div>
			</div>			
		</div>
	</div>
</div>




<!-- Modal -->
<div class="modal fade pw-modal" id="consult-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign in to start a Consult</h2>
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container row">
					<div class="col-md-6">
						<div class="user-title">Existing User</div>
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label for="loginUser" class="col-sm-5 control-label">Username:</label>
								<div class="col-sm-7">
									<input type="email" class="form-control" id="loginUser" autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<label for="loginPass" class="col-sm-5 control-label">Password:</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="loginPass" autocomplete="off" >
								</div>
							</div>	
							<div class="form-group forgot">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									Forgot your Password? <a href="">Click Here.</a>
								</div>
							</div>	
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" type="submit" title="Login" onclick="return consult_LoginSignUp(consult_signin)">Sign In</button>
								</div>
							</div>											
						</form>
					</div>
					<div class="col-md-6 right-form">
						<div class="newuser-title">New Users</div>
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label for="name" class="col-sm-5 control-label">Your Name:*</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="name" autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<label for="age" class="col-sm-5 control-label">Age:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" id="age" autocomplete="off" >
								</div>
							</div>	
							<div class="form-group">
								<label for="gender" class="col-sm-5 control-label">Gender:*</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="gender" autocomplete="off" >
								</div>
							</div>	
							<div class="form-group">
								<label for="cardno" class="col-sm-5 control-label">Card No.:</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" id="cardno" autocomplete="off" >
								</div>
							</div>	
							<div class="form-group">
								<label for="eid" class="col-sm-5 control-label">Email ID:*</label>
								<div class="col-sm-7">
									<input type="email" class="form-control" id="eid" autocomplete="off" >
								</div>
							</div>	
							<div class="form-group">
								<label for="password" class="col-sm-5 control-label">Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="password" autocomplete="off" >
								</div>
							</div>
							<div class="form-group">
								<label for="password2" class="col-sm-5 control-label">Confirm Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" id="password2" autocomplete="off" >
								</div>
							</div>
							<div class="form-group">
								<label for="mobno" class="col-sm-5 control-label">Mobile No.:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" id="mobno" autocomplete="off" >
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" type="submit">Register</button>
								</div>
							</div>											
						</form>
				    </div>
				

				</div>
			</div>			
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade pw-modal" id="pink-btn-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign in to Start a Consult!</h2>
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container">
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="loginUser" class="col-sm-3 control-label">Username:</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="loginUser" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="loginPass" class="col-sm-3 control-label">Password:</label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="loginPass" autocomplete="off" >
							</div>
						</div>	
						<div class="form-group forgot">
							<label for="inputPassword3" class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								Forgot your Password? <a href="">Click Here.</a>
							</div>
						</div>	
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<button class="submit-button" type="submit">Login</button>
							</div>
						</div>											
					</form>
				</div>
			</div>			
		</div>
	</div>
</div>