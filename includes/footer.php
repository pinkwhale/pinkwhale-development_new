<?php include("site_config.php");
$site_url= "http://localhost:81/pinkwhale_newest/";
?>

<div class="clearfix"></div>
<div class="pw-footer-container">
	<div class="row max-960 row-one">
		<div class="col-sm-12 col-xs-12 left-nav">
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="<?php echo $site_url;?>index.php">Patients</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>meet-our-doctors.php">Meet our Doctors</a></li>
					<li><a href="<?php echo $site_url;?>pinkQuery.php">Our Services</a></li>
					<li><a href="<?php echo $site_url;?>happy-patients.php">Happy Patients</a></li>
					<li><a href="<?php echo $site_url;?>register.php">Register</a></li>
					<li><a href="<?php echo $site_url;?>patients-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="<?php echo $site_url;?>pw_Doctors/home-doctors.php">Doctors</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>happy-doctors.php">Happy Doctors</a></li>
					<li><a href="<?php echo $site_url;?>joinus.php">Join Us</a></li>
					<li><a href="<?php echo $site_url;?>doctors-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="title"><a href="<?php echo $site_url;?>pw_Corporates/home-corporates.php">Corporates</a></div>
				<ul>
					<li><a href="<?php echo $site_url;?>corporate-joinus.php">Join Us</a></li>
					<li><a href="<?php echo $site_url;?>corporate-faqs.php">FAQ's</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-12 right-content">
				<a href="<?php echo $site_url;?>subscribe.php" class="pw-btn">Subscribe</a>
			</div>
		</div>		
	</div>	
	<div class="row max-960 row-two">
		<div class="social-share">
			<div class="share facebook">
				<i class="fa fa-facebook"></i>
			</div>
			<div class="share youtube">
				<i class="fa fa-youtube-play"></i>
			</div>
		</div>		
	</div>
	<div class="row max-960 row-three">
		<div class="left-block">
		<div class="nav">
				<a href="<?php echo $site_url;?>sitemap.php">Sitemap</a>
			</div>
			<div class="nav">
				<a href="<?php echo $site_url;?>privacy-policy.php">Privacy Policy</a>
			</div>
			<div class="nav">
				<a href="<?php echo $site_url;?>terms.php">Terms of Service</a>
			</div>
			<div class="nav">
				<span class="copyright"><i class="fa fa-copyright"></i> Copyright 2014 www.pinkwhalehealthcare.com</span>
			</div>
			<div class="designed-by">			
				<span>Design by: </span> 
				<a href="http://connectcreative.in/">Connect Creative</a>			
			</div>
		</div>		
	</div>
</div>

<!-- <div class="footer-container col-md-12">
	<div class="footer">
		<div class="content-area max-960">
			<div class="left col-xl-8 col-md-8 col-sm-8 col-xs-12 hidden-xs">	
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-patients.php">Patients</a></div>
					<a href="meet-our-doctors.php">Meet our Doctors</a>
					<a href="pinkQuery.php">Our Services</a>
					<a href="happy-patients.php">Happy Patients</a>
					<a href="register.php">Register</a>
					<a href="patients-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-doctors.php">Doctors</a></div>
					<a href="happy-doctors.php">Happy Doctors</a>
					<a href="joinus.php">Join Us</a>
					<a href="doctors-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<div class="links-header"><a href="home-corporates.php">Corporates</a></div>
					<a href="corporate-joinus.php">Join Us</a>
					<a href="corporate-faqs.php">FAQ&#39;s</a>
				</div>
				<div class="clearfix"></div>							
			</div>
			<div class="right col-xl-4 col-md-4 col-sm-4 col-xs-12">
				<a href="subscribe.php" class="subscribe">subscribe</a>
			</div>
			<div class="clearfix"></div>
			<div class="footer-bottom col-xl-12 col-md-12 col-sm-12">
				<span class="designby">
					<div class="sns-icon facebook"></div>					
					<div class="sns-icon youtube"></div>
				</span>
				<div class="clearfix"></div>
				
				<span><a href="privacy-policy.php">Privacy Policy</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span><a href="terms.php">Terms of Service</a></span>
				
				
				<span class="copyright"><span class="noa">&copy; Copyright 2014 www.pinkwhalehealthcare.com</span></span>				
				<span class="designby">
					<span>Design by:</span> 
					<a href="http://connectcreative.in/">Connect Creative</a>
				</span>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div> -->

<!--div class="modal-container">
	<div class="modal-box">
		<div class="times" onclick="hideLogin();">&times;</div>
		<div class="clearfix"></div>
		<h2 class="modal-title">Sign In To Explore <br > pinkWhale Healthcare!</h2>
		<div class="login-form-box">
		<form class="form-horizontal" role="form" name="consult2" id="consult2" method="POST" 
						action="<?= $site_url . "pw_login_action.php" ?>">
						<div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
							<input name="login_nxt_page" id="login_nxt_page" type="hidden"  size="18" />
			<div class="col-xl-3 col-md-3 field-label">Username :</div><div class="col-xl-9 col-md-9 field-value">
			<input class="input-fields" type="text" name="username" id="user2" autocomplete="off"></div>
			<div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
			<div class="col-xl-3 col-md-3 field-label">Password :</div><div class="col-xl-9 col-md-9 field-value">
			<input class="input-fields" type="password" name="password" id="pass2" autocomplete="off" ></div>
			<div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
			
			<div class="col-xl-3 col-md-3"></div><div class="col-xl-9 col-md-9 field-value"><button class="submit-button" id="login_submit" type="submit" onclick="return LoginSign(consult2)">Submit</button></div>
			</form>
			<div class="col-xl-3 col-md-3"></div><div class="col-xl-9 col-md-9 field-value"><form name="forget" id="forget" >
									Forgot your Password?<br /> <!--a href="">Click Here.</a>
									<p>Your Email-ID &nbsp; : &nbsp; <input autocomplete="off" value="" name="forgot_email" id="forgot_email" maxlength="50" /></p>
    <p><div id="forgotemailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></p>
    <div class="loginrow" ><div class="login_popupbtn">
    <button name="send"  id="for_send" class="submit-button" onclick="return forgot_password(forget);" />Submit</button></div></div>
    </form></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div-->

<!-- login popup start -->

<div id="login_message" style="display:none"></div><div id="login_body">

<!-- Modal -->
<div class="modal fade pw-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign In To Explore pinkWhale Healthcare!</h2>
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container row">
					<div class="col-md-6">
						<div class="user-title">Existing Users</div>
						<form class="form-horizontal" role="form" name="consult_signin" id="consult_signin" method="POST" 
						action="<?= $site_url . "pw_login_action.php" ?>">
						<div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
							<input name="login_nxt_page" id="login_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="loginUser" class="col-sm-5 control-label">Username:</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="username" id="username"  autocomplete="off">
								</div>								
							</div>
							<div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							<div class="form-group">
								<label for="loginPass" class="col-sm-5 control-label">Password:</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" name="password" id="password" autocomplete="off" >
								</div>
							</div>	
							<div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" id="login_submit" type="submit" onclick="return consult_LoginSignUp(consult_signin)">Sign In</button>
								</div>
							</div>											
						</form>
						<div class="form-group forgot">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7"><form name="forget" id="forget" >
									Forgot your Password?<br /> <!--a href="">Click Here.</a-->
									<p>Your Email-ID &nbsp; : &nbsp; <input autocomplete="off" value="" name="forgot_email" id="forgot_email" maxlength="50" /></p>
    <p><div id="forgotemailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></p>
    <div class="loginrow" ><div class="login_popupbtn">
    <button name="send"  id="for_send" class="submit-button" onclick="return forgot_password(forget);" />Submit</button></div></div>
    </form>
								</div>
							</div>	
					</div>
					<div class="col-md-6 right-form">
						<div class="newuser-title">New Users</div>
						<form class="form-horizontal" role="form" name="reg_form" id="reg_form">
						<input name="register_nxt_page" id="register_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="name" class="col-sm-5 control-label">Your Name:*</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="regname" id="regname" value="" autocomplete="off">
								</div>
							</div>
							 <!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                                
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="age" class="col-sm-5 control-label">Age:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control" id="age" autocomplete="off" name="regage" id="regage" onkeypress="return isNumberKey(event);" value="">
								</div>
							</div>	
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="gender" class="col-sm-5 control-label">Gender:*</label>
								<div class="col-sm-7">
									<!--input type="text" class="form-control" id="gender" autocomplete="off" -->
									<select name="reggender" id="reggender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
								</div>
							</div>
								 <!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                              
                                <!--  END ERROR DIV --> 
							<!--div class="form-group">
								<label for="cardno" class="col-sm-5 control-label">Card No.:</label>
								<div class="col-sm-7">
									<input type="number" class="form-control"  autocomplete="off" onblur="return card_check(this.value)" id="regcard" name="regcard" value="" >
								</div>
							</div-->	
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="eid" class="col-sm-5 control-label">Email ID:*</label>
								<div class="col-sm-7">
									<input type="email" class="form-control" id="eid" autocomplete="off" id="regemail" name="regemail" value="" >
								</div>
							</div>	
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                              
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="password" class="col-sm-5 control-label">Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" autocomplete="off"  id="regPassword" name="regPassword" value="" onkeyup="validate_pswd(form)" >
								</div>
							</div>
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="password2" class="col-sm-5 control-label">Confirm Password:*</label>
								<div class="col-sm-7">
									<input type="password" class="form-control" name="regConfirmPassword" id="regConfirmPassword" value="" onkeyup="validate_pswd(form)" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV -->
                               
                                    <div  align="left" height="8">
                                        <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                               
                                <!--  END ERROR DIV --> 
							<div class="form-group">
								<label for="mobno" class="col-sm-5 control-label">Mobile No.:*</label>
								<div class="col-sm-7">
									<input type="number" class="form-control"  name="regPhone1" id="regPhone1"  value="" onkeypress="return isNumberKey(event);" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV -->
                                
                                    <div  align="left" height="8">
                                        <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </div>
                                </div>
                                <!--  END ERROR DIV -->
							<div class="form-group">
								<label for="" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" type="submit" name="btnSubmit" id="btnSubmit" onclick="return register_pink(reg_form)" >Register</button>
								</div>
							</div>											
						</form>
				    </div>
				

				</div>
			</div>			
		</div>
	</div>
</div>


</div>


<!-- Modal -->
<!--div class="modal fade pw-modal" id="consult-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign in to start a Consult</h2>
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container row">
					<div class="col-md-6 left-form">
						<div class="user-title">Existing User</div>
						<form class="form-horizontal" role="form" name="consult_signin" id="consult_signin" method="POST" 
						action="<?= $site_url . "pw_login_action.php" ?>">
						<div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
							<input name="login_nxt_page" id="login_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="loginUser" class="col-sm-4 control-label">Username:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" name="username" id="username"  autocomplete="off">
								</div>
							</div>
							<div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							<div class="form-group">
								<label for="loginPass" class="col-sm-4 control-label">Password:</label>
								<div class="col-sm-8">
									<input type="password" class="form-control"name="password" id="password" autocomplete="off" >
								</div>
							</div>	
							<div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7">
									<button class="submit-button" id="login_submit" type="submit" onclick="return consult_LoginSignUp(consult_signin)">Sign In</button>
								</div>
							</div>											
						</form>
							
							<div class="form-group forgot">
								<label for="inputPassword3" class="col-sm-5 control-label"></label>
								<div class="col-sm-7"><form name="forget" id="forget" >
									Forgot your Password?<br /> <!--a href="">Click Here.</a>
									<p>Your Email-ID &nbsp; : &nbsp; <input autocomplete="off" value="" name="forgot_email" id="forgot_email" maxlength="50" /></p>
    <p><div id="forgotemailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></p>
    <div class="loginrow" ><div class="login_popupbtn">
    <button name="send"  id="for_send" class="submit-button" onclick="return forgot_password(forget);" />Submit</button></div></div>
    </form>
								</div>
							</div>	
					</div>
					<div class="col-md-6 right-form">
						<div class="newuser-title">New Users</div>
						<form class="form-horizontal" role="form"  name="reg" id="reg">
						<input name="register_nxt_page" id="register_nxt_page" type="hidden"  size="18" />
							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">Your Name:*</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" name="regname" id="regname" value="" autocomplete="off">
								</div>
							</div>
							 <!--    ERROR DIV >
                               
                                    <div  align="left" height="8">
                                        <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                                
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="age" class="col-sm-4 control-label">Age:*</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" id="age" autocomplete="off" name="regage" id="regage" onkeypress="return isNumberKey(event);" value="">
								</div>
							</div>	
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="gender" class="col-sm-4 control-label">Gender:*</label>
								<div class="col-sm-8">
									<!--input type="text" class="form-control" id="gender" autocomplete="off" >
									<select name="reggender" id="reggender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
								</div>
							</div>	
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="cardno" class="col-sm-4 control-label">Card No.:</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" autocomplete="off" onblur="return card_check(this.value)" id="regcard" name="regcard" value="" >
								</div>
							</div>	
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="eid" class="col-sm-4 control-label">Email ID:*</label>
								<div class="col-sm-8">
									<input type="email" class="form-control" id="eid" id="eid" autocomplete="off" id="regemail" name="regemail" value="" >
								</div>
							</div>	
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="password" class="col-sm-4 control-label">Password:*</label>
								<div class="col-sm-8">
									<input type="password" class="form-control" id="regPassword" name="regPassword" value="" onkeyup="validate_pswd(form)" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="password2" class="col-sm-4 control-label pass2">Confirm Password:*</label>
								<div class="col-sm-8">
									<input type="password" class="form-control"  name="regConfirmPassword" id="regConfirmPassword" value="" onkeyup="validate_pswd(form)" autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="mobno" class="col-sm-4 control-label">Mobile No.:*</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="regPhone1" id="regPhone1"  value="" onkeypress="return isNumberKey(event);"  autocomplete="off" >
								</div>
							</div>
							<!--    ERROR DIV >
                                
                                    <div  align="left" height="8">
                                        <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </div>
                             
                                <!--  END ERROR DIV >
							<div class="form-group">
								<label for="" class="col-sm-4 control-label"></label>
								<div class="col-sm-8">
									<button class="submit-button" type="submit" name="btnSubmit" id="btnSubmit" onclick="return register_pink(reg_form)">Register</button>
								</div>
							</div>											
						</form>
				    </div>
				

				</div>
			</div>			
		</div>
	</div>
</div-->


<!-- Modal -->
<!--div class="modal fade pw-modal" id="pink-btn-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">
				<h2 class="modal-title">Sign in to Start a Consult!</h2>
				<!--form class="form-horizontal" role="form" name="consult_signin" id="consult_signin" method="POST" 
						action="<?= $site_url . "pw_login_action.php" ?>">
						<div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
							<input name="login_nxt_page" id="login_nxt_page" type="hidden"  size="18" />
				<div class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></div>
				<div class="form-container">
					
						<div class="form-group">
							<label for="loginUser" class="col-sm-3 control-label">Username:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control"  name="username" id="username" autocomplete="off">
							</div>
						</div>
						<div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
						<div class="form-group">
							<label for="loginPass" class="col-sm-3 control-label">Password:</label>
							<div class="col-sm-9">
								<input type="password" class="form-control"name="password" id="password" autocomplete="off" >
							</div>
						</div>	
						<div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<button class="submit-button" id="login_submit" type="submit" onclick="return consult_LoginSignUp(consult_signin)">Sign In</button>
							</div>
						</div>											
					</form>
						<div class="form-group forgot">
							<label for="inputPassword3" class="col-sm-3 control-label"></label>
							<div class="col-sm-9"><form name="forget" id="forget" >
								Forgot your Password?<br /> <!--a href="">Click Here.</a>
								<p>Your Email-ID &nbsp; : &nbsp; <input autocomplete="off" value="" name="forgot_email" id="forgot_email" maxlength="50" /></p>
    <p><div id="forgotemailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></p>
    <div class="loginrow" ><div class="login_popupbtn">
    <button name="send"  id="for_send" class="submit-button" onclick="return forgot_password(forget);" />Submit</button></div></div>
    </form>
							</div>
						</div>	
						
				</div>
			</div>			
		</div>
	</div>
</div-->



<div class="modal fade" id="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div>Card No:998767</div>
						<div>PICK YOUR TOP UP DETAILS</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						TEST
					</div>
					<div class="col-md-4">	
						TEST
					</div>
					<div class="col-md-4">
						TEST
					</div>
					<div class="col-md-4">
						TEST
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Topup overlay modal-->
<div class="modal fade pw-modal" id="topup-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">				
				<div class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</div>
				<div class="row slot1">
					<div class="col-md-12">
						<div class="card-no">Card No:998767</div>
						<div class="overlay-title">PICK YOUR TOP UP DETAILS</div>
					</div>
				</div>
				<div class="row slot2">
					<div class="col-md-3">
						<div class="image-text-container">
							<img src="img/ourservices/pinkquery/pinkQuery_Icon11.png" />
							<div><i>Pink</i><b>Query</b></div>
						</div>
					</div>
					<div class="col-md-3">	
						<div class="image-text-container imgtxt-container2">
							<img src="img/ourservices/pinkfollowup/pink-followup_How-It-Works_Icon_pinkFollow up1.png" />
							<div><i>Pink</i><b>FollowUp</b></div>							
						</div>
						<div class="slot2-form-container">
							<form>
								<label class="radio-inline"><input type="radio" name="url" value="online"/>
									<span>Online</span>
								</label>
								<label class="radio-inline">
									<input type="radio" name="url" value="video" checked/>
									<span>Video</span>
								</label>
							</form>
						</div>	
					</div>
					<div class="col-md-3">
						<div class="image-text-container">
							<img src="img/ourservices/pinkopinion/pinkOpinion_Icon_pinkOpinion1.png" />
							<div><i>Pink</i><b>Opinion</b></div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="image-text-container">
							<img src="img/ourservices/pinkquery/pinkQuery_Icon11.png" />
							<div><i>Pink</i><b>Diagnostics</b></div>
						</div>
					</div>
				</div>
				<div class="row slot3">
					<div class="col-md-12">
						<div class="slot3-form-container">
							<form>
								<label class="radio-inline">
									<input type="radio" name="test" value="physician"/>
										<span>General Physician</span>
								</label>
								<label class="radio-inline">
									<input type="radio" name="test" value="specialist" checked/>
										<span>Specialist:</span>
								</label>
								<select class="form-control">
									<option>Diabetologist</option>
									<option></option>
								</select>
							</form>
						</div>
					</div>
				</div>
				<?php for($i=0;$i<=2;$i++){?>
					<div class="row slot4">
						<div class="col-md-4">
							<img src="img/user.png"/ class="slot4-img">
							<div class="doc-details">
								&#9734;&#9734;&#9734;&#9734;&#9734;<br>
								<span class="doc-name">Doctor Name</span><br>
								Diabetologist
							</div>
						</div>
						<div class="col-md-8">
							<form>
								<label class="radio-inline">
									<input type="radio" name="consult" value="1"/>
										<span class="consult-no">1 Consult</span><br>
										<span class="consult-cost">&#8377;250/-</span>
								</label>
								<label class="radio-inline">
									<input type="radio" name="consult" value="5" checked/>
										<span class="consult-no">5 Consults</span><br>
										<span class="consult-cost">&#8377;500/-</span>
								</label>
								<label class="radio-inline">
									<input type="radio" name="consult" value="10" checked/>
										<span class="consult-no">10 Consults</span><br>
										<span class="consult-cost">&#8377;1200/-</span>
								</label>
								<button type="button" class="addtocart-btn" data-toggle="modal" data-target="#shoppingcart-overlay">Add To Cart</button>
							</form>
						</div>
					</div>
					<hr>
				<?php } ?>				
			</div>			
		</div>
	</div>
</div>



<!-- ShoppingCart Overlay Modal -->
<div class="modal fade pw-modal" id="shoppingcart-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">				
				<div class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</div>
				<div class="row slot1">
					<div class="col-md-12">
						<div class="card-no">Card No:998767</div>
						<div class="overlay-title">SHOPPING CART</div>
					</div>
				</div>
				<div class="row slot2">
					<div class="col-md-6 left-block">
						<div class="service-image-text-container">
							<img src="img/ourservices/pinkfollowup/pink-followup_How-It-Works_Icon_pinkFollow up1.png" />
							<div>
								<span class="text"><i>pink</i><b>FollowUp</b></span><br>
								<span class="sub-text">Video</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="doc-image-text-container">
							<img src="img/user.png" />
							<div>
								<span class="text">Doctor Name</span><br>
								<span class="sub-text">Diabetologist</span>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row slot3">
					<div class="col-md-12">
						<form>
							<div>
								<span>5 Consults:</span> &nbsp;<span class="consult-cost">&#8377;500/-</span>
							</div>
							<hr>
							<div>
								<label for="pcode">Promotional Code:</label>
								<input type="text" id="pcode" /><br>
								<span class="apply">Apply Now</span>
							</div>
							<hr>
							<div>
								<span>Grand Total:</span><br>
								<span class="total-cost">&#8377;500/-</span><br>
								<button type="submit" class="payment-btn">Proceed to Payment</button>
							</div>
						</form>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>


<!-- Book online overlay -->
<div class="modal fade pw-modal" id="bookonline-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">				
				<div class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="overlay-title">
							BOOK AN APPOINTMENT AT IDEAL CHILD CLINIC HERE!
						</div>
						<div class="calender-block doctors-domain overlay-calender-block">
							<div class="calendar-container">
								<div class="arrow-1 arrow-left-1"></div>
								<div id="arrow-right" class="arrow-1 arrow-right-1"></div>
								<div class="booking-table-container">
									<table class="booking-table overlay-table">
										<thead>
											<tr>
												<?php for($i=0;$i<=29;$i++){ ?>
													<th data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><?php echo date('D',$ts);?><br><span class="small-header"><?php echo date('M j',$ts);?></span></th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
											
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts."aa";?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts."ba";?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts."ca";?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts."da";?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts."ea";?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
											
												<tr class="more-row">
													<?php for($i=0;$i<=29;$i++){ ?>
														<td class="more-value"><span>More..</span></td>
													<?php } ?>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>	
						<div class="form-container">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="doc-name" class="col-md-6 control-label">Doctor Name:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="doc-name" value="Dr. Vanitha Rao Pangal"/>
									</div>
								</div>
								<div class="form-group">
									<label for="patient-name" class="col-md-6 control-label">Patient Name:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="patient-name"/>
									</div>
								</div>
								<div class="form-group">
									<label for="age" class="col-md-6 control-label">Age:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="age"/>
									</div>
								</div>
								<div class="form-group">
									<label for="eid" class="col-md-6 control-label">Email ID:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eid"/>
									</div>
								</div>
								<div class="form-group">
									<label for="mobno" class="col-md-6 control-label">Mobile No:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="mobno"/>
									</div>
								</div>
								<div class="form-group">
									<label for="mobno" class="col-md-6 control-label">Gender:*</label>
									<div class="col-md-6">
										<label class="radio-inline">
											<input type="radio" name="gender" value="male" /> Male
										</label>
										<label class="radio-inline">
											<input type="radio" name="gender" value="female" /> Female
										</label>
									</div>
								</div>
								<div class="form-group">
									<label for="appointment-time" class="col-md-6 control-label">Appointment Time:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="appointment-time"/>
									</div>
								</div>
								<div class="form-group last-field">
									<label for="type" class="col-md-6 control-label">Type of Patient:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="type"/>
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-md-6 control-label"></label>
									<div class="col-md-6">
										<button type="submit" class="book-btn">Submit</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>


<!-- Book online overlay clinic1-->
<div class="modal fade pw-modal" id="bookonline-overlay1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-body">				
				<div class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="overlay-title">
							BOOK AN APPOINTMENT AT IDEAL CHILD CLINIC 1 HERE!
						</div>
						<div class="calender-block doctors-domain overlay-calender-block">
							<div class="calendar-container">
								<div class="arrow-1 arrow-left-1"></div>
								<div id="arrow-right" class="arrow-1 arrow-right-1"></div>
								<div class="booking-table-container">
									<table class="booking-table">
										<thead>
											<tr>
												<?php for($i=0;$i<=29;$i++){ ?>
													<th data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><?php echo date('D',$ts);?><br><span class="small-header"><?php echo date('M j',$ts);?></span></th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
											<?php for($x=0;$x<=4;$x++){?>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td  class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><span>9:15am</span></td>
													<?php }?>
												</tr>
											<?php } ?>
												<tr>
													<?php for($i=0;$i<=29;$i++){ ?>
														<td class="more-value"><span>More..</span></td>
													<?php } ?>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>	
						<div class="form-container">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="doc-name" class="col-md-6 control-label">Doctor Name:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="doc-name" value="Dr. Vanitha Rao Pangal"/>
									</div>
								</div>
								<div class="form-group">
									<label for="patient-name" class="col-md-6 control-label">Patient Name:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="patient-name"/>
									</div>
								</div>
								<div class="form-group">
									<label for="age" class="col-md-6 control-label">Age:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="age"/>
									</div>
								</div>
								<div class="form-group">
									<label for="eid" class="col-md-6 control-label">Email ID:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="eid"/>
									</div>
								</div>
								<div class="form-group">
									<label for="mobno" class="col-md-6 control-label">Mobile No:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="mobno"/>
									</div>
								</div>
								<div class="form-group">
									<label for="mobno" class="col-md-6 control-label">Gender:*</label>
									<div class="col-md-6">
										<label class="radio-inline">
											<input type="radio" name="gender" value="male" /> Male
										</label>
										<label class="radio-inline">
											<input type="radio" name="gender" value="female" /> Female
										</label>
									</div>
								</div>
								<div class="form-group">
									<label for="appointment-time" class="col-md-6 control-label">Appointment Time:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="appointment-time"/>
									</div>
								</div>
								<div class="form-group last-field">
									<label for="type" class="col-md-6 control-label">Type of Patient:*</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="type"/>
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-md-6 control-label"></label>
									<div class="col-md-6">
										<button type="submit" class="book-btn">Submit</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>



