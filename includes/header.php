
<?php include "site_config.php";
$site_url   = "http://localhost:81/pinkwhale_newest/";
include 'analyticstracking.php';
error_reporting(E_ALL);
?>
<link rel="shortcut icon" href="<?php echo $site_url;?>favicon.ico" type="image/x-icon" />
 <link rel="icon" href="<?php echo $site_url;?>favicon.ico" type="image/x-icon">
<header class="main-menu">
	<div class="header content-area">
		<div class="menu-wrapper">
			<nav class="nav-main-menu">
				<ul>
					<?php if (basename($_SERVER['PHP_SELF']) != "doctorsdomain.php") {?>
					<li><a href="<?php echo $site_url;?>index.php" target="_self" class="contents center-menu menu-patients"><span>Patients</span></a></li>
					<li><a href="<?php echo $site_url;?>home-doctors.php" target="_self" class="contents center-menu menu-doctors"><span>Doctors</span></a></li>
					<li><a href="<?php echo $site_url;?>home-corporates.php" target="_self" class="contents center-menu menu-corporates"><span class="no-border">Corporates</span></a></li>
					<?php } ?>
					<li class="menu-blog-li">
						<a href="<?php echo $site_url;?>pinkblog" target="_self" class="contents side-menu menu-blog">
							<span>Blog</span>
						</a>
					</li>
					<li class="menu-aboutus-li">
						<a href="<?php echo $site_url;?>aboutus.php" target="_self" class="contents side-menu menu-aboutus">
							<span>About Us</span>
						</a>
					</li>
					<li class="menu-talktous-li">
						<a href="talktous.php" target="_self" class="contents side-menu menu-talktous">
							<span>Contact Us</span>
						</a>
					</li>
					<li class="menu-topup-li">
						<a href="#" target="_self" class="contents side-menu menu-topup no-border">
							<span class="no-border">Top Up Card</span>
						</a>
						<div class="topup-dropdown-container">							
							<div class="title">TOP UP YOUR CARD HERE!</div>
							<div class="form-container">
								<form class="form-horizontal" name="TopUPlogin" id="TopUPlogin" action="card_login.php" method="POST">
									<div class="form-group">
										<label for="cardno" class="col-md-5">Card No.:</label>
										<div class="col-md-7 field-container">
											<input type="text" id="card" name="card" class="form-control onkeypress=" onkeypress="return isNumberKey(event);" autocomplete="off" title=""  size="15" maxlength="6">
										</div>
									</div>
									<div id="topuplog_errordiv" align="center" value="error" class="error" 
									style="color:#ff0000;font-family:Arial;font-size:13px;"></div>
									
									<div class="form-group">
										<label for="" class="col-md-5"></label>
										<div class="col-md-7 topup-btn-container">
										<!-- 	<button class="topup-btn" type="button" data-toggle="modal" data-target="#topup-overlay">Top Up</button>
										 -->
										<button class="topup-btn" id="topupbtn" type="button" data-toggle="modal" onclick="Topup_login(TopUPlogin)">Top Up</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</li>
					<div class="clearfix"></div>
				</ul>
			</nav>
			<div class="clearfix"></div>
		</div>
	</div>
</header>
<!-- body contents ending in end.php -->
<div class="body-contents">