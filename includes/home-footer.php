<div class="clearfix"></div>

<div class="row max-960 home-footer-container hidden-xs">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="row block first">
			<div class=" image-container">
				<img src="img/Patients_Image_Bottom_1.png" class="first-image">
			</div>
			<div class="content">
				Very user-friendly
			</div>
		</div>		
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="row block second">
			<div class="image-container">
				<img class="second" src="img/Patients_Image_Bottom_2.png" >
			</div>
			<div class=" content">
				Prompt notifications<br> via SMS or Email
			</div>
		</div>		
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="row block no-border">
			<div class=" image-container">
				<img src="img/Patients_Image_Bottom_3.png" >
			</div>
			<div class="content">
				Manage Family Health<br> on Cloud
			</div>
		</div>		
	</div>
</div>