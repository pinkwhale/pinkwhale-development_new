<?php 

	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<?php

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<!-- header.......--><?php
include 'header.php'; 
?>

<?php
require_once('calendar/classes/tc_calendar.php');
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php"><font color="#ea0977"><strong>My Account</strong></font></a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- header.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- header.......-->
</td>

<td width="748" valign="top" class="s90phrcontent">
<?php
	$qry= "SELECT * FROM `user_health_info` where `card_no`='$user_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
			$date1=$result['user_dob'];
?>
<?php include("name_card_no.php");?>

<table>
	<tr>
		<td height='15px'></td>
	</tr>
</table>
<form action="actions/edit_user_health_info.php" method="post" name="user_health_info" id="user_health_info">
<input type="hidden" name="user_edit_id_save" value="<?php echo $user_id ; ?>" />
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
<tr><th>Edit Health Information</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="568" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
<tr><td width="87"><b>Date Of Birth: </b></td>
<td width="148">
<?php
	  $myCalendar = new tc_calendar("date5", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
<td width="170"><strong>Medical Device/Implants:</strong></td>
<td width="163"><input type="text" name="medical_decice_implants" id="medical_decice_implants" value="<?php echo $result['medical_device']; ?>"/></td>
</tr>
<tr><td><b>Gender: </b></td><td><select  name="user_gender" id="user_gender" > 
    	<option value="">-Select-</option>
        <option value="male">Male</option>
        <option value="Female">Femail</option>
        </select></td>
             <script type="text/javascript">
		type='<?php echo $result['user_gender']; ?>';
		document.getElementById('user_gender').value=type;
		document.getElementById('user_gender').selected=true;
	</script>
<td><strong>Blood Group:</strong> </td><td>
<select  name="blood_group" id="blood_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select>
</td>
 <script type="text/javascript">
		type='<?php echo $result['user_blood_group']; ?>';
		document.getElementById('blood_group').value=type;
		document.getElementById('blood_group').selected=true;
	</script>
    </tr>
<tr><td><b>Height: </b></td><td><input type="text" name="height" id="height" size="3" value="<?php echo $result['user_height']; ?>"/>
cm</td>
<td><strong>Blood Sugar:</strong> </td><td><input type="text" name="blood_sugar" id="blood_sugar"  size="5" value="<?php echo $result['user_blood_sugar']; ?>"/>
  mg/dL</td>
</tr>
<tr><td><b>Weight: </b></td><td><input type="text" name="weight" id="weight" size="3" value="<?php echo $result['user_weight']; ?>"/>
Kg</td>
<td><strong>Cholestrol:</strong> </td><td><input type="text" name="cholestrol" id="cholestrol" size="5" value="<?php echo $result['user_cholostrol']; ?>"/>
  mmol/L</td>
</tr>
<tr><td><b>Marital Status: </b></td><td><select  name="user_merit_status" id="user_merit_status" > 
    	<option value="">-Select-</option>
        <option value="Single">Single</option>
        <option value="Married">Married</option>
        </select></td>
          <script type="text/javascript">
		type='<?php echo $result['user_merital_status']; ?>';
		document.getElementById('user_merit_status').value=type;
		document.getElementById('user_merit_status').selected=true;
	</script>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="blood_pressur" id="blood_pressur" size="5" value="<?php echo $result['user_blood_pressure']; ?>"/>
  mmHg</td>
</tr></table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
<tr><th>Father's Health Information</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="563" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
<tr>
<td width="101"><b>Name: </b></td>
<td width="163"><input type="text" name="father_name" id="father_name"  value="<?php echo $result['father_name'] ?>"/></td>
<td width="145"><strong>Medical Device/Implants:</strong></td>
<td width="154"><input type="text" name="father_medical_device" id="father_medical_device"  value="<?php echo $result['father_medicaldevice'] ?>"/></td>
</tr>
<tr>
<td width="101"><b>Age: </b></td>
<td width="163"><input type="text" name="father_age" id="father_age" size="5" value="<?php echo $result['user_father_age'] ?>"/></td>
<td><strong>Blood Group:</strong> </td>
<td>
<select  name="father_boold_group" id="father_boold_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select>
</td>

 <script type="text/javascript">
		type='<?php echo $result['father_blood_group']; ?>';
		document.getElementById('father_boold_group').value=type;
		document.getElementById('father_boold_group').selected=true;
	</script>

<tr>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="father_boold_pressure" id="father_boold_pressure" size="5" value="<?php echo $result['father_blood_pressure'] ?>"/>
mmHg</td>
<td><strong>Blood Sugar:</strong> </td>
<td><input type="text" name="father_blood_sugar" id="father_blood_sugar"  size="5" value="<?php echo $result['father_blood_suger'] ?>"/>
  mg/dL</td>
</tr>
<tr><td><b>Major Illness: </b></td><td><input type="text" name="father_major_illness" id="father_major_illness"  value="<?php echo $result['user_father_illness'] ?>"/></td>
<td><strong>Cholestrol:</strong> </td>
<td><input type="text" name="father_cholesrol" id="father_cholesrol" size="5" value="<?php echo $result['father_cholestrol'] ?>"/>
  mmol/L</td>
</tr>
<tr><td colspan="0"><b>Remarks: </b></td><td colspan="3"><textarea name="father_remarks" id="father_remarks"  class="registetextbox1">
<?php echo $result['user_father_remarks'] ?>
</textarea></td>
</tr>
</table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:20px">
<tr><th>Mother's Health Information</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="556"  border="0" align="center" cellpadding="0" cellspacing="0" class="s90dbdtbls_drdetailstbl">
<tr>
<td width="97"><b>Name: </b></td>
<td width="163"><input type="text" name="mother_name" id="mother_name"  value="<?php echo $result['mother_name'] ?>"/></td>
<td width="145"><strong>Medical Device/Implants:</strong></td>
<td width="151"><input type="text" name="mother_medical_device" id="mother_medical_device"  value="<?php echo $result['mother_medical_device'] ?>"/></td>
</tr>
<tr>
<td width="97"><b>Age: </b></td>
<td width="163"><input type="text" name="mother_age" id="mother_age" size="5" value="<?php echo $result['user_mother_age'] ?>"/></td>
<td><strong>Blood Group:</strong> </td>
<td>
<select  name="mother_boold_group" id="mother_boold_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select>
</td>
 <script type="text/javascript">
		type='<?php echo $result['mother_blood_group']; ?>';
		document.getElementById('mother_boold_group').value=type;
		document.getElementById('mother_boold_group').selected=true;
	</script>
</tr>
<tr>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="mother_boold_pressure" id="mother_boold_pressure" size="5" value="<?php echo $result['mother_blood_pressure'] ?>"/> mmHg</td>
<td><strong>Blood Sugar:</strong> </td>
<td><input type="text" name="mother_blood_sugar" id="mother_blood_sugar"  size="5" value="<?php echo $result['mother_blood_sugar'] ?>"/>
  mg/dL</td>
</tr>
<tr><td><b>Major Illness: </b></td><td><input type="text" name="mother_major_illness" id="mother_major_illness" value="<?php echo $result['user_mother_illness'] ?>"/></td>
<td><strong>Cholestrol:</strong> </td>
<td><input type="text" name="mother_cholesrol" id="mother_cholesrol"  size="5" value="<?php echo $result['mother_cholestrol'] ?>"/>
  mmol/L</td>
</tr>
<tr><td colspan="0"><b>Remarks: </b></td><td colspan="3"><textarea name="mother_remarks" id="mother_remarks" class="registetextbox1">
<?php echo $result['user_mother_remarks'] ?>
</textarea></td>
</tr>
</table>
</td></tr></table>


<?php
}
?>
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:10px">
    <tr>
        <td width="285" align="right" style="margin-right:10px;">
        	<input type="submit" name="save_user_prof" value="Save" id="save_user_prof"/>
        </td>
        <td width="10">&nbsp;</td>
        <td width="285" align="left" style="margin-left:10px;">
            <a href="phr_healthinformation.php"><input type="button" name="cancel" value="Cancel"/></a>
        </td>
    </tr>
</table>
</form>

</table>
</td></tr>

</table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
