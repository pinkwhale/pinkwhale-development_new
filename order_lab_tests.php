<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<!-- header.......-->
<?php include 'header.php'; ?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu5').style.fontWeight= 'bold'</script>



<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="715" style="border-right:1px solid #4d4d4d" valign="top" class="s90ltcontent">
<h1>Order Lab Tests From Home Now</h1>

<p> Did you know that smart health consumers bring the lab to their doorstep? Here's how you can be smart: Order the required tests online and the lab assistant will arrive at your home at a time that is convenient to you. 
Get the tests done conveniently, easily, discreetly, and at a special discounted member price. </p>
<p>We have partnered with industry leading laboratory test providers, to bring lab tests right to your doorstep. So, save on time, and money.</p>

<p> Take a half-day off from work, drive to the lab braving the traffic, wait for your turn in the lab.</p> 
<p> Or, become a smart healh consumer! <strong>Order your lab tests online now! </strong></p>

<table width="480" border="0" cellspacing="1" cellpadding="5" align="left" class="s90labtests">
  <tr>
    <th width="40">S No</th>
    <th width="284">Test</th>
<!--    <th width="80">Details</th>-->
    <th width="100">Order</th>
  </tr>
  <tr>
    <td>1</td>
    <td>17 OH PROGESTERONE (17OH-PROG)</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
  <tr>
    <td>2</td>
    <td>25-OH VITAMIN D3</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
  <tr>
    <td>3</td>
    <td>Lipid Profile</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
    <tr>
    <td>4</td>
    <td>Liver Profile</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
    <tr>
    <td>5</td>
    <td>Thyroid Profile</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
    <tr>
    <td>6</td>
    <td>Renal Profile</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
  <tr>
    <td>7</td>
    <td>Complete Hemogram</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>
    <tr>
    <td>8</td>
    <td>Complete Urinogram</td>
<!--    <td><a href="#">More Details</a></td>-->
    <td><img src="images/order_test.png" width="86" height="19" /></td>
  </tr>

</table>

</td>
<td width="254" valign="top"><img src="images/lt_banner.jpg" width="254"/></td></tr>

<tr><td colspan="2"><div id="s90specialtfaqs">
<h3>FAQ's:</h3>

<ol>
<li><strong>Why should I use pinkWhale's diagnostic lab test  service?<br />
Ans.</strong>
<strong>1.</strong> It's fast (You can order it online. No need to  travel to a lab or wait at a lab).<br />
<strong>2.</strong>	It's convenient (You lab tests are done  from your doorstep).<br />
<strong>3.</strong>	It's affordable (pinkWhale members get a special  discounted price on all tests).</li>

<li><strong>How are appointments taken?<br />
Ans.</strong> After booking it online, Thyrocare will call you  and make an appointment with you.</li>

<li><strong>Where are the tests done?<br />
Ans.</strong> All samples are tested in Thyrocare laboratory.</li>

<li><strong>What samples are required?<br />
Ans.</strong> The tests, and test preparation information are  provided in the tests ordering page.</li>

<li><strong>Where will I get the results?<br />
Ans.</strong> The test report will be hand delivered to you. The  electronic copy of the report can also be provided.</li>

<li><strong>How long will it take to get the results? <br />
Ans.</strong> Usually within 48 hrs.</li>

<li><strong>How much does it cost?<br />
Ans.</strong> Cost is listed on the lab tests page.</li>

<li><strong>How do I pay?<br />
Ans.</strong> You can pay by credit card on the website. We  accept all credit cards except Mastercard.</li>

<li><strong>Can I pay by cash or cheque?<br />
Ans.</strong> No. We only accept credit card payments for lab  tests.</li>

<li><strong>What are shipping &amp; handling charges?<br />
Ans.</strong> Shipping &amp; Handling charges are for home  pickup of samples and home delivery of results. This is per order and can  include any number of samples.</li>

<li><strong>Can I visit your facility to provide samples?<br />
Ans.</strong> No.   We  take orders online, and samples are collected at your doorstep.</li>

<li><strong>Can I cancel my order?<br />
Ans.</strong> No.  If  you are travelling or are busy, you can re-schedule the appointment at your  convenience.</li>

<li><strong>Are there any age limits?<br />
Ans.</strong> Yes, you must be 18 yrs or above to purchase  services.</li>

<li><strong>Can I get a medical opinion on the test results?<br />
Ans.</strong> Yes. Please contact us.</li>

<li><strong>Is this service available in my city/town?<br />
Ans.</strong> This service is available in over 600 cities and  towns in India. The towns and cities are listed in the order page.</li>

<li><strong>Will you keep the test results on record?<br />
Ans.</strong> No. The test results will be delivered to  you.  You will have the option  to upload the test result reports onto your confidential  Personal Health Record (PHR) on pinkWhale website.</li>

<li><strong>Do I need to see the doctor to get tested?<br />
Ans.</strong> No. Doctor's prescription is not required.</li>

<li><strong>Are the results confidential?<br />
Ans.</strong> Absolutely. We respect your privacy and maintain  confidentiality. You are the only one to receive the results unless you  specifically direct us otherwise.</li>

<li><strong>Can I gift a wellness test for my family member?<br />
Ans.</strong> Yes.</li>

</ol>
</div>
</td></tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>