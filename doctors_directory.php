<?php
session_start();
include "actions/encdec.php";
include "modify_doctorname.php";
include "db_connect.php";
include 'site_config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


  function filterdelay(){
      setTimeout(filterby, 500);
  }
  
  function filterby(){
      var spe = document.getElementById("fil_special").value;
      var city = document.getElementById("fil_city").value;
      
      if(spe==""){
          spe = "specialist-doctors";
      }
      
      if(city == ""){
          city = "all";
      }
      
      spe = encodeURI(spe);
      
      city = encodeURI(city); 
      
      var siteprefix = "<?php echo $sitePrefix ?>";
      
      var url = siteprefix+"/doctors/"+spe+"/"+city;
           
      window.location.assign(url);
      
  }

</script>


<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
<!-- header.......-->
<?php include 'main_header.php'; ?>
<!-- header.......-->
<!--<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold';</script> -->

<div class="section-title  full-bg  " style="background-image:url(slider/slide_bg1.jpg)">
<div class="container"><h2 class="animated bounceInDown">Our Doctors</h2></div>    
</div>
      
<div class="container"><section>   
<div class="row" >
<div class="span12 clearfix" >
<div class="inner">

<h1></h1><br>
    <form name="filterby" id="filterby">
    <div class="show-grid clearfix">
        <div class="span12" style="width:100%;text-align:right">              
                <select name="fil_special" id="fil_special" onchange="filterdelay()">
                    <option value="" selected="selected" disabled> - Filter by Specialization - </option>
                    <?php
                        $spe_qry = "select doc_specialities from pw_doctors where blocked<>'Y' and doc_specialities<>'' group by doc_specialities";
                        $spe_result = mysql_query($spe_qry);
                        while($spe_data = mysql_fetch_array($spe_result)){
                            $doctor_spe = $spe_data['doc_specialities'];
                            $doctor_spe_encode = encode_city($doctor_spe);
                            echo "<option value='$doctor_spe_encode'>$doctor_spe</option>";
                        }
                    ?>
                </select>              
                <select name="fil_city" id="fil_city" onchange="filterdelay()" >
                    <option value="" selected="selected" disabled> - Filter by City - </option>
                    <?php
                        $spe_qry = "select city from pw_doctors where blocked<>'Y' and doc_specialities<>'' group by city";
                        $spe_result = mysql_query($spe_qry);
                        while($spe_data = mysql_fetch_array($spe_result)){
                            $doctor_city = $spe_data['city'];
                            $doctor_city_encode = encode_city($doctor_city);
                            echo "<option value='$doctor_city_encode'>$doctor_city</option>";
                        }
                    ?>
                </select>
        </div>
    </div>
    </form>
    
    <?php 

    $doctor_spe = $_REQUEST['specialist'];
    $doctor_city = $_REQUEST['city'];
    
    ?>
    <script>
        document.getElementById("fil_special").value = "<?php echo $doctor_spe; ?>";
        document.getElementById("fil_special").selected = true;
        document.getElementById("fil_city").value = "<?php echo $doctor_city; ?>";
        document.getElementById("fil_city").selected = true;
        
    </script>
    <?php
    
    $extend = "";
    
    if($doctor_spe!=""){
            
        $doctor_spe_decode = str_replace("-"," ",$doctor_spe);

        $doctor_spe_decode = mysql_escape_string($doctor_spe_decode);  
        
        if($doctor_spe == "specialist-doctors" ){
            $extend = "";
        }else{
            $extend = " and doc_specialities='$doctor_spe_decode' ";
        }
            
    }
    
    if($doctor_city!=""){
            
        $doctor_city_deocode = str_replace("-"," ",$doctor_city);

        $doctor_city_deocode = mysql_escape_string($doctor_city_deocode);   
            
         if($doctor_city == "all"){
            $extend .= "";
        }else{
            $extend .= " and city='$doctor_city_deocode' ";
        }
        
    }
    
    

    //............Expert table open 

    ?>
    <table width="100%" border="0" cellspacing="5" cellpadding="5" align="center">
              <?php
                
                $qry="SELECT * FROM `pw_doctors` WHERE  blocked !='Y' $extend"; //group by`sp_doctors_photo`                
                if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
               
              ?>			
             <tr>
              <?php	
            
			 $count=0;
			 $i=1;
			
                            while($qry_result=mysql_fetch_array($qry_rslt))
                             {
			 		if($i>=6)
					{
						$i=1;
						?> 
						</tr>
						<tr>
						<?php 
								           
					}
					 $i++;
			 	   $doc_name=$qry_result['doc_name'];  
                                   $doc_spe = $qry_result['doc_specialities'];
                                   $doc_spe = encode_city($doc_spe);
                                   $city = $qry_result['city'];  
                                   $city = encode_city($city);
                                   $doc_image = $qry_result['doc_photo'];
                                   $doc_gender = $qry_result['doc_gender'];
                                   
                                   if($doc_image=="" && $doc_gender=="Female"){
                                        $doc_image = $sitePrefix."/images/doctors_images/female.png";
                                    }else if($doc_image=="" && $doc_gender=="Male"){
                                        $doc_image = $sitePrefix."/images/doctors_images/male.png";
                                    }else if($doc_image=="" ){
                                        $doc_image = $sitePrefix."/images/doctors_images/male.png";
                                    }else{
                                        $doc_image = $sitePrefix."/".$doc_image;
                                    }
                                   
                                   $qry1="SELECT * FROM `pw_doctors` WHERE blocked !='Y' && doc_name='$doc_name'";
				   $qry_rslt1=mysql_query($qry1);
				   $rownum=mysql_num_rows($qry_rslt1);
				  
				    if($rownum>1)
                                    {
                                            $count++;

                                            $doc_name_r = modifyname($doc_name);

                                    }
                                    else                                            
                                    {

                                        $doc_name_r = modifyname($doc_name);


                                    }
					 
                   ?>
		   
															
                                 <td width="20%" height="20%" class="doclist_imgs" valign="baseline">
                                 <a href="<?php echo $sitePrefix; ?>/doctors/<?php echo $doc_spe; ?>/<?php echo $city; ?>/<?php echo $doc_name_r; ?>"><img src="<?php echo $doc_image; ?>" Style="cursor: pointer;max-height: 110px; max-width: 110px; width: 62%;" ></a>
                                 <br />	
                                 <?php echo $qry_result['doc_name']; ?> <br /><strong>(<?php echo $qry_result['doc_specialities']; ?>)</strong>
                                 </td>

                 <?php }?>
		
                </tr>	
            </table>
<?php //............Expert table close	 ?>

</div><!-- /inner -->
</div><!-- /span12 -->			
</div><!-- /row-->
</section></div> <!-- /container --> 

<!-- footer -->
<script>

var site_config = "<?php echo $sitePrefix."/"; ?>";


function consult_LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
        document.getElementById("log_errordiv").innerHTML = "";        
	document.getElementById("username_errordiv").innerHTML = "";
	document.getElementById("password_errordiv").innerHTML = "";
	
	if (form.username.value=='')
	{
            
   		document.getElementById("username_errordiv").innerHTML = "UserName cannot be blank";
                usernameValidated = false;
	}
	if (form.password.value=='')
	{
            
		document.getElementById("password_errordiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{   
            
                           
                // -------------------ajax start--------------------- //
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                
                var username = encodeURI(form.username.value);
                var password = encodeURI(form.password.value);
                
                var paramString = 'username='+username+'&password='+password;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"login_popup.php",
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    loading("Loading",1);
                                    document.getElementById('consult_signin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("log_errordiv").innerHTML = "Invalid login Details";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;                                                                     
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{
                
		return false;
	}
	
}


function forgot_password(form)
{       
	Validated = true;
        
        document.getElementById("forgotemailErrDiv").innerHTML = ""; 
        
	if (form.forgot_email.value=='')
	{
            
   		document.getElementById("forgotemailErrDiv").innerHTML = "Email cannot be blank";
                Validated = false;
	}
        
	if(Validated)
	{   
            
            loading("Checking...",1);
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("for_send").value="Validating....";
            
                // -------------------ajax start--------------------- //
                var paramString = 'email='+form.forgot_email.value;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"actions/forgot_Passwd.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    unloading();
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully sent password to your Email-id</div>";
                                    
                                    
                                }else{       
                                    unloading();
                                    document.getElementById("forgotemailErrDiv").innerHTML = "Invalid Email-ID";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}



function register_pink(form)
{
        
	Validated = true;
        
        document.getElementById("nameErrDiv").innerHTML = "";
        document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
        document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = ""; 
        
        
        var name = form.regname.value;
        var age = form.regage.value;
        var gender = form.reggender.value;
        var card = form.regcard.value;
        var email = form.regemail.value;
        var password = form.regPassword.value;
        var confirmpassword = form.regConfirmPassword.value;
        var mobile = form.regPhone1.value;
        
        
        //alert(form.forgot_email.value);
	if ( name =='' )
	{
            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
        if ( age =='' )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
                Validated = false;
	}else if ( age < 18 )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
                Validated = false;
	}
        
        if ( gender =='' )
	{
            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
                Validated = false;
	}
        if ( email =='' )
	{
            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
                Validated = false;
	}
        
        if ( password =='' )
	{
            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
                Validated = false;
	}
        
        if ( password != confirmpassword )
	{
            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch";
                Validated = false;
	}
        
        if( mobile == "" )
	{
            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
                Validated = false;
	}
        
        
	if(Validated)
	{   
            
            loading("Registering...",1);
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("btnSubmit").value = "Processing....";
            
                // -------------------ajax start--------------------- //
                
                name = encodeURI(name);
                age = encodeURI(age);
                gender = encodeURI(gender);
                card = encodeURI(card);
                email = encodeURI(email);
                password = encodeURI(password);
                mobile = encodeURI(mobile);
                
                var paramString = 'name='+name+'&age='+age+'&gender='+gender+'&email='+email+'&password='+password+'&mobile='+mobile+'regcard='+card;
                
                $.ajax({  
                        type: "POST",  
                        url: site_config+"actions/regpink.php",  
                        data: paramString,  
                        success: function(response) {  
                            //alert(response);
                                if(response==true){
                                  
                                    unloading();
                                  
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully Register<br />Please check your mail to complete registration.</div>";
                                    
                                    
                                    
                                }else{       
                                    
                                    unloading();
                                    
                                    document.getElementById("emailErrDiv").innerHTML=response;
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                 
                                    
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}

function card_check(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("cardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: site_config+"check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        //alert(response);
                            if(response==true){
                                document.getElementById("cardErrDiv").innerHTML= "";
                                return false;
                            }else{
                                document.getElementById("regcard").value= "";
                                document.getElementById("cardErrDiv").innerHTML= "Invalid card";
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //
        
}

function Topup_login(form)
{
        
	validate = true;
        
	
	document.getElementById("topuplog_errordiv").innerHTML= "";
	
        
                
	if (form.card.value=='')
	{
                
   		document.getElementById("topuplog_errordiv").innerHTML = "Card No. cannot be blank";
                validate = false;
	}
        
	
	if(validate)
	{
            document.getElementById("topupbtn").value="loading";
            document.getElementById("topupbtn").disabled=true;
            
            // -------------------ajax start--------------------- //
                
                var paramString = 'card='+form.card.value;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"card_login_check.php",  
                        data: paramString,  
                        success: function(response) {         
                                if(response==true){
                                    
                                    document.getElementById('TopUPlogin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("topuplog_errordiv").innerHTML = "Invalid card No.";
                                    document.getElementById("topupbtn").value="Top Up";
                                    document.getElementById("topupbtn").disabled=false; 
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
		
                
	}
		
	else
	{
		return false;
	}
	
}




function isNumberKey(evt)
{
	
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}


function validate_consult(type){
    
    document.getElementById("login_nxt_page").value=type;
    document.getElementById("register_nxt_page").value=type;
        
    setTimeout(login_callpopup(true,0),200);
    
}
</script>
<?php
include 'footer.php'; ?>

</body></html>

