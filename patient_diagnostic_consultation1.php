<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login']!='user')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckfinder/ckfinder.js"></script>
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- menu.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- Menu.......-->
</td>

<td width="748" valign="top" class="s90phrcontent">
<?php
	$mail_ref_id=$_POST['cnsltion_id'];
	
	$qry= "SELECT `doctor_id` FROM `pw_diagnostic_consultation_emails` WHERE `consultation_id`='$mail_ref_id'";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))	
	{
		$spe_doctor_id=$result['doctor_id'];
	}
	
	$qryUser= "SELECT `user_name`, `user_email` FROM `user_details` WHERE `user_id`='$user_id'";
	$userData = mysql_query($qryUser);
	while($user = mysql_fetch_array($userData))	
	{
		$user_name	= $user['user_name'];
		$user_email	= $user['user_email'];
	}
	
	$qry1= "SELECT * FROM `pw_doctors` WHERE `doc_id`='$spe_doctor_id'";
	$qry_rslt1 = mysql_query($qry1);
	while($result1 = mysql_fetch_array($qry_rslt1))	
	{
		$doc_photo=$result1['doc_photo'] ;
		$cslt_doctor_email	= $result1['doc_email_id'];
		$cslt_doctor_phone	= $result1['doc_mobile_no'];
?>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls">
<tr><th>Diagnostic Consultation</th></tr>

<tr><td class="s90dbdtbls_drinfo">
    <table width="570" border="0" cellspacing="5" cellpadding="0">
        <tr><td width="80" align="center"><img src="<?php echo $result1['doc_photo'] ?>" width="72" height="72" /></td>
        <td width="485"><strong><?php echo $result1['doc_name'] ?></strong>- 
        	<?php echo $result1['doc_qualification'] ?>
        	(<?php echo $result1['doc_specialities'] ?>) <br />
       		<?php echo $result1['doc_specialist_for'] ?><br />
        	<?php echo $result1['address_line2'] ?>
        </td></tr>
    </table>
</td></tr>
<?php } 
$qry2= "SELECT * FROM pw_diagnostic_consultation_emails where consultation_id='$mail_ref_id'";
	$qry_rslt2 = mysql_query($qry2);
	while($result2= mysql_fetch_array($qry_rslt2))	
	{
		
		$con_num=$result2['consultation_id'];
		$con_pw_card=$result2['pw_card_id'];
		$con_doc_id=$result2['doctor_id'];
		$con_doc_name=$result2['doctor_name'];
		$con_ptnt_name=$result2['patient_name'];
		if($result2['complaint'] <> "") $con_complaint = $result2['complaint'];
		$gdate1=strtotime($result2['entered_date']);
		$final_date1=date("d M Y", $gdate1);
		
		if($result2['doctor_reply']== '')
		{
 ?>
<tr><td>
		
<table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
<?php $qry3= "SELECT user_photo FROM `user_details` WHERE `user_id`='$user_id'";
	$qry_rslt3 = mysql_query($qry3);
	while($result3= mysql_fetch_array($qry_rslt3))
	
		{
			$user_photo=$result3['user_photo'];
		}
	
	if($user_photo=="") {
?>
<tr><td width="80" align="center" valign="top" rowspan="2"><img src="images/avatar.png" width="72"  height="72" style="margin-top:16px"/><br /><br />
<?php }
		else {
		?>
   <tr><td width="80" rowspan="2" align="center" valign="top"><img src="<?php echo $user_photo;?>" width="72"  height="72" style="margin-top:16px"/><br /><br />     
        
	<?php		
		}
?>

	
</td>
<td width="390" valign="top"><p><strong><?php echo $result2['patient_name'] ?></strong></p>

</td>
<td width="90" valign="top"><p><?php echo $final_date1; ?></p></td>
   </tr>
   <tr>
     <td colspan="2" valign="top">
     	<p><?php echo $result2['cnsltion_mail_details'] ?></p>
        <p>
		<?php
        $qry2= "SELECT * FROM pw_diagnostic_consultation_emails as diagnostic, user_reports as user, pw_user_diagnostics_reports as diagreport
WHERE diagreport.user_report_id=user.report_id and diagnostic.consultation_id=diagreport.diagnostic_consultation_email_id and diagnostic.consultation_id='$mail_ref_id' group by user.report_file asc  ";
	$qry_rslt2 = mysql_query($qry2);
	
	while($result3 = mysql_fetch_array($qry_rslt2))	
	{ 
	$report_name=$result3['report_name'];
	$uploaded_records = $result3['report_file'];
	
           $tmp_file = explode("|||", $uploaded_records);                                                                        
               if ($uploaded_records != '') {
                 if($tmp_file==1){
                        $download_file = "download_diag_doc_report.php?id=$mail_id&type=1";
                 }else{       
                        $download_file = $uploaded_records;
                 }
				 $file_name=basename("$uploaded_records");
				
				 $filename = array_pop(explode('-report-',  $file_name));
				 
	?>
    	<a href="<?php echo $download_file; ?>">
        <img src="images/attach.png" width="16" height="16" align="absmiddle"/> <?php echo $filename; ?> </a>
    <?php }?>
    </p>
	
     </td>
   </tr>
</table>
<?php 
	}
    if ($result2['doctor_reply'] !='')
	{
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
    <tr>
        <td width="80" rowspan="2" align="center" valign="top">
        	<img src="<?php echo $doc_photo ; ?>" width="72"  height="72" style="margin-top:16px" />
        </td>
        <td width="390" valign="top"><p><strong><?php echo $result2['doctor_name'] ?></strong></p>
            
        </td>
        <td width="90" valign="top"><p><?php echo $final_date1; ?></p></td>
    </tr>
    <tr>
      <td colspan="2" valign="top"><p><?php echo $result2['doctor_reply'] ; ?> </p></td>
    </tr>
</table>

<?php }?>


<table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
    <tr>
        <td width="80" rowspan="2" align="center" valign="top">
        	<img src="<?php echo $doc_photo ; ?>" width="72"  height="72" style="margin-top:16px" />
        </td>
        <td width="390" valign="top"><p><strong><?php echo $result2['doctor_name'] ?></strong></p>
            
        </td>
        <td width="90" valign="top"><p><?php echo $final_date1; ?></p></td>
    </tr>
    <tr>
      <td colspan="2" valign="top"><p><?php echo $result2['doctor_reply'] ; ?> </p></td>
    </tr>
</table>
<?php 
	}}
?>
<!-- $$$$$$$$$$$$$$$$$$$$          text editor            $$$$$$$$$$$$$$$$$$$$$$$$ -->
<form action="actions/update-diag-patient-email-consultation.php" method="post">
    <table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
        <tr><td valign="top" colspan="2">
            <textarea id="body" name="body" cols="60" rows="10">
            </textarea>
              <script type="text/javascript">
				var editor = CKEDITOR.replace('body');
				CKFinder.setupCKEditor( editor, 'ckfinder/' );
				ste.init();
            </script>
            <input type="hidden" name="con_num" value="<?php echo $con_num ;?>" />
            <input type="hidden" name="cnslt_doc_name" value="<?php echo $con_doc_name ;?>" />
            <input type="hidden" name="cnslt_doc_id" value="<?php echo $con_doc_id ;?>" />
            <input type="hidden" name="pink_card_id" value="<?php echo $con_pw_card ;?>" />
            <input type="hidden" name="user_name" value="<?php echo $con_ptnt_name ;?>" />
            <input type="hidden" name="complaint" value="<?php echo $con_complaint ;?>" />
            <input type="hidden" name="cslt_doctor_email_id" value="<?php echo $cslt_doctor_email ;?>" />
			<input type="hidden" name="user_email_id" value="<?php echo $user_email ;?>" />
            <input type="hidden" name="cslt_doctor_phoneno" value="<?php echo $cslt_doctor_phone ;?>" />

        </td></tr>
		
        <tr>
        <td width="19%"><div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Share Report :</div></td>
        <td>
            <select name="sele_report[]" id="sele_report" multiple size="10">
                <option value="" selected>- Select Report -</option>
                <?PHP	
				
									
                $qry   = "SELECT `report_name`,`report_file` FROM `user_reports` WHERE `pwcard_no`='$user_id'";
				
                $qry_rslt = mysql_query($qry);
                while($result = mysql_fetch_array($qry_rslt))
                {
				$uploaded_records = $result['report_file'];
				 $file_name=basename("$uploaded_records");
				 
				 $filename2 = array_pop(explode('-report-',  $file_name));
                ?>
                  <option value="<?php echo $result['report_file']; ?>"><?php echo $filename2; ?></option>
                <?PHP }?>
            </select>
        </td></tr>
        <tr><td colspan="2">
            <input type="submit" value="Submit" onclick="ste.submit();">
        </td></tr>
    </table>
</form>
<!-- $$$$$$$$$$$$$$$$$$$$          text editor            $$$$$$$$$$$$$$$$$$$$$$$$ --></td></tr></table>

</td></tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>

