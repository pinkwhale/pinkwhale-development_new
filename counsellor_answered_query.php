<?php
session_start();
error_reporting(E_PARSE);
if($_SESSION['login']!='Counsellor')
	{
		header("Location: index.php");
		exit();
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Counsellor Q&A Listing</title>

<meta name="keywords" content="cancer treatment opinion,diabetes treatment opinion, surgery opinion, plastic surgery, cosmetic surgery, second opinion diagnosis"/>

<meta name="description" content="pinkWhale has brought together world renowned super-specialists, who can give you a second medical opinion online. Our 2nd opinion doctors will review your medical information, medical records, and diagnostic tests to render a
comprehensive second opinion that includes recommendations, alternative treatment options, and therapeutic considerations."/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<!-- header.......-->
<?php 
include "db_connect.php";
include 'header.php'; 
	include "actions/encdec.php";
	//echo $_SESSION['doctor_type'];
?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu4').style.fontWeight= 'bold'</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="175" style="border-right:1px solid #4d4d4d" valign="top">
<table width="175" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<tr><th>Counsellor Q&A :</th></tr>
<tr><td><a href="counsellor_qa_listing.php">Pending Queries</a></td></tr>
<tr><td><a href="counsellor_answered_query.php"><b>Answered Queries</b></a></td></tr>
</table></td>
<td><table width='750' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="8"> Counsellor Answered Q&A </th></tr>
<?php		
	include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
$check=0;
		// Display all the data from the table 
			$sql="SELECT * FROM `q_a_db`   where doctor_type='Counsellor' order by `q_id` DESC";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) 
			{
			?>
		<tr><td colspan="8" align="center"><b>No Records</b></td></tr>
        <?php }
		else {  ?>
			<tr>
                 <td align='left' width="30"><strong>SI.</strong></td>
                <td align='center'><strong>Subject</strong></td>
                <td align='center'><strong>User</strong></td>
                <td align='center'><strong>Group</strong></td>
                <td align='center'><strong>Card #</strong></td>
                <td align='center'><strong>Date</strong></td>
                <td align='center'><strong>Answered by</strong></td>
                <!--<td align='center'><strong>Status</strong></td>-->
                <td align='center'><strong>View</strong></td>
           </tr>
	
			<?php	
			$count =0;		
			while($row = mysql_fetch_assoc($rs)) 
			{
				$imf_flag=0;
				$question=$row['question'];
				$user_id=$row['using_user'];
				$qid=$row['q_id'];
				$date=$row['submited_date'];
				$date1=strtotime($date);
		 		$started_date= date('d-m-Y', $date1);
			
	$qry3= "SELECT * FROM `user_details` WHERE `user_id`='$user_id'";
	$qry_rslt3 = mysql_query($qry3);
	while($result3= mysql_fetch_array($qry_rslt3))
	{
		$user_name=$result3['user_name'];
		$card_id=$result3['user_id'];
		$group=$result3['group'];
	}	
	$sql_table="SELECT * FROM `answer_qa_query`  where qa_id='$qid' and qa_status='close' ";
		$qry_sql_table = mysql_query($sql_table);
		if(mysql_num_rows ($qry_sql_table)>0) {
			
		$sql_answered="SELECT * FROM `answer_qa_query`  where qa_id='$qid' and 	doc_user_dif='1'";
		$sql_answered_rslt = mysql_query($sql_answered);
		while($sql_answered_qry_result= mysql_fetch_array($sql_answered_rslt )) {
			$answered_by=$sql_answered_qry_result['answer_by'];
		} 		   
			$count ++;
			$check++;

			echo "<tr bgcolor='#ffffff' style='color:#000000;' >";
				echo "<td>$count </td> ";
					echo "<td align='left'>$question </td> ";
				
				?>
                <td align="center"><?php echo $user_name; ?> </td> 
                <td align="center"><?php echo $group; ?> </td> 
                <td align="center"><?php echo $card_id; ?></td>
                <td align="center"><?php echo $started_date; ?></td>
                <td align="center"><?php echo $answered_by; ?></td>				
				    <td align="center" width="50"><a href="view_counsellorq_a.php?q_a_id=<?php echo $row['q_id'];?>&q_a_status=counsellor_answered"> <img src="admin/images/EditIcon.gif"  /></a></td>
                           
				<?php
				echo "</tr>";
			}
			}
			
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>', '</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
            <?php if($check==0) { ?>
            <tr><td colspan="5" align="center"><b>No Records</b></td></tr>
        <?php } ?>
			<tr><td colspan='7' align='center'>
            <?php
            echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
            <?php } ?>
        </table></td>
</tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
