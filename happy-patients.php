 <?php
ob_start();
error_reporting(E_PARSE);
session_start();
?>
<?php
include "db_connect.php";

include "actions/encdec.php";

include 'site_config.php';


if (isset($_COOKIE['PinkWhale']) && $_SESSION['login'] != "Card") {

    $username = $_COOKIE['PinkWhale']['username'];

    $password_en = $_COOKIE['PinkWhale']['password'];

    $password = base64_decode($password_en);



    $query1 = "SELECT admin_name, admin_password FROM `login_user` where admin_name = '$username' and admin_password='$password'";

    $result1 = mysql_query($query1);



    if (mysql_num_rows($result1) > 0) {

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'admin';
    }



    $qry = "SELECT `user_name`,`user_id` FROM `user_details` WHERE `user_email`='$username' && `password`='$password_en'";

    $rslt = mysql_query($qry);

    if (mysql_num_rows($rslt) > 0) {

        while ($rs = mysql_fetch_array($rslt)) {

            $_SESSION['pinkwhale_id'] = $rs['user_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'user';
    }


    $qry1 = "SELECT `doc_name`,doc_id FROM `pw_doctors` WHERE `doc_email_id`='$username' && `password`='$password_en'";

    $rslt1 = mysql_query($qry1);

    if (mysql_num_rows($rslt1) > 0) {

        while ($rs = mysql_fetch_array($rslt1)) {

            $_SESSION['doctor_id'] = $rs['doc_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'doctor';
    }
}
?>
 <!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is a survey to tell us what is really going on when it comes to patient satisfaction and their happiness. ">
	<meta name="keywords" content="happy patients, patient satisfaction, patients satisfaction, patient satisfaction in healthcare, quality healthcare">
    <title>Happy Patients | pinkWhale Healthcare</title>
	<script src="js/jquery.min.js"></script>
	
    <?php include 'includes/include-css.php'; ?>
  </head>
<?php //include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<?php include "includes/site_config.php" ?>
	<link href="css/testimonials.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="happy-patients"></div>
	<?php include "menu-patients.php" ?>

	<div class="banner-bg banner-bg-happy-patients">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Know that you can always count on us <br>to ensure you lead a healthy life.</i></div>
		</div>
	</div>

	<div class="page-tagline row-bg-1">
		<div class="max-960">
			HEAR WHAT SOME OF OUR HAPPY PATIENTS <span class="hidden-xs"><br></span>HAVE TO SAY ABOUT <i>pink</i>WHALE
		</div>
	</div>
	
		
	<?php
$qry="select * from pw_happy_patients";
	$res=mysql_query($qry);	
	while($row=mysql_fetch_array($res)){
	?>

	<div id="happy-patient-slider" class="owl-carousel hidden-xs hidden-sm">
		
		<div class="item">
			<div class="page-row gradient4-background patients">
				<div class="content-area slider-row">
					<div class="row-header"></div>
					<div class="row-description">
						<div class="image col-xl-4 col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo $row['image'];?>" style="border-radius:100px;box-shadow: 2px 2px 2px #888888;">
						</div>
						<div class="message col-xl-8 col-md-8 col-sm-8 col-xs-12">
							<?php echo $row['patient_testimonial'];?>
							<div class="name">- <?php echo $row['patient_name'];?></div>				
						</div>
					</div>
				</div>
			</div> 
		</div>
		
	</div> 
	<?php } ?>
	
		
	<div class="page-row testimonials row-bg-2">
		<div class="content-area ">
			<div class="row-header hidden-xs hidden-sm">
				<img src="img/patients/happypatients/Testimonials_Patients_Icon2_Quotes.png" class="row-title-image">
				<div class="row-title">Patient Testimonials</div>
			</div>
			<?php
$qry="select * from pw_happy_patients";
	$res=mysql_query($qry);	
	
	
	while($row=mysql_fetch_array($res)){
	?>
			<div class="row-description">

				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4 col-sm-4">
						<img src="<?php echo $site_url.$row['image'];?>" style="border-radius:100px;box-shadow: 2px 2px 2px #888888;">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8 col-sm-8">
						<?php echo $row['patient_testimonial'];?>
					<div class="name">- <?php echo $row['patient_name']; ?></div>
					</div>
				</div>

			</div>
			<?php } ?>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>

	<script>
			$(document).ready(function() {
 
 			 $("#happy-patient-slider").owlCarousel({
	 
		      navigation : false, // Show next and prev buttons
	    	  slideSpeed : 300,
	      	  paginationSpeed : 400,
	          singleItem:true,
	          autoPlay : true,
	          
	 
		      // "singleItem:true" is a shortcut for:
		       items : 1, 
		      // itemsDesktop : false,
		      // itemsDesktopSmall : false,
		      // itemsTablet: false,
		      // itemsMobile : false
 			 });
 		});
	</script>
<?php include "includes/end.php" ?>
