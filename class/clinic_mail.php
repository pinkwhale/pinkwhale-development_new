<?php
class clinicMail
{
	var $params;
	var $logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/images/pinkwhale_logo.jpg'/>";
	public function clinicMail($params)
	{
		define('ROOT_PATH', realpath(__DIR__.'/../'));
		require_once (ROOT_PATH.'/Mail_helper/class.phpmailer.php');		
				
		$toName = $params[1];
		
		$fromMail = "info@pinkwhalehealthcare.com";
		$to    = $params[0];
		$subject = "pinkwhalehealthcare.com consultation: {$params[2]}";
		$details = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>";
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$this -> logo}</div>";
				
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($toName).",</div>";
			
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>";
		$details .= "Clinic has initiated a diagnostic consultation on your behalf with {$params[5]}, 
						Please login to your account at <a href='http://pinkwhalehealthcare.com'>http://pinkwhalehealthcare.com</a> to view the consultation.
						</div>";
			
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>
					<b>Pinkwhale Healthcare</b>
					</div>";
	
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From:{$fromMail}" . "\r\n";

		$mail = new PHPMailer();
        $mail->IsSMTP();   
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "ssl";
                $mail->Host = "smtp.gmail.com"; 
                $mail->Port = 465;
                $mail->Username = "info@pinkwhalehealthcare.com"; 
                $mail->Password = "pinkwhale";

                $mail->From = "info@pinkwhalehealthcare.com";
                $mail->FromName = "PinkWhale Healthcare";
                
                $mail->AddAddress($to);   // name is optional
               
                $mail->IsHTML(true);      // set email format to HTML

                $mail->Subject = $subject;

                $mail->Body    = $details;
                

                if(!$mail->Send())
                {
                   echo "Message could not be sent. <p>";
                   echo "Mailer Error: " . $mail->ErrorInfo;
                  
                }

	}
}
?>
