<?php
class FileHandler
{
	var $allowedMime = array( 'image/jpeg', // images
							  'image/pjpeg', 
							  'image/jpg', 
							  'image/png', 
							  'image/x-png', 
							  'image/gif', 
							  'image/tiff', 
							  'image/x-tiff',
							  'application/pdf', // pdf
							  'application/x-pdf', 
							  'application/acrobat', 
							  'text/pdf',
							  'text/x-pdf',
							  'text/xls', 
							  'text/plain', // text
							  'application/msword', // word
							  'application/x-msexcel', // excel
							  'application/xls',
							  'application/vnd.ms-excel',
							  'application/excel',
							  'application/x-excel',
							  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
							  'application/octet-stream',
							  'video/swf', //videos
							  'video/3gp', 
							  'video/3gpp', 
							  'video/wmv', 
							  'video/x-ms-wmv', 
							  'video/mpg', 
							  'video/mpeg', 
							  'video/flv', 
							  'video/avi', 
							  'video/mp4',
							  'video/wav',
							  'application/octet-stream'
							 );					 						 

	
	var $maxFileSize = 20000000;

	var $errorMessage = null;
	var $isError = false;

	var $filePath = null;
	/**
	 * function upload
	 * function to upload file
	 * @access public
	 * @param  upload file
	 * @param  path of the directory in which file will be upload
	 * @param  type of upload 
	 * @return none
	**/
	function upload($params)
	{
		$fileName = $params[0];
		$dirPath  = $params[1];
		$uniqId   = $params[2];
		
		$this -> errorMessage = null;
		
		//if(empty($fileName) || empty($dirPath))
			//$this -> setError('* You must supply a file field name and a directory on the server.');
		
		//if(!isset($fileName))
			//$this -> setError('* No file supplied.');
		
		if (file_exists($dirPath.$uniqId.$fileName['name']))
			$this -> setError('* The uploaded file already exists');
		
		if($fileName['error'] != 0)
		{	
			switch($fileName['error'])
			{
				case 1:
					$this -> setError('* The file is too large (server).');
				break;
				
				case 2:
					$this -> setError('* The file is too large (form).');
				break;
				
				case 3:
					$this -> setError('* The file was only partially uploaded.');
				break;
				
				case 4:
					$this -> setError('* No file was uploaded.');
				break;
				
				case 5:
					$this -> setError('* The servers temporary folder is missing.');
				break;
				
				case 6:
					$this -> setError('* Failed to write to the temporary folder.');
				break;
			}
		}
	
		//if(!is_dir($dirPath) || !is_writable($dirPath))
			//$this -> setError('* The supplied upload directory does not exist or is not writable.');
			
	/*	if(!in_array($fileName['type'], $this->allowedMime))
		{
			$this -> setError('* The file upload is of an illegal mime type .');
		}*/
			
		if(!$this -> isError)
		{
			if(move_uploaded_file($fileName['tmp_name'], $dirPath.$uniqId.$fileName['name']))
			{
				$this -> setFilePath($dirPath.session_id().$fileName['name']);
				return true;
			}
			else
			{
				//$this -> setError('* The uploaded file could not be moved to the created directory');
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	function setFilePath($path)
	{
		$this -> filePath = $path;
	}

	function getFilePath()
	{
		return $this -> filePath;
	}
	
	function setError($error)
	{
		$this -> isError = true;
		
		if($this -> errorMessage == null)
			$this -> errorMessage = $error;
		else
			$this -> errorMessage .= '<br />'.$error;
	}
	
	function getError()
	{
		return $this -> errorMessage;
	}
}
?>
