<?php
class doctorRegisterMail
{

	var $params;
	var $logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/images/pinkwhale_logo.jpg'/>";
	public function doctorRegisterMail($params)
	{
		define('ROOT_PATH', realpath(__DIR__.'/../'));
	//	include (ROOT_PATH.'/Mail_helper/class.phpmailer.php');		
		//$params[0] patient email
		//$params[1] patient name
		//$params[2] patient complaint
		//$params[3] patient details
		//$params[4] doctor email
		//$params[5] doctor name
		$fromMail = "info@pinkwhalehealthcare.com";
		$subject = "Welcome to Pinkwhale!";
		$details = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>";
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$this -> logo}</div>";
		
		if(isset($params[4]) <> "") //Doctor send the mail to Patient
		{
			$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".$params[5].",</div>";
			$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>";
			$details .= " Thanks for registering with pinkwhale, 
						Please login to your account at <a href='http://pinkwhalehealthcare.com'>http://pinkwhalehealthcare.com</a> to view your profile.
						</div>";
				
		}
		$details .= "<div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>
					<b>Pinkwhale Healthcare</b>
					</div>";
	
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From:{$fromMail}" . "\r\n";

		$to = $params[4];
		//mail($to, $subject, $details, $headers);
		$mail = new PHPMailer();
                $mail->IsSMTP();   
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "ssl";
                $mail->Host = "smtp.gmail.com"; 
                $mail->Port = 465;
                $mail->Username = "info@pinkwhalehealthcare.com"; 
                $mail->Password = "pinkwhale";

                $mail->From = "info@pinkwhalehealthcare.com";
                $mail->FromName = "PinkWhale Healthcare";
                //$mail->AddAddress("josh@example.net", "Josh Adams");
                $mail->AddAddress($to);   // name is optional
                //$mail->AddReplyTo("", "");
                //$mail->WordWrap = 50;                                 // set word wrap to 50 characters
                //$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
                //$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
                $mail->IsHTML(true);                                  // set email format to HTML

                $mail->Subject = $subject;
                $mail->Body    = $details;
                
                //$mail->AltBody = "This is the body in plain text for non-HTML mail clients";
                if(!$mail->Send())
                {
                   echo "Message could not be sent. <p>";
                   echo "Mailer Error: " . $mail->ErrorInfo;
                  // exit;
                }

/*
		echo $details;
		die();
*/
	}
}
?>
