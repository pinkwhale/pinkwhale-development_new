function search_spe(){

    var doc_name = encodeURI(document.getElementById("doc_name").value);
    var specialist = encodeURI(document.getElementById("specialist").value);

    $("#loading").fadeIn(900,0);
    $("#loading").html("<img src='images/ajax-loader.gif' />");

        // -------------------ajax start--------------------- //
        var paramString = 'doc_name='+doc_name+'&specialist='+specialist;


        $.ajax({
                type: "POST",
                url: "get-specialist-details.php",
                data: paramString,
                success: function(response) {

                        document.getElementById("spec_search").innerHTML=response;
                        $("#spe_details").hide();
			$("#loading").fadeOut('slow');	
                }

        });

        // -------------------ajax End--------------------- //

  //  $("#loading").fadeOut('slow');
}


function spe_popup(obj,value){

      if(obj==true)
      {
            document.getElementById('spe_pop_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.height = document.body.scrollHeight + "px";
            document.getElementById('spe_divDisable_'+value).style.width = document.body.scrollWidth + "px";

      }else{

            document.getElementById('spe_pop_'+value).style.display='none';
            document.getElementById('spe_divDisable_'+value).style.display='none';

      }

}

function showLoginForm(value){

        spe_popup(false,value);

        $(document).ready(function()
        {
                alert("You are not yet logged in. Please login.");
                $(".signin").click();
        });

}

function search_health_expert(){


    var doc_name = encodeURI(document.getElementById("doc_name").value);
    var specialist = encodeURI(document.getElementById("specialist").value);

    $("#loading").fadeIn(900,0);
    $("#loading").html("<img src='images/ajax-loader.gif' />");

        // -------------------ajax start--------------------- //
        var paramString = 'doc_name='+doc_name+'&specialist='+specialist;


        $.ajax({
                type: "POST",
                url: "get-health-expert-details.php",
                data: paramString,
                success: function(response) {

                        document.getElementById("spec_search").innerHTML=response;
                        $("#spe_details").hide();
			$("#loading").fadeOut('slow');
                }

        });

        // -------------------ajax End--------------------- //

  //  $("#loading").fadeOut('slow');
}


    
function displayLoginForm(){
           

        $(document).ready(function() 
        {
                alert("You are not yet logged in. Please login.");                       
                $(".signin").click();
        });

}    


function login_callpopup(obj,num){

      if(obj==true)        
      {
            document.getElementById("username_errordiv").innerHTML = "";
            document.getElementById("password_errordiv").innerHTML = "";
	    document.getElementById("log_errordiv").innerHTML = "";
            document.consult_signin.username.value="";
	    document.consult_signin.password.value="";
 
            document.getElementById('doc_id').value=num;
            document.getElementById('login_popupId').style.display='';
	    document.getElementById('login_divDisable').style.display='';	
            document.getElementById('login_divDisable').style.height = document.body.scrollHeight + "px";
            document.getElementById('login_divDisable').style.width = document.body.scrollWidth + "px";             

      }else{
          
	    document.getElementById('login_popupId').style.display='none';
	    document.getElementById('login_divDisable').style.display='none';

      } 
   
}

