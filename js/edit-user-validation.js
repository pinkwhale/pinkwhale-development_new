function submit_form()
{
	document.getElementById('email_cnsltion_form').submit();
}
function activate_edit_div()
{
	document.getElementById("user_details").style.display="none";
	document.getElementById("user_change_pswd").style.display="none";
	document.getElementById("edit_user_details").style.display="block";
}
function activate_change_pswd()
{
	document.getElementById("user_details").style.display="none";
	document.getElementById("edit_user_details").style.display="none";
	document.getElementById("user_change_pswd").style.display="block";	
}
function cancel_button()
{
	window.location.href="user_details.php";
}
function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}

function iskey_not_allowed(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode >350&& charCode < 0 )
	{
		return true; 
	}
	else
	{
		return false; 
	}
}

function validated_pswd(form)
{
	var pswValidated = false;
	var repswValidated = false;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";
		//form.new_pswd.focus();
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";	
		//form.cnfm_pswd.focus();
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (form.new_pswd.value!='' )	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
	}
}
function psw_val(form){
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";		
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (form.new_pswd.value!='' )	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}	
	}
	if(pswValidated && repswValidated )
	{
		document.getElementById('user_change_pswd_form').submit();
	}
	else
	{
		return false;
	}
}

function validated(form)
{
	Validated = true;        
	document.getElementById("nameErrDiv").innerHTML = "";		
	document.getElementById("emailErrDiv").innerHTML = "";        
	document.getElementById("mobileErrDiv").innerHTML = "";         
	
	var name = form.user_name.value;		
	var email = form.user_email.value;       
	var mobile = form.mobile_num.value;		
		
	if ( name =="" )
	{            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
		Validated = false;
	}
	 if (name != "") {
		 if ( /[^A-Za-z +$]/.test(name)) {
			document.getElementById("nameErrDiv").innerHTML = "Please enter characters only";      	
			form.user_name.focus();
			Validated = false;
    }
	}
        if ( email =="" )
	{            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
		Validated = false;
	}
	if ( email !='' )
	{
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";       
		Validated = false;
	}
	}
        
	if( mobile == "" )
	{            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
		Validated = false;
	} 
	if(Validated)
	{               
               
		document.getElementById('user_prof_form').submit();
	}		
	else
	{                
		return false;
	}	
}

