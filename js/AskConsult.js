function load_type(){
  //  alert("hii");
    if(document.getElementById("query").checked == true){
        window.location = "request_query.php?type=2";
    }else if(document.getElementById("opinion").checked == true){
        window.location = "request_opinion.php?type=3";
    }else if(document.getElementById("consult").checked == true){
        window.location = "request_consultation.php?type=4";
    }   
    
    
}


function doctor_list(doc_type){
   // var doc_type = encodeURI(document.getElementById("specialist").value);
   var doc_type = $('input[name=specialist]:checked').val();
	//alert(doc_type);
    var type = encodeURI(document.getElementById("consult_type").value);
    //alert(type);
    if(doc_type!="" && type!=""){
		//alert("hiiiiii");
        // -------------------ajax start--------------------- //
        
        //$("#doc_list").fadeIn(900,0);
        
        $("#docTbl").html("<div style='text-align: center;margin-top:50px;'><img src='images/square_loader.gif' /></div>");
        
        var paramString = 'doc_type='+doc_type+'&type='+type;
        
        $.ajax({
            
                type: "POST",
                url: "get_doctor_list.php",
                data: paramString,
                success: function(response) {
                 
                    document.getElementById("doc_id").value = "";
                    document.getElementById("cost").innerHTML = "";
                     if(doc_type=="0"){
                         $("#spe_disp_id").hide();
                        // document.getElementById("display_name").innerHTML = "Ask our Physicians";
                         document.getElementById("consult_type").value = "2";
                         update_doctor("2");
                     }else if(doc_type=="1"){
                         $("#spe_disp_id").show();
                      //   document.getElementById("display_name").innerHTML = "Ask our Specialists";
                         document.getElementById("consult_type").value = "1";
                         update_doctor("1");
                     }                     
		                          
                     document.getElementById("doc_list").innerHTML=response;  
		     $("a[rel=popover-left]").popover({ placement :'left'});
                }
                
        });

        // -------------------ajax End--------------------- //
    }else{
        document.getElementById("doc_list").innerHTML="No Records";
    }
}

function doctor_list_consult(){
   //alert("hiiiiii");
	var doc_type = encodeURI(document.getElementById("specialist").value);
   //var doc_type = $('input[name=specialist]:checked').val();
	//alert(doc_type);
    var type = encodeURI(document.getElementById("consult_type").value);
    //alert(type);
    if(doc_type!="" && type!=""){
		//alert("hiiiiii");
        // -------------------ajax start--------------------- //
        
        //$("#doc_list").fadeIn(900,0);
        
        $("#docTbl").html("<div style='text-align: center;margin-top:50px;'><img src='images/square_loader.gif' /></div>");
        
        var paramString = 'doc_type='+doc_type+'&type='+type;
        
        $.ajax({
            
                type: "POST",
                url: "get_doctor_list.php",
                data: paramString,
                success: function(response) {
                 
                    document.getElementById("doc_id").value = "";
                    document.getElementById("cost").innerHTML = "";
                     if(doc_type=="0"){
                         $("#spe_disp_id").hide();
                        // document.getElementById("display_name").innerHTML = "Ask our Physicians";
                         document.getElementById("consult_type").value = "2";
                         update_doctor("2");
                     }else if(doc_type=="1"){
                         $("#spe_disp_id").show();
                      //   document.getElementById("display_name").innerHTML = "Ask our Specialists";
                         document.getElementById("consult_type").value = "1";
                         update_doctor("1");
                     }                     
		                          
                     document.getElementById("doc_list").innerHTML=response;  
		     $("a[rel=popover-left]").popover({ placement :'left'});
                }
                
        });

        // -------------------ajax End--------------------- //
    }else{
        document.getElementById("doc_list").innerHTML="No Records";
    }
}

function doctor_list_opinion(){
   //alert("hiiiiii");
 var doc_type = encodeURI(document.getElementById("specialist").value);
  // var doc_type = $('input[name=specialist]:checked').val();
	//alert(doc_type);
    var type = encodeURI(document.getElementById("consult_type").value);
    //alert(type);
    if(doc_type!="" && type!=""){
		//alert("hiiiiii");
        // -------------------ajax start--------------------- //
        
        //$("#doc_list").fadeIn(900,0);
        
        $("#docTbl").html("<div style='text-align: center;margin-top:50px;'><img src='images/square_loader.gif' /></div>");
        
        var paramString = 'doc_type='+doc_type+'&type='+type;
        
        $.ajax({
            
                type: "POST",
                url: "get_doctor_list.php",
                data: paramString,
                success: function(response) {
                 
                    document.getElementById("doc_id").value = "";
                    document.getElementById("cost").innerHTML = "";
                     if(doc_type=="0"){
                         $("#spe_disp_id").hide();
                        // document.getElementById("display_name").innerHTML = "Ask our Physicians";
                         document.getElementById("consult_type").value = "2";
                         update_doctor("2");
                     }else if(doc_type=="1"){
                         $("#spe_disp_id").show();
                      //   document.getElementById("display_name").innerHTML = "Ask our Specialists";
                         document.getElementById("consult_type").value = "1";
                         update_doctor("1");
                     }                     
		                          
                     document.getElementById("doc_list").innerHTML=response;  
		     $("a[rel=popover-left]").popover({ placement :'left'});
                }
                
        });

        // -------------------ajax End--------------------- //
    }else{
        document.getElementById("doc_list").innerHTML="No Records";
    }
}

function doctor_spe_doc_list(){
    
   // var doc_type = document.getElementById("specialist").value;
     var doc_type = $('input[name=specialist]:checked').val();
    var type = document.getElementById("consult_type").value;
    var spe_type = document.getElementById("specialization").value;

    if(doc_type!="" && type!="" && spe_type!=""){
        // -------------------ajax start--------------------- //
        
        //$("#doc_list").fadeIn(900,0);
        
        $("#docTbl").html("<div style='text-align: center;margin-top:50px;'><img src='images/square_loader.gif' /></div>");
        
        doc_type = encodeURI(doc_type);
        type = encodeURI(type);
        spe_type = encodeURIComponent(spe_type);
        
        var paramString = 'doc_type='+doc_type+'&type=2&spe_type='+spe_type;
        
        $.ajax({
            
                type: "POST",
                url: "get_doctor_list.php",
                data: paramString,
                success: function(response) {
                    
                    document.getElementById("doc_id").value = "";
                    document.getElementById("cost").innerHTML = "";
                     if(doc_type=="0"){
                         update_doctor_amount("1");
                     }else if(doc_type=="1"){
                         update_doctor_amount("2");
                     }                     
                     
                     document.getElementById("doc_list").innerHTML=response;  
                }
                
        });

        // -------------------ajax End--------------------- //
    }else{
        document.getElementById("doc_list").innerHTML="No Records";
    }
}


function user_type(){
    
    if(document.getElementById("newuser").checked==true){
        $("#newusr").show();
        $("#extusr").hide();
        $("#info_button").hide();
		$("#send").show();
    }
    
    if(document.getElementById("existuser").checked==true){        
        $("#newusr").hide();
        $("#extusr").show();
        $("#info_button").show();
		$("#send").hide();
    }
    
}


var valid=false;

function validate_consultation(){
    
    valid = true;
    
    
    
    document.getElementById("send").value = "Uploading Please wait.....";
    document.getElementById("send").disabled = true;
    
   // document.getElementById("speErrDiv").innerHTML = "";
    //document.getElementById("complaintErrDiv").innerHTML = "";
    //document.getElementById("contentErrDiv").innerHTML = "";
   // document.getElementById("docErrDiv").innerHTML = "";
    /*
    if(document.getElementById("specialist").value==""){
        document.getElementById("speErrDiv").innerHTML = "Please select  Speciality";
        valid = false;
    }
    */
   /* if(document.getElementById("complaint").value==""){
        document.getElementById("complaintErrDiv").innerHTML = "Nature of Concern field cannot be Blank";
        valid = false;
    }*/
    
    if(document.getElementById("newuser").checked==true){        
        
        document.getElementById("newnameErrDiv").innerHTML = "";   
        document.getElementById("newageErrDiv").innerHTML = "";
        document.getElementById("newgenderErrDiv").innerHTML = "";
        document.getElementById("newemailErrDiv").innerHTML = "";   
        document.getElementById("newpasswordErrDiv").innerHTML = "";
        document.getElementById("newretypepasswordErrDiv").innerHTML = "";
		document.getElementById("newmobilenoErrDiv").innerHTML = "";
        
        if(document.getElementById("newname").value==""){
            document.getElementById("newnameErrDiv").innerHTML = "Name cannot be Blank";
            valid = false;
        } 
        
        if(document.getElementById("newage").value==""){
            document.getElementById("newageErrDiv").innerHTML = "Age cannot be Blank";
            valid = false;
        }else if(document.getElementById("newage").value<18){
            document.getElementById("newageErrDiv").innerHTML = "need to be > 18";
            valid = false;
        }
        
        if(document.getElementById("newgender").value==""){
            document.getElementById("newgenderErrDiv").innerHTML = "Please select Gender";
            valid = false;
        }
		
		 if(document.getElementById("mobileno").value==""){
            document.getElementById("newmobilenoErrDiv").innerHTML = "Mobile number cannot be blank";
            valid = false;
        }
        
        if(document.getElementById("newemail").value==""){
            document.getElementById("newemailErrDiv").innerHTML = "Email cannot be Blank";
            valid = false;
        }else if(document.getElementById("newemail").value!=""){
            
            
            var paramString = 'username='+document.getElementById("newemail").value+'&password=&type=1';
        
            $.ajax({  
                    type: "POST",  
                    url: "actions/email_check.php",  
                    data: paramString,  
                    success: function(response) {   

                            if(response==true){
                                document.getElementById("newemailErrDiv").innerHTML = "";
                            }else{
                                document.getElementById("newemailErrDiv").innerHTML = "Email Already Exists";
                                valid = false;
                            }
                    }

            });
            
            
        }
        
        if(document.getElementById("newpassword").value!=""){
            
            if((document.getElementById("newpassword").value).length<6 || (document.getElementById("newpassword").value).length>12){
                document.getElementById("newpasswordErrDiv").innerHTML = "Password length should be 6-12 characters.";
                valid = false;
            }else if(document.getElementById("newrepassword").value!=document.getElementById("newpassword").value){
                document.getElementById("newretypepasswordErrDiv").innerHTML = "Password Mismatch";
                valid = false;
            }
            
        }           
    }
        
    
    if(document.getElementById("existuser").checked==true){
        document.getElementById("extemailErrDiv").innerHTML = "";   
        document.getElementById("extuserpasswordErrDiv").innerHTML = "";
        
        if(document.getElementById("extuseremail").value==""){
            document.getElementById("extemailErrDiv").innerHTML = "Email cannot be Blank";
            valid = false;
        } 
        if(document.getElementById("extuserpassword").value==""){
            document.getElementById("extuserpasswordErrDiv").innerHTML = "Password cannot be Blank";
            valid = false;
        }
        
        if(document.getElementById("extuseremail").value!="" && document.getElementById("extuserpassword").value!=""){
            
            var paramString = 'username='+document.getElementById("extuseremail").value+'&password='+document.getElementById("extuserpassword").value+'&type=2';
        
            $.ajax({  
                    type: "POST",  
                    url: "actions/email_check.php",  
                    data: paramString,  
                    success: function(response) {   

                            if(response==true){
                                document.getElementById("extemailErrDiv").innerHTML = "";
                            }else{
                                document.getElementById("extemailErrDiv").innerHTML = "Invalid Email-ID or Password";
                                valid = false;
                            }
                    }

            });
        }
        
    }
    
  /* 
 if(document.getElementById("doc_id").value =="" && (document.getElementById("consult_type").value=="1" || document.getElementById("consult_type").value=="2"))
{  
    if(document.getElementById("i_choose").checked == true && document.getElementById("doc_id").value =="" ) 
    {
        document.getElementById("docErrDiv").innerHTML = "Please select Doctor";
        valid = false;
    }
 } else if(document.getElementById("doc_id").value =="" && (document.getElementById("consult_type").value=="3" || document.getElementById("consult_type").value=="4"))
{  
    
        document.getElementById("docErrDiv").innerHTML = "Please select Doctor";
        valid = false;
    
 }*/
 

    if(document.getElementById("consult_type").value=="4" && document.getElementById("newuser").checked==true){
        if(document.getElementById("newPinkcard").value==""){
            document.getElementById("newpinkcardErrDiv").innerHTML = "PinkWhale Card Cannot be blank";
            valid = false;
        }
    }
	
	/*
    if(document.getElementById("specialist").value=="1"){
        if(document.getElementById("specialization").value=="" && document.getElementById("doc_id").value==""){
            document.getElementById("spelizErrDiv").innerHTML = "Please select Specialization";
            valid = false;
        }else{
            document.getElementById("spelizErrDiv").innerHTML = "";
        }
    }
    
    
    if(document.getElementById("uploadedfile").value !=""){
        
        var max_upload_limit = document.getElementById("MAX_FILE_SIZE").value;
        
        if(document.getElementById("request_type").value=="query"){
            var filesize = document.getElementById("uploadedfile").files[0].size;   

            if(filesize>max_upload_limit){
                document.getElementById("uploadedfile").value="";
                document.getElementById("fileErrDiv").innerHTML="Max File Size limit is 32KB";
                valid = false;
            }
        }
        
    }*/
    
    
    
    if(valid==true){
        
        loading('Sending',1);
        document.request_consult.submit();
       
    }else{
        
        document.getElementById("send").value = "Send";
        document.getElementById("send").disabled = false;
        return false;
        
    }
    
}


function card_check_consult(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("newpinkcardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: "check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        
                            if(response==true){
                                document.getElementById("newpinkcardErrDiv").innerHTML= "";
                                return false;
                            }else{
                                document.getElementById("newPinkcard").value= "";
                                document.getElementById("newpinkcardErrDiv").innerHTML= "Invalid Pinkwhale card";
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //
        
}


function check_new_email(username)
{
	            
        
        // -------------------ajax start--------------------- //

        var paramString = 'username='+username+'&password=&type=1';
        //alert(paramString);
        $.ajax({  
                type: "POST",  
                url: "actions/email_check.php",  
                data: paramString,  
                success: function(response) {   
                    //alert(response);
                        if(response==true){
                            document.getElementById("newemailErrDiv").innerHTML = "";
                            return true;
                        }else{
                            document.getElementById("newemail").value="";
                            document.getElementById("newemailErrDiv").innerHTML = "Email Already Exists";
                            return false;
                        }
                }

        }); 
        
        // -------------------ajax End--------------------- //	
        
        
	
}



function check_login(username,password,type)
{
	            
        var r_value = false;            
        // -------------------ajax start--------------------- //

        var paramString = 'username='+username+'&password='+password+'&type='+type;

        $.ajax({  
                type: "POST",  
                url: "email_login_check.php",  
                data: paramString,  
                success: function(response) {                                                      

                        if(response==true){

                            r_value = true;

                        }else{

                            r_value = false;
                            
                        }
                }

        }); 
        
        // -------------------ajax End--------------------- //	
        
        return r_value;
	
}


function update_doctor_amount(id){
  
    // -------------------ajax start--------------------- //
    
    $("#cost").html("<div style='text-align: center;position: -80px'><img src='images/square_loader.gif' /></div>");

    var type = encodeURI(document.getElementById("consult_type").value);
    
    if(type=="1"){
        type="2";
    }else if(type=="2"){
        type="1";
    }

    var paramString = 'id='+id+'&type='+type;
    
    $.ajax({  
            type: "POST",  
            url: "get_doc_amount.php",  
            data: paramString,  
            success: function(response) {                  
                 document.getElementById("cost").innerHTML = "";
                 document.getElementById("doc_id").value = "";
                 if(type=="4"){
                     if(response=="0"){
                         alert("Selected Doctor will Provide only Tele-Consultation.");
                     }else{
                         document.getElementById("cost").innerHTML = response;
                         document.getElementById("doc_id").value = "";
                     }                     
                 }else{
                     document.getElementById("cost").innerHTML = response;
                     document.getElementById("doc_id").value = "";
                 }
            }

    }); 

    // -------------------ajax End--------------------- //	
    
}


function update_doctor(id){
  
    // -------------------ajax start--------------------- //
    
    $("#cost").html("<div style='text-align: center;position: -80px'><img src='images/square_loader.gif' /></div>");
	$("#arrow").hide();
    var type = encodeURI(document.getElementById("consult_type").value);
    
    if(type=="1"){
        type="2";
    }else if(type=="2"){
        type="1";
    }

    var paramString = 'id='+id+'&type='+type;
    
    $.ajax({  
            type: "POST",  
            url: "get_doc_amount.php",  
            data: paramString,  
            success: function(response) {
					$("#doctor_id").val(id);
						$("#doct_id").val(id);
						$("#docto_id").val(id);
						$("#docte_id").val(id);
		//alert(response);
                 document.getElementById("cost").innerHTML = "";
                 document.getElementById("doc_id").value = "";
                 if(type=="4"){
                     if(response=="0"){
                         alert("Selected Doctor will Provide only Tele-Consultation.");
                     }else{
                         document.getElementById("cost").innerHTML = response;
						  $("#doc_name").html(response);
						  $("#doctor_id").val(id);
						  
                         document.getElementById("doc_id").value = id;
                     }                     
                 }else{
                     document.getElementById("cost").innerHTML = response;
                     document.getElementById("doc_id").value = id;
                 }
            }

    }); 

    // -------------------ajax End--------------------- //	
    
}




function isNumberKey(evt)
{
	
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
