
function expertCarousel()
{
	stepcarousel.setup(
	{
		galleryid: 'expgallery', //id of carousel DIV
		beltclass: 'belt', //class of inner "belt" DIV containing all the panel DIVs
		panelclass: 'panel', //class of panel DIVs each holding content
		autostep: {enable:false, moveby:1, pause:3000},
		panelbehavior: {speed:750, wraparound:false, persist:true},
		defaultbuttons: {enable: true, moveby: 4, leftnav: ['images/pagination/navLeft.jpeg', -25, 50], rightnav: ['images/pagination/navRight.jpeg', 3, 50]},
		statusvars: ['statusA', 'statusB', 'statusC'], //register 3 variables that contain current panel (start), current panel (last), and total panels
		contenttype: ['inline'] //content setting ['inline'] or ['ajax', 'path_to_external_file']
	});
}	
