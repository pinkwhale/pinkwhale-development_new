// JavaScript Document

var usernameValidated = false;
var pswValidated = false;
var repswValidated = false;
	

function psw_val(form)
{
	usernameValidated = true;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("username_div").innerHTML	= "";
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.username.value=='')
	{
   		document.getElementById("username_div").innerHTML = "UserName cannot be blank";
    	usernameValidated = false;
	}
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";
		//form.new_pswd.focus();
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";	
		//form.cnfm_pswd.focus();
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (form.new_pswd.value!='' )	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
	}
	if(usernameValidated && pswValidated && repswValidated)
	{
		document.getElementById('changePassword').submit();
	}
	else
	{
		return false;
	}	
}

function validate_pswd(form)
{
	var pswValidated = false;
	var repswValidated = false;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank"		
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (form.new_pswd.value!='')	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
	}
}



