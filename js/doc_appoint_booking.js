var doctor = false;
var patient_name= false;
var age = false;
var mobile = false;
var gender = false;
var email =true;
var appointment_time = false;
var new_patient = false;
var new_to_system = false;

function book_final(form){

    doctor = true;
    patient_name= true;
    age = true;
    mobile = true;
    gender = true;
    email = true;
    appointment_time = true;
    new_patient = true;
    new_to_system = true;
    
    document.getElementById("p_nameErrDiv").innerHTML="";
    document.getElementById("ageErrDiv").innerHTML="";
    document.getElementById("mobileErrDiv").innerHTML="";
    document.getElementById("genderErrDiv").innerHTML="";
    document.getElementById("emailErrDiv").innerHTML="";
    document.getElementById("appointment_timeErrDiv").innerHTML="";
    document.getElementById("NewPatientErrDiv").innerHTML="";
    document.getElementById("NewForPatientErrDiv").innerHTML="";
   
    
    if(document.getElementById("p_name").value==""){
        document.getElementById("p_nameErrDiv").innerHTML="Pateint Name cannot be blank";
        patient_name =false;
    }
    
    if(document.getElementById("age").value==""){
        document.getElementById("ageErrDiv").innerHTML="Age cannot be blank";
        age =false;
    }
    
    if(document.getElementById("mob").value==""){
        document.getElementById("mobileErrDiv").innerHTML="Mobile No. cannot be blank";
        mobile =false;
    }
    
    if(document.getElementById("email").value==""){
        document.getElementById("emailErrDiv").innerHTML="Email-id cannot be blank";
        email =false;
    }
    
    if(document.getElementById("gender").value==""){
        document.getElementById("genderErrDiv").innerHTML="Please select Gender";
        gender =false;
    }
    
    if(document.getElementById("time_slot").value==""){
        document.getElementById("appointment_timeErrDiv").innerHTML="Appointment Date and Time cannot be blank";
        appointment_time =false;
    }
    
    if(document.getElementById("new_patient").value==""){
        document.getElementById("NewPatientErrDiv").innerHTML="Are you new patient ?";
        new_patient =false;
    }
    
    if(document.getElementById("new_for_system").value==""){
        document.getElementById("NewForPatientErrDiv").innerHTML="Are you new for this System ?";
        new_to_system =false;
    }

    if(patient_name && age && mobile && gender && appointment_time && new_patient && new_to_system){     
        
        document.getElementById("book_doc_app").action = "actions/book_appointment_by_doctor.php";
        document.getElementById("book_doc_app").submit();
    }else{
        return false;
    }

    
}



function book_cancel(form){

    doctor = true;
    patient_name= true;
    age = true;
    mobile = true;    
    gender = true;
    appointment_time = true;

    if(doctor && patient_name && age && mobile && gender && appointment_time){
        document.getElementById("book_doc_app").action = "actions/book_appointment_cancel_bydoctor.php";
        document.getElementById("book_doc_app").submit();
    }else{
        return false;
    }

    
}

function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}



