var nameValidated = false;
var emailValidated = false;
var passwordValidated = false;
var re_passwordValidated = false;
var termsValidated = false;
var mobileValidated = false;
function PWRegisterSignUp(form)
{
	nameValidated = true;
	emailValidated = true;
	passwordValidated = true;
	re_passwordValidated = true;
	termsValidated = true;
	mobileValidated = true;
	document.getElementById("nameErrDiv").innerHTML	= "";
	document.getElementById("emailErrDiv").innerHTML = "";
	document.getElementById("passwordErrDiv").innerHTML = "";
	document.getElementById("repasswordErrDiv").innerHTML	= "";
	document.getElementById("termsErrDiv").innerHTML	= "";
	document.getElementById("mobileErrDiv").innerHTML	= "";
	if (form.regname.value=='')
	{
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
    	nameValidated = false;
	}
        
        email = form.regemail.value;
        var atpos=email.indexOf("@");
   	if (form.regemail.value=='')
	{
		document.getElementById("emailErrDiv").innerHTML = "Email Address cannot be blank";
		emailValidated = false;
	}else if(atpos<1){
            document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";
		emailValidated = false;

        }
         phone = form.regPhone1.value;
        phone = phone.replace(/[^0-9]/g, '');
 	if (form.regPhone1.value=='')
	{	document.getElementById("mobileErrDiv").innerHTML = "Mobile Number cannot be blank";
		mobileValidated = false;
	}else if(phone.length != 10) {
           document.getElementById("mobileErrDiv").innerHTML = "Mobile Number must be in 10 digits";
            mobileValidated = false;
        }

	if (form.regPassword.value=='')
	{
		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}

	if (form.regConfirmPassword.value=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";
		re_passwordValidated = false;
	}
	else if(form.regPassword.value!='')
	{
		if(form.regConfirmPassword.value!=form.regPassword.value)
		{
			document.getElementById("repasswordErrDiv").innerHTML = "Password mismatch Try again!";
			re_passwordValidated = false;
			passwordValidated = false;
		}
	}
	if (form.regagreeterms.checked!=true  )
	{
		document.getElementById("termsErrDiv").innerHTML = "Please Agree the Terms of service ";
		termsValidated = false;
	}
	if(nameValidated && emailValidated && passwordValidated && re_passwordValidated && termsValidated && mobileValidated)
	{
		document.getElementById('reg_form').submit();
	}
	else
	{
		return false;
	}
	
	
}
function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
function iskey_not_allowed(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode >350&& charCode < 0 )
	{
		return true; 
	}
	else
	{
		return false; 
	}
}
function enable_doc_submenu()
{
	document.getElementById("submenu6").style.display="block";
	document.getElementById("submenu8").style.display="block";
}

var deprtmentValidation = false;
var forgotpswvalidated = false;
function add_department_validate(form)
{
	deprtmentValidation = true;
	document.getElementById("dprtmntErrDiv").innerHTML	= "";
        document.getElementById("spemntErrDiv").innerHTML	= "";
        
        if (form.dept.value=='')
	{
   		document.getElementById("dprtmntErrDiv").innerHTML = "Please select department";
                deprtmentValidation = false;
	}        
	if (form.new_speciality.value=='')
	{
   		document.getElementById("spemntErrDiv").innerHTML = "New Speciality Cannot be blank";
                deprtmentValidation = false;
	}
	if(deprtmentValidation)
	{
		document.getElementById('add_department').submit();
	}
	else
	{
		return false;
	}
}


function add_deptment_validate(form)
{
	deprtmentValidation = true;
	document.getElementById("dprtmntErrDiv").innerHTML	= "";
	if (form.new_department.value=='')
	{
   		document.getElementById("dprtmntErrDiv").innerHTML = "New Department Cannot be blank";
                deprtmentValidation = false;
	}
	if(deprtmentValidation)
	{
		document.getElementById('add_department').submit();
	}
	else
	{
		return false;
	}
}


function enable_speciality_submenu()
{
	document.getElementById("submenu52").style.display="block";
}

function enable_department_submenu()
{
	document.getElementById("submenu53").style.display="block";
}

/* ---------------    FORGOT PSW VALIDATION     -------------------*/
function validateforgotpsw(form)
{
	forgotpswvalidated = true;
	document.getElementById("emailErrDiv1").innerHTML	= "";
	if (form.user_email_id.value=='')
	{
   		document.getElementById("emailErrDiv1").innerHTML = "Please Enter a valid Email ID";
    	forgotpswvalidated = false;
	}
	if(forgotpswvalidated && emailvalidate)
	{
		document.getElementById('forgot_password').submit();
	}
	else
	{
		return false;
	}
}
/* ---------------    END FORGOT PSW VALIDATION     -------------------*/