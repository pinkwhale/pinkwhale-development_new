

// JavaScript Document
var usernameValidated = false;
var passwordValidated = false;

function LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
	document.getElementById("username_div").innerHTML = "";
	document.getElementById("password_div").innerHTML = "";
	
	if (form.username.value=='')
	{
   		document.getElementById("username_div").innerHTML = "UserName cannot be blank";
    	usernameValidated = false;
	}
	if (form.password.value=='')
	{
		document.getElementById("password_div").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{
		document.getElementById('signin_pop').submit();
	}
		
	else
	{
		return false;
	}
	
}

function LoginSignUp_ask_query(form)

{
	usernameValidated = true;

	passwordValidated = true;

	document.getElementById("username_div").innerHTML	= "";

	document.getElementById("password_div").innerHTML = "";

	

	if (document.getElementById("uname").value=='')

	{

   		document.getElementById("uname_div").innerHTML = "UserName cannot be blank";

                usernameValidated = false;

	}

	if (document.getElementById("pwd").value=='')

	{

		document.getElementById("pwd_div").innerHTML = "Password cannot be blank";

		passwordValidated = false;

	}

	if(usernameValidated && passwordValidated)

	{

		document.getElementById('signin1').submit();

	}
	else

	{
		return false;

	}
}

function consult_LoginSignUp(form)
{
	
	usernameValidated = true;
	passwordValidated = true;
    document.getElementById("log_errordiv").innerHTML = "";        
	document.getElementById("username_errordiv").innerHTML = "";
	document.getElementById("password_errordiv").innerHTML = "";
	
	if (form.username.value=='')
	{
            
   		document.getElementById("username_errordiv").innerHTML = "UserName cannot be blank";
                usernameValidated = false;
	}
	if (form.password.value=='')
	{
            
		document.getElementById("password_errordiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{   
            
                // -------------------ajax start--------------------- //
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                
				var username = encodeURI(form.username.value);
                var password = encodeURIComponent(form.password.value);
                
                var paramString = 'username='+username+'&password='+password;
	
                $.ajax({  
                        type: "POST",  
                        url: "pw_login_popup.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
									
                                    //loading("loading ",1); 
                                    document.getElementById('consult_signin').submit();
                                    
                                }else{
                                    document.getElementById("log_errordiv").innerHTML = "Invalid login Details";
                                  document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{
                
		return false;
	}
	
}
/*
function consultg_LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
        document.getElementById("log_errordiv").innerHTML = "";        
	document.getElementById("usernam_errordiv").innerHTML = "";
	document.getElementById("passwor_errordiv").innerHTML = "";
	
	if (form.username.value=='')
	{
            
   		document.getElementById("usernam_errordiv").innerHTML = "UserName cannot be blank";
                usernameValidated = false;
	}
	if (form.password.value=='')
	{
            
		document.getElementById("passwor_errordiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{   
            
                // -------------------ajax start--------------------- //
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                
		var username = encodeURI(form.username.value);
                var password = encodeURIComponent(form.password.value);
                
                var paramString = 'username='+username+'&password='+password;
	
                $.ajax({  
                        type: "POST",  
                        url: "pw_login_popup.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    //loading("loading ",1); 
                                    document.getElementById('consultg_signin').submit();
                                    
                                }else{
                                    document.getElementById("log_errordiv").innerHTML = "Invalid login Details";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{
                
		return false;
	}
	
}*/


function forgot_password(form)
{
        
	Validated = true;
        document.getElementById("forgotemailErrDiv").innerHTML = ""; 
        //alert(form.forgot_email.value);
	if (form.forgot_email.value=='')
	{
            
   		document.getElementById("forgotemailErrDiv").innerHTML = "Email cannot be blank";
                Validated = false;
	}
        
	if(Validated)
	{   
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("for_send").value="Validating....";
            
                // -------------------ajax start--------------------- //
                var paramString = 'email='+form.forgot_email.value;

                $.ajax({  
                        type: "POST",  
                        url: "actions/forgot_Passwd.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                //alert(response);
                                if(response==true){
                                    
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully sent password to your Email-id</div>";
                                    
                                }else{                                    
                                    document.getElementById("forgotemailErrDiv").innerHTML = "Invalid Email-ID";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}

function forgot_pass(form)
{
        
	Validated = true;
        document.getElementById("forgotErrDiv").innerHTML = ""; 
        //alert(form.forgot_email.value);
	if (form.forget_email.value=='')
	{
            
   		document.getElementById("forgotErrDiv").innerHTML = "Email cannot be blank";
                Validated = false;
	}
        
	if(Validated)
	{   
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("for_send").value="Validating....";
            
                // -------------------ajax start--------------------- //
                var paramString = 'email='+form.forgot_email.value;

                $.ajax({  
                        type: "POST",  
                        url: "actions/forgot_Passwd.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                //alert(response);
                                if(response==true){
                                    
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully sent password to your Email-id</div>";
                                    
                                }else{                                    
                                    document.getElementById("forgotErrDiv").innerHTML = "Invalid Email-ID";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}

	function register_pink(form)
	{
		Validated = true;        
        document.getElementById("nameErrDiv").innerHTML = ""; 
		document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
		document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = "";         
        
        var name = form.regname.value;
		var age = form.regage.value;
        var gender = form.reggender.value;
        var email = form.regemail.value;
        var password = form.regPassword.value;
        var confirmpassword = form.regConfirmPassword.value;
        var mobile = form.regPhone1.value;
		var card = '';
		
	if ( name =='' )
	{            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
		 if (name != "") {
	 if ( /[^A-Za-z +$]/.test(name)) {
	document.getElementById("nameErrDiv").innerHTML = "Please enter characters only";      	
	form.regname.focus();
	Validated = false;
    }
	}
	if ( age =='' )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
		Validated = false;
	}else if ( age < 18 )
	{            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
		Validated = false;
	}        
        if ( gender =='' )
	{            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
		Validated = false;
	}	
        if ( email =='' )
	{            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
		Validated = false;
	}
	if ( email !='' )
	{
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
			document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";       
			Validated = false;
	}
	}
        if ( password =='' )
	{            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
		Validated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
        if ( password != confirmpassword )
	{            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch Try again!";
		Validated = false;
		repswValidated = false;
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  Validated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  Validated = false;
		 } 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  Validated = false;
		 }
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 Validated = false;
		 }
			
	}
        if( mobile == "" )
	{            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
		Validated = false;
	} 
	if(Validated)
	{               
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("btnSubmit").value = "Processing....";            
                // -------------------ajax start--------------------- //                
                name = encodeURI(name);
		age = encodeURI(age);
                gender = encodeURI(gender);
		card = encodeURI(card);
                email = encodeURI(email);
                password = encodeURIComponent(password);
                mobile = encodeURI(mobile);                
                var paramString = 'name='+name+'&age='+age+'&gender='+gender+'&email='+email+'&password='+password+'&mobile='+mobile+'&regcard='+card;

                $.ajax({  
                        type: "POST",  
                        url: "actions/pw_regpink.php",  
                        data: paramString,  
                        success: function(response) {  
	                		//alert(response);            
                                if(response==true){
                                  
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("btnSubmit").value = "Register";
                                    //document.getElementById("login_message").innerHTML="<div align='center'>Successfully Register<br />Please check your mail to complete registration.</div>";
                                    alert("Successfully Registered");
									  window.location.reload();
                                }else{       
                                    
                                    document.getElementById("emailErrDiv").innerHTML= response;
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;                                    
                                }
                        }
                });
                return false;
                // -------------------ajax End--------------------- //                
		}		
		else
		{                
			return false;
		}	
	}

	 
	function validate_pswd(form)
{
	var pswValidated = false;
	var repswValidated = false;
	var password = form.regPassword.value;
    var confirmpassword = form.regConfirmPassword.value;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("passwordErrDiv").innerHTML = ""; 
    document.getElementById("repasswordErrDiv").innerHTML = ""; 
	if (password=='')
	{
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";		
    	pswValidated = false;
	}
	if (confirmpassword=='')
	{
		document.getElementById("repasswordErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(confirmpassword!='')
	{
		if(password!=confirmpassword)
		{
			document.getElementById("repasswordErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (password!='' )	{	
		if(password.length < 6) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			 pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(password)) {
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			 pswValidated = false;
		} 
		 re = /[a-z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			 pswValidated = false;
		}
		 re = /[A-Z]/;
		 if(!re.test(password)) { 
			 document.getElementById("passwordErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		}
			 pswValidated = true;
	}
	 	
}

function check_mobile(){
    
    var mobile = document.getElementById("regPhone1").value;
    mobile = encodeURI(mobile);
    
    if(mobile!="" && mobile!=null){
        var paramString = 'mobile='+mobile;
        
        $.ajax({  
                type: "POST",  
                url: "check_mobile.php",  
                data: paramString,  
                success: function(response) { 
                    if(response==true){
                        document.getElementById("mobileErrDiv").innerHTML = "";
                    }else{
                        document.getElementById("regPhone1").value = "";
                        document.getElementById("mobileErrDiv").innerHTML = "Mobile Number Already Exis";
                    }
                }
        });
    }
}

function card_check(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("cardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: "check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        
                            if(response==true){
                                document.getElementById("cardErrDiv").innerHTML= "<font color='green'>Valid Card</font>";
                                return false;
                            }else{
                                document.getElementById("regcard").value= "";
                                document.getElementById("cardErrDiv").innerHTML= response;
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //        
}
function Topup_login(form)
{
        
	validate = true;
        
	
	document.getElementById("topuplog_errordiv").innerHTML= "";
	
        
                
	if (form.card.value=='')
	{
                
   		document.getElementById("topuplog_errordiv").innerHTML = "Card No. cannot be blank";
                validate = false;
	}
        
	
	if(validate)
	{
            document.getElementById("topupbtn").value="loading";
            document.getElementById("topupbtn").disabled=true;
            
            // -------------------ajax start--------------------- //
                var paramString = 'card='+form.card.value;

                $.ajax({  
                        type: "POST",  
                        url: "card_login_check.php",  
                        data: paramString,  
                        success: function(response) {         
                                if(response==true){
                                    
                                    document.getElementById('TopUPlogin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("topuplog_errordiv").innerHTML = "Invalid card No.";
                                    document.getElementById("topupbtn").value="Top Up";
                                    document.getElementById("topupbtn").disabled=false; 
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
	}
	else
	{
		return false;
	}
	
}

function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}


function validate_consult(type){
    
    document.getElementById("login_nxt_page").value=type;
    document.getElementById("register_nxt_page").value=type;
        
    setTimeout(login_callpopup(true,0),200);
    
}
