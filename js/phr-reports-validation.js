var reportNameValidated = false;
var reportTypeValidated = false;
var uploadFileValidated = false;
function uploadfile(form)
{

	reportNameValidated = true;
	reportTypeValidated = true;
	uploadFileValidated = true;
	document.getElementById("repo_nameErrDiv").innerHTML   = "";
	document.getElementById("report_typeErrDiv").innerHTML = "";
	document.getElementById("uploadErrDiv").innerHTML = "";
	if (form.repo_name.value=='')
	{
   		document.getElementById("repo_nameErrDiv").innerHTML = "Please enter your report name";
    	reportNameValidated = false;
	}
	if (form.report_type.value=='')
	{
		document.getElementById("report_typeErrDiv").innerHTML = "Please enter report type ";
		reportTypeValidated = false;
	}
	
	if (form.uploadedfile.value=='')
	{
		document.getElementById("uploadErrDiv").innerHTML = "Please upload your file";
		uploadFileValidated = false;
	}
	
	if(reportNameValidated && reportTypeValidated && uploadFileValidated)
	{
		

		document.getElementById('upload_file_form').submit();
	}
	else
	{
		return false;
	}
}