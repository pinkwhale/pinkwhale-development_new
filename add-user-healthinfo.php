<?php 

	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>

<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<!-- header.......--><?php
include 'header.php'; 
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php"><font color="#ea0977"><strong>My Account</strong></font></a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- header.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- header.......-->
</td>
<td width="748" valign="top" class="s90phrcontent">
<form action="actions/add_user_health_info.php" method="post" name="user_health_info" id="user_health_info">
<input type="hidden" name="user_id_save" value="<?php echo $user_id ; ?>" />
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:20px">
<tr><th>Edit Health Info:</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="551" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
<tr><td width="87">Date Of Birth: </td>
<td width="149"><?php
	  $myCalendar = new tc_calendar("date5", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d'), date('m'), date('Y'));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
<td width="147"><strong>Medical Device/Implants:</strong></td>
<td width="168"><input type="text" name="medical_decice_implants" id="medical_decice_implants"/></td>
</tr>
<tr>
<td>Gender: </td><td><select  name="user_gender" id="user_gender" > 
    	<option value="">-Select-</option>
        <option value="male">Male</option>
        <option value="Female">Femail</option>
        </select></td>
<td><strong>Blood Group:</strong> </td><td>
	<select  name="blood_group" id="blood_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select></td></tr>
<tr><td>Height: </td><td><input type="text" name="height" size="3" id="height"/>cm</td>
<td><strong>Blood Sugar:</strong> </td><td><input type="text" name="blood_sugar" id="blood_sugar" size="5"/>mg/dL</td></tr>
<tr><td>Weight: </td><td><input type="text" name="weight" size="3" id="weight"/>Kg</td>
<td><strong>Cholestrol:</strong> </td><td><input type="text" name="cholestrol" id="cholestrol" size="5"/>mmol/L</td></tr>
<tr><td>Marital Status: </td><td><select  name="user_merit_status" id="user_merit_status" > 
    	<option value="">-Select-</option>
        <option value="Single">Single</option>
        <option value="Married">Married</option>
           <option value="Divorced">Divorced</option>
        <option value="Married">Separated </option>
           <option value="Widow">Widow</option>
        </select></td>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="blood_pressur" id="blood_pressur" size="5"/>mmHg</td></tr></table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:20px">
<tr><th>Fathers Health Info:</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="559" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
<tr>
<td width="94"><b>Name: </b></td>
<td width="167"><input type="text" name="father_name" id="father_name"  /></td>
<td width="141"><strong>Medical Device/Implants:</strong></td>
<td width="157"><input type="text" name="father_medical_device" id="father_medical_device"  /></td>
</tr>
<tr>
<td width="94"><b>Age: </b></td>
<td width="167"><input type="text" name="father_age" id="father_age" size="5"/></td>
<td><strong>Blood Group:</strong></td>
<td>
	<select  name="father_boold_group" id="father_boold_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select></td>
<!--<td><input type="text" name="father_boold_group" id="father_boold_group"  size="5"/></td>-->
</tr>
<tr>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="father_boold_pressure" id="father_boold_pressure" size="5"/>mmHg</td>
<td><strong>Blood Sugar:</strong> </td>
<td><input type="text" name="father_blood_sugar" id="father_blood_sugar"  size="5"/>mg/dL</td>
</tr>
<tr><td><b>Major Illness: </b></td><td><input type="text" name="father_major_illness" id="father_major_illness" /></td>
<td><strong>Cholestrol:</strong> </td>
<td><input type="text" name="father_cholesrol" id="father_cholesrol" size="5"/>mmol/L</td>
</tr>
<tr><td colspan="0"><b>Remarks: </b></td><td colspan="3"><textarea name="father_remarks" id="father_remarks" class="registetextbox1"></textarea></td>
</tr>
</table>
</td></tr></table>

<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:20px">
<tr><th>Mothers Health Info:</th></tr>
<tr><td class="s90dbdtbls_drdetails">
<table width="558" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
<tr>
<td width="93"><b>Name: </b></td>
<td width="165"><input type="text" name="mother_name" id="mother_name"  /></td>
<td width="145"><strong>Medical Device/Implants:</strong></td>
<td width="155"><input type="text" name="mother_medical_device" id="mother_medical_device"  /></td>
</tr>
<tr>
<td width="93"><b>Age: </b></td>
<td width="165"><input type="text" name="mother_age" id="mother_age" size="5"/></td>
<td><strong>Blood Group:</strong> </td>

<td>
	<select  name="mother_boold_group" id="mother_boold_group" > 
    	<option value="">-Select-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        </select></td>


<!--<td><input type="text" name="mother_boold_group" id="mother_boold_group"  size="5"/></td>-->
</tr>
<tr>
<td><strong>Blood Pressure:</strong> </td><td><input type="text" name="mother_boold_pressure" id="mother_boold_pressure" size="5"/>mmHg</td>
<td><strong>Blood Sugar:</strong> </td>
<td><input type="text" name="mother_blood_sugar" id="mother_blood_sugar"  size="5"/>mg/dL</td>
</tr>
<tr><td><b>Major Illness: </b></td><td><input type="text" name="mother_major_illness" id="mother_major_illness" /></td>
<td><strong>Cholestrol:</strong> </td>
<td><input type="text" name="mother_cholesrol" id="mother_cholesrol" size="5"/>mmol/L</td>
</tr>
<tr><td colspan="0"><b>Remarks: </b></td><td colspan="3"><textarea name="mother_remarks" id="mother_remarks" class="registetextbox1"></textarea></td>
</tr>
</table>
</td></tr>
</table>


<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:20px">
<tr>
<td width="44" colspan="2" align="right" style="margin-left:10px;"> <input type="submit" name="save_user_prof" value="Save" id="save_user_prof"/></td>
<td colspan="2" align="left" style="margin-right:10px;"><a href="phr_healthinformation.php"><input type="button" name="cancel" value="Cancel"/></a></td>

</tr>
</table>
</form>

</table>
</td></tr>

</table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>