<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pinkwhale_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',DL?~h8r?>F$R!$:xc-V`HF-Lmpt)-Xu}mGELCQR$dHGnk2P<@`,ZLudWvLY)Q-D');
define('SECURE_AUTH_KEY',  ']cq5*7)EBogn0@w8Q^/:T6{H=`x*[PrAEnVk*f@P+g%qc<jz=0h)L^2t-{{{|7X^');
define('LOGGED_IN_KEY',    '_/&<L{{!jv!_wu~.-v%G@Q&8d]wrz|=Q~wl@xOt:7$0O9wxi jjk +f<ww*ij&&*');
define('NONCE_KEY',        'O<Arzj0[|/K>+j=wcKxK.&d|r`y]F-|Aai_Q<>u>WsrhL:(^=BlM?k=g(r[7J$i_');
define('AUTH_SALT',        'YF|]+-}98l+M|(jX?t_cZa0a3Y:/: 88G=9fiM]V[{n}=odr_mnC<XZN)]2(hSkW');
define('SECURE_AUTH_SALT', '2F+WN*i8B,5~hoq~bD7xgH[Jk|xbjJ!8?l-EJ,dE?FtJ:a3{JlN0R_BN;gj(T7<.');
define('LOGGED_IN_SALT',   '!a3n`gw-VB4cZX-_p)Y36VTc82TiwH))ty8ChhK)a/Q:B1M++8uh6%?+.;6Qw<K1');
define('NONCE_SALT',       'a?ngmaW3<R-DsanxmGPu</lWE5~]o@nh|tsdeuZD6kjeD}G_&r[lF$pMP&|GU!L?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
