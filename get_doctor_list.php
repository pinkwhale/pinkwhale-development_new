<?php

include 'db_connect.php';

include 'modify_doctorname.php';
include 'includes/site_config.php';
$doc_type = urldecode($_POST['doc_type']);

$type = urldecode($_POST['type']);

//$spe_type = ($_POST["spe_type"] <> "") ? trim($_POST["spe_type"]) : "";

//if(isset($_POST['spe_type'])){ $spe_type = $_POST['spe_type']; } 

//$spe_type = urldecode($_POST['spe_type']);
$spe_type = isset($_POST['spe_type']) ? $_POST['spe_type'] : '';

$extend = "";

/******
 *    Pink Query(General Physician) ----->  1
 *    Pink Query(Specialist)        ----->  2
 *    Pink Opinion                  ----->  3
 *    Pink Consult                  ----->  4 
 * 
 ******/

if($type=="2"){
    $output = "<br /><div id='docErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px;text-align: center'></div>
                <img src='images/choose_specialist.png' align='absmiddle' />
                <div id='docTop'>
                <div id='docTbl'>";
   
    if($spe_type!=""){
    
            $extend = " and doc_specialities='$spe_type'";

    }
    
    $qry = "select city,doc_id,doc_name,doc_photo,more_expertise,doc_qualification,doc_specialities,about_doctor,doc_specialist_for from pw_doctors where blocked<>'Y' and doc_specialities not like'%Physician%'  and activate_online_query='Query' $extend group by doc_id order by doc_name";
    $reslt = mysql_query($qry);
    $num = mysql_num_rows($reslt);
    if ($num > 0) {

        while ($data = mysql_fetch_array($reslt)) {
            
            $doctor_id = $data['doc_id'];
            $doctor_name = $data['doc_name'];
            $qualification = $data['doc_qualification'];
            $speciality = $data['doc_specialities'];  
            $specialist_for = $data['doc_specialist_for'];
            $doc_image = $data['doc_photo'];  
            $about_doc = $data['about_doctor'];
            $more_exterpise  = $data['more_expertise'];  
            $expert = "";
            if($more_exterpise!=""){   
              $expert = "<strong>Core Expertise</strong> : <br />";
              $more_exterpise_tmp = explode(";", $more_exterpise) ;
                  foreach($more_exterpise_tmp as $more_exp){
                     $expert .="* ".$more_exp."<br />";
             	  }
            }
            if ($doc_image == "") {
                $doc_image = "images/doctor.png";
            }

	    $doc_url_name = modifyname($doctor_name);
            $location = $data['city'];
            $doc_url_city = encode_city($location);
            $doc_url_spe = encode_city($speciality);
            $url = $site_url."doctorsdomain.php?doc_id=".$doctor_id."/doctors/".$doc_url_spe."/".$location."/".$doc_url_name;
            
            $display = "";
                                                
            $display .= "<strong>Qualification</strong> &nbsp;:&nbsp;$qualification<br />";

            $display .= "<strong>Specialization</strong> &nbsp;:&nbsp;$speciality<br />";

            $display .= $expert;


            $output .="<div class='docContainer' style='cursor:pointer' onclick=\"update_doctor('$doctor_id')\">
              <div class='docPhoto'>
                    <a href='#' title='<img src=$site_url/$doc_image width=73 height=83 /><strong>$doctor_name</strong>' data-content='$display' >
                      <img src='$doc_image' width='70' height='70' />
                     </a> 
              </div>
              <div class='docDis'>
              <strong>$doctor_name  <br /> $qualification </strong><br />$speciality <br />
              </div>
	      <div><a href='$url'>Read more</a></div>
              </div>";

        }
    }

}else if($type=="1"){
     $output = "<br /><div id='docErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px;text-align: center'></div>
                <img src='images/choose_specialist.png' align='absmiddle' />
                <div id='docTop'>
                <div id='docTbl'>";

    $qry = "select city,doc_id,more_expertise,doc_name,doc_photo,doc_qualification,doc_specialities,about_doctor,doc_specialist_for from pw_doctors where blocked<>'Y' and doc_specialities like'%Physician%'  and activate_online_query='Query'  group by doc_id order by doc_name";
    $reslt = mysql_query($qry);
    $num = mysql_num_rows($reslt);
    if ($num > 0) {

        while ($data = mysql_fetch_array($reslt)) {
            
            $doctor_id = $data['doc_id'];
            $doctor_name = $data['doc_name'];
            $qualification = $data['doc_qualification'];
            $speciality = $data['doc_specialities'];  
            $specialist_for = $data['doc_specialist_for'];
            $doc_image = $data['doc_photo'];  
            $about_doc = $data['about_doctor'];
            $more_exterpise  = $data['more_expertise'];
            $expert = "";
            if($more_exterpise!=""){
              $expert = "<strong>Core Expertise</strong> : <br />";
              $more_exterpise_tmp = explode(";", $more_exterpise) ;
                  foreach($more_exterpise_tmp as $more_exp){
                     $expert .="* ".$more_exp."<br />";
                  }
            }

            if ($doc_image == "") {
                $doc_image = "images/doctor.png";
            }
            
	    $doc_url_name = modifyname($doctor_name);
            $location = $data['city'];
            $doc_url_city = encode_city($location);
            $doc_url_spe = encode_city($speciality);
            $url = $site_url."doctorsdomain.php?doc_id=".$doctor_id."/doctors/".$doc_url_spe."/".$location."/".$doc_url_name;

            $display = "";
                                                
            $display .= "<strong>Qualification</strong> &nbsp;:&nbsp;$qualification<br />";

            $display .= "<strong>Specialization</strong> &nbsp;:&nbsp;$speciality<br />";

            $display .= $expert;


            $output .= "<div class='docContainer' style='cursor:pointer' onclick=\"update_doctor('$doctor_id')\">
                  <div class='docPhoto'>
                        <a href='#' title='<img src=$site_url/$doc_image width=73 height=83 /><strong>$doctor_name</strong>' data-content='$display' >
                          <img src='$doc_image' width='70' height='70' />
                         </a> 
                  </div>
                  <div class='docDis'>
                  <strong>$doctor_name  <br /> $qualification </strong><br />$speciality <br />
                  </div>
		  <div><a href='$url'>Read more</a></div>
                  </div>";

        }
    }

}else if($type=="3"){
     $output = "<br /><div id='docErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px;text-align: center'></div>
                <img src='images/choose_specialist.png' align='absmiddle' />
                <div id='docTop'>
                <div id='docTbl'>";

    $qry = "select city,more_expertise,doc_id,doc_name,doc_photo,doc_qualification,doc_specialities,about_doctor,doc_specialist_for from pw_doctors where (`doc_category`='Expert' || `doc_category`='Specialist/Expert' ) && blocked<>'Y'  and doc_specialities='$doc_type' group by doc_id order by doc_name";
    $reslt = mysql_query($qry);
    $num = mysql_num_rows($reslt);
    if ($num > 0) {

        while ($data = mysql_fetch_array($reslt)) {
            $doctor_id = $data['doc_id'];
            $doctor_name = $data['doc_name'];
            $qualification = $data['doc_qualification'];
            $speciality = $data['doc_specialities'];  
            $specialist_for = $data['doc_specialist_for'];
            $doc_image = $data['doc_photo'];  
            $about_doc = $data['about_doctor'];
            $more_exterpise  = $data['more_expertise'];
            $expert = "";
            if($more_exterpise!=""){
              $expert = "<strong>Core Expertise</strong> : <br />";
              $more_exterpise_tmp = explode(";", $more_exterpise) ;
                  foreach($more_exterpise_tmp as $more_exp){
                     $expert .="* ".$more_exp."<br />";
                  }
            }

            if ($doc_image == "") {
                $doc_image = "images/doctor.png";
            }

	    $doc_url_name = modifyname($doctor_name);
            $location = $data['city'];
            $doc_url_city = encode_city($location);
            $doc_url_spe = encode_city($speciality);
            $url = $site_url."doctorsdomain.php?doc_id=".$doctor_id."/doctors/".$doc_url_spe."/".$location."/".$doc_url_name;
            
            $display = "";
                                                
            $display .= "<strong>Qualification</strong> &nbsp;:&nbsp;$qualification<br />";

            $display .= "<strong>Specialization</strong> &nbsp;:&nbsp;$speciality<br />";

            $display .= $expert;


            $output .= "<div class='docContainer' style='cursor:pointer' onclick=\"update_doctor('$doctor_id')\">
                  <div class='docPhoto'>
                        <a href='#' title='<img src=$site_url/$doc_image width=73 height=83 /><strong>$doctor_name</strong>' data-content='$display' >
                          <img src='$doc_image' width='70' height='70' />
                         </a> 
                  </div>
                  <div class='docDis'>
                  <strong>$doctor_name  <br /> $qualification </strong><br />$speciality <br />
                  </div>
				  <div><a href='$url'>Read more</a></div>
                  </div>";

        }
    }

}else if($type=="4"){
     $output = "<br /><div id='docErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px;text-align: center'></div>
                <img src='images/choose_specialist.png' align='absmiddle' />
                <div id='docTop'>
                <div id='docTbl'>";

    $qry = "select city,doc_id,more_expertise,doc_name,doc_photo,doc_qualification,doc_specialities,about_doctor,doc_specialist_for from pw_doctors where (`doc_category`='Specialist' || `doc_category`='Specialist/Expert' ) && blocked<>'Y'  and (activate_online_consultation='online' || activate_tele_Consultation='tele') and doc_specialities='$doc_type' group by doc_id order by doc_name";
    $reslt = mysql_query($qry);
    $num = mysql_num_rows($reslt);
    if ($num > 0) {

        while ($data = mysql_fetch_array($reslt)) {
            $doctor_id = $data['doc_id'];
            $doctor_name = $data['doc_name'];
            $qualification = $data['doc_qualification'];
            $speciality = $data['doc_specialities'];  
            $specialist_for = $data['doc_specialist_for'];
            $doc_image = $data['doc_photo'];  
            $about_doc = $data['about_doctor'];
            if ($doc_image == "") {
                $doc_image = "images/doctor.png";
            }
            $more_exterpise  = $data['more_expertise'];
            $expert = "";
            if($more_exterpise!=""){
              $expert = "<strong>Core Expertise</strong> : <br />";
              $more_exterpise_tmp = explode(";", $more_exterpise) ;
                  foreach($more_exterpise_tmp as $more_exp){
                     $expert .="* ".$more_exp."<br />";
                  }
            }

	    $doc_url_name = modifyname($doctor_name);
            $location = $data['city'];
            $doc_url_city = encode_city($location);
            $doc_url_spe = encode_city($speciality);
            $url = $site_url."doctorsdomain.php?doc_id=".$doctor_id."/doctors/".$doc_url_spe."/".$location."/".$doc_url_name;

            $display = "";
                                                
            $display .= "<strong>Qualification</strong> &nbsp;:&nbsp;$qualification<br />";

            $display .= "<strong>Specialization</strong> &nbsp;:&nbsp;$speciality<br />";

            $display .= $expert;


            $output .= "<div class='docContainer' style='cursor:pointer' onclick=\"update_doctor('$doctor_id')\">
                  <div class='docPhoto'>
                        <a href='#' title='<img src=$site_url/$doc_image width=73 height=83 /><strong>$doctor_name</strong>' data-content='$display' >
                          <img src='$doc_image' width='70' height='70' />
                         </a> 
                  </div>
                  <div class='docDis'>
                  <strong>$doctor_name  <br /> $qualification </strong><br />$speciality <br />
                  </div>
		  <div><a href='$url'>Read more</a></div>
                  </div>";

        }
    }

}

echo $output;
?>
