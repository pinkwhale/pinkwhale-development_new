<?php include "includes/start.php" ?>
<?php include "includes/header.php" ?>
<?php include "includes/db_connect.php" ?>
		<link href="css/doctorsdomain.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="meet-our-doctors" data-pink-name="pink-followup"></div>
	<?php include "includes/menu-patients.php";	?>
	
						<?php		
						$id=$_GET['doc_id'];
						$qry1= "SELECT * FROM `pw_doctors` WHERE `doc_id`='$id'";
							$qry_rslt1 = mysql_query($qry1);
							while($result1 = mysql_fetch_array($qry_rslt1))
							{								
						?>	
		
	
	 <div class="banner-bg banner-bg-doctors-domain ">
		<div class="content-area ">
			<div class="red-text hidden-xs hidden-sm">
				<i>
					Polio Camp at Ideal Child Clinic<br>on <strong>24th Dec, 2014</strong><br><br>
					Come with your child and<br>
					make a difference to their lives
				</i>
			</div>
		</div>
	</div> 
	

	<div class="our-bouquet-container widgets-row">						
		<div class="content-area">
			<div class="header"><?php echo strtoupper($result1['doc_name']);?> OFFERS YOU THESE <span class="hidden-xs"><br></span> VIRTUAL SERVICES FOR ENHANCED CARE</div>
			<div class="clearfix"></div>

			<div class="widgets-image-container">
				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkQuery.php">
						<img src="img/home/patients/Patients_Icon_PinkQuery.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Query</strong></div>
						<div class="hr-container"><hr></div>
						<!-- <img src="img/home/patients/Patients_Icon_PinkLine_for Service Buttons.png" class="line"> -->
						<div class="widget-text">Our Doctors are available<br>now to answer your<br>questions and give advice.</div>
					</a>
				</div>

				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkAppoint.php">
						<img src="img/home/patients/Patients_Icon_pinkAppoint.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Appoint</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our synced calendar<br>makes it simple to<br>schedule doctor visits.</div>
					</a>
				</div>

				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkfollowup-online.php">
						<img src="img/home/patients/Patients_Icon_pinkFollowUp.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>FollowUp</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our Virtual Follow-ups<br>make connecting with<br>your doctor fast & easy.</div>
					</a>
				</div>

				<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
					<a href="pinkOpinion.php">
						<img src="img/home/patients/Patients_Icon_pinkOpinion.png" class="gradient2-background">
						<div class="widget-name"><i>pink</i><strong>Opinion</strong></div>
						<div class="hr-container"><hr></div>
						<div class="widget-text">Our credible second<br>opinions help you make<br>the best health decisions.</div>
					</a>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>



	
	<div class="page-row doctors-domain row-bg-1 hidden-xs">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-4 col-xs-12">
				<img src="<?php echo $result1['doc_photo'];?>" width="260" height="100" style="border-radius:100px;"><br>
				<span class="red-text">Health Tips from <?php echo $result1['doc_name'];?>!</span><br>
				Receive Useful Health Tips,<br>
				Updates & lots more via Email!<br><br>
				<?php
				echo '<a href="subscribe2.php?doc_id='.$_GET['doc_id'].'" class="doctor-button blue">Subscribe</a>';
			?>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-8 col-xs-12">
				<div class="details">
					<div class="name"><?php echo $result1['doc_name'];?></div>
					<div class="designation red-text"><?php echo $result1['doc_specialities'];?></div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
					<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
				</div>
				<div class="clearfix"></div>
				<div class="message">
					<?php //echo $result1['doc_specialities'];?>
				</div>

				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Services.png">
						</div>
						<div class="info-data">
							<div class="red-text">SERVICES</div>
							<?php echo nl2br($result1['more_expertise']);?><br>
							
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image"></div>
						<div class="info-data second-info">
							<div class="red-text"><br></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Child-Clinic.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_clinic_name'];?></div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Hospital.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_hospital_name'];?></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-12 col-md-12 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Education.png">
						</div>
						<div class="info-data">
							<div class="red-text">EDUCATION</div>
							<?php echo $result1['doc_qualification'];?><br>
							
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Membership.png">
						</div>
						<div class="info-data">
							<div class="red-text">MEMBERSHIPS</div>
							Indian Academy of<br>
							Paediatrics (IAP)<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Registration.png">
						</div>
						<div class="info-data">
							<div class="red-text">REGISTRATIONS</div>
							85652 Karnataka Medical Council<br>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="page-row doctors-domain row-bg-1 visible-xs">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-12 col-xs-12">
				<img src="<?php echo $result1['doc_photo'];?>"><br>
				<div class="details">
					<div class="name"><?php echo $result1['doc_name'];?></div>
					<div class="designation red-text"><?php echo $result1['doc_specialities'];?></div>
					<div class="clearfix"></div>
				</div>								
			</div>
			<div class="clearfix"></div>
			<div class="calendar text-center">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
			</div>

			<div class="message">
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
				Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem
				Ipsum is simply dummy text of the printing and typesetting industry..
			</div>
			

			<div class="data-block col-xl-8 col-md-8 col-sm-12 col-xs-12">								
				<div class="clearfix"></div>				

				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Services.png">
						</div>
						<div class="info-data">
							<div class="red-text">SERVICES</div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image"></div>
						<div class="info-data second-info">
							<div class="red-text"><br></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Child-Clinic.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_clinic_name'];?></div>
							Consultation<br>
							Newborn Jaundice<br>
							Nutrition Assessment<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Hospital.png">
						</div>
						<div class="info-data">
							<div class="red-text"><?php echo $result1['visit_hospital_name'];?></div>
							New Born Care<br>
							Vaccination<br>
							Respiratory Tract Infection<br>
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-12 col-md-12 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Education.png">
						</div>
						<div class="info-data">
							<div class="red-text">EDUCATION</div>
							<?php echo $result1['doc_qualification'];?><br>
							
						</div>
					</div>
				</div>


				<div class="info-block">
					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Membership.png">
						</div>
						<div class="info-data">
							<div class="red-text">MEMBERSHIPS</div>
							Indian Academy of<br>
							Paediatrics (IAP)<br>
						</div>
					</div>

					<div class="info info-left col-xl-6 col-md-6 col-sm-12 col-xs-12">
						<div class="info-image">
							<img src="img/meetourdoctors/doctorsdomain/DoctorsDomain_Icon_Registration.png">
						</div>
						<div class="info-data">
							<div class="red-text">REGISTRATIONS</div>
							85652 Karnataka Medical Council<br>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<!-- <span class="form-tagline">Book a Clinic Appointment with Dr. Vanitha here.</span> -->
				<span class="form-tagline">Book a Clinic Appointment here!</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form">
						

						<div class="form-group">
							<label for="dname" class="col-xs-6 control-label">Doctor Name:</label>
							<div class="col-xs-6">
								<p class="form-control-static"><?php echo $result1['doc_name'];?></p>
							</div>
						</div>	
						<div class="form-group">
							<label for="pname" class="col-xs-6 control-label">Patient Name:</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="pname" name="pname">
							</div>
						</div>						
						<div class="form-group">
							<label for="age" class="col-xs-6 control-label">Age:</label>
							<div class="col-xs-6">
								<input type="number" class="form-control" id="age" name="age">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-xs-6 control-label">Email:</label>
							<div class="col-xs-6">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>	
						<div class="form-group">
							<label for="mobno" class="col-xs-6 control-label">Mobile No:</label>
							<div class="col-xs-6">
								<input type="number" class="form-control" id="mobno" name="mobno">
							</div>
						</div>
						<div class="form-group">
							<label for="gender" class="col-xs-6 control-label">Gender:</label>
							<div class="col-xs-6">
								<select class="form-control">
									<option selected disabled>--- Select Gender --</option>
									<option>Male</option>
									<option>Female</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-xs-6 control-label">Appointment Time:</label>
							<div class="col-xs-6">
								<p class="form-control-static" id="bookonline-time-slot"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-xs-6 control-label">New Patient:</label>
							<div class="col-xs-6">
								<select class="form-control">
									<option selected disabled>--- Select Type ---</option>
									<option>First Time Appointment</option>
									<option>Follow up</option>
								</select>
							</div>
						</div>

						<div class="calendar-container">
							<div class="arrow-1 arrow-left-1"></div>
							<div id="arrow-right" class="arrow-1 arrow-right-1"></div>
							<div class="booking-table-container">
								<table class="booking-table">
									<thead>
										<tr>
											<?php for($i=0;$i<=29;$i++){ ?>
			
											<th data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><?php echo date('D',$ts);?><br><span class="small-header"><?php echo date('M j',$ts);?></span></th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php for($x=0;$x<=4;$x++){?>
										<tr>
											<?php for($i=0;$i<=29;$i++){
												
												$qry = "select substr(from_time,12) from_time,DATE_FORMAT(from_time,'%h:%i %p') from doctor_time_slots 
												where doc_id='8393' and Day='4' order by from_time";

												$res = mysql_query($qry);
												while($data = mysql_fetch_array($res)){	
													
											?>
											<td class="time-value time-block" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><span><?php echo $data[1];?></span></td>
																				
											<?php }}?>
										</tr>
										<?php } ?>
										<tr>
											<?php for($i=0;$i<=29;$i++){ ?>
											<td class="more-value"><span>More..</span></td>
											<?php } ?>
										</tr>
									</tbody>									
								</table>
							</div>

						</div>	

						<div class="form-group">
							<div class="col-md-6">
							</div>
							<div class="col-md-6">
								<input type="submit" class="pw-btn" value="Book">
								<input type="submit" class="pw-btn" value="Cancel">
							</div>
						</div>									
					</form>
				</div>	
							
			</div>
			
			<div class="message form-right col-sm-4 hidden-xs">

				Why wait on hold on the phone <br>
				to schedule an appointment <br>
				when you can do it online<br>
				without wasting any time?<br> <br>
				Use pinkAppoint to book a  <br>
				Clinic appointment.  You don't<br>
				even need to register to<br>
				schedule one!<br>
			</div>
		</div>
	</div>
	
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>

	<script>
		$(document).ready(function(){
			$('.arrow-right-1').click(function(){
				if($('table.booking-table').css('left') == '-2000px')
					return;
				var tablePos=$('.booking-table').position();
				var thwidth=$('.booking-table tr th').width();
				$('.booking-table').css("left",tablePos.left-80);
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.arrow-left-1').click(function(){
				console.log($('table.booking-table'));
				console.log(document.getElementsByClassName('booking-table'));

				if($('table.booking-table').css('left') >= '0px' || $('table.booking-table').css('left') == 'auto')
					return;
				var tablePos=$('.booking-table').position();
				$('.booking-table').css("left",tablePos.left+80);
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.arrow-right-1').click(function(){
				
			});
		});
	</script>
	<script>
		$(function(){
			/*var text=$('.booking-table tr:nth-child(2) td:first-child').data('ts');
			var d=new Date(text*1000);
			$("#bookonline-time-slot").val(d);*/
				styleActiveTime();
				getTimeDate();	
				
		});	 
		function getTimeDate(){				
			 $('.booking-table tbody tr .time-value').click(function(){
				var getTime=$(this).text();
				var getTs=$(this).data('ts');
				
					var d=new Date(getTs*1000);
				var getDate=d.getDate();
				var getMonth=d.getMonth();
				var getYear=d.getFullYear();
				var monthName=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
				
				/*$('#bookonline-time-slot').val(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);*/
				
				$('#bookonline-time-slot').text(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);
			});
		}	

		function styleActiveTime(){
			$('.booking-table tbody tr .time-block').click(function(){
				$(this).each(function(){
					$('.booking-table tbody tr .time-block').removeClass("active-time");
				});
				$(this).addClass("active-time");
			});
		}

		function getNewTimeDate(){				
			 $('.booking-table tbody tr .new-time-value').click(function(){
				var getTime=$(this).text();
				var getTs=$(this).data('ts');
				
					var d=new Date(getTs);
				
				var getDate=d.getDate();
				var getMonth=d.getMonth();
				var getYear=d.getFullYear();
				var monthName=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
				
				/*$('#bookonline-time-slot').val(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);*/
				
				$('#bookonline-time-slot').text(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);
			});
		}	  

	</script>
	<script>
		$(function(){
			
			$('.booking-table tbody tr:nth-child(6)').click(function(){
				$(this).css("display","none");
				var d=new Date();
				var tss=d.getTime();
				var html="";
				for(var j=0;j<=2;j++){
					html+='<tr>';
					for(var i=0;i<=29;i++){
						var currentTs=tss+3600*24*1000*i;
						html+='<td class="new-time-value time-block" data-ts='+currentTs+'><span>09:30am</span></td>';
					}
					html+='</tr>';
				}	
				$(html).appendTo(".booking-table");
				styleActiveTime();
				getNewTimeDate();
			});

		});
	</script>

<?php } ?>
<?php include "includes/end.php";
/*
function get_week($d){
	$day=0;

	switch($d){
		case "Mon" : $day = 1;break;
		case "Tue" : $day = 2;break;
		case "Wed" : $day = 3;break;
		case "Thu" : $day = 4;break;
		case "Fri" : $day = 5;break;
		case "Sat" : $day = 6;break;
		case "Sun" : $day = 7;break;
	}

	return $day;

}*/

 ?>
