
 <!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Ask our Physician – Have health related questions? Get your queries answered today. 
	Ask our Physician/Specialist regarding your concern.">
	<meta name="keywords" content="Ask your physician, ask medical questions online, online physician, ask health questions, ask a physician,
	ask a doctor online, find a physician, talk to doctor online, ">
    <title>Ask your Physician Online | pinkWhale Healthcare</title>
    <?php include 'includes/include-css.php'; ?>
  </head>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<?php include "includes/site_config.php"?>
	<link href="css/findadoctor.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="meet-our-doctors" data-pink-name="pink-followup"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-meet-our-doctors">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Make the right choice for<br>your loved ones!</i></div>
		</div>
	</div>


	<div class="find-a-doctor-row">	
		<div class="content-area find-a-doctor-form-container">
			<div class="header">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_FindDoctor.png">
				Find a Doctor
			</div>			
			<form class="form-inline" role="form" method="GET">				
				<div class="form-group">
					<div class="input-group">
						<select name="speciality" id="speciality" class="form-control">
							<option value="" selected="selected" disabled> -- Physician -- </option>
							<?php // code added by Shoaib //
								$qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE blocked !='Y' and doc_specialities<>'' group by`doc_specialities`";
								$res = mysql_query($qry);
								while ($data = mysql_fetch_array($res)){
								echo "<option value='".$data['doc_specialities']."'>".$data['doc_specialities']."</option>"; 
								}
							?>					
						</select>										
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="city" id="city">
						<option value="Bangalore" selected="selected">  Bangalore  </option>									
						</select>									
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="dynamic_select">
						<option value="" selected="selected">  -- Select Type --  </option>	
						<option value="<?php echo $site_url;?>meet-our-physicians.php" >  Ask our Physicians  </option>	
						<option  value="<?php echo $site_url;?>meet-our-specialists.php" > Ask our Specialists  </option>	
						</select>									
					</div>
				</div>
							
				
				<div class="form-group">
					<button type="submit" name="submit" class="btn pw-btn">Find Doctor</button>
				</div>										
			</form>
		</div>
	</div>
	
	
	<?php
	$spec = isset( $_GET['speciality']) ?  $_GET['speciality'] : '';
	$cit = isset(  $_GET['city']) ?   $_GET['city'] : '';
	
	if(isset($_GET['speciality'])){ 
	$spec = $_GET['speciality'];
	}     
	if(isset($_GET['city'])){ 
	$cit = $_GET['city']; 
	} 
	
		
	$qry="select * from pw_doctors where blocked !='Y' and doc_specialities<>'' and activate_online_query='Query' and doc_specialities like '%$spec%' and city like '%$cit%'";
	$res=mysql_query($qry);	
	$counter = 0;
	
	while($row=mysql_fetch_array($res)){	
	 $counter++;		
//$class = ($num % 2)? "page-row gradient-background meet-our-doctors first-row": "page-row meet-our-doctors";

	?>
	<?php if($counter%2==0) {?>
	<div class="page-row gradient-background meet-our-doctors first-row">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-3 col-xs-12">
				<img src="<?php echo $site_url.$row['doc_photo'];?>" width="170" height="170" style="border-radius:100px; box-shadow: 2px 2px 2px #888888;">
				
				<?php
				echo '<a href="doctorsdomain.php?doc_id='.$row['doc_id'].'" class="doctor-button blue hidden-xs" target="_blank">View Profile</a>';
			?>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-9 col-xs-12">
				<div class="details">
					<div class="name"><?php echo $row['doc_name'];?></div>
					<div class="designation red-text"><?php echo $row['doc_specialities'];?></div>
					<div class="message hidden-xs">
						<?php //echo $row[''];?><span class="hidden-xs hidden-sm"> <br></span>
						 <span class="hidden-xs hidden-sm"> <br></span>
						
					</div>

					<div class="location col-xl-6 col-md-6  col-sm-6 hidden-xs">
						<span class="red-text"><?php echo strtoupper($row['visit_clinic_name']);?></span><br>
						<?php echo $row['doc_timing'];?><br>
						<?php echo $row['tele_consult_phno'];?>
					</div>
					<div class="location col-xl-6 col-md-6 col-sm-6  hidden-xs">
						<span class="red-text"><?php echo strtoupper($row['visit_hospital_name']);?></span><br>
						<?php echo $row['doc_timing'];?><br>
						<?php echo $row['tele_consult_phno'];?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="calendar">
					<div class="hidden-xs">
					
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
					</div>
					<span class="hidden-sm"><br></span>
					<div class="red-text idno">ID No: <?php echo $row['doc_id'];?></div>
					
					<div class="hidden-xs">
					<?php //if ($row['activate_online_query']!=''){?>
							
							<?php //} if ($row['doc_category']=='Expert'){?>
						
						<?php //} if ($row['appoint_flag']!='0'){?>
						<a href="book-online.php" class="doctor-button red" id="pa">Click to Call</a><br>			
						<a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="pq">Start a Query</a><br>
						<!--a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="po"><i>pink</i>Opinion</a><br>
						<?php //} if ($row['activate_online_consultation']!=''){?>
						<a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="pf"><i>pink</i>FollowUp</a><br-->
						<?php //} ?>
					</div>
										
					<a href="doctorsdomain.php" class="doctor-button blue visible-xs" target="_blank">View Profile</a>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>

<?php } else {?>
	<div class="page-row meet-our-doctors">
		<div class="content-area">
			<div class="image-block col-xl-4 col-md-4 col-sm-3 col-xs-12">
				<img src="<?php echo $site_url.$row['doc_photo'];?>" width="150" height="150" style="border-radius:100px;box-shadow: 2px 2px 2px #888888">
				<?php
				echo '<a href="doctorsdomain.php?doc_id='.$row['doc_id'].'" class="doctor-button blue hidden-xs" target="_blank">View Profile</a>';
				?>
			</div>

			<div class="data-block col-xl-8 col-md-8 col-sm-9 col-xs-12">
				<div class="details">
					<div class="name"><?php echo $row['doc_name'];?></div>
					<div class="designation red-text"><?php echo $row['doc_specialities'];?></div>
					<div class="message hidden-xs">
						<?php //echo $row[''];?><span class="hidden-xs hidden-sm"> <br></span>
						<span class="hidden-xs hidden-sm"> <br></span>						
					</div>

					<div class="location col-xl-6 col-md-6 col-sm-6 hidden-xs">
						<span class="red-text"><?php echo strtoupper($row['visit_clinic_name']);?></span><br>
						<?php echo $row['doc_timing'];?><br>
						<?php echo $row['tele_consult_phno'];?>
					</div>
					<div class="location col-xl-6 col-md-6 col-sm-6 hidden-xs">
						<span class="red-text"><?php echo strtoupper($row['visit_hospital_name']);?></span><br>
						<?php echo $row['doc_timing'];?><br>
						<?php echo $row['tele_consult_phno'];?>
					</div>
					<div class="clearfix"></div>
				</div>				
				<div class="calendar">
					<div class="hidden-xs">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_2.png">
						<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_Star_1.png">
					</div>
					<span class="hidden-sm"><br></span>
					<div class="red-text idno">ID No: <?php echo $row['doc_id'];?></div>					
					<div class="hidden-xs">
					<?php //if ($row['activate_online_query']!=''){?>
						<a href="book-online.php" class="doctor-button red" id="pa">Click to Call</a><br>			
						<a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="pq">Start a Query</a><br>
						<!--a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="po"><i>pink</i>Opinion</a><br>
						<?php //} if ($row['activate_online_consultation']!=''){?>
						<a href="" data-toggle="modal" data-target="#pink-btn-login-modal" class="doctor-button red" id="pf"><i>pink</i>FollowUp</a><br-->						
					<?php //} ?>
					</div>
					<a href="doctorsdomain.php" class="doctor-button blue visible-xs" target="_blank">View Profile</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	
<?php //} ?>
	
	
	<?php }}?>
	<div class="footer-cover">
		<?php include "includes/footer.php" ?>
	</div>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>

<script type="text/javascript">
 /* $(document).ready(function(){
    $("a").click(function () {
      $(this).fadeTo("fast", .5).removeAttr("href");
    });
  });
*/
</script>

<script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
