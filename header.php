<?php
error_reporting(0);
include("includes/site_config.php");
include 'analyticstracking.php';
$site_url = "http://localhost:81/pinkwhale_newest/";
$currentFile = $_SERVER["SCRIPT_NAME"];
$parts = Explode('/', $currentFile);
$currentFile = $parts[count($parts) - 1];
echo "<script language=javascript>var sitePrefix = '" . $site_url . "';</script>";
?>
<link rel="shortcut icon" href="<?php echo $site_url;?>favicon.ico" type="image/x-icon" />
 <link rel="icon" href="<?php echo $site_url;?>favicon.ico" type="image/x-icon">
<link type="text/css" rel="stylesheet" href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,300,700'/>
<!-- CSS Stylesheet-->
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/bootstrap-responsive.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/styles.css"  />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/styles-responsive.css"  />

<link type="text/css" rel="alternate stylesheet" media="screen" title="style1" href="<?php echo $site_url; ?>css/style1.css" />
<link type="text/css" rel="alternate stylesheet" media="screen" title="style2" href="<?php echo $site_url; ?>css/style2.css" />
<link type="text/css" rel="alternate stylesheet" media="screen" title="style3" href="<?php echo $site_url; ?>css/style3.css" />

<!-- Java Library -->
<script type="text/javascript" src="<?php echo $site_url; ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>js/modernizr.custom.26887.js"></script> 
<script type="text/javascript" src="<?php echo $site_url; ?>js/jquery.transit.min.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>js/jquery.gridrotator.js"></script>

<script type="text/javascript" src="<?php echo $site_url; ?>components/scrolltop/scrolltopcontrol.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>components/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>components/fancybox/jquery.fancybox.js"></script>	
<script type="text/javascript" src="<?php echo $site_url; ?>components/flickr/flickr.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>components/isotope/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>components/peelback/jquery.peelback.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>js/selectnav.min.js"></script>

<!-- Library bootstrap 2.04-->
<script type="text/javascript"  src="<?php echo $site_url; ?>js/bootstrap.min.js"></script>
<!-- Library Theme Custom-->
<script type="text/javascript"  src="<?php echo $site_url; ?>js/custom.js"></script>    

<!--
<link href="css/style.css" rel="stylesheet" type="text/css">        
<script type="text/javascript" src="<?php echo $site_url; ?>/js/header_colourchange.js"></script>
-->
<script type="text/javascript" src="<?php echo $site_url; ?>js/login.js"></script>
<script src="<?= $site_url; ?>js/search.js" type="text/javascript"></script>

<!-- header -->
<div id="header" class="header-style2 clearfix">                
    <div class="container">
        <!-- <a class="logo" href="<?php echo $site_url; ?>index.php"></a> -->
        <a class="logo" href="<?php echo $site_url; ?>index.php">
            <img src="images/pinkWhale_Logo_CCP.png">
        </a>
        <div class="contact-top">
            <span class="phone">Need Help? Call: <font color="#C36262">+91-80-43000333</font></span>
            <div class="tlogin" >

                <?php
                if (!isset($_SESSION['username']) || $_SESSION['login'] == 'Card') {
                    ?>

                    <a href="#" onclick='callpopup(true)'>Top Up</a> | 
                    <a href="#" id='login' onclick="login_callpopup(true,0);" >Sign in
                        <img src="<?php echo $site_url; ?>images/pinkarrow.png" align="absmiddle" />
                    </a>



                    <?php
                } else if ($_SESSION['login'] == 'user') {
                    ?>		
			<a href="<?php echo $site_url; ?>index.php">Home</a> | 
                    <a href="<?php echo $site_url; ?>phr.php">My Account</a> | 
                    <a href="<?php echo $site_url; ?>user_details.php">My Profile</a> | 
                    <a href="<?php echo $site_url; ?>card_topup.php">Order Services</a> | 
                    <a href="<?php echo $site_url; ?>cart.php">Shopping Cart</a> | 
                    <a href="<?php echo $site_url; ?>logout.php" class="signin">Sign out</a>



                    <?php
                } else if ($_SESSION['login'] == 'doctor') {
                    ?>
			<a href="<?php echo $site_url; ?>index.php">Home</a> | 
                    <a href="<?php echo $site_url; ?>doc_phr.php">My Account</a> | 
                    <a href="<?php echo $site_url; ?>doc_phr_profile.php">My Profile</a> |                                        
                    <a href="<?php echo $site_url; ?>logout.php" class="signin">Sign out</a>



                    <?php
                } else if ($_SESSION['login'] == 'admin') {
                    ?>
			<a href="<?php echo $site_url; ?>index.php">Home</a> | 
                    <a href="<?php echo $site_url; ?>admin/pw_admin.php">My Account</a> | 
                    <a href="<?php echo $site_url; ?>logout.php" class="signin">Sign out</a>


                    <?php
                }
//..................................................................................................
                else if ($_SESSION['login'] == 'Counsellor' || $_SESSION['login'] == 'Doctor' || $_SESSION['login'] == 'Dietician' || $_SESSION['login'] == 'content_manager') {
                    ?>

                    <a href="<?php echo $site_url; ?>logout.php" class="signin">Sign out</a>


                    <?php
                }
//..................................................................................................	
                else {
                    ?>

                    <a href="#" onclick='callpopup(true)'>Top Up</a> | 
                    <a href="#" id='login' onclick="login_callpopup(true,0);" >Sign in  
                        <img src="<?php echo $site_url; ?>images/pinkarrow.png" align="absmiddle" /></a>


                    <?php
                }
                ?>

            </div>
        </div>
        <!-- <div class="navbar-over">
            <div class="ri-shadow">
                <div class="navbar navbar-top">
                    <ul class="nav" id="navigation">
                        <li class="active"><a href="<?php echo $site_url; ?>index.php">Home</a></li>
                        <li><a href="<?php echo $site_url; ?>about_us.php">About Us</a></li>
                        <li class="dropdown"><a href="blog.html" class="dropdown-toggle" data-toggle="dropdown">Our Services</a>
                            <ul class="dropdown-menu animated fast fadeInUp">
                                <li><a href="<?php echo $site_url; ?>AskYourPhysician">Ask Our Physicians</a></li>
                                <li><a href="<?php echo $site_url; ?>AskYourSpecialist">Ask Our Specialists</a></li>
                                <li><a href="<?php echo $site_url; ?>GetASecondOpinion">Get A Second Opinion</a></li>
                                <li><a href="<?php echo $site_url; ?>HaveAConsultation">Consult Your Specialist</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $site_url; ?>doctors_directory.php">Our Doctors</a></li>
                        <li><a href="<?php echo $site_url; ?>contact_us.php">Contact Us</a></li>                        
                    </ul>
                </div>
            </div>
        </div> -->

    </div>
</div>

<!-- <div class="section-title  full-bg  " style="background-image:url(slider/slide_bg1.jpg)">
    <div class="container">
        <h2 class="animated bounceInDown"></h2>
    </div>    
</div> -->
