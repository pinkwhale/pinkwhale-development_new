<?php include "../includes/start.php" ?>
	<?php include "../includes/header.php" ?>
	<link href="../css/testimonials.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="doctors" data-sub-page="satisfied-doctors"></div>
	<?php include "menu-doctors.php" ?>
		
	
	<img src="../img/satisfieddoctors/Testimonials_Doctors_banner.png" class="page-banner">	
	<div class="page-tagline content-area row-bg-1">HEAR WHAT SOME OF OUR SATISFIED<br>DOCTORS HAVE TO SAY ABOUT <i>pink</i>WHALE</div>

	<div class="page-row gradient4-background patients">
		<div class="content-area slider-row">
			<div class="row-header"></div>
			<div class="row-description">
				<div class="image col-xl-4 col-md-4 col-sm-12 col-xs-12">
					<img src="../img/patients/happypatients/Testimonials_Patients_Testimonial_Dr.Name.png">
				</div>
				<div class="message col-xl-8 col-md-8 col-sm-12 col-xs-12">
					Lorem Ipsum is simply dummy text of the printing and 
					typesetting industry. Lorem Ipsum has been the 
					industry's standard dummy text ever since.
					<div class="name">- Kirti Singh</div>
					<div class="pagination">
						<div class="circle circle-bg"></div>
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
						<div class="circle"></div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="page-row testimonials">
		<div class="content-area row-bg-2">
			<div class="row-header">
				<img src="../img/patients/happypatients/Testimonials_Patients_Icon2_Quotes.png" class="row-title-image">
				<div class="row-title">Doctor Testimonials</div>
			</div>
			<br><br>
			<div class="row-description">
				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4">
						<img src="../img/patients/happypatients/Testimonials_Patients_Dr.Name1.png">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8">
						Lorem Ipsum is simply dummy text of the printing 
						and typesetting industry. Lorem Ipsum has been 
						the industry's standard dummy text ever since the 
						1500s,it to make a type specimen book.
						<div class="name">- Heenaa Biswas</div>
					</div>
				</div>


				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4">
						<img src="../img/patients/happypatients/Testimonials_Patients_Dr.png">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8">
						Lorem Ipsum is simply dummy text of the printing 
						and typesetting industry. Lorem Ipsum has been 
						the industry's standard dummy text ever since the 
						1500s,it to make a type specimen book.
						<div class="name">- Heenaa Biswas</div>
					</div>
				</div>

				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4">
						<img src="../img/patients/happypatients/Testimonials_Patients_Dr.Name1.png">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8">
						Lorem Ipsum is simply dummy text of the printing 
						and typesetting industry. Lorem Ipsum has been 
						the industry's standard dummy text ever since the 
						1500s,it to make a type specimen book.
						<div class="name">- Heenaa Biswas</div>
					</div>
				</div>


				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4">
						<img src="../img/patients/happypatients/Testimonials_Patients_Dr.png">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8">
						Lorem Ipsum is simply dummy text of the printing 
						and typesetting industry. Lorem Ipsum has been 
						the industry's standard dummy text ever since the 
						1500s,it to make a type specimen book.
						<div class="name">- Heenaa Biswas</div>
					</div>
				</div>


				<div class="testimonial-row">
					<div class="testimonial-image col-xl-4 col-md-4">
						<img src="../img/patients/happypatients/Testimonials_Patients_Dr.Name1.png">
					</div>
					
					<div class="testimonial-message col-xl-8 col-md-8">
						Lorem Ipsum is simply dummy text of the printing 
						and typesetting industry. Lorem Ipsum has been 
						the industry's standard dummy text ever since the 
						1500s,it to make a type specimen book.
						<div class="name">- Heenaa Biswas</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	

	<?php include "../includes/footer.php" ?>
	<?php include "../includes/include-js.php" ?>
<?php include "../includes/end.php" ?>
