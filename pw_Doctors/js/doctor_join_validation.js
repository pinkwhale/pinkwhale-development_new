var fname = false;
var lname = false;
var email = false;
var mobile = false;
var speciality = false;
var why = false;
var uploadBtn = false;

function add_doctor(form)
{
	fname = true;
	lname = true;
	email = true;
	mobile = true;	
	speciality = true;	
	why = true;
	uploadBtn = true;
	
	document.getElementById("fname_ErrDiv").innerHTML = "";
	document.getElementById("lname_ErrDiv").innerHTML = "";
	document.getElementById("email_ErrDiv").innerHTML = "";
	document.getElementById("phone_ErrDiv").innerHTML = "";
	document.getElementById("spec_ErrDiv").innerHTML = "";
	document.getElementById("why_ErrDiv").innerHTML	= "";
	document.getElementById("upload_ErrDiv").innerHTML = "";
	
	 var email = form.email.value;
	 var mobile = form.mobile.value;
	
	if (form.fname.value=='')
	{
   		document.getElementById("fname_ErrDiv").innerHTML = "Please enter your First Name";
                fname = false;
	}
	if (form.fname.value != "") {
	 if ( /[^A-Za-z +$]/.test(form.fname.value)) {
	document.getElementById("fname_ErrDiv").innerHTML = "Please enter characters only";      	
	form.fname.focus();
	fname = false;
    }
	}
	
	if (form.lname.value=='')
	{
   		document.getElementById("lname_ErrDiv").innerHTML = "Please enter your Last Name";
                lname = false;
	}
	if (form.lname.value != "") {
	 if ( /[^A-Za-z +$]/.test(form.lname.value)) {
	document.getElementById("lname_ErrDiv").innerHTML = "Please enter characters only";      	
	form.lname.focus();
	lname = false;
    }
	}
        
	if (email == '')
	{
   		document.getElementById("email_ErrDiv").innerHTML = "Please enter your email";
                email = false;
	}
	if ( email !='' )
	{
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
			document.getElementById("email_ErrDiv").innerHTML = "Not a valid e-mail address";       
			email = false;
	}
	}
	if (form.mobile.value=='')
	{
   		document.getElementById("phone_ErrDiv").innerHTML = "Please enter mobile";
                mobile = false;
	}
	

	
	if (form.speciality.value=='')
	{
   		document.getElementById("spec_ErrDiv").innerHTML = "Please enter speciality";
                speciality = false;
	}
	if (form.why.value=='')
	{
   		document.getElementById("why_ErrDiv").innerHTML = "Please enter your reason for interest";
                why = false;
	}
	
	if (form.uploadBtn.value=='')
	{
   		document.getElementById("upload_ErrDiv").innerHTML = "Please upload your resume";
                uploadBtn = false;
	}
        if(fname && lname && email && mobile && speciality && why && uploadBtn)
	{
		document.getElementById('doc_join').submit();
	}
	else
	{
		return false;
	}
}

function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}

