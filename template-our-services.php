<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<link href="css/patientss.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-query"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-pinkquery">
	</div>

	<?php include "includes/menu-pink-buttons.php" ?>
	
	<div class="page-tagline pink-page">
		<img src="img/ourservices/pinkquery/pinkQuery_Icon11.png"><i>pink</i><strong>Query</strong>
		<div class="text">
			<i>
				Was your father just diagnosed with diabetes and you want <br>
				to know more about the disease? <br><br>
				Is your newborn baby up late at night with an ear infection<br> 
				and you are unsure which pain medicine to give? <br><br>
				Do you just need some medical advice to calm your fears? <br>
			</i>
			<br><br>
			<strong>
				If you need a trusted doctor to answer a specific question or give some <br>
				immediate advice, pinkQuery is the service for you! pinkQuery puts a <br>
				doctor at your fingertips, just when you need one most.<br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="work-bg work-pink-query"></div>

	<div class="row-before-video">
		SEE HOW <i>pink</i>QUERY<br>CAN HELP YOU IN TIME OF NEED
	</div>

	<div class="work-bg video-bg video-image"></div>

	<div class="form-block col-md-12">
		<div class="content-area form-contents">
			<h1 class="form-title">STAY IN THE LOOP WITH<br>OUR HEALTH TIPS AND UPDATES</h1>
			<div class="col-xl-8 col-md-8 col-sm-12 col-xs-12">
				<div class="form-header">
					<img src="img/Subscribe1_icon_Apply.png">
					<span class="form-tagline">Hear more from Dr. Vanitha Here!</span>
				</div>
				<div class="form-elements">
					<div class="input-label col-xl-4 col-md-4 col-sm-6">First Name:*</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Last Name:*</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Email:</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Phone Number:</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6"></div><div class="input col-xl-8 col-md-8 col-sm-6"><button type="submit" class="submit">Submit</button></div>
				</div>
			</div>
			<div class="message col-xl-4 col-md-4 col-sm-12 col-xs-12">

				Like the idea of pinkWhale but aren’t <br>
				sure of Enrolling yet?<br>
				Not a problem! <br><br>
				Drop in your Email ID and we’ll send you <br>
				a newsletter with Updates & Health Tips <br>
				to keep you in the Pink of Health all year 
				round.
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
