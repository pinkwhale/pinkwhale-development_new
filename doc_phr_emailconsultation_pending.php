<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("includes/pw_db_connect.php");
	include 'not_found.php';
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/my_css.css" rel="stylesheet" type="text/css">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="clinic/css/TableCSSCode.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/doc_phr_mail_validation.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; 
$name=@$_GET['search_query_name'];
$pink=@$_GET['search_query_pink'];
?>
<!-- header.......-->
<?php 
$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_ctgry = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
		$doc_ctgry_flag=$result['doc_category_change_flag'];
	}
?>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><!--<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->
</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<!--td width="220"><h1>Online Consultation</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	<div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>
        </div>
    </td-->
	<td width="370"><h1><?php echo ucfirst($doc_name) ; ?></h1></td>
							<td width="370" align='right' bgcolor="#f1f1f1"><div style="color:#C36262; margin-right:20px; font-family:Arial; font-size:16px; font-weight:bold;">Pinkwhale ID: <?php echo $doc_id ; ?></div></td>
</tr>
</table>
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
?>

<form method="get" action="doc_phr_emailconsultation_pending.php" name="search_clinic_patients">
<table height="30" width="600" border="0" cellspacing="0" cellpadding="0" style="margin-left:50px;margin-top:40px;">
<tr>
	 <td><label style="font-weight: bold;">Enter Client Name</label>
	<input type="text" name="search_query_name" id="search_query_name" value="<?php echo $name;?>" placeholder="Search User Name here" size="30"/>
	</td> 
	<td><label style="font-weight: bold;">Enter Client Id</label>
	<input type="text" name="search_query_pink" id="search_query_pink" value="<?php echo $pink;?>" placeholder="Search Card id here" size="30"/>
	</td>
	<td>
	<input type="text" value="true" style="display:none;" name="search">
			<input type="submit" value="Search" id="button_find" name="button_find" style="margin-top: 12px;" class="Manually"  />
	</td>
	
</tr>
</table>
</form>


		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='CSSTableGenerator'>
		<tr><th colspan="5" style="background:#C4C4C4;height:32px;">Pending Consults</th></tr>
        <?php	
        $append="";
        if(isset($_GET['search']))
        {
        	if($_GET['search_query_name']=="")
        	{
        		$name="";
        	}
        	else
        		$name=$_GET['search_query_name'];
        
        	if($_GET['search_query_pink']=="")
        	{
        		$card="";
        	}
        	else
        		$card=$_GET['search_query_pink'];
        
        	$append=" and patient_name like '%$name%' and pw_card_id like '%$card%' ";
        }
        
        
        
		if($doc_ctgry=='Specialist' || $doc_ctgry_flag!="" )
		{
			$count =0;
			// Display all the data from the table 
			$qry1 = "SELECT MAX(`mail_serial_no`),min(`entered_date`),max(entered_date) FROM `pw_consultation_emails` WHERE `doctor_id` = '$doctor_id' GROUP BY `consultation_no`";
			$qry_rslt1 = mysql_query($qry1);
	
			$mailIds = "";
			$mailDataArray = null;
			
			while($result1 = mysql_fetch_array($qry_rslt1))
			{
				$max_ref_id=$result1['MAX(`mail_serial_no`)'];
				if($mailIds=="") $mailIds = $result1['MAX(`mail_serial_no`)'];
				else $mailIds .= ",".$result1['MAX(`mail_serial_no`)'];
				$mailDataArray[$result1['MAX(`mail_serial_no`)']] = $result1['min(`entered_date`)']."||".$result1['max(entered_date)'];
			}
	
			if(mysql_num_rows($qry_rslt1) > 0)
			{
				//$sql="SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`='$max_ref_id' && `mail_status`!='closed'";
				$sql="SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`in ($mailIds) && `mail_status`!='closed' $append";
				$obj->query($sql);	
				
				$pager = new PS_Pagination($dbcnx, $sql, 10, 5, "search=true&search_query_name=".$name."&search_query_pink=".$pink."");
				$pager->setDebug(true);
				$rs = $pager->paginate();
				
				
				if(mysql_num_rows($rs) > 0)
				{
					?>
					<tr>
						<td align='left'><strong>SI No:</strong> </td>
						<td align='left'><strong>Pinkwhale id</strong></td>
						<td align='left'><strong>User Name</strong></td>
						<td align='left'><strong>Booking Date</strong></td>
						<td align='left'><strong>Last Modified date</strong></td>
					</tr>
					<?php
					while($row = mysql_fetch_assoc($rs)) 
					{
						$count ++;
						$pw_id			= $row['pw_card_id'];
						$user_name		= $row['patient_name'];
						$enteredDate 	= explode("||", $mailDataArray[$row['mail_serial_no']]);
						$booking_date 	= $enteredDate[0];
						$last_modified	= $enteredDate[1];
						
						$gdate1=strtotime($booking_date);
						$final_date1=date("d M Y", $gdate1);
						
						$gdate2=strtotime($last_modified);
						$final_date2=date("d M Y", $gdate2);
						
						//$booking_date=$result1['min(`entered_date`)'];
						//$last_modified=$result1['max(entered_date)'];
						echo "<tr bgcolor='#ffffff'>";
							echo "<td>$count</td> ";
							echo "<td>";
							?>
							<a href="spe_doc_email_consultation.php?expd=<?php echo $row['consultation_no']; ?>&cad_id=<?php echo $row['pw_card_id']; ?>" style="text-decoration:underline;"> 			<?php echo $pw_id ; ?></a></td> 
							<?php
							echo "<td>$user_name</td> ";
							echo "<td>$final_date1</td> ";
							echo "<td>$final_date2</td> ";
						echo "</tr>";          			
					}
			
					$ps1 = $pager->renderFirst();
					$ps2 = $pager->renderPrev();
					$ps3 = $pager->renderNav('<span>','</span>');
					$ps4 = $pager->renderNext();
					$ps5 = $pager->renderLast();
					?>
					</table>
					<table style="margin-left: 50%;">
					<tr><td colspan='5' align='center'>
					<?php
					echo "$ps1";
					echo "$ps2";
					echo "$ps3";
					echo "$ps4";
					echo "$ps5";
					echo "</td></tr></table>"; 
				}
				else
				{
					echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
				}
			}
			else
			{
				echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
			}	
		
		}
		

		if($doc_ctgry=='Specialist/Expert')
		{
			$count =0;
			// Display all the data from the table 
			$qry1 = "SELECT MAX(`mail_serial_no`),min(`entered_date`),max(entered_date) FROM `pw_consultation_emails` WHERE `doctor_id` = '$doctor_id' GROUP BY `consultation_no`";
			$qry_rslt1 = mysql_query($qry1);
	
			$mailIds = "";
			$mailDataArray = null;
			
			while($result1 = mysql_fetch_array($qry_rslt1))
			{
				$max_ref_id=$result1['MAX(`mail_serial_no`)'];
				if($mailIds=="") $mailIds = $result1['MAX(`mail_serial_no`)'];
				else $mailIds .= ",".$result1['MAX(`mail_serial_no`)'];
				$mailDataArray[$result1['MAX(`mail_serial_no`)']] = $result1['min(`entered_date`)']."||".$result1['max(entered_date)'];
			}

			if(mysql_num_rows($qry_rslt1) > 0)
			{
				//$sql="SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`='$max_ref_id' && `mail_status`!='closed'";
				$sql="SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`in ($mailIds) && `mail_status`!='closed' $append";
				$obj->query($sql);	
		
				$pager = new PS_Pagination($dbcnx, $sql, 10, 5, "search=true&search_query_name=".$name."&search_query_pink=".$pink."");
				$pager->setDebug(true);
				$rs = $pager->paginate();
				
				if(mysql_num_rows($rs) > 0)
				{
					?>
					<tr>
						<td align='left'><strong>SI No:</strong> </td>
						<td align='left'><strong>Pinkwhale id</strong></td>
						<td align='left'><strong>User Name</strong></td>
						<td align='left'><strong>Booking Date</strong></td>
						<td align='left'><strong>Last Modified date</strong></td>
					</tr>
					<?php
				
					while($row = mysql_fetch_assoc($rs)) 
					{
						$count ++;
						$pw_id			= $row['pw_card_id'];
						$user_name		= $row['patient_name'];
						$enteredDate 	= explode("||", $mailDataArray[$row['mail_serial_no']]);
						$booking_date 	= $enteredDate[0];
						$last_modified	= $enteredDate[1];
						
						$gdate1=strtotime($booking_date);
						$final_date1=date("d M Y", $gdate1);
						
						$gdate2=strtotime($last_modified);
						$final_date2=date("d M Y", $gdate2);
						
						//$booking_date=$result1['min(`entered_date`)'];
						//$last_modified=$result1['max(entered_date)'];
						echo "<tr bgcolor='#ffffff'>";
							echo "<td>$count</td> ";
							echo "<td>";
							?>
							<a href="spe_doc_email_consultation.php?expd=<?php echo $row['consultation_no']; ?>&cad_id=<?php echo $row['pw_card_id']; ?>" style="text-decoration:underline;"> <?php echo $pw_id ; ?></a></td> 
							<?php
							echo "<td>$user_name</td> ";
							echo "<td>$final_date1</td> ";
							echo "<td>$final_date2</td> ";
						echo "</tr>";          			
					}
					
					$ps1 = $pager->renderFirst();
					$ps2 = $pager->renderPrev();
					$ps3 = $pager->renderNav('<span>','</span>');
					$ps4 = $pager->renderNext();
					$ps5 = $pager->renderLast();
					?>
					</table><table style="margin-left: 50%;">
					<tr><td colspan='5' align='center'>
					<?php
					echo "$ps1";
					echo "$ps2";
					echo "$ps3";
					echo "$ps4";
					echo "$ps5";
					echo "</td></tr></table>"; 
				}
				else
				{
					echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
				}
			}
			else
			{
				echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
			}	
		}
		
		
		else if($doc_ctgry=='Counsellor')
		{
			$count =0;
			// Display all the data from the table 
			
			$qry1 = "SELECT MAX(`mail_id`),min(`entered_date`),max(entered_date) FROM `pw_cunslr_consultation_emails` WHERE `counsellor_id`='$doctor_id' group by `consultation_id`";
			$qry_rslt1 = mysql_query($qry1);
			
			$mailIds = "";
			$mailDataArray = null;
			
			while($result1 = mysql_fetch_array($qry_rslt1))
			{
				$max_ref_id=$result1['MAX(`mail_id`)'];
				if($mailIds=="") $mailIds = $result1['MAX(`mail_id`)'];
				else $mailIds .= ",".$result1['MAX(`mail_id`)'];
				$mailDataArray[$result1['MAX(`mail_id`)']] = $result1['min(`entered_date`)']."||".$result1['max(entered_date)'];
			}
			
			//$sql="SELECT * FROM `pw_cunslr_consultation_emails` WHERE `counsellor_replay`!='closed' && `mail_id`='$max_ref_id'";
			$sql="SELECT * FROM `pw_cunslr_consultation_emails` WHERE `counsellor_replay`!='closed' && `mail_id` in ($mailIds) $append";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 10, 5, "search=true&search_query_name=".$name."&search_query_pink=".$pink."");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			while($row = mysql_fetch_assoc($rs)) 
			{
				$count ++;
				$pw_id			= $row['pw_card_id'];
				$user_name		= $row['patient_name'];
				$enteredDate 	= explode("||", $mailDataArray[$row['mail_id']]);
				$booking_date 	= $enteredDate[0];
				$last_modified	= $enteredDate[1];
				
				$gdate1=strtotime($booking_date);
				$final_date1=date("d M Y", $gdate1);
				
				$gdate2=strtotime($last_modified);
				$final_date2=date("d M Y", $gdate2);
				
			    //$booking_date=$result1['min(`entered_date`)'];
				//$last_modified=$result1['max(entered_date)'];
				echo "<tr bgcolor='#ffffff'>";
					echo "<td>$count</td> ";
					echo "<td>";
					?>
                    <a href="coun_doc_email_consultation.php?expd=<?php echo $row['consultation_id']; ?>&cad_id=<?php echo $row['pw_card_id']; ?>" style="text-decoration:underline;"><?php echo $pw_id ; ?></a></td> 
               		<?php
					echo "<td>$user_name</td> ";
					echo "<td>$final_date1</td> ";
					echo "<td>$final_date2</td> ";
				echo "</tr>";          			
			}
	
				$ps1 = $pager->renderFirst();
				$ps2 = $pager->renderPrev();
				$ps3 = $pager->renderNav('<span>','</span>');
				$ps4 = $pager->renderNext();
				$ps5 = $pager->renderLast();
				?>
				</tr></table><table style="margin-left: 50%;">
            	<tr><td colspan='5' align='center'>
				<?php
                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr></table>"; 
		}
                ?>
		

</td></tr>
</table>
<!-- footer -->
<script language="javascript" type="text/javascript">
enable_email_submenu()
</script>
<?php
include 'footer.php'; ?>

</body></html>
