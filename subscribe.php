<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php";
	error_reporting ( E_PARSE );
	session_start ();
	?>
	<link href="css/subscribe.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="js/subscribe_validation.js"></script>

	
	<div class="menu-data" data-main-page="patients"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-subscribe">
		<div class="content-area">
		</div>
	</div>
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			STAY IN THE LOOP WITH<br>OUR HEALTH TIPS AND UPDATES
		</div>
	</div>
	<div align="center"><span style="color:#3C6;"><?php if (isset($_SESSION['addoc']))
                                                    echo $_SESSION['addoc']; $_SESSION['addoc']=""; ?></span></div>
	<div class="form-block col-md-12 row-bg-2">
		<div class="content-area form-contents ">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Hear more from a certified Doctor!</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form" action="actions/subscribe_action.php" method="POST" name="subscribe"
					enctype="multipart/form-data" id="subscribe">
						<!--div class="form-group">
							<label for="dname" class="col-xs-5 control-label">Doctor's Name:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="dname" >
							</div>
						</div-->
						<div class="form-group">
							<label for="speciality" class="col-xs-5 control-label">Category:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="category" name="category" >
							</div>
						</div>
						<div class="form-group">
							<label for="fname" class="col-xs-5 control-label">First Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="fname" name="fname">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="fname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="lname" class="col-xs-5 control-label">Last Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="lname" name="lname">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="lname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" name="email" >
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="email_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Phone No:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" name="mobile" onkeypress="return isNumberKey(event);">
							</div>
						</div>	
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="phone_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" value="Register" onclick="return add_subscription(subscribe)"/>
							</div>
						</div>										
					</form>
				</div>				
			</div>						
			<div class="message form-right col-sm-4 hidden-xs">
	
				Like the idea of pinkWhale but aren't <span class="hidden-xs hidden-sm"<br></span>
				sure of Enrolling yet?<span class="hidden-xs hidden-sm"<br></span>
				Not a problem! <span class="hidden-xs hidden-sm"<br><br></span>
				Drop in your Email ID and we'll send you <span class="hidden-xs hidden-sm"<br></span>
				a newsletter with Updates &amp; Health Tips <span class="hidden-xs hidden-sm"<br></span>
				to keep you in the Pink of Health all year 
				round.
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
