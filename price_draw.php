<?php
session_start();
include "actions/encdec.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<style>
.fond {
color: #333333;
    font-family: Arial,Verdana;
    font-size: 13px;
    line-height: 18px;
    margin: 10px 0;
    padding: 0;
    text-align: justify;
}

</style>
<body>
<!-- header.......-->
<?php include 'header.php'; ?>
<!-- header.......-->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" class="s90greybigbox" style="font-family: Arial,Verdana;"> 
<tr><td>
<table width="950" border="0" cellspacing="0" cellpadding="0" align="center"  style="font-family: Arial,Verdana;">
<tr><td>&nbsp;</td></tr>
<tr><td><b style="font-size: 18px;color:#1645D0;font-family: "Arial",Arial;">PinkWhale Healthcare Services 2011 Monthly Prize Drawing</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">Every month we have a random drawing to select 2 winners for a special prize. The first drawing is on Aug 5, 2011. We're not sure when it will end just yet.   </td></tr>
<tr>
  <td class="fond"><b style="font-style:italic;">Registrations received by 23:59 on 5/8/2011 will be considered for the drawing on Aug 5, 2011. The winner shall be selected by means of a random draw from all registrations received by the Closing Date. The winner will be announced on the pinkWhale blog  <br />(<a href="http://blog.pinkwhalehealthcare.com/"> http://blog.pinkwhalehealthcare.com</a>)on Aug 15, 2011.</b></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr><td><b style="font-family: "Arial",Arial;">The prizes for the next drawing (Aug 1, 2011) are:</b></td></tr>
<tr><td>&nbsp;</td></tr>

<tr><td><table width="950" border="0" cellspacing="0" cellpadding="0" align="center"  style="font-family: Arial,Verdana;">
<tr><td colspan="2"><b style="color:red;">1.	First Prize</b></td></tr>
<tr><td><img src="images/MotorMouse_Image1_195.jpg" /></td>
<td class="fond" valign="middle"> Motormouse is a slim, ergonomic design that fits comfortably in the palm of your hand with 3 sensitivity settings for all types of applications.. </td></tr>
<tr><td colspan="2"><b style="color:red;">2.	Second Prize  </b></td></tr>
<tr><td><img src="images/second_prize.JPG" /></td>
<td class="fond" valign="middle">
 Non-contact Infra-red Forehead Thermometer to allow you to take your baby's temperature without even touching him/her.</td></tr>
 </table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><b style="font-size: 18px;font-family: "Arial",Arial;">How to Enter:</b></td></tr>
<tr><td class="fond">These terms and conditions shall apply to all entries into the 2011 Monthly Prize Drawing. </td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table>
<tr><td valign="top"><b>1.</b></td><td class="fond"><b>New Members:</b> You will automatically be entered into the prize draw when you register to become a pinkWhale Healthcare Services member on or after June 25, 2011. You can do this by joining at <b><a href="http://www.pinkwhalehealthcare.com/join.php">http://www.pinkwhalehealthcare.com/join.php</a></b>.  Register once to automatically go into the draw each month of the 2011 Monthly Prize Drawing for a chance to win a fantastic prize.</td></tr>
<tr><td valign="top"><b>2.</b></td><td valign="top" class="fond"><b>Existing Members:</b> You can increase your chances of winning by referring a friend to us. Simply ask your friend to register at  <b> <a href="http://www.pinkwhalehealthcare.com/join.php"> http://www.pinkwhalehealthcare.com/join.php</a> </b>and ask him/her include your name and email id during registration.</td></tr>
</table>
</td></tr>
<tr><td>&nbsp;</td></tr>

<tr><td><b style="font-size: 18px;font-family: "Arial",Arial;">Terms & Conditions:</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">These terms and conditions shall apply to all entries into the 2011 Monthly Prize Drawing. </td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">This is not a lottery and no payment is required to enter into the prize draw. There are two prizes per draw and no alternative is offered. The lucky winner will be randomly selected from among all the entries as of the 1st of the month.  Delivery to a mailing address in India is included in the prize.</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">You must be a resident of India and 18 years of age or over to enter into this prize draw. This prize draw is not open to employees of pinkWhale Healthcare Services or families.  The winner will be notified via email to the email address and phone number provided by you when registering at  <b><a href="http://www.pinkwhalehealthcare.com"> www.pinkwhalehealthcare.com</a></b>. The winners will also be announced on the pinkWhale blog  (<b><a href="http://blog.pinkwhalehealthcare.com"> http://blog.pinkwhalehealthcare.com</a></b>).</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">To claim your prize you must respond to the email within 10 days of the notification email being sent. Failure to claim the prize within the specified time shall result in a further draw taking place and a new winner being selected.</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">pinkWhale Healthcare Services does not accept responsibility for any lost, late or misdirected entries of any network, computer hardware or software failures of any kind which may restrict or delay the sending or receipt of your entry or any electronic communication by you or pinkWhale Healthcare Services.  </td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class="fond">If any act, omission, event or circumstances occurs which is beyond the reasonable control of pinkWhale Healthcare Services and which prevents pinkWhale Healthcare Services from complying with these terms and conditions, pinkWhale Healthcare Services will not be liable for any failure to perform or delay in performing its obligations and pinkWhale Healthcare Services reserves the right (to the fullest extent permitted by any applicable law) to cancel, terminate, modify or suspend the prize draw or alternatively substitute the prize for a prize of equivalent or greater monetary value</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
</td></tr>
</table>

<!-- footer -->

<?php
include 'footer.php'; ?>
</body>
</html>