
<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include("includes/site_config.php");?>
	<link href="css/home-patients.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	
	<div class="banner-bg banner-bg-patients">
		<div class="hidden-xs hidden-sm max-960">
			<div class="tag-line">We bring doctors to where you are</div><br><br>
			<div class="text">For customers who want easy<br>access to their doctors at all times</div><br>
			<a href ="<?php echo $site_url; ?>pinkQuery.php" target="_self" class="button-learn">Learn More</a>
		</div>
	</div>
	
		<div class="widgets-row row-bg-2">
		<!--div class="content-area">

			<div class="header max-960">CONNECT WITH A DOCTOR. IT'S EASY!</div>
			<div class="clearfix"></div>

			<div class="max-960">
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Login.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Login</div>
					<div class="widget-text">Login for easy access<br>to one of our doctors.</div>
				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Select.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Select</div>
					<div class="widget-text">Pick from our strong<br>database of trusted doctors</div>

				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Consult.png" class="widget-image">
					<div class="widget-name">Consult</div>
					<div class="widget-text">Connect with a doctor<br>online or by phone.</div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div-->
		
		<div class="grey-row">
		<h1 class="content-area">
		<?php
$str = "Say goodbye to traffic and waiting time."?><br><?php $str2 = "Consult our doctor right from your living room!";
$str = strtoupper($str);
$str2 = strtoupper($str2);
echo $str?> <br/><?php echo $str2; 
?>
			
		</h1>
	</div>
		<div class="our-bouquet-container">		
			<div class="content-area">				
				<div class="header">Our bouquet of Services for enhanced care</div>
				<div class="clearfix"></div>

				<div class="widgets-image-container max-960" >
					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkQuery.php">
							<img src="img/home/patients/AskAPhysician-Service-Icon.png" class="gradient2-background">
							<h3 style="color:#c36262;"><i>pink</i><b>Query</b></h3>
							<h2 class="widget-name" style="color:#6d6e71;font-size:21px;">Ask Your Physician</h2>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Doctors are available <span class="hidden-sm"><br></span> now to answer your<span class="hidden-sm"><br></span> questions and give advice.</div>
						</a>
					</div>
					
					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkQuerySpecialists.php">
							<img src="img/home/patients/AskASpecialist-Service-Icon.png" class="gradient2-background">
							<h3 style="color:#c36262;"><i>pink</i><b>Query</b></h3>
							<h2 class="widget-name" style="color:#6d6e71;font-size:21px;">Ask Your Specialist</h2>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Virtual Follow-ups<br>make connecting with<br>your doctor fast & easy.</div>
						</a>
					</div>

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="bookAppointment.php">
							<img src="img/home/patients/BookAppointment-Service-Icon.png" class="gradient2-background">
							<h3 style="color:#c36262;"><i>pink</i><b>Visit</b></h3>
							<h2 class="widget-name" style="color:#6d6e71;font-size:21px;">Book an Appointment</h2>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our synced calendar<br>makes it simple to<br>schedule doctor visits.</div>
						</a>
					</div>

					

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkOpinion.php">
							<img src="img/home/patients/SecondOpinion-Service-Icon.png" class="gradient2-background">
							<h3 style="color:#c36262;"><i>pink</i><b>Opinion</b></h3>
							<h2 class="widget-name" style="color:#6d6e71;font-size:21px;">Get a Second Opinion</strong></h2>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our credible second<br>opinions help you make<br>the best health decisions.</div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		
	</div>
	
	<!--div class="background_hand"-->
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			<div class="header">We Simplify your Life</div>
			<div class="description">We make Doctors accessible to you, anytime, anywhere.</div>
			<img src="img/home/patients/Patients_Graphic_PatientBenefits.png" alt="How online doctor consultations simplify your life">
		</div>
	</div>

	<div class="background_hand">
	
	<div class="followup-row">
		<div class="max-960 hidden-xs">
			<div class = "link-box">
				<a href = "#zero" class="active" onclick="makeThisActive(this,'one');">Tele Consult</a>	
				<a href = "#one" onclick="makeThisActive(this,'two');" class="second-lnk">Online Consult</a>
			</div>
		</div>

		<div class="bubbles-container">
			<div class="bubble-one bubbles active-bubble"></div>
			<div class="bubble-two bubbles"></div>
		</div>
		<div class="how-it-works">
			<div class="content-area">
				<!--img src="img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png"-->
				<div class="title">Connect with a Doctor. It's easy!</div>
			</div>
		</div>
		<div class="follow-up-carousel">
			<div class="" data-hash="zero"><img src="img/home/patients/Call Choose Consult_1.png" alt="How to do tele consultation"></div>
			<div class="" data-hash="one"><img src="img/home/patients/Login-Select-Consult_1.png" alt="How to do online consultation"></div>
		</div>
	</div>
	</div>
<!--/div-->
	<style type="text/css">
		.follow-up-carousel img{max-width: 100%;}
		.follow-up-carousel.owl-carousel .owl-item img{width: auto; margin: 0px auto;}
		.follow-up-carousel.owl-carousel .owl-item {}
		.followup-row .bubbles-container{bottom: -20px;}
		.follow-up-carousel.owl-carousel{margin-bottom: 40px;}
		.follow-up-carousel.owl-carousel {margin-bottom: 60px;} 
	</style>
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			<h3 class="tag">What is pinkWhale?</h3>
			<div class="description" style="font-size:13px;">pinkWhale Healthcare Services is an e-Health company offering <b>integrated online and 
			telephone based personal healthcare services</b>. Our goal is to give consumers and businesses quick, easy, and 
			convenient access to the best health care from wherever they are, using a state-of-the-art technology platform. 
			We are building the best network of health specialists, <a href="meet-our-doctors.php">healthcare providers</a>, and partners who are committed to
			providing health consultations by phone or online.
</div>
			
		</div>
	</div>

	
	<!--div class="widgets-row row-bg-2">
		<div class="content-area">

			<div class="header max-960">CONNECT WITH A DOCTOR. IT'S EASY!</div>
			<div class="clearfix"></div>

			<div class="max-960">
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Login.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Login</div>
					<div class="widget-text">Login for easy access<br>to one of our doctors.</div>
				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Select.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Select</div>
					<div class="widget-text">Pick from our strong<br>database of trusted doctors</div>

				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Consult.png" class="widget-image">
					<div class="widget-name">Consult</div>
					<div class="widget-text">Connect with a doctor<br>online </div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<div class="content-area">

			<div class="header max-960">TELE CONNECT WITH A DOCTOR. IT'S EASY!</div>
			<div class="clearfix"></div>

			<div class="max-960">
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Login.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Dial</div>
					<div class="widget-text">Dial for easy access<br>to one of our doctors.</div>
				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Select.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Connect</div>
					<div class="widget-text">Connect from our strong<br>database of trusted doctors</div>

				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Consult.png" class="widget-image">
					<div class="widget-name">Consult</div>
					<div class="widget-text">Consult with a doctor<br>by phone.</div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!--div class="our-bouquet-container">		
			<div class="content-area">				
				<div class="header">OUR BOUQUET OF SERVICES<br>FOR ENHANCED CARE</div>
				<div class="clearfix"></div>

				<div class="widgets-image-container max-960">
					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkQuery.php">
							<img src="img/home/patients/Patients_Icon_PinkQuery.png" class="gradient2-background">
							<div class="widget-name"><i>Ask Our Physicians</i><strong></strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Doctors are available <span class="hidden-sm"><br></span> now to answer your<span class="hidden-sm"><br></span> questions and give advice.</div>
						</a>
					</div>
					
					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkfollowup-online.php">
							<img src="img/home/patients/Patients_Icon_pinkFollowUp.png" class="gradient2-background">
							<div class="widget-name"><i>Ask Our Specialists</i><strong></strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Virtual Follow-ups<br>make connecting with<br>your doctor fast & easy.</div>
						</a>
					</div>

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkAppoint.php">
							<img src="img/home/patients/Patients_Icon_pinkAppoint.png" class="gradient2-background">
							<div class="widget-name"><i>Book Appointment</i><strong></strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our synced calendar<br>makes it simple to<br>schedule doctor visits.</div>
						</a>
					</div>

					

					<!--div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkOpinion.php">
							<img src="img/home/patients/Patients_Icon_pinkOpinion.png" class="gradient2-background">
							<div class="widget-name"><i>pink</i><strong>Opinion</strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our credible second<br>opinions help you make<br>the best health decisions.</div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div-->

		
	</div>
	
	<?php include "includes/home-footer.php" ?>
	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
	<script>
		$(document).ready(function(){
			$('.btn-shadow').click(function(){
				$(this).css('box-shadow','0 0 5px #000');
			});
		});
	</script>
	
<?php include "includes/end.php" ?>
