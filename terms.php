<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/site_config.php" ?>
	

	
	<div class="menu-data" data-main-page="none" data-sub-page="terms"></div>
	<?php include "includes/page-menu-start.php" ?>
		<li class="contents"><a href="privacy-policy.php" class="menu-elements upper-case sub-privacy-policy">privacy policy</a></li>
		<li class="contents"><a href="terms.php" class="menu-elements upper-case sub-terms">terms of service</a></li>
		<li class="contents"><a href="" data-toggle="modal" data-target="#login-modal"  class="login-button upper-case">Login</a></li>
	<?php include "includes/page-menu-end.php" ?>
		
	
	<!-- <img src="img/privacypolicy/Terms of Service_Banner.png" class="page-banner">	 -->

	<div class="banner-bg banner-bg-terms">
		<div class="content-area">
			
		</div>
	</div>

	<div class="page-tagline content-area row-bg-1" id="top">END USER AGREEMENT<br>AND TERMS OF SERVICE</div>

	
	<div class="page-row gradient3-background terms">
		<div class="content-area">
			<a href=""><strong><i>pink</i>Whale</strong>healthcare.com</a> (the "Site") is an online healthcare service provided by <strong><i>pink</i>Whale</strong>
			Healthcare Services Pvt Ltd ("<strong><i>pink</i>Whale</strong>").<br><br>
			<strong><i>pink</i>Whale</strong> offers a variety of online products and services enabled by telephone, internet, online 
			messaging, online chat, including: Consult a Doctor, Consult a Specialist, Consult a Counsellor, Get 
			a Second Opinion, Personal Health Record ("PHR"), and Order Lab Tests (together, "Services").<br><br>
			DO NOT use the Site or <strong><i>pink</i>Whale</strong>'s Services for EMERGENCY MEDICAL NEEDS. You understand 
			that, in case of a medical emergency, you must immediately contact the emergency number in your 
			country. This Site and <strong><i>pink</i>Whale</strong>'s Services does not treat medical emergency and is not liable for 
			the same.
		</div>
	</div>

	<div class="page-row terms">
		<div class="content-area row-bg-2 row-bg-3 row-bg-4">
			<div class="row-header">
				<img src="img/privacypolicy/Terms-of-Service_Icon.png" class="row-title-image">
				<div class="row-title first">Registration:</div>
				<div class="row-description">
					To avail of the Site and Services offered by pinkWhale, you are required to register with us by completing the 
					registration process prescribed in the 'Registration' section of the Site. <br><br>
					By visiting, accessing or registering on this Site, you have unconditionally accepted to be bound by the terms and 
					conditions provided herein below ("Terms of Service"). If you do not agree to these Terms of Service, please do not 
					access or use this Site or pinkWhale's Services. You agree and confirm that the Terms of Service may be amended 
					from time to time at the sole discretion of pinkWhale, without notice. Please ensure that you periodically update 
					yourself of the current version of the Terms of Service. <br><br>
					You agree to (i) provide current, complete and accurate information as required by the registration process and (ii) 
					maintain and update this information, when required. By submitting the registration form, you represent that the 
					information provided by you in your registration form is true. <br><br>
					pinkWhale requires that to avail Services, you must be over eighteen (18) years of age. In the case of minors (under 
					eighteen -18 - years), a parent or guardian must accept these Terms of Service, and monitor all consultations under 
					the offered Services. The parent or guardian assumes full responsibility for ensuring that the information submitted 
					is accurate and true.<br><br>
					pinkWhale has the right to discontinue service, or deny access to anyone who violates our policies or the terms and 
					conditions stated herein, without prior notice or warning
				</div>

				<div class="row-title">Use of the Site:</div>
				<div class="row-description">
					Upon registration, you can avail the Service as per the packages available to you on the Site. You can select the 
					package you want and make the payment of the fee ("Service Fee") for that particular package as detailed against 
					each package in the concerned section of the Site. You agree and confirm that the Site may, at its sole discretion, 
					change the Service Fee structure at any time without prior intimation.<br><br>
					You understand that, except for information or Services clearly identified as being supplied by pinkWhale on the 
					Site, the Site does not operate, control or endorse any information, products or services on the Internet in any way.<br><br>
					You understand that the information or advice provided on the Site by a general physician/ specialist doctor/ expert 
					doctor/ health specialist/ wellness specialist ("Professional"), are for general information and are not intended to 
					substitute for informed professional medical advice and do not establish a professional-client relationship.<br><br>
					Therefore, before acting on any such advice provided by a Professional on the Site, you will confirm that the 
					treatment plan recommended is suitable to you by referring to your own physician/ medical practitioner. No 
					medications or treatments described on this Site should be taken without the prior consultation of your physician. 
					The advice provided to you is the opinion of the concerned Professional and not of pinkWhale. You understand and 
					confirm that reliance on any information or advice provided by the Professionals on the Site is solely at your own 
					risk. While utmost care is taken to provide correct and up to date information, pinkWhale does not warrant the 
					accuracy, completeness and timely availability of the information provided on the site and accepts no responsibility 
					or liability for any error or omission in any information provided on the Site.<br><br>
					You understand that, upon payment of Service Fee/Consultancy Fee, as the case may be, through the payment 
					gateway or through other payment channels, you can avail any of the Services/packages provided in the Site in 
					accordance with the terms mentioned in the Site. Depending on your package, you can obtain the required advice 
					from the doctor for the prescribed duration, by making a phone call/through web consultation. <br><br>
					Your PHR is created to store your personal health information. Any information provided as part of your use of 
					Services becomes part of your PHR. You agree to provide accurate information, periodically review such 
					information, and update such information as and when necessary.<br><br>
					pinkWhale may send you information and messages on medical and health-related topics, including links to other 
					related websites. These messages are not a substitute for consulting an appropriate healthcare professional for 
					medical treatment. Such general messages and communications are not comprehensive in nature and are solely 
					meant for educational, informational purposes and should not be relied upon as a substitute for medical diagnosis 
					and treatment or construed, directly or indirectly as the practice of medicine or dispensing of medical services by 
					pinkWhale. The information contained in these messages may be compiled from a variety of sources and may or 
					may not be authored by pinkWhale. Hence, pinkWhale makes no warranty with respect to the content of these 
					materials or the information contained therein.<br><br>
					You agree that you will not use the Site to post any advertisement or any other unauthorized commercial 
					communications. You agree that you will not use the Site for anything unlawful, misleading, malicious, or 
					discriminatory or post any knowingly false, defamatory, inaccurate, abusive, harassing, obscene, sexually oriented 
					or threatening message or material that is illegally invasive of another person's privacy.
				</div>

				<div class="row-title"></div>
				<div class="row-description">
					The entire contents of the Site are protected by international copyright and trademark laws. The owner of the 
					copyrights and trademarks are pinkWhale, its affiliates or other third party licensors. You may not modify, copy, 
					reproduce, republish, upload, post, transmit, or distribute, in any manner, the material on the Site, including text, 
					graphics, code and/or software. You may print and download portions of material from the different areas of the 
					Site solely for your own non-commercial use provided that you agree not to change or delete any copyright or 
					proprietary notices from the materials. You agree to grant to pinkWhale a non-exclusive, royalty-free, worldwide, 
					perpetual license, with the right to sub-license, to reproduce, distribute, transmit, create derivative works of, 
					publicly display and publicly perform any materials and other information (including, without limitation, ideas 
					contained therein for new or improved products and Services) you submit to any public areas of the Site (such as 
					bulletin boards, forums and newsgroups) or by e-mail to the Site by all means and in any media now known or 
					hereafter developed. You also grant to pinkWhale the right to use your name in connection with the submitted 
					materials and other information as well as in connection with all advertising, marketing and promotional material 
					related thereto. You agree that you shall have no recourse against pinkWhale for any alleged or actual infringement 
					or misappropriation of any proprietary right in your communications with the Site. <br><br>
					pinkWhale reserves the right to modify, add, delete and/or change the contents and classification and presentation 
					of the information / content at any time as it may in its absolute discretion find to be expedient and without giving 
					any notice. You understand that as a user, it is your responsibility to refer to the terms and/or any change or 
					addition to the same while accessing the site.<br><br>
					The Site provides at many places links to other websites hosted on servers maintained by third parties over which 
					the Site has no control. pinkWhale and the Site accept no responsibility as to the validity of contents and shall in no 
					way be liable for any damage/loss arising to users seeking access or using such websites. <br><br>
					This Site also contains resources which incorporate material contributed or licensed by individuals, companies, or 
					organizations that may be protected by Indian and foreign copyright laws. All persons reproducing, redistributing, 
					or making commercial use of this information are expected to adhere to the terms and conditions asserted by the 
					respective copyright holder. Transmission or reproduction of protected items beyond that allowed by the 
					applicable copyright laws requires the written permission of the copyright owners. <br><br>
					The names and logos and all related products and service names, design marks and slogans are trademarks and 
					service marks are owned by and used under license from pinkWhale. Publications, products, content or Services 
					referenced herein or on the Site are the exclusive trademarks or service marks of pinkWhale. All other trademarks 
					and service marks herein are the property of their respective owners.
				</div>

				<div class="row-title">Privacy Policy:</div>
				<div class="row-description">
					pinkWhale is committed to protecting the privacy and confidentiality of any personal information that it may 
					request and receive from visitors and users of the Site. To read our privacy policy regarding such personal 
					information please refer Privacy Policy. 
				</div>

				<div class="row-title">Indemnity: </div>
				<div class="row-description">
					You agree to defend, indemnify, and hold pinkWhale, its officers, directors, employees, agents, licensors, and 
					suppliers, harmless from and against any loss, damages, claims, actions or demands, liabilities and settlements 
					including without limitation, reasonable legal and accounting fees, resulting from, or alleged to result from, your 
					breach or violation of these Terms of Service.
				</div>

				<div class="row-title">Limitation of Liability: </div>
				<div class="row-description">
					The use of this Site is entirely at your own risk, and in no event shall pinkWhale be liable for any direct, indirect, 
					incidental, consequential, special, exemplary, punitive, or any other monetary or other damages, fees, fines, 
					penalties, or liabilities arising out of or relating in any way to this Site, or sites accessed through this Site, and/or 
					content or information provided herein. Your sole and exclusive remedy for dissatisfaction with the Site is to stop 
					using the Site. Access to the Site may not be legal by certain persons or in certain states or countries. If you access 
					the Site from outside India, you do so at your own risk and are responsible for compliance with the laws of your 
					jurisdiction.<br><br>
					YOU EXPRESSLY UNDERSTAND AND AGREE THAT: A) YOUR USE OF PINKWHALE IS AT YOUR SOLE RISK. THE SITE IS 
					PROVIDED ON AN "AS IS" BASIS AND WITHOUT WARRANTY OF ANY KIND. TO THE MAXIMUM EXTENT PERMITTED BY 
					LAW, PINKWHALET EXPRESSLY DISCLAIMS ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS 
					OR IMPLIED. B) PINKWHALE DOES NOT MAKE ANY WARRANTY (I) THAT THE SITE WILL MEET YOUR REQUIREMENTS 
					OR IS MERCHANTABLE OR FIT FOR A PARTICULAR PURPOSE, (II) THAT THE SITE WILL BE ERROR-FREE OR BUG-FREE, 
					(III) REGARDING THE SECURITY, RELIABILITY, TIMELINESS, OR PERFORMANCE OF THE SITE, (IV) THAT ANY ERRORS IN 
					THE SITE WILL BE CORRECTED, (V) accuracy, suitability or completeness OF THE INFORMATION CONTAINED IN THE 
					SITE, (VI) Regarding the qualifications of the PROFESSIONALS THAT PROVIDE SERVICES or advice ON THE SITE, (VII) 
					ANY INCORRECT INFORMATION OR ADVICE PROVIDED BY THE PROFESSIONALS OR CONTAINED WITHIN THE SITE, 
					(VIII) FOR ANY MEDICAL, LEGAL OR FINANCIAL EVENTS OR OUTCOMES RELATED TO SERVICES ATTAINED THROUGH 
					THE USE OF THIS SITE AND (IX) DELAY IN CALLING BACK OR RECONNECTING, TO OBTAIN COMPLETE ADVICE FROM 
					THE PROFESSIONAL (X) ANY INFORMATION OR MATERIALS OBTAINED BY USING THIS SITE, (XI) YOUR CHOICE TO 
					SEEK PROFESSIONAL MEDICAL ADVICE, OR YOUR CHOICE OF CHOOSING A SPECIFIC SERVICE/PACKAGE BASED ON 
					THE INFORMATION PROVIDED THROUGH THIS SITE, IS DONE AT YOUR SOLE DISCRETION AND/OR RISK. YOU WILL 
					BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA 
					THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH CONTENT OR MATERIAL. YOU EXPRESSLY UNDERSTAND AND 
					AGREE THAT PINKWHALE'S TOTAL LIABILITY SHALL ALWAYS BE LIMITED TO DIRECT, GENERAL DAMAGES NOT 
					EXCEEDING THE SERVICE FEE/ CONSULATION FEE, AS THE CASE MAY BE, PAID BY YOU TO PINKWHALE TOWARDS 
					THE CONCERNED SERVICE/PACKAGE AND THAT PINKWHALE SHALL NOT BE LIABLE TO YOU FOR ANY INDIRECT, 
					INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES.
				</div>

				<div class="row-title">Term and Termination </div>
				<div class="row-description">
					The Terms of Service shall become effective from the date on which the same has been posted on this Site. 
					PinkWhale reserves the right, without providing prior notice, to delete your account or any information contained 
					on the Site for any reason whatsoever.
				</div>

				<div class="row-title">Governing Law and Dispute Resolution:</div>
				<div class="row-description">
					These Terms of Service will be governed by and construed in accordance with the laws of India, without giving effect 
					to its conflict of laws provisions or your actual state or country of residence, and you agree to submit to personal 
					jurisdiction in India at Bangalore. <br><br>
					Any disputes and differences whatsoever arising in connection with these Terms of Service shall be settled by 
					arbitration in accordance with the Arbitration and Conciliation Act, 1996. Unless the Parties agree on a sole 
					arbitrator, there shall be three Arbitrators, one to be selected by each of the parties, and the third to be selected by 
					the two Arbitrators appointed by the parties. The venue of Arbitration shall be in Bangalore, India and the language 
					shall be English.
				</div>
			</div>
		</div>
		<div class="content-area">
			<div class="triangle">
				<a href="#top"><div class="top-triangle"></div>top</a>
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
	<script>
		$(document).ready(function(){
			$('.content-area .triangle a').click(function(e){
				e.preventDefault();
				$('html,body').animate({scrollTop:$('#top').offset().top-60},"slow");
			});
		});
	</script>
<?php include "includes/end.php" ?>
