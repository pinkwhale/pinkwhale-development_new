<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<link href="css/home-patients.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients"></div>
	<?php include "pw_Patients/menu-patients.php" ?>
		
	
	
	<div class="banner-bg banner-bg-patients book-online">
		<div class="content-area">
			<div class="red-text hidden-xs hidden-sm"><i>Scheduling an appointment <br> has never been easier!</i></div>
		</div>
	</div>

	<div class="page-tagline row-bg-1">
		<div class="max-960">
			<i>pink</i>APPOINT BRINGS YOU CLOSER<br>TO SEEING YOUR DOCTOR
		</div>
	</div>

		<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<!-- <span class="form-tagline">Book a Clinic Appointment with Dr. Vanitha here.</span> -->
				<span class="form-tagline">Book Appointment</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form">
						<!-- <div class="form-group">
							<label for="fname" class="col-xs-6 control-label">First Name:*</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="fname" >
							</div>
						</div>	
						<div class="form-group">
							<label for="lname" class="col-xs-6 control-label">Last Name:*</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="lname" >
							</div>
						</div>						
						<div class="form-group">
							<label for="email" class="col-xs-6 control-label">Email*</label>
							<div class="col-xs-6">
								<input type="email" class="form-control" id="email" >
							</div>
						</div>
						<div class="form-group">
							<label for="reason" class="col-xs-6 control-label">Reason for Visit:*</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="reason" >
							</div>
						</div>	
						<div class="form-group">
							<label for="clinic" class="col-xs-6 control-label">Choose Clinic:*</label>
							<div class="col-xs-6">
								<select class="form-control">
									<option></option>
								</select>
							</div>
						</div> -->


						<div class="form-group">
							<label for="dname" class="col-xs-6 control-label">Doctor Name:</label>
							<div class="col-xs-6">
								<p class="form-control-static">Dr Vanitha</p>
							</div>
						</div>	
						<div class="form-group">
							<label for="pname" class="col-xs-6 control-label">Patient Name:</label>
							<div class="col-xs-6">
								<input type="textbox" class="form-control" id="pname" >
							</div>
						</div>						
						<div class="form-group">
							<label for="age" class="col-xs-6 control-label">Age:</label>
							<div class="col-xs-6">
								<input type="number" class="form-control" id="age" >
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-xs-6 control-label">Email:</label>
							<div class="col-xs-6">
								<input type="email" class="form-control" id="email" >
							</div>
						</div>	
						<div class="form-group">
							<label for="mobno" class="col-xs-6 control-label">Mobile No:</label>
							<div class="col-xs-6">
								<input type="number" class="form-control" id="mobno">
							</div>
						</div>
						<div class="form-group">
							<label for="gender" class="col-xs-6 control-label">Gender:</label>
							<div class="col-xs-6">
								<select class="form-control">
									<option selected disabled>--- Select Gender --</option>
									<option>Male</option>
									<option>Female</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-xs-6 control-label">Appointment Time:</label>
							<div class="col-xs-6">
								<p class="form-control-static" id="bookonline-time-slot"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-xs-6 control-label">New Patient:</label>
							<div class="col-xs-6">
								<select class="form-control">
									<option selected disabled>----Select----</option>
									<option></option>
									<option></option>
								</select>
							</div>
						</div>				



						<!-- <div class="form-group">
							<label for="clinic" class="col-xs-6 control-label">Choose a Time Slot:*</label>
							<div class="col-xs-3" style="padding-right: 5px;">
								<input type="textbox" class="form-control">								
							</div>
							<div class="col-xs-3" style="padding-left: 5px;">
								<input type="textbox" class="form-control">								
							</div>
						</div>	 -->	
						<!-- <div class="form-group">
							<label for="clinic" class="col-xs-6 control-label">Choose a Time Slot:*</label>
							<div class="col-xs-6">
								<input id="bookonline-time-slot" type="textbox" class="form-control">								
							</div>							
						</div> -->
						
						<!-- <div class="calendar-container">
							<div class="arrow-1 arrow-left-1"></div>
							<div class="arrow-1 arrow-right-1"></div>
							<table>
								<tr>
									<th>Mon<br><span class="small-header">12 Dec 2104</span></th>
									<th>Tue<br><span class="small-header">12 Dec 2104</span></th>
									<th>Wed<br><span class="small-header">12 Dec 2104</span></th>
									<th>Thur<br><span class="small-header">12 Dec 2104</span></th>
									<th>Fri<br><span class="small-header">12 Dec 2104</span></th> 
									<th>Sat<br><span class="small-header">12 Dec 2104</span></th>
								</tr>

								<tr>
									<td></td> <td></td> <td>9:15am</td> <td></td> <td></td> <td>9:15am</td> 															
								</tr>
								<tr>
									<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td>9:30am</td> 															
								</tr>
								<tr>
									<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td>9:45am</td> 															
								</tr>
								<tr>
									<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>															
								</tr>
								<tr>
									<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>															
								</tr>
								<tr>
									<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td>10:30am</td> <td></td>															
								</tr>
								<tr>
									<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td>10:45am</td> <td></td>															
								</tr>
								<tr>
									<td></td> <td>More...</td> <td>More...</td> <td></td> <td>More...</td> <td></td>															
								</tr>
							</table>
						</div>		 -->


						<div class="calendar-container">
							<div class="arrow-1 arrow-left-1"></div>
							<div id="arrow-right" class="arrow-1 arrow-right-1"></div>
							<div class="booking-table-container">
								<table class="booking-table">
									<thead>
										<tr>
											<?php for($i=0;$i<=29;$i++){ ?>
			
											<th data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><?php echo date('D',$ts);?><br><span class="small-header"><?php echo date('M j',$ts);?></span></th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php for($x=0;$x<=4;$x++){?>
										<tr >
											<?php for($i=0;$i<=29;$i++){ ?>
											<td  class="time-value" data-ts="<?php echo $ts=time()+3600*24*$i;?>" id="<?php echo $ts;?>"><span>9:15am</span></td>
											<?php }?>
										</tr>
										<?php } ?>
										<tr>
											<?php for($i=0;$i<=29;$i++){ ?>
											<td class="more-value"><span>More..</span></td>
											<?php } ?>
										</tr>
									</tbody>
									<!-- <tr>
										<td></td> <td></td> <td>9:30am</td> <td></td> <td></td> <td>9:30am</td> 															
									</tr>
									<tr>
										<td></td> <td></td> <td>9:45am</td> <td></td> <td></td> <td>9:45am</td> 															
									</tr>
									<tr>
										<td></td> <td>10:00am</td> <td>10:00am</td> <td></td> <td></td> <td></td>															
									</tr>
									<tr>
										<td></td> <td>10:15am</td> <td>10:15am</td> <td></td> <td></td> <td></td>															
									</tr>
									<tr>
										<td></td> <td>10:30am</td> <td>10:30am</td> <td></td> <td>10:30am</td> <td></td>															
									</tr>
									<tr>
										<td></td> <td>10:45am</td> <td>10:45am</td> <td></td> <td>10:45am</td> <td></td>															
									</tr>
									<tr>
										<td></td> <td>More...</td> <td>More...</td> <td></td> <td>More...</td> <td></td>															
									</tr> -->
								</table>
							</div>

						</div>	

						<?php 
											
						/*$date = strtotime("+1 day", strtotime(date("Y-m-d")));
						echo date("M j", $date);*/
					
						?>
					
						
						<!-- <div class="form-group text-left">
							<div class="col-xs-offset-6 col-xs-6">
								<input type="submit" class="pw-btn" value="Book">
							</div>
						</div>		 -->	
						<div class="form-group">
							<div class="col-md-6">
							</div>
							<div class="col-md-6">
								<input type="submit" class="pw-btn" value="Book">
								<input type="submit" class="pw-btn" value="Cancel">
							</div>
						</div>									
					</form>
				</div>	
							
			</div>
			<!-- <div class="form-left col-xl-8 col-md-8 col-sm-8 col-xs-12">				
				<div class="form-elements">
					<div class="input-label col-xl-4 col-md-6 col-sm-6">First Name:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Last Name:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Email:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Reason for Visit:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Choose Clinic:*</div>
					<div class="input col-xl-8 col-md-6 col-sm-6">
						<label class="select-label ">
							<select class="input-fields">
								<option></option>
							</select>
						</label>
					</div>
					<div class="clearfix"></div>
					
					<div class="input-label col-xl-4 col-md-6 col-sm-6">Choose a Time Slot:*</div><div class="input col-xl-8 col-md-6 col-sm-6"><input type="text" class="input-fields timeslot">to <input type="text" class="input-fields timeslot"></div><div class="clearfix"></div>
					<div class="clearfix"></div>
					
					
					

					<div class="clearfix"></div>
					<br><br>
					<div class="input-label col-xl-4 col-md-6 col-sm-6"></div><div class="input col-xl-8 col-md-6 col-sm-6"><button type="submit" class="submit">Submit</button></div>
				</div>
			</div>	 -->		
			<div class="message form-right col-sm-4 hidden-xs">

				Why wait on hold on the phone <br>
				to schedule an appointment <br>
				when you can do it online<br>
				without wasting any time?<br> <br>
				Use pinkAppoint to book a  <br>
				Clinic appointment.  You don't<br>
				even need to register to<br>
				schedule one!<br>
			</div>
		</div>
	</div>
	

	
	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
	<script>
		$(function(){
			App.Date.init();

		});
	</script>
	<script>
		$(document).ready(function(){
			$('.arrow-right-1').click(function(){
				if($('table.booking-table').css('left') == '-2000px')
					return;
				var tablePos=$('.booking-table').position();
				var thwidth=$('.booking-table tr th').width();
				$('.booking-table').css("left",tablePos.left-80);
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.arrow-left-1').click(function(){
				console.log($('table.booking-table'));
				console.log(document.getElementsByClassName('booking-table'));

				if($('table.booking-table').css('left') == '0px' || $('table.booking-table').css('left') == 'auto')
					return;
				var tablePos=$('.booking-table').position();
				$('.booking-table').css("left",tablePos.left+80);
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.arrow-right-1').click(function(){
				
			});
		});
	</script>
	<script>
		$(function(){
			/*var text=$('.booking-table tr:nth-child(2) td:first-child').data('ts');
			var d=new Date(text*1000);
			$("#bookonline-time-slot").val(d);*/
			getTimeDate(0);			
		});	 
		function getTimeDate(a){				
			 $('.booking-table tbody tr .time-value').click(function(){
				var getTime=$(this).text();
				var getTs=$(this).data('ts');
				if(a==1)
					var d=new Date(getTs);
				else
					var d=new Date(getTs*1000);
				var getDate=d.getDate();
				var getMonth=d.getMonth();
				var getYear=d.getFullYear();
				var monthName=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];
				
				/*$('#bookonline-time-slot').val(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);*/
				$(this).each(function(){
					$('.booking-table tbody tr .time-value').removeClass("active-time");
				});
				$(this).addClass("active-time");
				$('#bookonline-time-slot').text(getDate+"-"+monthName[getMonth]+"-"+getYear+" "+getTime);
			});
		}	 

	</script>
	<script>
		$(function(){
			/*$('.booking-table tbody tr .more-value').click(function(){
				$('.booking-table tbody tr:nth-child(6)').css("display","none");
				$('.booking-table').html("test");
			});*/
			/*$('.booking-table tbody tr:nth-child(6)').click(function(){
				$('.booking-table tbody tr:nth-child(6) td').each(function(){
					$(this).text("9:15am");*/
					/*$("<tr><td>test</td><td>test</td><td>test</td><td>test</td></tr>").appendTo('.booking-table');*/

				/*});
				
			});*/
			$('.booking-table tbody tr:nth-child(6)').click(function(){
				$(this).css("display","none");
				var d=new Date();
				var tss=d.getTime();
				var html="";
				for(var j=0;j<=2;j++){
					html+='<tr>';
					for(var i=0;i<=29;i++){
						html+='<td class="time-value" data-ts='+tss+'><span>9:15am</span></td>';
					}
					html+='</tr>';
				}	
				$(html).appendTo(".booking-table");
				getTimeDate(1);
			});
		});
	</script>



<?php include "includes/end.php" ?>
