<?php
error_reporting(E_PARSE);
session_start();
include "actions/encdec.php";
include("site_config.php"); 
include "db_connect.php";
include "modify_doctorname.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Find a Specialist Doctor | pinkWhale Healthcare</title>
<meta name="keywords" content="specialist, doctor, online doctor, online consultation, telephone consultation, teleconsultation"/>
<meta name="description" content="If you are looking for convenient access to the best specialist doctors, you search ends here. We have a panel of qualified and renowned specialist doctors in several medical areas. Follow-up consultation with doctor has never been this easy.  Our specialists provide the convenience of phone consultation or online consultation for follow-up questions and visits. Find Your Specialist Now!"/>

<link href="<?= $sitePrefix ?>/css/designstyles.css" rel="stylesheet" type="text/css">
<script src="<?= $sitePrefix ?>/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/stepcarousel.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/carousel.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/search.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/login.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>
    #loading { 
        width: 100%; 
        position: absolute;
    }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<script src="<?= $sitePrefix ?>/js/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="<?= $sitePrefix ?>/text/javascript" src="js/header_colourchange.js"></script>
<script src="<?= $sitePrefix ?>/js/mouseoverscripts.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/NoIEActivate.js" type="text/javascript"></script>

<!-- header.......-->
<?php

 include 'header.php';
 include "site_config.php";
 ?>
<!-- header.......-->
<!--<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold'</script> -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td colspan="2"><div id="consult_banner">
<form method="POST" name="find_exp" id="find_exp">
<div id="banner_titles2"><h3>Find YOUR Specialist</h3><h4>& Start your E-Consult</h4></div> <br clear="all" />
<div id="banner_fields"><div class="txt">Doctors Name: </div><div class="inpt"><input name="doc_name" type="text" id="doc_name" size="25" /></div> 
<div class="txt">Specialty: </div><div class="inpt"><select name="specialist" id="specialist"  style="width:200px">
    <option value=""> -- Specialist  -- </option>
    <?php
    $qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE `doc_category`='Specialist' && blocked !='Y' and `doc_specialities`<>'' group by`doc_specialities`";
    if (!$qry_rslt = mysql_query($qry))
        die(mysql_error());
    while ($qry_result = mysql_fetch_array($qry_rslt)) {
        echo "<option value=\"$qry_result[0]\">$qry_result[0]</option>";
    }
    ?>
</select></div> <div class="txt"> </div><div class="inpt"><input name="input" type="button"  class="findSpe"  value=""  onclick="search_health_expert()"/>
</div></div>
</form>
</div></td></tr></table>


<div id="loading" align="center"></div>
<div id="spec_search">

<?php

//$doc_name = mysql_escape_string(trim(urldecode($_POST['doc_name'])));

//$doc_spe = mysql_escape_string(trim(urldecode($_POST['specialist'])));

	/*--------Start Get Doctor Name Value ------*/

        $doc_name_get = trim($_GET["exp_docname"]);
        
        $doc_name_get_TMP = $doc_name_get;
        
        $doc_name_get = str_replace("-"," ",$doc_name_get);
        
        $doc_arr = explode(" ",$doc_name_get);
        
        $doc_arr_len = sizeof($doc_arr);
                
        $doc_name_get = mysql_escape_string($doc_name_get);
        
        $doc_name = "";
        

        /*--------End Get Doctor Name Value ------*/

        /*--------Start Get Specialist Value ------*/

        $doc_spe = "";

        /*--------END Get Specialist Value ------*/


$spe = $doc_spe;

if($doc_name!=null){

    $doc_name = " and doc_name like '%".$doc_name."%'";

}

if($doc_spe!=null){
    
    $doc_spe = " and doc_specialities='".$doc_spe."'";
    
}

$display = "<div align='center'><img src='$sitePrefix/images/pinkConsultbar.jpg' /></div>";
$display .= "<div id='DocSearch' style='border:0px'><table align='center' align='center' border='0' cellpadding='0' cellspacing='1' width='100%' class='attable' id='mytable'>";

$display .="<tr>";

if($spe!=null){
 
    $display .="<th width='35%' align='left' style='font-size:16px'>$spe</th>";
    
}else{
    
    $display .="<th width='35%' align='left' style='font-size:16px'><strong>Specialist</strong></th>";
}

$display .="<th align='center'>Visiting Hospitals</th><th align='center'>Visiting Clinics</th><th align='center'></th></tr>";

$qry = "SELECT * FROM `pw_doctors` WHERE (`doc_category`='Specialist' || `doc_category`='Specialist/Expert') && blocked !='Y' $doc_name $doc_spe order by doc_name ASC";

$res = mysql_query($qry);

$num = mysql_num_rows($res);

if($num>0){

    while($dat = mysql_fetch_array($res)){
        
        $check_docname = modifyname($dat['doc_name']);

        if($check_docname == $doc_name_get_TMP){

            $display .= "<tr><td>";

            if($dat['doc_photo']!=""){

                $display .= "<div class=\"docPhoto\"><a href='#'><img src='$sitePrefix/".$dat['doc_photo']."' width=\"73\" height=\"83\" onclick=\"spe_popup(true,'".$dat['doc_id']."')\" /></a></div>";

            }else{

                $display .= "<div class=\"docPhoto\"><a href='#' ><img src='$sitePrefix/images/doctor.png' width=\"73\" height=\"83\" onclick=\"spe_popup(true,'".$dat['doc_id']."')\" /></a></div>";

            }

            $display .= "<div class=\"docDis\"><a href='#' onclick=\"spe_popup(true,'".$dat['doc_id']."')\"><span>".$dat['doc_name']."</span></a><br /> ".$dat['doc_qualification']."<br /><br />";

            $display .="<form method='post' action='".$sitePrefix."/patient_new_exp_email_consultation.php'><input type='hidden' name='doc_id' value='".$dat['doc_id'] . "' />";

            if(!isset($_SESSION['username']))
            {
                    $display .= "<a href='#top'><img src='".$sitePrefix."/images/stratConsultBtn.jpg' onClick='login_callpopup(true,\"".$dat['doc_id']."\");' style='cursor:pointer;' /></a>";
            }
            else
            {
                    $display .= "<input src='".$sitePrefix."/images/stratConsultBtn.jpg' type='image' style='cursor:pointer;'/>";
            }

            $display .= "</form>";

            $display .= "</div></div>";

            $display .= "<!--Specialist-pop-up--><div id=\"spe_pop_".$dat['doc_id']."\" style=\"display:none; z-index: 10008; left:10%; height:1000px;  width:85%; height: auto; position: absolute; top:20%;   border:4px solid #666; background-color:#fff;\" >";

            $display .= "<div style=\"float:right ; padding:5px;\"> <a href=\"#\" onclick=\"spe_popup(false,'".$dat['doc_id']."')\">
                         <img src=\"$sitePrefix/images/close.gif\" border=\"0\" /></a> 
                         </div>";

            if($dat['doc_photo']!=""){

                  $display .= "<div><table><tr><td align='left' colspan='2'><div class=\"docPhoto\"><img src='$sitePrefix/".$dat['doc_photo']."' width=\"73\" height=\"83\"/></div>";

            }else{

                  $display .= "<div><table><tr><td align='left' colspan='2'><div class=\"docPhoto\"><img src='$sitePrefix/images/doctor.png' width=\"73\" height=\"83\"/></div>";

            }

            $display .= "<div class=\"docDis\"><strong style='color:#0C4686'>".$dat['doc_name']." </div></td></tr>";

            $display .= "<tr><td><strong>Qualification</strong> &nbsp;:&nbsp;".$dat['doc_qualification']." </td>";

            $display .= "<td rowspan='2'></td></tr>";

            $display .= "<td><strong>Specialization</strong> &nbsp;:&nbsp;".$dat['doc_specialist_for']." </td></tr>";

            $display .= "<tr><td colspan=2><p> ".$dat['about_doctor']."</p></td></tr>";

            $display .= "</table></div>";

            $display .= "</div><div id=\"spe_divDisable_".$dat['doc_id']."\" style=\"DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48\"> </div>";

            $display .= "<!-- POP-UP-END -->";

            $visit_hospital_name = $dat['visit_hospital_name'];        

            $display .= "</td><td align='center'>";

            if($visit_hospital_name==""){

                $display .="<font color='red'>None</font>";

            }else{

                $visit_h_name = explode(";", $visit_hospital_name);

                $h_size = sizeof($visit_h_name);

                for($i=0 ; $i<$h_size ;$i++ ){

                    $display .= "<font color='#4065B3'>".$visit_h_name[$i]."</font><br />";

                }

            }

            $display .="</td >";

            $visit_clinic_name = $dat['visit_clinic_name'];

            $display .= "</td><td align='center'>";

            if($visit_clinic_name==""){

                $display .="<font color='red'>None</font>";

            }else{

                $visit_c_name = explode(";", $visit_clinic_name);

                $clinic_size = sizeof($visit_c_name);

                for($i=0 ; $i<$clinic_size ;$i++ ){

                    $display .= "<font color='#4065B3'>".$visit_c_name[$i]."</font><br />";

                }

            }

            $display .="</td>";

            $display .= "<td align='center'>";
           /* 
            $display .="<form method='post' action='".$sitePrefix."/patient_new_exp_email_consultation.php'><input type='hidden' name='doc_id' value='".$dat['doc_id'] . "' />";

            if(!isset($_SESSION['username']))
            {
                    $display .= "<a href='#top'><img src='".$sitePrefix."/images/stratConsultBtn.jpg' onClick='login_callpopup(true,\"".$dat['doc_id']."\");' style='cursor:pointer;' /></a>";
            }
            else
            {
                    $display .= "<input src='".$sitePrefix."/images/stratConsultBtn.jpg' type='image' style='cursor:pointer;'/>";
            }

            $display .= "</form>";
            */
            $display .="</td>";

            $display .= "</td></tr>";

        }
    
    }
    
}else{
    
    $display .="<tr><td colspan='3' align='center'><font color='red'>Doctor Not Found</font></td></tr>";
    
}

$display .="</table></div>";

echo $display;


?>

</div>
<div id="spe_details"> 

</div>
<!-- footer -->

<?php
include 'footer.php'; 
?>

 <!-- login popup start -->
  
 <div id="login_popupId" style="display:none; z-index: 10008; left:33%; height:220px;  width:406px; height: auto; position: absolute; top:20%; font-family:Arial; font-size:13px;
   border:4px solid #666; background:url('<?= $sitePrefix ?>/images/bg-btn-blue.png') repeat-x ; background-color:#fff;" > 
    
    <div style="float:right ; padding:5px;"> <a href="#" onclick="login_callpopup(false,0)"><img src="<?= $sitePrefix ?>/images/close.gif" border="0" /></a> </div>
       <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; "><center> Login </center></div> <br/>
    <div id="loginarea">

    <br/>
    <form name="consult_signin" id="consult_signin" method="post" action="<?= $sitePrefix."/patient_new_email_consultation.php" ?>">
        <div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
        
        <div class="loginrow"> <span class="label_logn"> UserName : </span>
          <span class="forml_logn">
            <input name="username" id="username"  class="input" type="text"  size="18" />
          </span>
        </div>
        
            <div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        
        <div class="loginrow"> <span class="label_logn">Password :</span> 
          <span class="forml_logn">
            <input name="password" id="password" type="password"   class="input"  size="18"  />
          </span>
        </div>
            <div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        <div class="loginrow"></div>
        <div class="loginrow"> <span class="label_logn">&nbsp;</span> 
        <span ><input type="hidden" name="doc_id" id="doc_id" />
            <input type="submit" id=""  value=""  class="signinBtn"  title="Login" onclick="return consult_LoginSignUp(consult_signin)"/>
        </span> </div>
    </form>

  </div>
</div>

<div id="login_divDisable" style="DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48"> </div>

 <!-- login popup End -->
 
</body></html>


