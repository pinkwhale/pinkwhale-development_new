<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="229" style="border-right:1px solid #4d4d4d" valign="top">
<table width="220" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<tr><th>Specialities:</th></tr>
<tr><td><a href="specialist.php">Addiction Medicine Specialist</a></td></tr>
<tr><td><a href="specialist.php">Allergist</a></td></tr>
<tr><td><a href="specialist.php">Cardiologist</a></td></tr>
<tr><td><a href="specialist.php">Cosmetic Surgeon</a></td></tr>
<tr><td><a href="specialist.php">Critical Care Specialist</a></td></tr>
<tr><td><a href="specialist.php">Dentist</a></td></tr>
<tr><td><a href="specialist.php">Dermatologist</a></td></tr>
<tr><td><a href="specialist.php">Diabetologist</a></td></tr>
<tr><td><a href="specialist.php">Endocrinologist</a></td></tr>
<tr><td><a href="specialist.php">ENT Specialist</a></td></tr>
<tr><td><a href="specialist.php">Gastroenterologist</a></td></tr>
<tr><td><a href="specialist.php">Gynecologist</a></td></tr>
<tr><td><a href="specialist.php">Hematologist</a></td></tr>
<tr><td><a href="specialist.php">HIV AIDS Specialist</a></td></tr>
<tr><td><a href="specialist.php">Infertility Specialist</a></td></tr>
</table></td>

<td width="740" valign="top"><table width="740" border="0" cellspacing="0" cellpadding="0">

<tr><td><img src="images/sopinion_steps.jpg" width="740" height="66"/></td></tr>

<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="s90specialtiesdocs" align="center">
<tr><th colspan="9">Second Opinion Doctors:</th></tr>
<tr><td width="16" class="s90unselected"><img src="images/larrow.jpg" width="16"></td>
<td width="96" class="s90unselected"><img src="images/drspec1.jpg" width="72" height="72" /><br />Dr. Rajesh Sharma<br />Cardiology</td>
<td width="96" class="s90selected"><img src="images/drspec2.jpg" width="72" height="72" /><br />Dr. Rajeev Annigeri<br />Cardiology</td>
<td width="96" class="s90unselected"><img src="images/drspec3.jpg" width="72" height="72" /><br />Dr. Javahar Panjwani<br />Cardiology</td>
<td width="96" class="s90unselected"><img src="images/drspec4.jpg" width="72" height="72" /><br />Dr. Rajendra Karkare<br />Cardiology</td>
<td width="96" class="s90unselected"><img src="images/drspec1.jpg" width="72" height="72" /><br />Dr. Rajesh Sharma<br />Cardiology</td>
<td width="96" class="s90unselected"><img src="images/drspec2.jpg" width="72" height="72" /><br />Dr. Rajeev Annigeri<br />Cardiology</td>
<td width="96" class="s90unselected"><img src="images/drspec3.jpg" width="72" height="72" /><br />Dr. Javahar Panjwani<br />Cardiology</td>
<td width="16" class="s90unselected"><img src="images/rarrow.jpg" width="16"></td></tr>
<tr><th colspan="9" class="s90selecteddrdetails">
<h2><strong style="color:#b60359">Dr. Rajeev Annigeri</strong> -  M.S., M.Ch., Cardio-Thoracic Surgery <a href="specialist_doctor.php"><img src="images/view_profile.jpg" height="16" align="absmiddle"></a></h2>

<p>Dr. Rajesh is the Starr-Ahmad fellow from Oregon Health & Science University, Portland, Oregon, USA. He has handled over 6000 cardiac cases in his career and was awarded Chief of Army Staff Commendation Medal for inspiring relief work during Bhuj earthquake.</p>

<p><a href="patient_email_consultation.php"><img src="images/emailconsult.jpg" width="247" height="37" /></a></p>

</th></tr></table></td></tr></table></td></tr></table>


<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>