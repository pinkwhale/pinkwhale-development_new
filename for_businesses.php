<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>
<?php
	include 'header.php'; 
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td colspan="2" class="s90cpages">
<div style="font-family:Arial; font-size:20px; line-height:24px; color:blue; font-weight:bold; margin:5px 5px 20px 10px">
Employers
</div>
<p>

<table width="978" border="0" cellspacing="0" cellpadding="0" style="border-bottom:0px solid #000">
<tr><td width="613" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="250"><br /><img src="images/forb1.jpg" width="229" height="177" /> <br /><br /> </td>
<td width="363" valign="top">
<div style="font-family:Arial; font-size:20px; line-height:20px; color:#e36c0a; font-weight:bold; margin:24px 0px 10px 0px">
DID YOU KNOW?</div>
<div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:black;  margin:10px 0px 10px 0px"> <img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> 91% of the employees questioned in a research survey admitted that personal life events led to loss of productivity at work.</div>
<div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:black;  margin:10px 0px 10px 0px"> <img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> Work and personal  responsibilities can weigh down on any of your employees and shift their  attention and energy away from your business.</div>
</td></tr>
</table></td>
<td width="365"><img src="images/fb1.jpg" width="357" height="231"/></td></tr><tr></tr></table>

<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; margin-bottom:20px;">
<div style="font-family:Arial; font-size:20px; line-height:20px; color:purple; font-weight:bold; margin:24px 0px 10px 0px" align="left">
Your employees are busy. Here's what they are saying:</div>
<tr><td width="610"><img src="images/busy_employees_no_time_for_health.jpg" width="595" height="380" /></td>
<td width="368" valign="top">
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"I am so stressed out, I can't think straight anymore. I need to talk to someone"</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"I have no time to visit the clinic for an annual check up"</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"I travel often and find it difficult to reach a doctor"</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"I have been putting on weight despite my best efforts and need some expert advice"</div>

</td></tr></table>


<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px; margin-bottom:10px">

<div style="font-family:Arial; font-size:20px; line-height:20px; color:#e36c0a; font-weight:bold; margin:24px 0px 10px 0px">What is Wellness@myDesk?</div>
<tr>

<td width="100%" valign="top">

<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Comprehensive Health & Wellness program to look at total well-being of employees.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Supports employees by offering them detailed health & wellness information, tools, and resources and help them to make informed decisions about their health and well-being.</div> 
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Covers Physical Health, Nutritional Health, and Emotional Health.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Anytime, Anywhere, access to health professionals by Phone or Online.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Covers employees and their family members.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Focus is on Prevention & Guidance, so employees can lead a healthy and stress-free life.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />No overhead for HR � Program is completely managed and administered by pinkWhale.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Bottom Line: Your employees get the best possible access to medical, health and wellness expertise whenever they most need it.</div>
</td>
<td><img src="images/forb3.jpg" width="200" style="margin-left:10px;" /></td>
</tr>

<tr><td colspan="2">
<div style="font-family:Arial; font-size:20px; line-height:20px; color:purple; font-weight:bold; margin:24px 0px 10px 0px">
Invest in your employee's well-being, reduce attrition, and build a high-performance company!</div></td></tr>


<tr><td colspan="2" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left">
<tr><td width="100%">
<div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">We Make It Easy For HR</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:10px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Launch the service and promote actively to  your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Newsletters and brochures for your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Manage the service on an on-going basis<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Seminars to increase awareness<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Participate and promote in your employee benefit and orientation workshops<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Bring professional speakers for information sessions on various health & wellness topics<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Brand it as your program<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Provide usage reports & recommendations<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Share best practices in the industry<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Recommend well-being programs to improve employee retention<br />
</div></td></tr></table>


<tr><td colspan="2" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left">
<td width="100%">
<div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">Corporate Benefits:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:10px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Employee motivation, high morale, leading to lower attrition and improved organizational health<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />92% of (corporate) respondents surveyed said that their productivity had significantly improved after proper counselling<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />According to ASSOCHAM 2009 study, 84% of employees find company-sponsored wellness programs as a motivating factor<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Provide your employees with better access to healthcare.<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Differentiate you from your competition by providing effective health benefit..<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Convenience for travelling employees..<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Increase productivity by reducing absenteeism..<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Reduce insurance claims by ensuring preventive approach to healthcare..<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> Has direct impact on the cost and quality of care for your employees..</div>
</td></tr></table></td></tr></table>

<div style="font-family:Arial; font-size:20px; line-height:20px; color:purple; font-weight:bold; margin:24px 0px 10px 0px">We help you to take care of your employee's Health and Wellness.</div>

<p style="text-align:right"><br /><strong>To learn more, email us at: <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a></strong></p>

</td>
</tr></table>


<?php
	include 'footer.php'; 
?>
<script type="text/javascript">
	document.getElementById("header_menu1").style.backgroundImage = "url(images/s90mainmenu1_bgp.jpg)";
	document.getElementById("header_menu2").style.backgroundImage = "url(images/s90mainmenu2_bgb.jpg)";
	document.getElementById("header_menu3").style.backgroundImage = "url(images/s90mainmenu3_bgb.jpg)";
</script>

</body></html>