<?php
ob_start();
session_start();
include "site_config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Get a Second Medical Opinion from Expert Doctors, Physicians, and Surgeons | pinkWhale Healthcare </title>

<meta name="keywords" content="cancer treatment opinion,diabetes treatment opinion, surgery opinion, plastic surgery, cosmetic surgery, second opinion diagnosis"/>

<meta name="description" content="pinkWhale has brought together world renowned super-specialists, who can give you a second medical opinion online. Our 2nd opinion doctors will review your medical information, medical records, and diagnostic tests to render a
comprehensive second opinion that includes recommendations, alternative treatment options, and therapeutic considerations."/>

<link href="<?= $sitePrefix ?>/css/designstyles.css" rel="stylesheet" type="text/css">
<script src="<?= $sitePrefix ?>/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/stepcarousel.js" type="text/javascript"></script>
<script src="<?= $sitePrefix ?>/js/carousel.js" type="text/javascript"></script>

<script src="<?= $sitePrefix ?>/js/login.js"></script>
<script src="<?= $sitePrefix ?>/js/search.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();



function spe_popup(obj,value){
        
      if(obj==true)        
      {
            document.getElementById('spe_pop_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.height = document.body.scrollHeight + "px";
            document.getElementById('spe_divDisable_'+value).style.width = document.body.scrollWidth + "px";             

      }else{
          
            document.getElementById('spe_pop_'+value).style.display='none';
            document.getElementById('spe_divDisable_'+value).style.display='none';

      } 
   
}

</script>
<style>

    #loading { 
        width: 100%; 
        position: absolute;
    }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<!-- header.......-->
<?php include 'header.php'; 
        include "actions/encdec.php";
        include "db_connect.php";
        
?>
<!-- header.......-->
<!--<script type="text/javascript"> document.getElementById('menu4').style.fontWeight= 'bold'</script> -->

 <table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td colspan="2">
<div id="opinion_banner">
<form method="POST" name="find_spe" id="find_spe">
<div id="banner_titles2"><h3>Find a Specialist</h3><h4>& Get a Second Opinion</h4></div><br clear="all" />            
<div id="banner_fields"> <div class="txt"> Doctors Name:</div><div class="inpt"> <input name="doc_name" type="text" id="doc_name" size="26" onkeypress="return isalphabitKey(event);" /></div>
<div class="txt"> Specialty:</div> <div class="inpt"><select name="specialist" id="specialist"  style="width:200px">
    <option value=""> -- Specialist  -- </option>
    <?php
    $qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE (`doc_category`='Expert' || `doc_category`='Specialist/Expert')&& blocked !='Y' group by`doc_specialities`";
    if (!$qry_rslt = mysql_query($qry))
        die(mysql_error());
    while ($qry_result = mysql_fetch_array($qry_rslt)) {
        echo "<option value=\"$qry_result[0]\">$qry_result[0]</option>";
    }
    ?>
</select></div>
 <div class="txt">  </div><div class="inpt">  <input name="input" type="button"  class="findSpe"  value=""  onclick="search_spe()"/></div>
</div>
</form>
</div>  </td></tr>
 </table>
        <div align="center" id="loading"></div>    
<div id="spec_search" ></div>
<div id="spe_details">
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
    <div class="cont_big">  <h5><span>pink</span>Opinion</h5>
 <p>Have you been recommended to take a surgery or pursue an expensive treatment plan?
Do you want reassurance that you have explored all treatment options before finalizing on a procedure?
                                                                                                      Our world renowned super-specialists will make this possible for you, so you can make informed decisions by providing  recommendations, alternative treatment options & therapeutic considerations.
</p>
  </div>
</td>
</tr>
<tr>
<td align="right" style="padding-top:3px">
<?php 
if(!isset($_SESSION['login']) || $_SESSION['login']=='Card')
{
?>

<a href="join.php"><img src="images/signUpNow.gif"  /></a>
        <a href="#"><img src="images/topupCard.gif" onclick="callpopup(true)"  /></a>

<?php } ?>
</td>
</tr>

</table>
</div>
<?php

include 'footer.php'; ?>


 <!-- login popup start -->
  
 <div id="login_popupId" style="display:none; z-index: 10008; left:33%; height:220px;  width:406px; height: auto; position: absolute; 
              top:20%; font-family:Arial; font-size:13px;  border:4px solid #666; background:url('<?= $sitePrefix ?>/images/bg-btn-blue.png') repeat-x ; background-color:#fff;" > 
    
    <div style="float:right ; padding:5px;"> <a href="#" onclick="login_callpopup(false,0)"><img src="<?= $sitePrefix ?>/images/close.gif" border="0" /></a> </div>
                <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; "><center> Login </center></div> <br/>
    <div id="loginarea">

    <br/>
    <form name="consult_signin" id="consult_signin" method="post" action="<?= $sitePrefix."/patient_new_exp_email_consultation.php" ?>">
        <div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
        
        <div class="loginrow"> <span class="label_logn"> UserName : </span>
          <span class="forml_logn">
            <input name="username" id="username"  class="input" type="text"  size="18" />
          </span>
        </div>
        
            <div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        
        <div class="loginrow"> <span class="label_logn">Password :</span> 
          <span class="forml_logn">
            <input name="password" id="password" type="password"   class="input"  size="18"  />
          </span>
        </div>
            <div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        <div class="loginrow"></div>
        <div class="loginrow"> <span class="label_logn">&nbsp;</span> 
        <span ><input type="hidden" name="doc_id" id="doc_id" />
            <input type="submit" id=""  value=""  class="signinBtn"  title="Login" onclick="return consult_LoginSignUp(consult_signin)"/>
        </span> </div>
    </form>

  </div>
  <br/>
</div>

<div id="login_divDisable" style="DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48"> </div>

 <!-- login popup End -->
 
</body></html>
<?php

ob_end_flush();

?>

