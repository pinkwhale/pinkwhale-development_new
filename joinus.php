 <?php
ob_start();
error_reporting(E_PARSE);
session_start();
?>
<?php
include "db_connect.php";

include "actions/encdec.php";

include 'site_config.php';


if (isset($_COOKIE['PinkWhale']) && $_SESSION['login'] != "Card") {

    $username = $_COOKIE['PinkWhale']['username'];

    $password_en = $_COOKIE['PinkWhale']['password'];

    $password = base64_decode($password_en);



    $query1 = "SELECT admin_name, admin_password FROM `login_user` where admin_name = '$username' and admin_password='$password'";

    $result1 = mysql_query($query1);



    if (mysql_num_rows($result1) > 0) {

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'admin';
    }



    $qry = "SELECT `user_name`,`user_id` FROM `user_details` WHERE `user_email`='$username' && `password`='$password_en'";

    $rslt = mysql_query($qry);

    if (mysql_num_rows($rslt) > 0) {

        while ($rs = mysql_fetch_array($rslt)) {

            $_SESSION['pinkwhale_id'] = $rs['user_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'user';
    }


    $qry1 = "SELECT `doc_name`,doc_id FROM `pw_doctors` WHERE `doc_email_id`='$username' && `password`='$password_en'";

    $rslt1 = mysql_query($qry1);

    if (mysql_num_rows($rslt1) > 0) {

        while ($rs = mysql_fetch_array($rslt1)) {

            $_SESSION['doctor_id'] = $rs['doc_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'doctor';
    }
}
?>
 <!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="By joining with us become one of our renowned doctors and raise your practice to a global standard">
	<meta name="keywords" content="Doctors registration, register doctor, join us, registration forms, doctors registration form, online registration form, doctor registration form">
    <title>Doctors - Join Us | pinkWhale Healthcare</title>
	<script src="js/jquery.min.js"></script>
    <?php include 'includes/include-css.php'; ?>
  </head>

<?php //include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<link href="css/joinus.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="pw_Doctors/js/doctor_join_validation.js"></script>
<?php 
error_reporting ( E_PARSE );
session_start ();
?>
	
	<div class="menu-data" data-main-page="doctors" data-sub-page="join-us"></div>
	<?php include "menu-doctors.php" ?>
	
	<div class="banner-bg banner-bg-joinus">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Increase your reach and accessibility<br>with the help of pinkWhale</i></div>
		</div>
	</div>
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			BECOME ONE OF OUR RENOWNED DOCTORS <span class="hidden-xs"><br></span>&amp; RAISE YOUR PRACTICE TO A GLOBAL STANDARD
		</div>
	</div>
	
	<div align="center"><span style="color:#3C6;"><?php if (isset($_SESSION['addoc']))
                                                    echo $_SESSION['addoc']; $_SESSION['addoc']=""; ?></span></div>

	<div class="form-block col-md-12 joinus row-bg-2">
		<div class="content-area form-contents ">
			<div class="form-header hidden-xs">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Apply to join our Virtual Klinic&trade; Network.</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form" action="pw_Doctors/actions/doc_join_action.php" method="POST" name="doc_join"
					enctype="multipart/form-data" id="doc_join">
						<div class="form-group">
							<label for="fname" class="col-xs-5 control-label">First Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="fname" name="fname">
							</div>
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="fname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="lname" class="col-xs-5 control-label">Last Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="lname" name="lname">
							</div>
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="lname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" name="email" >
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="email_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Phone No:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" name="mobile"  onkeypress="return isNumberKey(event);">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="phone_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="degree" class="col-xs-5 control-label">Degree:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="degree" name="degree">
							</div>
						</div>
						<div class="form-group">
							<label for="speciality" class="col-xs-5 control-label">Primary Speciality:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="speciality" name="speciality">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="spec_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="why" class="col-xs-5 control-label">Why are you interested in teleheath?*</label>
							<div class="col-xs-7">								
								<textarea class="form-control" id="why" name="why"></textarea>
							</div>
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:45px;">
						<div id="why_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="report" class="col-xs-5 control-label">Upload Resume:*</label>
							<div class="col-xs-7">
								<div class="fileUpload pw-btn">
								    <span>Choose File</span>
								    <input id="uploadBtn" name="uploadBtn" type="file" class="upload upload-button" />
								</div>
								<div id="uploadFile" class="upload-file-path"></div>
								<div class="clearfix"></div>																					
							</div>							
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:45px;">
						<div id="upload_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" value="Register" onclick="return add_doctor(doc_join)"/>
							</div>
						</div>										
					</form>
				</div>				
			</div>			
			<div class="message form-right col-sm-4 hidden-xs">
				Our Next Generation doctors <span class="hidden-xs hidden-sm"><br></span>
				are bringing their unique skills <span class="hidden-xs hidden-sm"><br></span>
				and knowledge online to reach <span class="hidden-xs hidden-sm"><br></span>
				more patients, faster, and more <span class="hidden-xs hidden-sm"><br></span>
				efficiently. Contact pinkWhale <span class="hidden-xs hidden-sm"><br></span>
				for more information on joining <span class="hidden-xs hidden-sm"><br></span>
				this prestigious league of <span class="hidden-xs hidden-sm"><br></span>
				doctors.
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
