<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<link href="css/home-doctors.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="doctors"></div>
	<?php include "includes/menu-doctors.php" ?>
		
	
	<!-- <img src="img/home/doctors/Patients_Banner with Text.png" class="page-banner"> -->
	<div class="banner-bg banner-bg-doctors">
		<div class="content-area">
			<div class="tag-line">We help you upgrade your Practice</div><br><br>
			<div class="text">Become a part of our growing virtual<br>Community of Trusted global Doctors</div><br>
			<a href ="" class="button-learn">Join Us</a>
		</div>
	</div>
	

	<div class="page-tagline doctors row-bg-1 doctors-home-page">
		<div class="tag">VIRTUALLY ENABLED CLINICS &amp; HOSPITALS<br>TRANSFORM DOCTOR-PATIENT INTERACTIONS</div>
		<img src="img/home/doctors/Doctors_Graphic_Doctor-Benefits.png">
	</div>


	<div class="grey-row">
		<div class="content-area">
			Differentiate your Centre with our Virtual Services.<br>We make it easy for you to control disease management for your patients.
		</div>
	</div>

	
	<div class="widgets-row row-bg-2">
		<div class="content-area">

			<a href="home-doctors.php" class="doctors-button">Doctors</a>
			<a href="home-doctors-clinic.php" class="doctors-button active">clinics</a>


			<div class="header">GET THE BEST FROM TECHNOLOGY<br>WITH ZERO EFFORT</div>
			<div class="clearfix"></div>

			<div class="widget-images col-xl-4 col-md-4 col-sm-12 col-xs-12">
				<img src="img/home/doctors/Doctors_Icon_Join.png" class="widget-image">
				<img src="img/home/doctors/Doctors_Icon_Line.png" class="lines">
				<div class="widget-name">Join</div>
				<div class="widget-text">Join our community of<br>world class doctors.</div>
			</div>
			<div class="widget-images col-xl-4 col-md-4 col-sm-12 col-xs-12">
				<img src="img/home/doctors/Doctors_Icon_Upgrade.png" class="widget-image">
				<img src="img/home/doctors/Doctors_Icon_Line.png" class="lines">
				<div class="widget-name">Upgrade</div>
				<div class="widget-text">Ensure your practice<br>moves ahead with the times</div>

			</div>
			<div class="widget-images col-xl-4 col-md-4 col-sm-12 col-xs-12">
				<img src="img/home/doctors/Doctors_Icon_Reach.png" class="widget-image">
				<div class="widget-name">Reach</div>
				<div class="widget-text">Deliver quality care when<br>your patients need it most.</div>
			</div>
		</div>
	</div>

	<div class="form-block col-md-12 joinus row-bg-3">
		<div class="content-area form-contents ">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Apply to join our Virtual Klinic&trade; Network.</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form">
						<div class="form-group">
							<label for="fname" class="col-xs-5 control-label">First Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="fname" >
							</div>
						</div>	
						<div class="form-group">
							<label for="lname" class="col-xs-5 control-label">Last Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="lname" >
							</div>
						</div>	
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" >
							</div>
						</div>
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Phone No:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" >
							</div>
						</div>
						<div class="form-group">
							<label for="degree" class="col-xs-5 control-label">Degree:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="degree" >
							</div>
						</div>
						<div class="form-group">
							<label for="speciality" class="col-xs-5 control-label">Primary Speciality:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="speciality" >
							</div>
						</div>
						<div class="form-group">
							<label for="why" class="col-xs-5 control-label">Why are you interested in teleheath?*</label>
							<div class="col-xs-7">								
								<textarea class="form-control" id="why"></textarea>
							</div>
						</div>									
						<div class="form-group">
							<label for="report" class="col-xs-5 control-label">Upload Resume:*</label>
							<div class="col-xs-7">
								<div class="fileUpload pw-btn">
								    <span>Choose File</span>
								    <input id="uploadBtn" type="file" class="upload upload-button" />
								</div>
								<div id="uploadFile" class="upload-file-path"></div>
								<div class="clearfix"></div>																					
							</div>							
						</div>	
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" value="Register">
							</div>
						</div>										
					</form>
				</div>				
			</div>			
			<div class="message form-right col-sm-4 hidden-xs">
				<br>
				Our Virtual Services will streamline <br>
				processes and help our Next <br>
				Generation Health Partners provide <br>
				efficient, quality care.<br><br>
				Contact us to learn more about how <br>
				you can benefit from the pinkWhale <br>
				platform to reach more patients.<br>
			</div>
		</div>
	</div>
	
	<?php include "includes/home-footer.php" ?>
	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
