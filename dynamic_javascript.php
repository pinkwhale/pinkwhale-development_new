<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
<title>jCarousel Examples</title>
<link href="style.css" rel="stylesheet" type="text/css" />

<!--
  jQuery library
-->
<script type="text/javascript" src="lib/jquery-1.4.2.min.js"></script>

<!--
  jCarousel library
-->
<script type="text/javascript" src="lib/jquery.jcarousel.min.js"></script>

<!--
  jCarousel skin stylesheet
-->
<link rel="stylesheet" type="text/css" href="skins/ie7/skin.css" />

<script type="text/javascript">

var mycarousel_itemList = [
						     <?php
	include "db_connect.php";
	$qry="SELECT * FROM `specialist_doctors` order by sp_doctors_id ASC";
	if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
	while($qry_result=mysql_fetch_array($qry_rslt))
	{
?>
		{url: "<?php echo $qry_result['sp_doctors_photo']; ?>", title: "Flower1"},
				 
<?php 	}?>

];

function mycarousel_itemLoadCallback(carousel, state)
{
    for (var i = carousel.first; i <= carousel.last; i++) {
        if (carousel.has(i)) {
            continue;
        }

        if (i > mycarousel_itemList.length) {
            break;
        }

        carousel.add(i, mycarousel_getItemHTML(mycarousel_itemList[i-1]));
    }
};

/**
 * Item html creation helper.
 */
function mycarousel_getItemHTML(item)
{
    return '<img src="' + item.url + '" width="75" height="75" alt="' + item.url + '" />';
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        size: mycarousel_itemList.length,
        itemLoadCallback: {onBeforeAnimation: mycarousel_itemLoadCallback}
    });
});

</script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<div id="wrap">

  <ul id="mycarousel" class="jcarousel-skin-ie7">
    <!-- The content will be dynamically loaded in here -->
  </ul>

</div>
</body>
</html>
