<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<link href="css/joinus.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="pw_Corporates/js/corporate_join_validation.js"></script>

	
	<div class="menu-data" data-main-page="corporates" data-sub-page="join-us"></div>
	<?php include "menu-corporates.php" ?>
		
	<div class="banner-bg banner-bg-joinus-corp">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Shake hands with us to help your <br>employees lead a healthy working life.</i></div>
		</div>
	</div>
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			DIFFERENTIATE YOURSELF WITH <span class="hidden-xs"><br></span> OUR VIRTUAL HEALTHCARE SERVICES
		</div>
	</div>
	
	<div align="center"><span style="color:#3C6;"><?php if (isset($_SESSION['addcorp']))
                                                    echo $_SESSION['addcorp']; $_SESSION['addcorp']=""; ?></span></div>

	<div class="form-block col-md-12 row-bg-2">
		<div class=" content-area form-contents ">
			<div class="form-header hidden-xs">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Apply to join our Virtual Klinic&trade; Network.</span>
			</div>
			<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form" action="pw_Corporates/actions/corporate_join_action.php" method="POST" name="corporate_join"
					enctype="multipart/form-data" id="corporate_join">
						<div class="form-group">
							<label for="organisation" class="col-xs-5 control-label">Organisation</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="organisation" name="organisation" >
							</div>
						</div>	
						<div class="form-group">
							<label for="cname" class="col-xs-5 control-label">Contact Name</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="cname" name="cname" >
							</div>
						</div>	
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>
						 <!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="email_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Phone No:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" name="mobile"  onkeypress="return isNumberKey(event);">
							</div>
						</div>
						 <!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="phone_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="business_type" class="col-xs-5 control-label">Type of Business:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="business_type" name="business_type" >
							</div>
						</div>
						<div class="form-group">
							<label for="speciality" class="col-xs-5 control-label">Primary Speciality:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="speciality" name="speciality" >
							</div>
						</div>
						 <!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="spec_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group">
							<label for="requirement" class="col-xs-5 control-label">Briefly describe your Requirement:*</label>
							<div class="col-xs-7">					
								<textarea class="form-control" id="requirement" name="requirement"></textarea>
							</div>
						</div>	
							 <!--    ERROR DIV -->						
						<div  align="right" style="margin-right:100px;">
						<div id="req_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" value="Register" onclick="return add_corporate(corporate_join)"/>
							</div>
						</div>										
					</form>
				</div>				
			</div>			
			<div class="message form-right col-sm-4 hidden-xs">
				Our wide array of health and <br>
				wellness programs will help keep <br>
				your employees healthy and <br>
				happy. And, our virtual services <br>
				will streamline processes and <br>
				help your organization provide <br>
				quality care. Contact us to learn <br>
				more about how pinkWhale can <br>
				benefit your organization.<br>
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
