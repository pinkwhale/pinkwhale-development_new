<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Call a Counsellor: Positive Change through Professional Counselling | pinkWhale Healthcare</title>
<meta name="keywords" content="counseling, counselling, stress counselling, depression counselling, anxiety counselling, marriage councelling, couples counseling, adolescent counsellors, life skills coaching, psychologists, clinical psychologist, child psychologist"/>
<meta name="description" content="Professional help from experienced counsellors, psychologists has never been easier. pinkWhale brings you a panel of highly experienced counselors, psychologists, and life coaches who can help you on several life-issues and make a positive change"/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<SCRIPT src="images/mouseoverscripts.js" language="JavaScript" type="text/javascript"></SCRIPT>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<!-------------------------- header..------------------------------>
<?php
	include 'header.php'; 
?>
<!------------------------- header.----------------------------------------->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td colspan="2"><img src="images/coun_title.jpg" width="978"/></td></tr>
<tr><td width="714" style="border-right:1px solid #4d4d4d" valign="top">
<table width="705" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="9" class="s90counsellingcontent"><h2>Positive Change through Professional Counselling</h2>
<p>Most of us go through challenges at different points in our life. There are times when all of us may feel worried or unhappy. 
Our problems could be due to relationship issues, difficulty in coping with studies & exams, career concerns, work-life imbalance, 
parenting pressure, loneliness, or other reasons. We become stressed, anxious, and struggle to create positive change.
</p>
<p><img src="images/counselling_chart.jpg" width="695" height="410"/>
</p>
<p>
During such difficult or painful times when things are not going well, we may not know what to do.  Handling such situations require life-management skills.
Often we can turn to family and friends, but there are times when we do not want to burden them or do not feel comfortable to share with them. Sometimes they may not be the best ones to help.
</p>
<p> 
This is where counselors, life coaches, and professional counseling can help.
A counsellor can offer you the time and space, and is qualified and experienced in responding to emotional issues and difficulties without 
getting over involved and provide a third-person perspective. They listen genuinely to your concerns, and to what is troubling you, 
and are non-judgemental, and non-directive. They help you to expand your options and choices, foster inner healing, and enhance the way you can relate to others.
</p>
<p>Getting professional help from experienced counsellors, psychologists has never been easier. pinkWhale brings you a panel of highly 
experienced counselors, psychologists, and life coaches who can help you on several life-issues and make a positive change. In addition, 
our tele-counseling facility enables you to discreetly consult with experienced counselors, no matter where you are located. <b>This service is completely confidential.</b></p>

</td></tr>

<tr><td>
<table width="705" border="0" cellspacing="0" cellpadding="0" class="s90coundocs" align="center">

<p><font size=5 color="deeppink" type=arial><strong><a href="join.php">Join Now!</a> Achieve the positive changes you desire!</strong></font></p>

<p>
<h4> <font color="deeppink"> Call +91 - 080-43000333: </font> If you want to consult face-to-face with a particular specialist psychologist or counsellor in our network. 
</h4></p>
<p>
<h4> <font color="deeppink"> Call +91 - 080-43000300:  </font> If you are struggling with issues that can�t bring yourself to talk about face-to-face with someone, pick up the telephone, and connect anonymously to a 
counsellor. This facility is open from Mon-Fri: 11 am - 8 pm India Standard Time.  And you can call from a place that is comfortable for you.
</h4>
</p>

</p>
</table></td></tr></table>

<td width="254" valign="top" align="right"><!--<img src="images/counsellor_4steps.jpg" width="254" style="margin-bottom:10px;" />-->
<img src="images/counselling_banner.jpg" width="254" style="margin-bottom:20px;"/>
<a href="specialist.php?specialist=Psychiatry&id=0"><img src="images/meet_psychiatrist.jpg" width="254" height="36" style="margin-bottom:10px" /></a>
<a href="specialist.php?specialist=Psychology and Counselling&id=0"><img src="images/meet_psychologists.jpg" width="254" height="36" style="margin-bottom:20px" /></a>
<h4>Customer Testimonials</h4> <img src="images/new_testimony2.jpg" width="254" height="160"/></td></tr>

<tr><td colspan="2"><div id="s90specialtfaqs">
<h3>FAQ's:</h3>

<ol>
<li><strong>When and why should I talk to a counsellors?<br />
Ans.</strong> During difficult or painful times when things are not going well, a counsellor can offer you the time and space, provide a third-person perspective and help you deal with the situation.</li>

<li><strong>Why should I use pinkWhale's counselling service?<br />
Ans.</strong> pinkWhale brings you an experienced counselling team consisting of certified counsellors, psychologists, and psychiatrists, who can resolve a wide variety of problems. They will listen to you, provide non-judgemental support, and help you to overcome struggles in life.  Our counsellors are committed to providing you with high quality care that is centered on your needs, provide convenient access & comfort, and to give you the peace of mind.</li>

<li><strong>How can talking with a counsellors help my problems?<br />
Ans.</strong> Often when we experience problems that get us down we can start to feel worse as the thoughts and feelings persist and we feel more and more stuck.  This can be unhelpful especially if we don't know what to do about it.</li>

<li><strong>How will counselling help me?<br />
Ans.</strong> Counselling can help us get a new perspective on our difficulties and feelings, to unburden and demystify our issues.  It can help to hear ourselves describe the problems, and in talking about it we can gain confidence to look at and address our problems.</li>

<li><strong>What sort of issues does counselling deal with? <br />
Ans.</strong> Counselling can help with a wide variety of problems whether they seem relatively straightforward or more complex and long standing.  It can help with relatively minor problems that are frustrating you or more complicated issues that are affecting your well-being and self-esteem.  Counselling helps you to deal with problems and challenges such as anger, relationships, self-esteem, lack of motivation, bereavement, depression, anxiety, sexuality change, compulsive behaviour, phobias and traumas (this is not a comprehensive list and only to give you an idea about the kind of problems that can be handled).</li>

<li><strong>I don't know what to say or how I should be saying it? <br />
Ans.</strong> This is a common feeling, especially initially. Not knowing what to say is often about nervousness and the fear of thinking you are going to sound stupid or it won't make sense. There is no right or wrong way.  How you present your problem doesn't matter, you can say whatever you like. Sometimes there is silence, sometimes you might find yourself saying things you had not expected to say or talking about things you didn't expect to. The counsellor will help you explore the matter and keep checking that he/she understands what you are describing.</li>

<li><strong>I'm worried that I'll be judged? Will the counsellor think badly of me?<br />
Ans.</strong> No, you will not be judged, our counsellors are non-judgemental.  Counsellors understand that we all go through bad times.  We can often feel ashamed by the things we do, the things we say and how we feel about others and ourselves. Our counsellors are supportive and help to address your problems openly.</li>

<li><strong>What shall I expect from my counsellor? <br />
Ans.</strong> Counsellors are trained to listen while you talk through your problems.  They will try to help you to find answers and to gain insight into the complexities and dilemmas that life presents.  Moreover, they will try to help you to see the overall picture. It is about the counsellor facilitating you to build up your own self-reliance and self-esteem, rather than you being advised, instructed or directed. You may worry about telling your problems to your family or friends, but you do not need to be concerned about overburdening the counsellor. In fact, the more open you are , the more you are likely to gain from counselling.  You can expect a professional service from the counsellor, who will be respectful of the problems you are bringing.</li>

<li><strong>What if I feel I need to see a counsellor urgently?<br />
Ans.</strong> You can make an appointment with our counsellors who are available for face to face consultation.</li>

<li><strong>What will happen when I first talk to a counsellor?<br />
Ans.</strong> The first session with a counsellor is an initial session so that you can talk about the difficulties you are having, and what changes you would like to make. The counsellor will listen and help you define the problems.  You might already have an idea of what you would like from counselling. You and the counsellor will look at the best way forward to help you tackle your problems.</li>

<li><strong>How many sessions will I need & how long do sessions last?<br />
Ans.</strong> The number of sessions can vary from person to person. We provide short term counselling, which ranges from 1 to 6 sessions.  Sessions usually last 30-45 minutes, and scheduled periodically.</li>

<li><strong>Can I get a confidential service?<br />
Ans.</strong> Yes, our service is completely confidential.</li>

</ol>
</div>
</td></tr></table>


<?php
	include 'footer.php';
 ?>



<div id=couns1 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">WINNIE</strong> -  Air Cmde (Retd) JWR Chinniah </h2>
<p>Winnie has 30 plus yrs of counselling experience. He has worked as a counsellor at various Air Force locations for about 20yrs in his service. He has conducted many workshops in the Air Force, Schools, Colleges, Churches and Corporates. He has also counselled for de-addiction in a psychiatric hospital in Muscat, Oman.</p>
<p>His areas of expertise include Transactional Analysis and Life Skills Training.</p>

<p><strong>Case study: </strong> Anita (name changed) shared that she was upset because her parents did not understand her at all. She was in tears as she spoke about her issues and her inability to make them understand her. Everything she did was wrong to them and she felt highly misunderstood. She narrated the story to me.  After listening to her and helping her understand her parents; with her permission, I brought her parents into the loop. A couple of sessions with the parents proved very useful as the parents got a unbiased and objective perspective on their and their daughters behaviour. Their issue got resolved and they are now getting along very well as their understanding about each other has increased and the communication is much better.</p></div></div></center></div>

<div id=couns2 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">Shweta Bhat</strong></h2>
<p>Shweta Bhat is a trained Child Psychologist with 5 plus yrs of experience.</p>
<p>She has worked as a counsellor/ psychologist at Bangalore Medical College and Turning Point (under guidance of Dr Sulata Shenoy). She is trained in Tobacco Control at NIMHANS.</p>
<p>She is a qualified psychologist with a MSc in Clinical Psychology.</p>
<p><strong>Case study: </strong>Rajesh(Name Changed), aged 13 years was a very intelligent boy and also a quick learner. His parents approached me and explained that their son had was being snobbish and not expressing or sharing his thoughts and feelings to anybody. After about two to three sessions with him I discovered that he was actually a very warm and a caring person. After spending appropriate time with him he learnt to express and share his feelings and said that he would communicate and reciprocate appropriately with all. This was later confirmed by his parents to be true.
</p></div></div></center></div>

<div id=couns3 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">Hema Indraneel</strong></h2>
<p>Hema has 20 plus yrs of HR work experience. She has worked in corporates like C-Dot and Oracle. She has been the proprieter at Positive Results, her HR consultation company.</p>
<p>Her areas of expertise include employee and corporate counselling.</p>
<p>Hema is a post graduate in Management and Industrial Administration . She is also qualified in Personnel Management and Counselling.</p>

<p><strong>Case study:</strong> Sapna (name changed) had a boy-friend staying abroad and their e-relationship had lasted a few years, she was very upset and anxious that though he had promised to marry her, he kept giving excuses when she spoke about marriage.  She was under pressure from her family to get married and was in an emotional turmoil.   Speaking about her relationship and dilemma helped her clear her minds and she was able to deal with the situation and come to a decision which settled her internal conflict.  She is now happily married and at peace with herself.
</p></div></div></center></div>

<div id=couns4 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">Radha G.</strong></h2>
<p>Radha has 5 plus yrs of counselling experience. Her areas of expertise include student counselling. She has worked in schools and colleges. (Christ College).<br />
Radha is a qualified lawyer. She has a Diploma in Counselling and MS in Psychotherapy.</p>
<p><strong>Testimonials:</strong></p>
<ul type="disc">
  <li>"Thanks for being there for me and accepting me unconditionally"</li>
  <li>"Thank you, I am at more peace with myself now"</li></ul>

<p><strong>Case study:</strong> Mr. Ramesh (name changed) had martial issues, and it was affecting his work and personal life. He had never shared his issues with anyone before. Listening to what he had to say at his own pace and assuring him about his confidentiality, helped him to share his issues comfortably, after sharing he was relieved and felt much better. He was also able to think more clearly and was also able to identify the areas which he needed to work on so as to improve the situation.</p></div></div></center></div>

<div id=couns5 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">Samanvitha Shetty</strong></h2>
<p>Samanvitha has 3 yrs counselling  experience. She is a qualified engineer and holds a diploma in counselling. Her  areas of expertise include emotional counselling.</p>
<p><strong>Testimonials:</strong></p>
<ul type="disc">
  <li>I am able to analyze my situation in a better way &ndash; Shruthi</li>
  <li>I feel relieved after sharing my thoughts and feelings with you &ndash; Suma</li>
</ul>
<p><strong>Case study:</strong> Shwetha (name changed) had to  decide on going ahead with her marriage proposal but had doing so. She was  unable to put forward her views about the situation to her family. After  speaking about her anxieties she was able to think clearly about what she  wanted and why. She was also able to express herself to her family much clearly  which made things easier for her and her family to decide. </p>
</div></div></center></div>

<div id=couns6 class="s90mouseovercoun"><center><div class="s90mouseovercoun2"><div class="s90mouseovercoun3">
<h2><strong style="color:#b60359">Suneetha Ramesh</strong></h2>
<p>Suneetha has 15 plus yrs of HR experience. She has worked corporates like Infosys, Cross Domain Solutions, Compass and Aditya Birla Minacs.</p>
<p>She is a qualified commerce graduate and holds a MS and Diploma in counselling. Her areas of expertise include communication and interpersonal skills. She is a certified QMS Internal Auditor and Lean Champion.</p></div></div></center></div>

</body></html>