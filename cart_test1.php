<?php
error_reporting(E_PARSE); 
// Start the session
session_start();

// Include MySQL class
require_once('inc/mysql.class.php');
// Include database connection
require_once('inc/global.inc.php');
// Include functions
require_once('inc/functions.inc.php');

// Process actions
include 'db_connect.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/topup_validation.js"></script>
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
include 'header.php'; 
	$chat = $_SESSION['chat'];
	$chat_type = $_SESSION['chat_type'];
	$charge = $_SESSION['charge'];
	$doc_id = $_SESSION['doc_id'];
	$session_type = $_SESSION['session_type'];
        
//       echo $session_type."<br />";
//       echo $doc_id."<br />";
?>

<?php
$chatitems = explode(',',$chat);
$i=0;
foreach ($chatitems as $chatitem)
	{
		$session[$i]= $chatitem;	
		//echo $session[$i];
			 		$i++;
		}
?>

<?php
$chat_type_items = explode(',',$chat_type);
$j=0;
foreach ($chat_type_items as $chat_type_item)
	{
		$package[$j]= $chat_type_item;	
		//echo $package[$j];
			 		$j++;
		}
?>

<?php
$charge_items = explode(',',$charge);
$k=0;
$item_charge = array();
foreach ($charge_items as $charge_item)
	{
		$item_charge[$k]= $charge_item;	
		//echo $item_charge[$k];
			 	$k++;
		}
?>  

<div id="changed_value" align="center">
	<table align="center" border="0">
		<tr>
			<td style="font-family: Arial,Verdana;">
				<h4>My Shopping Cart</h4>
			</td>
		</tr>
		<tr>
			<?php echo writeShoppingCart(); ?>
		</tr>
		<tr>
			<td colspan="2" style="font-family: Arial,Verdana;">
				<?php echo showCart(); ?>
			</td>
            
		</tr>
	</table>
</div>

 <script type="text/javascript">
 function usePromoCode(str)
{
if (str=="")
  {
  //document.getElementById("changed_value").innerHTML="";
  //alert("Test");
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("changed_value").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","promo_discount_check.php?q="+str,true);
xmlhttp.send();
}
 </script>   
    
    
<?php include 'footer.php'; ?>
</body>
</html>
