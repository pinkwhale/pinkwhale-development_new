/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/
$().ready(function () {
    //Setup FLV files player example
    var playerFlv = new SWFObject("player.swf", "myplayer2", "295", "235", "9");
    playerFlv.addVariable("screencolor", "#3f3f3f");
    playerFlv.addVariable("image", "images/home-intro.gif");
    playerFlv.addVariable("file", "flv/About.flv");
    playerFlv.addParam("allowscriptaccess", "always");
    playerFlv.addParam('allowfullscreen', "true");
    playerFlv.write("player2");
    //create a javascript object to allow us send events to the flash player
    var player2 = document.getElementById("myplayer2");
    var mute2 = 0;

    //EVENTS for FLV files player
//    $("#play2").click(function () {
//        player2.sendEvent("PLAY", "true");
//    });
//    $("#pause2").click(function () {
//        player2.sendEvent("PLAY", "false");
//    });
//    $("#mute2").click(function () {
//        if (mute2 == 0) {
//            player2.sendEvent("mute", "true");
//            mute2 = 1;
//        }
//        else {
//            player2.sendEvent("mute", "false");
//            mute2 = 0;
//        }
//    });

});
function playVideo(video) {
    //Setup FLV files player example
    var playerFlv = new SWFObject("player.swf", "myplayer2", "295", "235", "9");
    playerFlv.addVariable("screencolor", "#3f3f3f");
    playerFlv.addVariable("autostart", "true");
    playerFlv.addVariable("image", "images/home-intro.gif");
    playerFlv.addParam("allowfullscreen", "true");
    playerFlv.addVariable("file", video);
    playerFlv.addParam("allowscriptaccess", "always");
    playerFlv.write("player2");
}
