<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
    <script>
    function initialize() {
            var secheltLoc = new google.maps.LatLng(13.016806599999999, 77.64350100000001);
            var myMapOptions = {
                     center: secheltLoc
                    ,mapTypeId: google.maps.MapTypeId.ROADMAP
                    ,zoom: 18 , scrollwheel: false,mapTypeControl: false, zoomControl: false
            };
            var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);
            var image = new google.maps.MarkerImage(
                    'images/pinMap.png',
                    new google.maps.Size(17,26),
                    new google.maps.Point(0,0),
                    new google.maps.Point(8,26)
            );
            var shadow = new google.maps.MarkerImage(
                    'images/pinMap-shadow.png',
                    new google.maps.Size(33,26),
                    new google.maps.Point(0,0),
                    new google.maps.Point(9,26)
            );
            var marker = new google.maps.Marker({
                    map: theMap,
                    icon: image,
                    shadow: shadow,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    position: secheltLoc,
                    visible: true
            });

            var boxText = document.createElement("div");
            boxText.innerHTML = '<div class="captionMap animated bounceInDown"><img src="images/place.jpg" class="alignleft"  alt=""> <span>PinkWhale Health Care</span> #219, 2nd Floor, 7th A Main,<br> 1st Block HRBR Layout,<br> Kalyan Nagar, Bangalore - 560 043.</div>';

            var myOptions = {
                     content: boxText
                    ,disableAutoPan: false,maxWidth: 0
                    ,pixelOffset: new google.maps.Size(-140, 0)
                    ,zIndex: null
                    ,boxStyle: { 
                            width: "280px"
                     }
                    ,closeBoxURL: ""
                    ,infoBoxClearance: new google.maps.Size(1, 1)
                    ,isHidden: false
                    ,pane: "floatPane"
                    ,enableEventPropagation: false
            };

            google.maps.event.addListener(theMap, "click", function (e) {
                    ib.open(theMap, this);
            });

            var ib = new InfoBox(myOptions);
            ib.open(theMap, marker);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
  </script>
  <script type="text/javascript">
  $(document).ready(function() {				
            jQuery("#contactform").validationEngine({
                ajaxFormValidation: true,
                onBeforeAjaxFormValidation: Contact_form
            });
  });
  </script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>

<?php
	include 'main_header.php';
?>
<!-- header.......-->
        

		<!-- map_canvas-->
		<div id="map_canvas">
           <!-- css3 preLoading-->
           <div class="mapPerloading">
                   <span>Loading</span>
                  <span class="l-1"></span>
                  <span class="l-2"></span>
                  <span class="l-3"></span>
                  <span class="l-4"></span>
                  <span class="l-5"></span>
                  <span class="l-6"></span>
            </div>
        </div><!--/map_canvas-->
        
    
<div class="container">

    <section>   
    
      <div class="row" >
          
            <div class="span4 " >
            <div class="inner">
						<h2><span>Contact</span> Info</h2>
                        <div class="heading_border"></div>
                        <address>
						<ul class="listcontact">
								<li class="cont-phone">+91 80 43000333</li>
								<li class="cont-email"><a href="mailto:info@pinkwhalehealthcare.com">info@pinkwhalehealthcare.com </a></li>
								<li class="cont-adress"><strong>Mail us at:</strong><br>
									#219, 2nd Floor, 7th A Main,<br />
									1st Block HRBR Layout,<br /> Kalyan Nagar, Bangalore - 560 043<br></li>
						</ul>
						</address>
                        
                        <h2><span>Additional</span> contact </h2>
						<address class="clearfix">
						<ul class="listcontact">
								<li class="cont-email"><a href="mailto:corpenquiry@pinkwhalehealthcare.com">corpenquiry@pinkwhalehealthcare.com</a> (if you are a corporate or a gated community who needs our services)</li>
								<li class="cont-email"><a href="mailto:feedback@pinkwhalehealthcare.com">feedback@pinkwhalehealthcare.com</a> (if you wish to give us any feedback on our services)</li>
						</ul>
						</address>
            </div><!-- /inner-->
            </div><!-- /span4-->
			
			<div class="span8" >
			<div class="inner  unleft">
            

                
                <!-- Contact Us text-->
                <h2><span>Let's keep</span> in touch</h2>
                
                <div class="heading_border"></div>
                
				<!--Success notifications-info text-->
				<div class="notifications clearfix" style="display:none">
                    <div class="alert alert-info">
                      <a class="close" data-dismiss="alert" href="#">×</a>
                      <strong>Success</strong> Send Messages Complete ! 
                    </div>
                </div>
                 
				<form class="form-horizontal" id="contactform">
					<div class="control-group">
					<label class="control-label" >Name <small>Your Name</small></label>
						<div class="controls">
							<input  type="text" name="youname"  id="youname" class="validate[required] span3" />
						</div>
					</div>
					<div class="control-group">
					<label class="control-label" >Email  <small>Your Email</small></label>
						<div class="controls">
							<input  type="text"  name="email" id="email" class="validate[required,custom[email]] span3"/>
						</div>
					</div>
				  <div class="control-group">
					<label class="control-label" for="textarea">Messages<small>Your Messages</small></label>
					<div class="controls">
					 		<textarea  name="Messages" class="validate[required] input-xlarge " id="textarea" rows="3"></textarea>
					</div>
				  </div>
				  <div class="control-group last">
					<div class="controls">
							<button type="submit" class="btn">SEND MEASSAGES</button>
					</div>
				  </div>
				</form>

			</div><!-- /inner -->
			</div><!-- /span8 -->			
      </div><!-- /row-->
	  
		
	</section>
    </div> <!-- /container -->    

<?php
include 'footer.php'; ?>
</body></html>
