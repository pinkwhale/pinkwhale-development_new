<?php 
        error_reporting(E_PARSE); 
      session_start();
        include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
        {
                header("Location: index.php");
                exit();
        }
        else
        {
                $doctor_id=$_SESSION['doctor_id'];
                

        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cancel_appointment.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<link href="calendar_1/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar_1/calendar.js"></script>

<script type="text/javascript">
   
   function get_time_slots(){
     
        var clinic_id = encodeURI(document.getElementById('clinic').value);
        
        var ddate=encodeURI(document.getElementById('date2').value);                
        
        if(clinic_id!="" && ddate!="0000-00-00"){
            
            $('#clinic_timeslots').load('get-clinic-appoint-time-slots.php?clinic_id='+clinic_id+'&date='+ddate);

            document.getElementById("clinic_timeslots").style.display="block";

        }
        
    }
    
    
$(function() {
		$( "#dialog" ).dialog();
	});
    
</script>
<!--------------------------Jquery start------------------------------------->
<link rel="stylesheet" href="ui/jquery.ui.all.css">
<script src="ui/jquery-1.7.2.js"></script>	
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.mouse.js"></script>
<script src="ui/jquery.ui.draggable.js"></script>
<script src="ui/jquery.ui.position.js"></script>
<script src="ui/jquery.ui.resizable.js"></script>
<script src="ui/jquery.ui.dialog.js"></script>
<!---------------------------Jquery End------------------------------------->
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

</div>
<?php include 'header.php'; ?>
<?php

require_once('calendar_1/tc_calendar.php');

?>
<?php 

$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag FROM `pw_doctors` where `doc_id`='$doctor_id' ";

$qry_rslt = mysql_query($qry);

while($result = mysql_fetch_array($qry_rslt))

	{

		$doc_category = $result['doc_category'];

		$doc_name = $result['doc_name'];

		$doc_id = $result['doc_id'];

		$doc_ctgry_flag=$result['doc_category_change_flag'];

	}

?>

<div id="dialog-confrm-cancel" title="Cancel Doctor Appointments !" style="display:none;">
        <?php echo "Dear Admin"; ?>,<p> Are you sure ?</p>
</div>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="220" valign="top">

<div id="s90dashboardbg">

<!--    <img src="images/dots.gif" />

    <a href="doc_phr.php"><b>Dashboard</b></a></div>




<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>



<td width="748" valign="top" class="s90docphr">

<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">

<tr>

	<td width="220"><h1>Cancel My Schedule</h1></td>

	<td width="528" bgcolor="#f1f1f1" align="right">    	

    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">

        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>

    </div>

    </td>

</tr>

</table>

<table height="30"></table>
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
            
                <tr><td width="528"  align="center">  
            <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            
    ?>
    <form action="actions/cancel_doctor_appointment.php" method="post" name="doctor_appointment_cancel" id="doctor_appointment_cancel">
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Cancel My Schedule</th></tr>                
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Clinic</b><font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="clinic" id="clinic" onchange="get_time_slots()">
                    <option value="" selected="selected" disabled>-----select Clinic-----</option>
                    <?php
                        $qry = "select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id where doctor_id='$doctor_id' group by name union select c.clinic_id,name from clinic_details c inner join clinic_doctors_details d on c.clinic_id=d.clinic_id where doctor_id='$doctor_id' group by name";
                        $res = mysql_query($qry);
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['clinic_id']."|".$data['name']."'>".$data['name']."</option>";                            
                        }                        
                    ?>
                </select>
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="clinicErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Date</b><font color="#FF0000">*</font>:</td>
            <td width="124" valign="top" bgcolor="#F5F5F5"><?php
                      $date2=date("Y-m-d", time()+86400);
                      $date3=date("Y-m-d", time()+86400);
                      $myCalendar = new tc_calendar("date2");
                      $myCalendar->setIcon("calendar_1/images/iconCalendar.gif");
                      //$myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
                      $myCalendar->setPath("calendar_1/");
                      $myCalendar->setYearInterval(1910, 2015);
                      $myCalendar->dateAllow($date3, '2015-03-01');
                      $myCalendar->setDateFormat('j F Y');
                      $myCalendar->setOnChange("get_time_slots()");
                      $myCalendar->writeScript();
	  ?></td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="calErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr  >
            <td colspan="2" align="center" bgcolor="#F5F5F5" >
                <div id="clinic_timeslots" style="display:none;">
                    
                </div>                
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td colspan="2" align="center" height="8">
	            <div id="clinic_timeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Cancel Appointment" tabindex="6" type="button"  onclick="cancel_appointment(doctor_appointment_cancel)"  /></td>        
        </tr>
        
    </table>
    </form>
    <?php
        }
    ?>
            
    </td></tr>
</table>

    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>

</body></html>

