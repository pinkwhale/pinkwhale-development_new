<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/doc_phr_mail_validation.js" type="text/javascript"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><!--<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->
</td>

<td width="748" valign="top" class="s90phrcontent">

<?php
	$cnsltion_id=$_GET['expd'];
	$qry1= "SELECT * FROM `pw_doctors` WHERE `doc_id`='$doctor_id'";
	$qry_rslt1 = mysql_query($qry1);
	while($result1 = mysql_fetch_array($qry_rslt1))	
	{
		$doc_photo=$result1['doc_photo'] ;
		$con_doc_name=$result1['doc_name'] ;
	}

?>
<table height="30" width="700" class='tbl' border="0" cellspacing="0" cellpadding="0" >
						<tr>
							
							<td width="370"><h1><?php echo ucfirst($con_doc_name) ; ?></h1></td>
							<td width="370" align='right' bgcolor="#f1f1f1"><div style="color:#C36262; margin-right:20px; font-family:Arial; font-size:16px; font-weight:bold;">Pinkwhale ID: <?php echo $doctor_id ; ?></div></td>
						</tr>
					</table>
					<br/>

	<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls">
	<!--td width="99"  valign="top">
                        <img src="<?php echo $doc_photo ; ?>" width="72"  height="72" style="border-radius:100%" />
                    </td-->
	<tr><th>Completed Follow-up Summary</th></tr>
	<tr><td >
	<table width="570" border="0" cellspacing="5" cellpadding="0">
	<?php 
	$cad_no=$_GET['cad_id'];
	$qry3= "SELECT user_photo FROM `user_details` WHERE `user_id`='$cad_no'";
	$qry_rslt3 = mysql_query($qry3);
	while($result3= mysql_fetch_array($qry_rslt3))
	
		{
			$user_photo=$result3['user_photo'];
		}
	
	if($user_photo=="") {
?>
<tr><td width="80" align="center" valign="top"><img src="images/avatar.png" width="72"  height="72" style="margin-top:16px"/><br /><br />
<?php }
		else {
		?>
   <tr><td width="80" align="center" valign="top"><img src="<?php echo $user_photo;?>" width="72"  height="72" style="margin-top:16px"/><br /><br />     
        
	<?php		
		}
?>
    
    
    
    
<?php
	$qry0= "SELECT `doc_photo`, `doc_name`, `doc_email_id` FROM `pw_doctors` WHERE `doc_id`='$doctor_id'";
	$qry_rslt0 = mysql_query($qry0);
	while($result0 = mysql_fetch_array($qry_rslt0))	
	{
		$doctor_photo=$result0['doc_photo'];
		$con_doc_name = $result0['doc_name'];
		$cslt_doctor_email = $result0['doc_email_id'];
	}
	$cnsltion_id=$_GET['expd'];
	$qry= "SELECT * FROM `pw_consultation_emails` WHERE `consultation_no`='$cnsltion_id' group by consultation_no";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))	
	{
		$pw_card_id=$result['pw_card_id'];
	
?>
<td width="485"><?php echo $result['patient_name']; ?> <br />
</td></tr></table>
</td></tr>
<?php } ?>
<?php
	$qry2= "SELECT * FROM `pw_consultation_emails` WHERE `consultation_no`='$cnsltion_id' order by `mail_serial_no` ASC";
	$qry_rslt2 = mysql_query($qry2);
	while($result2 = mysql_fetch_array($qry_rslt2))	
	{
		
		$gdate1=strtotime($result2['entered_date']);
		if($result2['complaint'] <> "") $con_complaint = $result2['complaint'];
		$final_date1=date("d M Y", $gdate1);
		
		if ($result2['doctor_replay'] =='')
		{
		
?>
<tr>
  <td>
    <table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
    <?php 
	$cad_no=$_GET['cad_id'];
	$qry3= "SELECT `user_photo`, `user_name`, `user_email` FROM `user_details` WHERE `user_id`='$cad_no'";
	$qry_rslt3 = mysql_query($qry3);
	while($result3= mysql_fetch_array($qry_rslt3))
	
		{
			$user_photo=$result3['user_photo'];
			$user_name	= $result3['user_name'];
			$user_email	= $result3['user_email'];
		}
	
	
?>
    
	
	
	
	
	<?php
	if( $result2['upload_record']!='')
	{
	?>
    	<a href="<?php echo $result2['upload_record']; ?>">
        <img src="images/attach.png" width="16" height="16" align="absmiddle"/> Download</a>
    <?php } ?>
    </td>
    <td width="360" valign="top"><p><strong><?php echo $result2['patient_name']; ?> </strong> </p>
    
    </td>
    <td width="120" valign="top"><p align="right"><?php echo $final_date1; ?></p></td>
   </tr>
   <tr>
     <td colspan="2" valign="top"><p><?php echo $result2['cnsltion_mail_details']; ?> </p></td>
   </tr>
    </table>
</td></tr>
<?php
}
if($result2['doctor_replay']!='')
{
?>
<tr>
  <td>
    <table width="100%" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
    <tr><!--td width="80" rowspan="2" align="center" valign="top">
    <img src="<?php echo $doctor_photo; ?>" width="55" style="margin-top:16px" />
    </td-->
    <td width="360" valign="top"><p><strong><?php echo $result2['doctor_name']; ?></strong></p>
    
    </td>
    <td width="120" valign="top"><p align="right"><?php echo $final_date1; ?></p></td>
    </tr>
    <tr>
      <td colspan="2" valign="top"><p><?php echo $result2['doctor_replay']; ?></p></td>
    </tr>
    </table>
</td></tr>
<?php } } ?>

</table>

</td></tr></table>



<!-- footer -->
<script language="javascript" type="text/javascript">
enable_sec_opinion_submenu();
</script>
<?php
include 'footer.php'; ?>

</body></html>
