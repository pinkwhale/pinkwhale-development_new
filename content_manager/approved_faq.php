<?php
ob_start ();
error_reporting ( E_PARSE );
session_start ();
include ("../db_connect.php");
 if(!isset($_SESSION['content_manager_name']) || $_SESSION['login'] !='content_manager')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$email_id=$_SESSION['email_id'];
		$content_manager_id = $_SESSION['content_manager_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" media="screen, projection"	rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
<script type="text/javascript" src="js/faq_comments_validation.js"></script>
</head>
<body>	
	<?php include "../header.php"; ?>
	<!-- side Menu -->
	<table width="1000" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="228" valign="top">
				<?php include "content_writer_left_menu.php"; ?>
			</td>			
			<td width="772" id="mainBg" valign="top">				 
				<form action="actions/approve_faq_action.php" method="post" name="approved_faq"
					id="approved_faq" enctype="multipart/form-data">					
					<table border="0" cellpadding="0" cellspacing="1" width="500"
						align="center" class="s90registerform">						
					<?php
						$id=$_GET['faq_id'];
						$qry1= "SELECT * FROM `pw_faq` WHERE `faq_id`='$id'";
							$qry_rslt1 = mysql_query($qry1);
							while($result1 = mysql_fetch_array($qry_rslt1))
							{								
						?>							
						<tr>
							<th colspan="2">Approved FAQ</th>
						</tr>
							<input type="hidden" name="id" id="id" value="<?php echo $id?>"/>
						<tr>
							<td align="left" bgcolor="#F5F5F5">Question<font color="#FF0000">*</font>:
							</td>
							<td width="70%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3"	cols="35" name="question" id="question" maxlength="200" disabled><?php echo $result1['question']; ?></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="questionErrDiv" class="error"
									style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""	border="0"></td>
						</tr>

						<tr>
							<td width="40%" align="left" bgcolor="#F5F5F5">Answer<font	color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3"	cols="35" name="answer" id="answer" maxlength="200" disabled><?php echo $result1['answer']; ?></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="answerErrDiv" class="error" style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						
						<tr>
							<td width="40%" align="left" bgcolor="#F5F5F5">Comments<font color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3" cols="35" name="comments" id="comments" maxlength="200"></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="commentsErrDiv" class="error" style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->

						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt=""	border="0"></td>
						</tr>
						<tr>							
								 <td bgcolor="#F5F5F5">
								 <input value="Approve & Upload" name="approvenupload" type="submit" style="margin-left:120px"/></td>
								 <td bgcolor="#F5F5F5" >
								 <input value="Reject" type="submit" name="reject" style="margin-left:50px" onclick="return comments_val(approved_faq)"/></td>
								 
						</tr>
					</table>
				</form> 
				<?php }?>
				</td>				
		</tr>		
	</table>
	<?php include '../footer.php'; ?>
</body>
</html>
<?php
ob_end_flush ();
?>