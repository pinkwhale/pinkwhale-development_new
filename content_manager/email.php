<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../db_connect.php");
	 if(!isset($_SESSION['content_manager_name']) || $_SESSION['login'] !='content_manager')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$email_id=$_SESSION['email_id'];
		$content_manager_id = $_SESSION['content_manager_id'];
	}    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/content_email_validation.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include '../header.php';
include ('../db_connect.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!-- ///// left menu //////  -->
<?php include 'content_writer_left_menu.php';
?>
<!-- ///// left menu //////  -->
</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Email</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">        		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
                                    <form name="add_email" id="add_email" action="actions/add_email_action.php" method="post" enctype="multipart/form-data">
                                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">
                                          <tr><td colspan="2"><span style="color:#3C6;"><?php if (isset($_SESSION['addemail']))
                                                    echo $_SESSION['addemail']; $_SESSION['addemail']=""; ?></span></td></tr>
                                            <tr><th colspan="2"> Email </th></tr>                                           
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Type:</div></td>
                                                    <td bgcolor="#F5F5F5" width="238">                                                        
                                                        <select  name="type" id="type" class="registetextbox" > 
                                                                <option value="" selected="selected" disabled>-Select Type-</option>
                                                                <option value="Healthcare Tips" >Healthcare Tips</option>
																 <option value="Vaccination Tips" >Vaccination Tips</option>
                                                        </select>
                                                    </td>
                                            </tr>
                                            <!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="type_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr>
                                                <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>                                            
                                           <tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Category:</div></td>
                                                    <td bgcolor="#F5F5F5" width="238">                                                        
                                                        <select  name="category" id="category" class="registetextbox" > 
                                                                <option value="" selected="selected" disabled>-Select Category-</option>
                                                                <option value="Diabetology" >Diabetology</option>
																 <option value="Pediatrics" >Pediatrics</option>
                                                        </select>
                                                    </td>
                                            </tr>
											<!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="cat_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV -->
                                          <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>    
                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Upload File:</div></td>
                                                <td bgcolor="#F5F5F5" width="238"><input type="file" name="photo" id="photo" />
                                                </td>
                                            </tr>
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="file_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>
                                                <td bgcolor="#F5F5F5">&nbsp;</td>
                                                <td bgcolor="#F5F5F5"><input type="submit" value="Upload"  name="send" id="send" onclick="return add_content_email(add_email)"/></td>
                                            </tr>
                                            </table> 
                               </form>           
            </tr></table>
        </td>
    </tr>
</table>
</td></tr>
</table>
<?php
include '../footer.php'; ?>                    
</body></html>