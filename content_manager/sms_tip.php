<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../db_connect.php");
	 if(!isset($_SESSION['content_manager_name']) || $_SESSION['login'] !='content_manager')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$email_id=$_SESSION['email_id'];
		$content_manager_id = $_SESSION['content_manager_id'];
	}    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/sms_tip_validation.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include '../header.php';
include ('../db_connect.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >

<!-- ///// left menu //////  -->
<?php include 'content_writer_left_menu.php';
?>
<!-- ///// left menu //////  -->
</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>SMS Tips</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">        		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
                                    <form name="add_sms_tip" id="add_sms_tip" action="actions/add_sms_tip_action.php" method="POST" enctype="multipart/formdata">
                                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">
										<tr><td colspan="2"><span style="color:#3C6;"><?php if (isset($_SESSION['addsmstip']))
                                                    echo $_SESSION['addsmstip']; $_SESSION['addsmstip']=""; ?></span></td></tr>
                                            <tr><th colspan="2">SMS Tips</th></tr>
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>                                            <tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Type:</div></td>
                                                    <td bgcolor="#F5F5F5" width="238">                                                        
                                                        <select  name="type" id="type" class="registetextbox" > 
                                                                <option value="" selected="selected" disabled>-Select Type-</option>
                                                                <option value="Healthcare Tips" >Healthcare Tips</option>
																 <option value="Vaccination Tips" >Vaccination Tips</option>
                                                        </select>
                                                    </td>
                                            </tr>
                                            <!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="type_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr>
                                                <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>                                            
                                           <tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Category:</div></td>
                                                    <td bgcolor="#F5F5F5" width="238">                                                        
                                                        <select  name="category" id="category" class="registetextbox" > 
                                                                <option value="" selected="selected" disabled>-Select Category-</option>
                                                                <option value="Diabetology" >Diabetology</option>
																 <option value="Pediatrics" >Pediatrics</option>
                                                        </select>
                                                    </td>
                                            </tr>
											<!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="cat_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV -->
                                          <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>
                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Evidence/Reference:</div></td>
                                                <td bgcolor="#F5F5F5" width="238"><textarea name="evidence" id="evidence" value=""></textarea> 
                                                </td>
                                            </tr>
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="evid_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <tr> 
                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">SMS Tips:</div></td>
                                                <td bgcolor="#F5F5F5" width="238">												
												<!--textarea style="overflow-x: hidden;" value="" type="text" id="gBann"       
												name="tips" maxlength="172" size="1000" rows="8" cols="40" onKeyUp="toCount('gBann','uBann','{CHAR} characters remaining',172);" >
												</textarea>  
												<span id="uBann" class="minitext">160 characters remaining</span-->
													<textarea id="tips" name="tips" cols="40" rows="8" width="380px" onKeyDown="textCounter(document.add_sms_tip.tips,document.add_sms_tip.remLen2,0)" onKeyUp="textCounter(document.add_sms_tip.message,document.add_sms_tip.remLen2,0)" maxlength="160" ></textarea> 
													<div align="right">characters Count <input readonly type="text" name="remLen2" size="3" maxlength="3" style="width: 25px;" value="0"> (Max Limit 160)</div>	
                                                </td>
                                            </tr>
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="tip_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>
                                                <td bgcolor="#F5F5F5">&nbsp;</td>
                                                <td bgcolor="#F5F5F5"><input type="submit" value="Add"  name="send" id="send" onclick="return add_sms(add_sms_tip)"/></td>
                                            </tr>
                                            </table> 
                               </form>           
            </tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include '../footer.php'; ?> 
    </body></html>
	<script type="text/javascript">
	/*
function toCount(entrance,exit,text,characters) {  
    var entranceObj=document.getElementById(entrance);  
    var exitObj=document.getElementById(exit);  
    var length=characters - entranceObj.value.length;  
    if(length <= 0) {  
    length=0;  
    text='<span class="disable"> '+text+' <\/span>';  
    entranceObj.value=entranceObj.value.substr(0,characters);  
    }  
    exitObj.innerHTML = text.replace("{CHAR}",length);  
    }*/
	
	  function textCounter(field,cntfield,maxlimit) {
        if (field.value.length > 160) // if too long...trim it!
        field.value = field.value.substring(0, field.value.length-1);
        // otherwise, update 'characters left' counter
        else
        cntfield.value = maxlimit + field.value.length;
  }
    </script>