<?php
ob_start ();
error_reporting ( E_PARSE );
session_start ();
include ("../../db_connect.php");
 if(!isset($_SESSION['content_manager_name']) || $_SESSION['login'] !='content_manager')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$email_id=$_SESSION['email_id'];
		$content_manager_id = $_SESSION['content_manager_id'];				
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<script type="text/javascript" src="js/add-faq.js"></script> 
</head>
<body>
	<link href="../css/designstyles.css" media="screen, projection"	rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />
	<?php include "../header.php"; ?>
	<!-- side Menu -->
	<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="228" valign="top">
			<?php include "content_writer_left_menu.php"; ?>
		</td>			
		<td width="772" id="mainBg" valign="top"><?php
		if ($_SESSION ['msg'] != "") {
			echo "<center><font color='green' size='3'>" . $_SESSION ['msg'] . "</font></center>";
			$_SESSION ['msg'] = "";
		} else {
			if ($_SESSION ['error'] != "") {
				echo "<center><font color='red' size='3'>" . $_SESSION ['error'] . "</font></center><br />";
				$_SESSION ['error'] = "";
			}
			?>
				 <script type="text/javascript">
				</script>
				<form action="actions/add-faq-action.php" method="post" name="add_faq"
					id="add_faq" enctype="multipart/form-data">
					<table border="0" cellpadding="0" cellspacing="1" width="500"
						align="center" class="s90registerform">
						<tr>
							<th colspan="2">Add New FAQ</th>
						</tr>
						<tr>
						<tr>
							<td align="left" bgcolor="#F5F5F5">Category<font color="#FF0000">*</font>:
							</td>
							<td align="left" bgcolor="#F5F5F5"><select name="category"	id="category">
									<option value="" selected="selected" disabled>-----Select Category-----</option>
									<option value="doctor" >Doctor</option>
									<option value="patient">Patient</option>
									<option value="corporate">Corporate</option>
							</select></td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="categoryErrDiv" class="error"	style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<td><img src="../images/blank.gif" width="1" height="6" alt=""	border="0"></td>
						</tr>
						<tr>
							<td align="left" bgcolor="#F5F5F5">Question<font color="#FF0000">*</font>:
							</td>
							<td width="70%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3"	cols="35" name="question" id="question" maxlength="60"></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="questionErrDiv" class="error" style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td>
						</tr>
						<tr>
							<td width="40%" align="left" bgcolor="#F5F5F5">Answer<font
								color="#FF0000">*</font>:
							</td>
							<td width="40%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3" cols="35" name="answer" id="answer"></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="answerErrDiv" class="error" style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
						<!--  END ERROR DIV -->
						<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td>
						</tr>
						<tr>
						<td align="center" colspan="5" bgcolor="#F5F5F5">
						<input onmouseover="this.style.cursor='pointer'" value="Save" tabindex="6" type="button" onclick="add_faq_submit(add_faq)"/></td>
						</tr>
					</table>
				</form> 
				<?php }	?>
				</td>
		</tr>
	</table>
	<?php include '../footer.php'; ?>
</body>
</html>
<?php
ob_end_flush ();
?>