<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../db_connect.php");
	 if(!isset($_SESSION['content_manager_name']) || $_SESSION['login'] !='content_manager')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$email_id=$_SESSION['email_id'];
		$content_manager_id = $_SESSION['content_manager_id'];
	}    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="../css/designstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/content_email_validation.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include '../header.php';
include ('../db_connect.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!-- ///// left menu //////  -->
<?php include 'content_writer_left_menu.php';
?>
<!-- ///// left menu //////  -->
</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Update Happy Patients</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
                                    <form name="edit_email" id="edit_email" action="actions/edit_happy_patients_action.php" method="post" enctype="multipart/form-data">
                                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">
                                          <tr><td colspan="2"><span style="color:#3C6;"><?php if (isset($_SESSION['addemail']))
                                                    echo $_SESSION['editemail']; $_SESSION['editemail']=""; ?></span></td></tr>
                                            <tr><th colspan="2"> Update Happy Patients </th></tr>  
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
											<?php
											$id=$_GET['happy_patients_id'];
						$qry1= "select * from pw_happy_patients where happy_patients_id='$id'";
							$qry_rslt1 = mysql_query($qry1);
							while($result1 = mysql_fetch_array($qry_rslt1))
							{
														?>											
                                            <tr>
							<td align="left" bgcolor="#F5F5F5">Patient Name<font color="#FF0000">*</font>:
							</td>
							<td width="70%" align="left" bgcolor="#F5F5F5">
							<input type="text" name="name" id="name" value="<?php echo $result1['patient_name'] ?>" />
							</td>
						</tr>
											<input type="hidden" name="id" id="id" value="<?php echo $id?>"/>
											<input type="hidden" id="mail_photo" name="mail_photo" value="<?php echo $result1['image']; ?>" />
                                            <!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="type_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr>
                                                <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>                                            
                                           <!--tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Category:</div></td>
                                                    <td bgcolor="#F5F5F5" width="238">                                                        
                                                        <select  name="category" id="category" class="registetextbox" >
                                                                <option value="<?php echo $result1['category']; ?>" selected="selected" ><?php echo $result1['category']; ?></option>
                                                                <option value="Diabetology" >Diabetology</option>
																 <option value="Pediatrics" >Pediatrics</option>
                                                        </select>
                                                    </td>
                                            </tr-->
											<!--    ERROR DIV -->
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="cat_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV -->
											<tr>
							<td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td>
						</tr>
						
						<tr>
							<td align="left" bgcolor="#F5F5F5">Patient Testimonial<font color="#FF0000">*</font>:
							</td>
							<td width="70%" align="left" bgcolor="#F5F5F5">
							<textarea rows="3"	cols="35" name="patient_testimonial" id="patient_testimonial" ><?php echo $result1['patient_testimonial']; ?></textarea>
							</td>
						</tr>
						<!--    ERROR DIV -->
						<tr>
							<td></td>
							<td align="left" height="8">
								<div id="testimonialErrDiv" class="error" style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
							</td>
						</tr>
                                          <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
										<tr>
							<td align="center"><img src="../<?php echo $result1['image']; ?>" width="150" height="150" alt="image" border="1"/>
							</td>
							<td height="116" colspan="4">&nbsp;</td>
						</tr>
                                            <tr>
                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Upload File:</div></td>
                                                <td bgcolor="#F5F5F5" width="238"><input type="file" name="photo" id="photo" />
                                                </td>
                                            </tr>
                                            <tr><td> </td>
                                                <td  align="left">
                                                    <div id="file_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
                                            </tr>
                                            <!--  END ERROR DIV --> 
                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            <tr>
                                                <td bgcolor="#F5F5F5">&nbsp;</td>
                                                <td bgcolor="#F5F5F5"><input type="submit" value="Update"  name="send" id="send" /></td>
                                            </tr>
                                            </table> 
                               </form> 
									<?php } ?>
            </tr></table>
        </td>
    </tr>
</table>
</td></tr>
</table>
<?php
include '../footer.php'; ?>                     
</body></html>