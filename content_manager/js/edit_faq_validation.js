var category = false;
var question = false;
var answer  = false;

function edit_faq_submit(form){
	question = true;
	answer = true;
    category = true;

    document.getElementById("questionErrDiv").innerHTML= "";
    document.getElementById("questionErrDiv").innerHTML = "";
    document.getElementById("answerErrDiv").innerHTML = "";
    
    if (form.category.value=='')
    {
        document.getElementById("categoryErrDiv").innerHTML = "Select a category";
        category = false;
    }    
    if (form.question.value=='')
    {
        document.getElementById("questionErrDiv").innerHTML = "Please enter a question";
        question = false;
    }    
    if (form.answer.value=='')
    {
        document.getElementById("answerErrDiv").innerHTML = "Please enter an answer";
        answer = false;
    }    
    if(category && question && answer){
        document.getElementById('edit_faq').submit();
    }
	else
	{
		return false;
	}
}
