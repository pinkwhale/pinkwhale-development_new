<?php
session_start();
include "actions/encdec.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="Scripts/tabcontent.js"></script>
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
<!-- header.......-->
<?php include 'header.php'; ?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold'</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="970" valign="top">


 	  <!-- tabs starts here-->
<div id="tabs">
<ul id="countrytabs" class="shadetabs">     
<li><a href="#" rel="country1"><span>Pink</span> Query FAQ's</a></li>                    
<li><a href="#" rel="country2"><span>Pink</span>Appoint FAQ's</a></li>
<li><a href="#" rel="country3"><span>Pink</span>Opinion FAQ's</a></li>
<li><a href="#" rel="country4"><span>Pink</span>Consult FAQ's</a></li>
</ul>

<div style="border:1px solid #ccc; width:922px; font-family:Arial; font-size:13px; line-height:20px; font-weight:normal; margin-bottom: 1em; padding: 15px 20px">

  <!-- tabs latestMusic starts here-->
<div id="country1" class="tabcontent">
   <!-- banner left div starts here-->
<div class="dvbannerleft">   
<h3>PinkQuery FAQ's:</h3>

<ol>
<li><strong>How do I communicate with a doctor?<br>
Ans.</strong> You can communicate with the doctor through:<br>
- A phone call <br>
- Online consultation</li>

<li><strong>How do I communicate with a well-being counsellor?<br>
Ans.</strong> You can communicate with the counsellor through:<br>
- A phone call through prior appointment <br>
- Online consultation</li>

<li><strong>How do I communicate with a dietician?<br>
Ans.</strong> You can communicate with the dietician through:<br>
- Online consultation</li>

<li><strong>What does it cost?<br>
Ans.</strong> There are several convenient plans available to suit your needs. <br>
- Pay-as-you-go plan gives you unlimited access for a month. <br>  
- Annual Plan gives you unlimited access for a year. <br>
Annual plans are particularly useful if you have chronic conditions, or are taking care of elders or young children.</li>

<li><strong>How do I pay?<br>
Ans.</strong> Payment can be made through your credit or debit card via a secure payment gateway.</li>

<li><strong>What health experts are available? At what times are they available?<br>
Ans.</strong> We have certified & licensed physicians, emotional counsellors, well-being coaches, and dieticians. We are constantly expanding our network.</li>

<li><strong> How can a health expert answer my question without a physical examination?<br>
Ans.</strong> There are many health questions and concerns that we have that doesn't require a physical examination.<br>
In such situations, we offer a reliable and a effective alternative, that saves you time, energy, and money. <br>
In other situations, sharing your medical reports electronically and getting another opinion gives you a peace of mind. </li>

<li><strong> What questions can I ask health experts?<br>
Ans.</strong> Any Health & Wellness question that's bothering you. Here are some sample questions to get you going.<br>
- What kind of diet can I follow to control my blood pressure?  Can you recommend a diet lan to help me reach that weight?<br>
-  How do I control my cholesterol through lifestyle changes alone?  Which lifestyle changes will be helpful?<br>
-  How often should I check my cholesterol? Should my family members be also tested? <br>
- How often should I test my blood sugar? What should I do if it is too high or too low? <br>
- How can exercise make a difference in my diabetes? <br>
- What can be done to prevent my depression from recurring?<br>
- How do I prevent sexually transmitted disease?<br>
- How do I manage my fertility?<br>
- What are the success rates for IVF?  What is the best place to get this procedure done?<br>
- How do I de-stress myself?<br>
- What I can do to manage my anger?<br>
- What can I do to improve my relationship with peers & colleagues?<br>
- I seem to have lost perspective on this relationship. How can I rebuild it?<br>
- How might my family history affect my risk of having cardiovascular problems? <br>
- My father was diagnosed with Cancer. How can I ensure that the hospital is putting him on best treatment plan? Where can I get a second opinion?<br>
- What is this procedure? How much does it cost? What are the risks involved?<br>
- What treatments are available for this situation?<br>
- What are the side effects of this medication?<br>
- What signs and symptoms are there for kidney damage? <br>
- My mother is undergoing surgery. What questions should I ask the surgeon in the hospital? <br>


<li><strong>How confidential is my interaction with health experts?<br>
Ans.</strong> Your consultation record will not be accessed by anyone other than you and the consulting expert</li>

<li><strong>What are the benefits I get if I register with pinkWhale?<br>
Ans.</strong> <ul><li>Access to doctors, dieticians and well-being counsellors from the comfort of your home / office</li>

<li>Ability to interact with doctors without waiting in queues and spending time on travel</li> 
<strong>Additional benefits if you subscribe to our packages:</strong>
<ul><li>Access to a Personal Health Record system that helps track vital health parameters and sends alerts.</li> 
<li>Reduced rates on online second opinion consultation with super-specialists from the comfortof your home </li>
<li>Discounted rates on diagnostic services.</li></ul>


</ol>
</div>

   <!-- banner left div ends here-->
</div>

 <!-- Tab Production Directory starts here-->
<div id="country2" class="tabcontent">
  <!-- banner left div starts here-->
<div class="dvbannerleft">
   <h3>PinkAppoint FAQ's:</h3>
	pinkWhale Healthcare Services is an e-Health company offering integrated online and telephone based personal healthcare services. Our goal is to give consumers and businesses quick, easy, and convenient access to the best health care from wherever they are, using a state-of-the-art technology platform.  We are building the best network of health specialists, healthcare providers, and partners who are committed to providing health consultations by phone or online.
      </div>
   <!-- banner left div ends here-->	
 </div>

  <!-- Tab Featured Jobs starts here-->
<div id="country3" class="tabcontent">
  <!-- banner left div starts here-->
<div class="dvbannerleft">     
  <h3>PinkOpinion FAQ's:</h3>

<ol>
<li><strong>Why should I use pinkWhale's second opinion service?<br />
Ans.</strong> We bring access to super-specialist doctors at your convenience, wherever and whenever you need it. Our specialists are committed to providing you with high quality care that is centered on your needs, provide convenient access, comfort, and to give you the peace of mind.</li>

<li><strong>When and why should I seek second opinion?<br />
Ans.</strong><strong>1.</strong>        If your health insurance company requires it<br />
<strong>2.</strong>     When you are not happy with your diagnosis<br />
<strong>3.</strong>     You are not satisfied with your doctor's initial opinion<br />
<strong>4.</strong>     To see if there are alternate treatment options<br />
<strong>5.</strong>     Having a major surgery, questioning if surgery is the only option, and to see if you could avoid a surgical procedure<br />
<strong>6.</strong>     A problem that has been difficult for a regular doctor to diagnose<br />
<strong>7.</strong>     Having trouble talking to your current doctor<br />
<strong>8.</strong>     You are not seeing improvement in your medical condition<br />
<strong>9.</strong>     You are diagnosed with a life threatening disease such as cancer, heart disease or brain tumor, and that a second surgery is recommended<br />
<strong>10.</strong>    You are having multiple medical complications.</li>

<li><strong>In what medical areas can I get second opinion?<br />
Ans.</strong> Second opinion is vital for patients with heart ailments, cancer, hip replacement, brain tumor, or any major medical decision. </li>

<li><strong>How much does second opinion cost?<br />
Ans.</strong> It varies depending on the specialist, and the area of specialty. The cost that each specialist charges is listed on the specialist page.</li>

<li><strong>How do I choose an expert for second opinion?<br />
Ans.</strong> We have super specialists in several medical areas and their expertise is described on the specialist page. If you have difficulty in selecting a specialist, contact us and our medical specialists will help you.</li>

<li><strong>Do I need a computer or email account to get second opinion?<br />
Ans.</strong> You need to have access to a computer and internet to upload your medical information. For convenience, you need to have an email id so that you can get updates/confirmation when the second opinion is delivered.</li>

<li><strong>How do I upload my medical records?<br />
Ans.</strong> All documents can be uploaded in digital form. Paper records can be scanned and uploaded. Lab test information can be entered as plain text. Any imaging data such as ECG, EEG, X-ray will have to be scanned.</li>

<li><strong>Can my medical records be in any language?<br />
Ans.</strong> We can analyze only documents in English.</li>

<li><strong>How long will it take to get second opinion?<br />
Ans.</strong> Usually 24-48 hrs</li>

<li><strong>How are payments made?<br />
Ans.</strong> By credit card or debit card.</li>

<li><strong>Can I speak or meet with the doctor who provides second opinion?<br />
Ans.</strong> After you received your second opinion, if you need to discuss, follow-up, or meet the doctor, you can seek an appointment to talk/meet the specialist.</li>

<li><strong>Will my medical information be private?<br />
Ans.</strong> Yes.  pinkWhale's portal is secure and this service is confidential. Only you and the referring expert will have access to  your medical data.</li>
</ol>
      </div>
   <!-- banner left div ends here-->	
</div>


 <!-- Tab Production Directory starts here-->
<div id="country4" class="tabcontent">
  <!-- banner left div starts here-->
<div class="dvbannerleft">  
	<h3>PinkConsult FAQ's:</h3>

<ol>
<li><strong>Why should I use pinkWhale's specialists?<br />
Ans.</strong> We bring access to the best specialist doctors for your convenience. We provide the convenience of teleconsultation and online consultation for follow-up visits.</li>

<li><strong>What should I discuss with a specialist?<br />
Ans.</strong> Any medical problem for you or for any of your members in your family.</li>

<li><strong>What does it cost?<br />
Ans.</strong> It varies depending on the specialist, and the area of specialty. The cost that each specialist charges is listed on the specialist page.  When you consult by phone or online, you are saving on the travel time & waiting time too.</li>


<li><strong>How are payments made?<br />
Ans.</strong> By cash or cheque at the doctor's office. By credit card, debit card, at pinkWhale website.</li>

<li><strong>Can you give me a referral for a specialist in my locality?<br />
Ans.</strong> Please call us at: Please write to us at info@pinkwhalehealthcare.com. We will help you to identify the right specialist for you.</li>

<li><strong>When is phone or email follow-up consultation appropriate?<br />
Ans.</strong><strong>1.</strong> Email and phone consultation is NOT for, emergencies or life-threatening situations (for example, stroke, chest pain).<br />
<strong>2.</strong>	Email consultation is appropriate for non-urgent care when you need medical advice, a second medical opinion, or you need clarification on a treatment plan.<br />
<strong>3.</strong>	Phone consultations are appropriate for Urgent Care, not Emergency Care.<br /> For example:<br />
<blockquote>
<strong>a.</strong>	For diabetes patients, if you are going into hypoglycemia, a quick advice from the doctor on the phone can save you the rush to the ER. <br /> 
<strong>b.</strong>	After a post-cataract surgery, you want to follow-up with doctor because of a red eye or pain in the eye.  <br />
<strong>c.</strong>	After your chemo sessions, you want to call because of side-effects such as gastritis, constipation, or loss of hair. <br />
<strong>d.</strong>	A couple of days after the in-person visit to the doctor, your child continues to have fever, cold, cough, and you want to confirm medications or need to start new medications.<br />
<strong>e.</strong>	In case of a foreign body in the eye or ear, to get advice on the first aid. <br />
<strong>f.</strong>	If you have other medical or surgical conditions, a call to get advice is appropriate.<br />
</blockquote>
</li>
<li><strong>How long is a phone follow-up consultation?<br />
Ans.</strong> There are no time limits. It is up to and your doctor to determine depending on the situation. It's good practice for you to think about write down your questions you want to ask/discuss before you start the phone consultation.</li></ol>
</div>
      </div>
   <!-- banner left div ends here-->	
 </div>

 <!-- Featured Classifieds here--> 

<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>	  
 <!--Main Container ends here-->
 </div> </div>
 



&nbsp;</td></tr>
</table>


<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
