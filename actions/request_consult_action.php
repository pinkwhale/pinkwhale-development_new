<?php
ob_start();
session_start();
error_reporting('E_NOTICES');
include '../db_connect.php';
include 'new_validation.php';
include '../class/fileHandler.php';

date_default_timezone_set('Asia/Calcutta');
$timestamp=time();
$timestamp = date("Y-m-d H:i:s",$timestamp);
$after1min = time() + (1*1*1*10);   // 7 days; 24 hours; 60 mins; 60secs
$timestamp1 = date("Y-m-d H:i:s",$after1min);
$date=substr($timestamp,0,10);
$date;
$time=substr($timestamp,11,30);
$time;
$date_time=date("Y-m-d H:i:s");

$complaint = mysql_escape_string(trim($_POST['complaint']));
$content = mysql_escape_string(trim($_POST['body']));
$user = mysql_escape_string(trim($_POST['user']));
$doc_id = mysql_escape_string(trim($_POST['doc_id']));
$doc_name = get_docname($doc_id);
$doc_email = get_doc_email($doc_id);
$valid_login = false;
$pinkwhalecard = mysql_escape_string(trim($_POST['newPinkcard']));

if($user=="new"){
    $name = mysql_escape_string(trim($_POST['newname']));
    $age = mysql_escape_string(trim($_POST['newage']));
    $gender = mysql_escape_string(trim($_POST['newgender']));
    $email = mysql_escape_string(trim($_POST['newemail']));
    $password = mysql_escape_string(trim($_POST['newpassword']));
    if($pinkwhalecard==""){
        $valid_login = login_new_user($name,$age,$gender,$email, $password);
    }else{
        $valid_login = login_new_card_user($name,$age,$gender,$email, $password,$pinkwhalecard);
    }
}else if($user=="exist"){
    $email = mysql_escape_string(trim($_POST['extuseremail']));
    $password = mysql_escape_string(trim($_POST['extuserpassword']));
    $valid_login = login_exist_user($email, $password);
}else{
    $valid_login = true;
}

$path = "images/upload_reports/";

if($valid_login){
    
    $pw_card = $_SESSION['pinkwhale_id'];
    $user_name = $_SESSION['username'];
    
    
    if(!$qry4=mysql_query("SELECT max(`report_id`) FROM `user_reports` ")) exit("error in table1");
    else 
    {
            while($r=mysql_fetch_array($qry4)) $maxno=$r[0];
    }
    
    foreach($_FILES['uploadedfile']['error'] as $key => $error){
        ++$maxno;
        $filename = $_FILES['uploadedfile']['name'][$key];
        $filename_tmp = $_FILES['uploadedfile']['tmp_name'][$key];
        $repo_size=$_FILES["uploadedfile"]["size"][$key];
        $filePath = '../'.$path.$maxno.'-report-'.$filename;
        if(move_uploaded_file($filename_tmp,$filePath)){
            $repo_name = $filename;
            $upload_photo = $path . $maxno."-report-".$filename;
            $multiple_file_upload .= $upload_photo."|||";
            $qryUserReport = mysql_query("INSERT INTO `user_reports` (`pwcard_no`,`report_name`,`report_file`,`size`,`report_type`,`added_date`) VALUES ('$pw_card','$repo_name','$upload_photo','$repo_size','Others','$timestamp')");
        }
    }
    
    $multiple_file_upload = substr($multiple_file_upload, 0, -3);
    
    
    $qry1=mysql_query("INSERT INTO `pw_consultation_emails_tmp`(`pw_card_id`,`doctor_id`,`doctor_name`,`patient_name`,`cnsltion_mail_details`,`mail_status`,`entered_time`,`entered_date`,upload_record,complaint) VALUES ('$pw_card','$doc_id','$doc_name','$user_name','$content','open','$time','$date','$multiple_file_upload','$complaint')");
    if($qry1){
        
        $serial_no = get_tmp_consult_id($pw_card, $doc_id);
        
        $qry2= "SELECT * FROM `online_cnsultn_credit_spe_exp` WHERE `user_id`='$pw_card' && `spe_doctor_id`='$doc_id'";
        $qry_rslt2 = mysql_query($qry2);
        $qry2_num = mysql_num_rows($qry_rslt2);
        if($qry2_num>0){
            while($result2 = mysql_fetch_array($qry_rslt2))	
            {
                    $report_userid=$result2['user_id'];
                    $report_docid=$result2['spe_doctor_id'];
                    $old_avlble_bal=$result2['spe_balance'];
                    $usage=$result2['spe_usage'];

                    if($old_avlble_bal>0){
                        
                        $updated = insert_consult($serial_no, $email, $doc_email); 
                        
                        if($updated){
                            $_SESSION['success']="Successfully sent consultation.";
                        }else{
                            $_SESSION['fail']="Error!! Please try Again";
                        }
                        
                    }else{
                        /***** Send to cart******/
                        $doc_charge = doc_charge_pinkConsult($doc_id); 
                        
                        
                        /****
                         * 
                         *  $doc_ser = doctor_id||TempMailNo;
                         * 
                         ****/
                        
                        $_SESSION["DirectConsult"] = "pinkconsult";
                        
                        $doc_ser = $doc_id."||".$serial_no;
                        
                        addCart("PinkConsult", $doc_ser, $doc_name, $doc_charge);
                        
                        header("Location: ../cart_test.php");
                        
                        exit;
                    }
            }
        }else{
             /***** Send to cart******/
            
            $doc_charge = doc_charge_pinkConsult($doc_id); 
            
            /****
             * 
             *  $doc_ser = doctor_id||TempMailNo;
             * 
             ****/
            
            $_SESSION["DirectConsult"] = "pinkconsult";

            $doc_ser = $doc_id."||".$serial_no;
            
            $doc_charge = doc_charge_pinkConsult($doc_id);      
            
            addCart("PinkConsult", $doc_ser, $doc_name, $doc_charge);
            
            header("Location: ../cart_test.php");
            
            exit;
        }
        
    }else{
        $_SESSION['fail']="Error!! Please try Again";
        
    }    
    
    
}else{
    if($user=="new"){
        
	if($pinkwhalecard!=""){
            $_SESSION['fail']="Invalid pinkWhale Card number";
        }else{
            $_SESSION['fail']="Email ID Already Exists";
        }         
    
    }else if($user=="exist"){
        
        $_SESSION['fail']="Invalid Email ID";
        
    }
    
}

header("Location: ../request_consultation.php?type=4");

ob_end_flush();
?>

