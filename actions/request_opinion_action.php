<?php
ob_start();
session_start();

include '../db_connect.php';
include 'new_validation.php';
include '../class/fileHandler.php';

date_default_timezone_set('Asia/Calcutta');
$timestamp=time();
$timestamp = date("Y-m-d H:i:s",$timestamp);
$after1min = time() + (1*1*1*10);   // 7 days; 24 hours; 60 mins; 60secs
$timestamp1 = date("Y-m-d H:i:s",$after1min);
$date=substr($timestamp,0,10);
$date;
$time=substr($timestamp,11,30);
$time;
$date_time=date("Y-m-d H:i:s");

$path = "images/upload_reports/";

$multiple_file_upload = "";
$condition_first=mysql_escape_string($_POST['condition_first']);
$first_opinion=mysql_escape_string($_POST['first_opinion']);
$treatment=mysql_escape_string($_POST['treatment']);
$complaint = mysql_escape_string(trim($_POST['complaint']));
$content = mysql_escape_string(trim($_POST['body']));
$user = mysql_escape_string(trim($_POST['user']));
$doc_id = mysql_escape_string(trim($_POST['doc_id']));
$doc_name = get_docname($doc_id);
$doc_email = get_doc_email($doc_id);
$valid_login = false;


if($user=="new"){
    $name = mysql_escape_string(trim($_POST['newname']));
    $age = mysql_escape_string(trim($_POST['newage']));
    $gender = mysql_escape_string(trim($_POST['newgender']));
    $email = mysql_escape_string(trim($_POST['newemail']));
    $password = mysql_escape_string(trim($_POST['newpassword']));
    $valid_login = login_new_user($name,$age,$gender,$email, $password);
}else if($user=="exist"){
    $email = mysql_escape_string(trim($_POST['extuseremail']));
    $password = mysql_escape_string(trim($_POST['extuserpassword']));
    $valid_login = login_exist_user($email, $password);
}else{
    $valid_login = true;
}



if($valid_login){
    
    $pw_card = $_SESSION['pinkwhale_id'];
    $user_name = $_SESSION['username'];
    
    
    if(!$qry4=mysql_query("SELECT max(`report_id`) FROM `user_reports` ")) exit("error in table1");
    else 
    {
            if($r=mysql_fetch_array($qry4)) 
                $maxno=$r[0];
            
            if($maxno==""){
                $maxno = 0;
            }
    }
   
    foreach($_FILES['uploadedfile']['error'] as $key => $error){
        ++$maxno;
        $filename = $_FILES['uploadedfile']['name'][$key];
        $filename_tmp = $_FILES['uploadedfile']['tmp_name'][$key];
        $repo_size=$_FILES["uploadedfile"]["size"][$key];
        $filePath = '../'.$path.$maxno.'-report-'.$filename;
        if(move_uploaded_file($filename_tmp,$filePath)){
            $repo_name = $filename;
            $upload_photo = $path . $maxno."-report-".$filename;
            $multiple_file_upload .= $upload_photo."|||";
            $qryUserReport = mysql_query("INSERT INTO `user_reports` (`pwcard_no`,`report_name`,`report_file`,`size`,`report_type`,`added_date`) VALUES ('$pw_card','$repo_name','$upload_photo','$repo_size','Others','$timestamp')");
        }
    }
    
    $multiple_file_upload = substr($multiple_file_upload, 0, -3);
    
    
    $qry1=mysql_query("INSERT INTO `pw_expert_consultation_emails_tmp`(`pw_card_id`,`doctor_id`,`doctor_name`,`patient_name`,
	`cnsltion_mail_details`,`mail_status`,`entered_time`,`entered_date`,upload_record,complaint,condition_first,first_opinion,treatment)
	VALUES ('$pw_card','$doc_id','$doc_name','$user_name','$content','open','$time','$date','$multiple_file_upload','$complaint','$condition_first','$first_opinion','$treatment')");
   // echo $qry1;exit;
	if($qry1){
        
        $serial_no = get_tmp_opinion_id($pw_card, $doc_id);
        
        $qry2= "SELECT * FROM `online_cnsultn_credit_spe_exp` WHERE `user_id`='$pw_card' && `spe_doctor_id`='$doc_id'";
        $qry_rslt2 = mysql_query($qry2);
        $qry2_num = mysql_num_rows($qry_rslt2);
        if($qry2_num>0){
            while($result2 = mysql_fetch_array($qry_rslt2))	
            {
                    $report_userid=$result2['user_id'];
                    $report_docid=$result2['exp_doctor_id'];
                    $old_avlble_bal=$result2['exp_balance'];
                    $usage=$result2['exp_usage'];

                    if($old_avlble_bal>0){
                        
                        $updated = insert_opinion($serial_no, $email, $doc_email); 
                        
                        if($updated){
                            $_SESSION['success']="Successfully sent consultation.";
                        }else{
                            $_SESSION['fail']="Error!! Please try Again";
                        }
                        
                    }else{
                        /***** Send to cart******/
                        $doc_charge = doc_charge_pinkOpinion($doc_id); 
                        
                        
                        /****
                         * 
                         *  $doc_ser = doctor_id||TempMailNo;
                         * 
                         ****/
                        
                        $_SESSION["DirectConsult"] = "pinkopinion";
                        
                        $doc_ser = $doc_id."||".$serial_no;
                        
                        addCart("PinkOpinion", $doc_ser, $doc_name, $doc_charge);
                        
                        header("Location: ../cart_test.php");
                        
                        exit;
                    }
            }
        }else{
             /***** Send to cart******/
            
            $doc_charge = doc_charge_pinkOpinion($doc_id);
            
            /****
             * 
             *  $doc_ser = doctor_id||TempMailNo;
             * 
             ****/
            
            $_SESSION["DirectConsult"] = "pinkopinion";

            $doc_ser = $doc_id."||".$serial_no;
            
            $doc_charge = doc_charge_pinkOpinion($doc_id); 
            
            addCart("PinkOpinion", $doc_ser, $doc_name, $doc_charge);
            
            header("Location: ../cart_test.php");
            
            exit;
        }
        
    }else{
        $_SESSION['fail']="Error!! Please try Again";
        
    }    
    
    
}else{
    if($user=="new"){
        $_SESSION['fail']="Email ID Already Exists";
    }else if($user=="exist"){
        $_SESSION['fail']="Invalid Email ID";
    }
    
}

header("Location: ../request_opinion.php?type=3");

ob_end_flush();
?>

