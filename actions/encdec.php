<?php
function encode_base64($sData)
{ 
    $sBase64 = base64_encode($sData); 
    return strtr($sBase64, '+/', '-_'); 
} 

function decode_base64($sData)
{ 
    $sBase64 = strtr($sData, '-_', '+/'); 
    return base64_decode($sBase64); 
}

/*

$key='mysecretkey'; 


$string1='rejanish'; 


$string2=encode_base64($string1); 


$string3=decode_base64($string2); 

echo '<span style="font-family:Courier">'."\n"; 
echo 'Key: '.$key.'<br>'."\n"; 
echo $string1.'<br>'."\n"; 
echo $string2.'<br>'."\n"; 
echo $string3.'<br>'."\n"; 
echo '</span>'."\n"; 

*/
?>