<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Ask a Doctor: Health, Medical, and Wellness Questions from Anywhere | Health Counselling & Coaching</title>
<meta name="keywords" content="Seasonal Diseases, Diabetes, Hypertension, Arthritis, Asthma, Allergies, Heart Burn, Conjunctivitis, Glaucoma, Gastric Disorder, Heartburn, Irritable Bowels, Oral Health, Women's Health, Pregnancy, Infertility, Breastfeeding, Child Development, Immunizations, Diet, Exercise, Nutrition, Weight Loss, Obesity,  Acne, Hair Loss, Men's Health, Hepatitis, Sexual Disorders, Skin Disorders, Aches/Pains, Fatigue, Stress, Anxiety, Parenting, Work/Life Balance, Relationships, Depression, Alcohol, Smoking, Addiction, Medical Procedure Information, Medication Side Effects, Signs & Symptoms, Preventive Check-ups, Specialist Referrals, Getting Medical Second Opinion"/>
<meta name="description" content="Professional help from certified & licensed physicians, emotional counsellors, well-being coaches, and dieticians"/>



<meta name="description" content="Professional help from certified dieticians & nutritional coaches">
<link href="./Ask a Dietitian_files/designstyles.css" rel="stylesheet" type="text/css">

<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
<script src="./Ask a Dietitian_files/mouseoverscripts.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" src="../js/registration_validation.js"></script>


<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
<style type="text/css">
 @media print { #feedlyMiniIcon { display: none; }
  }
  </style>
</head>
    <body align="center">

<!-------------------------- header..------------------------------>
<script language="javascript">var sitePrefix = 'http://www.pinkwhalehealthcare.com';</script>
<link href="./Ask a Dietitian_files/front.css" media="screen, projection" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./Ask a Dietitian_files/header_colourchange.js"></script>
<script type="text/javascript" src="./Ask a Dietitian_files/login.js"></script>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px">
<tbody>
<tr><td width="298" rowspan="2">
	<a href="http://www.pinkwhalehealthcare.com/index.php"><img src="./Ask a Dietitian_files/pinkwhale_logo.jpg" width="294" height="104"></a>
</td>
<td width="700" align="right" valign="bottom">
<img src="./Ask a Dietitian_files/ask-a-doctor.png" width="550" height="104">
</td>

</tr>
</tbody></table>
<script type="application/javascript">
colourchange3();
</script>
<!------------------------- header.----------------------------------------->

<!-- header.......-->
<form action="../actions/registration_action.php" method="post" name="reg_form" id="reg_form">
   
    <table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" class="s90greybigbox">
  	<tr><td width="503">
<img src="./Ask a Dietitian_files/lady.jpg" width="550" height="400">
</td>

    <td align="center" bgcolor="orange"><table border="0" cellpadding="0" cellspacing="1" width="445" align="left" class="s90registerform">
           <font face="arial"> <h2> Sign-up Now! Get 7-day FREE Trial!<br> <font color="white"> Your Personal Health Guide & Coach. </font></h2>
  		<tbody>

    <tr>
        	<td width="40%" align="right">Name<font color="#FF0000">*</font>:</td>
  			<td width="60%" align="left">
  				<input size="20" type="text" maxlength="40" name="regname" value="" class="s90regformtext">
        	</td>
        </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td align="left" height="8">
	            <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  		<tr>
            <td align="right">Email<font color="#FF0000">*</font>:<span onmouseover="showsummary(&#39;details&#39;, event, &#39;To be used as User ID&#39;);this.style.cursor=&#39;pointer&#39;;" onmouseout="hideSummary(&#39;details&#39;);" onmousemove="moveSummary(&#39;details&#39;, event);" class="pwinfohelp"></span></td>
            <td nowrap="nowrap">
            	<input class="s90regformtext" type="text" maxlength="45" size="20" id="regemail" name="regemail" value="" onblur=" return check_mail(regemail);">
            </td>
        </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td align="left" height="8">
	            <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  		<tr>
        	<td align="right">Password<font color="#FF0000">*</font>:<span onmouseover="showsummary(&#39;details&#39;, event, &#39;Please enter password of your choice with a minimum of 6 characters and a maximum of 12 characters.&#39;);this.style.cursor=&#39;pointer&#39;;" onmouseout="hideSummary(&#39;details&#39;);" onmousemove="moveSummary(&#39;details&#39;, event);" class="pwinfohelp"></span></td>
  			<td>
  			<input type="password" class="s90regformtext" maxlength="12" size="20" id="regPassword" name="regPassword" value=""><span style="color:green" id="result"></span>
         	</td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td align="left" height="8">
	            <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  	   <tr>
       		<td align="right">Confirm Password<font color="#FF0000">*</font>:</td>
  			<td>
  				<input type="password" class="s90regformtext" maxlength="12" size="20" name="regConfirmPassword" value="">
            </td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td align="left" height="8">
	            <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  <tr>
       		<td align="right" style="line-height:20px;">Mobile #<font color="#FF0000">*</font>:</td>
  			<td style="line-height:30px;">
  				<input type="text" class="s90regformtext" maxlength="20" name="regPhone1" value="" onkeypress="return isNumberKey(event);"></td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->


  		<tr>
        	<td colspan="2"><img src="./Ask a Dietitian_files/spacer.gif" width="44" height="20" align="absmiddle">
  				<input type="checkbox" name="regagreeterms" value="agree">I agree to the <a href="http://www.pinkwhalehealthcare.com/terms.php" onmouseover="this.style.cursor=&#39;pointer&#39;;" style="text-decoration: underline;">Terms of service</a><font color="#FF0000">*</font>
            </td>
        </tr>
<!--    ERROR DIV -->
		<tr>
             <td colspan="2" height="8" align="center">
	            <div id="termsErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  		<tr>
        	<td>&nbsp;</td>
        	<td height="50">
  				<input onmouseover="this.style.cursor=&#39;pointer&#39;" type="button" name="btnSubmit" value=" Join Now " onclick="PWRegisterSignUp(reg_form)" id="btnSubmit">
        	</td>
        </tr>
      </table>
 </td></tr></table>
</form>

<table width="1000" border="1" align="center"><tr>
<td width="495" bgcolor="white" valign="top" align="left"> <font face="arial" color="black">
<h3>
<img src="./Ask a Dietitian_files/question.jpg" width="80" height="80"> WHAT YOU CAN ASK A DOCTOR?</h3>
<ol>
<li> Preventive measures to live a healthy lifestyle. </li>
<p>
</p><li>Management plans for chronic conditions such as hypertension and diabetes. </li>
<p>
</p><li> Understand infertility, manage your fertility, and handle sexual health issues. </li>
<p>
</p><li> Questions related to pregnancy, and motherhood. </li>
<p>
</p><li> How to manage health issues such as obesity, hair loss, and allergies.</li>
<p>
</p><li> Handling digestive problems, and gastric conditions </li>
<p>
</p><li> Exploring alternative treatment plans, get second opinion from leading experts.</li>
<p>
</p><li> Understand blood tests, health check-up reports, diagnostic reports, and more...</li>
</ol>
</font>
</td>

<td width="495" bgcolor="white" valign="top" align="left"> <font face="arial">
<h3>
<img src="./Ask a Dietitian_files/ask.jpg" width="80" height="80"> WHY YOU SHOULD USE THIS SERVICE?</h3>
<ol>
<li>Ask from wherever you are. It's about convenient access to certified doctors.</li>
<p>
</p><li>No need to travel, and wait in traffic to ask a doctor. Ask immediately.</li>
<p>
</p><li> Having a doctor as your personal health coach will help you handle health situations well. </li>
<p>
</p><li>You can follow-up unlimited number of times with doctor. </li>
<p>
</p><li>Ask any Health & Wellness question. Understand ways to live a stress-free life.</li>
<p>
</p><li> Online Doctor & Health Coach will allow you to ask whenever you have a question. </li>
<p>
</p><li> Take control of your health with authentic information from a doctor </li>
</ol>
</font>
</td>
</tr></table>



<table width="1000" border="1" align="center"><tr>
<td width="495" bgcolor="white" valign="top" align="left"> <font face="arial">
<h3>
<img src="./Ask a Dietitian_files/dietitian_benefits.jpg" width="80" height="80"> WHY PINKWHALE? - BENEFITS</h3>
<ol>
<li> Experienced and Certified doctors, dietitians, and well-being coaches. </li>
<p>
</p><li> Pioneers in bringing the convenience of access to dietitians online. </li>
<p>
</p><li> Your interactions with expert is stored in your health record for your reference.</li>
<p>
</p><li> It's simple &amp; affordable. </li>
<p>
</p><li> It's completely secure &amp; private. </li>
</ol>
</font>
</td>

<td width="495" bgcolor="white" valign="top" align="left"> <font face="arial">
<h3>
<img src="./Ask a Dietitian_files/dietitian_feedback.jpg" width="80" height="80"> CUSTOMER TESTIMONIALS</h3>
<font color="blue">
<ol> <i>
<li>"Response is very much precise. Helps understand the root cause..."</li>
<p>
</p><li> "...gave me specifics and asked specific questions." </li>
<p>
</p><li> "I would recommend pinkwhale to my friends."</li>
<p>
</p><li> "Your service is very client friendly and helpful." </li>
<p>
</p><li> "I would recommend this to a friend." </li></i>
</ol>
</font>
</font>
</td>
</tr></table>



<table width="1000" border="0" cellspacing="0" cellpadding="0" class="s90footer" align="center">
<tbody><tr><td colspan="2"><img src="./Ask a Dietitian_files/footertop.jpg" width="1000" height="32"></td></tr>
<tr><td>

<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers.">
<tbody><tr>
<td width="135" align="center" valign="top"><script type="text/javascript" src="./Ask a Dietitian_files/getseal"></script><br>
<a href="http://www.verisign.com/verisign-trust-seal" target="_blank" style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT TRUST ONLINE</a></td>
</tr>
</tbody></table>

</td>
<td width="700"><a href="http://www.pinkwhalehealthcare.com/index.php">Home</a> |
<a href="http://www.pinkwhalehealthcare.com/about_us.php">About Us</a> |
<a href="http://blog.pinkwhalehealthcare.com/">Wellness Blog</a> |
<a href="http://www.pinkwhalehealthcare.com/contact_us.php">Contact Us</a> |
<a href="http://www.pinkwhalehealthcare.com/doctors_directory.php">Doctors List</a><br>
<!--<a href="http://www.pinkwhalehealthcare.com/faqs.php">FAQ</a> |-->
<a href="http://www.pinkwhalehealthcare.com/advertise_with_us.php">Advertise with us</a> |
<a href="http://www.pinkwhalehealthcare.com/terms.php">Terms of Service</a> |
<a href="http://www.pinkwhalehealthcare.com/privacy_policy.php">Privacy Policy</a> <br>
<!--<a href="http://www.pinkwhalehealthcare.com/price_draw.php">Prize Draw</a><br />-->
©  www.pinkwhalehealthcare.com. All rights reserved.  </td></tr></tbody></table>

<script type="text/javascript" src="./Ask a Dietitian_files/clickheat.js"></script><noscript>&amp;lt;p&amp;gt;&amp;lt;a href="http://www.labsmedia.com/index.html"&amp;gt;Seo tools&amp;lt;/a&amp;gt;&amp;lt;/p&amp;gt;</noscript><script type="text/javascript"><!--
clickHeatSite = 'Pinkwhalehealthcare';clickHeatGroup = encodeURIComponent(window.location.pathname+window.location.search);clickHeatQuota = 3;clickHeatServer = 'http://www.pinkwhalehealthcare.com/clickheat/click.php';initClickHeat(); //-->
</script>

<img id="feedlyMiniIcon" title="feedly mini" style="position: fixed; bottom: 14px; right: 14px; z-index: 99; cursor: pointer; border-style: initial; border-color: initial; -webkit-transition-property: opacity; -webkit-transition-duration: 0.3s; -webkit-transition-timing-function: ease; -webkit-transition-delay: initial; visibility: visible; width: 24px; height: 24px; max-height: 24px; max-width: 24px; overflow-x: hidden; overflow-y: hidden; display: block; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; opacity: 0.35; " width="24" height="24" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAYlJREFUeNrMVD1rwlAUPfkwBBxFKQQlDhmjqzj4F9w7FfwThZaW9n8UnDrrn1BEXNTBRQQhEE1ERRR0Sd990A7VaKJJ6YHzEu4N99z3zrsRGo2GAuCF8YFRQzSwGOuM7zJb3hgfES2o0SdGSWRLDfGhRgLpGAXS4q0VCoUCBEHwzd8skM/nUSqVoCjKybz8O1CtVn2LeZ4H27YxnU6xXC6x3+95PJPJoFKpoN1uY7vdnhfww3w+x3A4xGaz4d3quo5sNvuTTyaTXKTb7cJ13csCzWbzKKaqKj/zXC4HSZKO8iRcLpcxGAwwmUzC7UDTNBSLRSQSiVAeyUGNpM4v4XA4oNfr8eMMLJBKpWCaJn9fLBawLAur1Qq73Y6b/H0pyNyrTDYMA7PZDKPRCOv1+uQ3ZCqZe9UcOI6DTqfjW5zMbLVa3Bu6RaE9GI/HZ/P9fp8/qfipYROj+ukEnuQgEx0GImKGHGSC//UO/kTAibG+QwIfMQrUyeRXusaM94x3ERW2GT8Zn78EGACRmoKUJhB1TQAAAABJRU5ErkJggg==">
    </body></html>
