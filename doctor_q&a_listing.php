<?php
session_start();
if($_SESSION['login']!='Doctor')
	{
		header("Location: index.php");
		exit();		
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Counsellor Q&A Listing</title>

<meta name="keywords" content="cancer treatment opinion,diabetes treatment opinion, surgery opinion, plastic surgery, cosmetic surgery, second opinion diagnosis"/>

<meta name="description" content="pinkWhale has brought together world renowned super-specialists, who can give you a second medical opinion online. Our 2nd opinion doctors will review your medical information, medical records, and diagnostic tests to render a
comprehensive second opinion that includes recommendations, alternative treatment options, and therapeutic considerations."/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<!-- header.......-->
<?php 
include "db_connect.php";
include 'header.php'; 
	include "actions/encdec.php";
	//echo $_SESSION['doctor_type'];
?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu4').style.fontWeight= 'bold'</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="175" style="border-right:1px solid #4d4d4d" valign="top">
<table width="175" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<tr><th>Doctor Q&A :</th></tr>
<tr><td><a href="doctor_q&a_listing.php"><b>Pending Queries</b></a></td></tr>
<tr><td><a href="doctor_answered_list.php">Answered Queries</a></td></tr>
</table></td>
<td><table width='750' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="9"> Doctor Pending Q&A </th></tr>
<?php		
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
$check=0;
		// Display all the data from the table 
			$sql="SELECT * FROM `q_a_db`   where doctor_type='Doctor' order by `q_id` DESC";
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) 
			{
			?>
		<tr><td colspan="9" align="center"><b>No Records</b></td></tr>
        <?php }
		else {  ?>
			<tr>
                 <td align='left' width="30"><strong>SI.</strong></td>
                <td align='center'><strong>Subject</strong></td>
                <td align='center'><strong>User</strong></td>
                <td align='center'><strong>Group</strong></td>
                <td align='center'><strong>Card #</strong></td>
                <td align='center'><strong>Last Date</strong></td>
                <td align='center'><strong>Answered by</strong></td>
                <td align='center'><strong>Status</strong></td>        
                <td align='left'><strong>Answer</strong></td>
           </tr>
	
			<?php	
			$count =0;		
			while($row = mysql_fetch_assoc($rs)) 
			{
				$imf_flag=0;
				$question=$row['question'];
				$user_id=$row['using_user'];
				$qid=$row['q_id'];
				$submited_date=$row['submited_date'];
				$start_date=date("d-m-Y", strtotime($submited_date));
				
					$qry3= "SELECT * FROM `user_details` WHERE `user_id`='$user_id'";
					$qry_rslt3 = mysql_query($qry3);
					while($result3= mysql_fetch_array($qry_rslt3))
					{
						$user_name=$result3['user_name'];
						$card_id=$result3['user_id'];
						$group=$result3['group'];
					}
		$sql_table="SELECT * FROM `answer_qa_query`  where qa_id='$qid' and qa_status='close' ";
		$qry_sql_table = mysql_query($sql_table);
		if(mysql_num_rows ($qry_sql_table)<=0) {
			
			
				$count ++;
				$check++;
				echo "<tr bgcolor='#ffffff' style='color:#000000;' >";
				echo "<td>$count </td> ";
				echo "<td align='left'>$question </td> ";
				echo "<td align='center'>$user_name</td> ";
				echo "<td align='center'>$group</td> ";
				echo "<td align='center'>$card_id</td> ";
						$max_qry= "SELECT max(`qa_answer_qry`), max(`qa_answer_date`) FROM `answer_qa_query` WHERE `qa_id`='$qid'";
						$max_qry_rslt = mysql_query($max_qry);
						while($max= mysql_fetch_array($max_qry_rslt)) {
							$max_qa_id=$max['max(`qa_answer_qry`)'];
							$last_date1=$max['max(`qa_answer_date`)'];
							if($last_date1!="0000-00-00 00:00:00")
							{
							$added_date=date("d M Y g:i A",strtotime($last_date1));
							$today_date=$added_date;
							$new_time1 = strtotime(date("d M Y g:i A", strtotime($today_date)) . " +10 hour");
							$new_time21=date("d M Y g:i A", $new_time1);
							$timestamp2 = strtotime("$new_time21");
							$etime = strtotime("+30 minutes", $timestamp2);
					
							$new_time=date("d M Y g:i a", $etime);
							$last_date= date("d M Y g:i A",strtotime($new_time)).'<br>';
							}
							else $last_date="";
						}
						
					$sql_table="SELECT * FROM `answer_qa_query`  where qa_answer_qry='$max_qa_id'";
					$sql_table_qry_rslt = mysql_query($sql_table);
					$num_rows = mysql_num_rows($sql_table_qry_rslt);
					if($num_rows<=0) {
						echo '<td align="center">'.$start_date .'</td>';
						echo '<td align="center">New </td>';
						echo '<td align="center">Pending From Q&A</td>';
					}
		else {
				echo '<td align="center">'.$last_date.'</td>';	
				while($sql_table_qry_result= mysql_fetch_array($sql_table_qry_rslt)) {
					$answered_by_id=$sql_table_qry_result['qa_id'];
				
				$sql_answered="SELECT * FROM `answer_qa_query`  where qa_id='$answered_by_id' and 	doc_user_dif='1'";
				$sql_answered_rslt = mysql_query($sql_answered);
				if(mysql_num_rows ($sql_answered_rslt)<=0) $answered_by="New";
				while($sql_answered_qry_result= mysql_fetch_array($sql_answered_rslt )) {
					$answered_by=$sql_answered_qry_result['answer_by'];
				}
				echo '<td align="center">'.$answered_by.'</td>';
					$dif=$sql_table_qry_result['doc_user_dif'];
					$status=$sql_table_qry_result['qa_status'];
					if($status=="close") {
						echo '<td align="center">Closed</td>';
					}
					else if($dif=="1") {
						echo '<td align="center">Pending From User</td>';
					}
					else {
					echo '<td align="center">Pending From Q&A</td>';
				}
			}
		}
				?>
				<td align="center" width="50"><a href="edit_counsellorq_a.php?q_a_id=<?php echo $row['q_id'];?>&q_a_status=doctor_pending"> <img src="admin/images/EditIcon.gif"  /></a></td>
                
				<?php
				echo "</tr>";
			}
			}
			
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>', '</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			if($check==0) {
			?><tr><td colspan="9" align="center"><b>No Records</b></td></tr>
            <?php	
			}
			?>
			<tr><td colspan='9' align='center'>
            <?php
            echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
            <?php } ?>
        </table></td>
</tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>