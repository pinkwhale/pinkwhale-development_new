<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Ask our Physician – Have health related questions? Get your queries answered today. 
	Ask our Physician/Specialist regarding your concern.">
	<meta name="keywords" content="Ask your physician, ask medical questions online, online physician, ask health questions, ask a physician,
	ask a doctor online, find a physician, talk to doctor online, ">
    <title>Ask our Physicians Online | pinkWhale Healthcare</title>
	<script src="js/jquery.min.js"></script>
    <?php include 'includes/include-css.php'; ?>
  </head>
	<?php include "includes/header.php" ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var speciality = $('#speciality').val();
		var city = $('#city').val();
		var sel = $('#dynamic_select').val();
		if(sel == ""  && speciality == "" && city == "Bangalore"){
		var select_type = "";
			$.ajax({
				url: 'our_services.php',
				data: {select_type:select_type,city:city},
				cache: false,
				error: function(e){
					alert(e);
				},
				success: function(response){
					// A response to say if it's updated or not
					//alert(response);
					$("#output1").html(response);
				}
			});
		}
		
		
		$('#sub').click(function(){
		var speciality1 = $('#speciality').val();
		var city1 = $('#city').val();
			$.ajax({
				url: 'our_services.php',
				data: {speciality1:speciality1,city1:city1},
				async: false,
				error: function(e){
					alert("error");
				},
				success: function(response){
					// A response to say if it's updated or not
					//alert(response);
					$("#output1").html(response);
				}
			});
	});

	})
	function saveChanges(object){
		var speciality = $('#speciality').val();
		var city = $('#city').val();
		var select_type = object.options[object.selectedIndex].value; 
		//alert(select_type);
		$.ajax({
			url: 'our_services.php',
			data: {select_type:select_type,speciality:speciality,city:city},
			cache: false,
			error: function(e){
				alert(e);
			},
			success: function(response){
				// A response to say if it's updated or not
				//alert(response);
				$("#output1").html(response);
			}
		});   
	}
	
	
	
	
	
	</script>
	<?php include "includes/pw_db_connect.php" ?>
	<?php include "includes/site_config.php"?>
	<link href="css/findadoctor.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients" data-sub-page="meet-our-doctors" data-pink-name="pink-followup"></div>
	<?php include "includes/menu-patients.php" ?>
		
	
	<div class="banner-bg banner-bg-meet-our-doctors">
		<div class="content-area hidden-xs hidden-sm">
			<div class="red-text"><i>Make the right choice for<br>your loved ones!</i></div>
		</div>
	</div>


	<div class="find-a-doctor-row">	
		<div class="content-area find-a-doctor-form-container">
			<div class="header">
				<img src="img/ourservices/findadoctor/pinkAppoint_FindADoctor_Icon_FindDoctor.png">
				Ask Our Physicians
			</div>			
			<div class="form-inline" style="margin-left:-5px;">
				<div class="form-group form-inline">
					<div class="input-group">
						<select name="speciality" id="speciality" class="form-control">
							<option value="" selected="selected" > -- Select Doctor Specialities -- </option>
							<?php // code added by Shoaib //
								$qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE blocked !='Y' and doc_specialities<>'' group by`doc_specialities`";
								$res = mysql_query($qry);
								while ($data = mysql_fetch_array($res)){
								echo "<option value='".$data['doc_specialities']."'>".$data['doc_specialities']."</option>"; 
								}
							?>					
						</select>										
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" name="city" id="city">
						<option value="Bangalore" selected="selected">  Bangalore  </option>									
						</select>									
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="dynamic_select" onchange="saveChanges(this);">
						<option value="" selected="selected">  -- Select Type --  </option>	
						<option value="phy" >  Ask our Physicians  </option>	
						<option  value="spe" > Ask our Specialists  </option>	
						<option  value="apt" > Book an Appointment  </option>	
						<option  value="sec" > Take a Second Opinion </option>	
						</select>									
					</div>
				</div>
							
				
				<div class="form-group">
					<button type="submit" name="submit" id="sub" class="btn pw-btn">Find Doctor</button>
				</div>										
			</div>
		</div>
	</div>
	<div id="output1"></div>
	<div class="footer-cover">
		<?php include "includes/footer.php" ?>
	</div>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>

<script type="text/javascript">
 /* $(document).ready(function(){
    $("a").click(function () {
      $(this).fadeTo("fast", .5).removeAttr("href");
    });
  });
*/
</script>

<!--script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script-->
