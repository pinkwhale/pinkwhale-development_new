 <?php
ob_start();
error_reporting(E_PARSE);
session_start();
?>
<?php
include "db_connect.php";

include "actions/encdec.php";

include 'site_config.php';


if (isset($_COOKIE['PinkWhale']) && $_SESSION['login'] != "Card") {

    $username = $_COOKIE['PinkWhale']['username'];

    $password_en = $_COOKIE['PinkWhale']['password'];

    $password = base64_decode($password_en);



    $query1 = "SELECT admin_name, admin_password FROM `login_user` where admin_name = '$username' and admin_password='$password'";

    $result1 = mysql_query($query1);



    if (mysql_num_rows($result1) > 0) {

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'admin';
    }



    $qry = "SELECT `user_name`,`user_id` FROM `user_details` WHERE `user_email`='$username' && `password`='$password_en'";

    $rslt = mysql_query($qry);

    if (mysql_num_rows($rslt) > 0) {

        while ($rs = mysql_fetch_array($rslt)) {

            $_SESSION['pinkwhale_id'] = $rs['user_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'user';
    }


    $qry1 = "SELECT `doc_name`,doc_id FROM `pw_doctors` WHERE `doc_email_id`='$username' && `password`='$password_en'";

    $rslt1 = mysql_query($qry1);

    if (mysql_num_rows($rslt1) > 0) {

        while ($rs = mysql_fetch_array($rslt1)) {

            $_SESSION['doctor_id'] = $rs['doc_id'];
        }

        $_SESSION['username'] = $username;

        $_SESSION['login'] = 'doctor';
    }
}
?>
 <!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="pinkWhale offers integrated online and telephone based personal healthcare services. 
	We are building the best network of health specialists, healthcare providers, and partners who are committed to providing
	health consultations by phone or online.">
	<meta name="keywords" content="About us, healthcare">
    <title>About Us | pinkWhale Healthcare</title>
	<script src="js/jquery.min.js"></script>
    <?php include 'includes/include-css.php'; ?>
  </head>

<?php //include "includes/start.php" ?>
<?php include "includes/site_config.php" ?>
	<link href="css/aboutus.css" media="all" rel="Stylesheet" type="text/css" />
	<?php include "includes/header.php" ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $site_url; ?>js/login.js"></script>
	<div class="menu-data" data-main-page="aboutus"></div>

	<?php include "includes/page-menu-start.php" ?>
		<li class="contents scrollLinks"><a  href="#who-we-are" class="menu-elements">WHO WE ARE</a></li>
		<!--li class="contents scrollLinks"><a  href="#our-team" class="menu-elements">OUR TEAM</a></li-->
		<li class="contents scrollLinks"><a  href="#pink-values" class="menu-elements"><i>pink</i>VALUES</a></li>
		<li class="contents scrollLinks"><a  href="#pink-vision" class="menu-elements"><i>pink</i>VISION</a></li>
		<!--li class="contents scrollLinks"><a href="" data-toggle="modal" data-target="#login-modal" class="login-button">Login</a></li-->
			
	
	<?php
             if (!isset($_SESSION['username']) || $_SESSION['login'] == 'Card') {
                    ?>		

                   <li class="contents scrollLinks"><a href="" data-toggle="modal" data-target="#login-modal" class="login-button upper-case" >Login</a></li>

			<?php
                } else if ($_SESSION['login'] == 'user') {			
					
                    ?>		
					
					   <li class="contents"><a href="<?php echo $site_url; ?>phr.php" >My Account</a></li>
					  <!--li class="contents"><a href="<?php echo $site_url; ?>cart.php"  >Shopping Cart</a></li-->
					   <li class="contents"><a href="<?php echo $site_url; ?>logout.php" class="login-button upper-case" >Logout</a></li>	

                    <?php
                } else if ($_SESSION['login'] == 'doctor') {
                    ?>

                     <li class="contents"><a href="<?php echo $site_url; ?>doc_phr.php" >My Account</a></li>
                     <li class="contents"><a href="<?php echo $site_url; ?>logout.php" class="login-button upper-case" >Logout</a></li>


                    <?php
                } else if ($_SESSION['login'] == 'admin') {
                    ?>

                    <li class="contents"><a href="<?php echo $site_url; ?>admin/pw_admin.php" >My Account</a></li>
                     <li class="contents"><a href="<?php echo $site_url; ?>logout.php"  class="login-button upper-case" >Logout</a></li>



                    <?php
                }
//..................................................................................................
                else if ($_SESSION['login'] == 'Counsellor' || $_SESSION['login'] == 'Doctor' || $_SESSION['login'] == 'Dietician' || $_SESSION['login'] == 'content_manager') {
                    ?>

                    <li class="contents"><a href="<?php echo $site_url; ?>logout.php" class="login-button upper-case" >Logout</a></li>


                    <?php
                }
//..................................................................................................	
                else {
                    ?>

                     <li class="contents"><a href="" data-toggle="modal" data-target="#login-modal" class="login-button upper-case" >Login</a></li>


                    <?php
                }
                ?>
	<?php include "includes/page-menu-end.php" ?>
	
	<div class="banner-bg banner-bg-about-us">
		<div class="content-area">
		</div>
	</div>
	
	<div id="who-we-are"></div>
	<div class="page-row row-bg-1 about-us" id="who-we-are">
		<div class="content-area">
			<div class="row-header">
				<img src="img/aboutus/AboutUs_Icon_WhoWeAre.png" class="row-title-image">
				<div class="row-title" >Who We Are</div>
			</div>
			<div class="row-description">
				pinkWhale is an e-Health company offering integrated online and telephone based personal healthcare 
				services. We are building the best network of health specialists, healthcare providers, and partners who 
				are committed to providing health consultations by phone or online.<br><br>
				We are a diverse, dynamic, and dedicated team of highly committed professionals who are passionate 
				about providing high quality, convenient access to healthcare.<br><br>
				The leadership team has a unique combination of healthcare, technology, and business experience 
				across industries. As experts, they have led long and distinguished careers in top multinational 
				organizations. 
			</div>
		</div>
	</div>

	<!--div id="our-team"></div>
	<div class="page-row gradient4-background about-us" id="our-team">
		<div class="content-area">
			<div class="row-header">
				<img src="img/aboutus/AboutUs_Icon_OurtTeam.png" class="row-title-image">
				<div class="row-title" >Our Team</div>
			</div>
			<div class="row-description">
				<div class="member-left col-xl-6 col-md-6 col-sm-6 col-xs-12">
					<div class="member-image">
						<img class="member-image" src="img/aboutus/AboutUs_Dr.Anita.png">
					</div>
					<div class="member-name">Anita Shet</div>
					<div class="member-designation">Chairman & Chief Executive Officer</div>
					<div class="member-description">
						Anita has over 15 years of experience 
						in semiconductor industry. She spent 
						over 14 years at Texas Instruments 
						where she was the worldwide product 
						manager of Multimedia Codec. She 
						has B.E in Electrical Engineering from 
						UVCE Bangalore, and a Diploma in 
						Computer Networks &amp; Applications 
						from Digital India Ltd.
					</div>

				</div>
				<div class="member-right col-xl-6 col-md-6 col-sm-6 col-xs-12">
					<div class="member-image">
						<img class="member-image" src="img/aboutus/AboutUs_Dr,Rekha.png">
					</div>
					<div class="member-name">Dr. Rekha B S.</div>
					<div class="member-designation">Chief Medical Officer</div>
					<div class="member-description">
						Dr. Rekha has practiced medicine in 
						leading hospitals in Bangalore. She 
						was a Consultant Ophthalmologist at 
						Mallya hospital, and at Mahaveer Jain 
						hospital prior to that. She is actively 
						involved in social organizations and 
						has participated in multiple eye 
						camps in rural areas. She has a MBBS 
						and a MS in Ophthalmology from 
						KIMS, and F.G.O from L.E.H.
					</div>
				</div>
			</div>
		</div>
	</div-->


	<div id="pink-values"></div>
	<div class="page-row  row-bg-2  about-us" id="pink-values">
		<div class="content-area">
			<div class="row-header">
				<img src="img/aboutus/AboutUs_Icon_PinkValues.png" class="row-title-image">
				<div class="row-title" ><i>pink</i>Values</div>
			</div>
			<div class="row-description">
				<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 aboutus-pinkvalues">
					<div class="col-xl-4 col-ms-4 col-sm-4 col-xs-4">
						<img src="img/aboutus/AboutUs_Icon_PinkValues_Trust.png" class="widget-image one">
						<div class="image-title">Trust</div>
					</div>
					<div class="col-xl-4 col-ms-4 col-sm-4 col-xs-4">
						<img src="img/aboutus/AboutUs_Icon_PinkValues_Simplicity.png" class="widget-image two">
						<div class="image-title">Simplicity</div>
					</div>
					<div class="col-xl-4 col-ms-4 col-sm-4 col-xs-4">
						<img src="img/aboutus/AboutUs_Icon_PinkValues_Quality.png" class="widget-image three">
						<div class="image-title">Quality</div>
					</div>
					<div class="clearfix"></div>
					<div class="description">
						We understand that the problem of convenient access to healthcare cannot be 
						solved by any single doctor, clinic, hospital, diagnostic lab, or care provider.<br><br>
						We believe strongly that health encompasses physical and emotional well-being 
						and we want to provide value to our customers in both of these areas. We are on a 
						mission to build the best health specialist network for easy access and 
						convenience of our customers.<br>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div id="pink-vision"></div>
	<div class="page-row row-bg-3 about-us last-row" id="pink-vision">
		<div class="content-area ">
			<div class="row-header">
				<img src="img/aboutus/AboutUs_Icon_PinkVision.png" class="row-title-image">
				<div class="row-title" ><i>pink</i>Vision</div>
			</div>
			<div class="row-description">
				Our goal is to give consumers and businesses quick, easy, and convenient access to the best health 
				care from wherever they are, using a state-of-the-art technology platform built on a proprietary software 
				framework.<br><br>
				We believe that it is possible to make it easier for patients to access the best health specialists, and for 
				healthcare providers to provide the best care to their patients. We are united by this vision. <br><br>
				pinkWhale aims to revolutionize the way healthcare is being delivered in India
			</div>
		</div>
	</div>

<script type="text/javascript">
function goTo(id){
		$('html,body').animate({
        	scrollTop: $(id).offset().top
    	}, "slow");
    	return false;
	}
	$("a[href=#who-we-are], a[href=#our-team], a[href=#pink-values], a[href=#pink-vision]").click(function(e){
		e.preventDefault();
		goTo($(this).attr("href"));
	});
</script>
	
	<div class="clearfix"></div>
	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
