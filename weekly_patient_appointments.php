<?php 
        error_reporting(E_PARSE); 
      session_start();
        include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
        {
                header("Location: index.php");
                exit();
        }
        else
        {
                $doctor_id=$_SESSION['doctor_id'];
                

        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cancel_appointment.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css" />
<script type="text/javascript" src="js/timepicker/rainbow.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/rainbow.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<style type="text/css">
	.ui-dialog { font-size: 11px; }
	body {
		font-family: Tahoma;
		font-size: 12px;
	}
	#question {
		width: 300px!important;
		height: 60px!important;
		padding: 10px 0 0 10px;
	}
	#question img {
		float: left;
	}
	#question span {
		float: left;
		margin: 20px 0 0 10px;
	}
        .ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
<script>
    
    function get_week_details(){
        
        var clinic = document.getElementById("clinic").value;
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='images/ajax-loader.gif' />");
        $('#report').load('get-weekly-appointment-details.php?clinic_id='+clinic+'&val=0&date=');
        $("#loading").fadeOut('slow');
    }
    
    function get_next_day(val,date){
        
        var clinic = document.getElementById("clinic").value;
        
        if(val==0){
            $('#next-id').html('<p><img src="images/ajax-loader_small.gif" /></p>');    
        }else{
            $('#pre-id').html('<p><img src="images/ajax-loader_small.gif" /></p>');    
        }
    
    
        $('#report').load('get-weekly-appointment-details.php?clinic_id='+clinic+'&val='+val+'&date='+date);
        
    }
    
</script>
<style>

    #loading { 
        width: 80%; 
        position: absolute;
    }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
    
</div>
<?php include 'header.php'; ?>
<?php

require_once('calendar/classes/tc_calendar.php');

?>
<?php 

$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag FROM `pw_doctors` where `doc_id`='$doctor_id' ";

$qry_rslt = mysql_query($qry);

while($result = mysql_fetch_array($qry_rslt))

	{

		$doc_category = $result['doc_category'];

		$doc_name = $result['doc_name'];

		$doc_id = $result['doc_id'];

		$doc_ctgry_flag=$result['doc_category_change_flag'];

	}

?>



<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="220" valign="top">

<div id="s90dashboardbg">

<!--    <img src="images/dots.gif" />

    <a href="doc_phr.php"><b>Dashboard</b></a></div>

<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php 
include 'doc_phr_left_menu.php'; 
$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>



<td width="748" valign="top" class="s90docphr">

<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">

<tr>

	<td width="280"><h1>Patient Appointment Summary</h1></td>

	<td width="528" bgcolor="#f1f1f1" align="right">    	

    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">

        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>

    </div>

    </td>

</tr>

</table>

<table height="30"></table>
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">            
    <tr>
        <td width="528"  align="center">
            <div id="clinic_dash">   
                <table border="0" cellpadding="0" cellspacing="1"  align="center" >                    
                    <tr>
                        <td>
                            <select name="clinic" id="clinic" onchange='get_week_details()'>
                            <option value="" selected="selected" disabled>-----select Clinic-----</option>
                            <option value="all"  >All</option>
                            <?php
                                $qry = "select c.clinic_id,name from clinic_details c inner join doctor_clinic_details d on c.clinic_id=d.clinic_id where doctor_id='$doctor_id' group by name union select c.clinic_id,name from clinic_details c inner join clinic_doctors_details d on c.clinic_id=d.clinic_id where doctor_id='$doctor_id' group by name";
                                $res = mysql_query($qry);
                                while ($data = mysql_fetch_array($res)){
                                    echo "<option value='".$data['clinic_id']."'>".$data['name']."</option>";                            
                                }                        
                            ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="loading"></div>
                <div id="report">
                    
                </div>               
                
            </div>
        </td>
    </tr>
</table>

    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>
</body></html>
