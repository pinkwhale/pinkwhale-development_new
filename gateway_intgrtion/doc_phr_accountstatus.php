<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
include("admin/includes/host_conf.php");
include("admin/includes/mysql.lib.php");
include('admin/includes/ps_pagination.php');
$obj=new connect;
?>
<?php
include 'header.php'; 
?>
<!-- header.......-->
<?php 
$qry= "SELECT doc_category,doc_name,doc_id FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
	}
?>
<?php 
$today_date=date("Y-m-d");
$arr = explode('-', $today_date);
if($arr[1]=="01")
{
	$month=1;
}
else if($arr[1]=="02")
{
	$month=2;
}
else if($arr[1]=="03")
{
	$month=3;
}
else if($arr[1]=="04")
{
	$month=4;
}
else if($arr[1]=="05")
{
	$month=5;
}
else if($arr[1]=="06")
{
	$month=6;
}
else if($arr[1]=="07")
{
	$month=7;
}
else if($arr[1]=="08")
{
	$month=8;
}
else if($arr[1]=="09")
{
	$month=9;
}
else if($arr[1]=="10")
{
	$month=10;
}
else if($arr[1]=="11")
{
	$month=11;
}
else if($arr[1]=="12")
{
	$month=12;
}
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<?php include "doc_phr_left_menu.php"; ?></td>
<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="260"><h1>Account Status</h1></td>
	<td width="488" bgcolor="#f1f1f1" align="right">    	<div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwheale ID <?php echo $doc_id ; ?>
        </div>
    </td>
</tr>
</table>
<table height="30"></table>

<form name="account_form" id="account_form" action="doc_phr_accountstatus.php" method="post" >
<table width="650" border="0">
<tr><td width="116">
	<select  name="select_year" id="select_year" > 
    	<option value="">- Select Year -</option>
        <?php 
			if($arr[0] >= 2011)
			{
			for($i=2011;$i==$arr[0];$i++)
				{ ?>
				 <option value="<?php echo $i;?>"><?php echo $i; ?></option>
			<?php 
			}
			}
		?>
   </select>
</td>
      <td width="524"><input type="submit" name="get_details" id="get_details" value="Get Details" /></td>  
        </tr>
      </table>
</form>
<?php 
if($_POST['select_year'])
{
	$year=$_POST['select_year'];
	?>
    <script type="text/javascript">
		sel_year='<?php echo $_POST['select_year']; ?>';
		document.getElementById('select_year').value=sel_year;
		document.getElementById('select_year').selected=true;
	</script>
    <?php
}
else
{
	$year=$arr[0];
}
$qry= "SELECT doc_category FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
	}
?>
<!------------------/////////          Specialist Grid      /////////////---------------------->
<?php
if ($doc_category=='Specialist')
{
	if($year <= $arr[0])
{
?>	<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform_account'>
        <tr bgcolor='#f3f7f8'>
            <td align='left' width="175"><strong>Month-Year</strong></td>
            <td align='left' width="175"><strong>Online Consultations</strong></td>
            <td align='left' width="175"><strong> Gross Earnings(INR)</strong></td>
        </tr>
    </table>
<?php

$recordExists = false;
	
if($year==$arr[0])
{	 
for($i=1;$i<=$month; $i++)
{
	$qry="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_spe_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
	<?php
		$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
			/*	echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['1_email_session_charge'];
				$usage_session =$row['SUM(online_spe_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";	
				$recordExists = true;
			}
		}
		}
		}
if($year < $arr[0])
{	 
for($i=1;$i<=12; $i++)
{
	$qry="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_spe_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
	<?php
		$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
				/*echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['1_email_session_charge'];
				$usage_session =$row['SUM(online_spe_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
				$recordExists = true;	
			}
		}
		}
		}
		
		if(!$recordExists)
		echo htmlForNoRecords("700", false);
		?>	 
	</table>
	</table>
<?php
}
	
}
?>


<!-- Expert Gride-->
<?php
if ($doc_category=='Expert')
{
	if($year <= $arr[0])
	{
?>
<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform_account'>
        <tr bgcolor='#f3f7f8'>
            <td align='left' width="175"><strong>Month-Year</strong></td>
            <td align='left' width="175"><strong>Online Consultations</strong></td>
          <td align='left' width="175"><strong> Gross Earnings(INR)</strong></td>
        </tr>
    </table>
<?php	
if($year==$arr[0])
{	 
for($i=1;$i<=$month; $i++)
{	$qry="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_exp_consultation_reports.`entered_date`)";
	?>
		
        <table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
	
	<?php
		$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
				/*echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['exprt_email_1_session'];
				$usage_session =$row['SUM(online_exp_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
				$recordExists = true;
			}
		}
		}
		}
if($year < $arr[0])
{	 
for($i=1;$i<=12; $i++)
{	$qry="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_exp_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;" >
	<?php
		$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
				/*echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['exprt_email_1_session'];
				$usage_session =$row['SUM(online_exp_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs.0 /-</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
				$recordExists = true;
			}
		}
		}	
		}
		if(!$recordExists)
			echo htmlForNoRecords("700", false);
		?>
</table>
<?php
}
}
?>

<!--Counsellor-->
<?php
if ($doc_category=='Counsellor')
{
if($year <= $arr[0])
	{
?>
<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform_account'>
        <tr bgcolor='#f3f7f8'>
            <td align='left' width="175"><strong>Month-Year</strong></td>
            <td align='left' width="175"><strong>Online Consultations</strong></td>
          <td align='left' width="175"><strong> Gross Earnings(INR)</strong></td>
        </tr>
    </table>
<?php	
if($year==$arr[0])
		 
{	 
for($i=1;$i<=$month; $i++)
{
	$qry="SELECT packages.1_email_session_charge,online_coun_consultation_reports.usage_session,online_coun_consultation_reports.entered_date,SUM(online_coun_consultation_reports.usage_session) FROM packages INNER JOIN online_coun_consultation_reports ON packages.doctor_id=online_coun_consultation_reports.counsellor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_coun_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
	<?php
		$qry_rslt = mysql_query($qry);
	if(mysql_fetch_row($qry_rslt)<1)
	{
			$entered_date=$row['entered_date'];
			$arr[1]=$i;
			$arr[0]=$year;
			$arr[2]=01;
			$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
			$gdate1=strtotime($date);
			$final_date1=date(" M Y", $gdate1);
			/*echo "<tr bgcolor='#ffffff'>";
			echo "<td width='175'>$final_date1</td> ";
			echo "<td width='175'></td> ";
			echo "<td width='175'></td> ";
			echo "</tr>";*/
	}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['1_email_session_charge'];
				$usage_session =$row['SUM(online_coun_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
			}
		}
		}
		}
if($year < $arr[0])
{	 
for($i=1;$i<=12; $i++)
{
	$qry="SELECT packages.1_email_session_charge,online_coun_consultation_reports.usage_session,online_coun_consultation_reports.entered_date,SUM(online_coun_consultation_reports.usage_session) FROM packages INNER JOIN online_coun_consultation_reports ON packages.doctor_id=online_coun_consultation_reports.counsellor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_coun_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial;font-size:15px;">
	<?php
		$qry_rslt = mysql_query($qry);
	if(mysql_fetch_row($qry_rslt)<1)
	{
			$entered_date=$row['entered_date'];
			$arr[1]=$i;
			$arr[0]=$year;
			$arr[2]=01;
			$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
			$gdate1=strtotime($date);
			$final_date1=date(" M Y", $gdate1);
			/*echo "<tr bgcolor='#ffffff'>";
			echo "<td width='175'>$final_date1</td> ";
			echo "<td width='175'></td> ";
			echo "<td width='175'></td> ";
			echo "</tr>";*/
	}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$email_session_charge=$row['1_email_session_charge'];
				$usage_session =$row['SUM(online_coun_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($email_session_charge*$usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
			}
		}
		}
		}
		?>
</table>
<?php
}
}

$recordExists = false;	

if ($doc_category=='Specialist/Expert')
{
	if($year <= $arr[0])
	{
?>
<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform_account'>
        <tr bgcolor='#f3f7f8'>
            <td align='left' width="175"><strong>Month-Year</strong></td>
            <td align='left' width="175"><strong>Online Consultations</strong></td>
          <td align='left' width="175"><strong> Gross Earnings(INR)</strong></td>
        </tr>
    </table>
<?php	
if($year==$arr[0])
{	 
for($i=1;$i<=$month; $i++)
{
/*	$qry="SELECT packages.1_email_session_charge,packages.exprt_email_1_session,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session) FROM packages 
		INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id 
		INNER JOIN online_exp_consultation_reports ON online_spe_consultation_reports.doctor_id =online_exp_consultation_reports.doctor_id
		&& packages.doctor_id='$doctor_id' && month(online_exp_consultation_reports.`entered_date`)='$i' && month(online_spe_consultation_reports.`entered_date`)='$i' && year(online_exp_consultation_reports.`entered_date`)='$year' && year(online_spe_consultation_reports.`entered_date`)='$year'
		group by month(online_exp_consultation_reports.`entered_date`),month(online_spe_consultation_reports.`entered_date`)";*/
		
		
	$expQry="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_exp_consultation_reports.`entered_date`)";	
		
	$speQry="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && month(entered_date)='$i' && year(entered_date)='$year' group by month(online_spe_consultation_reports.`entered_date`)";	
		
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
      <?php
	     	$total_exp_consultation_charge=0;
	  		$exp_rslt = mysql_query($expQry);
			while($row = mysql_fetch_assoc($exp_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$exp_email_session_charge=$row['exprt_email_1_session'];
				$exp_usage_session = $row['SUM(online_exp_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$total_exp_consultation_charge = $total_exp_consultation_charge + ($exp_email_session_charge*$exp_usage_session);
			}
			
			$total_spe_consultation_charge=0;
			$spe_rslt = mysql_query($speQry);
			while($row = mysql_fetch_assoc($spe_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$spe_email_session_charge=$row['1_email_session_charge'];
				$spe_usage_session =$row['SUM(online_spe_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$total_spe_consultation_charge = $total_spe_consultation_charge + ($spe_email_session_charge*$spe_usage_session);
			}

			if($total_exp_consultation_charge > 0 || $total_spe_consultation_charge > 0)
			{
				$total_charge = $total_exp_consultation_charge + $total_spe_consultation_charge;
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. ".$total_charge."/-</td> ";
				echo "<td width='175'>Rs.".$total_charge. "/- </td> ";
				echo "</tr>";
				$recordExists = true;	
			}
	  ?>  
        
	<?php
		/*$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
				/*echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		/*}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$spe_email_session_charge=$row['1_email_session_charge'];
				$exp_email_session_charge=$row['exprt_email_1_session'];
				$exp_usage_session =$row['SUM(online_exp_consultation_reports.usage_session)'];
				$spe_usage_session =$row['SUM(online_spe_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($spe_email_session_charge*$spe_usage_session)+($exp_email_session_charge*$exp_usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
			}
		}*/
		}
		}
		
if($year < $arr[0])
{	 
for($i=1;$i<=12; $i++)
{
	$qry="SELECT packages.1_email_session_charge,packages.exprt_email_1_session,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session) FROM packages 
		INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id 
		INNER JOIN online_exp_consultation_reports ON online_spe_consultation_reports.doctor_id =online_exp_consultation_reports.doctor_id
		&& packages.doctor_id='$doctor_id' && month(online_exp_consultation_reports.`entered_date`)='$i' && month(online_spe_consultation_reports.`entered_date`)='$i' && year(online_exp_consultation_reports.`entered_date`)='$year' && year(online_spe_consultation_reports.`entered_date`)='$year'
		group by month(online_exp_consultation_reports.`entered_date`),month(online_spe_consultation_reports.`entered_date`)";
	?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' style="font-family: Arial; font-size: 15px;">
	<?php
		$qry_rslt = mysql_query($qry);
		if(mysql_fetch_row($qry_rslt)<1)
		{
				$entered_date=$row['entered_date'];
				$arr[1]=$i;
				$arr[0]=$year;
				$arr[2]=01;
				$date =$arr[2].'-'.$arr[1].'-'.$arr[0];
				$gdate1=strtotime($date);
				$final_date1=date(" M Y", $gdate1);
				/*echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'></td> ";
				echo "<td width='175'></td> ";
				echo "</tr>";*/
		}
		else
		{
			$qry_rslt = mysql_query($qry);
			while($row = mysql_fetch_assoc($qry_rslt)) 
			{
				$entered_date=$row['entered_date'];
				$spe_email_session_charge=$row['1_email_session_charge'];
				$exp_email_session_charge=$row['exprt_email_1_session'];
				$exp_usage_session =$row['SUM(online_exp_consultation_reports.usage_session)'];
				$spe_usage_session =$row['SUM(online_spe_consultation_reports.usage_session)'];
				$gdate1=strtotime($entered_date);
				$final_date1=date(" M Y", $gdate1);
				$tele_consultation_charge=0;
				$total_email_consultation_charge = ($spe_email_session_charge*$spe_usage_session)+($exp_email_session_charge*$exp_usage_session);
				$total_charge= $total_charge + $total_email_consultation_charge;
				$tds=$total_charge+$tele_consultation_charge;
				
				echo "<tr bgcolor='#ffffff'>";
				echo "<td width='175'>$final_date1</td> ";
				echo "<td width='175'>Rs. $total_email_consultation_charge/-</td> ";
				echo "<td width='175'>Rs.$total_email_consultation_charge/- </td> ";
				echo "</tr>";
				$recordExists = true;	
			}
		}
		}
		}
?>
</table>
<?php
		if(!$recordExists)
			echo htmlForNoRecords("700", false);
}
}
?>

</table>


<?php 
include 'footer.php'; ?>

</body></html>
