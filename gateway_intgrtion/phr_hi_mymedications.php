<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<?php
include 'header.php'; ?>
<!-- header.......-->
<?php
include("admin/includes/host_conf.php");
include("admin/includes/mysql.lib.php");
include('admin/includes/ps_pagination.php');
$obj=new connect;
?>
<script type="text/javascript">
function chage_period_usage()
{
	var period_usage=document.getElementById('period_usage').value;
	if (period_usage=='Other than These')
	{
		document.getElementById("other_than_these").style.display="block";
	}
	 
	 if (period_usage!='Other than These')
	{
		document.getElementById("other_than_these").style.display="none";
		
	}
}


function changMedicineDiv()
{
		document.getElementById("medication_save").style.display="block";
		document.getElementById("medicine_listing").style.display="none";
		document.getElementById("cancel").style.display="none";
}
</script>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="220" valign="top">
<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<?php include 'user_left_menu.php'; ?>
</td>

<td width="748" valign="top" class="s90phrcontent">
<?php include("name_card_no.php");?>
<table>
	<tr>
		<td height='10px'></td>
	</tr>
</table>

<table width="700" cellspacing="0" cellpadding="0" class="s90dbdtbls" style="margin-top:20px">
<tr>
		<td>
			<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;margin-left:20px;width:250px;">My Medications</span>
			<div style='float:right;margin-top:25px;'>
				<input type="button" name="cancel" id="cancel" value="Add Medications" onclick="changMedicineDiv()"/>
			</div>
		</td>

  </tr>
  <tr><td >
  <div id="medicine_listing" style="display:block;">
<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform tbl'>
<tr><th colspan="7" style="background-image: url(images/register_hbg.jpg); background-repeat: repeat-x;color:#e70976;">My Medications :</th></tr>
<!-- .......... Grid Listing not specified where condition.......-->	

<?php		
		// Display all the data from the table 
			$sql="SELECT * FROM my_medication WHERE `pwcard_no`='$user_id' order by `medication_id` DESC " ;
			$obj->query($sql);	
			$pager = new PS_Pagination($dbcnx, $sql, 10, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
				if(!$rs)
				{
					echo htmlForNoRecords("700", true, "Your medication records are empty");
				}
				else{
			?>
			<tr bgcolor='#f3f7f8'>
                <td align='left'><strong>Medicine Details</strong></td>
                <td align='left'><strong>Complaint</strong></td>
                <td align='left'><strong>Dosage</strong></td>
                <td align='left'><strong> Medications Schedule</strong></td>
                <td align='left'><strong> Period</strong></td>
                <td align='left'><strong> Remarks</strong></td>
                <td align='left'><strong> Date</strong></td>
            </tr>
	
			<?php	
			while($row = mysql_fetch_assoc($rs)) 
			{
				$imf_flag=0;
				$medication_id=$row['medication_id'];
				$name_of_medicine=$row['name_of_medicine'];
				$complaint=$row['complaint'];
				$dosage =$row['dosage'];
				$period_of_medication =$row['period_of_medication'];
				$period_of_usage =$row['period_of_usage'];
				$medication_remarks =$row['medication_remarks'];
				$date = $row['entered_date'];
				
				echo "<tr bgcolor='#ffffff'>";
				?>
                <td valign="top"><a href="edit-medication.php?id=<?php echo $medication_id ?>" style="text-decoration:none;"> <?php echo $name_of_medicine ; ?></a></td>
                
                <?php
				echo "<td valign='top'>$complaint </td> ";
				echo "<td valign='top'> $dosage</td> ";
				echo "<td valign='top'>$period_of_usage </td> ";
				echo "<td valign='top'>$period_of_medication </td> ";
				echo "<td valign='top'>$medication_remarks </td> ";
				echo "<td valign='top'>$date</td> ";
				echo "</tr>";
			}
				$ps1 = $pager->renderFirst();
				$ps2 = $pager->renderPrev();
				$ps3 = $pager->renderNav('<span>','</span>');
				$ps4 = $pager->renderNext();
				$ps5 = $pager->renderLast();
			
			
			?>
            <tr><td colspan='6' align='center'>
            
            <?php
                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr>"; 
		}?>
        </table>
		</div>
		<div id="medication_save" style="display:none;">
<form name="medication_form" id="medication_form" action="actions/add_medicine.php" method="post">
    <table width="668" border="0" cellspacing="10" cellpadding="2" align="center" style="border:1px solid #CCC;" >
        <tr>
            <td colspan="3" bgcolor="#DBEEF9"><div class="phr_usr_dtils">ADD MEDICATIONS</div></td>
        </tr>
        <input type="hidden" name="pw_no" id="pw_no" value="<?php echo $user_id; ?>" />
        <tr>
            <td width="198" colspan="1" align="right"><div class="label">Name of Medicine :</div></td>
            <td width="161" colspan="1"><input type="text" name="name_of_medicine" id="name_of_medicine" /></td>
        </tr>
        <tr>
            <td colspan="1" align="right"><div class="label">Complaint :</div></td>
            <td colspan="1" ><input type="text" name="complaint" id="complaint" /></td>
        </tr>
        <tr>
            <td colspan="1" align="right"><div class="label">Dosage :</div></td>
            <td colspan="1"><input type="text" name="dosage" id="dosage" /></td>
        </tr>
        <tr>
            <td colspan="1" align="right"><div class="label">Medication Schedule :</div></td>
            <td colspan="1">
   
            <select  name="period_usage" id="period_usage" class="registetextbox"  onchange="chage_period_usage()"> 
			   <option value="" >-Select Period-</option>
               <option value="Morning-Afternoon-night">Morning-Afternoon-night</option>
               <option value="Mornig Only">Mornig Only</option>
               <option value="Night Only">Night Only</option>
               <option value="Every 4 Hours">Every 4 Hours</option>
                <option value="Other than These">Other than These</option>
  		</select>

            </td>
        </tr>
         <tr><td colspan="3">
         

<!--        Other than these div            -->
        <div id="other_than_these" style="display:none;">
       
        <table width="630">
        <tr><td width="206">&nbsp;</td>
        <td width="412"><input type="text" name="other_than" id="other_than" /></td>
        </tr>
        </table>
         </div>
<!--         End Div-->
        </td></tr>
          <tr>
            <td colspan="1" align="right"><div class="label">Period Of Medication :</div></td>
            <td colspan="1"><input type="text" name="period_of_medication" id="period_of_medication" /></td>
        </tr>
    
           <tr>
            <td colspan="1" align="right"><div class="label">Date :</div></td>
            <td colspan="1">
            <?php
	  $myCalendar = new tc_calendar("last_occured", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d'), date('m'), date('Y'));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?>
        </td>
        </tr>
   
        
         <tr>
            <td colspan="1" align="right"><div class="label">Remarks :</div></td>
            <td colspan="1"><textarea name="medication_remarks" id="medication_remarks"></textarea></td>
        </tr>
        
        
        <tr>
        	<td>&nbsp;</td>
            <td align="right"><input type="submit" name="medication_save" id="medication_save" value="Save"  />
            <a href="phr_hi_mymedications.php"><input type="button" name="medication_cancel" id="medication_cancel" value="Cancel" /></a></td>
        <td>&nbsp;</td>
        </tr>
    </table>
</form>
</div>
</td></tr></table>
</td></tr></table>

<script type="text/javascript">
document.getElementById("menu_text4").style.fontWeight="bold";
document.getElementById("sub_menu2").style.fontWeight="bold";
</script>
<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
