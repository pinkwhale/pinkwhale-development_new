<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']) || $_SESSION['login']!='user')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>pinkwhalehealthcare</title>
		<meta name="description" content="pinkwhalehealthcare">
		<link href="css/designstyles.css" rel="stylesheet" type="text/css">
		<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
		<script language="JavaScript">
			<!--
			function submit_form()
			{
				document.getElementById('email_cnsltion_form').submit();
			}
			-->
		</script>

	<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
	<body>
		<!-- header.......-->
		<?php include 'header.php'; ?>
		<!-- header.......-->
		<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
			<tr>
				<td width="220" valign="top">
					<div id="s90dashboardbg">
						<img src="images/dots.gif" />
						<a href="phr.php"><font color="#ea0977"><strong>My Account</strong></font></a>
					</div>
					<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
					<?php include 'user_left_menu.php'; ?>
				</td>
				<td width="115" valign="top" class="s90phrcontent">
					<?php include("name_card_no.php");?>
					<table>
						<tr>
							<td height='10px'></td>
						</tr>
						<tr>
							<td height="40">
								<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-left:20px;">Account Status</span>
							</td>
							<?php
							if(isset($_SESSION['saved_msg']) && $_SESSION['saved_msg']=='1')
							{ 
							?>
							<td colspan="4" >
								<div style="font-family:Arial; font-size:12px; color:#F00;">Your order has been processed.</div>
							</td>
							<?php
							unset($_SESSION['saved_msg']); 
							}
							?>
						</tr>
					</table>
					<table height="30" width="580" cellspacing="0" cellpadding="0" class="s90dbdtbls_drdetailstbl tblWithBorder" >
						
						<tr bgcolor="#CCCCCC">
							<td width="250" align="left"><div class="phr_usr_dtils"><span style="color:#000000;margin-left:10px;">My Services</span></div></td>
							<td width="120" align="center"><div class="phr_usr_dtils"><span style="color:#000000">Available</span></div></td>
							<td width="120" align="center"><div class="phr_usr_dtils"><span style="color:#000000">Purchased</span></div></td>
							<td width="120" align="center"><div class="phr_usr_dtils"><span style="color:#000000">Used</span></div></td>
							<td width="120">&nbsp;</td>
						</tr>
						<?php
							$qry1   = "SELECT `phy_balance`,`phy_usage` FROM `online_cnsultn_credit_phy_coun` WHERE `user_id`='$user_id' ";
							$qry_rslt1 = mysql_query($qry1);
							while($result1 = mysql_fetch_array($qry_rslt1))
							{
								$phy_bal=$result1['phy_balance'];
								$phy_usage=$result1['phy_usage'];
							}
							$phy_purchase=$phy_bal+$phy_usage;
						?>
						<tr class='trMyaccount'>
							<td align="left"><div class="phr_usr_dtils">&nbsp; Consult a Specialist -Online</div></td>
							<td align="center"><div id="total_coun_bal" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center"><div id="total_coun_purchase" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center"><div id="total_coun_usage" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center">
							<!--<input type="image" src="images/phr_clickhere91.jpg"/>-->
							</td>
						</tr>
						<?php
						$total_spe_bal=0;
						$total_spe_usage=0;
						$qry2   = "SELECT `spe_doctor_id`,`spe_balance`,`spe_usage`,pw_doctors.doc_name FROM `online_cnsultn_credit_spe_exp` INNER JOIN pw_doctors ON `online_cnsultn_credit_spe_exp`.`spe_doctor_id`=pw_doctors.doc_id && `user_id`='$user_id' ";
						$qry_rslt2 = mysql_query($qry2);
						while($result2 = mysql_fetch_array($qry_rslt2))
						{
							$spe_bal=$result2['spe_balance'];
							$spe_usage=$result2['spe_usage'];
							$total_spe_bal=$total_spe_bal+$spe_bal;
							$total_spe_usage=$total_spe_usage+$spe_usage;
							$spe_purchase=$spe_bal+$spe_usage;
							?>
							<tr bgcolor="#FFFFFF">
								<td align="left"><div style="font-family:Arial,Verdana; font-size:12px;">&nbsp; <?php echo $result2['doc_name']; ?></div></td>
								<td align="center"><?php echo $spe_bal; ?></td>
								<td align="center"><?php echo $spe_purchase; ?></td>
								<td align="center"><?php echo $spe_usage; ?></td>
								<td align="center">
									<form method='post' action='patient_new_email_consultation.php'>
										<input type='hidden' name='doc_id' value='<?php echo $result2['spe_doctor_id']; ?>'/>
										<input type="image" src="images/phr_clickhere91.jpg"/>
									</form>
								</td>
							</tr>
						   <?php
						}
						
						$phy_purchase=$phy_bal+$phy_usage;
						$total_spe_purchase=$total_spe_usage+$total_spe_bal;
						?>
						<script type="text/javascript">
							document.getElementById("total_coun_bal").innerHTML	=<?php echo $total_spe_bal; ?> ;
							document.getElementById("total_coun_purchase").innerHTML=<?php echo $total_spe_purchase; ?>;
							document.getElementById("total_coun_usage").innerHTML=<?php echo $total_spe_usage; ?> ;
						</script>
	
						<?php
						$tele_total_spe_bal=0;
						$tele_total_spe_usage=0;
						$qry12   = " ";
						$qry_rslt12 = mysql_query($qry12);
						while($result12 = mysql_fetch_array($qry_rslt12))
						{
							$tele_spe_bal=$result12['spe_balance'];
							$tele_spe_usage=$result12['spe_usage'];
							$tele_total_spe_bal=$tele_total_spe_bal+$tele_spe_bal;
							$tele_total_spe_usage=$tele_total_spe_usage+$tele_spe_usage;
							$tele_spe_purchase=$tele_spe_bal+$tele_spe_usage;
						}
						$phy_purchase=$phy_bal+$phy_usage;
						$total_spe_purchase=$total_spe_usage+$total_spe_bal;
						?>
						<script type="text/javascript">
							document.getElementById("tele_total_coun_bal").innerHTML	=<?php echo 0; ?> ;
							document.getElementById("tele_total_coun_purchase").innerHTML=<?php echo 0; ?>;
							document.getElementById("tele_total_coun_usage").innerHTML=<?php echo 0; ?> ;
						</script>
						<?php
						$qry3   = "SELECT `coun_balance`,`coun_usage` FROM `online_cnsultn_credit_phy_coun` WHERE `user_id`='$user_id' ";
						$qry_rslt3 = mysql_query($qry3);
						while($result3 = mysql_fetch_array($qry_rslt3))
						{
							$coun_bal=$result3['coun_balance'];
							$coun_usage=$result3['coun_usage'];
						}
						$coun_purchase=$coun_bal+$coun_usage;

						$tele_coun_bal=0;
						$tele_coun_purchase =0;
						$tele_coun_usage=0;
						?>
						<tr>
							<td height='10px'></td>
						</tr>
						<tr class='trMyaccount'>
							<td align="left"><div class="phr_usr_dtils">&nbsp; Get a Second Opinion</div></td>
							<td align="center"><div id="total_exp_bal" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center"><div id="total_exp_purchase" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center"><div id="total_exp_usage" style="font-family:Arial,Verdana; font-weight:bold; font-size:12px;"></div></td>
							<td align="center">&nbsp;</td>
						</tr>
						<?php
						$total_spe_bal=0;
						$total_spe_usage=0;
						$qry4   = "SELECT `exp_doctor_id`,`exp_balance`,`exp_usage`,pw_doctors.doc_name FROM `online_cnsultn_credit_spe_exp` INNER JOIN pw_doctors ON `online_cnsultn_credit_spe_exp`.`exp_doctor_id`=pw_doctors.doc_id && `user_id`='$user_id' ";
						$qry_rslt4 = mysql_query($qry4);
						while($result4 = mysql_fetch_array($qry_rslt4))
						{
							$exp_bal=$result4['exp_balance'];
							$exp_usage=$result4['exp_usage'];
							$total_exp_bal=$total_exp_bal+$exp_bal;
							$total_exp_usage=$total_exp_usage+$exp_usage;
							$exp_purchase=$exp_bal+$exp_usage;
							?>
							<tr bgcolor="#FFFFFF">
								<td align="left" ><div style="font-family:Arial,Verdana; font-size:12px;">&nbsp; <?php echo $result4['doc_name']; ?></div></td>
								<td align="center"><?php echo $exp_bal; ?></td>
								<td align="center"><?php echo $exp_purchase; ?></td>
								<td align="center"><?php echo $exp_usage; ?></td>
								<td align="center">
									<form method='post' action='patient_new_exp_email_consultation.php'>
										<input type='hidden' name='doc_id' value='<?php echo $result4['exp_doctor_id']; ?>'/>
										<input type="image" src="images/phr_clickhere91.jpg"/>
									</form>
								</td>
							</tr>
							<?php
						}
						$exp_purchase=$exp_bal+$exp_usage;
						$total_exp_purchase=$total_exp_usage+$total_exp_bal;
						?>
						<script type="text/javascript">
							document.getElementById("total_exp_bal").innerHTML	=<?php echo $total_exp_bal; ?> ;
							document.getElementById("total_exp_purchase").innerHTML=<?php echo $total_exp_purchase; ?>;
							document.getElementById("total_exp_usage").innerHTML=<?php echo $total_exp_usage; ?> ;
						</script>
						<tr>
							<td height='10px'></td>
                         <tr bgcolor="#cccccc">
							<td align="left"><div class="phr_usr_dtils">&nbsp; </div></td>
							<td></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td colspan="2" align="center"><a href="card_topup.php"><img src="images/card_topup.jpg" width="88" height="21" /></a></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height='15px'></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
				<!--    specialist Email Consultation  -->
					<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;margin-left:20px;">On Going Consultations</span>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left">
					<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" >
						<tr><th>Specialist Online Consults</th></tr>
						<tr><td class="s90dbdtbls_drname"></td></tr>
						<tr>
							<td class="s90dbdtbls_drdetails">
    						<?php
							$qry6   = "SELECT max(mail_serial_no),min(mail_serial_no) FROM `pw_consultation_emails` WHERE `pw_card_id`='$user_id' group by `consultation_no` ";
							$qry_rslt6 = mysql_query($qry6);
							$recordsAvailbale = false;
							
						
								while($result6= mysql_fetch_array($qry_rslt6))
								{
									$max_serial_no=$result6['max(mail_serial_no)'];
									$min_serial_no=$result6['min(mail_serial_no)'];
									$qry17   = "SELECT `complaint` FROM `pw_consultation_emails` WHERE `mail_serial_no`='$min_serial_no' ";
									$qry_rslt17 = mysql_query($qry17);
									while($result17 = mysql_fetch_array($qry_rslt17))
									{
										$complaint=$result17['complaint'];
									}

									$qry7   = "SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`='$max_serial_no' && `mail_status`!='closed' ";
									$qry_rslt7 = mysql_query($qry7);
									
									while($result7 = mysql_fetch_array($qry_rslt7))
									{
										$recordsAvailbale = true;
										$gdate1=strtotime($result7['entered_date']);
										$final_date1=date("d M Y", $gdate1);
										?>
										<table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
											<tr>
												<td width="180"><?PHP echo $result7['doctor_name']; ?></td>
												<td width="100"><?PHP echo $final_date1; ?></td>
												<td width="140"><?PHP echo $complaint; ?></td>
												<td width="120">   
													<form method="post" action="patient_email_consultation.php" name="email_cnsltion_form"> 
														<input type="hidden" name="cnsltion_id" value="<?php echo $result7['consultation_no']; ?>"  />
														<input type="image" src="images/phr_clickhere91.jpg" onclick="submit_form();"  />
													</form>
												</td>
											</tr>
										</table>
								<?php 
									} 
								}
							
							if(!$recordsAvailbale) echo htmlForNoRecords("580" ,false);
							?>   
							</td>     
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
				<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" >
					<tr><th>Expert Online Consults</th></tr>
					<tr><td class="s90dbdtbls_drname"></td></tr>
					<tr>
						<td class="s90dbdtbls_drdetails">
						<?php
						$qry8   = "SELECT max(mail_id),min(mail_id)  FROM `pw_expert_consultation_emails` WHERE `pw_card_id`='$user_id' group by `consultation_id` ";
						$qry_rslt8 = mysql_query($qry8);
						
						$recordsAvailbale = false;
							while($result8 = mysql_fetch_array($qry_rslt8))
							{
								$max_serial_no=$result8['max(mail_id)'];
								$min_serial_no=$result8['min(mail_id)'];
							
								$qry19   = "SELECT complaint FROM `pw_expert_consultation_emails` WHERE `mail_id`='$min_serial_no' ";
								$qry_rslt19 = mysql_query($qry19);
								while($result19 = mysql_fetch_array($qry_rslt19))
								{
									$complaint1=$result19['complaint'];
								}
								
								$qry9   = "SELECT * FROM `pw_expert_consultation_emails` WHERE `mail_id`='$max_serial_no' && `mail_status`!='closed' ";
								$qry_rslt9 = mysql_query($qry9);
								while($result9 = mysql_fetch_array($qry_rslt9))
								{
									$recordsAvailbale = true;
									$gdate2=strtotime($result9['entered_date']);
									$final_date2=date("d M Y", $gdate2);
									?>
									<table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
										<tr>
											<td width="180"><?PHP echo $result9['doctor_name']; ?></td>
											<td width="100"><?PHP echo $final_date2; ?></td>
											<td width="140"><?PHP echo $complaint1; ?></td>
											<td width="120">
												<form method="post" action="patient_exp_email_consultation.php" name="email_cnsltion_form"> 
													<input type="hidden" name="cnsltion_id" value="<?php echo $result9['consultation_id']; ?>"  />
													<input type="image" src="images/phr_clickhere91.jpg" onclick="submit_form();"  />
												</form>
											</td>
										</tr>
									</table>
							<?php 
								} 
							}
						if(!$recordsAvailbale) echo htmlForNoRecords("580" ,false);
					
						?>        
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- /////////////////////////     end expert  ////////////////  -->
		<!-- /////////////////////////    Counsellor  ////////////////////// -->
		
	</td>
</tr>
</table>
<!-- footer -->
<?php
include 'footer.php'; ?>
</body>
</html>
