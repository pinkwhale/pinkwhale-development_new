<link href="css/front.css" media="screen, projection" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/header_colourchange.js"></script>
<script type="text/javascript" src="js/login.js"></script>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px">
<tr><td width="298" rowspan="2"><a href="index.php"><img src="images/pinkwhale_logo.jpg" width="294" height="104" /></a></td>
<td width="364" height="54"><div id="s90social"><a href="http://blog.pinkwhalehealthcare.com/"><img src="images/feed.png" /></a><a href="http://www.facebook.com/pages/PinkWhale-Healthcare/151598318209476"><img src="images/facebook.png" /></a><a href="http://twitter.com/pinkwhalehealth"><img src="images/twitter.png" /></a></div>
</td>
<?php 
if(!isset($_SESSION['username']))
{
	?>
    <td width="330">
    <div id="container" style='width:auto;'>
    <img src="images/tml.jpg" align="absmiddle" />
    <a href="index.php">Home</a> | 
    <a href="join.php">Join</a> |
    <a href="login" class="signin"><span>Sign in</span> 
    <img src="images/pinkarrow.png" align="absmiddle" /></a>
    <img src="images/tmr.jpg" align="absmiddle" />
    <fieldset id="signin_menu">
        <form method="post" id="signin" action="login_action.php" name="login_name">
        <p>
          <label for="username"><strong>Username or email</strong></label>
          <input id="username" name="username" value="" title="username" tabindex="4" type="text">
          <div id="username_div" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
          </p>
          <p>
            <label for="password"><strong>Password</strong></label><br/>
            <input id="password" name="password" value="" title="password" tabindex="5" type="password">
            <div id="password_div" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
          </p>
          <p class="remember">
            <input id="signin_submit" value="Sign in" tabindex="6" type="button" onclick="LoginSignUp(login_name)">
           <label for="setcookie"> <input id="setcookie" name="setcookie" value="setcookie"  type="checkbox">
            Remember me</label>
         </p>   
        <p class="forgot"> <a href="forgot_password.php" id="resend_password_link"><span style="color:#000000;"><b>Forgot your password?</b></span></a> </p>
          
        </form>
      </fieldset>
    </div>
    <script src="js/jquery1.js" type="text/javascript"></script>
    <script type="text/javascript">
            $(document).ready(function() {
    
                $(".signin").click(function(e) {          
                    e.preventDefault();
                    $("fieldset#signin_menu").toggle();
                    $(".signin").toggleClass("menu-open");
                });
                
                $("fieldset#signin_menu").mouseup(function() {
                    return false
                });
                $(document).mouseup(function(e) {
                    if($(e.target).parent("a.signin").length==0) {
                        $(".signin").removeClass("menu-open");
                        $("fieldset#signin_menu").hide();
                    }
                });			
                
            });
    </script>
    
    <script src="js/jquery.tipsy.js" type="text/javascript"></script>
    <script type='text/javascript'>
        $(function() {
          $('#forgot_username_link').tipsy({gravity: 'w'});   
        });
      </script>
    </td>
		

<?php 
	}
	else if($_SESSION['login']=='user')
	{
?>		
	    <td width="330">
        <div id="container1" style='width:auto;'>
            <img src="images/tml.jpg" align="absmiddle" />
            <a href="index.php">Home</a> | 
            <a href="phr.php">My Account</a> |
            <a href="cart.php">Shoping Cart</a> |
            <a href="logout.php" class="signin"><span>Sign out</span></a>
            <img src="images/tmr.jpg" align="absmiddle" />
        </div>
<?php } 
	else if($_SESSION['login']=='doctor')
	{
?>
	    <td width="330">
        <div id="container2" style='width:auto;'>
            <img src="images/tml.jpg" align="absmiddle" />
            <a href="index.php">Home</a> | 
            <a href="doc_phr.php">My Account</a> |
            <a href="logout.php" class="signin"><span>Sign out</span></a>
            <img src="images/tmr.jpg" align="absmiddle" />
        </div>
<?php } 

	else if($_SESSION['login']=='admin')
		{
		?>
			<td width="330">
<div id="s90topmenus_admin1" style='width:auto;'>
<img src="images/tml.jpg" align="absmiddle" />
<a href="index.php">Home</a> | 
<a href="admin/pw_admin.php">My Account</a> |
<a href="logout.php" class="signin"><span>Sign out</span></a>
<img src="images/tmr.jpg" align="absmiddle" /></div>
	<?php	}
	
	   else
	   {
		?>
    <td width="330">
    <div id="container" style='width:auto;'>
    <img src="images/tml.jpg" align="absmiddle" />
    <a href="index.php">Home</a> | 
    <a href="join.php">Join</a> |
    <a href="login" class="signin"><span>Sign in</span> 
    <img src="images/pinkarrow.png" align="absmiddle" /></a>
    <img src="images/tmr.jpg" align="absmiddle" />
    <fieldset id="signin_menu">
        <form method="post" id="signin" action="login_action.php" name="login_name">
        <p>
          <label for="username"><strong>Username or email</strong></label>
          <input id="username" name="username" value="" title="username" tabindex="4" type="text">
          <div id="username_div" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
          </p>
          <p>
            <label for="password"><strong>Password</strong></label><br/>
            <input id="password" name="password" value="" title="password" tabindex="5" type="password">
            <div id="password_div" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
          </p>
          <p class="remember">
            <input id="signin_submit" value="Sign in" tabindex="6" type="button" onclick="LoginSignUp(login_name)">
            <label for="setcookie"> <input id="setcookie" name="setcookie" value="setcookie"  type="checkbox">
            Remember me</label>
         </p> 
         <p class="forgot"> <a href="forgot_password.php" id="resend_password_link"><span style="color:#000000;"><b>Forgot your password?</b></span></a> </p>
    
        </form>
      </fieldset>
    </div>
    <script src="js/jquery1.js" type="text/javascript"></script>
    <script type="text/javascript">
            $(document).ready(function() {
    
                $(".signin").click(function(e) {          
                    e.preventDefault();
                    $("fieldset#signin_menu").toggle();
                    $(".signin").toggleClass("menu-open");
                });
                
                $("fieldset#signin_menu").mouseup(function() {
                    return false
                });
                $(document).mouseup(function(e) {
                    if($(e.target).parent("a.signin").length==0) {
                        $(".signin").removeClass("menu-open");
                        $("fieldset#signin_menu").hide();
                    }
                });			
                
            });
    </script>
    <script src="js/jquery.tipsy.js" type="text/javascript"></script>
    <script type='text/javascript'>
        $(function() {
          $('#forgot_username_link').tipsy({gravity: 'w'});   
        });
      </script>
    </td>
        
        <?php
	   }
	   ?>

</tr>

<tr><td height="50" colspan="2" class="s90mainmenus"><ul>
<div id="header_menu1">
	<a href="for_businesses.php" style="text-decoration:none; color:#FFF;">For Businesses</a>
</div>
<div id="header_menu2">
	<a href="for_health_care_providers.php" style="text-decoration:none; color:#FFF;">For Health Care Providers</a>
</div>
<div id="header_menu3">
<a href="index.php" style="text-decoration:none; color:#FFF;">For Users</a>
</div>
<!--<li class="s90mainmenu4"><a href="#">My Account</a></li>-->
</ul></td></tr>
<tr><td colspan="3" class="s90submenusbg">
<div id="s90submenus"><img src="images/sml.jpg" align="absmiddle"/>
<a href="get-second-medical-opinion.php">Get a Second Opinion</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="consultation-with-specialist-doctors.php">Consult a Specialist</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="online-counselling.php">Call a Counsellor</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="chat_with_a_doctor_online.php">Consult a Physician</a><img src="images/smr.jpg" align="absmiddle"/></div>
</td></tr>
</table>
<script type="application/javascript">
colourchange3();
</script>
<?
	/**
	 * function htmlForNoRecors
	 * 
	 * To create html content for show 'No records available' label
	 * 
	 * @param none
	 * 
	 * @return $htmlContent
	 */
	function htmlForNoRecords($width, $outer = false, $label = 'No records available')
	{
		$htmlContent =  "";
		
		//if outer is true table content should be inside tr td tags
		if($outer == true)
		{
			$htmlContent =  "<tr>
								<td class='s90dbdtbls_drdetails'>";
		}
		
		$htmlContent	 .=	"<table width='$width' border='0' cellspacing='0' cellpadding='5' align='center' style='font-size:15px;font-family:arial;color:#21759B;'>
								<tr>
									<td align='center'><b>$label</b></td>
								</tr>
							</table>";
							
		if($outer == true)
		{
			$htmlContent .=  	"</td>
							</tr>";
		}					
							
		return $htmlContent;
	}
?>
