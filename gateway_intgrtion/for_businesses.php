<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="images/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px">
<tr><td width="294" rowspan="2"><a href="https://www.pinkwhalehealthcare.com/"><img src="images/pinkwhale_logo.jpg" width="294" height="104" /></a></td>
<td width="706" height="54"><div id="s90social"><a href="https://blog.pinkwhalehealthcare.com/"><img src="images/feed.png" /></a><a href="http://www.facebook.com/pages/PinkWhale-Healthcare/151598318209476"><img src="images/facebook.png" /></a><a href="http://twitter.com/pinkwhalehealth"><img src="images/twitter.png" /></a></div>
<div id="s90topmenus"><img src="images/tml.jpg" align="absmiddle" />
<a href="index.php">Home</a> | 
<!--<a href="card_topup.php">Card Topup</a> | -->
<a href="join.php">Join</a>
<!-- | <a href="phr.php">Login <img src="images/pinkarrow.png" align="absmiddle" /></a>-->
<img src="images/tmr.jpg" align="absmiddle" /></div></td></tr>
<tr><td height="50" class="s90mainmenus"><ul>
<li class="s90mainmenu1 s90active1"><a href="for_businesses.php">For Businesses</a></li>
<li class="s90mainmenu2"><a href="for_health_care_providers.php">For Health Care Providers</a></li>
<li class="s90mainmenu3"><a href="index.php">For Users</a></li>
<!--<li class="s90mainmenu4"><a href="#">My Account</a></li>-->
</ul></td></tr>
<tr><td colspan="2" class="s90submenusbg2"><img src="images/spacer.gif" width="5" height="4" /></td></tr>
<!--<tr><td colspan="2" class="s90submenusbg">
<div id="s90submenus"><img src="images/sml.jpg" align="absmiddle"/>
<a href="for_schools.php">For Schools</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="for_corporates.php">For Corporates</a><img src="images/smr.jpg" align="absmiddle"/></div></td></tr>--></table>


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td colspan="2" class="s90cpages">
<h1>Employee Life Success Program</h1>

<table width="978" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #000">
<tr><td width="613" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:#00b050; font-weight:bold; margin:10px 0px 10px 0px">Work and personal  responsibilities can weigh down on any of your employees and shift their  attention and energy away from your business</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="250"><img src="images/forb1.jpg" width="229" height="177" /><br />
  <br /></td>
<td width="363" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">DID YOU KNOW?</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:10px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />91% of the employees questioned in a research<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;survey admitted that personal life events led to<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loss of productivity at work.</div>
<div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:#00b050; font-weight:bold; margin:18px 0px 10px 0px">Your employees are looking to achieve their personal & professional goals and lead a satisfied & fulfilling life</div></td></tr>
</table></td><td width="365"><img src="images/fb1.jpg" width="357" height="231"/></td></tr></table>



<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; margin-bottom:20px; border-bottom:1px solid #000">
<tr><td width="610"><img src="images/forb2.jpg" width="595" height="380" /></td>
<td width="368" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:24px 0px 10px 0px">Testimonials:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"Within six months of joining the industry I developed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;problems of acidity, lethargy and fatigue. When I &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;approached a counsellor online,  He set me a daily &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;routine and counselled me that improved my health," </div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"Thank you, I am at more peace with myself and at &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;work now" </div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"I am able to control my anger and able to interact &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;with others more effectively"</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:20px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />"Thank you so much. I feel I can handle myself now."</div>
</td></tr></table>


<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px; margin-bottom:10px">
<tr><td><img src="images/forb3.jpg" width="200" style="margin-right:10px;" /></td>
<td width="100%" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">Employee Life Success Program:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's a benefit program for your employees to reach out for support on any work/life &nbsp;&nbsp;&nbsp;&nbsp;related question that they want to address.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's a phone call away, and employees can get answers to their questions quickly, &nbsp;&nbsp;&nbsp;&nbsp;easily, and conveniently.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's completely confidential and anonymous for your employee's benefit</div></td>

<td width="211" rowspan="3" align="right"><img src="images/forb42.jpg" width="209"/></td></tr>

<tr><td colspan="2"><div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:#00b050; font-weight:bold; margin:16px 0px 6px 0px">By investing in your employee's well-being, you can reduce attrition and build a high-performance company</div></td></tr>


<tr><td colspan="2" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left">
<tr><td width="60%"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">We Make It Easy For HR</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:10px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Launch the service and promote actively to  your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Newsletters and brochures for your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Manage the service on an on-going basis<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Seminars to increase awareness<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Participate and promote in your employee benefit and orientation &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;workshops<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Bring professional speakers for information sessions on various life &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;topics<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Brand it as your program<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Provide usage reports & recommendations<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Share best practices in the industry<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Recommend well-being programs to improve employee retention<br /></div></td>
<td width="40%"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">Corporate Benefits:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Employee motivation, high morale, leading &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to lower attrition and improved  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;organizational health</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />92% of (corporate) respondents surveyed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;said that their productivity had significantly &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;improved after proper counselling</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />According to ASSOCHAM 2009 study, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;84% of employees find company- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sponsored wellness programs as a  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;motivating factor</div></td></tr></table></td></tr></table>

<p style="text-align:right"><br /><strong>To learn more, email us at: <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a></strong></p>



<!--
<img src="images/forbusiness_tcsteps.jpg" height="189" />
<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px; margin-bottom:10px">
<tr><td width="245"><img src="images/forb3.jpg" width="221" height="208" /></td>
<td width="733" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:14px 0px 10px 0px">Employee Life Success Program:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's a benefit program for your employees to reach out for support on any work/life related question that they want <br />
&nbsp;&nbsp;&nbsp;&nbsp;  to address.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's a phone call away, and employees can get answers to their questions quickly, easily, and conveniently.</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />It's completely confidential and anonymous for your employee's benefit</div></td>
<td width="245" rowspan="4"><img src="images/forb4.jpg" width="234" height="723" /></td>
</tr>
<tr><td colspan="2"><div style="font-family:'Trebuchet MS', Arial; font-size:14px; line-height:20px; color:#00b050; font-weight:bold; margin:16px 0px 6px 0px">By investing in your employee's well-being, you can reduce attrition and build a high-performance company</div></td></tr>


<tr><td width="245" align="center"><img src="images/forb5.jpg" width="150" /></td>
<td width="733" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:24px 0px 10px 0px">We Make It Easy For HR</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:10px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Launch the service and promote actively to  your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Newsletters and brochures for your employees<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Manage the service on an on-going basis<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Seminars to increase awareness<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Participate and promote in your employee benefit and orientation workshops<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Bring professional speakers for information sessions on various life topics<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Brand it as your program<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Provide usage reports & recommendations<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Share best practices in the industry<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Recommend well-being programs to improve employee retention<br /></div>



</td>
</tr>
<tr><td width="245">&nbsp;</td>
<td width="733" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:24px 0px 10px 0px">Corporate Benefits:</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Employee motivation, high morale, leading to lower attrition and improved &nbsp;&nbsp;&nbsp;&nbsp;organizational health</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />92% of (corporate) respondents surveyed said that their productivity had &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;significantly improved after proper counselling</div>
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:14px"><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />According to ASSOCHAM 2009 study, 84% of employees find <br />
 &nbsp;&nbsp;&nbsp;&nbsp;company-sponsored wellness programs as a motivating factor</div></td>
</tr></table>


<table width="978" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; margin-bottom:20px">
<tr><td width="630" valign="top"><div style="font-family:'Trebuchet MS', Arial; font-size:15px; line-height:20px; color:#e36c0a; font-weight:bold; margin:30px 0px 10px 0px">PinkWhale Healthcare Services:</div>

<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:0px 20px 20px 0px">
<strong>pinkWhale Healthcare allows you to access high quality medical care anytime, anywhere.</strong></div>
<div style="font-family:Arial; font-size:13px; line-height:22px; color:#333333; margin:12px">
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />We bring to you renowned specialist doctors, who provide telephone, email, or  face-to-face &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;consultations.  
<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Our super-specialist doctors provide a second opinion online.  
<br /><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Our physicians provide telephone or online consultations empowering you with knowledge about a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;medical condition or treatment.  
<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Our certified counsellors and life skills experts provide confidential telephone, online or face-to-face &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;consultations. <br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />We bring diagnostic lab tests to your doorstep.  
<br /><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />You can create your online personal medical record, consolidate your medical information in one &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;place, and access it from anywhere, anytime. 
<br />
<img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Our goal is to give you the peace of mind by providing convenient access to the best medical care &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wherever you are, using a state-of-the-art technology platform.<br />
</div>
</td>
<td width="348"><div style="margin:4px; width:340px; float:left; text-align:center"><img src="images/forb6.jpg" width="300"/></div><br />
<div style="font-family:Arial; font-size:13px; line-height:20px; color:#333333; margin:0px; width:348px; float:left">
  <p><strong>Contact:</strong><strong> </strong><br />
    Sai Subramaniam,  Ph.D., MBA <br />
    Head, Business  Development &amp; Strategy<br />
    pinkWhale  Healthcare Services Pvt. Ltd.<br />
    275, 16th  Cross, 2nd Block, RT Nagar, Bangalore 560032<br />
    Mobile:  +91-99723-98080 <br />
    Email: <a href="mailto:sai@pinkwhalehealthcare.com">sai@pinkwhalehealthcare.com</a> </p>
</div></td>
</tr></table>

<ol><br /><br /><li><strong>Work and personal responsibilities can weigh  down on any of your employees and shift their attention and energy away from  your business. Did you Know?</strong>
      <ol>
        <li>91% of the employees questioned in a research  survey admitted that personal life events led to loss of productivity at work. </li>
        <li>According to 2010 ASSOCHAM corporate employees  study, 22% of the corporate employees suffer from depression.<br /><br /><br /></li>
      </ol></li>
      
<li><strong>pinkWhale&rsquo;s Corporate Employee Assistance  Program (EAP)</strong><ol>
      <li>pinkWhale&rsquo;s Employee Assistance Program (EAP) is  designed to help your employees better handle life situations without adversely  affecting their work productivity. By including EAP in your HR benefit program,  you invest in improving your employee well-being, demonstrate your commitment  to your employees, and a commitment to foster a healthier workforce. </li>
      <li>pinkWhale EAP provides solutions to most common problems  that cause worry and distract your employees</li>
</ol></li>
<img src="images/fb2.jpg" width="260" align="right" style="margin-top:30px;" />
<table width="676" border="0" cellspacing="2" cellpadding="0">
<tr><td width="310" valign="top">  
        <li value="1"><strong>Workplace and Work-Life Balance</strong></li>
        <ol>
          <li>Relationships with peers &amp; managers </li>
          <li>Dealing with stress </li>
          <li>Handling criticism </li>
          <li>Dealing with change </li>
          <li>Balancing workload and family responsibilities </li>
          <li>Time management </li>
          <li>Harassment</li>
        </ol></td>
    <td width="374" valign="top">
    <li value="2"><strong>Personal &amp; Emotional Well-being</strong></li>
        <ol>
          <li>Anxiety</li>
          <li>Anger management</li>
          <li>Building self-esteem</li>
          <li>Motivation &amp; focus</li>
          <li>Staying positive</li>
          <li>Dealing with depression</li>
          <li>Dealing with bereavement</li>
          <li>Help during rehabilitation</li>
          <li>Overcoming habits (alcohol, smoking, substance  abuse)</li>
        </ol></td></tr></table>

<img src="images/fb3.jpg" width="260" align="right" style="margin-top:30px;" />
<table width="676" border="0" cellspacing="2" cellpadding="0">
<tr><td width="310" valign="top"><li value="3"><strong>Personal &amp; Family Relationships</strong></li>
        <ol>
          <li>Pre-marital relationships</li>
          <li>Marital conflicts</li>
          <li>Sexual concerns</li>
          <li>Recovering from divorce</li>
          <li>Handling In-laws, parents</li>
          <li>Caring for the aged members</li>
        </ol></td>
<td width="374" valign="top"><li value="4"><strong>Parenting</strong></li>
        <ol>
          <li>Single parenting</li>
          <li>Parenthood</li>
          <li>Raising adolescents</li>
          <li>Sibling rivalry</li>
          <li>Child development &amp; behavior</li>
          <li>School performance</li>
          <li>Building self-esteem in children</li>
        </ol></td>
</tr></table>

     <li>Did you Know?</li>
      <ol>
        <li>92% of (corporate) respondents surveyed said  that their productivity had significantly improved after proper counseling. </li>
        <li>According to ASSOCHAM 2009 study, 84% of  employees find company-sponsored wellness programs as a motivating factor.</li>
      </ol>
      <li>Contact us today to see how pinkWhale EAP can  help your organization to foster a healthy and productive workforce and become  a desirable place to work.</li>
    </ol>
</li>
</ol>

<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;pinkWhale Corporate Employee Health Program (Coming  Soon!)</strong></p>-->
</td>
</tr></table>



<table width="1000" border="0" cellspacing="0" cellpadding="0" class="s90footer" align="center">
<tr><td colspan="2"><img src="images/footertop.jpg" width="1000" height="32" /></td></tr>
<tr><td width="300" height="80"><img src="images/footer_logos.jpg" width="110" height="54" /></td>
<td width="700"><a href="index.php">Home</a> | 
<a href="about_us.php">About Us</a> | 
<a href="faqs.php">Faq's</a> | 
<a href="http://blog.pinkwhalehealthcare.com/">Wellness Blog</a> | 
<a href="contact_us.php">Contact Us</a><br />
<a href="advertise_with_us.php">Advertise with us</a> | 
<a href="terms.php">Terms of Service</a> | 
<a href="privacy_policy.php">Privacy Policy</a><br />
� www.pinkwhalehealthcare.com. All rights reserved.  </td></tr></table>

</body></html>