<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="images/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px">
<tr><td width="294" rowspan="2"><a href="https://www.pinkwhalehealthcare.com/"><img src="images/pinkwhale_logo.jpg" width="294" height="104" /></a></td>
<td width="706" height="54"><div id="s90social"><a href="https://blog.pinkwhalehealthcare.com/"><img src="images/feed.png" /></a><a href="http://www.facebook.com/pages/PinkWhale-Healthcare/151598318209476pages/PinkWhale-Healthcare/151598318209476"><img src="images/facebook.png" /></a><a href="http://twitter.com/pinkwhalehealth"><img src="images/twitter.png" /></a></div>
<div id="s90topmenus"><img src="images/tml.jpg" align="absmiddle" />
<a href="index.php">Home</a> | 
<!--<a href="card_topup.php">Card Topup</a> | -->
<a href="join.php">Join</a>
<!-- | <a href="phr.php">Login <img src="images/pinkarrow.png" align="absmiddle" /></a>-->
<img src="images/tmr.jpg" align="absmiddle" /></div></td></tr>
<tr><td height="50" class="s90mainmenus"><ul>
<li class="s90mainmenu1"><a href="for_businesses.php">For Businesses</a></li>
<li class="s90mainmenu2  s90active2"><a href="for_health_care_providers.php">For Health Care Providers</a></li>
<li class="s90mainmenu3"><a href="index.php">For Users</a></li>
<!--<li class="s90mainmenu4"><a href="#">My Account</a></li>-->
</ul></td></tr>
<tr><td colspan="2" class="s90submenusbg2"><img src="images/spacer.gif" width="5" height="4" /></td></tr>
<!--<tr><td colspan="2" class="s90submenusbg">
<div id="s90submenus"><img src="images/sml.jpg" align="absmiddle"/>
<a href="for_physicians.php">For Physicians</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="for_counsellors.php">For Counsellors</a><img src="images/smcut.jpg" width="5" align="absmiddle" />
<a href="for_hospitals.php">For Hospitals</a><img src="images/smr.jpg" align="absmiddle"/></div></td></tr>--></table>


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td colspan="2" class="s90cpages">
<table width="980" border="0" cellspacing="0" cellpadding="0">
<tr><td width="400" style="border-bottom:1px solid #000"><h1><font size=4 color="purple">Doctors & Physicians:</font></h1>
<p><font size=4 color="darkpink"><strong>Join us, and grow your practice!</strong></font> </p>
<p>Healthcare is in the midst of a cultural shift from traditional models to technology-enabled, consumer-driven models. 
With the penetration of Mobile phones, Email, Chat, and Internet, consumers are increasingly looking for information on the Web. 
With changing lifestyles, surging aspirations, busy work patterns, and active travel schedules, more and more medical requests are being initiated on the phone or online.</p>
<p>Our "Affiliate Doctors" service helps doctors who wish to upgrade their practices and provide phone and online consultations for their patient follow-up visits.  
With pinkWhale E-clinic, your practice will be visible on the web and social media with tools to attract new patients. </p>
<p><strong>To learn more, email us at:</strong> <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a></p></td>
<td width="580" style="border-bottom:1px solid #000"><img src="images/for-docs_img1.jpg" width="580"/></td></tr>
<td><br></td>
<tr><td> <h1><font size=4 color="purple">Hospitals, Poly-Clinics & Nursing Homes:</font></h1>
<p><font size=4 color="darkpink"><strong>Join us, grow your quality of care & visibility!</strong></font> </p>
<p>There are two emerging trends in the hospital industry.</p>
<p>1.	First, with a growing shift to day procedures or short hospital stays, hospital doctors have less time to prepare patients adequately. 
<li>In a recent study, 80% of patients said that they did not have enough information following a hospital discharge. 
Many patients encounter a variety of problems in the first few days and weeks after they have been discharged from the hospital. 
<li>Patients typically have insufficient knowledge to understand symptoms, experience adverse events, have anxiety and emotional stress, less clear about diet restrictions, or just lack information on their medications. 
<li>A proper follow-up process is imperative to ensure patient satisfaction. 
How patients perceive the services they received will affect their perception of the hospital.</p>
<p>2.	A second emerging trend today is medical tourism. 
According to a recent survey, the main factors that influence patient choice of a hospital are: expertise of doctor, and patient reviews. 
A patient's impression of hospitals will be formed based on their past experience and the experience shared in their social group.</p></td>
<td><img src="images/for_hospitalsimg1.jpg" width="580"/></td></tr></table>
<p>pinkWhale Healthcare's services enable you to deliver better care for your patients and ensure that your patients are satisfied with the hospital visit. Our technology can be used to provide telephone or online consultations for pre- and post-operative care. These channels are a good way to exchange information, with your patients, educating the patient on treatment plans, managing symptoms, recognizing complications, and most importantly, giving peace of mind to the patient.</p>
<p>By having your hospital and specialists registered with us, your expertise will be visible to our customers and you will be able to attract new patients for medical tourism.</p>
<p><strong>To learn more, email us at:</strong> <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a></p>

</td></tr></table>

<table width="1000" border="0" cellspacing="0" cellpadding="0" class="s90footer" align="center">
<tr><td colspan="2"><img src="images/footertop.jpg" width="1000" height="32" /></td></tr>
<tr><td width="300" height="80"><img src="images/footer_logos.jpg" width="110" height="54" /></td>
<td width="700"><a href="index.php">Home</a> | 
<a href="about_us.php">About Us</a> | 
<a href="faqs.php">Faq's</a> | 
<a href="http://blog.pinkwhalehealthcare.com/">Wellness Blog</a> | 
<a href="contact_us.php">Contact Us</a><br />
<a href="advertise_with_us.php">Advertise with us</a> | 
<a href="terms.php">Terms of Service</a> | 
<a href="privacy_policy.php">Privacy Policy</a><br />
� www.pinkwhalehealthcare.com. All rights reserved.  </td></tr></table>

</body></html>