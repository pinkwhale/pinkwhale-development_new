<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- menu.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- Menu.......-->
</td>

<td width="748" valign="top" class="s90phrcontent">
<?php include("name_card_no.php");?>

<table>
	<tr>
		<td height='15px'></td>
	</tr>
</table>
<table height="30" width="700" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;margin-left:20px;">My Completed Consultaions</span>
		</td>
	</tr>
</table>


<!--///////////////////////  specialist email consults  //////////////////////////////// -->
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:10px">
	<tr><th>Specialist Online Consults</th></tr>
<?php
	$qry1= "SELECT * FROM `pw_consultation_emails` WHERE `pw_card_id`='$user_id' && `mail_status`='closed' group by `consultation_no`";
	$qry_rslt1 = mysql_query($qry1);
	if(mysql_num_rows($qry_rslt1)<=0)
	{
		echo htmlForNoRecords("580", true, "Your completed specialist consultation record is empty");
	}
	else {
	while($result1= mysql_fetch_array($qry_rslt1))
	{
		$gdate=strtotime($result1['entered_date']);
		$final_date=date("d M Y", $gdate);
		 
?>    

	<tr><td class="s90dbdtbls_drdetails">
        <table width="557" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
        <tr>
        	<td width="158"><?php echo $result1['doctor_name']; ?> </td>
            <td width="120"><font color="#000000"><?php echo $final_date; ?></font></td>
            <td width="200"><font color="#000000"><?php echo $result1['complaint']; ?></font></td>
            <td width="100">
            	<a href="history_patient_spe_consultation.php?idfr=<?php echo $result1['consultation_no']; ?>">
            	<img src="images/phr_clickhere91.jpg" /></a>
            </td>
        </tr>
        </table>
	</td></tr>
<?php } 
		}
?>    
</table>
<!--///////////////////////  specialist email consults  //////////////////////////////// -->

<!--///////////////////////  Expert email consults  //////////////////////////////// -->
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl" style="margin-top:10px">
	<tr><th>Expert Online Consults</th></tr>
    
<?php
	$qry2   = "SELECT * FROM `pw_expert_consultation_emails` WHERE `pw_card_id`='$user_id' && `mail_status`='closed' group by `consultation_id`";
	$qry_rslt2 = mysql_query($qry2);
	if(mysql_num_rows($qry_rslt2)<=0)
	{
		echo htmlForNoRecords("580", true, "Your completed expert consultation record is empty");
	}
	else
	{
	while($result2= mysql_fetch_array($qry_rslt2))
	{
		$gdate=strtotime($result2['entered_date']);
		$final_date=date("d M Y", $gdate);
?>    
	<tr><td class="s90dbdtbls_drdetails">
        <table width="557" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
        <tr>
            <td width="158"><?php echo $result2['doctor_name']; ?></td>
            <td width="120"><font color="#000000"><?php echo $final_date; ?></font></td>
            <td width="200"><font color="#000000"><?php echo $result2['complaint']; ?></font></td>
            <td width="100">
            	<a href="history_patient_exp_consultation.php?idfr=<?php echo $result2['consultation_id']; ?>">
            	<img src="images/phr_clickhere91.jpg" /></a>
            </td>
        </tr>
        </table>
	</td></tr>
<?php } 
		}
?>    
</table>
<!--///////////////////////  Expert email consults  //////////////////////////////// -->

</td></tr></table>

<!-- ---------- menu selection    ---------------    -->
<script type="text/javascript">
	enable_history_menu()
</script>
<!----------- END menu selection    ----------------- -->
<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
