<?php 
        error_reporting(E_PARSE); 
      session_start();
        include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
        {
                header("Location: index.php");
                exit();
        }
        else
        {
                $doctor_id=$_SESSION['doctor_id'];
                

        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cancel_appointment.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css" />
<script type="text/javascript" src="js/timepicker/rainbow.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/rainbow.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<style type="text/css">
	.ui-dialog { font-size: 11px; }
	body {
		font-family: Tahoma;
		font-size: 12px;
	}
	#question {
		width: 300px!important;
		height: 60px!important;
		padding: 10px 0 0 10px;
	}
	#question img {
		float: left;
	}
	#question span {
		float: left;
		margin: 20px 0 0 10px;
	}
        .ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>

<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
    
</div>
<?php include 'header.php'; ?>
<?php

require_once('calendar/classes/tc_calendar.php');
include "get-who-booked-details.php";
?>
<?php 

$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag FROM `pw_doctors` where `doc_id`='$doctor_id' ";

$qry_rslt = mysql_query($qry);

while($result = mysql_fetch_array($qry_rslt))

	{

		$doc_category = $result['doc_category'];

		$doc_name = $result['doc_name'];

		$doc_id = $result['doc_id'];

		$doc_ctgry_flag=$result['doc_category_change_flag'];

	}

?>



<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="220" valign="top">

<div id="s90dashboardbg">

   <!-- <img src="images/dots.gif" />

    <a href="doc_phr.php"><b>Dashboard</b></a></div>




<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php 
include 'doc_phr_left_menu.php'; 
$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>



<td width="748" valign="top" class="s90docphr">

<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">

<tr>

	<td width="220"><h1>Appointment Summary </h1></td>

	<td width="528" bgcolor="#f1f1f1" align="right">    	

    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">

        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>

    </div>

    </td>

</tr>

</table>

<table height="30"></table>
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">            
    <tr>
        <td width="528"  align="center">
            <div id="clinic_dash">   
                <form  method="POST">
                <table border="0" cellpadding="0" cellspacing="1"  align="center" ><tr>   
                    <td width="30%" align="right" ><b>Date</b>:</td>            
                    <td width="124" valign="top" >

                            <?php


                              $date2=date("Y-m-d", time()+86400);
                              $date3=date("Y-m-d", time()+86400);
                              $myCalendar = new tc_calendar("date2",true,false);
                              $myCalendar->setIcon("calendar/images/iconCalendar.gif");
                              $myCalendar->setDate(date('d', strtotime($today_date)), date('m', strtotime($today_date)), date('Y', strtotime($today_date)));
                              $myCalendar->setPath("calendar/");
                              $myCalendar->setYearInterval(1910, 2015);
                              $myCalendar->dateAllow($today_date, '2015-03-01');
                              $myCalendar->setDateFormat('j F Y');
                              //$myCalendar->setOnChange("get_time_slots()");
                              //$myCalendar->setWidth(500);	  
                              //$myCalendar->autoSubmit(true, "form1");
                              //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                              $myCalendar->writeScript();



                                ?>
                    </td>                    
                    <td width="124" valign="top" ><input type="submit" value="Go" /></td>                    
                    </tr>
                </table>
                </form>
                
                <table border="0" cellpadding="0" cellspacing="1" width="600" align="center" bgcolor='#eeeeee' class="s90registerform">
                <?php     
                    if($_POST['date2']==""){
                        echo "<tr><th colspan=\"5\"> Today's Appointment Summary</th></tr>";
                    }else{
                        $today_date = $_POST['date2'];
                        echo "<tr><th colspan=\"5\">Clinic Wise Appointment Details for ". date("d M Y",strtotime($_POST['date2']))."</th></tr>";
                    }
                ?>                
                
                <tr>
                    <td align='center'><b>Clinic Name</b></td>
                    <td align='center'><b>Appointment Time</b></td>
                    <td align='center'><b>Patient Name</b></td>
                    <td align='center'><b>Token-id</b></td>
                    <td align='center'><b>Booked By</b></td>
                </tr>
                <tr>
                <?php

                   

                    $qry = "select cd.clinic_id,cd.name from clinic_details cd inner join doctor_clinic_details cdd on cd.clinic_id=cdd.clinic_id where doctor_id=$doc_id and status=0 union select cd.clinic_id,cd.name from clinic_details cd inner join clinic_doctors_details cdd on cd.clinic_id=cdd.clinic_id where doctor_id=$doc_id and status=0";
                    $res = mysql_query($qry);
                    $num = mysql_num_rows($res);
                    if($num==0){
                        echo "<script>document.getElementById(\"clinic_dash\").innerHTML=\"<font size=4 color=red>Clinic's are Not Added</font>\";</script>";
                    }else{
                        while($dat = mysql_fetch_array($res)){
                            $qry1 = "select admin_id,name,DATE_FORMAT(from_time,'%h:%i %p') as from_time,patient_name,a.email,mobile,gender,age,adress,token_id from Appointment_book_details a inner join clinic_details c on a.clinic_id=c.clinic_id where doc_id=$doc_id and a.clinic_id='".$dat['clinic_id']."' and c.status=0 and a.status=2 and from_time like '$today_date%' order by from_time ";
                            $res1 = mysql_query($qry1);
                            $num1 = mysql_num_rows($res1);                            
                            if($num1==0){
                                echo "<td  align='left' bgcolor='#F5F5F5'>".$dat['name']."</td>";
                                echo "<td colspan='4' align='center' bgcolor='#F5F5F5'>No Appointment's </td></tr><tr>";
                            }else{
                                echo "<td rowspan='$num1' align='left' bgcolor='#F5F5F5'>".$dat['name']."</td>";
                                while($data1 = mysql_fetch_array($res1)){
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['from_time']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['patient_name']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['token_id']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".get_name_detals($data1['admin_id'])."</td></tr><tr>";
                                }                            
                            }
                        }
                    }
                
                ?>
                </tr></table>
            </div>
        </td>
    </tr>
</table>

    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>
</body></html>
<div id="dialog-confrm-cancel" title="Cancel Doctor Appointments !" style="display:none;">
        <?php echo "Dear Admin"; ?>,<p> Are you sure ?</p>
</div>
