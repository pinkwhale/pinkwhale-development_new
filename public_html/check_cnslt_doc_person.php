<?php
	error_reporting(E_PARSE); 
	$doc_id=$_GET["id"];

?>
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<form name="check_form" action="check_cnslt_doc_person.php" method="post">
    <table cellpadding="0" border="0" cellspacing="2">
    	<tr><td colspan="3"><img src="images/spacer.gif" height="10" align="absmiddle" /></td></tr>
    	<tr><td><div class="label">Enter Doctor ID </div></td>
        	<td colspan="2"><input type="text" name="val_doc_id" id="val_doc_id" /></td>
        </tr> 
<!--    ERROR DIV -->
		<tr><td  colspan="3" align="center" height="8">
	    <div id="doc_id_errdiv" class="error" style="color: #F33;font-family:Arial,verdana;font-size:12px; margin-left:8px;"></div>
        </td></tr>
<!--  END ERROR DIV --> 
        <tr>
            <td width="116" align="right"><input type="checkbox" name="agree" /> </td>
            <td colspan="2"><p>I agree that I have consulted this doctor in person</p> </td>
            <input type="hidden" name="old_doc_id" value="<?php echo $doc_id ; ?>"  />
        </tr>
<!--    ERROR DIV -->
		<tr><td  colspan="3" align="center" height="8">
	    <div id="agree_errdiv" class="error" style="color: #F33;font-family:Arial,verdana;font-size:12px; margin-left:8px;"></div>
        </td></tr>
<!--  END ERROR DIV --> 
        <tr><td colspan="2"><img src="images/spacer.gif" height="4" align="absmiddle" /></td></tr>
        <tr>
        	<td>&nbsp;</td>
        	<td width="110"><input type="button" name="ok" value="Continue" onclick="validate_new_docid(check_form)"/></td>
        	<td width="223"><input type="button" name="Cancel" value="Cancel" onclick="cancel_tooltip()"/> </td>
        </tr>
    </table>
</form>



<script type="text/javascript">
var idValidated = false;
var agreeValidated = false;


	function validate_new_docid(form)
	{
		idValidated = true;
		agreeValidated = true;
		document.getElementById("doc_id_errdiv").innerHTML="";
		document.getElementById("agree_errdiv").innerHTML="";
		if (form.val_doc_id.value=='')
		{
			document.getElementById("doc_id_errdiv").innerHTML = "Please enter a doctor id" ;
			idValidated = false;
		}
		if(form.agree.checked!=true)
		{
			document.getElementById("agree_errdiv").innerHTML = "Please Read and accept the agreement" ;
			agreeValidated = false;			
		}
		if(agreeValidated  && idValidated )
		{
			if(form.val_doc_id.value==form.old_doc_id.value)
			{
				document.getElementById('specialist_topup_form').submit();
				ajax_hideTooltip();
			}
			else
			{
				document.getElementById("agree_errdiv").innerHTML="Please enter a valid doctor id";
			}
		}
		
	}
	function cancel_tooltip()
	{
		ajax_hideTooltip();
	}
</script>