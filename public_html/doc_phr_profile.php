<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="admin/js/jquery.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php 
$qry= "SELECT doc_category,doc_name,doc_id,doc_email_id FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
		$doc_email_id = $result['doc_email_id'];
	}
?>

<?php
include ("db_connect.php");
include 'header.php'; ?>
<!-- header.......-->
<script type="text/javascript">
function changeDocProfEditDiv()
{
		document.getElementById("edit_doc_prof").style.display="block";
		document.getElementById("doc_prof").style.display="none";
		document.getElementById("edit").style.display="none";
		document.getElementById("change_pass").style.display="none";
}

function uploadPhoto()
{
		document.getElementById("edit_doc_photo_upload").style.display="block";
		document.getElementById("doc_photo_upload").style.display="none";
}

function changePassDiv()
{
		document.getElementById("change_doc_pass_prof").style.display="block";
		document.getElementById("doc_prof").style.display="none";
		document.getElementById("edit").style.display="none";
		document.getElementById("edit_doc_prof").style.display="none";
		document.getElementById("change_pass").style.display="none";
}

</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="220" valign="top"><!--<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" /> -->
<?php include "doc_phr_left_menu.php"; ?>
</td>
<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>My Profile</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	<div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>
        </div>
    </td>
</tr>
</table>

<table height="30"></table>

<table width="650" border="0">
<tr><td width="467"><a href="#" style="float:right;">
  <input type="button" name="edit" id="edit" value="Edit Profile" onclick="changeDocProfEditDiv()"/></a></td>
	<!--<td width="173"><a href="#" style="float:left;">
	  <input type="button" name="change_pass" id="change_pass" value="Change Password" onclick="changePassDiv()"/></a></td>-->
      </tr>


<tr><td colspan="2">

<div id="doc_prof" style="display:block;">

<table border="0" cellspacing="10" align="left" style="border:1px solid #CCC;"  width="644">
<tr><td colspan="2" align="left"><span style="color: #0a74d8; font-family:Arial,Verdana;"><b>Personal Details  </b></span></td></tr>
<tr><td>
<div style="font-family:Arial,Verdana;">
<table width="368" border="0" cellspacing="5" align="left">

<!--------All edit action done with `doc_id`='29'------------->
<?php
$qry= "SELECT * FROM `pw_doctors` where `doc_id`='$doctor_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
		
?>	
<tr><td width="152" align="right"><div class="label">Full Name :</div></td>
<td width="206"><div class="text"><?php echo $result['doc_name']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label">Address :</div></td>
	<td><div class="text"><?php echo $result['addressline_1']; ?>.<?php echo $result['address_line2']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label">Clinical Address :</div></td>
    <td><div class="text"><?php echo $result['doc_clinical_address']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label">Doctor ID :</div></td>
    <td><div class="text"><?php echo $result['doc_id']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label">Gender :</div></td>
    <td><div class="text"><?php echo $result['doc_gender']; ?></div></td>
</tr>
<tr>
    <td><span style="color: #0a74d8; font-family:Arial,Verdana;"><h4>Contact Details:</h4></span></td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td align="right"><div class="label">Phone Number :</div></td>
    <td><div class="text"><?php echo $result['tele_consult_phno']; ?></div></td>
</tr>
    <tr><td align="right"><div class="label">Mobile :</div></td>
    <td><div class="text"><?php echo $result['doc_mobile_no']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label">Email ID :</div></td>
    <td><div class="text"><?php echo $result['doc_email_id']; ?></div></td>
</tr>
</table>
</div>
</td>
  <td align="center">
  <table align="left">
  <tr valign="top"><td align="center"><img src="<?php echo $result['doc_photo']; ?>" width="91" height="97" /></td></tr>
  <tr><td align="center"><a href="#">
	   <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
  </table>
<div id="edit_doc_photo_upload" style="display:none;">
  <table align="left">
  <form name="doc_prof_photo_form" id="doc_prof_photo_form" action="actions/doc_prof_photo_upload.php" method="post" enctype="multipart/form-data">
  <tr><td><span style="color: #0a74d8"><b>Select Photo :</b></span></td></tr>
  <input type="hidden" id="doctor_id" name="doctor_id" value="<?php echo $result['doc_id']; ?>"/>
  <input type="hidden" id="doctor_photo" name="doctor_photo" value="<?php echo $result['doc_photo']; ?>"/>
  <tr><td align="center"><input type="file" name="photo"  /></td></tr>
  <tr><td align="center"><input type="submit" name="cancel" value="Save"/></td></tr>
   <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    </form>
  </table>
</div>  
  <?php 
    }
	?>
</td></tr>
</table>
</div>


<div id="edit_doc_prof" style="display: none;font-family:Arial,Verdana;">
<table width="644" border="0" align="left" cellspacing="10" style="border:1px solid #CCC;" >
<tr><td>
<form name="doc_prof_form" id="doc_prof_form" action="actions/edit_doc_prof.php" method="post" enctype="multipart/form-data">
<table width="620" border="0" class="s90dbdtbls_drdetailstbl" align="center">

<!--------All edit action done with `doc_id`='29'------------->
<?php
$qry= "SELECT * FROM `pw_doctors` where `doc_id`='$doctor_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
		
?>	

<tr>
<td width="116">&nbsp;</td>
<td width="144"  align="right"><div class="label">Full Name :</div></td>
<input type="hidden" id="doctor_id" name="doctor_id" value="<?php echo $result['doc_id']; ?>"/>
<td width="218"><input type="text" name="doc_name" id="doc_name" value="<?php echo $result['doc_name']; ?>"/></td>
<td width="124">&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Doctor ID :</div></td><td width="218"><input type="text" name="doc_id" id="doc_id" disabled="disabled" value="<?php echo $result['doc_id']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Address Line1 :</div></td><td><input type="text" name="addressline_1" id="addressline_1"  value="<?php echo $result['addressline_1']; ?>"/></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Address Line2 :</div></td><td><input type="text" name="addressline_2" id="addressline_2"  value="<?php echo $result['address_line2']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Gender :</div></td><td><select  name="doc_gender" id="doc_gender" > 
    	<option value="">-Select-</option>
        <option value="male">Male</option>
        <option value="Female">Femail</option>
        </select></td>
    <script type="text/javascript">
	type='<?php echo $result['doc_gender']; ?>';
	document.getElementById('doc_gender').value=type;
	document.getElementById('doc_gender').selected=true;
	</script>
    <td>&nbsp;</td>
    </tr>
    
 <tr>
 <td>&nbsp;</td>
 <td align="right" valign="top"><div class="label">Email ID :</div></td><td><input type="text" name="doc_email_id" id="doc_email_id" value="<?php echo $result['doc_email_id']; ?>"/>
 <div id="confi_doc_email_div" style="color: #F33;font-family:verdana;font-size:10px; "></div>
 </td>  
 <td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Phone :</div></td><td><input type="text" name="doc_land_line_number" id="doc_land_line_number" value="<?php echo $result['tele_consult_phno']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Mobile :</div></td><td><input type="text" name="doc_mobile_number" id="doc_mobile_number" value="<?php echo $result['doc_mobile_no']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Clinical Address  :</div></td><td><textarea name="clinical_address"  id="clinical_address" style="font-family:Arial,Verdana;font-size:14px;"><?php echo $result['doc_clinical_address']; ?></textarea></td>
<td>&nbsp;</td>
</tr>

<input type="hidden" id="doctor_id" name="doctor_id" value="<?php echo $result['doc_id']; ?>"/>
<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Select Photo :</div></td>
 <td align="left"><input type="file" name="photo"/></td>
 <td>&nbsp;</td>
 </tr>
        <tr>
        <td>&nbsp;</td>
            <td align="center" style="margin-right:10px;">&nbsp;</td>
        <td align="left"><span style="margin-right:10px;">
          <input type="button" name="medication_save" id="medication_save" value="Save" onclick="chechDocEmailid(doc_prof_form)"/>
          <a href="doc_phr_profile.php">
          <input type="button" name="medication_cancel" id="medication_cancel" value="Cancel" />
          </a></span></td> 
          <td>&nbsp;</td>      
        </tr>
        
<tr><td>&nbsp;</td>
<td colspan="3" align="center"> Do you want to <span style="font-style:italic;">change your password ? </span><b><a onmouseover="this.style.cursor='pointer'" onclick="changePassDiv()" >Click Here</a></b></td>        
</table>
</form>
<?php 
}
?>
</td></tr>
</table>
</div>


<!--	#######################  Doctor Emailid Checking  ###################################-->

<script type="text/javascript">
function chechDocEmailid (form)
{


	var paramString = 'docId='+ form.doc_id.value + '&email=' + form.doc_email_id.value;  


		$.ajax({  
			type: "POST",  
			url: "check_doc_emailid.php",  
			data: paramString,  
			success: function(response) {  
				if(response == 'success')
				{
					document.getElementById('doc_prof_form').submit();
				}
				else
				{
					document.getElementById('confi_doc_email_div').innerHTML ="Email address already exists";
				}	
			}
			  
		});  

}
</script>

<!--	##################### End Doctor Emailid Checking #######################-->




<div id="change_doc_pass_prof" style="display: none;font-family:Arial,Verdana;">
<table width="644" border="0" align="center" cellspacing="10" style="border:1px solid #CCC;" class="s90dbdtbls_drdetailstbl">
<tr><td>
<form name="doc_change_pass_form" id="doc_change_pass_form" action="actions/edit_doc_change_pass.php" method="post">
<table width="580" border="0" align="center">

<tr><td width="185" align="right"><div class="label">User Name :</div></td>
<input type="hidden" id="doctor_id" name="doctor_id" value="<?php echo $doc_email_id;?>" />
<td width="155"><input type="text" name="doc_email_id" id="doc_email_id" value="<?php echo $doc_email_id;?>" disabled="disabled"/></td>
<td width="226">&nbsp;</td>
</tr>
<tr><td align="right"><div class="label">New Password :</div></td><td><input type="password" name="new_password" id="new_password" /></td>

<!--    ERROR DIV -->
		
             <td  align="left" height="8">
	            <div id="pswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
 
<!--  END ERROR DIV --> 

</tr>



<tr>
<td align="right"><div class="label">Confirm Password :</div></td>
<td><input type="password" name="confirm_password" id="confirm_password" /></td>
<!--    ERROR DIV -->
	
             <td  align="left" height="8">
	            <div id="repswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
<!--  END ERROR DIV --> 

</tr>
<tr>
<td></td>
<td colspan="2" ><div id="machpswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div></td>

</tr>

<tr>
    <td>&nbsp;</td>
    <td align="left"><input type="button" name="doc_change_pass_save" id="doc_change_pass_save" value="Save"  onclick="validate_pswd(doc_change_pass_form)" />
    <a href="doc_phr_profile.php"><input type="button" name="doc_change_pass_cancel" id="doc_change_pass_cancel" value="Cancel" /></a></td>
</tr>
</table>
</form>
<script type="text/javascript">
function validate_pswd(form)
{
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_password.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Please enter your new password";
    	pswValidated = false;
	}
	if (form.confirm_password.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Please enter your new password again";
		repswValidated = false;
	}
	else if(form.confirm_password.value!='')
	{
		if(form.new_password.value!=form.confirm_password.value)
		{
			document.getElementById("machpswErrDiv").innerHTML = "Password mismatch Try again!";
			pswValidated = false;
			repswValidated = false;
		}
	}
	
	if(pswValidated && repswValidated )
	{
		
		document.getElementById('doc_change_pass_form').submit();
	}
	else
	{
		return false;
	}
	
}

</script>


</td></tr>
</table>
</div>

</td></tr>
</table>
</td>
</tr></table>

<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
