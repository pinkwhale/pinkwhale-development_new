<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head><body>

<!-- header.......-->
<?php include 'header.php'; ?>
<!-- header.......-->
    <div id="subSlider">  
<h1>Join</h1>
</div>
       <div id="content"> 


<table width="1000" border="0" cellspacing="10" cellpadding="0" class="s90cardstbl" style="clear:both; margin:0px auto">
<tr><td width="154" valign="top"><h1>About Us</h1></td>
<td width="812" valign="top" class="s90contactpage">
<p>
pinkWhale is an e-Health company offering integrated online and telephone based personal healthcare services.� Our goal is to give consumers and businesses quick, easy, and convenient access to the best health care from wherever they are, using a state-of-the-art technology platform built on a proprietary software framework.� We are building the best network of health specialists, healthcare providers, and partners who are committed to providing health consultations by phone or online.
pinkWhale aims to revolutionize the way healthcare is being delivered in India.</p>
</td></tr>
<tr><td width="154" valign="top"><h1>Management Team</h1></td>
<td width="812" valign="top">
<p>
<p> pinkWhale is led by a diverse, dynamic, and  dedicated team of highly committed professionals who are <font color="darkpink" size=3><strong>PASSIONATE </strong></font> about providing convenient access to healthcare.  
We believe that <font color="darkpink" size=3><strong>IT IS POSSIBLE </strong></font> to make it easier for patients to access the best health specialists, and for healthcare providers to provide the best care to their patients.  We are united by this <font color="darkpink" size=3><strong> VISION </strong></font> . 
<p> We <font color="darkpink" size=3><strong> UNDERSTAND </strong></font> that the problem of convenient access to healthcare cannot be solved by any single doctor, clinic, hospital, diagnostic lab, or care provider.  We believe strongly that health encompasses physical and emotional well-being and we want to provide value to our customers in both of these areas.  
We are <font color="darkpink" size=3><strong>ON A MISSION </strong></font> to build the best health specialist network for easy access and convenience of our customers. 
<p> The leadership team has a unique combination of healthcare, technology, and business experience across industries.  The team is composed of experts, who have led long and distinguished careers in top multinational organizations.
</p>
</td></tr></table>


<table width="80%" border="0" cellspacing="0" cellpadding="0"  align="center">
<tr><td width="180" valign="top"><img src="images/cp_mascott2.jpg" width="127" /></td>
<td><h3> Anita Shet <font color="black"> <br> Chairman & Chief Executive Officer </font> </p> </h3>
<p>Anita has over 15 years of experience in semiconductor industry.  She spent over 14 years at Texas Instruments where she was the worldwide product manager of Multimedia Codec.
She has B.E in Electrical Engineering from UVCE Bangalore, and a Diploma in Computer Networks & Applications from Digital India Ltd..</p></td></tr>


<tr><td><img src="images/cp_mascott2.jpg" width="127" /></td>
<td><h3> Dr. Rekha B S. <font color="black"> <br> Chief Medical Officer </font> </p> </h3>
<p>Dr. Rekha has practiced medicine in leading hospitals in Bangalore.  She was a Consultant Ophthalmologist at Mallya hospital, and at Mahaveer Jain hospital prior to that.  She is actively involved in social organizations and has participated in multiple eye camps in rural areas. She has a MBBS and a MS in Ophthalmology from KIMS, and F.G.O from L.E.H.</p></td></tr>
<!--
<tr><td valign="top"><img src="images/cp_mascott2.jpg" width="127" /></td>
<td><h3>Dr. Sai Subramaniam <font color="black"> <br> Chief Operating Officer </font> </p> </h3>
<p>Sai has over 15 years of experience in Life Sciences & Healthcare Industry.  His international experience in the pharmaceutical industry includes stints at Merck, and Biogen Idec.  He has a Ph.D in Biochemistry from Oregon Health & Sciences University (OGI), Oregon, USA, and a MBA from Babson College, Massachusetts, USA..</p></td></tr>
-->
</table>


       
<?php
	include 'footer.php';
 ?>


</body></html>
