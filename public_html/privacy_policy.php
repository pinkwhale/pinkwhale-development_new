<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>pinkwhalehealthcare</title>
        <meta name="description" content="pinkwhalehealthcare">
            <link href="css/designstyles.css" rel="stylesheet" type="text/css">
                <!-- ------------------------------   google analytics    ------------------------------------------- -->
                <script type="text/javascript">

                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-23649814-1']);
                    _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
                    _gaq.push(['_trackPageview']);

                    (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();

                </script>
                <!--  --------------------------------------     END         -------------------------------------------------- --></head><body>
                    <?php
                    include 'main_header.php';
                    ?>
                    <!-- header.......-->
                    <div class="section-title  full-bg  " style="background-image:url(slider/slide_bg1.jpg)">
                        <div class="container">
                            <h2 class="animated bounceInDown">Privacy Policy</h2>
                        </div>    
                    </div>
                    <!-- /section title -->

                        <div class="container">
                            <section>   
                                <div class="row" >

                                    <div class="span12 clearfix" >
                                        <div class="inner">
                                            
                                            <p><b>1.</b> &nbsp;  Your privacy is important to you and to us. We are totally committed in protecting the information you share with us. We created this Website these policies (together "Privacy Policy") to give you confidence as you visit and use the Website, and to demonstrate our commitment to fair information practices and the protection of privacy.</p>
                                            <p><b>2.</b> &nbsp;  In the course of registering as a member and thereby availing our Services on <a href="index.php" target="_blank">pinkwhalehealthcare.com</a> (the "Site"), the Site may require you to provide your contact information viz. e-mail address, your name and information about your health, and other personal information ("Personal Information"), that it collects, retains and uses for the purpose of providing you Services.</p>
                                            <p><b>3.</b> &nbsp;  For the purposes of offering you Services and providing appropriate advice, your Personal Information may be seen and used by our general physicians/health specialist/wellness specialist/expert ("Professionals"). In some situations in order to provide us with technical services for the operation and maintenance of our Site, some of the technical or mechanical contractors will have access to your Personal Information, on a need to know basis.<br />
                                                The Site strictly protects the security of your personal information and honors your choices for its intended use. We carefully protect your data from loss, misuse, unauthorized access or disclosure, alteration, or destruction.</p>
                                            <p><b>4.</b> &nbsp;  The Site seeks to maintain the accuracy, correctness and completeness of the information including Personal Information provided by the visitors, subscribers, and non-subscribers. Therefore, you are required to provide the Site, at all times, with complete and accurate information.</p>
                                            <p><b>5.</b> &nbsp;  We shall be the sole owner of the Personal Information provided by you. We shall not sell or rent your Personal Information. The Site shall simply act as a facilitator and procure the information and share the same with our Professionals in order to facilitate services to you. However, there may be occasions wherein the law compels the Site to disclose your Personal Information. </p>
                                            <p><b>6.</b> &nbsp;  We collect internet traffic data, of visits to our website. This includes IP address of all visitors to the Site, and a number is automatically assigned to your computer whenever you access the Internet, domain servers, types of computers, types of web browsers (together called "Traffic Data"). We collect IP address information in order to administer our system and gather aggregate information, to avoid any scope of misuse of the Site. The Traffic Data information may be shared with advertisers, sponsors and other businesses on aggregate basis, and not on an individual, personally-identifiable basis.</p>

                                            <p><b>7.</b> &nbsp;  This Site does not intend to collect any personally identifiable information of children below the age of 18, without the guidance of the parent or guardian. The Site, on receiving any notification, shall ensure to delete such information. </p>
                                            <p><b>8.</b> &nbsp;  This Site contains links to other sites. Please note that pinkWhale is not responsible for the privacy policies of such other sites. We encourage you to be aware when they leave our site and read the privacy statements of each and every website that collects personally identifiable information.</p>
                                            <p><b>9.</b> &nbsp;  We will occasionally amend this Privacy Policy. Please ensure that you periodically update yourself with the current version of the Privacy Policy.</p>
                                                      
                                        </div><!-- /inner -->
                                    </div><!-- /span12 -->			

                                </div><!-- /row-->


                            </section>
                        </div> <!-- /container -->    

                        <!-- footer -->

                        <?php include 'footer.php'; ?>

                </body></html>
