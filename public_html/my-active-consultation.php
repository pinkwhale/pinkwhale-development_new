<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']) || $_SESSION['login']!='user')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script language="JavaScript">
<!--
function submit_form()
{
	document.getElementById('email_cnsltion_form').submit();
}
-->
</script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<!-- header.......--><?php
include 'header.php'; 
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- menu.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- Menu.......-->
</td>
<td width="748" valign="top" class="s90phrcontent">
<?php include("name_card_no.php");?>
</div>
<table border="0" cellpadding="0" cellspacing="0" style="margin-top:20px;">

	<tr><td>
			<span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;margin-left:20px;">Ongoing Consultations</span>
		</td></tr>	
	<tr><td height='8px'></td></tr>
<tr><td>
<a name="mac" id="mac"></a>
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl">
	<tr><th>Specialist Online Consults</th></tr>
	<tr><td class="s90dbdtbls_drname"></td></tr>
	<tr><td class="s90dbdtbls_drdetails">
    
    <?php
		$qry6   = "SELECT max(mail_serial_no),min(mail_serial_no) FROM `pw_consultation_emails` WHERE `pw_card_id`='$user_id' group by `consultation_no` ";
	$qry_rslt6 = mysql_query($qry6);
	$recordsAvailbale = false;
	while($result6= mysql_fetch_array($qry_rslt6))
	{
		$max_serial_no=$result6['max(mail_serial_no)'];
		$min_serial_no=$result6['min(mail_serial_no)'];
	$qry17   = "SELECT `complaint` FROM `pw_consultation_emails` WHERE `mail_serial_no`='$min_serial_no' ";
	$qry_rslt17 = mysql_query($qry17);
	while($result17 = mysql_fetch_array($qry_rslt17))
	{
		$complaint=$result17['complaint'];
	}
	
	$qry7   = "SELECT * FROM `pw_consultation_emails` WHERE `mail_serial_no`='$max_serial_no' && `mail_status`!='closed' ";
	$qry_rslt7 = mysql_query($qry7);
	while($result7 = mysql_fetch_array($qry_rslt7))
	{
		$recordsAvailbale = true;
		$gdate1=strtotime($result7['entered_date']);
		$final_date1=date("d M Y", $gdate1);
?>
		<table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
		<tr>
        	<td width="180"><?PHP echo $result7['doctor_name']; ?></td>
            <td width="100"><?PHP echo $final_date1; ?></td>
            <td width="140"><?PHP echo $complaint; ?></td>
            <td width="120">
                <form method="post" action="patient_email_consultation.php" name="email_cnsltion_form"> 
                    <input type="hidden" name="cnsltion_id" value="<?php echo $result7['consultation_no']; ?>"  />
                    <input type="image" src="images/continue_consultation.jpg" onclick="submit_form();"  />
                </form>
            </td>
        </tr>
		</table>
<?php } 
	}
	
	if(!$recordsAvailbale) echo htmlForNoRecords("580" ,false);
?>        
</td></tr></table>
</td></tr>
<tr><td height="10"></td></tr>
<!-- /////////////////////////    expert  ////////////////////// -->
<tr><td>
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls tbl">
	<tr><th>Expert Online Consults</th></tr>
	<tr><td class="s90dbdtbls_drname"></td></tr>
	<tr><td class="s90dbdtbls_drdetails">
    
    <?php
		$qry8   = "SELECT max(mail_id),min(mail_id)  FROM `pw_expert_consultation_emails` WHERE `pw_card_id`='$user_id' group by `consultation_id` ";
	$qry_rslt8 = mysql_query($qry8);
	$recordsAvailbale = false;
	while($result8 = mysql_fetch_array($qry_rslt8))
	{
		$max_serial_no=$result8['max(mail_id)'];
		$min_serial_no=$result8['min(mail_id)'];
	
	$qry19   = "SELECT complaint FROM `pw_expert_consultation_emails` WHERE `mail_id`='$min_serial_no' ";
	$qry_rslt19 = mysql_query($qry19);
	while($result19 = mysql_fetch_array($qry_rslt19))
	{
		$complaint1=$result19['complaint'];
	}
	
	$qry9   = "SELECT * FROM `pw_expert_consultation_emails` WHERE `mail_id`='$max_serial_no' && `mail_status`!='closed' ";
	$qry_rslt9 = mysql_query($qry9);
	while($result9 = mysql_fetch_array($qry_rslt9))
	{
		$recordsAvailbale = true;
		$gdate2=strtotime($result9['entered_date']);
		$final_date2=date("d M Y", $gdate2);
?>
		<table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
		<tr>
        	<td width="180"><?PHP echo $result9['doctor_name']; ?></td>
            <td width="100"><?PHP echo $final_date2; ?></td>
            <td width="140"><?PHP echo $complaint1; ?></td>
            <td width="120">
                <form method="post" action="patient_exp_email_consultation.php" name="email_cnsltion_form"> 
                    <input type="hidden" name="cnsltion_id" value="<?php echo $result9['consultation_id']; ?>"  />
                    <input type="image" src="images/continue_consultation.jpg" onclick="submit_form();"  />
                </form>
            </td>
        </tr>
		</table>
<?php } }

	if(!$recordsAvailbale) echo htmlForNoRecords("580" ,false);
	?>        
</td></tr></table>
</td></tr>
<!-- /////////////////////////     end expert  ////////////////  -->
</table>

<table>
	<tr>
		<td height='15px'></td>
	</tr>
</table>
</td></tr>
<tr>
  <td valign="top"><table border="0" width="217" cellspacing="10px">
                <tr><td align="center"><a href=" ask_new_q&a_counsellor.php"><img src="images/Ask_a_councellor1.png" width="195" height="37"/></a></td></tr>
                <tr><td align="center"><a href="ask_new_q&a_dietician.php"><img src="images/Ask_a_dietician1.jpg" width="195" height="37" /></a></td></tr>  
                <tr><td align="center"><a href="ask_naw_q&a_doctor.php"><img src="images/Ask_a_doc1.jpg" width="195" height="37" /></a></td></tr>  
                </table>&nbsp;</td>
  <td valign="top" class="s90phrcontent">&nbsp;</td>
</tr>
</table>


<!-- ---------- menu selection    ---------------    -->
<script type="text/javascript">
	enable_active_cnsltion_menu()
</script>
<!----------- END menu selection    ----------------- -->

<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
