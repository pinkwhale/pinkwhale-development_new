<?php
session_start();
include "actions/encdec.php";
include("site_config.php"); 
include "db_connect.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Find a Specialist Doctor | pinkWhale Healthcare</title>
<meta name="keywords" content="specialist, doctor, online doctor, online consultation, telephone consultation, teleconsultation"/>
<meta name="description" content="If you are looking for convenient access to the best specialist doctors, you search ends here. We have a panel of qualified and renowned specialist doctors in several medical areas. Follow-up consultation with doctor has never been this easy.  Our specialists provide the convenience of phone consultation or online consultation for follow-up questions and visits. Find Your Specialist Now!"/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="js/stepcarousel.js" type="text/javascript"></script>
<script src="js/carousel.js" type="text/javascript"></script>
<script src="js/search.js" type="text/javascript"></script>
<script src="js/login.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>
    #loading { 
        width: 100%; 
        position: absolute;
    }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<script src="js/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/header_colourchange.js"></script>
<script src="js/mouseoverscripts.js" type="text/javascript"></script>
<script src="js/NoIEActivate.js" type="text/javascript"></script>

<!-- header.......-->
<?php

 include 'header.php';
 include "site_config.php";
 ?>
<!-- header.......-->
<!--<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold'</script> -->
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td colspan="2"><div id="consult_banner">
<div id="banner_titles2"><h3>Find YOUR Specialist</h3><h4>& Start your E-Consult</h4></div> <br clear="all" />
<div id="banner_fields"><div class="txt">Doctors Name: </div><div class="inpt"><input name="doc_name" type="text" id="doc_name" size="25" /></div> 
<div class="txt">Specialty: </div><div class="inpt"><select name="specialist" id="specialist"  style="width:200px">
    <option value=""> -- Specialist  -- </option>
    <?php
    $qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE `doc_category`='Specialist' && blocked !='Y' and `doc_specialities`<>'' group by`doc_specialities`";
    if (!$qry_rslt = mysql_query($qry))
        die(mysql_error());
    while ($qry_result = mysql_fetch_array($qry_rslt)) {
        echo "<option value=\"$qry_result[0]\">$qry_result[0]</option>";
    }
    ?>
</select></div> <div class="txt"> </div><div class="inpt"><input name="input" type="button"  class="findSpe"  value=""  onclick="search_health_expert()"/>
</div></div>
</div></td></tr></table>


<div id="loading" align="center"></div>
<div id="spec_search"></div>
<div id="spe_details"> 

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
    <div class="cont_big">  <h5><span>pink</span>Consult</h5>
 Follow-up consultation with your Specialist has never been this easy &amp; Convenient ! This Service allows you to conveniently  follow-up with your own doctor  by phone or online.&nbsp;Find your Specialist here &amp; see if he/she is providing this  convenience to you. <br/>
<!-- <p style="color:#600">&nbsp;<strong>NOT able to find your  specialist !&nbsp; Refer  them  to us by writing to <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a> and  we will get in touch with them.</strong></div>
    </p>-->
</td>
</tr>
<tr><td colspan="2" class="notfindingspec"><h5>NOT Finding YOUR Specialist in our Network?  Write to <a href="mailto:info@pinkwhalehealthcare.com">info@pinkwhalehealthcare.com</a> or Call 080-43000333</h5></td></tr>
<tr>
<td align="right" style="padding:0px">
<img src="images/heasy_steps3.jpg"  />
<?php 
if(!isset($_SESSION['login']) || $_SESSION['login']=='Card' )
{
?>

<a href="join.php"><img src="images/signUpNow.gif"  /></a>
<a href="#"><img src="images/topupCard.gif" onclick="callpopup(true)"  /></a>


<?php } ?>
</td>
</tr>
</table>

</div>
<!-- footer -->

<?php
include 'footer.php'; ?>

 <!-- login popup start -->
  
 <div id="login_popupId" style="display:none; z-index: 10008; left:33%; height:220px;  width:406px; height: auto; position: absolute; top:20%; font-family:Arial; font-size:13px;
   border:4px solid #666; background:url('images/bg-btn-blue.png') repeat-x ; background-color:#fff;" > 
    
    <div style="float:right ; padding:5px;"> <a href="#" onclick="login_callpopup(false,0)"><img src="images/close.gif" border="0" /></a> </div>
       <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; "><center> Login </center></div> <br/>
    <div id="loginarea">

    <br/>
    <form name="consult_signin" id="consult_signin" method="post" action="<?= $sitePrefix."/patient_new_email_consultation.php" ?>">
        <div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
        
        <div class="loginrow"> <span class="label_logn"> UserName : </span>
          <span class="forml_logn">
            <input name="username" id="username"  class="input" type="text"  size="18" />
          </span>
        </div>
        
            <div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        
        <div class="loginrow"> <span class="label_logn">Password :</span> 
          <span class="forml_logn">
            <input name="password" id="password" type="password"   class="input"  size="18"  />
          </span>
        </div>
            <div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
        <div class="loginrow"></div>
        <div class="loginrow"> <span class="label_logn">&nbsp;</span> 
        <span ><input type="hidden" name="doc_id" id="doc_id" />
            <input type="submit" id=""  value=""  class="signinBtn"  title="Login" onclick="return consult_LoginSignUp(consult_signin)"/>
        </span> </div>
    </form>

  </div>
</div>

<div id="login_divDisable" style="DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48"> </div>

 <!-- login popup End -->
 
</body></html>


