<?php
session_start();
include "site_config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Ask a Query: Ask Health & Wellness Questions from Anywhere | pinkWhale Healthcare</title>
<meta name="keywords" content="Seasonal Diseases, Diabetes, Hypertension, Arthritis, Asthma, Allergies, Heart Burn, Conjunctivitis, Glaucoma, Gastric Disorder, Heartburn, Irritable Bowels, Oral Health, Women's Health, Pregnancy, Infertility, Breastfeeding, Child Development, Immunizations, Diet, Exercise, Nutrition, Weight Loss, Obesity,  Acne, Hair Loss, Men's Health, Hepatitis, Sexual Disorders, Skin Disorders, Aches/Pains, Fatigue, Stress, Anxiety, Parenting, Work/Life Balance, Relationships, Depression, Alcohol, Smoking, Addiction, Medical Procedure Information, Medication Side Effects, Signs & Symptoms, Preventive Check-ups, Specialist Referrals, Getting Medical Second Opinion"/>
<meta name="description" content="Professional help from certified & licensed physicians, emotional counsellors, well-being coaches, and dieticians"/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="images/mouseoverscripts.js" language="JavaScript" type="text/javascript"></script>
<script src="js/login.js" language="JavaScript" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<!-------------------------- header..------------------------------>
<?php
	include 'header.php'; 
?>
<!------------------------- header.----------------------------------------->
<form method="post" id="signin1" action="<?php echo $sitePrefix; ?>/login_action1.php" name="login_name">
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td colspan="2"><div id="askquery_banner">
<div id="banner_fields">
<div id="banner_titles4"><h3>Have A Query?</h3><h4>Ask Our General Physician</h4></div><br clear="all" />
<div class="txt"> User Name: </div> <div class="inpt"> <input autocomplete="off" id="uname" name="uname" value="" title="username" size="22" type="text" />
   </div>
    <div id="uname_div" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
    <div class="txt">   Password:  </div> <div class="inpt"> 
  <input id="pwd" name="pwd" value="" title="password" size="22" type="password" />
</div>
    <div id="pwd_div" class="error" align="center" style="color: #F33;font-family:verdana;font-size:9px;"></div>
    <div class="txt"> </div> <div class="inpt"> 
    <input name="input" type="button"  class="askbtn"  value=""  onclick="LoginSignUp_ask_query(login_name)" onkeydown="javascript: if (window.event.keyCode == 13) LoginSignUp(login_name); else window.event.keyCode = null; "/></div>
 <i><a href="<?=$sitePrefix."/join.php" ?>"><font color="#3a61ae">New to PinkWhale Register</font></a>  /  <a href="<?=$sitePrefix."/forgot_password.php" ?>">Forgot your password?</a></i></div>
</div></td>
</tr>
<tr>
<td>
 <div class="cont_big">  <h5><span>pink</span>Query</h5>
 <p>There are many health questions and concerns that we have that doesn't require a physical examination. In such situations, we offer a reliable and a effective alternative, that saves you time, energy, and money. Our certified & licensed physician network and health experts will help address these concerns for you from the convenience of your location.</p>
</div>
</td>
</tr>
<tr>
<td align="right" style="padding-top:0px">
<img src="images/heasy_steps2.jpg"  />
<?php 
if(!isset($_SESSION['login']))
{
?>
     <a href="join.php"><img src="images/signUpNow.gif"  /></a>
        <a href="#"><img src="images/topupCard.gif" onclick="callpopup(true)"  /></a>

<?php } ?>
</td>
</tr>

</table>
</form>
<?php
	include 'footer.php';
 ?>
</body></html>
