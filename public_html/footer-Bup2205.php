<?php
include("site_config.php");
?>


<div id="popupId" style="display:none; z-index:9999; right:50px; height:1000px;  width:380px; height: auto; position: absolute; top:190px;   border:4px solid #666; font-family: Arial;  background:url('images/bg-btn-blue.png') repeat-x ; background-color:#fff;"  > 

    <div style="float:right ; padding:5px;"> <a href="#" onclick="callpopup(false)"><img src="images/close.gif" border="0" /></a> </div>

    <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; ">
        <center>Top Up Your Card </center></div>
    <br/>
    <form name="TopUPlogin" id="TopUPlogin" action="card_login.php" method="POST">
        <table align="center"> <tr><td>Card No.</td><td> 
                    <input name="card" id="card"  class="input" type="text" onkeypress="return isNumberKey(event);" autocomplete="off" title=""  size="15" maxlength="6" />
                </td><td colspan="2" >
                    <input type="button" class="btn"   value=" Top Up" name="topupbtn" id="topupbtn"  title="Top Up" onclick="Topup_login(TopUPlogin)"/>
                </td></tr>
        </table>
        <div id="topuplog_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>
        <br />
    </form>
    
</div>

<!-- end contents -->

<!-- login popup start -->

<div id="login_popupId" style="display:none; z-index: 10008; left:25%; height:330px;  width:700px; height: auto; position: absolute; 
     top:20%; font-family:Arial; font-size:13px;  border:4px solid #666; background:url('<?= "$sitePrefix" ?>/images/bg-btn-blue.png') repeat-x ; background-color:#fff;" > 

    <div style="float:right ; padding:5px;"> <a href="#" onclick="login_callpopup(false,0)"><img src="<?= "$sitePrefix" ?>/images/close.gif" border="0" /></a> </div>
    <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; "><center> Sign In/Register </center></div> <br/>  
    <div id="login_message" style="display:none"></div>
    <div id="login_body">
        <table>
            <tr>
                <td>
                    <div id="loginarea">
                        <br/>
                        <form name="consult_signin" id="consult_signin" method="post" action="<?= $sitePrefix . "/login_action.php" ?>">
                            <div id="log_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:14px;"></div>

                            <div class="loginrow"> <span class="label_logn"> UserName : </span>
                                <span class="forml_logn">
                                    <input name="username" id="username"  class="input" type="text"  size="18" />
                                </span>
                            </div>

                            <div id="username_errordiv" align="center" value="error" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>

                            <div class="loginrow" style="margin-top:10px;"> <span class="label_logn">Password :</span> 
                                <span class="forml_logn" style="margin:0 0 0 5px ;">
                                    <input name="password" id="password" type="password"   class="input"  size="18"  />
                                </span>
                            </div>
                            <div id="password_errordiv" value="error" align="center" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
                            <div class="loginrow"></div>
                            <div class="loginrow" align="center"> 
                                <span class="label_logn">&nbsp;</span> 
                                <span >
                                    <input type="hidden" name="doc_id" id="doc_id" />
                                    <input type="submit" class="btn" id="login_submit" style="cursor:pointer" value="Sign In"   title="Login" onclick="return consult_LoginSignUp(consult_signin)"/>
                                </span> 
                            </div>
                        </form>

                    </div>
                    <br />
                </td>
                <td rowspan="2">
                    <div  >
                        <p class="forgot" align="center"><span style="color:#000000;"><b>Register </b></span></p>
                        <form name="reg_form" id="reg_form" >
                            <table>
                                <tr>	
                                    <td width="40%" align="right">Your Name<font color="#FF0000">*</font>:</td>
                                    <td width="60%" align="left">
                                        <input size="20" type="text" maxlength="40" name="regname" id="regname" value="" class="s90regformtext">
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>	
                                    <td width="40%" align="right">Age<font color="#FF0000">*</font>:</td>
                                    <td width="60%" align="left">
                                        <input size="20" type="text" maxlength="2" onkeypress="return isNumberKey(event);" name="regage" id="regage" value="" class="s90regformtext">
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV -->
                                <tr>	
                                    <td width="40%" align="right">Gender<font color="#FF0000">*</font>:</td>
                                    <td width="60%" align="left">
                                        <select name="reggender" id="reggender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>
                                    <td align="right">Card No.<font color="#FF0000"></font>:<span onmouseover="showsummary('details', event, 'Pinkwhale Card');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
                                    <td nowrap="nowrap">
                                        <input class="s90regformtext" type="text" maxlength="45" size="20" onblur="return card_check(this.value)" id="regcard" name="regcard" value="" >
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>
                                    <td align="right">Your Email<font color="#FF0000">*</font>:<span onmouseover="showsummary('details', event, 'To be used as User ID');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
                                    <td nowrap="nowrap">
                                        <input class="s90regformtext" type="text" maxlength="45" size="20" id="regemail" name="regemail" value="" >
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>
                                    <td align="right">Password<font color="#FF0000">*</font>:<span onmouseover="showsummary('details', event, 'Please enter password of your choice with a minimum of 6 characters and a maximum of 12 characters.');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
                                    <td>
                                        <input type="password" class="s90regformtext" maxlength="12" size="20"  id="regPassword" name="regPassword" value=""><span style="color:green" id='result'></span>
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>
                                    <td align="right">Confirm Password<font color="#FF0000">*</font>:</td>
                                    <td>
                                        <input type="password" class="s90regformtext" maxlength="12" size="20" name="regConfirmPassword" id="regConfirmPassword" value="">
                                    </td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV --> 
                                <tr>
                                    <td align="right" style="line-height:20px;">Mobile #<font color="#FF0000">*</font>:</td>
                                    <td style="line-height:30px;">
                                        <input type="text" class="s90regformtext" maxlength="20" name="regPhone1" id="regPhone1"  value="" onkeypress="return isNumberKey(event);"></td>
                                </tr>
                                <!--    ERROR DIV -->
                                <tr>
                                    <td> </td>
                                    <td  align="left" height="8">
                                        <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                    </td>
                                </tr>
                                <!--  END ERROR DIV -->
                                <tr>
                                    <td height="50" colspan="2" align="center" >
                                        <input class="btn" type="button" name="btnSubmit" id="btnSubmit" value=" Register " onclick="register_pink(reg_form)" >
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <div  >
                        <form name="forget" id="forget" >
                            <p class="forgot" ><span style="color:#000000;"><b>Forgot your password?</b></span></p>
                            <p>Your Email-ID &nbsp; : &nbsp; <input autocomplete="off" value="" name="forgot_email" id="forgot_email" maxlength="50" /></p>
                            <p><div id="forgotemailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></p>
                            <div class="loginrow" align="center">
                                <input name="send"  id="for_send" class="btn" value="submit" onclick="return forgot_password(forget);" type="submit" /> 
                            </div>
                        </form>
                    </div>
            </tr>
        </table>
    </div>
</div>

<div id="login_divDisable" style="DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48"> </div>

<!-- login popup End -->

<div di="clear"></div>    


<div class="footer" > 

<div class="copyright">
    <div class="container">
        <a href="http://www.pinkwhalehealthcare.com/"> &copy; Copyright 2013, www.pinkwhalehealthcare.com</a>
        <ul class="footer-link pull-right">
            <li> <a href="<?php echo $sitePrefix ?>/index.php">Home </a></li>
            <li><a href="<?php echo $sitePrefix ?>/about_us.php">About Us</a></li>
            <li><a href="http://blog.pinkwhalehealthcare.com/"> Wellness Blog</a></li>
	    <li><a href="<?php echo $sitePrefix ?>/appointment_search.php">Book Appointment</a></li>
            <li><a href="<?php echo $sitePrefix ?>/contact_us.php">Contact Us</a> </li>
            <li><a href="<?php echo $sitePrefix ?>/doctors_list.php">Doctors List</a></li>
            <li><a href="<?php echo $sitePrefix ?>/advertise_with_us.php">Advertise with us</a></li>
            <li><a href="<?php echo $sitePrefix ?>/terms.php">Terms of Service</a></li>
            <li><a href="<?php echo $sitePrefix ?>/privacy_policy.php">Privacy Policy</a></li>
        </ul>
    </div><!-- /container -->   
</div>

</div><!-- End  footer  -->


<script type="text/javascript" src="http://www.pinkwhalehealthcare.com/clickheat/js/clickheat.js"></script><noscript><p><a href="http://www.labsmedia.com/index.html">Seo tools</a></p></noscript><script type="text/javascript"><!--
    clickHeatSite = 'Pinkwhalehealthcare';clickHeatGroup = encodeURIComponent(window.location.pathname+window.location.search);clickHeatQuota = 3;clickHeatServer = 'http://www.pinkwhalehealthcare.com/clickheat/click.php';initClickHeat(); //-->
</script>

