<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php include 'header.php'; ?>
<!-- header.......-->
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<?php 
/*
$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag,activate_online_consultation FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
		$doc_ctgry_flag=$result['doc_category_change_flag'];
		$doc_online_consult = $result['activate_online_consultation'];
	}
?>
<?php
$date1=date("Y-m-d", time()-86400);
$date2=date("Y-m-d");
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top">
<div id="s90dashboardbg">
    <img src="images/dots.gif" />
    <a href="doc_phr.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>
    </div>
    </td>
</tr>
</table>
<table height="30"></table>

<!--   ---------------------         Specialist         ------------------------      -->
<?php
if ($doc_category=='Specialist' && $doc_ctgry_flag=="" && $doc_online_consult=="online")
{
?>
<div style="font-family: Arial;">
<table width="700" style="border:1px solid #CCC;" align="center" >
<tr><td>
<form name="sp_ex_form" id="sp_ex_form" action="doc_phr.php" method="post" >
<table height="32" bgcolor="#EEEEEE" width="700" class="fontset">
<tr><td width="201" height="26" valign="bottom"><span style="color:#5F8BAA;"><b style="font-size:17px">My Account Summary</b></span></td>
<td width="55" valign="top" align="right" ><span style="font-size:14px">From :</span></td>
	<td width="116" valign="top"><?php
	  $myCalendar = new tc_calendar("date1", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="54" valign="top" align="right"><span style="font-size:14px">To :</span></td>
      <td width="124" valign="top"><?php
	  $myCalendar = new tc_calendar("date2", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="65" valign="top"><input type="submit" name="search" value="Go" /></td>
</tr>
</table>
</form>
<?php
if (!$_POST['date1'] && !$_POST['date2']){
	$qry1="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),SUM(online_spe_consultation_reports.balance_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id'";

$qry12="SELECT max(`mail_serial_no`) FROM `pw_consultation_emails` where `doctor_id`='$doctor_id' group by `consultation_no`";
}

if($_POST['date1'] && $_POST['date2'])
{
	$date1=$_POST['date1'];
	$date2=$_POST['date2'];
	
	$qry1="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),SUM(online_spe_consultation_reports.balance_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2'";
	
	$qry12="SELECT max(`mail_serial_no`) FROM `pw_consultation_emails` where `doctor_id`='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' group by `consultation_no`";
	}

$qry_rslt = mysql_query($qry1);
while($row = mysql_fetch_assoc($qry_rslt)) 
{
	$email_session_charge=$row['1_email_session_charge'];
	if($email_session_charge=="") {
			$qry="SELECT `1_email_session_charge` FROM `packages` WHERE `type_of_doctor`='Specialist' && doctor_id='0'";	
			if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
			$email_session_charge = $qry_result['1_email_session_charge'];
		}
	}
	
	$usage_session1 =$row['SUM(online_spe_consultation_reports.usage_session)'];
	$usage_session=$usage_session1+0;
	$balance_session=$row['SUM(online_spe_consultation_reports.balance_session)'] +0;
}
$earning = $email_session_charge * $usage_session;
$completed=0;
$pending=0;
$qry_rslt12 = mysql_query($qry12);
while($row12 = mysql_fetch_assoc($qry_rslt12)) 
{
	$mail_serial_no=$row12['max(`mail_serial_no`)'];
	$qry123="SELECT `mail_status` FROM `pw_consultation_emails` WHERE `mail_serial_no`='$mail_serial_no' ";
	$qry_rslt123 = mysql_query($qry123);
	while($row123 = mysql_fetch_assoc($qry_rslt123))
	{
		$mail_status =$row123['mail_status'];
			
		if($mail_status=='closed')
		{
			$completed=$completed+1;
		}
		
	if($mail_status=='open')
		{
			$pending=$pending+1;
		}
	}
}
?>
<table width="700" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td width="180">&nbsp;</td>
	<td width="90" align="center"><b style="font-size:13px">Used Cards</b></td>
    <td width="95" align="center"><b style="font-size:13px">Earnings</b></td>
    <td width="145" align="center"><b style="font-size:13px">Pending Consultations.</b></td>
    <td width="160" align="center"><b style="font-size:13px">Completed Consultations.</b></td>
</tr>
<!--<tr>
    <td style="font-size:13px">Telephone Consultation</td>
    <td align="center" style="font-size:13px">0</td>
    <td align="center" style="font-size:13px">Rs.0 /-</td>
    <td align="center" style="font-size:13px">0</td>
    <td align="center" style="font-size:13px">0</td>
</tr>-->
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr>
    <td style="font-size:13px">Online Consultation</td>
    <td align="center" style="font-size:13px"><?php echo $usage_session;?></td>
    <td align="center" style="font-size:13px">Rs.<?php echo $earning;?> /-</td>
    <td align="center" style="font-size:13px"><?php echo $pending;?></td>
    <td align="center" style="font-size:13px"><?php echo $completed; ?></td>
</tr>
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr bgcolor="#DDDDDD">
	<td width="180"><b style="font-size:13px;">Total Earnings</b></td>
    <td width="90" align="center" style="font-size:13px"><?php echo $usage_session;?></td>
    <td width="95" align="center" style="font-size:13px">Rs.<?php echo $earning;?> /-</td>
    <td width="145" align="center" style="font-size:13px"><?php echo $pending;?></td>
    <td width="160" align="center" style="font-size:13px"><?php echo $completed; ?></td>
</tr>
</table>
</td></tr>
</table>
</div>
<?php
}
?>
<!--    --------------------        END  Specialist         ------------------------      -->
<!--    --------------------         Expert                 ------------------------    -->
<?php
if ($doc_category=='Expert' && $doc_ctgry_flag=="")
{
?>
<div style="font-family: Arial;">
<table width="700" style="border:1px solid #CCC;" align="center" class="accont_boarder">
<tr><td>
<form name="sp_ex_form" id="sp_ex_form" action="doc_phr.php" method="post" >
<table height="32" bgcolor="#EEEEEE" width="700" class="fontset">
<tr><td width="210" valign="bottom"><span style="color:#5F8BAA;"><b style="font-size:17px">My Account Summary</b></span> </td>
<td width="48" valign="top" align="right"><span style="font-size:14px">From :</span></td>
	<td width="132" valign="top">
	<?php
	  $myCalendar = new tc_calendar("date1", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="25" valign="top" align="right"><span style="font-size:14px">To :</span></td>
      <td width="138" valign="top">
	  <?php
	  $myCalendar = new tc_calendar("date2", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="62" valign="top"><input type="submit" name="search" value="Go" /></td>
</tr>
</table>
</form>
<?php 
if (!$_POST['date1'] && !$_POST['date2']){
$qry2="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session),SUM(online_exp_consultation_reports.balance_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' ";

$qry22="SELECT max(`mail_id`) FROM `pw_expert_consultation_emails` where `doctor_id`='$doctor_id' group by `consultation_id`";
}
if($_POST['date1'] && $_POST['date2'])
{
	$date1=$_POST['date1'];
	$date2=$_POST['date2'];
$qry2="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session),SUM(online_exp_consultation_reports.balance_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' ";
	
	$qry22="SELECT max(`mail_id`) FROM `pw_expert_consultation_emails` where `doctor_id`='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' group by `consultation_id`";
}

$qry_rslt = mysql_query($qry2);
while($row = mysql_fetch_assoc($qry_rslt)) 
{
	$email_session_charge=$row['exprt_email_1_session'];
	if($email_session_charge=="") {
			$qry="SELECT `exprt_email_1_session` FROM `packages` WHERE `type_of_doctor`='Expert' && doctor_id='0'";	
			if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
			$email_session_charge = $qry_result['exprt_email_1_session'];
		}
	}
	
	$usage_session1 =$row['SUM(online_exp_consultation_reports.usage_session)'];
	$usage_session=$usage_session1+0;
	$balance_session=$row['SUM(online_exp_consultation_reports.balance_session)'] +0;
}
$earning = $email_session_charge * $usage_session;
$completed=0;
$pending=0;
$qry_rslt22 = mysql_query($qry22);
while($row22 = mysql_fetch_assoc($qry_rslt22)) 
{
	$mail_serial_no=$row22['max(`mail_id`)'];
	$qry223="SELECT `mail_status` FROM `pw_expert_consultation_emails` WHERE `mail_id`='$mail_serial_no'";
	$qry_rslt223 = mysql_query($qry223);
	while($row223 = mysql_fetch_assoc($qry_rslt223))
	{
		$mail_status =$row223['mail_status'];
		
		if($mail_status=='closed')
		{
			$completed=$completed+1;
		}
		
	if($mail_status=='open')
		{
			$pending=$pending+1;
		}
	}
}
?>
<table width="700" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td width="180">&nbsp;</td>
	<td width="90" align="center"><b style="font-size:13px">Used Cards</b></td>
    <td width="95" align="center"><b style="font-size:13px">Earnings</b></td>
    <td width="145" align="center"><b style="font-size:13px">Pending Consultations.</b></td>
    <td width="160" align="center"><b style="font-size:13px">Completed Consultations.</b></td>
</tr>
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr>
    <td style="font-size: 13px">Online Consultation</td>
    <td align="center" style="font-size: 13px"><?php echo $usage_session;?></td>
    <td align="center" style="font-size: 13px">Rs.<?php echo $earning;?> /-</td>
    <td align="center" style="font-size: 13px"><?php echo $pending;?></td>
    <td align="center" style="font-size: 13px"><?php echo $completed;?></td>
</tr>
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr bgcolor="#DDDDDD">
    <td width="180"><b style="font-size: 13px;">Total Earnings</b></td>
    <td width="90" align="center" style="font-size: 13px"><b><?php echo $usage_session;?></b></td>
    <td width="95" align="center" style="font-size: 13px"><b>Rs.<?php echo $earning;?> /-</b></td>
    <td width="145" align="center" style="font-size: 13px"><b><?php echo $pending;?></b></td>
    <td width="160" align="center" style="font-size: 13px"><b><?php echo $completed;?></b></td>
</tr>
</table>
</td></tr></table>
</div>
<?php
}
?>
<!--    --------------------         END Expert                 ------------------------    -->
<!--    --------------------         Counsellor                 -----------------------    -->
<?php
if ($doc_category=='Counsellor' && $doc_ctgry_flag=="")
{
?>
<div style="font-family: Arial;">
<table width="700" style="border:1px solid #CCC;" align="center"  class="accont_boarder">
<tr><td>
<form name="sp_ex_form" id="sp_ex_form" action="doc_phr.php" method="post" >
<table height="32" bgcolor="#EEEEEE" width="700">
<tr><td width="200" height="26" valign="bottom"><span style="color:#5F8BAA;"><b style="font-size:17px">My Account Summary</b></span></td>
<td width="58" valign="top" align="right"><span style="font-size:14px">From :</span></td>
	<td width="132" valign="top"><?php
	  $myCalendar = new tc_calendar("date1", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="25" valign="top" align="right"><span style="font-size:14px">To :</span></td>
      <td width="138" valign="top"><?php
	  $myCalendar = new tc_calendar("date2", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="62" valign="top"><input type="submit" name="search" value="Go" /></td>
</tr>
</table>
</form>
<?php  
if (!$_POST['date1'] && !$_POST['date2']){
$qry3="SELECT packages.1_email_session_charge,online_coun_consultation_reports.usage_session,online_coun_consultation_reports.entered_date,SUM(online_coun_consultation_reports.usage_session),SUM(online_coun_consultation_reports.balance_session) FROM packages INNER JOIN online_coun_consultation_reports ON packages.doctor_id=online_coun_consultation_reports.counsellor_id && packages.doctor_id='$doctor_id' ";

$qry33="SELECT max(`mail_id`) FROM `pw_cunslr_consultation_emails` where `counsellor_id`='$doctor_id' group by `consultation_id`";
}
if($_POST['date1'] && $_POST['date2'])
{
	$date1=$_POST['date1'];
	$date2=$_POST['date2'];
	$qry3="SELECT packages.1_email_session_charge,online_coun_consultation_reports.usage_session,online_coun_consultation_reports.entered_date,SUM(online_coun_consultation_reports.usage_session),SUM(online_coun_consultation_reports.balance_session) FROM packages INNER JOIN online_coun_consultation_reports ON packages.doctor_id=online_coun_consultation_reports.counsellor_id && packages.doctor_id='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' ";
$qry33="SELECT max(`mail_id`) FROM `pw_cunslr_consultation_emails` where `counsellor_id`='$doctor_id'  && `entered_date` BETWEEN '$date1' AND '$date2' group by `consultation_id`";		
}	

$qry_rslt = mysql_query($qry3);
while($row = mysql_fetch_assoc($qry_rslt)) 
{
	$email_session_charge=$row['1_email_session_charge'];
	
	if($email_session_charge=="") {
			$qry="SELECT `1_email_session_charge` FROM `packages` WHERE `type_of_doctor`='Counsellor' && doctor_id='0'";	
			if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
			$email_session_charge = $qry_result['1_email_session_charge'];
		}
	}
	$usage_session1 =$row['SUM(online_coun_consultation_reports.usage_session)'];
	$usage_session=$usage_session1+0;
	$balance_session=$row['SUM(online_coun_consultation_reports.balance_session)'] +0;
}
$earning = $email_session_charge * $usage_session;
$completed=0;
$pending=0;
$qry_rslt33 = mysql_query($qry33);
while($row33 = mysql_fetch_assoc($qry_rslt33)) 
{
	$mail_serial_no=$row33['max(`mail_id`)'];
	$qry333="SELECT `mail_status` FROM `pw_cunslr_consultation_emails` WHERE `mail_id`='$mail_serial_no'";
	$qry_rslt333 = mysql_query($qry333);
	while($row333 = mysql_fetch_assoc($qry_rslt333))
	{
		$mail_status =$row333['mail_status'];
		
		if($mail_status=='closed')
		{
			$completed=$completed+1;
		}
		
	if($mail_status=='open')
		{
			$pending=$pending+1;
		}
	}
}
?>
<table width="700" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td width="180">&nbsp;</td>
	<td width="90" align="center"><b style="font-size:13px">Used Cards</b></td>
    <td width="95" align="center"><b style="font-size:13px">Earnings</b></td>
    <td width="145" align="center"><b style="font-size:13px">Pending Consultations.</b></td>
    <td width="160" align="center"><b style="font-size:13px">Completed Consultations.</b></td>
</tr>
<!--<tr>
    <td style="font-size: 13px">Telephone Consultation</td>
    <td align="center" style="font-size: 13px">0</td>
    <td align="center" style="font-size: 13px">Rs.0 /-</td>
    <td align="center" style="font-size: 13px">0</td>
    <td align="center" style="font-size: 13px">0</td>
</tr>-->
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr>
    <td style="font-size: 13px">Online Consultation</td>
    <td align="center" style="font-size: 13px"><?php echo $usage_session;?></td>
    <td align="center" style="font-size: 13px">Rs.<?php echo $earning;?> /-</td>
    <td align="center" style="font-size: 13px"><?php echo $pending;?></td>
    <td align="center" style="font-size: 13px"><?php echo $completed;?></td>
</tr>
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr bgcolor="#DDDDDD">
	<td width="180"><b style="font-size: 13px">Total Earnings</b></td>
    <td width="90" align="center" style="font-size: 13px"><b><?php echo $usage_session;?></b></td>
    <td width="95" align="center" style="font-size: 13px"><b>Rs.<?php echo $earning;?> /-</b></td>
    <td width="145" align="center" style="font-size: 13px"><b><?php echo $pending;?></b></td>
    <td width="160" align="center" style="font-size: 13px"><b><?php echo $completed;?></b></td>
</tr>
</table>
</td></tr></table>
</div>
<?php
}
?>
<!--    --------------------         END Counsellor                 -----------------------    -->
<!--     -------------------         Specialist/Expert           ----------------------    -->
<?php
if ($doc_category=='Specialist/Expert' || $doc_ctgry_flag!="")
{
?>
<div style="font-family: Arial;">
<table width="700" style="border:1px solid #CCC;" align="center" class="accont_boarder">
<tr><td>
<form name="sp_ex_form" id="sp_ex_form" action="doc_phr.php" method="post" >
<table height="32" bgcolor="#dadada" width="700">
<tr>
<td width="200" height="26" valign="bottom"><span style="color:#5F8BAA;"><b style="font-size:17px">My Account Summary</b></span></td>
<td width="58" valign="top" align="right"><span style="font-size:14px">From :</span></td>
	<td width="132" valign="top"><?php
	  $myCalendar = new tc_calendar("date1", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="25" valign="top" align="right"><span style="font-size:14px">To :</span></td>
      <td width="138" valign="top"><?php
	  $myCalendar = new tc_calendar("date2", true, false);
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearInterval(1910, 2015);
	  $myCalendar->dateAllow('1910-01-01', '2015-03-01');
	  $myCalendar->setDateFormat('j F Y');
	  //$myCalendar->setHeight(350);	  
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
	  $myCalendar->writeScript();
	  ?></td>
      <td width="63" valign="top"><input type="submit" name="search" value="Go" /></td>
</tr>
</table>
</form>
<?php

if (!$_POST['date1'] && !$_POST['date2']){
	$qry1="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),SUM(online_spe_consultation_reports.balance_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id'";

$qry12="SELECT max(`mail_serial_no`) FROM `pw_consultation_emails` where `doctor_id`='$doctor_id' group by `consultation_no`";

$qry2="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session),SUM(online_exp_consultation_reports.balance_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' ";

$qry22="SELECT max(`mail_id`) FROM `pw_expert_consultation_emails` where `doctor_id`='$doctor_id' group by `consultation_id`";
}

if($_POST['date1'] && $_POST['date2'])
{
	$date1=$_POST['date1'];
	$date2=$_POST['date2'];
	
	$qry1="SELECT packages.1_email_session_charge,online_spe_consultation_reports.entered_date,SUM(online_spe_consultation_reports.usage_session),SUM(online_spe_consultation_reports.balance_session) FROM packages INNER JOIN online_spe_consultation_reports ON packages.doctor_id=online_spe_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2'";
	
$qry12="SELECT max(`mail_serial_no`) FROM `pw_consultation_emails` where `doctor_id`='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' group by `consultation_no`";
	
$qry2="SELECT packages.exprt_email_1_session,online_exp_consultation_reports.usage_session,online_exp_consultation_reports.entered_date,SUM(online_exp_consultation_reports.usage_session),SUM(online_exp_consultation_reports.balance_session) FROM packages INNER JOIN online_exp_consultation_reports ON packages.doctor_id=online_exp_consultation_reports.doctor_id && packages.doctor_id='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' ";
	
$qry22="SELECT max(`mail_id`) FROM `pw_expert_consultation_emails` where `doctor_id`='$doctor_id' && `entered_date` BETWEEN '$date1' AND '$date2' group by `consultation_id`";
}
/*	######################### Srecialist Check ############################*/

$qry_rslt = mysql_query($qry1);
while($row = mysql_fetch_assoc($qry_rslt)) 
{
	$email_session_charge1=$row['1_email_session_charge'];
	if($email_session_charge1=="") {
			$qry="SELECT `1_email_session_charge` FROM `packages` WHERE `type_of_doctor`='Specialist' && doctor_id='0'";	
			if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
			$email_session_charge1 = $qry_result['1_email_session_charge'];
		}
	}
	$usage_session1 =$row['SUM(online_spe_consultation_reports.usage_session)'];
	$total_usage_session1=$usage_session1+0;
	$balance_session=$row['SUM(online_spe_consultation_reports.balance_session)'] +0;
}
$earning1 = $email_session_charge1 * $usage_session1;
$completed1=0;
$pending1=0;
$qry_rslt12 = mysql_query($qry12);
while($row12 = mysql_fetch_assoc($qry_rslt12)) 
{
	$mail_serial_no=$row12['max(`mail_serial_no`)'];
	$qry123="SELECT `mail_status` FROM `pw_consultation_emails` WHERE `mail_serial_no`='$mail_serial_no' ";
	$qry_rslt123 = mysql_query($qry123);
	while($row123 = mysql_fetch_assoc($qry_rslt123))
	{
		$mail_status =$row123['mail_status'];
			
		if($mail_status=='closed')
		{
			$completed1=$completed1+1;
		}
		
	if($mail_status=='open')
		{
			$pending1=$pending1+1;
		}
	}
}
/*	########################   Epert Check #################### */

$qry_rslt = mysql_query($qry2);
while($row = mysql_fetch_assoc($qry_rslt)) 
{
	$email_session_charge2=$row['exprt_email_1_session'];
	if($email_session_charge2=="") {
			$qry="SELECT `exprt_email_1_session` FROM `packages` WHERE `type_of_doctor`='Expert' && doctor_id='0'";	
			if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
			$email_session_charge2 = $qry_result['exprt_email_1_session'];
		}
	}
	$usage_session2 =$row['SUM(online_exp_consultation_reports.usage_session)'];
	$total_usage_session2=$usage_session2+0;
	$balance_session=$row['SUM(online_exp_consultation_reports.balance_session)'] +0;
}
$earning2 = $email_session_charge2 * $usage_session2;
$completed2=0;
$pending2=0;
$qry_rslt22 = mysql_query($qry22);
while($row22 = mysql_fetch_assoc($qry_rslt22)) 
{
	$mail_serial_no=$row22['max(`mail_id`)'];
	$qry223="SELECT `mail_status` FROM `pw_expert_consultation_emails` WHERE `mail_id`='$mail_serial_no'";
	$qry_rslt223 = mysql_query($qry223);
	while($row223 = mysql_fetch_assoc($qry_rslt223))
	{
		$mail_status =$row223['mail_status'];
		if($mail_status=='closed')
		{
			$completed2=$completed2+1;
		}
		if($mail_status=='open')
		{
			$pending2=$pending2+1;
		}
	}
}
?>
<?php
$total_earning = $earning1 + $earning2;
$total_usage = $total_usage_session1 + $total_usage_session2;
$total_balance= $balance_session1 + $balance_session2;
$total_complete= $completed1 + $completed2;
$total_pending = $pending1 + $pending2;
?>
<table width="700" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td width="180">&nbsp;</td>
	<td width="90" align="center"><b style="font-size:13px">Used Cards</b></td>
    <td width="95" align="center"><b style="font-size:13px">Earnings</b></td>
    <td width="145" align="center"><b style="font-size:13px">Pending Consultations.</b></td>
    <td width="160" align="center"><b style="font-size:13px">Completed Consultations.</b></td>
</tr>
<!--<tr>
    <td style="font-size: 13px">Telephone Consultation</td>
    <td align="center" style="font-size: 13px">0</td>
    <td align="center" style="font-size: 13px">Rs.0 /-</td>
    <td align="center" style="font-size: 13px">0</td>
    <td align="center" style="font-size: 13px">0</td>
</tr>-->
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr>
    <td style="font-size: 13px">Online Consultation</td>
    <td align="center" style="font-size: 13px"><?php echo $total_usage;?></td>
    <td align="center" style="font-size: 13px">Rs.<?php echo $total_earning;?> /-</td>
    <td align="center" style="font-size: 13px"><?php echo $total_pending;?></td>
    <td align="center" style="font-size: 13px"><?php echo $total_complete;?></td>
</tr>
<tr><td colspan="5"><img src="images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
<tr bgcolor="#DDDDDD">
	<td width="180"><b style="font-size: 13px">Total Earnings</b></td>
    <td width="90" align="center" style="font-size: 13px"><b><?php echo $total_usage;?></b></td>
    <td width="95" align="center" style="font-size: 13px"><b>Rs.<?php echo $total_earning;?> /-</b></td>
    <td width="145" align="center" style="font-size: 13px"><b><?php echo $total_pending;?></b></td>
    <td width="160" align="center" style="font-size: 13px"><b><?php echo $total_complete;?></b></td>
</tr></table>
</td></tr></table>
</div>
<?php
}
?>
</td></tr></table>
<?php
include 'footer.php'; ?>
</body></html>
