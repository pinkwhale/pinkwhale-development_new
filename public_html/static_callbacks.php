<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
<title>jCarousel Examples</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<!--
  jQuery library
-->
<script type="text/javascript" src="lib/jquery-1.4.2.min.js"></script>
<!--
  jCarousel library
-->
<script type="text/javascript" src="lib/jquery.jcarousel.min.js"></script>

<script type="text/javascript" src="js/slidshow.js" > </script>
<!--
  jCarousel skin stylesheet
-->
<link rel="stylesheet" type="text/css" href="skins/tango/skin.css" />

<style type="text/css">

/**
 * Overwrite for having a carousel with dynamic width.
 */
.jcarousel-skin-tango .jcarousel-container-horizontal {
    width: 85%;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width: 100%;
}

#display {
    clear: both;
    width: auto;
    height: 250px;
    overflow: scroll;
    border: 1px solid #666;
    background-color: #fcfcfc;
    padding: 10px;
}
</style>

<script type="text/javascript">

/**
 * This is the callback function which receives notification
 * about the state of the next button.
 */
function mycarousel_buttonNextCallback(carousel, button, enabled) {
    display('Next button is now ' + (enabled ? 'enabled' : 'disabled'));
};

/**
 * This is the callback function which receives notification
 * about the state of the prev button.
 */
function mycarousel_buttonPrevCallback(carousel, button, enabled) {
    display('Prev button is now ' + (enabled ? 'enabled' : 'disabled'));
};

/**
 * This is the callback function which receives notification
 * right after initialisation of the carousel
 */
function mycarousel_initCallback(carousel, state) {
    if (state == 'init')
        display('Carousel initialised');
    else if (state == 'reset')
        display('Carousel reseted');
};

/**
 * This is the callback function which receives notification
 * right after reloading of the carousel
 */
function mycarousel_reloadCallback(carousel) {
    display('Carousel reloaded');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 */
function mycarousel_itemFirstInCallback(carousel, item, idx, state) {
    display('Item #' + idx + ' is now the first item');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 */
function mycarousel_itemFirstOutCallback(carousel, item, idx, state) {
    display('Item #' + idx + ' is no longer the first item');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 */
function mycarousel_itemLastInCallback(carousel, item, idx, state) {
    display('Item #' + idx + ' is now the last item');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 */
function mycarousel_itemLastOutCallback(carousel, item, idx, state) {
    display('Item #' + idx + ' is no longer the last item');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 * Triggered before animation.
 */
function mycarousel_itemVisibleInCallbackBeforeAnimation(carousel, item, idx, state) {
    // No animation on first load of the carousel
    if (state == 'init')
        return;

    jQuery('img', item).fadeIn('slow');
};

/**
 * This is the callback function which receives notification
 * when an item becomes the first one in the visible range.
 * Triggered after animation.
 */
function mycarousel_itemVisibleInCallbackAfterAnimation(carousel, item, idx, state) {
    display('Item #' + idx + ' is now visible');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 * Triggered before animation.
 */
function mycarousel_itemVisibleOutCallbackBeforeAnimation(carousel, item, idx, state) {
    jQuery('img', item).fadeOut('slow');
};

/**
 * This is the callback function which receives notification
 * when an item is no longer the first one in the visible range.
 * Triggered after animation.
 */
function mycarousel_itemVisibleOutCallbackAfterAnimation(carousel, item, idx, state) {
    display('Item #' + idx + ' is no longer visible');
};

/**
 * Helper function for printing out debug messages.
 * Not needed for jCarousel.
 */
var row = 1;
function display(s) {
    // Log to Firebug (getfirebug.com) if available
    //if (window.console != undefined && typeof window.console.log == 'function')
      //  console.log(s);

    if (row >= 1000)
        var r = row;
    else if (row >= 100)
        var r = '&nbsp;' + row;
    else if (row >= 10)
        var r = '&nbsp;&nbsp;' + row;
    else
        var r = '&nbsp;&nbsp;&nbsp;' + row;

    jQuery('#display').html(jQuery('#display').html() + r + ': ' + s + '<br />').get(0).scrollTop += 10000;

    row++;
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        scroll: 1,

        initCallback:   mycarousel_initCallback,
        reloadCallback: mycarousel_reloadCallback,

        buttonNextCallback:   mycarousel_buttonNextCallback,
        buttonPrevCallback:   mycarousel_buttonPrevCallback,

        itemFirstInCallback:  mycarousel_itemFirstInCallback,
        itemFirstOutCallback: mycarousel_itemFirstOutCallback,
        itemLastInCallback:   mycarousel_itemLastInCallback,
        itemLastOutCallback:  mycarousel_itemLastOutCallback,
        itemVisibleInCallback: {
            onBeforeAnimation: mycarousel_itemVisibleInCallbackBeforeAnimation,
            onAfterAnimation:  mycarousel_itemVisibleInCallbackAfterAnimation
        },
        itemVisibleOutCallback: {
            onBeforeAnimation: mycarousel_itemVisibleOutCallbackBeforeAnimation,
            onAfterAnimation:  mycarousel_itemVisibleOutCallbackAfterAnimation
        }
    });
});

</script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<div id="wrap">
   <ul id="mycarousel" class="jcarousel-skin-tango">
  <?php
	include "db_connect.php";
	$qry="SELECT * FROM `specialist_doctors` order by sp_doctors_id ASC";
	if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
	while($qry_result=mysql_fetch_array($qry_rslt))
	{
?>
    <li>
    <a href="#">
    <img src="<?php echo $qry_result['sp_doctors_photo']; ?>" width="75" height="75" alt="" onclick="JavaScript:doctor_detaisl()"/>				    </a>
    </li>
    

 <?php 	}?>
  </ul>

  <p id="display"></p>

</div>
</body>
</html>
