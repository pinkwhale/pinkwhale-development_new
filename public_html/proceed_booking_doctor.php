<?php
ob_start();
date_default_timezone_set('Asia/Calcutta');
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='doctor')
{
        header("Location: index.php");
        exit();
}

include ("db_connect.php");


$doc_id = $_SESSION['doctor_id'];

$docname = get_doc_name($doc_id);

$date = urldecode($_GET['date']);

$slot12 = urldecode($_GET['slot12']);

$slot24 = urldecode($_GET['slot24']);

$d_day = date("D",time($date));

$week = get_week($d_day);

$clinic_id=urldecode($_GET['clinic_id']); 

$clinic_details = explode("|", $clinic_id);
$clinic_id = $clinic_details[0];
$clinic_name = $clinic_details[1];

$qry = "select status from Appointment_book_details where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and  admin_id<>'$doc_id'";

$result = mysql_query($qry);
$data = mysql_fetch_array($result);
if($data[0]=="" ){
    
    $query1 = "select admin_id from Appointment_book_details where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and  admin_id='$doc_id'";
    $result1= mysql_query($query1);
    $num_1 = mysql_num_rows($result1);
    
    if($num_1==0){
        $query = "insert into Appointment_book_details(admin_id,doc_id,clinic_id,Day,from_time,status,booktime) values('$doc_id','$doc_id','$clinic_id','$week','$date $slot24','1',now())";    
        mysql_query($query) or die ("error while engaging appointment"); 
    }else{
         $query = "update Appointment_book_details set booktime=now() where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and admin_id='$doc_id'";    
         mysql_query($query) or die ("error while engaging appointment");    
    }
    
    
    $qry1 = "select id from Appointment_book_details where  doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 ";
    $res1 = mysql_query($qry1);
    $da1 = mysql_fetch_array($res1);

    $proceed = "<form name='book_doc_app' id='book_doc_app'  method='POST'><table border='0' cellpadding='0' cellspacing='1' width='700' align='center' class='s90registerform'>";

    $proceed .="<tr><th colspan=\"2\">Book Appointment</th></tr>";

    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Doctor Name &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' readonly size='25'  name='clinic_id' id='clinic_id' value='$clinic_id'/><input type='hidden' readonly size='25'  name='doc_id' id='doc_id' value='$doc_id'/>$docname";

    $proceed .="<input type='hidden' readonly size='25'  name='app_id' id='app_id' value='".$da1['id']."'/><input type='hidden' readonly size='25'  name='doc_name' id='doc_name' value='$docname'/></td></tr>";
    
    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='doctorErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Patient Name &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='25' maxlength='20' name='p_name' id='p_name'/></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='p_nameErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Age &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='3' maxlength='2' name='age' id='age' onkeypress=\"return isNumberKey(event);\"/></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='ageErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Email &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='30' maxlength='50' name='email' id='email' /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='emailErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Mobile No: &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='13' maxlength='13' name='mob' id='mob' onkeypress=\"return isNumberKey(event);\" /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='mobileErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Gender &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='gender' id='gender'><option value=''>--- select Gender ---</option><option value='Male'>Male</option><option value='Female'>Female</option></select></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='genderErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    /*
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Address &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><textarea name='address' id='address' rows='3' cols='40' ></textarea></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='addressErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    */
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Appointment Time &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='time_slot' id='time_slot' readonly value='$date $slot24' />$date $slot12";
    
    $proceed .="<input type='hidden' name='display_time_slot' id='display_time_slot' readonly value='$date $slot12' /><input type='hidden' name='app_date' id='app_date' readonly value='$date' /><input type='hidden' name='app_time' id='app_time' readonly value='$slot12' /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='appointment_timeErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">New Patient &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='new_patient' id='new_patient' ><option value=''>----select----</option><option value='0'>yes</option><option value='1'>NO</option></select>";
    
    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='NewPatientErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">New to use this System &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='new_for_system' id='new_for_system' ><option value=''>----select----</option><option value='0'>yes</option><option value='1'>NO</option></select>";
    
    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='NewForPatientErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td colspan='2' bgcolor=\"#F5F5F5\" align='center'><input type='button' name=\"button2\" class=\"bookBtn\" onclick='book_final(book_doc_app)' value='Book' /><input type='button' class=\"bookBtn\" onclick='book_cancel(book_doc_app)' value='Cancel' /></td></tr>";

    $proceed .="</table></form>";

    echo $proceed;


}else{
    echo "<table border='0' cellpadding='0' cellspacing='1' width='500' align='center' class='s90registerform'><tr><td align='center'><font color='red'>Appointment Slot is already Booked</font></td></tr></table>";
}


ob_end_flush();


function get_doc_name($id){
    include "db_connect.php";
    
    $qry = "select doc_name from pw_doctors where doc_id=$id";    
    $res = mysql_query($qry);
    $data = mysql_fetch_array($res);
    return $data[0];
}


function get_week($d){
    $day=0;
    
    switch($d){
        case "Mon" : $day = 1;break;
        case "Tue" : $day = 2;break;
        case "Wed" : $day = 3;break;
        case "Thu" : $day = 4;break;
        case "Fri" : $day = 5;break;
        case "Sat" : $day = 6;break;
        case "Sun" : $day = 7;break;
    }
    
    return $day;
    
}
?>

