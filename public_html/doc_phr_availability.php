<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<script type="text/javascript">
function changeDoctorAvailDiv()
{
		document.getElementById("add_doc_availability").style.display="block";
		document.getElementById("doc_avaialability_grid").style.display="none";
}


</script>

<?php
include 'header.php'; ?>
<!-- header.......-->
<?php
include("admin/includes/host_conf.php");
include("admin/includes/mysql.lib.php");
include('admin/includes/ps_pagination.php');
$obj=new connect;
?>
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<?php include "doc_phr_left_menu.php"; ?></td>

<td width="748" valign="top" class="s90docphr"><h1>My Availability</h1>
<table height="30"></table>

<div id="add_doc_availability" style="display: none;">
<form name="add_doc_availability_form" id="add_doc_availability_form" action="actions/add_doc_availability.php" method="post" >
<input type="hidden" name="doc_id" value="<?php echo $doctor_id ; ?>" />
<table width="648" style="border:1px solid #CCC;">
<tr><td width="98" align="right"><b>Day :</b></td>
	<td width="44">&nbsp;</td>
	<td width="221"><select  name="select_day" id="select_day" > 
           <option value="">-Select Day-</option>
            	<option value="Sunday">Sunday</option>
        <option value="Monday">Monday</option>
        <option value="Tuesday">Tuesday</option>
           <option value="Wednesday">Wednesday</option>
        <option value="Thursday">Thursday</option>
           <option value="Fryday">Fryday</option>
        <option value="Saturday">Saturday</option>
           
        </select></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr><td align="right"><b> Time :</b></td>
	<td align="right">From :</td>
    <td><input type="text" name="morning_from_time" id="morning_from_time" />
    <select  name="select_am" id="select_am" > 
    <option value="">- -</option>
            	<option value="am">am</option>
        <option value="pm">pm</option>
        
        </select>
    
    </td>

    <td width="36" align="right">To :</td>
    <td width="225">
      <input type="text" name="morning_to_time" id="morning_to_time"  />
      <select  name="select_pm" id="select_pm" >
      <option value="">- -</option> 
            	<option value="am">am</option>
        <option value="pm">pm</option>
        
        </select>
      
      </td>
</tr>

<tr><td align="right"><b>Place :</b></td>
	<td>&nbsp;</td>
	<td><textarea type="text" name="morning_place" id="morning_place"> </textarea></td>
</tr>
<tr><td>&nbsp;</td></tr>





<tr><td>&nbsp;</td><td>&nbsp;</td>
	<td align="right"><input type="submit" name="add_doc_availability" id="add_doc_availability"  value="Save" /></td>

</table>
</form>
</div>

<div id="doc_avaialability_grid" style="display: block;">
<table width="713" border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
<?php		
		$sql1="SELECT * FROM doc_availability Where available_doc_id='$doctor_id' ORDER BY order_id ASC";
		

			$obj->query($sql1);	
			$pager = new PS_Pagination($dbcnx, $sql1, 100, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) die(mysql_error());
			?>
		
<tr bgcolor='#f3f7f8'>
                <td align='left'><strong>Day</strong></td>
                <td align='left'><strong>Time</strong></td>
                <td align='left'><strong>Place</strong></td>
            </tr>
	
			<?php	
			$total_charge = 0;
			while($row = mysql_fetch_assoc($rs)) 
			{
				$doc_avail_id=$row['doc_avail_id'];
				$doc_avail_day=$row['doc_avail_day'];
				$time_from=$row['time_from'];
				$time_am =$row['time_am'];
				$time_to=$row['time_to'];
				$time_pm=$row['time_pm'];
				$place =$row['place'];
			echo "<tr bgcolor='#ffffff'>";
			?>
	 
                <td><a href="edit-doc-availability.php?id=<?php echo $doc_avail_id ?>" style="text-decoration:none;"><?php echo $doc_avail_day; ?></a></td>
                
                <?php
				echo "<td>$time_from$time_am to $time_to$time_pm</td> ";
			
				echo "<td>$place</td> ";
				echo "</tr>";	
			}
			
			?>
    
        </table>
        <b>To add the timings for specified period please click <span style="color: #0a74d8" onmouseover="this.style.cursor='pointer';" onclick="changeDoctorAvailDiv();">Here</span></b>
</div>
</td>
<td></td>
</tr>
</table>

<?php
include 'footer.php'; ?>

</body></html>