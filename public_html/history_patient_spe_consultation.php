<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login']!='user')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/SimpleTextEditor.js"></script>
<link rel="stylesheet" type="text/css" href="css/SimpleTextEditor.css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
	<tr><td width="220" valign="top">
		<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="phr.php">My Account</a></div>
		<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- menu.......-->
<?php
include 'user_left_menu.php'; 
?>
<!-- Menu.......-->
	</td>

	<td width="748" valign="top" class="s90phrcontent">
<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls">
	<tr><th colspan="3" height="10">Specialist Consultation Summary </th></tr>
	
<?php
$mail_ref_id=$_GET['idfr'];
$qry2= "SELECT * FROM `pw_consultation_emails` WHERE `consultation_no`='$mail_ref_id' order by `mail_serial_no` ASC";
$qry_rslt2 = mysql_query($qry2);
while($result2= mysql_fetch_array($qry_rslt2))	
{
	$con_num=$result2['consultation_no'];
	$con_pw_card=$result2['pw_card_id'];
	$con_doc_id=$result2['doctor_id'];
	$con_doc_name=$result2['doctor_name'];
	$con_ptnt_name=$result2['patient_name'];
	$gdate1=strtotime($result2['entered_date']);
	$final_date1=date("d M Y", $gdate1);
	
	if ($result2['doctor_replay'] =='')
	{


?>
	<tr><td>
<table width="580" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
<?php 
$qry3= "SELECT user_photo FROM `user_details` WHERE `user_id`='$user_id'";
$qry_rslt3 = mysql_query($qry3);
while($result3= mysql_fetch_array($qry_rslt3))
{
	$user_photo=$result3['user_photo'];
}

if($user_photo=="") 
{
?>
	<tr><td width="98" align="center" valign="top" rowspan="2">
	<img src="images/avatar.png" width="72"  height="72" style="margin-top:16px"/><br /><br />
<?php 
}
else 
{
?>
	<tr><td width="98" rowspan="2" align="center" valign="top">
	<img src="<?php echo $user_photo;?>" width="72"  height="72" style="margin-top:16px"/><br /><br />     

<?php		
}
?>

	</td>
	<td width="362" valign="top"><p><strong><?php echo $result2['patient_name'] ?></strong></p>
	
	</td>
	<td width="100" valign="top"><p><?php echo $final_date1; ?></p></td>
	</tr>
	<tr>
	  <td colspan="2" valign="top">
      	<p><?php echo $result2['cnsltion_mail_details'] ?></p>
        <p>
        <?php
		if($result2['upload_record']!='')
		{
		?>
			<a href="<?php echo $result2['upload_record'] ; ?> ">
			<img src="images/attach.png" width="16" height="16" align="absmiddle"/> Download</a>
		<?php	} ?>
        </p>
      </td>
	  </tr>
</table>
<?php
}
if ($result2['doctor_replay'] !='')
{
	$qry= "SELECT `doc_photo` FROM `pw_doctors` WHERE `doc_id`='$con_doc_id'";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))	
	{
		$doc_photo=$result['doc_photo'];
	}
?>
        <table width="580" border="0" cellspacing="5" cellpadding="0" class="s90dbdtbls_dremailtbl">
                <tr>
                    <td width="99" rowspan="3" align="center" valign="top">
                        <img src="<?php echo $doc_photo ; ?>" width="72"  height="72" style="margin-top:16px" />
                    </td>
                    <td width="361" valign="top"><p><strong><?php echo $result2['doctor_name'] ?></strong></p>
                        
              </td>
                    <td width="100" valign="top"><p><?php echo $final_date1;  ?></p></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top"><p><?php echo $result2['doctor_replay'] ;	?></p></td>
                </tr>
            </table>
<?php }}?>

	</td></tr></table>

</td></tr></table>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
