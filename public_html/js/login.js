

// JavaScript Document
var usernameValidated = false;
var passwordValidated = false;

function LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
	document.getElementById("username_div").innerHTML = "";
	document.getElementById("password_div").innerHTML = "";
	
	if (form.username.value=='')
	{
   		document.getElementById("username_div").innerHTML = "UserName cannot be blank";
    	usernameValidated = false;
	}
	if (form.password.value=='')
	{
		document.getElementById("password_div").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{
		document.getElementById('signin_pop').submit();
	}
		
	else
	{
		return false;
	}
	
}

function LoginSignUp_ask_query(form)

{

        

	usernameValidated = true;

	passwordValidated = true;

	document.getElementById("username_div").innerHTML	= "";

	document.getElementById("password_div").innerHTML = "";

	

	if (document.getElementById("uname").value=='')

	{

   		document.getElementById("uname_div").innerHTML = "UserName cannot be blank";

                usernameValidated = false;

	}

	if (document.getElementById("pwd").value=='')

	{

		document.getElementById("pwd_div").innerHTML = "Password cannot be blank";

		passwordValidated = false;

	}

	

	if(usernameValidated && passwordValidated)

	{

		document.getElementById('signin1').submit();

                

                

	}

		

	else

	{

		return false;

	}

	

}

function consult_LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
        document.getElementById("log_errordiv").innerHTML = "";        
	document.getElementById("username_errordiv").innerHTML = "";
	document.getElementById("password_errordiv").innerHTML = "";
	
	if (form.username.value=='')
	{
            
   		document.getElementById("username_errordiv").innerHTML = "UserName cannot be blank";
                usernameValidated = false;
	}
	if (form.password.value=='')
	{
            
		document.getElementById("password_errordiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{   
            
                // -------------------ajax start--------------------- //
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                
		var username = encodeURI(form.username.value);
                var password = encodeURIComponent(form.password.value);
                
                var paramString = 'username='+username+'&password='+password;
	
                $.ajax({  
                        type: "POST",  
                        url: "login_popup.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    loading("loading ",1); 
                                    document.getElementById('consult_signin').submit();
                                    
                                }else{
                                    document.getElementById("log_errordiv").innerHTML = "Invalid login Details";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{
                
		return false;
	}
	
}


function forgot_password(form)
{
        
	Validated = true;
        document.getElementById("forgotemailErrDiv").innerHTML = ""; 
        //alert(form.forgot_email.value);
	if (form.forgot_email.value=='')
	{
            
   		document.getElementById("forgotemailErrDiv").innerHTML = "Email cannot be blank";
                Validated = false;
	}
        
	if(Validated)
	{   
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("for_send").value="Validating....";
            
                // -------------------ajax start--------------------- //
                var paramString = 'email='+form.forgot_email.value;

                $.ajax({  
                        type: "POST",  
                        url: "actions/forgot_Passwd.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                //alert(response);
                                if(response==true){
                                    
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully sent password to your Email-id</div>";
                                    
                                }else{                                    
                                    document.getElementById("forgotemailErrDiv").innerHTML = "Invalid Email-ID";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}



function register_pink(form)
{
        
	Validated = true;
        
        document.getElementById("nameErrDiv").innerHTML = ""; 
	document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
	document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = ""; 
        
        
        var name = form.regname.value;
	var age = form.regage.value;
        var gender = form.reggender.value;
        var email = form.regemail.value;
        var password = form.regPassword.value;
        var confirmpassword = form.regConfirmPassword.value;
        var mobile = form.regPhone1.value;
	var card = form.regcard.value;
        
        
        //alert(form.forgot_email.value);
	if ( name =='' )
	{
            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
	if ( age =='' )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
                Validated = false;
	}else if ( age < 18 )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
                Validated = false;
	}
        
        if ( gender =='' )
	{
            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
                Validated = false;
	}
        if ( email =='' )
	{
            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
                Validated = false;
	}
        
        if ( password =='' )
	{
            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
                Validated = false;
	}
        
        if ( password != confirmpassword )
	{
            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch";
                Validated = false;
	}
        
        if( mobile == "" )
	{
            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
                Validated = false;
	}
        
        
	if(Validated)
	{   
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("btnSubmit").value = "Processing....";
            
                // -------------------ajax start--------------------- //
                
                name = encodeURI(name);
		age = encodeURI(age);
                gender = encodeURI(gender);
		card = encodeURI(card);
                email = encodeURI(email);
                password = encodeURIComponent(password);
                mobile = encodeURI(mobile);
                
                var paramString = 'name='+name+'&age='+age+'&gender='+gender+'&email='+email+'&password='+password+'&mobile='+mobile+'&regcard='+card;

                $.ajax({  
                        type: "POST",  
                        url: "actions/regpink.php",  
                        data: paramString,  
                        success: function(response) {  
	                		//alert(response);            
                                if(response==true){
                                  
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully Register<br />Please check your mail to complete registration.</div>";
                                    
                                }else{       
                                    
                                    document.getElementById("emailErrDiv").innerHTML= response;
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}


function card_check(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("cardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: "check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        
                            if(response==true){
                                document.getElementById("cardErrDiv").innerHTML= "<font color='green'>Valid Card</font>";
                                return false;
                            }else{
                                document.getElementById("regcard").value= "";
                                document.getElementById("cardErrDiv").innerHTML= response;
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //
        
}


function Topup_login(form)
{
        
	validate = true;
        
	
	document.getElementById("topuplog_errordiv").innerHTML= "";
	
        
                
	if (form.card.value=='')
	{
                
   		document.getElementById("topuplog_errordiv").innerHTML = "Card No. cannot be blank";
                validate = false;
	}
        
	
	if(validate)
	{
            document.getElementById("topupbtn").value="loading";
            document.getElementById("topupbtn").disabled=true;
            
            // -------------------ajax start--------------------- //
                var paramString = 'card='+form.card.value;

                $.ajax({  
                        type: "POST",  
                        url: "card_login_check.php",  
                        data: paramString,  
                        success: function(response) {         
                                if(response==true){
                                    
                                    document.getElementById('TopUPlogin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("topuplog_errordiv").innerHTML = "Invalid card No.";
                                    document.getElementById("topupbtn").value="Top Up";
                                    document.getElementById("topupbtn").disabled=false; 
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
		
                
	}
		
	else
	{
		return false;
	}
	
}




function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}


function validate_consult(type){
    
    document.getElementById("login_nxt_page").value=type;
    document.getElementById("register_nxt_page").value=type;
        
    setTimeout(login_callpopup(true,0),200);
    
}
