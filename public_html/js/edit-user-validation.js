function submit_form()
{
	document.getElementById('email_cnsltion_form').submit();
}
function activate_edit_div()
{
	document.getElementById("user_details").style.display="none";
	document.getElementById("user_change_pswd").style.display="none";
	document.getElementById("edit_user_details").style.display="block";
}
function activate_change_pswd()
{
	document.getElementById("user_details").style.display="none";
	document.getElementById("edit_user_details").style.display="none";
	document.getElementById("user_change_pswd").style.display="block";
	
}
function cancel_button()
{
	window.location.href="user_details.php";
}
function isNumberKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
function iskey_not_allowed(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode >350&& charCode < 0 )
	{
		return true; 
	}
	else
	{
		return false; 
	}
}
var pswValidated = false;
var repswValidated = false;
function validate_pswd(form)
{
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";
		repswValidated = false;
	}
	else if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";
			pswValidated = false;
			repswValidated = false;
		}
	}
	if(pswValidated && repswValidated )
	{
		document.getElementById('user_change_pswd_form').submit();
	}
	else
	{
		return false;
	}
	
}

