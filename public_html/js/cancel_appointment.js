var clinic = false;
var date = false;
var time_slot = false;

function cancel_appointment(form){
    clinic = true;
    date = true;
    time_slot = true;
    
    document.getElementById("clinicErrDiv").innerHTML= "";
    document.getElementById("calErrDiv").innerHTML = "";
    document.getElementById("clinic_timeErrDiv").innerHTML = "";    

    if(document.getElementById("clinic").value==""){
        document.getElementById("clinicErrDiv").innerHTML= "Please select Doctor";        
        clinic = false;
    }
    
    if(document.getElementById("date2").value=="0000-00-00"){
        document.getElementById("calErrDiv").innerHTML= "Please select Date";        
        date = false;
    }

    var d_time = 0;
    for (var i=0; i < form.time_slot.length; i++){
        if (form.time_slot[i].checked){
            var rad_val = form.time_slot[i].value;            
            d_time = d_time + 1;                      
        }
    }   
    
    if(d_time==0){
        document.getElementById("clinic_timeErrDiv").innerHTML= "Please select time slots";
        time_slot = false;
    }
    
    
    if(time_slot && clinic && date){
        //document.getElementById('dialog-confrm-cancel').style = "block";
        var r=confirm("Are You Sure ?");
        if(r==true){
            document.getElementById('doctor_appointment_cancel').submit();
        }else{
            return false;
        }
        /*
        $(function() {
                          
                $( "#dialog-confrm-cancel" ).dialog({
                        resizable: false,                   
                        modal: true,
                        buttons: {
                                "Yes": function() {
                                      document.getElementById('doctor_appointment_cancel').submit();
                                },
                                No: function() {
                                    $( "#dialog-confrm-cancel" ).dialog( "close" );    
                                }
                        }
                });
        });
        */
    }else{
        return false;
    }
}
