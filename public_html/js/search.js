
var site_config = "http://10.200.202.55/";

//var site_config = "http://www.pinkwhalehealthcare.com/test2/";



function query_srch(){
    
   
    var doc_name = trim(document.getElementById("doc_name").value);
    
    var mode = "";
    
    if(document.getElementById("gp").checked == true){
        mode = document.getElementById("gp").value;
    }else if(document.getElementById("spe").checked == true){
        mode = document.getElementById("spe").value;
    }
    
    
    if(mode==""){
        alert("Please select Doctor type");
        return false;
    }
    
    if(doc_name==""){
        document.query_search.action= site_config+"query-doctors/"+mode+"/doctor";
        document.query_search.submit();
    }else{
        document.query_search.action= site_config+"query-doctors/"+mode+"/"+doc_name;
        document.query_search.submit();
    }
    
}

function search_spe(){
    
    var doc_name = trim(document.getElementById("doc_name").value);
    var specialist = trim(document.getElementById("specialist").value);
    
    /*
            doc_name = encodeURI(doc_name);
            specialist = encodeURI(specialist);

            $("#loading").fadeIn(900,0);
            $("#loading").html("<img src='"+site_config+"/images/ajax-loader.gif' />");


            var paramString = 'doc_name='+doc_name+'&specialist='+specialist;


            $.ajax({
                    type: "POST",
                    url: site_config+"get-specialist-details.php",
                    data: paramString,
                    success: function(response) {

                            document.getElementById("spec_search").innerHTML=response;
                            $("#spe_details").hide();
                            $("#loading").fadeOut('slow');


                    }

            });
  	*/      
       
              
       
    if( doc_name=="" &&  specialist=="" ){
	
	//setCookie("spe_docname",doc_name,1);

        document.find_spe.action= site_config+"expert-doctors";
        document.find_spe.submit();
	
    }else if( doc_name=="" &&  specialist!="" ){
	
        specialist = encode_spe(specialist);
        
	document.find_spe.action= site_config+"expert-doctors/"+specialist+"/Doctor";
        document.find_spe.submit();
	
    }else if( doc_name!="" &&  specialist=="" ){
	
        doc_name = encode_docname(doc_name);
        
	document.find_spe.action= site_config+"expert-doctors/specialist/"+doc_name+"";
        document.find_spe.submit();
	
    }else{
        
        specialist = encode_spe(specialist);
        doc_name = encode_docname(doc_name);
        
        document.find_spe.action= site_config+"expert-doctors/"+specialist+"/"+doc_name+"";
        document.find_spe.submit();
        
    }

        
}

function trim(text) {
    return text.replace(/^\s+|\s+$/g, "");
}

function encode_spe(specialist){
    var n=specialist.split(" ");
    n= n.length-1;
    for(var i=1 ; i<=n ; i++){
        specialist = specialist.replace(" ","-");
    }
    return specialist;
}

function encode_docname(docname){
    
    docname = docname.replace("Dr.","");
    docname = docname.replace("Dr","");
    docname = docname.toLowerCase();
    docname = trim(docname.replace("."," "));
    docname = docname.replace("   ","-");
    docname = docname.replace("  ","-");
    docname = docname.replace(" ","-");
    
    return docname;
}


/*
function submit_spe2(doc_name,specialist){
   
    var n=specialist.split(" ");
    n= n.length-1;
    for(var i=1 ; i<=n ; i++){
        specialist = specialist.replace(" ","-");
    }
    
    setCookie("spe_docname",doc_name,1);
    
    document.find_spe.action= site_config+"expert-doctors/"+specialist+"/"+doc_name+"/";
    document.find_spe.submit();
}

function submit_spe1(doc_name,specialist){

    var n=specialist.split(" ");
    n= n.length-1;
    for(var i=1 ; i<=n ; i++){
        specialist = specialist.replace(" ","-");
    }

    setCookie("spe_docname",doc_name,1);

    document.find_spe.action= site_config+"expert-doctors";
    document.find_spe.submit();
}

*/


function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function spe_popup(obj,value){

      if(obj==true)
      {
            document.getElementById('spe_pop_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.display='';
            document.getElementById('spe_divDisable_'+value).style.height = document.body.scrollHeight + "px";
            document.getElementById('spe_divDisable_'+value).style.width = document.body.scrollWidth + "px";

      }else{

            document.getElementById('spe_pop_'+value).style.display='none';
            document.getElementById('spe_divDisable_'+value).style.display='none';

      }

}

function showLoginForm(value){

        spe_popup(false,value);

        $(document).ready(function()
        {
                alert("You are not yet logged in. Please login.");
                $(".signin").click();
        });

}

function search_health_expert(){
	
	var doc_name = trim(document.getElementById("doc_name").value);
	var specialist = trim(document.getElementById("specialist").value);
	
        /*
	if( specialist==""  ){
	
		doc_name = encodeURI(doc_name);
	        specialist = encodeURI(specialist);

	    	$("#loading").fadeIn(900,0);
	    	$("#loading").html("<img src='"+site_config+"/images/ajax-loader.gif' />");

	        var paramString = 'doc_name='+doc_name+'&specialist='+specialist;


	        $.ajax({
	                type: "POST",
	                url: "get-health-expert-details.php",
	                data: paramString,
	                success: function(response) {

	                        document.getElementById("spec_search").innerHTML=response;
	                        $("#spe_details").hide();
				$("#loading").fadeOut('slow');
	                }
	
	        });


	}
        */
        
        if( doc_name=="" &&  specialist=="" ){
	
            //setCookie("spe_docname",doc_name,1);

            document.find_exp.action= site_config+"specialist-doctor";
            document.find_exp.submit();
	
        }else if( doc_name=="" &&  specialist!="" ){

            specialist = encode_spe(specialist);

            document.find_exp.action= site_config+"specialist-doctor/"+specialist+"/Doctor";
            document.find_exp.submit();

        }else if( doc_name!="" &&  specialist=="" ){

            doc_name = encode_docname(doc_name);

            document.find_exp.action= site_config+"specialist-doctor/specialist/"+doc_name+"";
            document.find_exp.submit();

        }else{

            specialist = encode_spe(specialist);
            doc_name = encode_docname(doc_name);

            document.find_exp.action= site_config+"specialist-doctor/"+specialist+"/"+doc_name+"";
            document.find_exp.submit();

        }


}

/*
function submit_exp1(doc_name,specialist){

    var n=specialist.split(" ");
    n= n.length-1;
    for(var i=1 ; i<=n ; i++){
        specialist = specialist.replace(" ","-");
    }

    setCookie("exp_docname",doc_name,1);

    document.find_exp.action= site_config+"specialist-doctor";
    document.find_exp.submit();
}


function submit_exp2(doc_name,specialist){

    var n=specialist.split(" ");
    n= n.length-1;
    for(var i=1 ; i<=n ; i++){
        specialist = specialist.replace(" ","-");
    }

    setCookie("exp_docname",doc_name,1);

    document.find_exp.action= site_config+"specialist-doctor/"+specialist+"/";
    document.find_exp.submit();
}

*/


    
function displayLoginForm(){
           

        $(document).ready(function() 
        {
                alert("You are not yet logged in. Please login.");                       
                $(".signin").click();
        });

}    


function callpopup(obj){



    if(obj==true)        

    {

        document.getElementById('popupId').style.display='';

        /*
        document.getElementById('divDisable').style.display='';	

        document.getElementById('divDisable').style.height = document.body.scrollHeight + "px";

        document.getElementById('divDisable').style.width = document.body.scrollWidth + "px";             
        */


    }else{

        document.getElementById('popupId').style.display='none';

        //document.getElementById('divDisable').style.display='none';



    } 



}



function login_callpopup(obj,num){

      if(obj==true)        
      {
            document.getElementById("login_body").style.display="block";
            document.getElementById("login_message").style.display="none";
            
            document.getElementById("username_errordiv").innerHTML = "";
            document.getElementById("password_errordiv").innerHTML = "";
	    document.getElementById("log_errordiv").innerHTML = "";
            document.consult_signin.username.value="";
	    document.consult_signin.password.value="";
            document.getElementById("forgotemailErrDiv").innerHTML = "";
            document.getElementById("forgot_email").value="";
            
            document.getElementById("nameErrDiv").innerHTML = ""; 
	    document.getElementById("ageErrDiv").innerHTML = "";
            document.getElementById("genderErrDiv").innerHTML = "";
            document.getElementById("emailErrDiv").innerHTML = ""; 
            document.getElementById("passwordErrDiv").innerHTML = ""; 
            document.getElementById("repasswordErrDiv").innerHTML = ""; 
            document.getElementById("mobileErrDiv").innerHTML = "";
            
            document.getElementById("forgot_email").value="";
            document.getElementById("regname").value="";
	    document.getElementById("regage").value= "";
            document.getElementById("reggender").value = "";
            document.getElementById("regemail").value="";
            document.getElementById("regPassword").value="";
            document.getElementById("regConfirmPassword").value="";
            document.getElementById("regPhone1").value="";
            
 
            document.getElementById('doc_id').value=num;
            document.getElementById('login_popupId').style.display='';
	    document.getElementById('login_divDisable').style.display='';	
            document.getElementById('login_divDisable').style.height = document.body.scrollHeight + "px";
            document.getElementById('login_divDisable').style.width = document.body.scrollWidth + "px";             

      }else{
          
	    document.getElementById('login_popupId').style.display='none';
	    document.getElementById('login_divDisable').style.display='none';

      } 
   
}

function isalphabitKey(evt)
{
	var Mpost_rqrmnt_phno_id	= document.getElementsByName('Mpost_rqrmnt_phno');
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode < 31 || charCode > 32) && charCode!=8 )
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
