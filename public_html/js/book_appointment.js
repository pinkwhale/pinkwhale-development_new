var doctor = false;
var patient_name= false;
var age = false;
var mobile = false;
var gender = false;
var email =true;
var appointment_time = false;
var new_patient = false;
var new_to_system = false;

function book_final(form){

    doctor = true;
    patient_name= true;
    age = true;
    mobile = true;
    gender = true;
    email = true;
    appointment_time = true;
    new_patient = true;
    new_to_system = true;
    
    document.getElementById("doctorErrDiv").innerHTML="";
    document.getElementById("p_nameErrDiv").innerHTML="";
    document.getElementById("ageErrDiv").innerHTML="";
    document.getElementById("mobileErrDiv").innerHTML="";
    document.getElementById("genderErrDiv").innerHTML="";
    document.getElementById("emailErrDiv").innerHTML="";
    document.getElementById("appointment_timeErrDiv").innerHTML="";
    document.getElementById("NewPatientErrDiv").innerHTML="";
    document.getElementById("NewForPatientErrDiv").innerHTML="";
    
    if(document.getElementById("doc_id").value==""){
        document.getElementById("doctorErrDiv").innerHTML="Doctor Name cannot be blank";
        doctor =false;
    }
    
    if(document.getElementById("p_name").value==""){
        document.getElementById("p_nameErrDiv").innerHTML="Pateint Name cannot be blank";
        patient_name =false;
    }
    
    if(document.getElementById("age").value==""){
        document.getElementById("ageErrDiv").innerHTML="Age cannot be blank";
        age =false;
    }
    
    if(document.getElementById("mob").value==""){
        document.getElementById("mobileErrDiv").innerHTML="Mobile No. cannot be blank";
        mobile =false;
    }
    
    if(document.getElementById("email").value==""){
        document.getElementById("emailErrDiv").innerHTML="Email-id cannot be blank";
        email =false;
    }
    
    if(document.getElementById("gender").value==""){
        document.getElementById("genderErrDiv").innerHTML="Please select Gender";
        gender =false;
    }
    
    if(document.getElementById("time_slot").value==""){
        document.getElementById("appointment_timeErrDiv").innerHTML="Appointment Date and Time cannot be blank";
        appointment_time =false;
    }
    
    if(document.getElementById("new_patient").value==""){
        document.getElementById("NewPatientErrDiv").innerHTML="Are you new patient ?";
        new_patient =false;
    }
    
    if(document.getElementById("new_for_system").value==""){
        document.getElementById("NewForPatientErrDiv").innerHTML="Are you new for this System ?";
        new_to_system =false;
    }

    if(doctor && patient_name && age && mobile && email && gender && appointment_time && new_patient && new_to_system){    
        $(".cont_big").hide(); 
	document.getElementById("appointbook").disabled=true;
	document.getElementById("appointcancel").disabled=true;
	document.getElementById("appointbook").value="Booking...";
        document.getElementById("book_doc_app").action = "actions/book_appointment.php";
        document.getElementById("book_doc_app").submit();
    }else{
        return false;
    }

    
}



function book_cancel(form){

    doctor = true;
    patient_name= true;
    age = true;
    mobile = true;    
    gender = true;
    appointment_time = true;

    if(doctor && patient_name && age && mobile && gender && appointment_time){
	document.getElementById("appointbook").disabled=true;
        document.getElementById("appointcancel").disabled=true;
        document.getElementById("appointcancel").value="cancel...";
        document.getElementById("book_doc_app").action = "actions/book_appointment_cancel.php";
        document.getElementById("book_doc_app").submit();
    }else{
        return false;
    }

    
}

function isNumberKey(evt)
{
        var Mpost_rqrmnt_phno_id        = document.getElementsByName('Mpost_rqrmnt_phno');
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        {
                return false; 
        }
        else
        {
                return true; 
        }
}


function get_time_for_doc(){
    
    document.getElementById("message").innerHTML="";
    var doc_name = encodeURI(document.getElementById("doc_name").value);
    var doc_spe = encodeURI(document.getElementById("specialist").value);
    var zip = encodeURI(document.getElementById("zip").value);
    
    $("#loading").fadeIn(900,0);
    $("#loading").html("<img src='images/ajax-loader.gif' />");
    
    // -------------------ajax start--------------------- //
	/*
        var paramString = 'doctor_name='+doc_name+'&doc_spe='+doc_spe+'&zip='+zip;


        $.ajax({  
                type: "POST",  
                url: "get-time-for-booking.php",  
                data: paramString,  
                success: function(response) {                                                      
                        document.getElementById("book_procceed").innerHTML=response;                        
                        Hide_Load();
                }

        }); 
	*/
     // -------------------ajax End--------------------- //
    
    $('#book_procceed').load('get-time-for-booking.php?doctor_name='+doc_name+'&doc_spe='+doc_spe+'&zip='+zip,Hide_Load());
    
 
}

 function book_appointment(doc_id,clinic_id,ttime24,ttime12,ddate){

    doc_id = encodeURI(doc_id);        
    ttime12 = encodeURI(ttime12);
    ttime24 = encodeURI(ttime24);
    clinic_id = encodeURI(clinic_id);
    ddate =  encodeURI(ddate);

    
    $("#loading").fadeIn(900,0);
    $("#loading").html("<img src='images/ajax-loader.gif' />");
    
    
    // -------------------ajax start--------------------- //
	/*
        var paramString = 'doctor_id='+doc_id+'&clinic_id='+clinic_id+'&date='+ddate+'&slot24='+ttime24+'&slot12='+ttime12;


        $.ajax({  
                type: "POST",  
                url: "proceed_booking.php",  
                data: paramString,  
                success: function(response) {                                                      
                        document.getElementById("book-details").innerHTML=response;                        
                        Hide_Load();
                }

        }); 
	*/
     // -------------------ajax End--------------------- //
    
    
    $('#book-details').load('proceed_booking.php?doctor_id='+doc_id+'&clinic_id='+clinic_id+'&date='+ddate+'&slot24='+ttime24+'&slot12='+ttime12,Hide_Load());

}


function get_next_date(val){
    if(val==0){
        $('#next-id').html('<p><img src="images/ajax-loader_small.gif" /></p>');    
    }else{
        $('#pre-id').html('<p><img src="images/ajax-loader_small.gif" /></p>');    
    }
    
    
    // -------------------ajax start--------------------- //
	/*
        var paramString = 'nextpreval='+val;


        $.ajax({  
                type: "POST",  
                url: "get-time-booking-dynamic.php",  
                data: paramString,  
                success: function(response) {                                                      
                        document.getElementById("DocSearch_1").innerHTML=response;                        
                        Hide_Load();
                }

        }); 
	*/
     // -------------------ajax End--------------------- //
    
    $('#DocSearch_1').load('get-time-booking-dynamic.php?nextpreval='+val,Hide_Load());
    
      
   
}

function Hide_Load()
{
        $("#loading").fadeOut('slow');
};


function doc_popup(obj,value){

      if(obj==true)        
      {
            document.getElementById('doc_pop_'+value).style.display='';
            document.getElementById('doc_divDisable_'+value).style.display='';
            document.getElementById('doc_divDisable_'+value).style.height = document.body.scrollHeight + "px";
            document.getElementById('doc_divDisable_'+value).style.width = document.body.scrollWidth + "px";             

      }else{

            document.getElementById('doc_pop_'+value).style.display='none';
            document.getElementById('doc_divDisable_'+value).style.display='none';

      } 

}

function cancel_popup(obj){
    document.getElementById("token_id").value = "";
    document.getElementById("mob_no").value = "";
    document.getElementById('tokenErrDiv').innerHTML="";
    document.getElementById('mobErrDiv').innerHTML="";
    document.getElementById('CancelAppoint_detail').style.display='block';
    document.getElementById('CancelAppoint').style.display='none';

      if(obj==true)        
      {
            document.getElementById('cancel_popupId').style.display='';
            document.getElementById('cancel_divDisable').style.display='';
            document.getElementById('cancel_divDisable').style.height = document.body.scrollHeight + "px";
            document.getElementById('cancel_divDisable').style.width = document.body.scrollWidth + "px";             

      }else{
            document.getElementById('cancel_popupId').style.display='none';
            document.getElementById('cancel_divDisable').style.display='none';

      } 
   
}

var validate_cancel = true;

function cancel_app(){
     
     
    document.getElementById('tokenErrDiv').innerHTML="";
    document.getElementById('mobErrDiv').innerHTML="";
     
    if(document.getElementById("token_id").value==""){
        document.getElementById('tokenErrDiv').innerHTML="Please enter Token No.";
        validate_cancel = false;
    } 
     
    if(document.getElementById("mob_no").value==""){
        document.getElementById('mobErrDiv').innerHTML="Please enter Mobile No.";
        validate_cancel = false;
    } 
    
    if(validate_cancel==true){

        document.getElementById('CancelAppoint_detail').style.display='none';
        document.getElementById('CancelAppoint').style.display='block';

        token_id = encodeURI(document.getElementById("token_id").value);        
        mob = encodeURI(document.getElementById("mob_no").value);

        $("#CancelAppoint").fadeIn(900,0);
        $("#CancelAppoint").html("<img src='images/ajax-loader.gif' />");    

        var paramString = 'token_id=' + token_id+'&mob_no='+mob;

        $.ajax({  
            type: "POST",  
            url: "get-cancel-details.php",  
            data: paramString,  
            success: function(response) {                                  
                    document.getElementById("CancelAppoint").innerHTML=response;
                    //Hide_Load();
            }
        });
        
    }

}



function confirm_cancel(){
   
   
   var check = confirm("Are You Sure");
   if(check){
       var tok_id = encodeURI(document.getElementById("token").value);        
       var mob_id = encodeURI(document.getElementById("p_mobile").value);

       $("#CancelAppoint").fadeIn(900,0);
       $("#CancelAppoint").html("<img src='images/ajax-loader.gif' />");

       var paramString = 'token=' + tok_id+'&mob='+mob_id;

       $.ajax({  
            type: "POST",  
            url: "actions/cancel_patient_appointment.php",  
            data: paramString,  
            success: function(response) {                             
                    document.getElementById("CancelAppoint").innerHTML=response;
            }
       });
   }
    
} 

