
//var site_config = "http://10.200.202.55/";

var site_config = "http://www.pinkwhalehealthcare.com/test3/";

function doctor_list(){

    var doc_type = encodeURI(document.getElementById("specialist").value);

    if(doc_type!=""){
        // -------------------ajax start--------------------- //
        
        var paramString = 'doc_type='+doc_type;

        $.ajax({
            
                type: "POST",
                url: "get_doctor_list.php",
                data: paramString,
                success: function(response) {
                                          
                     document.getElementById("doc_id").value = "";
                     document.getElementById("cost").innerHTML = "";
                     document.getElementById("doc_list").innerHTML=response;                        
                        
                }
                
        });

        // -------------------ajax End--------------------- //
    }else{
        document.getElementById("doc_list").innerHTML="No Records";
    }
}


function user_type(){
    
    if(document.getElementById("newuser").checked==true){
        $("#newusr").show();
        $("#extusr").hide();
    }
    
    if(document.getElementById("existuser").checked==true){
        $("#newusr").hide();
        $("#extusr").show();
    }
    
}

/*
$(function(){
      $("#send").click(function(){
          validate_consult();
      });
});
*/

var valid=false;

function validate_consult(){
    
    valid = true;
    
    document.getElementById("send").value = "Upoading Please wait.....";
    document.getElementById("send").disabled = true;
    
    document.getElementById("speErrDiv").innerHTML = "";
    document.getElementById("complaintErrDiv").innerHTML = "";
    document.getElementById("contentErrDiv").innerHTML = "";
    document.getElementById("docErrDiv").innerHTML = "";
    
    
    if(document.getElementById("specialist").value==""){
        document.getElementById("speErrDiv").innerHTML = "Please select  Speciality";
        valid = false;
    }
    
    if(document.getElementById("complaint").value==""){
        document.getElementById("complaintErrDiv").innerHTML = "Complaint field cannot be Empty";
        valid = false;
    }
    
    if(document.getElementById("newuser").checked==true){        
        
        document.getElementById("emailErrDiv").innerHTML = "";   
        document.getElementById("passwordErrDiv").innerHTML = "";
        document.getElementById("retypepasswordErrDiv").innerHTML = "";
        
        if(document.getElementById("newemail").value==""){
            document.getElementById("emailErrDiv").innerHTML = "Email cannot be Empty";
            valid = false;
        }        
        
        if(document.getElementById("newpassword").value!=""){
            
            if((document.getElementById("newpassword").value).length<6 || (document.getElementById("newpassword").value).length>12){
                document.getElementById("passwordErrDiv").innerHTML = "Password length should be 6-12 characters.";
                valid = false;
            }else if(document.getElementById("newrepassword").value!=document.getElementById("newpassword").value){
                document.getElementById("retypepasswordErrDiv").innerHTML = "Password Mismatch";
                valid = false;
            }
            
        }  
        
        if(document.getElementById("newemail").value!=""){
            var valid_email = check_email(document.getElementById("newemail").value,"","1");
            
            if(valid_email == false){            
                document.getElementById("emailErrDiv").innerHTML = "Email Already Exists";
                valid = false;
            }
        }
    
    }
        
    
    if(document.getElementById("existuser").checked==true){
        document.getElementById("extemailErrDiv").innerHTML = "";   
        document.getElementById("extuserpasswordErrDiv").innerHTML = "";
        
        if(document.getElementById("extuseremail").value==""){
            document.getElementById("extemailErrDiv").innerHTML = "Email cannot be Empty";
            valid = false;
        } 
        if(document.getElementById("extuserpassword").value==""){
            document.getElementById("extuserpasswordErrDiv").innerHTML = "Password cannot be Empty";
            valid = false;
        }         
   
	if(document.getElementById("extuseremail").value!="" && document.getElementById("extuserpassword").value!=""){

            valid_email = check_email(document.getElementById("extuseremail").value,document.getElementById("extuserpassword").value,"2");

            if(valid_email==false){
                document.getElementById("emailErrDiv").innerHTML = "Invalid Email-ID or Password";
                valid = false;
            }
        }
 
    }
    
    
    if(document.getElementById("doc_id").value==""){
        document.getElementById("docErrDiv").innerHTML = "Please select Doctor";
        valid = false;
    }
    
    if(valid){
        document.request_consult.submit();
       
    }else{
        
        document.getElementById("send").value = "Send";
        document.getElementById("send").disabled = false;
        return false;
    }
    
}



function check_email(username,password,type)
{
	            
        var r_value = false;            
        // -------------------ajax start--------------------- //

        var paramString = 'username='+username+'&password='+password+'&type='+type;
        
        $.ajax({  
                type: "POST",  
                url: site_config+"actions/email_check.php",  
                data: paramString,  
                success: function(response) {                                                      
                        
                        if(response){

                            r_value = true;

                        }else{

                            r_value = false;
                            
                        }
                }

        }); 
        
        // -------------------ajax End--------------------- //	
        
        return r_value;
	
}



function check_login(username,password,type)
{
	            
        var r_value = false;            
        // -------------------ajax start--------------------- //

        var paramString = 'username='+username+'&password='+password+'&type='+type;

        $.ajax({  
                type: "POST",  
                url: site_config+"/email_login_check.php",  
                data: paramString,  
                success: function(response) {                                                      

                        if(response==true){

                            r_value = true;

                        }else{

                            r_value = false;
                            
                        }
                }

        }); 
        
        // -------------------ajax End--------------------- //	
        
        return r_value;
	
}


function update_doctor(id){
  
    // -------------------ajax start--------------------- //

    var paramString = 'id='+id;
   
    $.ajax({  
            type: "POST",  
            url: "get_doc_amount.php",  
            data: paramString,  
            success: function(response) { 
		 //alert(response);
                 document.getElementById("cost").innerHTML = response;
                 document.getElementById("doc_id").value = id;
            }

    }); 

    // -------------------ajax End--------------------- //	
    
}
