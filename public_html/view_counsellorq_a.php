<?php 
	session_start();

	if($_SESSION['login']!='Counsellor' && $_SESSION['login']!='Doctor' && $_SESSION['login']!='Dietician')
	{
		header("Location: index.php");
		exit();
	}
	$q_a_id=$_GET['q_a_id'];
	$q_a_status=$_GET['q_a_status'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Counsellor Q&A Listing</title>

<meta name="keywords" content="cancer treatment opinion,diabetes treatment opinion, surgery opinion, plastic surgery, cosmetic surgery, second opinion diagnosis"/>

<meta name="description" content="pinkWhale has brought together world renowned super-specialists, who can give you a second medical opinion online. Our 2nd opinion doctors will review your medical information, medical records, and diagnostic tests to render a
comprehensive second opinion that includes recommendations, alternative treatment options, and therapeutic considerations."/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php 
include "db_connect.php";
include 'header.php'; 
	include "actions/encdec.php";

?>
<style>
.tableclass {
 font-family: Arial,Verdana;
   font-size: 14px;
    line-height: 18px;

}

#BVQAFieldTextQuestionSummaryID {
    border-color: #666666 #DDDDDD #CCCCCC #333333;
    border-style: solid;
    border-width: 2px 2px 1px;
	font-size: 12px;
    color: #333333;
    font-family: 'Trebuchet MS','Lucida Sans',sans-serif;
    font-weight: bold;
    height: 18px;
    padding: 0 5px 3px;
    width: 245px;
}



#BVQAQuestionQuestionDetailsFieldID {
    border-color: #666666 #DDDDDD #CCCCCC #333333;
    border-style: solid;
    border-width: 2px 2px 1px;
    color: #333333;
    font-family: 'Trebuchet MS','Lucida Sans',sans-serif;
	font-size: 13px;
    font-weight: bold;
    padding: 0 5px 3px;
    width: 400px;
}
</style>
<style type="text/css">
.table-with {
	border-bottom: 1px #999 solid;
}
.td-font {
	color: #333333;
    font-family: Arial,Verdana;
    font-size: 12px;
    line-height: 18px;
    margin: 10px;
    padding: 0;
}
</style>
<!-- side Menu -->
<link rel="stylesheet" href="../css/designstyles.css" type="text/css" />

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td width="175" style="border-right:1px solid #4d4d4d" valign="top">
<table width="175" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<?php if(($q_a_status=="counsellor_pending") || ($q_a_status=="counsellor_answered")){ ?>
<tr><th>Counsellor Q&A :</th></tr><?php }?>
<?php if(($q_a_status=="doctor_pending") || ($q_a_status=="doctor_answered")){ ?>
<tr><th>Doctor Q&A :</th></tr><?php }?>
<?php if(($q_a_status=="dietician_pending") || ($q_a_status=="dietician_answered")){ ?>
<tr><th>Dietician Q&A :</th></tr><?php }?>
<?php if($q_a_status=="counsellor_pending") {?>
<tr><td><a href="counsellor_qa_listing.php"><b>Pending Queries</b></a></td></tr>
<tr><td><a href="counsellor_answered_query.php">Answered Queries</a></td></tr>
<?php }
if($q_a_status=="counsellor_answered") { ?>
<tr><td><a href="counsellor_qa_listing.php">Pending Queries</a></td></tr>
<tr><td><a href="counsellor_answered_query.php"><b>Answered Queries</b></a></td></tr>
<?php }?>
<?php if($q_a_status=="doctor_pending") { ?>
<tr><td><a href="doctor_q&a_listing.php"><b>Pending Queries</b></a></td></tr>
<tr><td><a href="doctor_answered_list.php">Answered Queries</a></td></tr>
<?php }?>
<?php if($q_a_status=="doctor_answered") { ?>
<tr><td><a href="doctor_q&a_listing.php">Pending Queries</a></td></tr>
<tr><td><a href="doctor_answered_list.php"><b>Answered Queries</b></a></td></tr>
<?php }?>
<?php if($q_a_status=="dietician_pending") {?>
<tr><td><a href="dietician_q&a_listing.php"><b>Pending Queries</b></a></td></tr>
<tr><td><a href="dietician_answered_list.php">Answered Queries</a></td></tr>
<?php }
if($q_a_status=="dietician_answered") { ?>
<tr><td><a href="dietician_q&a_listing.php">Pending Queries</a></td></tr>
<tr><td><a href="dietician_answered_list.php"><b>Answered Queries</b></a></td></tr>
<?php }?>
</table></td>
<td width="800" valign="top" >
<table border="0" class="s90dbdtbls tbl" width="580">
<tr><td>&nbsp;</td></tr>
<tr><td>
<?php $qid= $_GET['q_a_id']; 
		$sql="SELECT * FROM `q_a_db`  where q_id='$qid'";
		$qry_sql = mysql_query($sql);
		while($result = mysql_fetch_array($qry_sql))
		 { 
		 	$user_id=$result['using_user'];
			$doctor_type=$result['doctor_type'];
		
				$sql="SELECT * FROM `user_details`  where 	user_id='$user_id'";
				$qry_sql = mysql_query($sql);
				while($result1 = mysql_fetch_array($qry_sql))
				 {
					$user_name=$result1['user_name'];  
				 }
		 ?> 
          <table width="580" border="0" cellspacing="0" cellpadding="0">   
         <tr><th width="570" colspan="2"><?php echo $result['question']; ?></th></tr>       
         <tr><td class="s90dbdtbls_drdetails" width="570">
         <?php if($result['query_type']!="")  {?>
            <p>
         <span style="margin-left:20px;margin-top:10px;margin-bottom:10px;color: #333333;  font-family: Arial,Verdana; font-size: 12px; font-weight:bold;"><?php echo $result['query_type'];
		 if($result['qry_sub_one']!="") echo '---'.$result['qry_sub_one'];
		  if($result['qry_sub_two']!="") echo '---'.$result['qry_sub_two'];
		  ?>         
         
         </span><br />
           </p>
           <?php } ?>
         <p><span style="margin-left:20px;margin-top:10px;margin-bottom:10px;color: #333333;  font-family: Arial,Verdana; font-size: 12px;"><?php echo $result['question_details']; ?></span></p>
          <p style="margin-left: 20px;">
        <?php
    	if($result['q_a_upload']!='')
    	{
    	?>
    		<a href="<?php echo $result['q_a_upload'] ; ?> ">
			<img src="images/attach.png" width="16" height="16" align="absmiddle"/>Download</a>
    <?php } ?>
    </p>
         </td>
         <td width="100" valign="middle" class="s90dbdtbls_drdetails"><span style="margin-left:20px;margin-top:10px;margin-bottom:10px;color: #333333;  font-family: Arial,Verdana; font-size: 12px;"><b><?php 
		$submit_date= $result['submited_date'];
		$date=strtotime($submit_date);
		 echo date('d-m-Y', $date);
; ?></b></span></td></tr>
         </table>
<?php  } ?>
</td></tr>
<tr><td>
    <table height="20">
    <tr><td></td></tr>
    </table>
 </td></tr>
 <tr><td>
	<?php   $qid= $_GET['q_a_id']; 
		$sql="SELECT * FROM `answer_qa_query`  where qa_id='$qid'";
		$qry_sql = mysql_query($sql);
		while($result = mysql_fetch_array($qry_sql))
		 { ?>  
         <tr><td>
         <table border="0" class="s90dbdtbls_dremailtbl" width="100%">
         <tr><td class="td-font" valign="top" >
		 <p><?php echo $result['qa_answer']; ?></p>
         <p style="margin-left: 20px;">
        <?php
    	if($result['answer_qa_uploadfile']!='')
    	{
    	?>
    		<a href="<?php echo $result['answer_qa_uploadfile'] ; ?> ">
			<img src="images/attach.png" width="16" height="16" align="absmiddle"/>Download</a>
    <?php } ?>
    </p>
         </td>
         <td valign="top" align="right"><?php
		 if($result['qa_answer_date']!="0000-00-00 00:00:00") {
		 echo '<span style="font-size: 11px; font-family: Arial,Verdana;">';
		 		 
		$added_date=date("d M Y g:i A",strtotime($result['qa_answer_date']));
		$today_date=$added_date;
		$new_time1 = strtotime(date("d M Y g:i A", strtotime($today_date)) . " +10 hour");
		$new_time21=date("d M Y g:i A", $new_time1);
		$timestamp2 = strtotime("$new_time21");
		$etime = strtotime("+30 minutes", $timestamp2);

		$new_time=date("d M Y g:i a", $etime);
		echo date("d M Y",strtotime($new_time)).'<br>';
		echo date("g:i A",strtotime($new_time)).'<br>';
		 echo '</span>';
		 }
		  ?></td>            
         </tr>
         <tr> <td class="td-font" valign="top" align="right" colspan="2"><b><?php 
		 $flag=FALSE;
		 if($result['doc_user_dif']=="1" && $result['answer_by']!=""){
			 echo $result['answer_by'];
			 $flag=TRUE;
		 }
		 else if($result['doc_user_dif']=="1" && $result['answer_by']=="") { 
		 		echo  $doctor_type;
				$flag=TRUE;
		 	}
		  if($result['doc_user_dif']!="1" && $flag==FALSE) echo $user_name; ?></b></td>
          </tr>
         </table>
         </td></tr>
<?php  } ?> 
</td></tr>
<tr><td>
        <table height="20">
    <tr><td></td></tr>
    </table>
    </td></tr>
    
</table>
</td></tr>
</table>

<?php
include 'footer.php'; ?>
</body></html>