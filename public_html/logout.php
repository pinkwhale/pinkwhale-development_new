<?php 
session_start(); 
session_unset();
session_destroy(); 

if(isset($_COOKIE['PinkWhale']))			
{
	$time = time();
	setcookie("PinkWhale[username]", $time - 2592000);
	setcookie("PinkWhale[password]", $time - 2592000);
}
header( 'Location: index.php' );  

?>