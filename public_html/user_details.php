<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
     if(!isset($_SESSION['username']))
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$user_id=$_SESSION['pinkwhale_id'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/edit-user-validation.js" type="text/javascript"></script>
<script src="js/left-menu-selection.js" type="text/javascript"></script>
<script type="text/javascript" src="admin/js/jquery.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include ("db_connect.php");
include 'header.php'; ?>
<script type="text/javascript">
</script>
<!-- header.......-->
<script type="text/javascript">
function changeUserProfEditDiv()
{
		document.getElementById("edit_user_prof").style.display="block";
		document.getElementById("user_prof").style.display="none";
		document.getElementById("user_change_pswd").style.display="none";
		document.getElementById("edit").style.display="none";
		
}

function changePassWord()
{
		document.getElementById("user_change_pswd").style.display="block";
		document.getElementById("user_prof").style.display="none";
		document.getElementById("edit_user_prof").style.display="none";
		document.getElementById("edit").style.display="none";
		
		
}
</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
        <td width="220" valign="top">
            <div id="s90dashboardbg">
                <img src="images/dots.gif" />
                <a href="phr.php">My Account</a>
            </div>
            <img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
                  <?php
                include 'user_left_menu.php'; 
            ?>
                </td>
                <td width="748" valign="top" class="s90phrcontent">
<!--<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="484"><h1>My Profile </h1></td>
	<td width="264" bgcolor="#f1f1f1">
    	<div style="color:#906; font-family:Arial; font-size:16px; font-weight:bold;">
        	Pinkwhale Card No: <?php echo $user_id ; ?>
        </div>
    </td>
</tr>-->
<?php include("name_card_no.php");?>
<table>
	<tr>
		<td height='15px'></td>
	</tr>
</table>
<table height="30">
<tr><td colspan="2" align="left"><span style="font-family:Arial;color:#0966BB;font-size:15px;font-weight:bold;margin-bottom:10px;margin-left:20px;">Personal Details</span></td></tr>
</table>
<table width="650" border="0">
<tr><td width="467">
	<div style="float:right;">
	<input type="button" name="edit" id="edit" value="Edit Details" onclick="changeUserProfEditDiv()"/>
	</div>
	</td>
	<!--<td width="173"><a href="#" style="float:left;">
	  <input type="button" name="change_password" id="change_password" value="Change Password" onclick="changePassWord()"/></a></td>-->
      </tr>


<tr><td colspan="2">

<div id="user_prof" style="display:block;">

<table border="0" cellspacing="10" align="left" style="border:1px solid #CCC;"  width="700" class='tbl'>
<tr><td>
<table width="500" border="0" cellspacing="5" align="left">

<!--------All edit action done with `doc_id`='29'------------->
<?php
$qry= "SELECT * FROM `user_details` WHERE `user_id`='$user_id'";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
?>
	
<tr><td width="152" align="right"><div class="label">Name :</div></td>
<td width="206"><div class="text"><?php echo $result['user_name']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label">PinkWhale Card Number  :</div></td>
	<td><div class="text"><?php echo $result['user_id']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label">Email ID :</div></td>
    <td><div class="text"><?php echo $result['user_email']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label">Mobile :</div></td>
    <td><div class="text"><?php echo $result['mobile_num']; ?></div></td>
</tr>

</table></td>
  <td align="center">
  <div id="user_photo_upload" style="display:block;">
  <table align="left">
  <tr><td align="center" ><img src="<?php echo $result['user_photo']; ?>" width="91" height="97" /></td></tr>
	 
  </table>
</div>  
    <?php 
    }
	?>
    
</td></tr>
</table>
</div>

<div id="edit_user_prof" style="display: none;">
<table width="644" border="0" align="left" cellspacing="10" style="border:1px solid #CCC;" class="s90dbdtbls tbl">
<tr><td>

<form name="user_prof_form" id="user_prof_form" action="actions/edit_user_prof.php" method="post" enctype="multipart/form-data">
<table width="604" border="0" class="s90dbdtbls_drdetailstbl">

<!--------All edit action done with `doc_id`='29'------------->
<?php
$qry1= "SELECT * FROM `user_details` WHERE `user_id`='$user_id'";
	$qry_rslt1 = mysql_query($qry1);
	while($result1 = mysql_fetch_array($qry_rslt1))
	{
		
?>		
<tr>
<td width="213" align="right"><span style="font-family:Arial;font-size:14px;"><b>Name :</b></span></td>
<input type="hidden" id="user_card_no" name="user_card_no" value="<?php echo $user_id; ?>"/>
<td width="218" ><input type="text" name="user_name" id="user_name" value="<?php echo $result1['user_name']; ?>"/></td>

</tr>
<tr><td align="right"><span style="font-family:Arial;font-size:14px;"><b>Pinkwhale Card Number :</b></span></td><td><input type="text" name="card_id" id="card_id" disabled="disabled" value="<?php echo $result1['user_id']; ?>"/></td>

</tr>

<tr><td align="right" valign="top"><span style="font-family:Arial;font-size:14px;"><b>Email ID :</b></span></td><td colspan="2"><input type="text" name="user_email" id="user_email"  value="<?php echo $result1['user_email']; ?>"/><div id="confi_user_email_div" style="color: #F33;font-family:verdana;font-size:10px; "></div></td>

</tr>

<tr><td align="right"><span style="font-family:Arial;font-size:14px;"><b>Mobile :</b></span></td><td><input type="text" name="mobile_num" id="mobile_num"  value="<?php echo $result1['mobile_num']; ?>"/></td>

</tr>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>"/>

<tr><td align="right"><span style="font:Arial;font-size:14px;"><b>Upload Photo :</b></span></td>
  <td align="left"><input type="file" name="photo" value="<?php echo $result1['user_photo']; ?>"/></td>

  </tr>
<!--<tr>
<td>&nbsp;</td>
<td><b>Change Password</b></td>
</tr>
<tr>-->
<td>&nbsp;</td>
<td align="left">
<input type="button" name="medication_save" id="medication_save" value="Save"  onclick="checkUserEmailid(user_prof_form)"/><a href="user_details.php" style="text-decoration: none;"> &nbsp;
  <input type="button" name="medication_cancel" id="medication_cancel" value="Cancel" /></a></td>
</tr>
<tr><td colspan="3" align="center">Do you want to <span style="font-style:italic;">change your password ? </span><b><a onmouseover="this.style.cursor='pointer'" onclick="changePassWord()">Click Here</a></b></td>  
</table>
</form>
<?php 
}

?>

<!--	#######################  User Emailid Checking  ###################################-->

<script type="text/javascript">
function checkUserEmailid (form)
{

	var paramString = 'userId='+ form.user_card_no.value + '&email=' + form.user_email.value;  

		$.ajax({  
			type: "POST",  
			url: "check_user_emailid.php",  
			data: paramString,  
			success: function(response) {  
				if(response == 'success')
				{
					document.getElementById('user_prof_form').submit();
				}
				else
				{
					document.getElementById('confi_user_email_div').innerHTML ="Email address already exists";
				}	
			}
			  
		});  

}

</script>

<!--	##################### End User Emailid Checking #######################-->

</td></tr>
</table>
</div>
<?php
$qry= "SELECT * FROM `user_details` WHERE `user_id`='$user_id'";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
{
	$user_name=$result['user_name'];
	$pw_card_no=$result['user_id'];
	$email_id=$result['user_email'];
	$mobile_no=$result['mobile_num'];

}
?>

<div id="user_change_pswd" style="display:none;">
		<table width="580" border="0" cellspacing="0" cellpadding="0" class="s90dbdtbls">
		<tr><th colspan="3">Change Password :</th></tr>
       <form name="user_change_pswd_form" id="user_change_pswd_form" action="actions/user_change_pswd_action.php" method="post">
       	<tr>
			<td class="s90dbdtbls_drdetails" colspan="3">
			<table width="540" border="0" cellspacing="3" cellpadding="0" align="center" class="s90dbdtbls_drdetailstbl">
		<tr>
			<td width="100" align="right"><b>User Name :</b></td>
			<td width="165"><input type="text" name="edit_name" value="<?php echo $email_id ; ?>"disabled="disabled"/></td>
		</tr>
        <tr>
        	<td align="right"><b> New Password :</b></td>
        	<td><input type="password" name="new_pswd" /></td>
        </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="pswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
        <tr>
        	<td align="right"><b>Confirm Password :</b></td>
        	<td><input type="password" name="cnfm_pswd" /></td>
        </tr>
        
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="repswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
        <tr>
        	<td>&nbsp;</td>
        	<td>
            	<input type="button" name="pswd_button" value="Change" onclick="validate_pswd(user_change_pswd_form)" />
                <input type="button" name="cancel" value="Cancel" onclick="cancel_button()" style="margin-right:20px;text-decoration:none;" />
            </td>
        </tr>
        <input type="hidden" name="edit_pw_cardno" value="<?php echo $user_id ; ?>"/>
        
    </table>
    </td></tr>
    </form>
    </table>
</div>
</td></tr>
</table>
</tr>
</table>

<!-- footer -->

<?php
include 'footer.php'; ?>
<script type="text/javascript">
	enable_personal_info()
</script>
</body></html>
