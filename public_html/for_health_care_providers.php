<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Grow your practice with E-Clinic | pinkWhale Healthcare</title>
<meta name="keywords" content="doctor website, retain patients, new patients, visible practice, online doctor, online consultation, telephone consultation, teleconsultation, email consultation, "/>

<meta name="description" content="Doctors and Physisicians, Join pinkWhale and grow our practice. Our Affiliate Doctors service helps doctors who wish to upgrade their practices and provide phone and online consultations for their patient follow-up visits. With pinkWhale E-clinic, your practice will be visible on the web and social media with tools to attract new patients. Hospital doctors and surgeons can register and provide second opinion and increase ther visibility"/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>
<?php
	include 'header.php'; 
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td colspan="2" class="s90cpages">
<table width="980" border="0" cellspacing="0" cellpadding="0">
<tr><td width="500" ><h1><font size=4 color="purple">Doctors & Physicians: Set up your own Virtual Practice! </font></h1>

<p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />Healthcare is in the midst of a cultural shift from traditional models to technology-enabled, consumer-driven models. 
<p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" />With the penetration of Mobile phones, Email, Chat, and Internet, consumers are increasingly looking for information on the Web. 
<p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> With changing lifestyles, surging aspirations, busy work patterns, and active travel schedules, more and more medical requests are being initiated on the phone or online.</p>
<p><p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> Now, you can quickly and easily upgrade your practice and provide phone and online consultations for your patient follow-up visits.  
<p> <p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> With pinkWhale TelE-clinic, your practice will be visible on the web and social media with tools to attract new patients. </p>
<p> <p><img src="images/forbdot.jpg" width="12" height="11" align="absmiddle" style="margin-right:6px" /> Your Virtual Practice can be tightly integrated with your website.
<p><strong>To learn more, email us at:</strong> <a href="mailto:marketing@pinkwhalehealthcare.com">marketing@pinkwhalehealthcare.com</a></p></td>
<td width="480"><img src="images/for-docs_img1.jpg" width="480"/></td></tr>



</table>
</td></tr></table>
<?php
include 'footer.php'; ?>
<script type="text/javascript">
	document.getElementById("header_menu1").style.backgroundImage = "url("+sitePrefix+"/images/s90mainmenu1_bgb.jpg)";
	document.getElementById("header_menu2").style.backgroundImage = "url("+sitePrefix+"/images/s90mainmenu2_bgp.jpg)";
	document.getElementById("header_menu3").style.backgroundImage = "url("+sitePrefix+"/images/s90mainmenu3_bgb.jpg)";
</script>

</body></html>

</body></html>