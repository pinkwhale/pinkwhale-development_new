<?php
  	include("Sajax.php");
	sajax_init();
	sajax_export('getdata');
	sajax_handle_client_request();
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/registration_validation.js"></script>
<script type="text/javascript">
	<?php sajax_show_javascript(); ?>
	function check_mail(js_email)
	{
		x_getdata(js_email.value,doc_details);
	}
	function doc_details(details) 
	{
		document.getElementById('emailErrDiv').innerHTML ="";
		if(details==false)
		{
			document.getElementById('emailErrDiv').innerHTML = " Email Id  already exists !";
			document.getElementById('btnSubmit').disabled=true;
			return false;
		}
		else
		{
			document.getElementById('btnSubmit').disabled=false;
			return true;
		}
		
	}
</script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
 function getdata($email)
{
	include "db_connect.php";
	$qry   = "SELECT `user_email` FROM `user_details` WHERE `user_email`='$email'";
	$qry_rslt = mysql_query($qry);
	if (mysql_num_rows($qry_rslt) > 0) 
	{
		return false;
	}
	else
	{
		return true;
	}
}
?>
<?php include 'header.php'; ?>
    <div id="subSlider">  
<p>Join</p>
</div>
       <div id="content">
<!-- header.......-->
<form action="actions/registration_action.php" method="post" name="reg_form" id="reg_form">
  <table width="1000" border="0" cellspacing="0" cellpadding="0" align="center"> 
  	
    <tr>
<!--    <td>
    <table border="0" cellpadding="0" cellspacing="1"  align="left" class="s90greybigbox">
    <tr><td>
    <img src="images/Second Opinion Offline Poster.jpg" width="400" height="566"/></td></tr>
    </table></td>-->
    
    <td align="center"><table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform" >
  		<tr><th colspan="2">Not Yet registered? Register Now</th></tr>
  		<tr><td colspan="2"><img src="images/spacer.gif" width="5" height="8" /></td></tr>
    <tr>	
        	<td width="40%" align="right">Your Name<font color="#FF0000">*</font>:</td>
  			<td width="60%" align="left">
  				<input size="20" type="text" maxlength="40" name="regname" value="" class="s90regformtext">
        	</td>
        </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
  		<tr>
            <td align="right">Your Email<font color="#FF0000">*</font>:<span onmouseover="showsummary('details', event, 'To be used as User ID');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
            <td nowrap="nowrap">
            	<input class="s90regformtext" type="text" maxlength="45" size="20" id="regemail" name="regemail" value="" onblur=" return check_mail(regemail);">
            </td>
        </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
  		<tr>
        	<td align="right">Password<font color="#FF0000">*</font>:<span onmouseover="showsummary('details', event, 'Please enter password of your choice with a minimum of 6 characters and a maximum of 12 characters.');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
  			<td>
  			<input type="password" class="s90regformtext" maxlength="12" size="20"  id="regPassword" name="regPassword" value=""><span style="color:green" id='result'></span>
         	</td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
  	   <tr>
       		<td align="right">Confirm Password<font color="#FF0000">*</font>:</td>
  			<td>
  				<input type="password" class="s90regformtext" maxlength="12" size="20" name="regConfirmPassword" value="">
            </td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
  	   <tr>
       		<td align="right" style="line-height:20px;">Mobile #<font color="#FF0000">*</font>:</td>
  			<td style="line-height:30px;">
  				<input type="text" class="s90regformtext" maxlength="20" name="regPhone1" value="" onkeypress="return isNumberKey(event);"></td>
       </tr>
<!--    ERROR DIV -->
		<tr>
        	 <td> </td>
             <td  align="left" height="8">
	            <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV -->
  	   <tr>
       <td align="right" style="line-height:30px;">PinkWhale Card #:</td>
  	   <td style="line-height:30px;">
  			<input type="text" class="s90regformtext" maxlength="20" name="regpwcard1" value=""></td></tr>
  	  <tr>
      	<td align="right" style="line-height:30px;">Where did you hear of us:</td>
            <td style="line-height:30px;">
                <select name="reghear" style="margin-left:8px" id="reghear" onchange="showFriendData()">
                	  <option value="">- Select -</option>
                	  <option value="Friends">Friends</option>
                      <option value="Internet">Internet</option>
                      <option value="Doctor">Doctor</option>
                      <option value="Pamphlet">Pamphlet</option>
                      <option value="SMS">SMS</option>
                      <option value="Email">Email</option>
               </select>
            </td>
        </tr>
  <script type="text/javascript">
  function showFriendData()
  {
  	var friend=document.getElementById('reghear').value;
	if(friend=="Friends")
	{
		document.getElementById("friends_details").style.display="block";
	}
	else
	{
	
		document.getElementById("friends_details").style.display="none";
	} 
  
  }  
  </script>     
        
     <tr><td colspan="2">
     <div id="friends_details" style="display:none;">
     <table width="348" border="0"  align="center" cellpadding="0" cellspacing="1" >
     <tr><td width="117" align="right">Friend's Name :</td>
     <td width="222" style="line-height:30px;"><input type="text" class="s90regformtext" maxlength="20" name="friends_name" id="friends_name" value=""></td></tr>
     
     <!--    ERROR DIV -->
     <tr>
     <td> </td>
             <td  align="left" height="8">
	            <div id="FriErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
     
     <tr><td width="117" align="right">Friend's Email Id :</td>
     <td width="222" style="line-height:30px;"><input type="text" class="s90regformtext" maxlength="20" name="friends_email"  id="friends_email" value=""></td></tr>
     </table>
     </div>
     </td></tr>
        
        
  		<tr>
        	<td colspan="2"><img src="images/spacer.gif" width="44" height="30" align="absmiddle" />
  				<input type="checkbox" name="regqnp" class="FormInput" value="Y" >I would like to receive health updates?
            </td>
       	</tr>
  		<tr>
        	<td colspan="2"><img src="images/spacer.gif" width="44" height="20" align="absmiddle" />
  				<input type="checkbox" name="regagreeterms" value="agree"  >I agree to the <a href="terms.php" onmouseover="this.style.cursor='pointer';" style="text-decoration: underline;">Terms of service</a><font color="#FF0000">*</font>
            </td>
        </tr>
<!--    ERROR DIV -->
		<tr>
             <td  colspan="2" height="8" align="center">
	            <div id="termsErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
        </tr>
<!--  END ERROR DIV --> 
  		<tr>
        	<td>&nbsp;</td>
        	<td height="50">
  				<input onmouseover="this.style.cursor='pointer'" type="button" name="btnSubmit" value=" Join Now " onclick="PWRegisterSignUp(reg_form)" id="btnSubmit">
        	</td>
        </tr>
      </table>
 </td></tr></table>
</form>



<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
