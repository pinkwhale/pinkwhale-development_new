<?php
session_start();
include "actions/encdec.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="js/stepcarousel.js" type="text/javascript"></script>
<script src="js/carousel.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<script src="js/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/header_colourchange.js"></script>
<script src="js/mouseoverscripts.js" type="text/javascript"></script>
<script src="js/NoIEActivate.js" type="text/javascript"></script>

<!-- header.......-->
<?php include 'header.php'; ?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold'</script>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">

<tr><td colspan="2"><img src="images/steps_smo1.jpg" width="978" /></td></tr>

<tr><td width="169" style="border-right:1px solid #4d4d4d" valign="top">
<table width="160" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<tr><th>Specialities:</th></tr>
<?php
	include "db_connect.php";
	$qry="SELECT `doc_specialities` FROM `pw_doctors` WHERE `doc_category`='Specialist' && blocked !='Y' group by`doc_specialities`";
		if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
?>
			<tr><td><a href="specialist.php?specialist=<?php echo $qry_result['doc_specialities']; ?>&id=0"><?php echo $qry_result['doc_specialities']; ?></a></td></tr>		
<?php 	} ?>
</table></td>
<td width="798" valign="top"><table width="798" border="0" cellspacing="0" cellpadding="0">
<tr><td width="548" class="s90specialtiestext" valign="top"><h1>Consult With Specialist </h1>
<p> Searching all over the Internet to find a doctor?  If you are looking for convenient access to the best specialist doctors, you search ends here. Follow-up consultation with doctor has never been this easy! </p>
<p> We have a panel of qualified and renowned specialist doctors in several medical areas. </p>
<p> Our specialists provide the convenience of phone consultation or online consultation for follow-up questions and visits. <strong> Find Your Specialist Now!</strong></p>
<br /><br /><br />
<!--<img src="images/specialist_steps.jpg" width="480" height="66" style="margin-top:10px" />-->
<table width="100%" border="0" cellspacing="4" cellpadding="0" class="s90specialtiesdocs" align="center" style="margin-top:10px">
<tr><th colspan="7">Featured Specialists:</th></tr>

<?php
	include "db_connect.php";
	$qry = "SELECT * FROM `pw_doctors` WHERE (`doc_category`='Specialist' || `doc_category`='Specialist/Expert') && blocked !='Y'"; 
	if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
	?>		
	<tr>
		<td>
			<div id='expertPhotos'>
				<div id="expgallery" class="stepcarousel">
					<div class="belt"> 
						<?php	
						$i=0;
						while($qry_result=mysql_fetch_array($qry_rslt))
						{  
							$i++;
							?>
							<div class="panel">
								<a href="specialist.php?specialist=<?php echo $qry_result['doc_specialities']; ?>&id=<?php echo base64_encode($qry_result['doc_id']); ?>">
									<img src="<?php echo $qry_result['doc_photo']; ?>" width="72" height="72" />
								</a><br />
								<?php echo $qry_result['doc_name']; ?><br />
								(<?php echo $qry_result['doc_specialities']; ?>)
							</div>	
							<?php 
						}	 
						?>
					</div>
				</div>
			</div>
		</td>							
	</tr>	
	
	<?php 
		if($i > 5) //More than 5 doctors are available
		{
			?><script language='javascript'>expertCarousel();</script><?php
		}
	?>	

</table>
</td>
<td align="right" valign="top"><img src="images/specialist_banner.jpg" width="240" style="margin-left:10px"/></td></tr></table></td></tr>
<tr><td colspan="2">


<div id="s90specialtfaqs">
<h3>FAQ's:</h3>

<ol>
<li><strong>Why should I use pinkWhale's specialists?<br />
Ans.</strong> We bring access to the best specialist doctors for your convenience. We provide the convenience of teleconsultation and online consultation for follow-up visits.</li>

<li><strong>What should I discuss with a specialist?<br />
Ans.</strong> Any medical problem for you or for any of your members in your family.</li>

<li><strong>What does it cost?<br />
Ans.</strong> It varies depending on the specialist, and the area of specialty. The cost that each specialist charges is listed on the specialist page.  When you consult by phone or online, you are saving on the travel time & waiting time too.</li>


<li><strong>How are payments made?<br />
Ans.</strong> By cash or cheque at the doctor's office. By credit card, debit card, at pinkWhale website.</li>

<li><strong>Can you give me a referral for a specialist in my locality?<br />
Ans.</strong> Please call us at: Please write to us at info@pinkwhalehealthcare.com. We will help you to identify the right specialist for you.</li>

<li><strong>When is phone or email follow-up consultation appropriate?<br />
Ans.</strong><strong>1.</strong> Email and phone consultation is NOT for, emergencies or life-threatening situations (for example, stroke, chest pain).<br />
<strong>2.</strong>	Email consultation is appropriate for non-urgent care when you need medical advice, a second medical opinion, or you need clarification on a treatment plan.<br />
<strong>3.</strong>	Phone consultations are appropriate for Urgent Care, not Emergency Care.<br /> For example:<br />
<blockquote>
<strong>a.</strong>	For diabetes patients, if you are going into hypoglycemia, a quick advice from the doctor on the phone can save you the rush to the ER. <br /> 
<strong>b.</strong>	After a post-cataract surgery, you want to follow-up with doctor because of a red eye or pain in the eye.  <br />
<strong>c.</strong>	After your chemo sessions, you want to call because of side-effects such as gastritis, constipation, or loss of hair. <br />
<strong>d.</strong>	A couple of days after the in-person visit to the doctor, your child continues to have fever, cold, cough, and you want to confirm medications or need to start new medications.<br />
<strong>e.</strong>	In case of a foreign body in the eye or ear, to get advice on the first aid. <br />
<strong>f.</strong>	If you have other medical or surgical conditions, a call to get advice is appropriate.<br />
</blockquote>
</li>
<li><strong>How long is a phone follow-up consultation?<br />
Ans.</strong> There are no time limits. It is up to and your doctor to determine depending on the situation. It's good practice for you to think about write down your questions you want to ask/discuss before you start the phone consultation.</li></ol>
</div>
</td></tr></table>


<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>
