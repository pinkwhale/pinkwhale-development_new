<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php include 'header.php'; ?>
<!-- header.......-->
<?php
require_once('calendar/classes/tc_calendar.php');
?>
<?php 

$qry= "SELECT doc_category,doc_name,doc_id,doc_category_change_flag,activate_online_consultation FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
		$doc_ctgry_flag=$result['doc_category_change_flag'];
		$doc_online_consult = $result['activate_online_consultation'];
	}
?>
<?php
$date1=date("Y-m-d", time()-86400);
$date2=date("Y-m-d");
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top">
<div id="s90dashboardbg">
    <img src="images/dots.gif" />
    <a href="doc_phr.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>
    </div>
    </td>
</tr>
</table>
<table height="30"></table>
</div>

</td></tr></table>
<?php
include 'footer.php'; ?>
</body></html>
