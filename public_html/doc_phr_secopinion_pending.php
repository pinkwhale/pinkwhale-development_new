<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='doctor')
	{
		header("Location: index.php");
		exit();
	}
	else
	{
		$doctor_id=$_SESSION['doctor_id'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/my_css.css" rel="stylesheet" type="text/css">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/doc_phr_mail_validation.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->
<?php 
$qry= "SELECT doc_category,doc_name,doc_id FROM `pw_doctors` where `doc_id`='$doctor_id' ";
$qry_rslt = mysql_query($qry);
while($result = mysql_fetch_array($qry_rslt))
	{
		$doc_category = $result['doc_category'];
		$doc_name = $result['doc_name'];
		$doc_id = $result['doc_id'];
	}
?>

<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><!--<div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'doc_phr_left_menu.php'; ?>
<!-- ///// left menu //////  -->
</td>
<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Second Opinion</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	<div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $doc_name;?>, Pinkwhale ID <?php echo $doc_id ; ?>
        </div>
    </td>
</tr>
</table>
<?php
include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;
?>
		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='s90registerform'>
		<tr><th colspan="5" >Pending Consults</th></tr>
        <?php	
			$count =0;
			// Display all the data from the table 
	$qry1 = "SELECT MAX(`mail_id`),min(`entered_date`),max(entered_date) FROM `pw_expert_consultation_emails` WHERE `doctor_id` = '$doctor_id' GROUP BY `consultation_id`";
	$qry_rslt1 = mysql_query($qry1);
	
	$mailIds = "";
	$mailDataArray = null;
	
	while($result1 = mysql_fetch_array($qry_rslt1))
	{
		$max_ref_id=$result1['MAX(`mail_id`)'];
		if($mailIds=="") $mailIds = $result1['MAX(`mail_id`)'];
		else $mailIds .= ",".$result1['MAX(`mail_id`)'];
		$mailDataArray[$result1['MAX(`mail_id`)']] = $result1['min(`entered_date`)']."||".$result1['max(entered_date)'];
	}
	
	if(mysql_num_rows($qry_rslt1) > 0)
	{
		//$sql="SELECT * FROM `pw_expert_consultation_emails` WHERE `mail_status`!='closed' && `mail_id`='$max_ref_id'";
		$sql="SELECT * FROM `pw_expert_consultation_emails` WHERE `mail_status`!='closed' && `mail_id` in ($mailIds)";
		$obj->query($sql);	
		
		$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
		$pager->setDebug(true);
		$rs = $pager->paginate();
		
		if(mysql_num_rows($rs) > 0)
		{
			?>
			<tr>
				<td align='left'><strong>SI No:</strong> </td>
				<td align='left'><strong>Pinkwhale id</strong></td>
				<td align='left'><strong>User Name</strong></td>
				<td align='left'><strong>Booking Date</strong></td>
				<td align='left'><strong>Last Modified date</strong></td>
			 </tr>
			<?
			while($row = mysql_fetch_assoc($rs)) 
			{
				$count ++;
				$pw_id			= $row['pw_card_id'];
				$user_name		= $row['patient_name'];
				$enteredDate 	= explode("||", $mailDataArray[$row['mail_id']]);
				$booking_date 	= $enteredDate[0];
				$last_modified	= $enteredDate[1];
				
				$gdate1=strtotime($booking_date);
				$final_date1=date("d M Y", $gdate1);
				
				$gdate2=strtotime($last_modified);
				$final_date2=date("d M Y", $gdate2);
						
				//$booking_date=$result1['min(`entered_date`)'];
				//$last_modified=$result1['max(entered_date)'];
				echo "<tr bgcolor='#ffffff'>";
					echo "<td>$count</td> ";
					echo "<td>";
					?>
					<a href="exp_ doc_email_consultation.php?expd=<?php echo $row['consultation_id']; ?>&cad_id=<?php echo $row['pw_card_id']; ?>" style="text-decoration:underline;"><?php echo $pw_id ; ?></a></td> 
					<?php
					echo "<td>$user_name</td> ";
					echo "<td>$final_date1</td> ";
					echo "<td>$final_date2</td> ";

				echo "</tr>";          			
			}
	
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>','</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
			<tr><td colspan='5' align='center'>
			<?php
			echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			
		}
		else
		{
			echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
		}	
	}
	else
	{
		echo htmlForNoRecords("700", true, "Your online pending consultion is empty");
	}		
		?>
	</table>
</td></tr>
</table>
</td>
</tr></table>

<!-- footer -->
<script language="javascript" type="text/javascript">
enable_sec_opinion_submenu();
</script>
<?php
include 'footer.php'; ?>

</body></html>
