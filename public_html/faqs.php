<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<?php
	include 'header.php';
?>
<!-- header.......-->

<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" class="s90greybigbox">
<tr><td align="center">

<table width="1000" border="0" cellspacing="10" cellpadding="0" class="s90cardstbl">
<tr><td width="254" valign="top"><img src="images/topup_banner.jpg" width="254" height="350" /></td>
<td width="712" valign="top" class="s90genfaqs"><h1>Frequently Asked Questions:</h1>

<ol><li><strong>How do I communicate with a doctor?<br>
Ans.</strong> You can communicate with the doctor through:<br>
- A phone call</a> <br>
- Online consultation</li>

<li><strong>What does it cost?<br>
Ans.</strong> There are various options available.</li>

<li><strong>How do I pay?<br>
Ans.</strong> Payment can be made through your credit or debit card via a secure payment gateway.</li>

<li><strong>What doctors are available? At what times are they available?<br>
Ans.</strong> We are building a network of specialists & general physicians.</li>

<li><strong>How confidential is my doctor-patient interaction?<br>
Ans.</strong> Your health records will not be accessed by anyone other than you and the consulting doctor</li>

<li><strong>What are the benefits I get if I register with pinkWhale?<br>
Ans.</strong> <ul><li>Access to specialist doctors from the comfort of your home / office</li>

<li>Ability to call or chat with doctors without waiting in queues and spending time on travel</li> 
<li>Diagnostic services at your doorstep.</li></ul>
<strong>Additional benefits if you subscribe to our packages:</strong>
<ul><li>Access to a Personal Health Record system that helps track vital health parameters and sends alerts.</li> 
<li>Reduced rates on call / chat sessions with our doctors.</li>
<li>Discounted rates on diagnostic services.</li></ul></li></ol>

</td></tr></table></td></tr></table>


<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>