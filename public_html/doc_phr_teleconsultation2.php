<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

<?php
include 'header.php'; ?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="220" valign="top"><div id="s90dashboardbg"><img src="images/dots.gif" /><a href="doc_phr.php">Dashboard</a></div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />
<table width="220" border="0" cellspacing="0" cellpadding="0" class="s90phrmenus">
<tr><td><img src="images/phr_menustop.jpg" width="220" height="6" /></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_profile.php">My Profile</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_accountstatus.php">Account Status</a></td></tr>
<tr><td><img src="images/dots2.gif" class="s90phrdotsarrow"/><a href="doc_phr_secondopinion.php">Second Opinion</a></td></tr>

<tr><th><div id="s90phrsubmenus2"><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_teleconsultation.php">Telephone Consultations</a></div>
<div id="s90phrsubmenus"><a href="doc_phr_teleconsultation.php">Pending Consults</a><br />
<a href="doc_phr_teleconsultation2.php">All Consults</a></div></th></tr>

<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_emailconsultation.php">E-mail Consultation</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_availability.php">Availability</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_appointments.php">Appointments</a></td></tr>
<tr><td><img src="images/dots.gif" class="s90phrdotsarrow" /><a href="doc_phr_users.php">My Clients</a></td></tr>
<tr><td><img src="images/phr_menusbot.jpg" width="220" height="6" /></td></tr></table></td>

<td width="748" valign="top" class="s90docphr"><h1>Second Opinion</h1>

<img src="images/docphr_allconsults.jpg" width="688" height="74" style="margin:20px 0px 0px 30px" /></td>
</tr></table>

<!-- footer -->

<?php
include 'footer.php'; ?>
</body></html>