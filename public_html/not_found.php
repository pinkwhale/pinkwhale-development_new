<?php
function htmlForNoRecords($width, $outer = false, $label = 'No records available')
	{
		$htmlContent =  "";
		
		//if outer is true table content should be inside tr td tags
		if($outer == true)
		{
			$htmlContent =  "<tr>
								<td class='s90dbdtbls_drdetails'>";
		}
		
		$htmlContent	 .=	"<table width='$width' border='0' cellspacing='0' cellpadding='5' align='center' style='font-size:15px;font-family:arial;color:#21759B;'>
								<tr>
									<td align='center'><b>$label</b></td>
								</tr>
							</table>";
							
		if($outer == true)
		{
			$htmlContent .=  	"</td>
							</tr>";
		}					
							
		return $htmlContent;
	}
	?>
