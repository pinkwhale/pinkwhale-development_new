<?php
require("sajax.php");
sajax_init();			
$sajax_debug_mode =0;
sajax_export("getdoctorinfo");
sajax_handle_client_request();
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/specialist.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>
<?php
	include 'header.php';
?>
<!-- header.......-->


<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<!--<tr><td colspan="2"></td></tr>-->


<tr><td width="169" style="border-right:1px solid #4d4d4d" valign="top">
<table width="160" border="0" cellspacing="0" cellpadding="0" class="s90specialties">
<tr><th>Specialities:</th></tr>
<?php
	include "db_connect.php";
	if($_GET['specialist'])
	$specialities=$_GET['specialist'];
	$qry="SELECT `sp_specialities` FROM `specialist_doctors` group by`sp_specialities`";
		if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
		while($qry_result=mysql_fetch_array($qry_rslt))
		{
?>
			<tr><td><a href="specialist.php?specialist=<?php echo $qry_result['sp_specialities']; ?>&id=0"><?php echo $qry_result['sp_specialities']; ?></a></td></tr>		
<?php 	} ?>
</table></td>

<td width="798" valign="top"><table width="798" border="0" cellspacing="0" cellpadding="0">

<tr><td><table width="790" border="0" cellspacing="0" cellpadding="0" class="s90specialtiesdocs" align="center" style="margin-top:10px;">
<tr><th colspan="9"><?php echo $specialities; ?>:</th></tr>
<?php
	for($j=0;$j<8;$j++)
	{
			$doctor_id[$j]=0;
			$doctor_photo[$j]="";
			$doctor_name[$j]="";
			$doctor_splisd_for[$j]="";
	}

	$i=0;
	$qry="SELECT * FROM `specialist_doctors` where `sp_specialities`='$specialities' order by sp_doctors_id ASC";
	if(!$qry_rslt=mysql_query($qry)) die(mysql_error());
	while($qry_result=mysql_fetch_array($qry_rslt))
	{
		 $i++;
		 $doctor_id[$i]=$qry_result['sp_doctors_id'];
		 $doctor_photo[$i]=$qry_result['sp_doctors_photo']; 
		 $doctor_name[$i]=$qry_result['sp_doctor_name'];
		 $doctor_splisd_for[$i]=$qry_result['sp_specialist_for'];
		 $doctor_specialisties=$qry_result['sp_specialities'];
        
 } ?>  
<tr><td width="20" class="s90unselected"><img src="images/larrow.jpg" width="16"></td>

<td width="107" id="s90unselected1">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[1]);?>" >
        <img src="<?php echo $doctor_photo[1]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[1]; ?>
        <br /><?php echo $doctor_splisd_for[1]; ?>
    </a>    
</td>
<td width="107" id="s90unselected2">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[2]); ?>" >
        <img src="<?php echo $doctor_photo[2]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[2]; ?>
        <br /><?php echo $doctor_splisd_for[2]; ?>
    </a>
</td>
<td width="107" id="s90unselected3">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[3]); ?>" >
        <img src="<?php echo $doctor_photo[3]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[3]; ?>
        <br /><?php echo $doctor_splisd_for[3]; ?>
    </a>
</td>
<td width="107" id="s90unselected4">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[4]); ?>" >
        <img src="<?php echo $doctor_photo[4]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[4]; ?>
        <br /><?php echo $doctor_splisd_for[4]; ?>
    </a>
</td>
<td width="107" id="s90unselected5">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[5]); ?>" >
        <img src="<?php echo $doctor_photo[5]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[5]; ?>
        <br /><?php echo $doctor_splisd_for[5]; ?>
    </a>
</td>
<td width="107" id="s90unselected6">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[6]); ?>" >
        <img src="<?php echo $doctor_photo[6]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[6]; ?>
        <br /><?php echo $doctor_splisd_for[6]; ?>
    </a>
</td>
<td width="107" id="s90unselected7">
	<a href="JavaScript:void(0);" style="text-decoration:none; font:Verdana, Geneva, sans-serif; font-size:10px;" onclick="<?php getdoctorinfo($doctor_id[7]); ?>" >
        <img src="<?php echo $doctor_photo[7]; ?>" width="72" height="72" />
        <br /><?php echo $doctor_name[7]; ?>
        <br /><?php echo $doctor_splisd_for[7]; ?>
    </a>
</td>
<td width="21" class="s90unselected"><img src="images/rarrow.jpg" width="16"></td></tr>
<tr><th colspan="9" class="s90selecteddrdetails">
<p>
<?php
function get_doctor_information($id)
{
	$qry   = "SELECT `sp_about_doctor` FROM `specialist_doctors` WHERE `sp_doctors_id`='$id'";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
		$abt_doctor = $result['sp_about_doctor'];
	}
	echo $abt_doctor ;
}
?>
</p>
<table width="100%" border="0" cellspacing="8" cellpadding="0" align="center" class="s90selecteddrcalls">
  <tr>
    <td width="132"><h6>New Patients:</h6></td>
    <td colspan="2"><img src="images/appointment.jpg" width="46" height="37" align="absmiddle" style="margin-right:6px"/>Call <strong style="color:#b60359"><?php //echo $qry_result1['sp_face_to_ face_cnsltion_phno']; ?></strong> to book an appointment for face to face consultation</td>
    </tr>
  <tr>
    <td><h6>Existing Patients:</h6></td>
    <td width="278"><img src="images/pconsult-icon.jpg" width="46" height="37" align="absmiddle" style="margin-right:6px"/>Call <strong style="color:#b60359"><?php //echo $qry_result1['sp_tele_consult_phno']; ?></strong> for a Tele-Consult</td>
    <td width="354"><a href="cs_rl.html"><img src="images/emailconsult2.jpg" width="347" height="37" align="left" /></a></td>
  </tr>
<?php
	if($_GET['id']<=$doctor_id[1])
	{
		?> <script type="application/javascript" > bgchange1(); </script> <?php 	
	}
	else if($_GET['id']==$doctor_id[2])
	{
		?> <script type="application/javascript" > bgchange2(); </script> <?php
	}
	else if($_GET['id']==$doctor_id[3])
	{
		?> <script type="application/javascript" > bgchange3(); </script> <?php
	}
	else if($_GET['id']==$doctor_id[4])
	{
		?> <script type="application/javascript" > bgchange4(); </script> <?php
	}
	else if($_GET['id']==$doctor_id[5])
	{
		?> <script type="application/javascript" > bgchange5(); </script> <?php
	}
	else if($_GET['id']==$doctor_id[6])
	{
		?> <script type="application/javascript" > bgchange6(); </script> <?php
	}
	else if($_GET['id']==$doctor_id[7])
	{
		?> <script type="application/javascript" > bgchange7(); </script> <?php
	}
?>
</table>


</th></tr></table></td></tr></table></td></tr></table>

<!-- footer -->

<?php
include 'footer.php'; ?>

</body></html>