<?php
ob_start();
session_start();
include "site_config.php";
include ("db_connect.php");

$doc_name = mysql_escape_string(trim(urldecode($_POST['doc_name'])));

$doc_spe = mysql_escape_string(trim(urldecode($_POST['specialist'])));

$spe = $doc_spe;

if($doc_name!=null){

    $doc_name = " and doc_name like '%".$doc_name."%'";

}

if($doc_spe!=null){
    
    $doc_spe = " and doc_specialities='".$doc_spe."'";
    
}

$display = "<div align='center'><img src='images/pinkOpinioBar.jpg' /></div>";

$display .= "<div id='DocSearch' style='border:0px'><table align='center' align='center' border='0' cellpadding='0' cellspacing='1' width='100%' class='attable' id='mytable'>";
 
$display .="<tr>";

if($spe!=null){
 
    $display .="<th width='40%' align='left' style='font-size:16px'>$spe</th>";
    
}else{
    
    $display .="<th width='40%' align='left' style='font-size:16px'><strong>Specialists</strong></th>";
}

$display .="<th align='center'>Visiting Hospitals</th><th align='center'>Visiting Clinics</th><th align='center'></th></tr>";

$qry = "SELECT * FROM `pw_doctors` where  blocked !='Y' && (`doc_category`='Expert' || `doc_category`='Specialist/Expert') $doc_name $doc_spe order by doc_id ASC";

$res = mysql_query($qry);

$num = mysql_num_rows($res);

if($num>0){

    while($dat = mysql_fetch_array($res)){

        $display .= "<tr><td >";

        if($dat['doc_photo']!=""){
        
            $display .= "<div class=\"docPhoto\"><a href='#'><img src='".$dat['doc_photo']."' width=\"73\" height=\"83\" onclick=\"spe_popup(true,'".$dat['doc_id']."')\" /></a></div>";

        }else{
            
            $display .= "<div class=\"docPhoto\"><a href='#'><img src='images/doctor.png' width=\"73\" height=\"83\" onclick=\"spe_popup(true,'".$dat['doc_id']."')\" /></a></div>";
            
        }
        
        $display .= "<div class=\"docDis\"><a href='#' onclick=\"spe_popup(true,'".$dat['doc_id']."')\"><span>".$dat['doc_name']."</span></a> <br /> ".$dat['doc_qualification']."<br /><br />";

	$display .="<form method='post' action='".$sitePrefix."/patient_new_exp_email_consultation.php'><input type='hidden' name='doc_id' value='".$dat['doc_id'] . "' />";
        
        if(!isset($_SESSION['username']))
        {
              
                $display .= "<a href='#top'><img src='".$sitePrefix."/images/stratConsultBtn.jpg' onClick='login_callpopup(true,\"".$dat['doc_id']."\");' style='cursor:pointer;' /> </a>";
        }
        else
        {
                $display .= "<input src='".$sitePrefix."/images/stratConsultBtn.jpg' type='image' style='cursor:pointer;'/>";
        }
        
        $display .= "</form>";

        $display .= "</div></div>";
        
        $display .= "<!--Specialist-pop-up--><div id=\"spe_pop_".$dat['doc_id']."\" style=\"display:none; z-index: 10008; left:10%; height:1000px;  width:85%; height: auto; position: absolute; top:20%;   border:4px solid #666; background-color:#fff;\" >";

        $display .= "<div style=\"float:right ; padding:5px;\"> <a href=\"#\" onclick=\"spe_popup(false,'".$dat['doc_id']."')\">
                     <img src=\"images/close.gif\" border=\"0\" /></a> 
                     </div>";
        
        if($dat['doc_photo']!=""){
        
            $display .= "<div><table><tr><td align='left' colspan='2'><div class=\"docPhoto\"><img src='".$dat['doc_photo']."' width=\"73\" height=\"83\"/></div>";
                    
        }else{
            
            $display .= "<div><table><tr><td align='left' colspan='2'><div class=\"docPhoto\"><img src='images/doctor.png' width=\"73\" height=\"83\"/></div>";
            
        }
        $display .= "<div class=\"docDis\"><strong style='color:#0C4686'>".$dat['doc_name']." </div>";
      /*  
        $display .= "<div style='text-align:right;'>";
        $display .= "<form method='post' action='".$sitePrefix."/patient_new_exp_email_consultation.php'><input type='hidden' name='doc_id' value='".$result['doc_id'] . "' />";
        if(!isset($_SESSION['username']))
        {
                $display .= "<a href=\"javascript: spe_popup(false,'".$dat['doc_id']."')\" ><img src='".$sitePrefix."/images/emailsopinion.jpg' onClick='login_callpopup(true,\"".$dat['doc_id']."\");' style='margin-right:10px; cursor:pointer;' /></a>";
        }
        else
        {
                $display .= "<input src='".$sitePrefix."/images/emailsopinion.jpg' style='margin-right:10px;' type='image' />";
        }
        $display .= "</form>";
        
        $display .="<p align='right'><b>Call +91-80-43000333 to discuss your situation <br/>with our Second Opinion Case Manager.</b></p></div>";
        */
        $display .= "</td></tr>";
        
        $display .= "<tr><td><strong>Qualification</strong> &nbsp;:&nbsp;".$dat['doc_qualification']." </td></tr>";
        
        $display .= "<tr><td><strong>Specialization</strong> &nbsp;:&nbsp;".$dat['doc_specialist_for']." </td></tr>";
        
        
        $display .= "<tr><td><p>".$dat['about_doctor']."</p></td></tr>";
        
        $display .= "</table></div>";
        
        $display .= "</div>";
        
        $display .= "</div><div id=\"spe_divDisable_".$dat['doc_id']."\" style=\"DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0; POSITION: absolute; TOP: 0; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48\"> </div>";
                  
        $display .= "<!-- POP-UP-END -->";
        
        $visit_hospital_name = $dat['visit_hospital_name'];        
        
        $display .= "</td><td align='center'>";
        
        if($visit_hospital_name==""){
            
            $display .="<font color='red'>None</font>";
            
        }else{
            
            $visit_h_name = explode(";", $visit_hospital_name);
            
            $h_size = sizeof($visit_h_name);
            
            for($i=0 ; $i<$h_size ;$i++ ){
                
                $display .= "<font color='#4065B3'>".$visit_h_name[$i]."</font><br />";
                
            }
            
        }
        
        $display .="</td >";
        
        $visit_clinic_name = $dat['visit_clinic_name'];
        
        $display .= "</td><td align='center'>";
        
        if($visit_clinic_name==""){
            
            $display .="<font color='red'>None</font>";
            
        }else{
            
            $visit_c_name = explode(";", $visit_clinic_name);
            
            $clinic_size = sizeof($visit_c_name);
            
            for($i=0 ; $i<$clinic_size ;$i++ ){
                
                $display .= "<font color='#4065B3'>".$visit_c_name[$i]."</font><br />";
                
            }
            
        }
        
        $display .="</td>";
        
        $display .= "<td align='center'>";
      /*  
        $display .="<form method='post' action='".$sitePrefix."/card_topup.php'><input type='hidden' name='doc_id' value='".$dat['doc_id'] . "' />";
        
        if(!isset($_SESSION['username']))
        {
                $display .= "<img src='".$sitePrefix."/images/buyOpinionBtn.jpg' onClick='displayLoginForm();' style='cursor:pointer;' />";
        }
        else
        {
                $display .= "<input src='".$sitePrefix."/images/buyOpinionBtn.jpg' type='image' style='cursor:pointer;'/>";
        }
        
        $display .= "</form><p style='height;7px'></p>";
        
        */
	/*
        $display .="<form method='post' action='".$sitePrefix."/patient_new_exp_email_consultation.php'><input type='hidden' name='doc_id' value='".$dat['doc_id'] . "' />";
        
        if(!isset($_SESSION['username']))
        {
                $display .= "<a href='#top'><img src='".$sitePrefix."/images/stratConsultBtn.jpg' onClick='login_callpopup(true,\"".$dat['doc_id']."\");' style='cursor:pointer;' /> </a>";
        }
        else
        {
                $display .= "<input src='".$sitePrefix."/images/stratConsultBtn.jpg' type='image' style='cursor:pointer;'/>";
        }
        
        $display .= "</form>";
        */
        $display .="</td></tr>";

    }
    
}else{
    
    $display .="<tr><td colspan='3' align='center'><font color='red'>Doctor Not Found</font></td></tr>";
    
}

$display .="</table></div>";

echo $display;

ob_end_flush();

?>



