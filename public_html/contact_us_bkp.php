<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head><body>

<?php
	include 'header.php';
?>
<!-- header.......-->

<div id="subSlider">  
<h1>Contact Us</h1>
</div>
       <div id="content"> 


<table width="1000" border="0" cellspacing="10" cellpadding="0" class="s90cardstbl">
<tr><td width="254" valign="top"><img src="images/pW_Mascot.jpg" /></td>
<td width="712" valign="top" class="s90contactpage">



<table width="708" border="0" align="center" cellpadding="0" cellspacing="4">
<tr><td width="70" align="center"><img src="images/icon-contact-address.jpg" width="70" height="87"/></td>
<td width="630"><strong>Snail mail us at:</strong><br>
No. 275, 16th Cross, 2nd Block R.T.Nagar,<br>Bangalore 560 032, Karnataka, India.</td></tr>

<tr><td align="center"><img src="images/icon-contact-phone.jpg" width="70" height="48" style="margin:8px 0px"/></td>
<td><strong>Call us on:</strong><br>+91 80 43000333</td></tr>

<tr><td align="center"><img src="images/icon-contact-site.jpg" width="70" height="53" style="margin:8px 0px"/></td>
<td><strong>Look us up at:</strong><br><a href="index.php">www.pinkwhalehealthcare.com</a></td></tr>

<tr><td align="center"><img src="images/icon-contact-mail.jpg" width="70" height="74" style="margin:12px 0px"/></td>
<td><strong>E-Mail us at:</strong><br><a href="mailto:corpenquiry@pinkwhalehealthcare.com">corpenquiry@pinkwhalehealthcare.com</a> <span class="SmallFont">(if you are a corporate or a gated community who needs our services)</span><br>
<a href="mailto:info@pinkwhalehealthcare.com">info@pinkwhalehealthcare.com</a> <span class="SmallFont">(if you need any more information regarding our services)</span><br>
<a href="mailto:feedback@pinkwhalehealthcare.com">feedback@pinkwhalehealthcare.com</a> <span class="SmallFont">(if you wish to give us any feedback on our services)</span></td></tr>

<!--<tr><td colspan="2" align="left" height="89" background="images/icon-contact-footer-box.jpg">
<div style="margin:10px 10px 10px 80px">If you are a doctor interested in being on our panel of Specialists or building a career with us -<br> 
please visit our <a href="/doctor?task=ManageDoctors">Doctors</a> page.</div></td></tr>--></table>

</td></tr></table>


<!-- footer -->

<?php
include 'footer.php'; ?>
</body></html>
