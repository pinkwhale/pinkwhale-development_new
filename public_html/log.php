<?php


class log {

    function write($the_string) {

        $date = strftime("%Y_%m_%d",time());

        $filename = "logs/".$date."_log_file.txt";

        if ($fh = @fopen($filename, 'a+')) {
            fputs($fh, $the_string);
            fclose($fh);
            return( true );

        } else {
            return( false );
        }
    }

}

?>
