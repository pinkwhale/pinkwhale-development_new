<?php include "includes/start.php" ?>
<?php include("includes/site_config.php");?>
	<?php include "includes/header.php" ?>
	<link href="css/home-patients.css" media="all" rel="Stylesheet" type="text/css" />

	
	<div class="menu-data" data-main-page="patients"></div>
	<?php include "pw_Patients/menu-patients.php" ?>
	
	
	<div class="banner-bg banner-bg-patients">
		<div class="hidden-xs hidden-sm max-960">
			<div class="tag-line">We bring doctors to where you are</div><br><br>
			<div class="text">For customers who want easy<br>access to their doctors at all times</div><br>
			<a href ="pinkQuery.php" target="_self" class="button-learn">Learn More</a>
		</div>
	</div>
	
	<div class="page-tagline row-bg-1">
		<div class="max-960">
			<div class="tag">WE SIMPLIFY YOUR LIFE</div>
			<div class="description">We make Doctors accessible to you, anytime, anywhere.</div>
			<img src="img/home/patients/Patients_Graphic_PatientBenefits.png">
		</div>
	</div>

	<div class="grey-row">
		<div class="content-area">
			Say goodbye to traffic and waiting time.<br>Consult a doctor right from your living room!
		</div>
	</div>

	
	<div class="widgets-row row-bg-2">
		<div class="content-area">

			<div class="header max-960">CONNECT WITH A DOCTOR. IT'S EASY!</div>
			<div class="clearfix"></div>

			<div class="max-960">
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Login.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Login</div>
					<div class="widget-text">Login for easy access<br>to one of our doctors.</div>
				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Select.png" class="widget-image">
					<img src="img/home/patients/Patients_Icon_Line.png" class="lines">
					<div class="widget-name">Select</div>
					<div class="widget-text">Pick from our strong<br>database of trusted doctors</div>

				</div>
				<div class="widget-images col-xl-4 col-md-4 col-sm-4 col-xs-12">
					<img src="img/home/patients/Patients_Icon_Consult.png" class="widget-image">
					<div class="widget-name">Consult</div>
					<div class="widget-text">Connect with a doctor<br>online or by phone.</div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="our-bouquet-container">		
			<div class="content-area">				
				<div class="header">OUR BOUQUET OF SERVICES<br>FOR ENHANCED CARE</div>
				<div class="clearfix"></div>

				<div class="widgets-image-container max-960">
					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkQuery.php">
							<img src="img/home/patients/Patients_Icon_PinkQuery.png" class="gradient2-background">
							<div class="widget-name"><i>pink</i><strong>Query</strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Doctors are available <span class="hidden-sm"><br></span> now to answer your<span class="hidden-sm"><br></span> questions and give advice.</div>
						</a>
					</div>

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkAppoint.php">
							<img src="img/home/patients/Patients_Icon_pinkAppoint.png" class="gradient2-background">
							<div class="widget-name"><i>pink</i><strong>Appoint</strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our synced calendar<br>makes it simple to<br>schedule doctor visits.</div>
						</a>
					</div>

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkfollowup-online.php">
							<img src="img/home/patients/Patients_Icon_pinkFollowUp.png" class="gradient2-background">
							<div class="widget-name"><i>pink</i><strong>FollowUp</strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our Virtual Follow-ups<br>make connecting with<br>your doctor fast & easy.</div>
						</a>
					</div>

					<div class="widget-images-small col-xl-3 col-md-3 col-sm-3 col-xs-12">
						<a href="pinkOpinion.php">
							<img src="img/home/patients/Patients_Icon_pinkOpinion.png" class="gradient2-background">
							<div class="widget-name"><i>pink</i><strong>Opinion</strong></div>
							<div class="hr-container"><hr></div>
							<div class="widget-text">Our credible second<br>opinions help you make<br>the best health decisions.</div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		
	</div>
	
	<?php include "includes/home-footer.php" ?>
	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
	<script>
		$(document).ready(function(){
			$('.btn-shadow').click(function(){
				$(this).css('box-shadow','0 0 5px #000');
			});
		});
	</script>
<?php include "includes/end.php" ?>
