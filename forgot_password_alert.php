<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Forgot password</title>
		<link href="css/designstyles.css" rel="stylesheet" type="text/css">
		<link href="css/designstyles.css" rel="stylesheet" type="text/css">
	<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
	<body>
		<?php include 'header.php'; ?>
		<table height="100"></table>
		<table border="0" cellpadding="0" cellspacing="1" width="500" align="center" class="s90registerform1">
			<tr>
				<th>
					<?php
						if($_GET['response'] == "success") echo " Please check your email address. Your password has been sent to your email address";
						else if($_GET['response'] == "failure") echo "Cannot send password to your e-mail address. Try again.";
						else if($_GET['response'] == "not found") echo "Not found your email address in our database";
					 ?>
				</th>
			</tr>
		</table>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<?php include 'footer.php'; ?>
	</body>
</html>
