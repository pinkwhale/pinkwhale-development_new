<?php
session_start();
include "actions/encdec.php";
include "modify_doctorname.php";
include "site_config.php";
include "db_connect.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
<!-- header.......-->
<?php include 'main_header.php'; ?>
<!-- header.......-->
<script type="text/javascript"> document.getElementById('menu2').style.fontWeight= 'bold'</script>
<br /><br />      
<div class="container"><section>   
<div class="row" >
<div class="span12 clearfix" >
<div class="inner">

<div class="doc_pgcontainer"><section><div class="row" ><div class="clearfix">
    
     <?php
     
        $specialist = $_REQUEST['specialist'];
        
        if($specialist!=""){
            
            $specialist = str_replace("-"," ",$specialist);
            
            $specialist = mysql_escape_string($specialist);   
            
        }
         
        
        $doc_name = $_REQUEST['docname'];
        
        if($doc_name!=""){
            
            $doc_name_get_TMP = $doc_name;
        
            $doc_name_get = str_replace("-"," ",$doc_name_get_TMP);

            $doc_arr = explode(" ",$doc_name_get);

            $doc_arr_len = sizeof($doc_arr);

            $doc_name_get = mysql_escape_string($doc_name_get);
            
            $doc_name = "";
            
        }
        
        
        if($doc_name_get!=null){

            for($h=0 ; $h<$doc_arr_len ; $h++){

                $doc_name .= " and doc_name like '%".$doc_arr[$h]."%'";

            }

        }
        
        
        $city = $_REQUEST['city'];
        
        if($city!=""){
            
            $city = str_replace("-"," ",$city);
            
            $city = mysql_escape_string($city);   
            
        }        
        
        
        if($specialist!="" && $doc_name!="" && $city!=""){        
     
            $qry = "select doc_id,doc_name,doc_gender,doc_photo,doc_category,doc_specialities,more_expertise,doc_qualification,activate_tele_Consultation,activate_online_consultation,activate_online_query,about_doctor from pw_doctors where city='$city' $doc_name and doc_specialities='$specialist' and blocked<>'Y'";
            $result = mysql_query($qry);
            $num = mysql_num_rows($result);
            if($num>0){
                $data = mysql_fetch_array($result);
                $doc_name = $data['doc_name'];
                $doc_id = $data['doc_id'];
                $doc_spe = $data['doc_specialities'];
		$doc_category = $data['doc_category'];
                $doc_more_expertise = $data['more_expertise'];
                $doc_more_expertise_tmp = explode(";", $doc_more_expertise);
                $doc_qualification = $data['doc_qualification'];
                $tele = $data['activate_tele_Consultation'];
                $online =  $data['activate_online_consultation'];
                $doc_query = $data['activate_online_query'];
                $about_doc = $data['about_doctor'];
                $doc_image =  $data['doc_photo'];
                $doc_gender = $data['doc_gender'];

                if($doc_image=="" && $doc_gender=="Female"){
                    $doc_image = $sitePrefix."/images/doctors_images/female.png";
                }else if($doc_image=="" && $doc_gender=="Male"){
                    $doc_image = $sitePrefix."/images/doctors_images/male.png";
                }else if($doc_image=="" ){
                    $doc_image = $sitePrefix."/images/doctors_images/male.png";
                }else{
                    $doc_image = $sitePrefix."/".$doc_image;
                }
                
                 ?>
                <div class="span8"><div class="docbox">
                        <div class="docbox_left"><img src="<?php echo $doc_image; ?>"></div>
                <div class="docbox_right"><h1><?php echo $doc_name; ?></h1>
                <h5><?php echo $doc_qualification; ?></h5>
                    <br><br>
                            <div class="clearfix">
                                <?php
                                if(!isset($_SESSION['username']) || $_SESSION['login']!='user'){
                                    if($doc_query=="Query" && $doc_spe=="Physician"){
                                        echo "<a href='#' onclick='validate_consult(1)' class='btn  btn-small' rel='popover-left' title='Ask our Physician' data-content='* Have a general Medical query?<br />* Need clarification on the query?<br />* Need advice on what to do next?'>Ask our Physician</a>";
                                    }else if($doc_query=="Query" && $doc_spe!="Physician"){
                                        echo "<a href='#' onclick='validate_consult(2)' class='btn  btn-small' rel='popover-left' title='Ask our Specialist' data-content='* Have a specific Medical query?<br />* Know which specialist can help?<br />* Want clarification before meeting a specialist?'>Ask our Specialist</a>";
                                    }
                                    
                                    if($online=="online" && ($doc_category=="Specialist" || $doc_category=="Specialist/Expert" )){
                                        echo "<a href='#' onclick='validate_consult(3)' class='btn  btn-small' rel='popover-top' title='Consult Your Specialist' data-content='* Bought a Card from your Specialist?<br />* Want to do an Online Consult?'>Consult Your Specialist</a>";
                                    }
                                    
                                    if($doc_category=="Specialist/Expert" || $doc_category=="Specialist/Expert"){
                                        echo "<a href='#' onclick='validate_consult(4)' class='btn  btn-small' rel='popover-right' title='Get a Second opinion' data-content='* Have met a Specialist already?<br />* Have got advice on treatment?<br />* Want to take another Opinion?'>Get a Second Opinion</a>";
                                    }
                                }else{
                                    if($doc_query=="Query" && $doc_spe=="Physician"){
                                        echo "<a href='$sitePrefix/patient_new_qry_email_consultation.php?doc_id=0' class='btn  btn-small' rel='popover-left' title='Ask our Physician' data-content='* Have a general Medical query?<br />* Need clarification on the query?<br />* Need advice on what to do next?'>Ask our Physician</a>";
                                    }else if($doc_query=="Query" && $doc_spe!="Physician"){
                                        echo "<a href='$sitePrefix/patient_new_qry_email_consultation.php?doc_id=1' class='btn  btn-small' rel='popover-left' title='Ask our Specialist' data-content='* Have a specific Medical query?<br />* Know which specialist can help?<br />* Want clarification before meeting a specialist?'>Ask our Specialist</a>";
                                    }
                                    
                                    if($online=="online" && ($doc_category=="Specialist" || $doc_category=="Specialist/Expert" )){
                                        echo "<a href='$sitePrefix/patient_new_email_consultation.php' class='btn  btn-small' rel='popover-top' title='Consult Your Specialist' data-content='* Bought a Card from your Specialist?<br />* Want to do an Online Consult?'>Consult Your Specialist</a>";
                                    }
                                    
                                    if($doc_category=="Specialist/Expert" || $doc_category=="Specialist/Expert"){
                                        echo "<a href='$sitePrefix/patient_new_exp_email_consultation.php' class='btn  btn-small' rel='popover-right' title='Get a Second opinion' data-content='* Have met a Specialist already?<br />* Have got advice on treatment?<br />* Want to take another Opinion?'>Get a Second Opinion</a>";
                                    }
                                }
                                ?>
                                
                            </div>
                    <br>
                </div><br clear="all">
                </div>
                <h2>More Details About <?php echo $doc_name; ?></h2>
                <?php
                    echo $about_doc;
                ?>
                </div><!-- /span8 -->
                    <?php
                    if($doc_more_expertise!=""){
                    ?>
                    <div class="span4 docpgleft"><br>
                    <h3>Specialization</h3>
                    <ul class="liststar">
                        <?php
                            foreach($doc_more_expertise_tmp as $exp){
                                echo "<li>$exp</li>";
                            }
                        ?>
                    </ul>

                <?php
                    }
            }else{
                header("location: $sitePrefix/index.php");
            }
        }else{
            header("location: $sitePrefix/index.php");
        }
    ?>
<div class="heading_border"></div>
                    
	</div><!-- /span4 -->
    
</div></div><!-- /row--></section></div> <!-- /container -->  

</div><!-- /inner -->
</div><!-- /span12 -->			
</div><!-- /row-->
</section></div> <!-- /container --> 

<!-- footer -->
<script>

var site_config = "<?php echo $sitePrefix."/"; ?>";

function consult_LoginSignUp(form)
{
	usernameValidated = true;
	passwordValidated = true;
        document.getElementById("log_errordiv").innerHTML = "";        
	document.getElementById("username_errordiv").innerHTML = "";
	document.getElementById("password_errordiv").innerHTML = "";
	
	if (form.username.value=='')
	{
            
   		document.getElementById("username_errordiv").innerHTML = "UserName cannot be blank";
                usernameValidated = false;
	}
	if (form.password.value=='')
	{
            
		document.getElementById("password_errordiv").innerHTML = "Password cannot be blank";
		passwordValidated = false;
	}
	
	if(usernameValidated && passwordValidated)
	{   
            
                           
                // -------------------ajax start--------------------- //
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                
                var username = encodeURI(form.username.value);
                var password = encodeURI(form.password.value);
                
                var paramString = 'username='+username+'&password='+password;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"login_popup.php",
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    loading("Loading",1);
                                    document.getElementById('consult_signin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("log_errordiv").innerHTML = "Invalid login Details";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;                                                                     
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{
                
		return false;
	}
	
}


function forgot_password(form)
{       
	Validated = true;
        
        document.getElementById("forgotemailErrDiv").innerHTML = ""; 
        
	if (form.forgot_email.value=='')
	{
            
   		document.getElementById("forgotemailErrDiv").innerHTML = "Email cannot be blank";
                Validated = false;
	}
        
	if(Validated)
	{   
            
            loading("Checking...",1);
            
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("for_send").value="Validating....";
            
                // -------------------ajax start--------------------- //
                var paramString = 'email='+form.forgot_email.value;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"actions/forgot_Passwd.php",  
                        data: paramString,  
                        success: function(response) {                                                      
                                
                                if(response==true){
                                    unloading();
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully sent password to your Email-id</div>";
                                    
                                    
                                }else{       
                                    unloading();
                                    document.getElementById("forgotemailErrDiv").innerHTML = "Invalid Email-ID";
                                    document.getElementById("for_send").value="Submit";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}



function register_pink(form)
{
        
	Validated = true;
        
        document.getElementById("nameErrDiv").innerHTML = "";
        document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
        document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("passwordErrDiv").innerHTML = ""; 
        document.getElementById("repasswordErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = ""; 
        
        
        var name = form.regname.value;
        var age = form.regage.value;
        var gender = form.reggender.value;
        var card = form.regcard.value;
        var email = form.regemail.value;
        var password = form.regPassword.value;
        var confirmpassword = form.regConfirmPassword.value;
        var mobile = form.regPhone1.value;
        
        
        //alert(form.forgot_email.value);
	if ( name =='' )
	{
            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
        if ( age =='' )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
                Validated = false;
	}else if ( age < 18 )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
                Validated = false;
	}
        
        if ( gender =='' )
	{
            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
                Validated = false;
	}
        if ( email =='' )
	{
            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
                Validated = false;
	}
        
        if ( password =='' )
	{
            
   		document.getElementById("passwordErrDiv").innerHTML = "Password cannot be blank";
                Validated = false;
	}
        
        if ( password != confirmpassword )
	{
            
   		document.getElementById("repasswordErrDiv").innerHTML = "Password Mismatch";
                Validated = false;
	}
        
        if( mobile == "" )
	{
            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
                Validated = false;
	}
        
        
	if(Validated)
	{   
            
            loading("Registering...",1);
                
                document.getElementById("for_send").disabled=true;
                document.getElementById("btnSubmit").disabled=true;
                document.getElementById("login_submit").disabled=true;
                document.getElementById("btnSubmit").value = "Processing....";
            
                // -------------------ajax start--------------------- //
                
                name = encodeURI(name);
                age = encodeURI(age);
                gender = encodeURI(gender);
                card = encodeURI(card);
                email = encodeURI(email);
                password = encodeURI(password);
                mobile = encodeURI(mobile);
                
                var paramString = 'name='+name+'&age='+age+'&gender='+gender+'&email='+email+'&password='+password+'&mobile='+mobile+'regcard='+card;
                
                $.ajax({  
                        type: "POST",  
                        url: site_config+"actions/regpink.php",  
                        data: paramString,  
                        success: function(response) {  
                            //alert(response);
                                if(response==true){
                                  
                                    unloading();
                                  
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                    
                                    document.getElementById("login_body").style.display="none";
                                    document.getElementById("login_message").style.display="block";
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("login_message").innerHTML="<div align='center'>Successfully Register<br />Please check your mail to complete registration.</div>";
                                    
                                    
                                    
                                }else{       
                                    
                                    unloading();
                                    
                                    document.getElementById("emailErrDiv").innerHTML=response;
                                    document.getElementById("btnSubmit").value = "Register";
                                    document.getElementById("for_send").disabled=false;
                                    document.getElementById("btnSubmit").disabled=false;
                                    document.getElementById("login_submit").disabled=false;
                                 
                                    
                                    
                                }
                        }

                }); 
                
                
                return false;
                // -------------------ajax End--------------------- //
                
	}
		
	else
	{                
		return false;
	}
	
}

function card_check(card){
    
        // -------------------ajax start--------------------- //

        if(card!=""){
            
            card = encodeURI(card);

            document.getElementById("cardErrDiv").innerHTML= "Checking Card.....";

            var paramString = 'card='+card;

            $.ajax({  
                    type: "POST",  
                    url: site_config+"check_pink_card.php",  
                    data: paramString,  
                    success: function(response) {
                        //alert(response);
                            if(response==true){
                                document.getElementById("cardErrDiv").innerHTML= "";
                                return false;
                            }else{
                                document.getElementById("regcard").value= "";
                                document.getElementById("cardErrDiv").innerHTML= "Invalid card";
                                return false;
                            }
                    }

            }); 

        }
        // -------------------ajax start--------------------- //
        
}

function Topup_login(form)
{
        
	validate = true;
        
	
	document.getElementById("topuplog_errordiv").innerHTML= "";
	
        
                
	if (form.card.value=='')
	{
                
   		document.getElementById("topuplog_errordiv").innerHTML = "Card No. cannot be blank";
                validate = false;
	}
        
	
	if(validate)
	{
            document.getElementById("topupbtn").value="loading";
            document.getElementById("topupbtn").disabled=true;
            
            // -------------------ajax start--------------------- //
                
                var paramString = 'card='+form.card.value;

                $.ajax({  
                        type: "POST",  
                        url: site_config+"card_login_check.php",  
                        data: paramString,  
                        success: function(response) {         
                                if(response==true){
                                    
                                    document.getElementById('TopUPlogin').submit();
                                    
                                }else{
                                    
                                    document.getElementById("topuplog_errordiv").innerHTML = "Invalid card No.";
                                    document.getElementById("topupbtn").value="Top Up";
                                    document.getElementById("topupbtn").disabled=false; 
                                    return false;
                                }
                        }

                }); 
                
                return false;
                // -------------------ajax End--------------------- //
		
                
	}
		
	else
	{
		return false;
	}
	
}




function isNumberKey(evt)
{
	
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}


function validate_consult(type){
    
    document.getElementById("login_nxt_page").value=type;
    document.getElementById("register_nxt_page").value=type;
        
    setTimeout(login_callpopup(true,0),200);
    
}
</script>
<?php
include 'footer.php'; ?>

</body></html>

