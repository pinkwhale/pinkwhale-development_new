<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>

	

	<?php include "includes/page-menu-start.php" ?>
		<li class="contents"><a href="" class="menu-elements">meet our doctors</a></li>
		<li class="contents"><a href="" class="menu-elements">our services</a></li>
		<li class="contents"><a href="" class="menu-elements">happy patients</a></li>
		<li class="contents"><a href="" class="menu-elements">register</a></li>
		<li class="contents"><a href="" class="menu-elements">faq's</a></li>
		<li class="contents"><a href="" class="login-button">Login</a></li>
	<?php include "includes/page-menu-end.php" ?>
	
	<img src="img/Subscribe1_Banner.png" class="page-banner">
	
	<div class="page-tagline">HEAR WHAT SOME OF OUR HAPPY PATIENTS<br>HAVE TO SAY ABOUT <i>pink</i>WHALE</div>

	<div class="form-block col-md-12">
		<div class="content-area form-contents">
			<h1 class="form-title">STAY IN THE LOOP WITH<br>OUR HEALTH TIPS AND UPDATES</h1>
			<div class="col-xl-8 col-md-8 col-sm-12 col-xs-12">
				<div class="form-header">
					<img src="img/Subscribe1_icon_Apply.png">
					<span class="form-tagline">Hear more from Dr. Vanitha Here!</span>
				</div>
				<div class="form-elements">
					<div class="input-label col-xl-4 col-md-4 col-sm-6">First Name:*</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Last Name:*</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Email:</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6">Phone Number:</div><div class="input col-xl-8 col-md-8 col-sm-6"><input type="text" class="input-fields"></div><div class="clearfix"></div>
					<div class="input-label col-xl-4 col-md-4 col-sm-6"></div><div class="input col-xl-8 col-md-8 col-sm-6"><button type="submit" class="submit">Submit</button></div>
				</div>
			</div>
			<div class="message col-xl-4 col-md-4 col-sm-12 col-xs-12">

				Like the idea of pinkWhale but aren’t <br>
				sure of Enrolling yet?<br>
				Not a problem! <br><br>
				Drop in your Email ID and we’ll send you <br>
				a newsletter with Updates & Health Tips <br>
				to keep you in the Pink of Health all year 
				round.
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
