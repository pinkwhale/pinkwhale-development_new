<?php //include "includes/start.php" 
//var_dump($_GET);exit;
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="By scheduling an appointment with doctor, you can easily consult a doctor directly without 
	waiting in a queue.">
	<meta name="keywords" content="Schedule an appointment, direct consultation">
    <title>Schedule an Appointment | pinkWhale Healthcare</title>
    <?php include 'includes/include-css.php'; ?>
	 <link href="css/designstyles.css?sd" rel="stylesheet" type="text/css">

    <style type="text/css">
        #book_procceed table tr:nth-child(odd){  }
        #book_procceed table tr:nth-child(even){ background-color: #F2F2F2; }
        #book_procceed table tr:nth-child(even) td{box-shadow: 0px 15px 10px -10px rgba(0, 0, 0, 0.15) inset;}
        #book_procceed table tr table.attable td{border: none;}
        #book_procceed #DocSearch_1 table tr td a{color: #6d6e71;}
        #book_procceed table tr td:first-child{padding-left: 4%;}
        #book_procceed table tr th:first-child{padding-left: 4%;}
        #book_procceed table.attable td{padding: 15px 5px;}
        #book_procceed table.attable td{border: 0px;} 
        #book_procceed table tr .docDis span{color: #C36262;}
        #book_procceed #DocSearch_1{width: auto;}
    </style>


	 <script type="text/javascript" src="js/jquery.min.js"></script>
                        <script src="pink_files/ga.js" async="" type="text/javascript"></script>
						<script src="pink_files/AC_RunActiveContent.js" type="text/javascript"></script>
                        <!-- ------------------------------   google analytics    ------------------------------------------- -->
                        <script type="text/javascript">

                            var _gaq = _gaq || [];
                            _gaq.push(['_setAccount', 'UA-23649814-1']);
                            _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
                            _gaq.push(['_trackPageview']);

                            (function() {
                                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                            })();
  
                        </script>
                        <script type="text/javascript" src="Scripts/alternatetable.js"></script>
                        <style>

                            #loading { 
                                width: 100%; 
                                position: absolute;
                            }
                        </style>
                        <script type="text/javascript" src="js/book_appointment.js"></script>
                        <script type="text/javascript">
                            
                            
                            
                            
                            function show_hidden(val){
								alert("hiii");
                                
                                    $( ".show_time_"+val ).show().fadeIn();
                                    $( ".hide_time_"+val ).hide();
                                                                    
                            }
                            
                        </script>
						<script language="javascript">var sitePrefix = 'http://www.pinkwhalehealthcare.com';</script><link href="pink_files/front.css" media="screen, projection" rel="stylesheet" type="text/css">
  </head>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<?php 
	 error_reporting(E_PARSE);
     session_start();
	?>
	<link href="css/findadoctor.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="js/login.js"></script>
	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-appoint"></div>
	<?php include "menu-patients.php" ?>
	<?php 
		if(isset($_GET['doc_name'])||($_GET['doc_spe']) ){
			
		?>
			<input type="hidden" name="doc_name" id="doc_name" value="<?php echo $_GET['doc_name'];?>"/>
			<input type="hidden" name="specialist" id="specialist" value="<?php echo $_GET['doc_spe'];?>"/>
			
			<script>
				$(document).ready(function(){
					//alert("sdgd");
					document.getElementById("message").innerHTML="";					
					var doc_name = encodeURI(document.getElementById("doc_name").value);
					var doc_spe = encodeURI(document.getElementById("specialist").value);
			
					var zip = encodeURI(document.getElementById("zip").value);
					
					$("#loading").fadeIn(900,0);
					$("#loading").html("<img src='images/ajax-loader.gif' />");

					$('#book_procceed').load('get-time-for-booking.php?doctor_name='+doc_name+'&doc_spe='+doc_spe+'&zip='+zip,Hide_Load());
					$("#divblock").hide();
					
					$('#sub').click(function(){
					document.getElementById("message").innerHTML="";					
					var doc_name = encodeURI(document.getElementById("doct_name").value);
					var doc_spe = encodeURI(document.getElementById("speciality").value);
			
					var zip = encodeURI(document.getElementById("zip").value);
					
					$("#loading").fadeIn(900,0);
					$("#loading").html("<img src='images/ajax-loader.gif' />");

					$('#book_procceed').load('get-time-for-booking.php?doctor_name='+doc_name+'&doc_spe='+doc_spe+'&zip='+zip,Hide_Load());
					$("#divblock").hide();
	});
	
				// -------------------ajax start--------------------- //
	/*
        var paramString = 'doctor_name='+doc_name+'&doc_spe='+doc_spe+'&zip='+zip;


        $.ajax({  
                type: "POST",  
                url: "get-time-for-booking.php",  
                data: paramString,  
                success: function(response) {                                                      
                        document.getElementById("book_procceed").innerHTML=response;                        
                        Hide_Load();
						//var temp = response.html("No Doctor's Available");
						if(response != ""){
							$("#divblock").hide();
						}
                }

        }); */
	
     // -------------------ajax End--------------------- //
				
				});
			</script>
	<?php 		
		}
	?>
		<div class="banner-bg banner-bg-pinkappoint">
		<div class="content-area hidden-xs">
			<div class="banner-form">
				<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
				<div class="form-horizontal pw-form"  style="max-width: 400px;">
				<div class="form-group">
						<label class="col-xs-5 control-label">Doctor's Name:</label>
						<div class="col-xs-7">
							 <input name="doct_name" type="text" id="doct_name"  size="25" />
						</div>
					</div>  
					<div class="form-group">
						<label class="col-xs-5 control-label">Choose a Specialty:</label>
						<div class="col-xs-7">
							<select name="speciality" id="speciality" class="form-control">
							<option value="" selected="selected" > -- Specialist / Expert -- </option>
							<?php // code added by Shoaib //
								$qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE blocked !='Y' and doc_specialities<>'' and (appoint_flag='1' or appoint_not_signed='1') group by`doc_specialities`";
								$res = mysql_query($qry);
								while ($data = mysql_fetch_array($res)){
								echo "<option value='".$data['doc_specialities']."'>".$data['doc_specialities']."</option>"; 
								}
							?>					
						</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-5 control-label">Zip:</label>
						<div class="col-xs-7">
							 <input name="zip"   type="text" id="zip" size="25"/></div>
						</div>
					</div>  
					<div class="form-group">
						<div class="col-xs-offset-5 col-sm-7" style="margin-left:164px;">
							<button type="button"  value="Find Doctor" id="sub" class="btn pw-btn" >Find a Doctor</button>
							 <!--input name="input" type="button"  value="Find Doctor"  onclick="get_time_for_doc()"/-->
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>


	<?php include "includes/menu-pink-buttons.php" ?>


	<!--div class="banner-form visible-xs mobile-form">
		<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
		<form class="form-horizontal" action="findadoctor.php" role="form">
			<div class="form-group pw-form">
				<label class="col-xs-5 control-label">Choose a Specialty:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Pediatrician</option>
						<option>Pediatrician</option>					
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-5 control-label">Choose a City:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Bangalore</option>
						<option>Mysore</option>
						<option>Ahmedabad</option>
						<option>Delhi</option>
						<option>Jammu</option>
					</select>
				</div>
			</div>  
			<div class="form-group">
				<div class="col-xs-offset-5 col-sm-7">
					<button type="submit" class="btn pw-btn">Find a Doctor</button>
				</div>
			</div>
		</form>
	</div-->
	
	
	<div id="subSlider"> 

<!--h1>PinkAppoint</h1-->

</div>

<div id="content">
                                <div id="book-details">
                                    
                                    <!--<form action="appointment_doctor_details.php" method="POST" > -->
                                    <!--form action=<?= $_SERVER['php_self'] ?> method="POST" >
                                        <table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr><td colspan="2"><div id="appointment_banner">
                                        <div id="banner_titles"><h3>Find a Doctor</h3><h4>& Book Your Appointment</h4></div><br clear="all" />
                                        <div id="banner_fields"> <div class="txt"> Doctors Name:</div> <div class="inpt"> <input name="doc_name" type="text" id="doc_name"  size="25" /></div> <br><br/>
                                        <div  class="txt">Specialty: </div> <div class="inpt">
                                          <select name="specialist" id="specialist" style="width:200px" >
                                            <option value=""> -- Specialist / Expert -- </option>
                                            <?php
                                                        $qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE blocked !='Y' and doc_specialities<>''  group by`doc_specialities`";
                                                        if (!$qry_rslt = mysql_query($qry))
                                                            die(mysql_error());
                                                        while ($qry_result = mysql_fetch_array($qry_rslt)) {
                                                            echo "<option value=\"$qry_result[0]\">$qry_result[0]</option>";
                                                        }
                                                        ?>
                                          </select>
                                        </div> <br><br/>
                                        <div class="txt">Zip: </div> <div class="inpt"> <input name="zip"   type="text" id="zip" size="25"/></div>
                                        <div class="txt"> </div> <div class="inpt"> <input name="input" type="button"  value="Find Doctor"  onclick="get_time_for_doc()"/></div>
                                        
                                        </div>
                                        </div></td></tr>

                                        </table>
                                </form-->

                                	<div id="valid"  style="margin:0px; clear:both; padding:0px;display:block">
                                        <?php

                                            echo "<div id='message' class=\"messageAlert\" align=center>" . $_SESSION['msg'] . "</div><br />";
                                            $_SESSION['msg'] = "";
                                            echo "<div id='message' class\".message_error_Alert\" align=center>" . $_SESSION['error'] . "</div><br />";
                                            $_SESSION['error'] = "";


                                        ?>
                                    </div>    
                                  
                                    
                                    <div id="loading" align="center" ></div>
					<div id="book_procceed" style="margin:0px auto; clear:both; padding:0px;">
                                    <table border="0" cellpadding="0" cellspacing="0"  align="center">
                                        <tr><td style="margin-top:5px">

                                                

                                                  
                                               
                                            </td>
                                        </tr>
                                        <tr>
<td align="right" style="padding-top:5px">
<?php 
// if(!isset($_SESSION['login']))
//{
?>
    <!-- <a href="join.php"><img src="images/signUpNow.gif"  /></a>-->
        <!--a href="#"><img src="images/Cancel_App.gif" onclick="cancel_popup(true)"  /></a-->
<center>

<?php //} ?>
</td>
</tr>
         </table>
                                     </div>
					 <div id="valid"  style="margin:0px; clear:both; padding:0px;">
                                        <?php

                                            echo "<div id='message' class=\"messageAlert\" align=center>" . $_SESSION['msg'] . "</div><br />";
                                            $_SESSION['msg'] = "";
                                            echo "<div id='message' class\".message_error_Alert\" align=center>" . $_SESSION['error'] . "</div><br />";
                                            $_SESSION['error'] = "";


                                        ?>
                                    </div>



                                </div>
    <?php //include"footer.php"; ?>
                                <div id="cancel_popupId" style="display:none; z-index: 10008; left:22%; height:1000px;  width:550px; height: auto; position: absolute; 
                                    top:20%; font-family:Arial; font-size:13px;  border:4px solid #666; background:url('images/bg-btn-blue.png') repeat-x ; background-color:#fff;" > 

                            <div style="float:right ; padding:5px;"> <a href="#" onclick="cancel_popup(false)"><img src="images/close.gif" border="0" /></a> </div>
                            
                            
                             <div style="padding-top:4px; color:#fff; font-size:18px; font-weight:bold; "><center> Cancel Appointment </center></div> <br/>

                            <div id="CancelAppoint_detail">

                                    <table align="center" height="50" cellpadding="1">
                                        <tr>
                                            <td>Token-id </td>
                                                <td>:</td>
                                                <td>
                                                <input type="token_id" id="token_id" size="6" maxlength="4" onkeypress="return isNumberKey(event);" />
                                            </td>
                                        </tr>
                                        <!--    ERROR DIV -->
                                        <tr>                                        
                                        <td colspan="3" align="center" height="8">
                                                        <div id="tokenErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                            </td>
                                        </tr>
                                        <!--  END ERROR DIV --> 
                                        <tr>
                                            <td>
                                                Mobile No. </td><td>:</td>
                                            <td>
                                                <input type="mob_no" id="mob_no" size="15" maxlength="12" onkeypress="return isNumberKey(event);" />
                                            </td>
                                        </tr>
                                        <!--    ERROR DIV -->
                                        <tr>                                      
                                        <td colspan="3" align="center" height="8">
                                                        <div id="mobErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                            </td>
                                        </tr>
                                        <!--  END ERROR DIV --> 
                                        <tr>                                            
                                            <td colspan="2"></td>
                                             <td>
                                                <input type="button" onclick="cancel_app()" value="Submit" />
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                            <div id="CancelAppoint" align="center">

                            </div>
                            </div>

                            <div id="cancel_divDisable" style="DISPLAY: none; Z-INDEX: 999; FILTER: alpha(opacity=48); LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000; opacity: .48; moz-opacity:.48"> </div>
                           

			
  
                        

<div id="divblock">
	
	
	<div class="page-tagline pink-page row-bg-1">
		<img src="img/ourservices/pinkappoint/pinkAppoint_Icon_PinkAppointment1.png"><i>pink</i><strong>Appoint</strong>
		<div class="text">
			<i>
				Forgot to book an appointment during clinic hours?<br>
				Book an appointment here!<br><br>
				We're not trying to replace your face-to-face visits. <br>
				pinkAppoint allows you to book an appointment within a few clicks! <br><br>
			</i>
			<br>
			<strong>
				One in-clinic visit makes you eligible for virtual follow ups. We even make <span class="hidden-xs"><br></span>
				scheduling that visit simple. Our pinkAppoint Service syncs with the doctor's <span class="hidden-xs"><br></span>
				calendar so you can make an appointment when it's convenient for you. <br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="how-it-works">
		<div class="content-area">
			<img src="img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png">
			<div class="title">How does it work?</div>
		</div>
	</div>

	<div class="how_it_works">
		<img src="img/ourservices/pinkappoint/pinkAppoint.png" style="max-width: 100%;">
	</div>

	<!-- <div class="work-bg work-pink-appoint"></div> -->

	<div class="row-before-video row-bg-2">
		SEE HOW <i>pink</i>Appoint<br>CAN HELP YOU WHEN YOU NEED IT MOST

	</div>

	<div class="work-bg video-bg video-image">
		<iframe width="100%" src="//www.youtube.com/embed/sV6ff8UbjHk?list=PLu4bXNfdFdSxW0UGWQwSMkNBCOAorfB1E" frameborder="0" allowfullscreen></iframe>		
	</div>

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Register with pinkWhale Today!</span>
			</div>
				<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form" name="register" id="register">
						<div class="form-group">
							<label for="fname" class="col-xs-5 control-label">Your Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="name" name="name" >
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
                            <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV --> 
						<!--div class="form-group">
							<label for="lname" class="col-xs-5 control-label">Last Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="lname" name="lname" >
							</div>
						</div-->	
						<!--    ERROR DIV -->						
						<!--div  align="right" style="margin-right:70px;">
						<div id="lname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div-->
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="age" class="col-xs-5 control-label">Age:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="age" name="age" onkeypress="return isNumberKey(event);" value="">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
                            <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="gender" class="col-xs-5 control-label">Gender:*</label>
							<div class="col-sm-7">
								<!--input type="textbox" class="form-control" id="gender" name="gender"-->
								<select name="gender" id="gender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
                            <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="cardno" class="col-xs-5 control-label">Card Number:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="regcard" name="regcard" value=""  autocomplete="off" onblur="return card_check(this.value)">
							</div>
						</div>		
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						  <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						    <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="password" class="col-xs-5 control-label">Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="password" name="password" value="" onkeyup="valid_pswd(form)" autocomplete="off">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						    <div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="cpassword" class="col-xs-5 control-label">Confirm Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="cpassword" name="cpassword" value="" onkeyup="valid_pswd(form)" autocomplete="off">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						    <div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Mobile:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" name="mobile"  value="" onkeypress="return isNumberKey(event);" autocomplete="off">
							</div>
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						    <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return register_pink(reg_form)"/>
							</div>
						</div>												
					</form>
				</div>				
			</div>	
			<div class="message form-right col-sm-4 hidden-xs">

				No need to wait on the phone to <br>
				schedule an appointment with <br>
				your doctor.<br><br>
				Schedule an in-clinic visit here to <br>
				become eligible for our other <br>
				virtual health services.<br>
			</div>
		</div>
	</div>
</div>
<div style="text-align: center;">
    <span class="pw-btn" onclick="cancel_popup(true)">Cancel Appointment</span>
</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
