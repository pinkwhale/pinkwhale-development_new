<?php
session_start();

if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
{
	header("Location: ../index.php");
	exit();
}

include ("../db_connect.php");

$clinic_id=urldecode($_GET['clinic']);

$doctor=urldecode($_GET['doctor']);

$doctor_details = explode("|", $doctor);
$doc_id = $doctor_details[0];
$doc_name = $doctor_details[1];

$clinic_details = explode("|", $clinic_id);
$clinic_id = $clinic_details[0];
$clinic_name = $clinic_details[1];

$qry_morning_slot = "select * from doctor_clinic_avail_time_slots where clinic_id='$clinic_id' AND doc_id='$doc_id' and day_type IN ('morning','afternoon','beforenoon','evening') group by day_type";

$slot_result = array();
$res = mysql_query($qry_morning_slot);
$num=mysql_num_rows($res);

while ($data = mysql_fetch_array($res)){
  $slot_result[$data['day_type']]=$data;
}

$qry_days = "select Day from doctor_time_slots where clinic_id='$clinic_id' AND doc_id='$doc_id' group by Day";
$qry_days_res = mysql_query($qry_days);
$selected_days = array();
while ($row = mysql_fetch_array($qry_days_res, MYSQL_BOTH)) {
	array_push($selected_days,  $row[0]);
}
$slot_result['day']=$selected_days;

echo json_encode($slot_result);
?>
