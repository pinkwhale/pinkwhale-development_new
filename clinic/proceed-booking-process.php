<?php 
	error_reporting(E_PARSE); 
	date_default_timezone_set('Asia/Calcutta');
	session_start();
	include ("../db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
		$clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/book_appointment.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />

<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
    
<?php include 'header.php'; 
require_once('calendar/tc_calendar.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="180" valign="top">
<!-- <div id="s90dashboardbg" style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div >
<img src="../images/phr_dashboard_uline.jpg" width="205" height="20" /> -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr" >
    
        <table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
        <tr>


<?php
ob_start();
date_default_timezone_set('Asia/Calcutta');
session_start();

$_SESSION['msg'] = "";

$_SESSION['error'] = "";

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='clinic')
{
        header("Location: ../index.php");
        exit();
}

include ("../db_connect.php");

$doc_id = urldecode($_POST['doctor_id']);

$docname = get_getdoc_name($doc_id);

$date = urldecode($_POST['date']);

$slot12 =""; 
//urldecode($_GET['slot12']);

$slot24 = "";
//urldecode($_GET['slot24']);
$i=0;
$parts3 ="";
//echo $data_arr=urldecode($_POST['avail']);
foreach($_POST['avail'] as $item)
{
$parts= explode('/', $item);
$parts1[$i]=$parts[0];
$slot24 .=$parts1[$i].",";
$parts2[$i]=$parts[1];
$slot12 .=$parts2[$i].",";
$i++;
$parts3.=$parts[2].",";
}
$parts1[$i-1];

//echo $parts3;

$time_slot_param = trim($parts3, ",");
//echo $time_slot_param;
$slot24=substr($slot24, 0, -1);
//$slot24=substr($slot24,1,strlen($slot24)-1);
$time=$slot24;
	
	$d_day = date("D",strtotime($date));

$week = get_week($d_day);

$clinic_id=$_SESSION['clinic_id']; 
/* to get start time and end of the end time */
/* $qry2=mysql_query("SELECT from_time FROM `appointment_book_details` where `from_time` in($time_slot_param) order by `from_time` ASC");
while($time_array= mysql_fetch_array($qry2))
{
} */
/* end to get start time and end of the end time */
$qry = "select status from Appointment_book_details where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and  admin_id<>'$clinic_id'";

$result = mysql_query($qry);
$data = mysql_fetch_array($result);
$i=1;
if($data[0]=="" ){
	
	$slot241= explode(',', $slot24);
   foreach($slot241 as $slot24)
   {
    $query1 = "select admin_id from Appointment_book_details where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and  admin_id='$clinic_id'";
    $result1= mysql_query($query1);
    $num_1 = mysql_num_rows($result1);
 
    if($num_1==0){
        $query = "insert into Appointment_book_details(admin_id,doc_id,clinic_id,Day,from_time,status,booktime) values('$clinic_id','$doc_id','$clinic_id','$week','$date $slot24','1',now())";    
        mysql_query($query) or die ("error while engaging appointment");    
    }else{
        $query = "update Appointment_book_details set booktime=now() where doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 and admin_id='$clinic_id'";    
        mysql_query($query) or die ("error while engaging appointment");    
    }
    $qry1 = "select id from Appointment_book_details where  doc_id='$doc_id' and clinic_id ='$clinic_id' and from_time ='$date $slot24' and status=1 ";
    $res1 = mysql_query($qry1);
    $da1 = mysql_fetch_array($res1);
    $i++;
   }

    $proceed = "<div id=\"alert\"></div><form name='book_doc_app' id='book_doc_app'  method='POST'><table border='0' cellpadding='0' cellspacing='1' width='700' align='center' class='s90registerform'>";

    $proceed .="<tr><th colspan=\"2\">Book Appointment</th></tr>";

    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Doctor Name &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' readonly size='25'  name='doc_id' id='doc_id' value='$doc_id'/>$docname";
    
    $proceed .="<input type='hidden' readonly size='25'  name='app_id' id='app_id' value='".$da1['id']."'/><input type='hidden' readonly size='25'  name='doc_name' id='doc_name' value='$docname'/></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='doctorErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Patient Name &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='25' maxlength='20' name='p_name' id='p_name'/></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='p_nameErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Age &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='3' maxlength='2' name='age' id='age' onkeypress=\"return isNumberKey(event);\"/></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='ageErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Email &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='30' maxlength='50' name='email' id='email' /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='emailErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Mobile No: &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='text' size='13' maxlength='13' name='mob' id='mob' onkeypress=\"return isNumberKey(event);\" /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='mobileErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Gender &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='gender' id='gender'><option value=''>--- select Gender ---</option><option value='Male'>Male</option><option value='Female'>Female</option></select></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='genderErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
   /* 
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Address &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><textarea name='address' id='address' rows='3' cols='40' ></textarea></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='addressErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    */
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    
    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">Appointment Time &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='time_slot' id='time_slot' readonly value='$date $time' />$date $slot12";
    
    $proceed .="<input type='hidden' name='time_slot_param' id='time_slot_param' readonly value=\"$time_slot_param\"/>";
    
    
    $proceed .="<input type='hidden' name='display_time_slot' id='display_time_slot' readonly value='$date $slot12' /><input type='hidden' name='app_date' id='app_date' readonly value='$date' /><input type='hidden' name='app_time' id='app_time' readonly value='$slot12' /></td></tr>";

    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='appointment_timeErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";

    $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">New Patient &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='new_patient' id='new_patient' ><option value=''>----select----</option><option value='0'>yes</option><option value='1'>NO</option></select>";
    
    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='NewPatientErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
    
  /*  $proceed .="<tr><td align=\"right\" bgcolor=\"#F5F5F5\">New to use this System &nbsp; : &nbsp;</td><td align=\"left\" bgcolor=\"#F5F5F5\"><select name='new_for_system' id='new_for_system' ><option value=''>----select----</option><option value='0'>yes</option><option value='1'>NO</option></select>";
    
    $proceed .="<!--ERROR DIV --><tr><td></td><td  align='left' height='8'><div id='NewForPatientErrDiv' class='error' style='color: #F33;font-family:verdana;font-size:10px; margin-left:8px'></div></td></tr><!--  END ERROR DIV --> ";
*/
    $proceed .="<tr><td colspan='2'><img src='../images/blank.gif' width='1' height='6' alt='' border='0'></td></tr>";

    $proceed .="<tr><td colspan='2' bgcolor=\"#F5F5F5\" align='center'><input type='button' name=\"button2\" class=\"bookBtn\" onclick='book_final(book_doc_app)' value='Book' /><input type='button' class=\"bookBtn\" onclick='book_cancel(book_doc_app)' value='Cancel' /></td></tr>";

    $proceed .="</table></form>";

    echo $proceed;


}else{
    echo "<table border='0' cellpadding='0' cellspacing='1' width='500' align='center' class='s90registerform'><tr><td align='center'><font color='red'>Appointment Slot is already Booked</font></td></tr></table>";
}


ob_end_flush();


function get_getdoc_name($id){
    include "../db_connect.php";
    
    $qry = "select doc_name from pw_doctors where doc_id=$id";    
    $res = mysql_query($qry);
    $data = mysql_fetch_array($res);
    return $data[0];
}


function get_week($d){
    $day=0;
    
    switch($d){
        case "Mon" : $day = 1;break;
        case "Tue" : $day = 2;break;
        case "Wed" : $day = 3;break;
        case "Thu" : $day = 4;break;
        case "Fri" : $day = 5;break;
        case "Sat" : $day = 6;break;
        case "Sun" : $day = 7;break;
    }    
    return $day;    
}
?>

</tr></table></td></tr></table></body></html>