<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cancel_appointment.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css" />
<script type="text/javascript" src="js/timepicker/rainbow.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/rainbow.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<style type="text/css">
	.ui-dialog { font-size: 11px; }
	body {
		font-family: Tahoma;
		font-size: 12px;
	}
	#question {
		width: 300px!important;
		height: 60px!important;
		padding: 10px 0 0 10px;
	}
	#question img {
		float: left;
	}
	#question span {
		float: left;
		margin: 20px 0 0 10px;
	}
        .ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
<script type="text/javascript">
   
   function get_time_slots(){
       
        var doc_id=encodeURI(document.getElementById('doctor').value);
        
        var ddate=encodeURI(document.getElementById('date2').value);                
        
        if(doc_id!="" && ddate!="0000-00-00"){
            //$('#doc_timeslots').load('get-doctor-time-slots.php?doctor_id='+doc_id+'&date='+ddate); 

            //$('#timeslots').load('get-html-time-slots.php?doctor_id='+doc_id);
            
            $('#doc_timeslots').load('get-doctor-appoint-time-slots.php?doctor_id='+doc_id+'&date='+ddate);

            document.getElementById("doc_timeslots").style.display="block";

            //document.getElementById("hid2").style.display="block";
        }
        
    }
    
    

    
</script>

<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php
require_once('calendar/tc_calendar.php');
?>
<?php include 'header.php'; ?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="180" valign="top">
<!-- <div id="s90dashboardbg"  style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="../images/phr_dashboard_uline.jpg" width="205" height="20" />  -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1> </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">                        
                    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                            <?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
                    </div>
                </td>
                
            </tr>
                <tr><td  colspan="2" align="center" class="clinicBg" valign="top">  
            <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            
    ?>
    <form action="actions/cancel_doctor_appointment.php" method="post" name="doctor_appointment_cancel" id="doctor_appointment_cancel">
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Cancel Doctor Availability</th></tr>                
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Doctor</b><font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
                <select name="doctor" id="doctor" onchange="get_time_slots()">
                   
                    <?php
                        $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id";                        
                        $res = mysql_query($qry);
                        $num=mysql_num_rows($res);
                        if($num==1)
                        {
                        while ($data = mysql_fetch_array($res)){
                            echo "<option value='".$data['doc_id']."|".$data['doc_name']."' selected='selected'>".$data['doc_name']."</option>";                            
                        }
                        } 
                         else{?>
<option value="" selected="selected" >-----select doctor-----</option>
                    <?php        while ($data = mysql_fetch_array($res)){
                            	echo "<option value='".$data['doc_id']."|".$data['doc_name']."'>".$data['doc_name']."</option>";
                             }
                         }
                                                 
                    ?>
                </select>
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr >
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Date</b><font color="#FF0000">*</font>:</td>            
            <td width="124" valign="top" bgcolor="#F5F5F5">
                
                    <?php
                    
                    
                      $date2=date("Y-m-d", time()+86400);
                      $date3=date("Y-m-d", time()+86400);
                      $myCalendar = new tc_calendar("date2");
                      $myCalendar->setIcon("calendar/images/iconCalendar.gif");
                      //$myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
                      $myCalendar->setPath("calendar/");
                      $myCalendar->setYearInterval(1910, 2015);
                      $myCalendar->dateAllow($date3, '2015-03-01');
                      $myCalendar->setDateFormat('j F Y');
                      $myCalendar->setOnChange("get_time_slots()");
                      //$myCalendar->setWidth(500);	  
                      //$myCalendar->autoSubmit(true, "form1");
                      //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                      $myCalendar->writeScript();
                      
                      
                      
                        ?>
                
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td colspan="2" align="left" height="8">
	            <div id="calErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        
        <tr  >
            <td colspan="2" align="center" bgcolor="#F5F5F5" >
                <div id="doc_timeslots" style="display:none;">
                    
                </div>                
            </td>        
        </tr>
        <!--    ERROR DIV -->
	<tr>
             <td colspan="2" align="left" height="8">
	            <div id="doc_timeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        

        <tr id="hid2" style="display:none;">
            <td colspan="2" width="124" align="center" bgcolor="#F5F5F5" >
                <div id="timeslots"></div>
            </td>        
        </tr>

        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Cancel Appointment" tabindex="6" type="button"  onclick="clinic_cancel_doc_appointment(doctor_appointment_cancel)"  /></td>        
        </tr>
        
    </table>
    </form>
    <?php
        }
    ?>
            
    </td></tr>
</table>
    
    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>
</body></html>
<div id="dialog-confrm-cancel" title="Cancel Doctor Appointments !" style="display:none;">
        <?php echo "Dear Admin"; ?>,<p> Are you sure ?</p>
</div>

