<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
	include ("../pwconstants.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
        $clinic_name=$_SESSION['clinic_name'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/appointment.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>

<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css" />
<script type="text/javascript" src="js/timepicker/rainbow.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/rainbow.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />
<script type="text/javascript"> 
    
$(function() {
    
    $('#fromtime_slot1').timepicker({
	'minTime': '07:00am',
	'maxTime': '10:59pm'
    });

    $('#fromtime_pm').timepicker({
	'minTime': '07:00am',
	'maxTime': '11:45pm'
    });

     $('#fromtime_slot2').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '10:59pm'
     });

    $('#fromtime_slot3').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '10:59pm'
     });
    
    $('#fromtime_slot4').timepicker({
    	'minTime': '07:00am',
    	'maxTime': '10:59pm'
    });
});

function set_to_time(slot){
    var time = document.getElementById("fromtime_slot"+slot).value;
    if(time!=""){
            $('#totime_slot'+slot).timepicker({
                  'minTime': time,
                  'maxTime': '11:59pm'
        });
    }
}


function set_from_time(slot){
    var time = document.getElementById("totime_slot"+slot).value;
    var nextSlot = slot+1;
     $('#fromtime_slot'+nextSlot).timepicker({
                  'minTime': time,
                  'maxTime': '10:59pm'	
     });
    
}


function get_detail(){
    var clinic=encodeURI('<?= $clinic_id."|".$clinic_name ?>');    
    $('#doc_clinic_details').load('get_clinic_doc_details.php?clinic='+clinic);
    get_doctors_slots();
}
  
function Hide_Load()
{
    $("#loading").fadeOut('slow');
}  
</script>
<style>
.sub{left:195px !IMPORTANT;}   
#cssmenu{width: 100% !IMPORTANT;}

#loading { 
width: 70%; 
position: absolute;
}
</style>
<style type="text/css">
.ui-dialog { font-size: 11px; }
body {
        font-family: Tahoma;
        font-size: 12px;
}
#question {
        width: 300px!important;
        height: 60px!important;
        padding: 10px 0 0 10px;
}
#question img {
        float: left;
}
#question span {
        float: left;
        margin: 20px 0 0 10px;
}
.ui-widget-header { border: 1px solid #EA0977; background: #EA0977 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php include 'header.php'; ?>
<table width="1025" border="0" cellspacing="10" cellpadding="2" align="center" class="s90greybigbox">
<tr><td width="220" valign="top">
<!-- <div id="s90dashboardbg" style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="../images/phr_dashboard_uline.jpg" width="205" height="20" />  -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="180"><h1> </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">                      
                    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                            <?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
                    </div>
                </td>
                
            </tr>
                <tr><td colspan="2"   align="center" valign="top" class="clinicBg"  style="background-repeat: repeat-x;" >
                        
            <?php
        if($_SESSION['msg']!=""){
            echo "<center><font color='green' size='3'>".$_SESSION['msg']."</font></center>";
            $_SESSION['msg'] = "";
        }else{
            if($_SESSION['error']!=""){
                    echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                    $_SESSION['error'] = "";
            }
            $selected_doctor = "";
            if(isset($_POST['selected_doc_id'])){
				 $doc_details = explode("|", $_POST['selected_doc_id']);
            	 $selected_doctor = $doc_details[0];
            }
    ?>
    <form action="actions/add_doctor_appointment.php" method="post" name="doctor_appointment" id="doctor_appointment">
     <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
        <tr><th colspan="2">Create-Modify Dr Schedule</th></tr>
		
 <iframe src="https://www.google.com/calendar/embed?title=pinkAppoint&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=shoaib%40pinkwhalehealthcare.com&amp;color=%232952A3&amp;src=%23contacts%40group.v.calendar.google.com&amp;color=%238C500B&amp;src=en.indian%23holiday%40group.v.calendar.google.com&amp;color=%235F6B02&amp;ctz=Asia%2FCalcutta" style=" border-width:0 " width="800" height="600" frameborder="0" scrolling="no"></iframe>               
<!--iframe src="http://pinkwhalehealthcare.appointy.com/?isGadget=1" width="760px" height="555px" scrolling="auto" frameborder="0" allowtransparency="true"></iframe>    
<!--iframe src="https://acuityscheduling.com/schedule.php?owner=11315932" width="100%" height="800" frameBorder="0"></iframe>
<script src="https://d3gxy7nm8y4yjr.cloudfront.net/js/embed.js" type="text/javascript"></script-->    
<iframe src="wdCalendar/wdCalendar/sample.php" width="100%" height="800" scrolling="auto" allowtransparency="true" frameborder="0" ></iframe>   
<a href="https://pinkwhale.appointlet.com/" title="Schedule Appointment">Schedule Appointment</a>
<!--iframe src="https://shoaib.youcanbook.me/?noframe=true&skipHeaderFooter=true" style="width:100%;height:1000px;border:0px;background-color:transparent;" frameborder="0" allowtransparency="true" onload="keepInView(this);"></iframe>
<script>function keepInView(item) {if((document.documentElement&&document.documentElement.scrollTop)||document.body.scrollTop>item.offsetTop)item.scrollIntoView();}</script-->

	  <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Doctor</b><font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
            	<input type="hidden" name="clinic" id="clinic" value="<?= $clinic_id."|".$clinic_name ?>" />
            	
                <select name="doctor" id="doctor" onchange="get_detail()" >
                <option value="" selected="selected" disabled>-----select doctor-----</option>
                    <?php
                        $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id";                        
                        //echo $qry;
                        $res = mysql_query($qry);
                        $num=mysql_num_rows($res);
                        if($num==1)
                        { while ($data = mysql_fetch_array($res)){
							if ($data['doc_id']==$selected_doctor){
								$selected = "selected='selected'";	
							}else{
								$selected = "";
							}
                            echo "<option value='".$data['doc_id']."|".$data['doc_name']."' $selected>".$data['doc_name']."</option>";                            
                        }
                        }
                        else {?>
							<option value="" selected="selected" disabled>-----select doctor-----</option>
							<?php while ($data = mysql_fetch_array($res)){
								if ($data['doc_id']==$selected_doctor){
									$selected = "selected='selected'";
								}else{
									$selected = "";
								}
							     echo "<option value='".$data['doc_id']."|".$data['doc_name']."' $selected>".$data['doc_name']."</option>";
							   }
							}
                    ?>
                </select>
                <div id="loading"></div>
            </td>        
        </tr>
        <?php// echo "select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id"; ?>
        <!--    ERROR DIV -->
	<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
        <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr><td colspan="2"><div id="doc_clinic_details"></div></td></tr>
        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        
        <tr>
            <td width="30%" align="right" bgcolor="#F5F5F5"><b>Appointment by Day</b><font color="#FF0000">*</font>:</td>
            <td width="40%" align="left" bgcolor="#F5F5F5">
            <?php
            	
            	if($selected_doctor!=""){
            	 $qry_days = "select Day from doctor_time_slots where clinic_id='$clinic_id' AND doc_id='$selected_doctor' group by Day";
            	 $qry_days_res = mysql_query($qry_days);
            	 $selected_days = array();
            	 while ($row = mysql_fetch_array($qry_days_res, MYSQL_BOTH)) {
            	 	array_push($selected_days,  $row[0]);
            	 } 
            	}
            ?>
                <table border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td><input type="checkbox" name="day" id="all" onclick="checkall()" value="all">All</input></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="day[]" id="day" value="1" <?php if (in_array('1', $selected_days)) {echo "checked='checked'";}?>/>Monday</td>
                        <td><input type="checkbox" name="day[]" id="day" value="2" <?php if (in_array('2', $selected_days)) {echo "checked='checked'";}?>/>Tuesday</td>                    
                        <td><input type="checkbox" name="day[]" id="day" value="3" <?php if (in_array('3', $selected_days)) {echo "checked='checked'";}?>/>Wednesday</td> 
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="day[]" id="day" value="4" <?php if (in_array('4', $selected_days)) {echo "checked='checked'";}?>/>Thursday</td>                     
                        <td><input type="checkbox" name="day[]" id="day" value="5" <?php if (in_array('5', $selected_days)) {echo "checked='checked'";}?>/>Friday</td> 
                        <td><input type="checkbox" name="day[]" id="day" value="6" <?php if (in_array('6', $selected_days)) {echo "checked='checked'";}?>/>Saturday</td> 
                    </tr>   
                    <tr>
                        <td><input type="checkbox" name="day[]" id="day" value="7" <?php if (in_array('7', $selected_days)) {echo "checked='checked'";}?>/>Sunday</td>   
                    </tr>
                </table>
            </td>        
        </tr>        
        <!--    ERROR DIV -->
		<tr>
             <td> </td>
             <td  align="left" height="8">
	            <div id="dayErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>
        <!--  END ERROR DIV --> 
       
        <tr>
            <td width="30%" colspan="2" align="center" bgcolor="#F5F5F5">
                <table border="0" >
                    <tr><td>
                        <table style="margin:2px;width:415px;" border="0" cellpadding="0" cellspacing="1"  align="center" class="s90registerform">
                            <tr><td colspan="2" align="center"><b>Slot 1</b></td></tr>
                            <tr>
                            	<td>
                            	  Category
	                                <select id="category1" name="category1" onchange="change_category(1)" style="margin:12px;">
	                                	<option value='-1'>---Category---</option>
		                                 <?php  foreach ($categories as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                                 }?>
	                                </select>
	                              
                                </td>
                                <td>
									<select name="app_type1" id="app_type1" style="display:none;">
										<?php  foreach ($patient_Types as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                                 }?>
		                            </select>								 
								</td>
                            </tr>
                            <tr>
                                <td nowrap>From Time :<input type="text" style="width: 70px;" readonly name="fromtime_slot1" id="fromtime_slot1" size="10" onchange="set_to_time(1)" maxlength="10" class="time" /></td>
                                <td nowrap> To Time : <input type="text" style="width: 70px;" readonly name="totime_slot1" id="totime_slot1" size="10" onchange="set_from_time(1)" maxlength="10" class="time" /></td>
                            </tr>
                            <!--    ERROR DIV -->
                            <tr>
                               <td colspan="2" align="center" height="8">
                                  <div id="from_to_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                               </td>
                            </tr>

                            <!--  END ERROR DIV --> 

                            <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                            <tr>
                                <td>Appointment Duration:</td>
                                <td>
                                    <select name="app_duration_slot1" id="app_duration_slot1" onchange="get_duration_slot(1)">
                                        <option value="" selected="selected" disabled>-- Duration --</option> 
                                        <?php
                                            for($i=5;$i<=60;){
                                                echo "<option value='$i'> $i min </option>";
                                                $i +=5;
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            
                            <!--    ERROR DIV -->
        
                            <tr>                                 
                                 <td colspan="2" align="center" height="8" >
                                        <div id="app_duration_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                 </td>
                            </tr>

                            <!--  END ERROR DIV --> 

                            <tr>
                                <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                    <div id="duration_slot1" style="display:none;"></div>
                                    <div id="duration_slot1_display" style="display:none;"></div>
                                </td>              
                            </tr>

                           <!--    ERROR DIV -->

                            <tr>
                                 <td  colspan="2" align="center" height="8">
                                        <div id="time_slotErrDiv_slot1" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                 </td>
                            </tr>
							<tr>
                                <td>NA :</td>
                                <td>
                                    <input type="checkbox" name="na_slot1" id="na_slot1" onchange="checkRemoveSlot();" value="1"/>
                                </td>
                            </tr>
                           
                            <!--  END ERROR DIV --> 
                        </table>
                        </td><td>
                            <table style="margin:2px;width: 415px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 2</b></td></tr>
                                <tr>
	                            	<td>
	                            	  Category
	                            	
		                                <select id="category2" name="category2" onchange="change_category(2)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td>
	                            	<td>
										<select name="app_type2" id="app_type2" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot2" id="fromtime_slot2" onchange="set_to_time(2)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot2" id="totime_slot2" onchange="set_from_time(2)" size="10" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot2" id="app_duration_slot2" onchange="get_duration_slot(2)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot2" style="display:none;">

                                                </div>
                                                <div id="duration_slot2_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->

                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                                <div id="time_slotErrDiv_slot2" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot2" id="na_slot2" onchange="checkRemoveSlot();" value="2"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>
                           </td>
                        </tr>
                        <tr>
					        <td>
								<table style="margin:2px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 3</b></td></tr>
                                <tr>
	                            	<td>Category
		                                <select id="category3" name="category3" onchange="change_category(3)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td><td>
										<select name="app_type3" id="app_type3" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot3" id="fromtime_slot3" onchange="set_to_time(3)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot3" id="totime_slot3" size="10" onchange="set_from_time(3)" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot3" id="app_duration_slot3" onchange="get_duration_slot(3)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot3" style="display:none;">

                                                </div>
                                                <div id="duration_slot3_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->

                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                                <div id="time_slotErrDiv_slot3" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot3" id="na_slot3" onchange="checkRemoveSlot();" value="1"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>					        
					        </td>
					        <td>
					           <table style="margin:2px;" border="0" cellpadding="0" cellspacing="1" align="center" class="s90registerform" >
                                <tr><td colspan="2" align="center"><b>Slot 4</b></td></tr>
                                <tr>
	                            	<td>Category
		                                <select id="category4" name="category4" onchange="change_category(4)" style="margin:12px;">
		                                	<option value='-1'>---Category---</option>
			                                 <?php  foreach ($categories as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
		                                </select>
	                                </td>
	                                <td>
										<select name="app_type4" id="app_type4" style="display:none;">
											<?php  foreach ($patient_Types as $key => $value) {
			                                	echo "<option value='$key'>".$value."</option>";
			                                 }?>
			                            </select>								 
									</td>
	                            </tr>
                                    <tr>
                                        <td nowrap>From Time :
                                            <input type="text" style="width: 70px;" readonly name="fromtime_slot4" id="fromtime_slot4" onchange="set_to_time(4)" size="10" maxlength="10" class="time" />                            
                                        </td>

                                        <td nowrap> To Time :
                                            <input type="text" style="width: 70px;" readonly name="totime_slot4" id="totime_slot4" size="10" maxlength="10" class="time" />
                                        </td>
                                    </tr>

                                    <!--    ERROR DIV -->

                                    <tr>
                                         <td colspan="4" align="center" height="8">
                                                <div id="from_to_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr><td ><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                    <tr>
                                        <td>Appointment Duration:</td>
                                        <td>
                                            <select name="app_duration_slot4" id="app_duration_slot4" onchange="get_duration_slot(4)">
                                                <option value="" selected="selected" disabled>-- Duration --</option> 
                                                <?php

                                                    for($i=5;$i<=60;){
                                                        echo "<option value='$i'> $i min </option>";
                                                        $i +=5;
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                     <!--    ERROR DIV -->
        
                                    <tr>
                                         <td  colspan="2" align="center"  height="8">
                                                <div id="app_duration_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>

                                    <!--  END ERROR DIV --> 

                                    <tr>
                                            <td  width="30%" colspan="2" align="center" bgcolor="#F5F5F5" >
                                                <div id="duration_slot4" style="display:none;">

                                                </div>
                                                <div id="duration_slot4_display" style="display:none;">

                                                </div>
                                            </td>              
                                    </tr>

                                   <!--    ERROR DIV -->
                                    <tr>
                                         <td> </td>
                                         <td  align="left" height="8">
                                            <div id="time_slotErrDiv_slot4" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                         </td>
                                    </tr>
                                    <tr>
		                                <td>NA :</td>
		                                <td>
		                                    <input type="checkbox" name="na_slot4" id="na_slot4" onchange="checkRemoveSlot();" value="4"/>
		                                </td>
		                            </tr>
                                    <!--  END ERROR DIV --> 
                                </table>
					        </td>
				        </tr>
                </table>
            </td>
        </tr>
        
        <!--    ERROR DIV -->

        <tr>
             <td colspan="2"  align="center" height="8">
                    <div id="AM_PM_time_ERROR" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
             </td>
        </tr>

        <!--  END ERROR DIV --> 
        
        <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
        <tr>
            <td colspan="2" align="center" bgcolor="#F5F5F5">
            	<input onmouseover="this.style.cursor='pointer'"  value="Create Appointment Slot" tabindex="6" type="button"  onclick="add_doc_appointment(doctor_appointment)"  />
            </td>
        </tr>
    </table>
    </form>
    <?php
        }
    ?>
          
    	</td></tr>
	</table>
  </td></tr>
</table>
<?php
include 'footer.php'; ?>
<div id="appointment-confirm" title="Appointment slots Already Created !!!" style="display: none">
    <p><?php echo "Dear $clinic_name,<br /> <p>Do you want to modify the appointment slots?</p>" ?></p>
</div>
</body></html>

