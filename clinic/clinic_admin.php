<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<link href="css/TableCSSCode.css" rel="stylesheet" type="text/css">
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
include "../admin/get-who-booked-details.php";
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!--<div id="s90dashboardbg">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="220" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());

include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;

?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
	    <div id="clinic_docs">
                <table width='750' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='CSSTableGenerator'>
		<tr><th colspan="6" style="background:#C4C4C4;height:32px;">Clinic Doctors</th></tr>
		<?php	
			$count =0;
			// Display all the data from the table 
			$sql="select p.doc_name,p.doc_id,p.doc_specialities from clinic_doctors_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.blocked<>'Y' union select p.doc_name,p.doc_id,p.doc_specialities from doctor_clinic_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.blocked<>'Y'";
			$obj->query($sql);
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) die(mysql_error());
                        
                        
			?>
			<tr>
                            <td align='left'><strong>SI No:</strong> </td>
                            <td align='left'><strong>Doctor-id</strong></td>
                            <td align='left'><strong>Name</strong></td>
                            <td align='left'><strong>speciality</strong></td>
                         </tr>
                                    <?php	
                                    while($row = mysql_fetch_assoc($rs)) 
                                    {
                                        $count ++;
                                            $id=$row['doc_id'];
                                            $name=$row['doc_name'];
                                            $specialities=$row['doc_specialities'];
                                                    echo "<tr bgcolor='#ffffff'>";
                                                    echo "<td>$count</td> ";
                                                    echo "<td>$id </td> ";
                                                    echo "<td>$name</td>"; 
                                                    echo "<td>$specialities </td> ";
                                                    echo "</tr>";          			
                                    }
                                            $ps1 = $pager->renderFirst();
                                            $ps2 = $pager->renderPrev();
                                            $ps3 = $pager->renderNav('<span>','</span>');
                                            $ps4 = $pager->renderNext();
                                            $ps5 = $pager->renderLast();
                                            ?>
                                            </table>
                                            <table style="margin-left:50%">
                            <tr><td colspan='6' align='center'>
                                            <?php
                            echo "$ps1";
                            echo "$ps2";
                            echo "$ps3";
                            echo "$ps4";
                            echo "$ps5";
                            echo "</td></tr>"; 
                            ?>
                    </table>
                <br />
                <br />
                </div>


            <div id="clinic_dash">   
                <form  method="POST">
                <table border="0" cellpadding="0" cellspacing="1"  align="center" ><tr>   
                    <td width="30%" align="right" ><b>Date</b>:</td>            
                    <td width="124" valign="top" >

                            <?php


                              $date2=date("Y-m-d", time()+86400);
                              $date3=date("Y-m-d", time()+86400);
                              $myCalendar = new tc_calendar("date2",true,false);
                              $myCalendar->setIcon("calendar/images/iconCalendar.gif");
                              $myCalendar->setDate(date('d', strtotime($today_date)), date('m', strtotime($today_date)), date('Y', strtotime($today_date)));
                              $myCalendar->setPath("calendar/");
                              $myCalendar->setYearInterval(1910, 2015);
                              $myCalendar->dateAllow($today_date, '2015-03-01');
                              $myCalendar->setDateFormat('j F Y');
                              //$myCalendar->setOnChange("get_time_slots()");
                              //$myCalendar->setWidth(500);	  
                              //$myCalendar->autoSubmit(true, "form1");
                              //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                              $myCalendar->writeScript();



                                ?>
                    </td>                    
                    <td width="124" valign="top" ><input type="submit" value="Go" /></td>                    
                    </tr>
                </table>
                </form>
                
                <table border="0" cellpadding="0" cellspacing="1" width="600" align="center" bgcolor='#eeeeee' class="CSSTableGenerator">
                <?php     
                    if($_POST['date2']==""){
                        echo "<tr><th colspan=\"5\" style='background:#C4C4C4;height:32px;'>Today Doctor Wise Appointment Details</th></tr>";
                    }else{
                        $today_date = $_POST['date2'];
                        echo "<tr><th colspan=\"5\"  style='background:#C4C4C4;height:32px;'>Doctor Wise Appointment Details for ". date("d M Y",strtotime($_POST['date2']))."</th></tr>";
                    }
                ?>                
                
                <tr>
                    <td align='center'><b>Doctor Name</b></td>
                    <td align='center'><b>Appointment Time</b></td>
                    <td align='center'><b>Patient Name</b></td>                    
                    <td align='center'><b>Token-ID</b></td>
		    <td align='center'><b>Booked By</b></td>
                </tr>
                <tr>
                <?php

                   

                    $qry = "select p.doc_name,p.doc_id from clinic_doctors_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.appoint_flag=1 and p.blocked<>'Y' union select p.doc_name,p.doc_id from doctor_clinic_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.appoint_flag=1 and p.blocked<>'Y'";                   
                    $res = mysql_query($qry);
                    $num = mysql_num_rows($res);
                    if($num==0){
                       // echo "<script>document.getElementById(\"clinic_dash\").innerHTML=\"<font size=4 color=red>No Doctor's</font>\";</script>";
			echo "<script>document.getElementById(\"clinic_dash\").innerHTML=\"<font size=4 color=red></font>\";</script>";
                    }else{
                        while($dat = mysql_fetch_array($res)){
                            $qry1 = "select admin_id,DATE_FORMAT(from_time,'%h:%i %p') as from_time,patient_name,email,mobile,gender,age,adress,token_id from Appointment_book_details where doc_id='".$dat['doc_id']."' and clinic_id='$clinic_id' and from_time like '$today_date%' and status=2 order by from_time";
                            
                            $res1 = mysql_query($qry1);
                            $num1 = mysql_num_rows($res1);                            
                            if($num1==0){
                                echo "<td  align='left' bgcolor='#F5F5F5'>".$dat['doc_name']."</td>";
                                echo "<td colspan='4' align='center' bgcolor='#F5F5F5'>No Appointment's </td></tr><tr>";
                            }else{
                                echo "<td rowspan='$num1' align='left' bgcolor='#F5F5F5'>".$dat['doc_name']."</td>";
                                while($data1 = mysql_fetch_array($res1)){
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['from_time']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['patient_name']."</td>";
                                    echo "<td align='center' bgcolor='#F5F5F5'>".$data1['token_id']."</td>";
				    echo "<td align='center' bgcolor='#F5F5F5'>".get_name_detals($data1['admin_id'])."</td></tr><tr>";
                                }                            
                            }
                        }
                    }
                ?>
                </tr></table>
            </div>
            </tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
    
    </body></html>
