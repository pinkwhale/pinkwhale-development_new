<?php
include("../includes/site_config.php");
?>
<link type="text/css" rel="stylesheet" href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,300,700'/>
<!-- CSS Stylesheet-->
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/bootstrap-responsive.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/styles.css"  />
<link type="text/css" rel="stylesheet" href="<?php echo $site_url; ?>css/styles-responsive.css"  />

<link type="text/css" rel="alternate stylesheet" media="screen" title="style1" href="<?php echo $site_url; ?>css/style1.css" />
<link type="text/css" rel="alternate stylesheet" media="screen" title="style2" href="<?php echo $site_url; ?>css/style2.css" />
<link type="text/css" rel="alternate stylesheet" media="screen" title="style3" href="<?php echo $site_url; ?>css/style3.css" />

<!-- Java Library -->

<!-- Library bootstrap 2.04-->
<!--<script type="text/javascript"  src="<?php echo $site_url; ?>/js/bootstrap.min.js"></script>-->
<!-- Library Theme Custom-->
<script type="text/javascript"  src="<?php echo $site_url; ?>js/custom.js"></script>
<script type="../text/javascript" src="js/registration_validation.js"></script>

<div id="header" class="header-style2 clearfix" style="margin-bottom: -20px;">
    <div class="container">
    <div class="login-type-name">
        <span class="name"><?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?></span>
    </div>
	
        <a class="logo" href="<?php echo $site_url; ?>index.php">
            <img src="../img/pinkWhale_Logo_CCP.png">
        </a>
       
        <div class="contact-top">            
            <span class="phone">Need Help? Call: <span class="pw-color">+91-80-67007700</span></span>
            <div class="tlogin" >
                <a href="../index.php">Home</a> | 
				 <a href="registration_details.php">My Account</a> | 
                <a href="clinic_details.php">Admin Profile</a> | 
                <a href="changepassword.php">Change Password</a> | 
                <a href="../logout.php" class="signin">Sign out</a>
            </div>
        </div>
    </div>
</div> 
<!-- <div class="section-title  full-bg  " style="background-image:url(../slider/slide_bg1.jpg)">
    <div class="container">
        <h2 class="animated bounceInDown"></h2>
    </div>    
</div> -->

