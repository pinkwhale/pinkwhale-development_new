<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/book_appointment.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />



<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
    
<?php include 'header.php'; 
require_once('calendar/tc_calendar.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="180" valign="top">

<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
    <table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
        <tr>
                <td width="220"><h1>Change Password</h1></td>
                <td width="528" bgcolor="#f1f1f1" align="right">                      
                            <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                                    <?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
                            </div>
                        </td>

                    </tr>
        
                        <tr><td colspan="2"   align="center" class="clinicBg" valign="top">  
  <?php
$qry= "SELECT * FROM `clinic_details` where `clinic_id`='$clinic_id' ";
	$qry_rslt = mysql_query($qry);
          
	$result = mysql_fetch_array($qry_rslt);
	
		
?>                 
        <table width="644" border="0" align="center" cellspacing="10" style="border:1px solid #CCC;" class="s90dbdtbls_drdetailstbl">
<tr><td>
<form name="clinic_change_pass_form" id="clinic_change_pass_form" action="actions/edit_clinic_password.php" method="post">
<table width="580" border="0" align="center">

<tr><td width="185" align="right"><div class="label">User Name :</div></td>
    <input type="hidden" name="clinic_id" id="clinic_id" value="<?php echo $clinic_id;?>" />
<td width="155"><input type="text" name="user_name" id="user_name" value="<?php echo $result['username'];?>" disabled="disabled"/></td>
<td width="226">&nbsp;</td>
</tr>
<tr><td align="right"><div class="label">New Password :</div></td><td><input type="password" name="new_pswd" id="new_pswd" onkeyup="validate_pswd(form)" autocomplete="off"/></td>

<!--    ERROR DIV -->
		
             <td  align="left" height="8">
	            <div id="pswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
<!--  END ERROR DIV --> 

</tr>
<tr>
<td align="right"><div class="label">Confirm Password :</div></td>
<td><input type="password" name="cnfm_pswd" id="cnfm_pswd" onkeyup="validate_pswd(form)"/></td>
<!--    ERROR DIV -->
	
             <td  align="left" height="8">
	            <div id="repswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
<!--  END ERROR DIV --> 

</tr>

<tr>
    <td>&nbsp;</td>
    <td align="left"><input type="button" name="clinic_change_pass_save" id="clinic_change_pass_save" value="Save"  onclick="return psw_val(clinic_change_pass_form)" />
    <a href="clinic_admin.php"><input type="button" name="clinic_change_pass_cancel" id="clinic_change_pass_cancel" value="Cancel" /></a></td>
</tr>
</table>
</form>
<script type="text/javascript">
function validate_pswd(form)
{
	var pswValidated = false;
	var repswValidated = false;
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";
		
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";	
		
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	//--Password validation--//	
	if (form.new_pswd.value!='' )	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			 pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			 pswValidated = false;
		 } 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			 pswValidated = false;
		 }
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		 }
			 pswValidated = true;
	 }
	 	
}

function psw_val(form){
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_pswd.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Password cannot be blank";		
    	pswValidated = false;
	}
	if (form.cnfm_pswd.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Confirm Password  cannot be blank";		
		repswValidated = false;
	}
	if(form.cnfm_pswd.value!='')
	{
		if(form.new_pswd.value!=form.cnfm_pswd.value)
		{
			document.getElementById("repswErrDiv").innerHTML = "Password mismatch Try again!";			
			pswValidated = false;
			repswValidated = false;
		}	
	}
	if (form.new_pswd.value!='' )	{	
		if(form.new_pswd.value.length < 6) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: Password must contain at least six characters!";
			  pswValidated = false;
		} 
		 re = /[0-9]/; 
		 if(!re.test(form.new_pswd.value)) {
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one number (0-9)!";
			  pswValidated = false;
		 } 
		 re = /[a-z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one lowercase letter (a-z)!";
			  pswValidated = false;
		 }
		 re = /[A-Z]/;
		 if(!re.test(form.new_pswd.value)) { 
			 document.getElementById("pswErrDiv").innerHTML = "Error: password must contain at least one uppercase letter (A-Z)!";
			 pswValidated = false;
		 }
			
	 }
	if(pswValidated && repswValidated )
	{
		document.getElementById('clinic_change_pass_form').submit();
	}
	else
	{
		return false;
	}
}

</script>

            
        
  
    </tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
    
    </body></html>
