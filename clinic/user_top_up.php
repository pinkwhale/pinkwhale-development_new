<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="../Scripts/jquery.js" type="text/javascript"></script>

<script type="text/javascript" src="js/add_package.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
include "../admin/get-who-booked-details.php";
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!--<div id="s90dashboardbg" style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="205" height="20" />-->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
            <div id="clinic_dash">   
                
                                    <form name="add_package" id="add_package" action="actions/top_up_package_action.php" method="POST">

                                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center" class="s90registerform">

                                            <tr><td colspan="2"><span style="color:#3C6;"><?php if (isset($_SESSION['addpackage']))

                                                    echo $_SESSION['addpackage']; $_SESSION['addpackage']=""; ?></span></td></tr>

                                            <tr><th colspan="2">Top Up </th></tr>
                                            

                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

                                            <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

                                            <tr>     
                                                    <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Select Doctor:</div></td>

                                                    <td bgcolor="#F5F5F5" width="238">
                                                        
                                                        <select  name="doctor_list" id="doctor_list" class="registetextbox" onChange="set_doc_packages();"> 
                                                            
                                                          

                                                            <?php
                                                                $doc_qry = "select p.doc_name doc_name,p.doc_id doc_id from clinic_doctors_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.blocked<>'Y'  and (`doc_category`='Specialist' || `doc_category`='Specialist/Expert') union select p.doc_name,p.doc_id from doctor_clinic_details c inner join pw_doctors p on p.doc_id=c.doctor_id and clinic_id='$clinic_id' where p.blocked<>'Y' and (`doc_category`='Specialist' || `doc_category`='Specialist/Expert')";
                                                                $doc_result = mysql_query($doc_qry);
                                                                $num=mysql_num_rows($doc_result);?>
                                                                <option value="" selected="selected" disabled>-Select Doctor-</option>
                                                                <?php while($doc_data = mysql_fetch_array($doc_result)){
                                                                    echo "<option value='".$doc_data['doc_id']."'>".$doc_data['doc_name']."</option>";
                                                                }
                                                                
                                                            ?>

                                                        </select>
                                                    </td>
                                            </tr>


                                            <!--    ERROR DIV -->



                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="doc_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>

                                            <!--  END ERROR DIV --> 



                                            <tr>

                                                <td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                            
                                            <tr>

                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">User ID #:</div></td>

                                                <td bgcolor="#F5F5F5" width="238">
                                                    
                                                    <input type="hidden" name="promo_doc_type" id="promo_doc_type" value="Specialist" />

                                                    <select  name="users_id" id="users_id" class="registetextbox" onchange="display_id();" > 
<!--
                                                        <option value="" selected="selected" >- Select User-ID # -</option>

                                                        <?php

                                                        $qry = "SELECT * FROM `user_details` where user_email!='' and user_id<>0 and blocked<>'Y' order by user_email ASC";

                                                        $qry_rslt = mysql_query($qry);

                                                        while ($result = mysql_fetch_array($qry_rslt)) {

                                                            echo '<option value="' . $result['user_id'] . '">' . $result['user_email'] . '</option>';

                                                        }

                                                        ?>         
-->
                                                    </select>

                                                </td>


                                            </tr>

                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="display_user_id"  class="error" style="color: 	#FF00FF;font-family:verdana;font-size:13px; margin-left:-8px"></div></td>

                                            </tr>


                                            <tr><td align="center" colspan="2"> OR </td></tr>


                                            <tr>

                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Card No. #:</div></td>

                                                <td bgcolor="#F5F5F5" width="238"><input type="text" value="" autocomplete="off" maxlength="6" name="users_id" id="users_id1" onkeyup="display_id();" onkeypress="return isNumberKey(event);" onchange="return check_card(this.value);"/></td>

                                            </tr>

                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

                                            <!--    ERROR DIV -->

                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="user_idErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>

                                            <!--  END ERROR DIV --> 

                                            

                                            <tr>    

                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Packages:</div></td>

                                                <td bgcolor="#F5F5F5" width="238">

                                                    <select  name="promo_packages" id="promo_packages" class="registetextbox" onChange="setPackagePrice();"> 

                                                        <option value="">Select Package</option>



                                                    </select>

                                                </td>

                                            </tr> 



                                            <!--    ERROR DIV -->



                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="package_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>

                                            <!--  END ERROR DIV --> 



                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

                                            <tr>    

                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Price:<span class="WebRupee">Rs.</span></div></td>

                                                <td bgcolor="#F5F5F5" width="238"><div id="price">

                                                        <input type="text" name="package_price" id="package_price" class="input" size="20" disabled="disabled"/></div>

                                                </td>

                                            </tr>



                                            <!--    ERROR DIV -->



                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="price_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>

                                            <!--  END ERROR DIV --> 

                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>

                                            <tr>    

                                                <td bgcolor="#F5F5F5" width="160"><div class="postpropertytext">Comments:</div></td>

                                                <td bgcolor="#F5F5F5" width="238"><textarea name="comments" id="comments"></textarea>

                                                </td>

                                            </tr>

                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="promocode_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>


                                            <tr><td> </td>

                                                <td  align="left">

                                                    <div id="discount_typeErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>

                                            </tr>

                                            <!--  END ERROR DIV --> 



                                            <tr><td><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>


                                            <tr>



                                                <td bgcolor="#F5F5F5">&nbsp;</td>

                                                <td bgcolor="#F5F5F5"><input type="button" value="Add" onclick="admin_pack(add_package)" name="add_pomocode" id="add_pomocode" /></td>

                                            </tr>

                                            </table> 

                               </form>
            </div>
            </tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
    
                                                    <script type="text/javascript">	

                                                        

                                                        

                                                        function display_id(){

                                                            

                                                                var id = encodeURI(document.getElementById('users_id').value);

                                                                var id1 = encodeURI(document.getElementById('users_id1').value);

                                                                

                                                                if(id!=""){

                                                                    document.getElementById("display_user_id").innerHTML= "Pinkwhale id : "+id;

                                                                    document.getElementById("users_id1").disabled = true;

                                                                }

                                                                else if(id1!=""){

                                                                    document.getElementById("users_id").disabled = true;

                                                                }

                                                                

                                                                

                                                        }

                                                        

                                                        function check_card(cardno){

                                                            //alert(cardno);

                                                            //$('#card_no').load('check_card.php?card='+cardno);           

								document.getElementById("user_idErrDiv").innerHTML="";                                                            

								                                                        

                                                            

                                                            //if(cardno!=""){

                                                                var xmlhttp;

                                                                if (window.XMLHttpRequest)

                                                                  {// code for IE7+, Firefox, Chrome, Opera, Safari

                                                                        xmlhttp=new XMLHttpRequest();

                                                                  } 

                                                                else

                                                                  {// code for IE6, IE5

                                                                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

                                                                  } 

                                                                xmlhttp.onreadystatechange=function()

                                                                  {

                                                                    if (xmlhttp.readyState==4 && xmlhttp.status==200)

                                                                    {

                                                                            //alert(xmlhttp.responseText);

                                                                            if(xmlhttp.responseText!=false){
                                                                                
                                                                                var response = xmlhttp.responseText;

                                                                                document.getElementById("user_idErrDiv").innerHTML="<font color=green>Valid Card No.</font><br />Email : "+response;

                                                                            }else{

                                                                                //user_idErrDiv                                                                    
										//alert(xmlhttp.responseText);
                                                                                document.getElementById("users_id1").value="";

                                                                                document.getElementById("users_id1").focus();

                                                                                document.getElementById("user_idErrDiv").innerHTML="Please provide valid Card No.";

                                                                            }



                                                                    }else{

                                                                        document.getElementById("user_idErrDiv").innerHTML='<font color=black>Checking card No. Please wait...</font>';                }

                                                                  }





                                                                xmlhttp.open("GET","check_card.php?card="+cardno,true);

                                                                xmlhttp.send();

                                                            //}

                                                            

                                                        }

                                                        

                                                        function set_doc_packages()

                                                        {

                                                            var doc_type=encodeURI("Specialist");

                                                            var doc_id=encodeURI(document.getElementById('doctor_list').value);

                                                            $('#promo_packages').load('get-doctor-packages.php?doctor_type='+doc_type+'&doc_id='+doc_id);
                                                            
                                                            getpatient();


                                                        }



                                                        function setPackagePrice() {

                                                            var doc_type=encodeURI("Specialist");

                                                            if(doc_type=='Specialist') {

                                                                var doc_id=encodeURI(document.getElementById('doctor_list').value);

                                                                var package=encodeURI(document.getElementById('promo_packages').value);

                                                                $('#price').load('get-spe-package-price.php?doctor_type='+doc_id+'&package='+package);

                                                            }

                                                        }
                                                        
                                                        function getpatient() {

                                                            var doc_id=document.getElementById('doctor_list').value;

                                                            if(doc_id!='') {

                                                                var doc_id=encodeURI(doc_id);

                                                                $('#users_id').load('get-patients-details.php?doc_id='+doc_id);

                                                            }

                                                        }
                                                        
                                                    </script>

                                                    <script type="text/javascript">

                                                        //enable_order_submenu();

                                                        function enable_add_package_submenu()

                                                        { 

                                                            document.getElementById("submenu19").style.display="block";		

                                                        }

                                                        enable_add_package_submenu();

                                                    </script>

    </body></html>

