<?php 
	error_reporting(E_PARSE); 
	date_default_timezone_set('Asia/Calcutta');
	session_start();
	include ("../includes/pw_db_connect.php");
	include ("../pwconstants.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" src="js/book_appointment.js"></script>
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css" />

<script type="text/javascript">

$.ajaxSetup({ cache: false });

   function get_doc_avail(){
        var doc_id=encodeURI(document.getElementById('doctor').value);
        
        $('#doc_avail').load('get-doctor_avail.php?doctor_id='+doc_id); 
        
        document.getElementById("hid").style.display="block";
        document.getElementById("doc_avail_time").innerHTML = "";
    }
   function change_category()
   {
	   if(document.getElementById('category').value=="3")
	   {
		   document.getElementById("typelabel").style.display="block";
		   document.getElementById("typ").style.display="block";
		   }
	   else{
		   document.getElementById("typelabel").style.display="none";
		   document.getElementById("typ").style.display="none";
   }
	   get_time_slots();
   }
    
    function get_time_slots(){

 //   	 alert(encodeURI(document.getElementById('doctor').value));
			var category=document.getElementById('category').value;

			var type=document.getElementById('type').value;

			var doc_id=encodeURI(document.getElementById('doctor').value);
        
        var ddate=encodeURI(document.getElementById('date2').value);
        
        $('#doc_avail_time').load('get-book-appointment-slots.php?doctor_id='+doc_id+'&date='+ddate+'&category='+category+'&type='+type); 
        
        //document.getElementById("hid").style.display="block";
        
    }
    
    function proceed_details(slot24,slot12){
        
        var doc_id=encodeURI(document.getElementById('doctor').value);
        
        var ddate=encodeURI(document.getElementById('date2').value);
        
        var slot24=encodeURI(slot24);
        
        var slot12=encodeURI(slot12);
        
        //alert(slot24);alert(slot12);
        
        $('#book_procceed').load('proceed-booking-process.php?doctor_id='+doc_id+'&date='+ddate+'&slot24='+slot24+'&slot12='+slot12); 
    }

    
</script>

<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
<body>
    
<?php include 'header.php'; 
require_once('calendar/tc_calendar.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr><td width="180" valign="top">
<!-- <div id="s90dashboardbg" style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div >
<img src="../images/phr_dashboard_uline.jpg" width="205" height="20" /> -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr" >
    
        <table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
        <tr>
                <td width="220"><h1></h1></td>
                <td width="528" bgcolor="#f1f1f1" align="right">                      
                            <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                                    <?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
                            </div>
                        </td>

                    </tr>
                        <tr><td colspan="2"   align="center" valign="top" class="clinicBg">  
                                <div id="alert">                 
                                    <?php
                                        if($_SESSION['msg']!=""){
                                            echo "<div class='messageAlert'>".$_SESSION['msg']."</div>";
                                            $_SESSION['msg'] = "";
                                        }else if($_SESSION['error']!=""){
                                            echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                                            $_SESSION['error'] = "";
                                        }else{                               

                                    ?>
                                 </div>
            <div id="book_procceed">
            <form action="book_appointment_proceesing.php" method="post" name="bookappointment" id="bookappointment">
             <table border="0" cellpadding="0" cellspacing="1" width="700" align="center" class="s90registerform">
                <tr><th colspan="2">Book Patient Appointment</th></tr>

                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <td width="30%" align="right" bgcolor="#F5F5F5"><b>Doctor</b><font color="#FF0000">*</font>:</td>
                    <td width="40%" align="left" bgcolor="#F5F5F5">
                        <select name="doctor" id="doctor" onchange="get_doc_avail()">
                            <option value="" selected="selected" disabled>-----select doctor-----</option>
                            <?php
                                $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id' and blocked<>'Y'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id' and blocked<>'Y'  group by doc_id";                        
                                echo $qry;
                                $res = mysql_query($qry);
                                while ($data = mysql_fetch_array($res)){
                                    echo "<option value='".$data['doc_id']."'>".$data['doc_name']."</option>";                            
                                }                        
                            ?>
                        </select>
                    </td>        
                </tr>
                <!--    ERROR DIV -->
                <tr>
                     <td> </td>
                     <td  align="left" height="8">
                            <div id="doctorErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                     </td>
                </tr>
                <!--  END ERROR DIV --> 

                 <tr>
                <td width="30%" align="right" bgcolor="#F5F5F5"><b>Category</b><font color="#FF0000">*</font>:</td>
                                    <td width="43%" align="left" bgcolor="#F5F5F5">
                <div id="cate">
                <select id="category" name="category" onchange="change_category();">
                <option value='-1'>---Category---</option>
                				<?php  foreach ($categories as $key => $value) {
                                	echo "<option value='$key'>".$value."</option>";
                                 }?>
                </select>
                </div>
                </td>
                
                </tr>
                   <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <!--  END ERROR DIV --> 
               <tr>
               <td colspan="1" width="40%" align="right" bgcolor="#F5F5F5"><div id="typelabel" style="display:none"><b>Type : </b><font color="#FF0000">*</font>: </div></td>
                <td colspan="1" width="40%" align="left" bgcolor="#F5F5F5">
               <div id="typ" style="display:none">
                              <select id="type" name="type" onchange="get_time_slots()">
                                <?php  foreach ($patient_Types as $key => $value) {
		                                	echo "<option value='$key'>".$value."</option>";
		                          }?>
                                </select>
                                </div>
                         </td>                              
               </tr>
                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                                 
                <tr>
                    <td id="doc_avail" bgcolor="#F5F5F5" width="30%" colspan="2" align="center" ></td>
                </tr>

                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <td colspan="2" align="center" bgcolor="#F5F5F5"><div id="hid" style="display:none">
                            <table border="0" cellpadding="0" cellspacing="1"  align="center" ><tr>
                                <td width="30%" align="right" bgcolor="#F5F5F5"><b>Date</b><font color="#FF0000">*</font>:</td>            
                                <td width="124" valign="top" bgcolor="#F5F5F5">

                                        <?php

                                          $date2=date("Y-m-d", time()+86400);
                                          $date3=date("Y-m-d", time());
                                          $myCalendar = new tc_calendar("date2",true,false);
                                          $myCalendar->setIcon("calendar/images/iconCalendar.gif");
                                          //$myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
                                          $myCalendar->setPath("calendar/");
                                          $myCalendar->setYearInterval(1910, 2020);
                                          $myCalendar->dateAllow($date3, '2020-03-01');
                                          $myCalendar->setDateFormat('j F Y');
                                          $myCalendar->setOnChange("get_time_slots()");
                                          //$myCalendar->setWidth(500);	  
                                          //$myCalendar->autoSubmit(true, "form1");
                                          //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
                                          $myCalendar->writeScript();

                                            ?>
                                </td></tr>
                               <!--  <tr>
                                <td colspan="2" width="30%" align="center">
                                <select>
                                <option value="1">OPD/New Patients</option>
                                <option value="2">OPD/Follow Ups</option>
                                <option value="3">OPD/Walk-ins</option>
                                <option value="2">VC</option>
                                </select>
                                </td>
                                </tr> -->
                                <!-- <tr>
                                <td colspan="2" width="30%" align="center">
                                <select>
                                <option value="1">New Patients</option>
                                <option value="2">Follow Ups</option>
                                <option value="3">Walkins</option>
                                </select>
                                </td>
                                </tr> -->
                            </table>
                        </div>
                    </td>        
                </tr>
                <!--    ERROR DIV -->
                <tr>
                     <td  align="center" height="8">
                            <div id="calErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                     </td>
                </tr>              
                
                <tr>
                    <td id="doc_avail_time" bgcolor="#F5F5F5" width="30%" colspan="2" align="center" ></td>
                </tr>

                <!--
                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <td width="40%" align="center" colspan="2" bgcolor="#F5F5F5"><input onmouseover="this.style.cursor='pointer'"  value="Create Appointment Slot" tabindex="6" type="button"  onclick="clinic_add_doc_appointment(doctor_appointment)"  /></td>        
                </tr>
                -->
            </table>
            </form>
                  </div>
            <?php
                }
            ?>
            </td></tr>
        </table>
  
    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>
</body></html>