<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
    
</script>
<script type="text/javascript">
    
    function get_week_detail(){
        
        var doctor = document.getElementById("doctor").value;
        
        $("#loading").fadeIn(900,0);
        $("#loading").html("<img src='../images/ajax-loader.gif' />");
        $('#report').load('get-weekly-appointment-details.php?doc_id='+doctor+'&val=0&date=');
        $("#loading").fadeOut('slow');
    }
    
    function get_next_day(val,date){
        
        var doctor = document.getElementById("doctor").value;
        
        if(val==0){
            $('#next-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }else{
            $('#pre-id').html('<p><img src="../images/ajax-loader_small.gif" /></p>');    
        }
    
    
        $('#report').load('get-weekly-appointment-details.php?doc_id='+doctor+'&val='+val+'&date='+date);
        
    }
    
</script>
<style>

    #loading { 
        width: 80%; 
        position: absolute;
    }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!-- <div id="s90dashboardbg" style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="205" height="20" /> -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="300"><h1>Appointment Summary</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
    </tr>
    <tr>
        <td colspan="2" align="center" class="clinicBg" height="500" valign="top">
            <br /><br />
            <div id="doctor_dash">
                <table border="0" cellpadding="0" cellspacing="1" align="center">
                    <tr>
                        <td>
                            <select name="doctor" id="doctor" onchange='get_week_detail()'>
                                
                                <?php
                                    $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id";                        
                                    $res = mysql_query($qry);
                                      $num=mysql_num_rows($res);
                                      if($num==1)
                                      {
                                    while ($data = mysql_fetch_array($res)){
                                        echo "<option value='".$data['doc_id']."'>".$data['doc_name']."</option>";                            
                                    }  
                                    }
                                    else
                                       {?>
                                <option value="" selected="selected" disabled>-----select doctor-----</option>
                                <option value="all"  >All</option>       
                                    <?php while ($data = mysql_fetch_array($res)){
                                        echo "<option value='".$data['doc_id']."'>".$data['doc_name']."</option>";                            
                                    }  
                                    }                     
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                
                <br />
                <div id="loading"></div>
                <div id="report">
                    
                </div>  
            </div>              
        </td>
     </tr>
</table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
</body></html>
