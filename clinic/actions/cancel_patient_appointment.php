<?php
ob_start();
date_default_timezone_set('Asia/Calcutta');
error_reporting(E_PARSE);
session_start();

if(!isset($_SESSION['username']) ||  $_SESSION['login']!='clinic'){
        header("Location: ../../index.php");
        exit();
}

include "../../db_connect.php";

include "send_mail.php";

$TOKEN = $_POST['token'];
$logo = "<img width='270' height='96' src='http://www.pinkwhalehealthcare.com/images/pinkwhale_logo.jpg'/>";
$ID = $_POST['appid'];
$clinic_name=$_POST['clinic_name'];
$clinic_address=$_POST['clinic_add'];
$clinic_city=$_POST['clinic_city'];
$clinic_state=$_POST['clini_state'];
$clinic_country=$_POST['clini_country'];
$clinic_pin=$_POST['clini_pin'];
$doc_name=$_POST['doc_name'];
$app_date=$_POST['app_date'];
//$app_date = strftime ( '%b %a %e', strtotime($appointment_date) );
$appointment_time=$_POST['app_time'];
$p_name=$_POST['p_name'];
$p_age=$_POST['p_age'];
$p_email=$_POST['p_email'];
$p_mob=$_POST['p_mobile'];
$currenttime = date("Y-m-d H:i:s",time());
$clinic_id = $_SESSION['clinic_id'];

$qry1 = "select status from Appointment_book_details where id='$ID' and token_id='$TOKEN'  and from_time > '$currenttime'";

$resq = mysql_query($qry1);

$num = mysql_num_rows($resq);

if($num>0){
    
    $data = mysql_fetch_array($resq);
    
    if($data[0]==2){

        $qry = "update Appointment_book_details set status='3' where id='$ID' and token_id='$TOKEN'";

        $res = mysql_query($qry);

        if($res){

	    $ip=$_SERVER['REMOTE_ADDR'];
            
            $qryt = "insert into appointment_activity_detaild(userid,act_description,act_time,address) values($clinic_id,'Cancelled Appointment, token No. :$TOKEN ',now(),'$ip')";
            
            mysql_query($qryt);            
            
            $details = "<div style='float:left;border:1px solid #BBBDC6;width:525px;padding:10px;font-family:arial;'>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>{$logo}</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:5px;'>Dear ".ucwords($p_name).",</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>Your Appointment Cancelled Details</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:2px;'><div style='float:left;width:18%;'>Doctor Name :</div> {$doc_name}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Appointment Time :</div>{$appointment_date} {$appointment_time}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:24%;'>Clinic Address :</div>{$clinic_name},{$clinic_address},{$clinic_city}-{$clinic_pin}<br/>{$clinic_state}<br />{$clinic_country}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Token-id :</div> {$TOKEN}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>patient Name :</div> {$p_name}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Age :</div> {$p_age}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Email :</div> {$p_email}</div>
                        <div style='float:left;width:95%;margin-top:2px;margin-left:25px;margin-bottom:10px;'><div style='float:left;width:18%;'>Phone No. :</div> {$p_mob}</div>
                        <div style='float:left;width:95%;margin-top:5px;margin-left:25px;margin-bottom:10px;'>                        
                        </div>
                        <div style='float:left;width:95%;margin-top:10px;margin-left:25px;margin-bottom:10px;'>
                        <b>Pinkwhale Healthcare</b>
                        </div>";
                        
                if(sendmail($p_email, $details, "Appointment Cancellation")){

                $url ="http://alerts.sinfini.com/api/web2sms.php?";
				$url .="username=pinkwhale&password=Pink@123";
				$url .="&to=".$p_mob."";
				$url .="&sender=PWCARE";            	
				$url .="&message=".str_replace(" ","%20",$p_name)."%2C%20ur%20Apt%20at%20".str_replace(" ","%20",$clinic_name)."%20with%20Dr.".str_replace(" ","%20",$doc_name)."%20on%20".str_replace(" ","%20",$app_date)."%20at%20".str_replace(" ","%20",$appointment_time)."%20Token".str_replace(" ","%20",$TOKEN)."%20stands%20cancelled%20as%20per%20your%20request";
			
            	$fetched = file_get_contents($url);            	 
            	$content = $fetched;
                }

            $_SESSION['msg'] ="Successfully Cancelled Appointment"; 
            header("Location: ../Cancel_patient_appoinment.php");
            exit;

        }else{

            $_SESSION['error'] ="Failed to Cancel Appointment";
            header("Location: ../Cancel_patient_appoinment.php");
            exit;

        }
        
    }else{
            $_SESSION['error'] ="Invalid Token No.";
            header("Location: ../Cancel_patient_appoinment.php");
            exit;
    }

}else{
            $_SESSION['error'] ="Invalid Token No.";
            header("Location: ../Cancel_patient_appoinment.php");
            exit;
}

ob_end_flush();

?>