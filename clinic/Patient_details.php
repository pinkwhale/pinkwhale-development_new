<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<link href="css/TableCSSCode.css" rel="stylesheet" type="text/css">
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!-- <div id="s90dashboardbg"  style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="205" height="20" />  -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 
include "../admin/get-who-booked-details.php";
$today_date = date("Y-m-d",time());
/*added by swathi  */
if(isset($_GET['doctor']))
{
	$doctor=$_GET['doctor'];
	$doctordetails = explode("|", $doctor);
	$docid = $doctordetails[0];
	
}
/*end added by swathi  */
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
</tr>
    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
            <form action="" method="GET" > 
            <table border="0" cellpadding="0" cellspacing="1"  align="center" >
                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <td width="30%" align="right" ><b>Doctor</b>:</td>
                    <td width="40%" align="left">
                        <select name="doctor" id="doctor" >
                            
                            <?php
                                $qry = "select p.doc_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id' and blocked<>'Y'  group by doc_id union select p.doc_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id' and blocked<>'Y'  group by doc_id";                        
                                echo $qry;
                                $res = mysql_query($qry);
                                 $num=mysql_num_rows($res);
                                  if($num==1)
                                  {
                                while ($data = mysql_fetch_array($res)){?>
                                    <option value="<?php echo $data['doc_id']."|".$data['doc_name'];?>" <?php  if($data['doc_id']==$docid) {echo "selected='selected'";}?>><?php echo $data['doc_name'];?></option>;                            
                                <?php }
                                }
                                 else
                                {?>
                                	<option value="" selected="selected" disabled>-----select doctor-----</option>
                                	<?php 
                                	while ($data = mysql_fetch_array($res)){?>
                                <option value="<?php echo $data['doc_id']."|".$data['doc_name'];?>" <?php  if($data['doc_id']==$docid) {echo "selected='selected'";}?>><?php echo $data['doc_name'];?></option>;
                                <?php }
                                }    
 
                            ?>
                        </select>
                    </td>
                    <td width="30%" align="right" ><input name="button" type="submit" value="Go" /></td>
                </tr>
                
            </table>
            </form>
            
            
            <?php
            
            if($_REQUEST['button']!='' && $_REQUEST['doctor']!=''){
                
                
                $doc = $_REQUEST['doctor'];
                $doctor_details = explode("|", $doc);
                $doc_id = $doctor_details[0];
                $doc_name = $doctor_details[1];
                
                
            
            
            include("../admin/includes/host_conf.php");
            include("../admin/includes/mysql.lib.php");
            include('../admin/includes/ps_pagination.php');
            $obj=new connect;
            ?>

		<table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='CSSTableGenerator'>
		<tr><th colspan="6" style="background:#C4C4C4;height:32px;"> Patient Details for Doctor : <?= $doc_name ?></th></tr>
		<?php	
			$count =0;			
			$sql="SELECT `patient_name`,`mobile`,`email` FROM `Appointment_book_details` WHERE doc_id=$doc_id and clinic_id=$clinic_id and new_patient=0 and patient_name<>''";
			
			$obj->query($sql);	
			
			$pager = new PS_Pagination($dbcnx, $sql, 20, 5,"param1=valu1&param2=value2&doctor=$doc&button=Go");
			$pager->setDebug(true);
			$rs = $pager->paginate();
			if(!$rs) 
			{
				echo htmlForNoRecords("700", true);
			} 
			else if(mysql_num_rows($rs) > 0)
			{
				?>
				<tr>
					<td align='left'><strong>SI No:</strong> </td>
					<td align='left'><strong>Patient Name</strong></td>
					<td align='left'><strong>Phone No.</strong></td>
					<td align='left'><strong>Email</strong></td>
				 </tr>
				<?php	
				while($row = mysql_fetch_assoc($rs)) 
				{
					$count ++;
					$user_dob="";
					$gender="";
					$age="";
						$user_id=$row['user_id'];
						$user_name=$row['patient_name'];
						$mail_id=$row['email'];
                                                $phone=$row['mobile'];
                                               
					
					$gdate1=strtotime($user_dob);
					$final_date1=date("d M Y", $gdate1);
					$age=$final_date1;
					echo "<tr bgcolor='#ffffff'>";
						echo "<td>$count</td> ";
						echo "<td>$user_name </td>"; 
						echo "<td>$phone</td> ";
						echo "<td>$mail_id</td> ";
					echo "</tr>";          			
				}
                        }else{
                            echo "<tr><td colspan=\"6\">No Records</td></tr>";
                        }
				
				$ps1 = $pager->renderFirst();
				$ps2 = $pager->renderPrev();
				$ps3 = $pager->renderNav('<span>','</span>');
				$ps4 = $pager->renderNext();
				$ps5 = $pager->renderLast();
				?>
				</table>
				<table>
            	<tr><td colspan='5' align='center'>
				<?php
                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr>"; 
            
		 
		?>
			</table>
            
            <?php
            }
            ?>
        </td></tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
    
    </body></html>
