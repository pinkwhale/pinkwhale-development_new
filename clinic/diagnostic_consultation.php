<?php
error_reporting(E_PARSE);
session_start();
include ("../includes/pw_db_connect.php");
if (!isset($_SESSION['username']) || $_SESSION['login'] != 'clinic') {
	header("Location: ../index.php");
	exit();
} else {
	$clinic_id = $_SESSION['clinic_id'];
	$clinic_name = $_SESSION['clinic_name'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script language="javascript" src="js/diagnostic_report_validation.js"></script>
<script src="../Scripts/jquery.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-23649814-1']);
                    _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
                    _gaq.push(['_trackPageview']);

                    (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();
  
                    

                </script>
<!--  --------------------------------------     END         -------------------------------------------------- -->
</head>
</head>
<body>
	<?php
	include 'header.php';
	require_once('calendar/tc_calendar.php');
	include ('../includes/pw_db_connect.php');
	?>

	<table width="1000" border="0" cellspacing="10" cellpadding="0"
		align="center" class="s90greybigbox">
		<tr>
			<td width="180" valign="top">
				<!-- ///// left menu //////  --> <?php
				include 'clinic_left_menu.php';

				$today_date = date("Y-m-d", time());
				?> <!-- ///// left menu //////  -->

			</td>
			<td width="748" valign="top" class="s90docphr">


				<table height="30" width="748" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td width="220"><h1>Dashboard</h1>
						</td>
						<td width="528" bgcolor="#f1f1f1" align="right">
							<div
								style="color: #EA0977; font-family: Arial; font-size: 16px; font-weight: bold; font-style: italic;">
								<?php echo $clinic_name; ?>
								, Pinkwhale ID
								<?php echo $clinic_id; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" class="clinicBg" height="500"
							valign="top"><br /> <?php

							if($_SESSION['success']!=""){
                               echo "<center><font color='green' size='3'>".$_SESSION['success']."</font></center>";
                               $_SESSION['success'] = "";
                           }
                           if($_SESSION['fail']!=""){
                               echo "<center><font color='red' size='3'>".$_SESSION['fail']."</font></center>";
                               $_SESSION['fail'] = "";
                           }
                           ?>

							<form enctype="multipart/form-data"
								action="actions/diagnostic_consultation.php" method="POST"
								name="upload_file_form" id="upload_file_form">
								<table border="0" cellpadding="0" cellspacing="1" width="600"
									align="center" bgcolor='#eeeeee' class="s90registerform">
									<tr>
										<th colspan="2">Diagnostic Consultation</th>
									</tr>
									<tr>
										<td colspan="2"><br />
										</td>
									</tr>

									<tr>
										<td width="40%" align="right">User ID #<font color="#FF0000">*</font>:</td>

										<td align="left" style="padding-left: 15px"><select
											style="width: 200px" name="users_id" id="users_id"
											class="registetextbox" onchange="display_id();">
												<option value=<?php $result['user_id'] ?>
													>--- Select User-ID # ---</option>

												<?php

												$qry = "SELECT * FROM `clinic_details` as a, `user_details` as b where a.clinic_id=b.parent_id and a.clinic_id=$clinic_id ";

												$qry_rslt = mysql_query($qry);

												while ($result = mysql_fetch_array($qry_rslt)) {

                                                            echo '<option value="' . $result['user_id'] . '">' . $result['user_email'] . '</option>';

                                                        }

                                                        ?>
										</select>
										</td>
									</tr>
									<tr>
										<td></td>

										<td align="left">

											<div id="display_user_id" class="error"
												style="color: #FF00FF; font-family: verdana; font-size: 13px; margin-left: -8px"></div>
										</td>

									</tr>

									<tr>

										<td align="center" colspan="2">OR</td>

									</tr>

									<tr>
										<td width="40%" align="right">Card No<font color="#FF0000">*</font>:</td>

										<td width="60%" align="left"><input size="20" type="text"
											maxlength="6" autocomplete="off"
											onkeypress="return isNumberKey(event);"
											onblur="return check_card();" " onkeyup="display_id();"
											name="card" id="card" value="" class="s90regformtext">
										</td>
									</tr>
									<!--    ERROR DIV -->
									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="cardErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>
									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="cardBalanceErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>

									<tr>
										<td width="40%" align="right">Doctor<font color="#FF0000">*</font>:
										</td>
										<td width="60%" align="left" style="padding-left: 15px"><select
											style="width: 200px" name="doctor" id="doctor"
											onchange="check_card_balance()">
												<option value="" selected="selected" disabled>-----select
													doctor-----</option>
												<?php
												$qry = "select p.doc_id,p.doc_name
												from pw_doctors p
												inner join clinic_doctors_details d on doctor_id=doc_id
												where clinic_id='$clinic_id' and blocked<>'Y' and activate_diagnostic_consultation = 'diagnostic'
												group by doc_id union
												select p.doc_id,p.doc_name
												from pw_doctors p
												inner join doctor_clinic_details d on doctor_id=doc_id
												where clinic_id='$clinic_id' and blocked<>'Y' and activate_diagnostic_consultation = 'diagnostic' group by doc_id";
												$res = mysql_query($qry);
												while ($data = mysql_fetch_array($res)){
					                                    echo "<option value='".$data['doc_id']."'>".$data['doc_name']."</option>";
					                                }
					                                ?>
										</select>
										</td>
									</tr>

									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="doctorErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>

									<tr>
										<td width="40%" align="right">Report Type<font color="#FF0000">*</font>:
										</td>
										<td width="60%" align="left" style="padding-left: 15px"><select
											style="width: 200px" name="reportType" id="reportType">
												<option value="">Select a report type</option>
												<option value="Physical examination">Physical examination</option>
								                <option value="Consultation report">Consultation report</option>
								                <option value="Operative report">Operative report</option>
								                <option value="Radiology report">Radiology report</option>
								                <option value="Pathology report">Pathology report</option>
								                <option value="Laboratory report">Laboratory report</option>
								                <option value="Emergency report">Emergency report</option>
								                <option value="Therapy report">Therapy report</option>
								                <option value="Clinical notes">Clinical notes</option>
								                <option value="Biopsy reports">Biopsy reports</option>
								                <option value="X-ray reports">X-ray reports</option>
								                <option value="Scan reports">Scan reports</option>
								                <option value="Scan reports">Combined reports</option>
								                <option value="Others">Others</option>
										</select>
										</td>
									</tr>

									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="reportTypeErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>

									<tr>
										<td width="40%" align="right">Report Name<font color="#FF0000">*</font>:
										</td>
										<td width="60%" align="left"><input size="20" type="text"
											maxlength="40" name="reportName" id="reportName" value=""
											class="s90regformtext">
										</td>
									</tr>

									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="reportNameErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>

									<tr>
										<td width="40%" align="right">Nature Of Concern<font color="#FF0000">*</font>:
										</td>
										<td width="60%" align="left"><input size="20" type="text"
											maxlength="40" name="natureOfConcern" id="natureOfConcern" value="Diagnostic Analysis" readonly
											class="s90regformtext">
										</td>
									</tr>

									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="natureOfConcernErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>
									
									<tr>
										<td colspan="2"><img src="images/blank.gif" width="1"
											height="6" alt="" border="0">
										</td>
									</tr>
									<tr>
										<td width="219" align="right">
											<div>Choose a file to upload :</div>
										</td>
										<td width="218"><input type="hidden" name="MAX_FILE_SIZE"
											value="20000000" /> 
											<input name="uploadedfile1" type="file" />
											<input name="uploadedfile2" type="file" />
											<input name="uploadedfile3" type="file" />
										</td>
									</tr>

									<tr>
										<td></td>
										<td align="left" height="8">
											<div id="fileSelectErrDiv" class="error"
												style="color: #F33; font-family: verdana; font-size: 10px; margin-left: 8px"></div>
										</td>
									</tr>
									
									<input type="hidden" name="clinic_id" id="clinic_id" value="<?php echo $clinic_id;?>" />	
									<tr>
										<td height="50" colspan="2" align="center"><input class="btn"
											type="button" name="btnSubmit" id="btnSubmit" value=" Save And Initiate Consult"
											onclick="form_validation(upload_file_form)">
										</td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	</td>
	</tr>
	<tr>
		<td colspan="4"><img src="images/blank.gif" width="1" height="12"
			alt="" border="0">
		</td>
	</tr>
	</table>
	</td>
	</tr>
	<?php
include 'footer.php'; ?>

	<script type="text/javascript">	

	function display_id(){
	 	var id = encodeURI(document.getElementById('users_id').value); 
	 	var id1= encodeURI(document.getElementById('card').value); 
	 	if(id!=""){
		document.getElementById("display_user_id").innerHTML= "Pinkwhale id :"+id; 
		document.getElementById("card").disabled = true; 
		} else
		if(id1!=""){ 
			document.getElementById("users_id").disabled = true; 
			}
	 }
	
	</script>
</body>
</html>

