<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<link href="css/TableCSSCode.css" rel="stylesheet" type="text/css">
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
//require('fpdf17/fpdf.php');
include ('../includes/pw_db_connect.php');
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >

<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
$mode=$_POST['mode'];
/*added by swathi  */


/*end added by swathi  */
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Consent Form </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
</tr>

<?php 

$qry = "select * from `clinic_details` where clinic_id='$clinic_id'";
$res = mysql_query($qry);
while ($data = mysql_fetch_array($res)){


?>



    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
        <div>
            <form action="actionpdf.php" method="post" enctype="multipart/formdata" name="consent" > 
            <table border="0" cellpadding="0" cellspacing="1"  align="center" >
                <tr><td colspan="2"><img src="../images/blank.gif" width="1" height="6" alt="" border="0"></td></tr>
                <tr>
                    <div width="10%" align="right" style="margin-right:50px;" >Address :<br/>
                    <?php echo $data ['address']; ?></div>
                </tr>
                <br/><br/>
                <tr>
                    <div align="right" style="margin-right:50px;" >Patient Clinic ID :&nbsp;<input type="text" name="number" value="" style="width:120px"/>
                   </div>
                </tr>
                
                <br/>
                <tr>
                    <div align="right" style="margin-right:50px;" >Date :&nbsp;<input type="text" name="date" value="" style="width:100px"/>
                   </div>
                </tr>
                
                <br/><br/>
                <tr>
                <p align="left" style="margin-left:30px; margin-right:50px;">I <input type="text" name="name1" value="" /> hereby authorize Mr/Mrs. <input type="text" name="name2" value=""/> to collect my lab report on my behalf. 
                The receipt for the same has been provided.<br/><br/> 
                 I also authorize <?php echo $data ['name']; ?> to upload a scanned copy of the above report onto my
                 pinkwhale account #<input type="text" name="userid" value="" style="width:50px;"/> and initiate a Diagnostic Report Consultation with Dr.<input type="text" name="doc" value=""/> .
                 </p>
                </tr>
                 <br/><br/>
                <tr>
                    <h3 align="right" style="margin-right:50px;">Bill Details</h3><br/>
                    <div align="right" style="margin-right:50px;">
                    Bill No. :&nbsp;<input type="text" name="bill_no" value="" /><br/>
                     Lab Name :&nbsp;<input type="text" name="lab" value=""/><br/>
                      Test's Taken :&nbsp;<input type="text" name="test" value=""/>
                      </div>
                </tr>
                <br/><br/>
                <tr>
                <div align="left" style="margin-left: 30px;">Your's Sincerely,<br/><br/>
                Name: <input type="text" name="name3" value=""/></div>
                
                </tr>
                
                <br/><br/>
                <tr>
                <div align="center">
                <input type="submit" value="Export to pdf" name="submit"/>
                </div>
                </tr>
                
            </table>
            </form>
            <?php }?>
            
            
        </td></tr>
        </div>
        </table>
        </td>
    </tr>
</table>


<?php
include 'footer.php'; ?>
    
    </body></html>
    

