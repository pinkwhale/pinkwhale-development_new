<?php
ob_start();
  	
?>
<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
	include ("../pwconstants.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>
#cssmenu{width: 100% !IMPORTANT;}
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>
<?php include 'header.php'; 
include("../admin/includes/host_conf.php");
include("../admin/includes/mysql.lib.php");
include('../admin/includes/ps_pagination.php');
$obj=new connect;

?>
<table width="1000" border="0" cellspacing="10" cellpadding="3" align="center" class="s90greybigbox">
<tr><td width="180" valign="top">
<!-- <div id="s90dashboardbg"  style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="../images/phr_dashboard_uline.jpg" width="205" height="20" />  -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; ?>
<!-- ///// left menu //////  -->

</td>

<td width="748" valign="top" class="s90docphr">
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220" ><h1></h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">                     
                    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                            <?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
                    </div>
                </td>
                
            </tr>
                <tr><td width="528" colspan="2"  align="left" class="clinicBg"  style="background-repeat: repeat-x;"  valign="top">
                    <?php
                    
                        $qry="select p.doc_id doctor_id,p.doc_name from pw_doctors p inner join clinic_doctors_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id union select p.doc_id doctor_id,p.doc_name from pw_doctors p inner join doctor_clinic_details d on doctor_id=doc_id where appoint_flag=1 and clinic_id='$clinic_id'  group by doc_id";
                        $obj->query($qry);
		                $pager = new PS_Pagination($dbcnx, $qry, 10, 1, "param1=valu1&param2=value2");
		                $pager->setDebug(true);
		                $rs = $pager->paginate();
		                if(!$rs) die(mysql_error());
                        $res = mysql_query($qry);
                        $cnt=mysql_num_rows($res);
                        if($cnt>0)
                        {
                                
                        ?>

                                <table width='812' border='1' cellpadding='0' cellspacing='0' bgcolor='#eeeeee' align='left' class='s90registerform'>
                                   <tr><th colspan="23">Doctors Schedule Summary</th></tr>
                                   <tr>

                                      			   <td rowspan="2" style="font-size:11px" align="left" bgcolor="#F5F5F5" ><b>Name</b></td>
                                      			   <td rowspan="2" style="font-size:11px" align="left" bgcolor="#F5F5F5" ><b>CTRY</b></td>
                                        <!--       <td rowspan="2" colspan="2" style="font-size:11px" align="left" bgcolor="#F5F5F5" ><b>Id</b></td>-->
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Mon</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Tue</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Wed</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Thu</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Fri</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Sat</b></td>
                                                   <td colspan="2"  style="font-size:11px" align="center" bgcolor="#F5F5F5" ><b>Sun</b></td>

                                  			 </tr>
                                  			 <tr>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                                        <td  style="font-size:11px;color: #EA0977;" align="center" bgcolor="#F5F5F5" ><b>From</b></td><td  style="font-size:11px;color: blue;" align="left" bgcolor="#F5F5F5" ><b>To</b></td>
                                           <tr>	

                                           </tr>
                        <?php
                        $count=0;
                        while($data = mysql_fetch_array($rs))
                        {
                              $count +=1;
                              $clinic_avail_details="";
                              
                               $qry_1="select doc_name from  pw_doctors where doc_id=".$data['doctor_id'] ."";
                               $res_1 = mysql_query($qry_1);
                               $data_1 = mysql_fetch_array($res_1);

                               echo "<tr>"; 
                               echo "<td rowspan='5' width='124px' align='left' bgcolor='#ffffff'  style='font-size:10px;color: black;font-weight:bold;' >".$data_1['doc_name']."</td>";
                               
                                foreach ($categories_short as $key => $value) {
								if($key!=3){
                               	echo "<td width='80px' align='left' style='font-size:9px;color:black;' bgcolor='#ffffff' >".$value."</td>";
                               	 
                               	for($i=1;$i<8;$i++)
                               	{
                               				 $doc_avail_slot="SELECT LOWER(from_time) as from_time,LOWER(to_time) as to_time,apt_category,patient_apt_type FROM doctor_clinic_avail_time_slots where clinic_id=".$clinic_id." and doc_id=".$data['doctor_id']." and day=".$i." and  patient_apt_type='$key' and apt_category='2' order by day_type ASC";
                                             
                                             $res_doc_avail = mysql_query($doc_avail_slot);
                                             $num_row=mysql_num_rows($res_doc_avail);
                                             
                                             if($num_row>0){
													display_slot($res_doc_avail);
                                             }else{
                                                     echo "<td width='70px' align='left' style='font-size:9px;color: #EA0977;font-weight:bold;' bgcolor='#ffffff' >&nbsp;</td>";
                                                     echo "<td width='70px' align='left' style='font-size:9px;color: blue;font-weight:normal;border-bottom:0px;' bgcolor='#ffffff'>&nbsp;</td>";
                                             }
                               	}
                               	echo "</tr>";
                               	}
                               	}
                               foreach ($patient_Types as $key => $value) {
                               echo "<td width='80px' align='left' style='font-size:9px;color:black;' bgcolor='#ffffff' >".$value."</td>";
                               
                               for($i=1;$i<8;$i++)
                               {    
                                   
                              				 $doc_avail_slot="SELECT LOWER(from_time) as from_time,LOWER(to_time) as to_time,apt_category,patient_apt_type FROM doctor_clinic_avail_time_slots where clinic_id=".$clinic_id." and doc_id=".$data['doctor_id']." and day=".$i." and  patient_apt_type='$key' and apt_category='3' order by day_type ASC";
                                             
                                             $res_doc_avail = mysql_query($doc_avail_slot);
                                             $num_row=mysql_num_rows($res_doc_avail);
                                             
                                             if($num_row>0){
													display_slot($res_doc_avail);
                                             }else{
                                                     echo "<td width='70px' align='left' style='font-size:9px;color: #EA0977;font-weight:bold;' bgcolor='#ffffff' >&nbsp;</td>";
                                                     echo "<td width='70px' align='left' style='font-size:9px;color: blue;font-weight:normal;border-bottom:0px;' bgcolor='#ffffff'>&nbsp;</td>";
                                             }
                               }
                               
                               echo "</tr>";
                              }                                
                         }
                         $ps1 = $pager->renderFirst();
						 $ps2 = $pager->renderPrev();
						 $ps3 = $pager->renderNav('<span>','</span>');
						 $ps4 = $pager->renderNext();
						 $ps5 = $pager->renderLast();
                 ?>
            	<tr>
            	<td colspan='23' align='center'>
				<?php
                                echo "$ps1";
                echo "$ps2";
                echo "$ps3";
                echo "$ps4";
                echo "$ps5";
                echo "</td></tr>"; 
                echo "</table>";
                }else
                        {
                            echo "<br /><center>No Data</center><br />";
                        }

                    ?>
                </td></tr>
        </table>

    </td></tr>
</table>
    
      </td></tr>
</table>  
<?php
include 'footer.php'; ?>
</body></html>
