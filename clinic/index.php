<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Online Doctor/Medical Consultation | find online medical advice and consult | ask a doctor for help</title>

<meta name="keywords" content="second opinion, doctors, find health specialists, online doctor, telephone counselling, call a counsellor, referral doctors"/>

<meta name="description" content="pinkWhale Healthcare Services is an e-Health company offering integrated online and telephone based personal healthcare services. Our goal is to give patients and businesses quick, easy, and convenient access to the best health care from wherever they are, using a state-of-the-art technology platform.  We are building the best network of health specialists, healthcare providers, and partners who are committed to providing health consultations by phone or online."/>

<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!--  --------------------------------------     END         -------------------------------------------------- --></head>
<body>

</body>
    


</body></html>