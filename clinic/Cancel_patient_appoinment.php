<?php 
	error_reporting(E_PARSE); 
        date_default_timezone_set('Asia/Calcutta');
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="js/jquery.easy-confirm-dialog.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                
                
   function confirm_cancel(form){
       $( "#dialog-confrm-cancel" ).dialog( "open" );
       $(function() {
           $( "#dialog-confrm-cancel" ).dialog({
                    resizable: false,			
                    modal: true,
                    buttons: {
                            "Cancel Appointment": function() {
                                document.getElementById("cancel_pat_app").submit();
                            },
                            No: function() {
                                    $( this ).dialog( "close" );
                                    
                            }
                    }
            }); 
        });
	
   }             

</script>
<style type="text/css">
	.ui-dialog { font-size: 11px; }
	body {
		font-family: Tahoma;
		font-size: 12px;
	}
	#question {
		width: 300px!important;
		height: 60px!important;
		padding: 10px 0 0 10px;
	}
	#question img {
		float: left;
	}
	#question span {
		float: left;
		margin: 20px 0 0 10px;
	}
        .ui-widget-header { border: 1px solid #01DFD7; background: #01DFD7 url(images/ui-bg_highlight-soft_15_#01DFD7_1x100.png) 50% 50% repeat-x; color: #ffffff; font-weight: bold; }
</style>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
?>'
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >
<!-- <div id="s90dashboardbg"  style="width: 205px;">
    <img src="images/dots.gif" />
    <a href="clinic_admin.php"><b>Dashboard</b></a>
</div>
<img src="images/phr_dashboard_uline.jpg" width="205" height="20" />  -->
<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="300"><h1>Cancel Patient Appointment</h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
    </tr>
    <tr>
        <td colspan="2" align="center" class="clinicBg" height="500" valign="top">
              <?php
                if($_SESSION['msg']!=""){
                    echo "<div class='messageAlert'>".$_SESSION['msg']."</div>";
                    $_SESSION['msg'] = "";
                }else{
                    if($_SESSION['error']!=""){
                            echo "<center><font color='red' size='3'>".$_SESSION['error']."</font></center><br />";
                            $_SESSION['error'] = "";
                    }
              ?>
                    
            <form action="" method="POST">
                <table  border="0" cellpadding="0" cellspacing="1"  align="center" class="s90registerform">                
                    <tr>
                        <td>
                            <b>Token-id</b>&nbsp;:&nbsp;
                        </td>
                        <td>
                            <input type="text" size="6" maxlength="4" name="token" id="token" />
                        </td>
                        <td>
                            <input type="Submit" name="Go" value="Go" />
                        </td>
                    </tr>

                </table>
            </form> 
            
            
            <?php
            
            if($_POST['Go']!="" && $_POST['token']!=""){
                
                $token = $_POST['token'];
                
                $currenttime = date("Y-m-d H:i:s",time());
                
                $qry = "select a.status app_status,a.id,p.doc_id,a.doc_id,doc_name,a.clinic_id,c.clinic_id,c.name,patient_name,a.email,mobile,gender,age,DATE_FORMAT(from_time,'%D %M %Y') dd,DATE_FORMAT(from_time,'%h:%i %p') as time,c.address,c.country,c.state,c.pin_zip,c.city from pw_doctors p,Appointment_book_details a,clinic_details c where p.doc_id=a.doc_id and a.clinic_id=c.clinic_id and token_id=$token and a.clinic_id=$clinic_id and a.status in (2,3) and a.from_time>'$currenttime' ";
    
                $res = mysql_query($qry);

                $num = mysql_num_rows($res);

                if($num>0){
                    
                    $disp .= "<form action='actions/cancel_patient_appointment.php' name='cancel_pat_app' id='cancel_pat_app' method='POST'>";

                    $disp .= "<table border='0' cellpadding='0' cellspacing='1'  align='center' class='s90registerform' width='400'><tr><th colspan='2'>Token No : $token </th></tr>";

                    if($data = mysql_fetch_array($res)){

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\" colspan=2><b>Appointment Details :</b></td></tr>";
                        
                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Clinic Name</td><td align=\"left\" bgcolor=\"#F5F5F5\">".$data['name']."<br />".$data['address']."<br />".$data['city']."-".$data['pin_zip']."<br />".$data['state']."<br />".$data['country']."";
                        
                        $disp .="<input type='hidden' name='clinic_name' value='".$data['name']."' />";
                        
                        $disp .="<input type='hidden' name='clinic_add' value='".$data['address']."' />";
                        
                        $disp .="<input type='hidden' name='clinic_city' value='".$data['city']."' />";
                        
                        $disp .="<input type='hidden' name='clini_pin' value='".$data['pin_zip']."' />";
                        
                        $disp .="<input type='hidden' name='clini_state' value='".$data['state']."' />";
                        
                        $disp .="<input type='hidden' name='clini_country' value='".$data['country']."' />";
                        
                        $disp .="</td></tr>";
                        
                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Doctor Name</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='doc_name' value='".$data['doc_name']."' >".$data['doc_name']."</td></tr>";

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Appointment Date</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='app_date' value='".$data['dd']."' />".$data['dd']."</td></tr>";
                        
                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Appointment Time</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='app_time' value='".$data['time']."' />".$data['time']."</td></tr>";

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\" colspan=2><b>Patient Details :</b></td></tr>";
                        
                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Name</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='p_name' value='".$data['patient_name']."' />".$data['patient_name']."</td></tr>";

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Age</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='p_age' value='".$data['age']."' />".$data['age']."</td></tr>";

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Email</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='p_email' value='".$data['email']."' />".$data['email']."</td></tr>";

                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\">Mobile</td><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='p_mobile' value='".$data['mobile']."' />".$data['mobile']."</td></tr>";            
                        
                        $disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\"><input type='hidden' name='appid' id='appid' value='".$data['id']."' /><input type='hidden' name='token' id='token' value='$token' /></td></tr>";            

			$disp .="<tr><td align=\"left\" bgcolor=\"#F5F5F5\"><b>Booking Status :</b></td>";                                               
                        if($data['app_status']==2){
                            
                            $disp .="<td align=\"left\" bgcolor=\"#F5F5F5\">Confirmed</td></tr>";            

                            $disp .="<tr><td colspan='2' align=\"center\" bgcolor=\"#F5F5F5\"><input type='button' name=go value='Cancel Appointment' onclick='confirm_cancel(cancel_pat_app)'></td></tr>";

                        }else if($data['app_status']==3){

                            $disp .="<td align=\"left\" bgcolor=\"#F5F5F5\">Cancelled</td></tr>";

                        }
                            
                            
                            
                    }

                     $disp .="</table></form>";
                     

                     echo $disp;

                }else{

                    echo "<center> Invalid Token</center>";

                }
                
            }
            
            
           }
            
            ?>
                
                
            
        </td>
     </tr>
</table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
<div id="dialog-confrm-cancel" title="Cancel Patient Appointment ?" style="display:none;">
        <?php echo "Dear Admin"; ?>,<p> Are you sure ?</p>
</div>    
    </body></html>
