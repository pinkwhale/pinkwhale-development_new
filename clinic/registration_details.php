<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
	$_GET['search']=false;
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="css/TableCSSCode.css" rel="stylesheet" type="text/css">
<script src="../Scripts/jquery.js" type="text/javascript"></script>

<script type="text/javascript" src="js/add_package.js"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php //include '../includes/header.php';
include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');

include("includes/host_conf.php");
include("includes/mysql.lib.php");
include('includes/ps_pagination.php');
$obj=new connect;

include "../admin/get-who-booked-details.php";
$name=@$_GET['search_query_name'];
$email=@$_GET['search_query_email'];
$card=@$_GET['search_query_card'];
?>



<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >

<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Dashboard </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <!--div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>		
    </div-->
    </td>
</tr>
    
    
<!-- added by swathi for searching patients based on name,email and card number on 18-03-2014 -->
<tr>
<td colspan="2">
<form action="registration_details.php" method="get" name="search-patient">	
<table style="margin: 15px;">
	<tr>
	 <td style="padding-left:10px;">
	   <label style="font-weight: bold;">Enter the name</label>
	      <input type="text" name="search_query_name" id="search_query_name" value="<?php echo $name;?>" placeholder="Search Name here" size="30" style="margin-bottom: 0px;"/>
	  </td>
	  <td style="padding-left:10px;">
         <label style="font-weight: bold;">Enter the email id</label>
          <input type="text" name="search_query_email" id="search_query_email" value="<?php echo $email;?>" placeholder="Search Email id here" size="30" style="margin-bottom: 0px;"/>
		 </td>
		 	
				
		<td style="padding-left:10px;">	   
        	<label style="font-weight: bold;">Enter Card Number </label>
	         <input type="text" name="search_query_card" id="search_query_card" value="<?php echo $card;?>" placeholder="Search card number here" size="30" style="margin-bottom: 0px;"/>  			  
			</td>
		<td style="padding-left:10px;">
		<input type="text" value="true" style="display:none;" name="search">
			<input type="submit" value="Search" id="button_find" name="button_find" class="Manually"  />
		</td></tr>
	 </table>
</form>
</td>
</tr>
<?php 
if(isset($_GET['search']))
			{				
				if($_GET['search_query_name']=="")
				{
					$name="";					
				}
				else
					$name=$_GET['search_query_name'];
				
				if($_GET['search_query_email']=="")
                {
					$email="";
				}
				else 
					$email=$_GET['search_query_email'];

				if($_GET['search_query_card']=="")
				{
					$card="";
				}
				else				
				$card=$_GET['search_query_card'];
			
								
			 $sql="select * from user_details u inner join clinic_details c on parent_id=clinic_id where user_name like '%$name%' and user_email like '%$email%' and CONVERT(user_id, CHAR(20)) like '%$card%' and parent_id='$clinic_id'";
				$se_re=mysql_query($sql);
				$result=mysql_fetch_array($se_re);
				$num_results = mysql_num_rows($se_re);
				if ($num_results > 0){
					$obj->query($sql);
					$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "search=true&search_query_name=".$name."&search_query_email=".$email."&search_query_card=".$card."");
					$pager->setDebug(true);
					$rs = $pager->paginate();
				}
			}

else{
echo $sql="select date_format(datetime,'%d-%m-%Y') datetime,blocked,parent_id,user_id,user_name,age,gender,user_email,mobile_num,u.grp_selected from user_details u inner join clinic_details c on parent_id=clinic_id where user_email<>'' and parent_id='$clinic_id' order by id desc";
$obj->query($sql);
	
$pager = new PS_Pagination($dbcnx, $sql, 20, 5, "param1=valu1&param2=value2");
$pager->setDebug(true);
$rs = $pager->paginate();}
?>

<!-- end added by swathi for searching patients based on name,email and card number on 18-03-2014 -->

    <tr>
        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
            <div id="clinic_dash">
                <table width='700' border='0' cellpadding='0' cellspacing='1' bgcolor='#eeeeee' align='center' class='CSSTableGenerator'>
		<tr><th colspan="10" style="background:#C4C4C4;height:32px;">Registration Details</th></tr>
                <?php		
                    
		
            
			if(!$rs) 
			{
			?>
		<tr><td colspan="10" align="center"><b>No Records</b></td></tr>
                <?php }
		else {  ?>
			<tr>
                <td align='center' width="45"><strong>SI No.</strong></td>
                <td align='center' width="65"><strong>Card No.</strong></td>
                <td align='center' width="110"><strong>Name</strong></td>
                <!--td align='center' width="110"><strong>Email</strong></td>
                <td align='center' width="100"><strong>Phone Number</strong></td-->
                <td align='center' width="100"><strong>Gender</strong></td>
                <td align='center' width="110"><strong>Age</strong></td>
            </tr>
	
			<?php	
			$count =0;		
			while($row = mysql_fetch_assoc($rs)) 
			{
				$count++;
				$date = $row['datetime'];
                                $name = $row['user_name'];
                                $pinkcard = $row['user_id'];
                                //$user_email = $row['user_email'];
                                //$mobile = $row['mobile_num'];
                                $age = $row['age'];
                                $gender = $row['gender'];
                                if($gender=="M"){
                                    $gender = "Male";
                                }else if($gender=="F"){
                                    $gender = "Female";
                                }
                                $blocked = $row['blocked'];
                                
                                if($blocked!="Y"){
                                    echo "<tr bgcolor='#ffffff' style='color:#000000;' >";
                                    echo "<td align='center' >$count </td> ";				
                                    echo "<td align='center' >$pinkcard </td> ";
                                    echo "<td align='center' >$name </td> ";				
                                    //echo "<td align='center' >$user_email </td> ";
                                   // echo "<td align='center' >$mobile </td> ";
                                    echo "<td align='center' >$gender</td> ";
                                    echo "<td align='center' >$age</td> ";
                                    echo "</tr>";
                                }else{
                                    echo "<tr bgcolor='#ffffff' style='color:#000000;' >";
                                    echo "<td align='center' ><font color=red>$count </font></td> ";				
                                    echo "<td align='center' ><font color=red>$pinkcard </font></td> ";
                                    echo "<td align='center' ><font color=red>$name </font></td> ";				
                                    //echo "<td align='center' ><font color=red>$user_email </font></td> ";
                                   // echo "<td align='center' ><font color=red>$mobile </font></td> ";
                                    echo "<td align='center' ><font color=red>$gender</font></td> ";
                                    echo "<td align='center' ><font color=red>$age</font></td> ";
                                    echo "</tr>";
                                }
				
				
                                
			
			}
			
			$ps1 = $pager->renderFirst();
			$ps2 = $pager->renderPrev();
			$ps3 = $pager->renderNav('<span>', '</span>');
			$ps4 = $pager->renderNext();
			$ps5 = $pager->renderLast();
			?>
			</table>
	                <table>
			<tr><td colspan='10' align='center'>
            <?php
            echo "$ps1";
			echo "$ps2";
			echo "$ps3";
			echo "$ps4";
			echo "$ps5";
			echo "</td></tr>"; 
			?>
            <?php } 
            ?>
        </table>    
                        
              </div>
        <?php
  if($count != 0){
  ?>
  <!--table align="right" style="margin-right: 50px;">
   <tr><td><a href="export-patient-details.php?sort=<?php echo $clinic_id; ?>&name=<?php echo $_GET['search_query_name']; ?>&email=<?php echo $_GET['search_query_email'];?>&card=<?php echo $_GET['search_query_card'];?>"><span style="font-size: 14px;">Export Table </span><img src="images/excelLogo.gif" /></a> </td></tr>     
  </table-->
  <?php
  }
  ?>
        
        
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
</body>
</html>

