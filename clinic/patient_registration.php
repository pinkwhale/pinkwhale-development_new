<?php
error_reporting(E_PARSE);
session_start();
include ("../includes/pw_db_connect.php");
if (!isset($_SESSION['username']) || $_SESSION['login'] != 'clinic') {
    header("Location: ../index.php");
    exit();
} else {
    $clinic_id = $_SESSION['clinic_id'];
    $clinic_name = $_SESSION['clinic_name'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>pinkwhalehealthcare</title>
        <meta name="description" content="pinkwhalehealthcare">
            <link href="css/designstyles.css" rel="stylesheet" type="text/css">
                <link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
                <script language="javascript" src="calendar/calendar.js"></script>
                <script language="javascript" src="js/registration.js"></script>
                <script src="../Scripts/jquery.js" type="text/javascript"></script>
                <!-- ------------------------------   google analytics    ------------------------------------------- -->
                <script type="text/javascript">

                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-23649814-1']);
                    _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
                    _gaq.push(['_trackPageview']);

                    (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();
  
                    

                </script>
                <!--  --------------------------------------     END         -------------------------------------------------- --></head>
                </head>
                <body>
<?php
include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
include "../admin/get-who-booked-details.php";
?>
                    
    <table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
        <tr>
            <td width="180" valign="top" >
               
                <!-- ///// left menu //////  -->
                <?php
                include 'clinic_left_menu.php';

                $today_date = date("Y-m-d", time());
                ?>
                <!-- ///// left menu //////  -->

            </td>
            <td width="748" valign="top" class="s90docphr"  > 
                
                
                <table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="220"><h1>Dashboard </h1></td>
                        <td width="528" bgcolor="#f1f1f1" align="right">    	
                            <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
                                <?php echo $clinic_name; ?>, Pinkwhale ID <?php echo $clinic_id; ?>		
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="2" align="center" class="clinicBg" height="500" valign="top">
                            
                             <br />

                           <?php

                           if($_SESSION['success']!=""){
                               echo "<center>".$_SESSION['success']."</center>";
                               $_SESSION['success'] = "";
                           }
                           if($_SESSION['fail']!=""){
                               echo "<ceneter>".$_SESSION['fail']."</center>";
                               $_SESSION['fail'] = "";
                           }
                           ?>
                            
                            <form name="reg_form" id="reg_form" action="actions/patient_registration_action.php" method="post">
                                <table border="0" cellpadding="0" cellspacing="1" width="600" align="center" bgcolor='#eeeeee' class="s90registerform">
                                    <tr><th colspan="2">Patient Registration</th></tr>
                                    <tr><td colspan="2"><br /></td></tr>
									<tr>	
                                        <td >Doctor<font color="#FF0000">*</font>:
									<select name="spe_doctor" id="spe_doctor" onchange="changeSpeDiv(this.value)">
									<option value="" selected="selected" disabled>- Select Doctor -</option> 
										<?PHP											
										$qry10 = "select a.doc_name, a.doc_id,a.activate_tele_Consultation,a.activate_online_consultation from pw_doctors as a, clinic_details as b, 
										clinic_doctors_details as d where a.doc_id=d.doctor_id 
										and b.clinic_id=d.clinic_id and b.clinic_id='$clinic_id'";										
										$qry_rslt10 = mysql_query($qry10);
										while($result10 = mysql_fetch_array($qry_rslt10))
										{
									?>	
									<option value="<?PHP echo $result10['doc_id']."/".$clinic_id."/".$result10['activate_tele_Consultation']."/".$result10['activate_online_consultation']; ?>"><?PHP echo $result10['doc_name']; ?></option>
										<?PHP } ?>
								</select>
									</td>
									
                                    </tr><br/>
									<tr>
									<td>
									<div id="cards_div">
									</div></td>
									</tr>
									<!--tr>	
                                        <td >Card Nos<font color="#FF0000">*</font>:
									<select name="regcard" id="regcard">
									<option value="" selected="selected" disabled>- Select Card -</option>
										<?PHP										
										
										/*
										
										$qry10="select a.card_num from pw_cards as a, user_details as c, pw_doctors as b  where
										a.card_num=c.user_id and a.spe_doctor_id=b.doc_id and c.parent_id='' and a.card_group=c.grp_selected $extend
										group by a.card_num ";
										$qry_rslt10 = mysql_query($qry10);
										while($result10 = mysql_fetch_array($qry_rslt10))
										{
									?>	
									<option value="<?PHP echo $result10['card_num']; ?>"><?PHP echo $result10['card_num']; ?></option>
										<?PHP } */?>
								</select>
									</td>
									
                                    </tr-->
									
                                    <!--tr>	
                                        <td width="40%" align="right">Card No.<font color="#FF0000">*</font>:</td>
                                        <td width="60%" align="left">
                                            <input size="20" type="text" maxlength="6" autocomplete="off" onkeypress="return isNumberKey(event);" onblur="return check_card();" name="regcar" id="regcard" value="" class="s90regformtext">
                                        </td>
                                    </tr-->
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV -->
                                    <tr>	
                                        <td width="40%" align="right">Your Name<font color="#FF0000">*</font>:</td>
                                        <td width="60%" align="left">
                                            <input size="20" type="text" maxlength="40" name="regname" id="regname" value="" class="s90regformtext">
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV --> 
                                    <tr>	
                                        <td width="40%" align="right">Age<font color="#FF0000">*</font>:</td>
                                        <td width="60%" align="left">
                                            <input size="20" type="text" maxlength="2" onkeypress="return isNumberKey(event);" name="regage" id="regage" value="" class="s90regformtext">
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV -->
                                    <tr>	
                                        <td width="40%" align="right">Gender<font color="#FF0000">*</font>:</td>
                                        <td width="60%" align="left">
                                            <select name="reggender" id="reggender">
                                                <option value="">Select Gender</option>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV --> 
                                    <tr>
                                        <td align="right">Your Email<font color="#FF0000">*</font>:<span onmouseover="showsummary('details', event, 'To be used as User ID');this.style.cursor='pointer';" onmouseout="hideSummary('details');" onmousemove="moveSummary('details', event);" class="pwinfohelp"></span></td>
                                        <td nowrap="nowrap">
                                            <input class="s90regformtext" type="text" autocomplete="off" maxlength="45" size="20" onblur="check_email()" id="regemail" name="regemail" value="" >
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV --> 
                                    <tr>
                                        <td align="right">Password<font color="#FF0000">*</font>:</td>
                                        <td nowrap="nowrap">
                                            <input class="s90regformtext" type="password" autocomplete="off" maxlength="45" size="20" id="regpassword" name="regpassword" value="" >
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="pwdErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV -->
                                    <tr>
                                        <td align="right">Re-Type Password<font color="#FF0000">*</font>:</td>
                                        <td nowrap="nowrap">
                                            <input class="s90regformtext" type="password" autocomplete="off" maxlength="45" size="20" id="regrepassword" name="regrepassword" value="" >
                                        </td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="repwdErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV --> 
                                    <tr>
                                        <td align="right" style="line-height:20px;">Mobile #<font color="#FF0000">*</font>:</td>
                                        <td style="line-height:30px;">
                                            <input type="text" class="s90regformtext" maxlength="20" onblur="check_mobile()" name="regPhone1" id="regPhone1"  value="" onkeypress="return isNumberKey(event);"></td>
                                    </tr>
                                    <!--    ERROR DIV -->
                                    <tr>
                                        <td> </td>
                                        <td  align="left" height="8">
                                            <div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;margin-left:8px"></div>
                                        </td>
                                    </tr>
                                    <!--  END ERROR DIV -->
                                    <tr>
                                        <td height="50" colspan="2" align="center" >
                                            <input class="btn" type="button" name="btnSubmit" id="btnSubmit" value=" Register " onclick="pink_register(reg_form)" >
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php include 'footer.php'; ?>

</body></html>

