function form_validation(form)
{
	Validated = true;
	document.getElementById("cardErrDiv").innerHTML = "";
	document.getElementById("doctorErrDiv").innerHTML = "";
	document.getElementById("reportTypeErrDiv").innerHTML = "";
	document.getElementById("fileSelectErrDiv").innerHTML = ""; 
	document.getElementById("reportNameErrDiv").innerHTML = ""; 

	var card = form.card.value;
	var userId = form.users_id.value;
	var doctor = form.doctor.value;
	var reportType = form.reportType.value;
	var reportName = form.reportName.value;
	var natureOfConcern = form.natureOfConcern.value;
	var uploadedfile1 = form.uploadedfile1.value;
	var uploadedfile2 = form.uploadedfile2.value;
	var uploadedfile3 = form.uploadedfile3.value;

	if ( card =='' && userId == '')
	{

		document.getElementById("cardErrDiv").innerHTML = "Select a user ID or enter the card number.";
		Validated = false;
	}

	if ( doctor =='' )
	{

		document.getElementById("doctorErrDiv").innerHTML = "Select a doctor.";
		Validated = false;
	}
	
	if ( reportType =='' )
	{

		document.getElementById("reportTypeErrDiv").innerHTML = "Report type cannot be blank";
		Validated = false;
	}
	if ( uploadedfile1 =='' && uploadedfile2=='' && uploadedfile3=='')
	{

		document.getElementById("fileSelectErrDiv").innerHTML = "Upload a file";
		Validated = false;
	}
	
	if ( reportName =='' )
	{

		document.getElementById("reportNameErrDiv").innerHTML = "Report name cannot be blank";
		Validated = false;
	}
	
	if ( natureOfConcern =='' )
	{

		document.getElementById("natureOfConcernErrDiv").innerHTML = "Nature of concern cannot be blank";
		Validated = false;
	}
	
	if(Validated)
	{
                
                document.upload_file_form.submit();
                
	}
		
	else
	{                
		return false;
	}

}


function check_card_balance(){
	var user_id = document.getElementById("users_id").value;
	var card = document.getElementById("card").value;
	if(user_id){
		card = document.getElementById("users_id").value;
	}
	var doctor = document.getElementById("doctor").value;
	card = encodeURI(card);

	if(card!="" && card!=null){
		var paramString = 'card='+card+'&doctor='+doctor;

		$.ajax({  
			type: "POST",  
			url: "check_diagnostic_balance.php",  
			data: paramString,  
			success: function(response) { 
				if(response==true){
					
				}else{
                     document.getElementById("cardBalanceErrDiv").innerHTML = response;
				}
			}
		});
	}
}


function check_card(){
    
    var card = document.getElementById("card").value;
    card = encodeURI(card);
    
    if(card!="" && card!=null){
        var paramString = 'card='+card+'&type=1';
        
        $.ajax({  
                type: "POST",  
                url: "check_card_diagnostic.php",  
                data: paramString,  
                success: function(response) { 
                    if(response==true){
                    var color = "#008000";
                        document.getElementById("cardErrDiv").innerHTML =  "<font color="+color+">Valid card No.</font>";
                    }else{
                        document.getElementById("cardErrDiv").innerHTML = response;
                    }
                }
        });
    }
}

function isNumberKey(evt)
{

	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}
