var doctor = false;
var date = false;
var ttime = false;

function clinic_cancel_doc_appointment(form){
    doctor = true;
    date = true;
    ttime = true;
    
    document.getElementById("doctorErrDiv").innerHTML= "";
    document.getElementById("calErrDiv").innerHTML = "";
    document.getElementById("doc_timeErrDiv").innerHTML= "";
    
    if(document.getElementById("doctor").value==""){
        document.getElementById("doctorErrDiv").innerHTML= "Please select Doctor";        
        doctor = false;
    }
    
    
    
    if(document.getElementById("date2").value==""){
        document.getElementById("calErrDiv").innerHTML= "Please select Date";        
        doctor = false;
    }
    
    
    var d_time = 0;
    for (var i=0; i < form.time_slot.length; i++){
        if (form.time_slot[i].checked){
            var rad_val = form.time_slot[i].value;            
            d_time = d_time + 1;                      
        }
    }   
    
    if(d_time==0){
        document.getElementById("doc_timeErrDiv").innerHTML= "Please select time slots";
        ttime = false;
    }
    
    
    if(doctor && date && ttime){
        document.getElementById('dialog-confrm-cancel').style="block";
        $(function() {
                          
                $( "#dialog-confrm-cancel" ).dialog({
                        resizable: false,                   
                        modal: true,
                        buttons: {
                                "Yes": function() {
                                      document.getElementById('doctor_appointment_cancel').submit();
                                },
                                No: function() {
                                    $( "#dialog-confrm-cancel" ).dialog( "close" );    
                                }
                        }
                });
        });
        
    }else{
        return false;
    }
}
