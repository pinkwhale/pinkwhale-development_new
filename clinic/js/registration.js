/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function changeSpeDiv(str)
{
        var doc_id = document.getElementById("spe_doctor").value;
            
        if(doc_id!=""){

            document.getElementById("spe_doctor").disabled=true;
           // document.getElementById("specialist_div").innerHTML="<img src='../images/Bar-loader.gif'><br />loading</img>";

            var paramString = 'q='+doc_id;
            //alert(paramString);
            $.ajax({
                    type: "POST",
                    url:  "doc_clinic_card_num.php",
                    data: paramString,
                    success: function(response) {
                           // alert(response);
                            document.getElementById("cards_div").innerHTML=response;
                            document.getElementById("spe_doctor").disabled=false;

                    }

            });

        }

}


function pink_register(form)
{
        
	Validated = true;
        // document.getElementById("docErrDiv").innerHTML = "";
        document.getElementById("cardErrDiv").innerHTML = "";
        document.getElementById("nameErrDiv").innerHTML = "";
        document.getElementById("ageErrDiv").innerHTML = "";
        document.getElementById("genderErrDiv").innerHTML = "";
        document.getElementById("emailErrDiv").innerHTML = ""; 
        document.getElementById("mobileErrDiv").innerHTML = ""; 
        document.getElementById("pwdErrDiv").innerHTML = ""; 
        document.getElementById("repwdErrDiv").innerHTML = "";
		
        
        var card = form.regcard.value;
        var name = form.regname.value;
        var age = form.regage.value;
        var gender = form.reggender.value;
        var email = form.regemail.value;
        var mobile = form.regPhone1.value;
        var regpassword = form.regpassword.value;
        var regrepassword = form.regrepassword.value; 
		
		
        
        if ( card =='' )
	{
            
   		document.getElementById("cardErrDiv").innerHTML = "Card No. cannot be blank";
                Validated = false;
	}
	if ( name =='' )
	{
            
   		document.getElementById("nameErrDiv").innerHTML = "Name cannot be blank";
                Validated = false;
	}
        if ( age =='' )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age cannot be blank";
                Validated = false;
	}else if ( age < 18 )
	{
            
   		document.getElementById("ageErrDiv").innerHTML = "Age should be greater than 18 years";
                Validated = false;
	}
        
        if ( gender =='' )
	{
            
   		document.getElementById("genderErrDiv").innerHTML = "Gender cannot be blank";
                Validated = false;
	}
        if ( email =='' )
	{
            
   		document.getElementById("emailErrDiv").innerHTML = "Email-ID cannot be blank";
                Validated = false;
	}
        
	if( regpassword == "" )
	{
            
   		document.getElementById("pwdErrDiv").innerHTML = "Password cannot be blank";
                Validated = false;
	}
        
        if( regpassword != regrepassword )
	{
            
                form.regrepassword.value = "";
   		document.getElementById("repwdErrDiv").innerHTML = "Password Miss-match";
                Validated = false;
	}

        if( mobile == "" )
	{
            
   		document.getElementById("mobileErrDiv").innerHTML = "Mobile cannot be blank";
                Validated = false;
	}
        
        
	if(Validated)
	{
                
                document.reg_form.submit();
                
	}
		
	else
	{                
		return false;
	}
	
}


function check_card(){
    
    var card = document.getElementById("regcard").value;
    card = encodeURI(card);
    
    if(card!="" && card!=null){
        var paramString = 'card='+card+'&type=0';
        
        $.ajax({  
                type: "POST",  
                url: "check_pink_card.php",  
                data: paramString,  
                success: function(response) { 
                    if(response==true){
                        document.getElementById("cardErrDiv").innerHTML = "<font color='green'>Valid Card</font>";
                    }else{
                        document.getElementById("regcard").value = "";
                        document.getElementById("cardErrDiv").innerHTML = response;
                    }
                }
        });
    }
}

function check_email(){
    
    var email = document.getElementById("regemail").value;
    email = encodeURI(email);
    
    if(email!="" && email!=null){
        var paramString = 'email='+email;
        
        $.ajax({  
                type: "POST",  
                url: "check_email.php",  
                data: paramString,  
                success: function(response) { 
                    if(response==true){
                        document.getElementById("emailErrDiv").innerHTML = "";
                    }else{
                        document.getElementById("regemail").value = "";
                        document.getElementById("emailErrDiv").innerHTML = "Email-Id Already Exists";
                    }
                }
        });
    }
}

function check_mobile(){
    
    var mobile = document.getElementById("regPhone1").value;
    mobile = encodeURI(mobile);
    
    if(mobile!="" && mobile!=null){
        var paramString = 'mobile='+mobile;
        
        $.ajax({  
                type: "POST",  
                url: "check_mobile.php",  
                data: paramString,  
                success: function(response) { 
                    if(response==true){
                        document.getElementById("mobileErrDiv").innerHTML = "";
                    }else{
                        document.getElementById("regPhone1").value = "";
                        document.getElementById("mobileErrDiv").innerHTML = "Mobile Number Already Exists";
                    }
                }
        });
    }
}



function isNumberKey(evt)
{
	
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false; 
	}
	else
	{
		return true; 
	}
}



