<?php 
	error_reporting(E_PARSE); 
	session_start();
	include ("../includes/pw_db_connect.php");
    if(!isset($_SESSION['username']) || $_SESSION['login'] !='clinic')
	{
		header("Location: ../index.php");
		exit();
	}
	else
	{
		$clinic_id=$_SESSION['clinic_id'];
                $clinic_name=$_SESSION['clinic_name'];

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>pinkwhalehealthcare</title>
<meta name="description" content="pinkwhalehealthcare">
<link href="css/designstyles.css" rel="stylesheet" type="text/css">
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery.js"></script>
<script language="javascript" src="js/ajax.js"></script>
<script language="javascript" src="calendar/calendar.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!-- ------------------------------   google analytics    ------------------------------------------- -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23649814-1']);
  _gaq.push(['_setDomainName', '.pinkwhalehealthcare.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
                    

</script>
 <script type="text/javascript">
function chechclinicEmailid()
{

        if(document.getElementById("email").value!=""){
            var paramString = 'email=' + document.getElementById("email").value;  


		$.ajax({  
			type: "POST",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) {  
                            
				if(response == 'success')
				{
					document.getElementById('emailErrDiv').innerHTML ="";
				}
				else
				{
					document.getElementById('emailErrDiv').innerHTML ="Email address already exists";
                                        
				}	
			}
			  
		});  
                
        }

}
</script>
<!--  --------------------------------------     END         -------------------------------------------------- --></head>
</head>
<body>
<?php include 'header.php';
require_once('calendar/tc_calendar.php');
include ('../includes/pw_db_connect.php');
?>
<table width="1000" border="0" cellspacing="10" cellpadding="0" align="center" class="s90greybigbox">
<tr>
<td width="180" valign="top" >

<!-- ///// left menu //////  -->
<?php include 'clinic_left_menu.php'; 

$today_date = date("Y-m-d",time());
?>
<!-- ///// left menu //////  -->

</td>
<td width="748" valign="top" class="s90docphr"  > 
<table height="30" width="748" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="220"><h1>Admin Profile </h1></td>
	<td width="528" bgcolor="#f1f1f1" align="right">    	
    <div style="color:#EA0977; font-family:Arial; font-size:16px; font-weight:bold; font-style:italic;">
        	<?php echo $clinic_name;?>, Pinkwhale ID <?php echo $clinic_id ; ?>
		
    </div>
    </td>
</tr>
   </table>
<script type="text/javascript">
var clinic_name = false;
var address = false;
var country = false;
var state = false;
var city = false;
var pin_zip = false;
var num_of_doc = false;
var logo = false;
var email = false;
var phone = false;

function add_clinic_submit_1(form){
    clinic_name = true;
    address = true;
    country = true;
    state = true;
    city = true;
    pin_zip = true;
    num_of_doc = true;
    logo = true;
    email = true;
    phone = true;
    
    document.getElementById("clinicnameErrDiv").innerHTML= "";
    document.getElementById("addressErrDiv").innerHTML = "";
    document.getElementById("countryRrrDiv").innerHTML = "";
    document.getElementById("stateErrDiv").innerHTML= "";
    document.getElementById("cityErrDiv").innerHTML= "";
    document.getElementById("pinErrDiv").innerHTML= "";
    document.getElementById("phoneErrDiv").innerHTML= "";
    document.getElementById("emailErrDiv").innerHTML= "";
    
    if (form.clinic_name.value=='')
    {
        document.getElementById("clinicnameErrDiv").innerHTML = "Clinic Name cannot be blank";
        clinic_name = false;
    }
	if (form.clinic_name.value!= "") {
		 if ( /[^A-Za-z +$]/.test(form.clinic_name.value)) {
			document.getElementById("clinicnameErrDiv").innerHTML = "Please enter characters only";  	
			clinic_name = false;
    }
	}
    if (form.address.value=='')
    {
        document.getElementById("addressErrDiv").innerHTML = "Address Name cannot be blank";
        address = false;
    }
    if (form.country.value=='')
    {
        document.getElementById("countryRrrDiv").innerHTML = "Country Name cannot be blank";
        country = false;
    }
    if (form.state.value=='')
    {
        document.getElementById("stateErrDiv").innerHTML = "State Name cannot be blank";
        state = false;
    }
    if (form.city.value=='')
    {
        document.getElementById("cityErrDiv").innerHTML = "City Name cannot be blank";
        city = false;
    }
    if (form.pinzip.value=='')
    {
        document.getElementById("pinErrDiv").innerHTML = "Pin/Zip Name cannot be blank";
        pin_zip = false;
    }
    if (form.phone.value=='')
    {
        document.getElementById("phoneErrDiv").innerHTML = "Phone number cannot be blank";
        phone = false;
    }
    if (form.email.value=='')
    {
        document.getElementById("emailErrDiv").innerHTML = "Email-Id cannot be blank";
        email = false;
    }
	else if (form.email.value !='' )
	{
		var atpos = form.email.value.indexOf("@");
		var dotpos = form.email.value.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=form.email.value.length) {
		document.getElementById("emailErrDiv").innerHTML = "Not a valid e-mail address";       
		email = false;
	}
	}
	else if(form.email.value!=''){
        var paramString = 'email=' + form.email.value;
        
                $.ajax({  
			type: "POST",  
			url: "check_clinic_email.php",  
			data: paramString,  
			success: function(response) { 
                           
				if(response == 'success')
				{
					document.getElementById('emailErrDiv').innerHTML ="";
				}
				else
				{
					document.getElementById('emailErrDiv').innerHTML ="Email address already exists";
				}	
			}
			  
		});          
    }       
    if(clinic_name && address && country && state && city && pin_zip && phone && email){        
        document.getElementById('clinic_prof_form').submit();        
    }else{        
        return false;        
    }
}
function changeclinicProfEditDiv()
{
		document.getElementById("edit_clinic_prof").style.display="block";
		document.getElementById("clinic_prof").style.display="none";
		document.getElementById("edit").style.display="none";
		document.getElementById("change_pass").style.display="none";
}
function uploadPhoto()
{
		document.getElementById("edit_clinic_logo_upload").style.display="block";
		document.getElementById("clinic_photo_upload").style.display="none";
}
function changePassDiv()
{
		document.getElementById("change_clinic_pass_prof").style.display="block";
		document.getElementById("clinic_prof").style.display="none";
		document.getElementById("edit").style.display="none";
		document.getElementById("edit_clinic_prof").style.display="none";
		document.getElementById("change_pass").style.display="none";
}
</script>
<table height="30"></table>

<table width="650" border="0" valign="top" class="clinicBg">
<tr><td width="467"><a href="#" style="float:right;">
  <input type="button" name="edit" id="edit" value="Edit Profile" onclick="changeclinicProfEditDiv()"/></a></td>
	<!--<td width="173"><a href="#" style="float:left;">
	  <input type="button" name="change_pass" id="change_pass" value="Change Password" onclick="changePassDiv()"/></a></td>-->
      </tr>


<tr><td colspan="2">
<?php
$qry= "SELECT * FROM `clinic_details` where `clinic_id`='$clinic_id' ";
	$qry_rslt = mysql_query($qry);
          
	while($result = mysql_fetch_array($qry_rslt))
	{
		
?>
<div id="clinic_prof" style="display:block;">
<h2><?php echo $_SESSION['ferror']; ?></h2>
<table border="0" cellspacing="10" align="left" style="border:1px solid #CCC;"  width="644">
    <tr>
<td width="206"><img src="../<?php echo $result['logo']; ?>" width="100" height="70"></td>
</tr>
<tr><td colspan="2" align="left"><span style="color: #0a74d8; font-family:Arial,Verdana;"><b>Personal Details  </b></span></td></tr>
<tr><td>
<div style="font-family:Arial,Verdana;">
    <table width="368" border="0" cellspacing="5" align="left">

<!--------All edit action done with `clinic_id`='29'------------->


<tr><td width="152" align="right"><div class="label">Full Name :</div></td>
<td width="206"><div class="text"><?php echo $result['name']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label">Address :</div></td>
	<td><div class="text"><?php echo $result['address']; ?></div></td>
</tr>
<tr>
	<td align="right" valign="top"><div class="label"></div></td>
    <td><div class="text"><?php echo $result['city']; ?> - <?php echo $result['pin_zip']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label"></div></td>
    <td><div class="text"><?php echo $result['state']; ?> ,<?php echo $result['country']; ?></div></td>
</tr>
<tr>
    <td align="right"><div class="label">Latitude :</div></td>
    <td><div class="text"><?php echo $result['latitude']; ?> </div></td>
</tr>
<tr>
    <td align="right"><div class="label">Longitude :</div></td>
    <td><div class="text"><?php echo $result['longitude']; ?> </div></td>
</tr>
<tr>
    <td><span style="color: #0a74d8; font-family:Arial,Verdana;"><h4>Contact Details:</h4></span></td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td align="right"><div class="label">Phone Number :</div></td>
    <td><div class="text"><?php echo $result['phone_no']; ?></div></td>

<tr>
    <td align="right"><div class="label">Email ID :</div></td>
    <td><div class="text"><?php echo $result['email']; ?></div></td>
</tr>

<!-- <div id="edit_cliniedit_clinic_photo_uploadc_photo_upload" style="display:none;">-->
<div id="edit_clinic_logo_upload" style= "display:none;">
  
  <form name="clinic_logo_form" id="clinic_logo_form" action="actions/clinic_logo_upload.php" method="post" enctype="multipart/form-data">
  <!--<tr><td><span style="color: #0a74d8"><b>Select Logo :</b></span></td></tr>-->
  <input type="hidden" id="clinic_id" name="clinic_id" value="<?php echo $result['clinic_id']; ?>"/>
  <input type="hidden" id="clinic_photo" name="clinic_photo" value="<?php echo $result['logo']; ?>"/>
  <!--<tr><td align="center"><input type="file" name="photo"  /></td></tr>
  <tr><td align="center"><input type="submit" name="cancel" value="Save"/></td></tr>-->
   <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    </form>
  </table>
</div> 

  <?php 
    }
	?>   
                </tr></table>
            </div>
        <div id="edit_clinic_prof" style="display: none;font-family:Arial,Verdana;">
<table width="644" border="0" align="left" cellspacing="10" style="border:1px solid #CCC;" >
<tr><td>
        <form name="clinic_prof_form" id="clinic_prof_form" action="actions/edit_clinic_prof.php" method="post" enctype="multipart/form-data" onsubmit="return chk()" >
<table width="620" border="0" class="s90dbdtbls_drdetailstbl" align="center">
    <!--------All edit action done with `clinic_id`='29'------------->
<?php
$qry= "SELECT * FROM `clinic_details` where `clinic_id`='$clinic_id' ";
	$qry_rslt = mysql_query($qry);
	while($result = mysql_fetch_array($qry_rslt))
	{
		
?>	

<tr>
<td width="116">&nbsp;</td>
<td width="144"  align="right"><div class="label">Name :</div></td>
<input type="hidden" id="clinic_id" name="clinic_id" value="<?php echo $result['clinic_id']; ?>"/>
<td width="218"><input type="text" name="clinic_name" id="clinic_name" value="<?php echo $result['name']; ?>"/></td>
<td width="124">&nbsp;</td>
</tr>
<!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="clinicnameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Clinic ID :</div></td><td width="218"><input type="text" name="clinic_id" id="clinic_id" disabled="disabled" value="<?php echo $result['clinic_id']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Address :</div></td><td><input type="text" name="address" id="address"  value="<?php echo $result['address']; ?>"/></td>
<td>&nbsp;</td>
</tr>
<!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3"  align="left" height="8">
            <div id="addressErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Country :</div></td><td><input type="text" name="country" id="country"  value="<?php echo $result['country']; ?>"/></td>
<td>&nbsp;</td>
</tr>

 <!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="countryRrrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV -->

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">State :</div></td><td><input type="text" name="state" id="state"  value="<?php echo $result['state']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="stateErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">City :</div></td><td><input type="text" name="city" id="city"  value="<?php echo $result['city']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="cityErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 

  <tr>
<td>&nbsp;</td>
<td align="right"><div class="label">PIN/ZIP :</div></td><td><input type="text" name="pinzip" id="pinzip"  value="<?php echo $result['pin_zip']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<!--    ERROR DIV -->
<tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="pinErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Latitude :</div></td><td><input type="text" name="late" id="late"  value="<?php echo $result['latitude']; ?>"/></td>
<td>&nbsp;</td>
</tr>



<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Longitude :</div></td><td><input type="text" name="longa" id="longa"  value="<?php echo $result['longitude']; ?>"/></td>
<td>&nbsp;</td>
</tr>
 <tr>
 <td>&nbsp;</td>
 <td align="right" valign="top"><div class="label">Email ID :</div></td><td><input type="text" name="email" id="email" onchange="chechclinicEmailid()" value="<?php echo $result['email']; ?>"/>
 
<!--    ERROR DIV -->
 <tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
     
</tr>
<!--  END ERROR DIV --> 

<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Phone :</div></td><td><input type="text" name="phone" id="phone" value="<?php echo $result['phone_no']; ?>"/></td>
<td>&nbsp;</td>
</tr>

<!--    ERROR DIV -->
 <tr>
     <td> </td>
     <td colspan="3" align="left" height="8">
            <div id="phoneErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div>
     </td>
</tr>
<!--  END ERROR DIV --> 



<input type="hidden" id="clinic_id" name="clinic_id" value="<?php echo $result['clinic_id']; ?>"/>
<tr>
<td>&nbsp;</td>
<td align="right"><div class="label">Select Photo :</div></td>
 <td align="left"><input type="file" name="photo"/></td>
 <td>&nbsp;</td>
 </tr>
        <tr>
        <td>&nbsp;</td>
            <td align="center" style="margin-right:10px;">&nbsp;</td>
        <td align="left"><span style="margin-right:10px;">
          <input type="button" name="medication_save" id="medication_save" value="Save" onclick="add_clinic_submit_1(clinic_prof_form)"/>
          <a href="clinic_details.php">
          <input type="button" name="medication_cancel" id="medication_cancel" value="Cancel" />
          </a></span></td> 
          <td>&nbsp;</td>      
        </tr>
        
<!--<tr><td>&nbsp;</td>
<td colspan="3" align="center"> Do you want to <span style="font-style:italic;">change your password ? </span><b><a onmouseover="this.style.cursor='pointer'" onclick="changePassDiv()" >Click Here</a></b></td>        -->
</table>
</form>
<?php 
}
?>
</td></tr>
</table>
</div>
        <!--	#######################  Doctor Emailid Checking  ###################################-->

<script type="text/javascript">

</script>
<!--	##################### End Doctor Emailid Checking #######################-->




<div id="change_clinic_pass_prof" style="display: none;font-family:Arial,Verdana;">
<table width="644" border="0" align="center" cellspacing="10" style="border:1px solid #CCC;" class="s90dbdtbls_drdetailstbl">
<tr><td>
<form name="clinic_change_pass_form" id="clinic_change_pass_form" action="actions/edit_doc_change_pass.php" method="post">
<table width="580" border="0" align="center">

<tr><td width="185" align="right"><div class="label">User Name :</div></td>
<input type="hidden" id="clinic_id" name="clinic_id" value="<?php echo $email;?>" />
<td width="155"><input type="text" name="clinic_email_id" id="clinic_email_id" value="<?php echo $email;?>" disabled="disabled"/></td>
<td width="226">&nbsp;</td>
</tr>
<tr><td align="right"><div class="label">New Password :</div></td><td><input type="password" name="new_password" id="new_password" /></td>

<!--    ERROR DIV -->
		
             <td  align="left" height="8">
	            <div id="pswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
 
<!--  END ERROR DIV --> 

</tr>
<tr>
<td align="right"><div class="label">Confirm Password :</div></td>
<td><input type="password" name="confirm_password" id="confirm_password" /></td>
<!--    ERROR DIV -->
	
             <td  align="left" height="8">
	            <div id="repswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div>
            </td>
<!--  END ERROR DIV --> 

</tr>
<tr>
<td></td>
<td colspan="2" ><div id="machpswErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px;"></div></td>

</tr>

<tr>
    <td>&nbsp;</td>
    <td align="left"><input type="button" name="clinic_change_pass_save" id="clinic_change_pass_save" value="Save"  onclick="validate_pswd(clinic_change_pass_form)" />
    <a href="clinic_details.php"><input type="button" name="clinic_change_pass_cancel" id="clinic_change_pass_cancel" value="Cancel" /></a></td>
</tr>
</table>
</form>
<script type="text/javascript">
function validate_pswd(form)
{
	pswValidated = true;
	repswValidated = true;
	document.getElementById("pswErrDiv").innerHTML	= "";
	document.getElementById("repswErrDiv").innerHTML = "";
	if (form.new_password.value=='')
	{
   		document.getElementById("pswErrDiv").innerHTML = "Please enter your new password";
    	pswValidated = false;
	}
	if (form.confirm_password.value=='')
	{
		document.getElementById("repswErrDiv").innerHTML = "Please enter your new password again";
		repswValidated = false;
	}
	else if(form.confirm_password.value!='')
	{
		if(form.new_password.value!=form.confirm_password.value)
		{
			document.getElementById("machpswErrDiv").innerHTML = "Password mismatch Try again!";
			pswValidated = false;
			repswValidated = false;
		}
	}
	
	if(pswValidated && repswValidated )
	{
		
		document.getElementById('clinic_change_pass_form').submit();
	}
	else
	{
		return false;
	}
	
}

</script>

            </tr></table>
        </td>
    </tr>
</table>

</td></tr>
</table>
<?php
include 'footer.php'; ?>
    
    </body></html>

