<?php include "includes/start.php" ?>
	<?php include "includes/header.php" ?>
	<?php include "includes/site_config.php" ?>

	
	<div class="menu-data" data-main-page="none" data-sub-page="privacy-policy"></div>
	<?php include "includes/page-menu-start.php" ?>
		<li class="contents"><a href="privacy-policy.php" class="menu-elements upper-case sub-privacy-policy">privacy policy</a></li>
		<li class="contents"><a href="terms.php" class="menu-elements upper-case sub-terms">terms of service</a></li>
		<li class="contents"><a href="" data-toggle="modal" data-target="#login-modal"  class="login-button upper-case">Login</a></li>
	<?php include "includes/page-menu-end.php" ?>
		
	
	<!-- <img src="img/privacypolicy/PrivacyPolicy_Banner.png" class="page-banner">	 -->

	<div class="banner-bg banner-bg-privacy">
		<div class="content-area">
			
		</div>
	</div>
	<div class="page-tagline content-area row-bg-1" id="top">Privacy Policy</div>

	
	

	<div class="page-row testimonials">
		<div class="content-area row-bg-2 row-bg-3 row-bg-4">
			<div class="row-header">
				<img src="img/privacypolicy/PrivacyPolicy_Icon.png" class="row-title-image privacy">
				<ol>
					<li>
						Your privacy is important to you and to us. We are totally committed in protecting the information you share with us. 
						We created this Website these policies (together "Privacy Policy") to give you confidence as you visit and use the 
						Website, and to demonstrate our commitment to fair information practices and the protection of privacy
					</li>

					<li>
						In the course of registering as a member and thereby availing our Services on pinkwhalehealthcare.com (the "Site"), 
						the Site may require you to provide your contact information viz. e-mail address, your name and information about your 
						health, and other personal information ("Personal Information"), that it collects, retains and uses for the purpose of 
						providing you Services.
					</li>

					<li>
						For the purposes of offering you Services and providing appropriate advice, your Personal Information may be seen 
						and used by our general physicians/health specialist/wellness specialist/expert ("Professionals"). In some situations in 
						order to provide us with technical services for the operation and maintenance of our Site, some of the technical or 
						mechanical contractors will have access to your Personal Information, on a need to know basis.<br>
						The Site strictly protects the security of your personal information and honors your choices for its intended use. We 
						carefully protect your data from loss, misuse, unauthorized access or disclosure, alteration, or destruction.
					</li>

					<li>
						The Site seeks to maintain the accuracy, correctness and completeness of the information including Personal 
						Information provided by the visitors, subscribers, and non-subscribers. Therefore, you are required to provide the Site, 
						at all times, with complete and accurate information.
					</li>

					<li>
						We shall be the sole owner of the Personal Information provided by you. We shall not sell or rent your Personal 
						Information. The Site shall simply act as a facilitator and procure the information and share the same with our 
						Professionals in order to facilitate services to you. However, there may be occasions wherein the law compels the Site 
						to disclose your Personal Information. 
					</li>

					<li>
						We collect internet traffic data, of visits to our website. This includes IP address of all visitors to the Site, and a number 
						is automatically assigned to your computer whenever you access the Internet, domain servers, types of computers, 
						types of web browsers (together called "Traffic Data"). We collect IP address information in order to administer our 
						system and gather aggregate information, to avoid any scope of misuse of the Site. The Traffic Data information may 
						be shared with advertisers, sponsors and other businesses on aggregate basis, and not on an individual, 
						personally-identifiable basis.
					</li>

					<li>
						This Site does not intend to collect any personally identifiable information of children below the age of 18, without the 
						guidance of the parent or guardian. The Site, on receiving any notification, shall ensure to delete such information.
					</li>

					<li>
						This Site contains links to other sites. Please note that pinkWhale is not responsible for the privacy policies of such 
						other sites. We encourage you to be aware when they leave our site and read the privacy statements of each and 
						every website that collects personally identifiable information.
					</li>

					<li>
						We will occasionally amend this Privacy Policy. Please ensure that you periodically update yourself with the current 
						version of the Privacy Policy.
					</li>
				</ol>
			</div>
		</div>
		
		<div class="triangle">
			<div class="content-area">
				<a href="#top"><div class="top-triangle"></div>top</a>
			</div>
		</div>
	</div>


	
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
	<script>
		$(document).ready(function(){
			$('.triangle .content-area a').click(function(e){
				e.preventDefault();
				$('html,body').animate({scrollTop:$('#top').offset().top - 40},"slow");
			});
		});
	</script>
<?php include "includes/end.php" ?>
