<?php //include "includes/start.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Book an appointment with your doctor within few clicks by selecting specialized doctor or 
	physician online for further consultation.">
	<meta name="keywords" content="book an appointment, medical information, take an appointment, online health, e-health,
	doctor, medical diagnosis, medical advice">
    <title>Book an Appointment with Doctor | pinkWhale Healthcare</title>
    <?php include 'includes/include-css.php'; ?>
  </head>
	<?php include "includes/header.php" ?>
	<?php include "includes/pw_db_connect.php" ?>
	<link href="css/pinkAppoint.css" media="all" rel="Stylesheet" type="text/css" />
	<script type="text/javascript" src="js/login.js"></script>
	
	<div class="menu-data" data-main-page="patients" data-sub-page="our-services" data-pink-name="pink-appoint"></div>
	<?php include "includes/menu-patients.php" ?>
		
	<div class="banner-bg banner-bg-pinkappoint">
		<div class="content-area hidden-xs">
			<div class="banner-form">
				<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
				<form class="form-horizontal pw-form" action="meet-our-doctors.php" role="form" style="max-width: 400px;">
					<div class="form-group">
						<label class="col-xs-5 control-label">Choose a Specialty:</label>
						<div class="col-xs-7">
							<select name="speciality" id="speciality" class="form-control">
							<option value="" selected="selected" disabled> -- Specialist / Expert -- </option>
							<?php // code added by Shoaib //
								$qry = "SELECT `doc_specialities` FROM `pw_doctors` WHERE blocked !='Y' and doc_specialities<>'' group by`doc_specialities`";
								$res = mysql_query($qry);
								while ($data = mysql_fetch_array($res)){
								echo "<option value='".$data['doc_specialities']."'>".$data['doc_specialities']."</option>"; 
								}
							?>					
						</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-5 control-label">Choose a City:</label>
						<div class="col-xs-7">
							<select class="form-control" name="city" id="city">
						<option value="Bangalore" selected="selected">  Bangalore  </option>									
						</select>
						</div>
					</div>  
					<div class="form-group">
						<div class="col-xs-offset-5 col-sm-7">
							<button type="submit" class="btn pw-btn">Find a Doctor</button>
						</div>
					</div>
				</form>
			</div>			
		</div>
	</div>


	<?php include "includes/menu-pink-buttons.php" ?>


	<div class="banner-form visible-xs mobile-form">
		<div class="tag-line">Scheduling an appointment<br>has never been easier!</div>
		<form class="form-horizontal" action="findadoctor.php" role="form">
			<div class="form-group pw-form">
				<label class="col-xs-5 control-label">Choose a Specialty:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Pediatrician</option>
						<option>Pediatrician</option>					
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-5 control-label">Choose a City:</label>
				<div class="col-xs-7">
					<select class="form-control">
						<option>Bangalore</option>
						<option>Mysore</option>
						<option>Ahmedabad</option>
						<option>Delhi</option>
						<option>Jammu</option>
					</select>
				</div>
			</div>  
			<div class="form-group">
				<div class="col-xs-offset-5 col-sm-7">
					<button type="submit" class="btn pw-btn">Find a Doctor</button>
				</div>
			</div>
		</form>
	</div>
	
	<div class="page-tagline pink-page row-bg-1">
		<img src="img/ourservices/pinkappoint/pinkAppoint_Icon_PinkAppointment1.png"><i>pink</i><strong>Appoint</strong>
		<div class="text">
			<i>
				Forgot to book an appointment during clinic hours?<br>
				Book an appointment here!<br><br>
				We're not trying to replace your face-to-face visits. <br>
				pinkAppoint allows you to book an appointment within a few clicks! <br><br>
			</i>
			<br>
			<strong>
				One in-clinic visit makes you eligible for virtual follow ups. We even make <span class="hidden-xs"><br></span>
				scheduling that visit simple. Our pinkAppoint Service syncs with the doctor's <span class="hidden-xs"><br></span>
				calendar so you can make an appointment when it's convenient for you. <br>
			</strong>
		</div>
	</div>

	<!-- <img src="img/ourservices/pinkquery/pinkQuery_How it Works.png" class="work-image"> -->
	<div class="how-it-works">
		<div class="content-area">
			<img src="img/ourservices/pinkquery/pinkQuery_Icon_How it Works.png">
			<div class="title">How does it work?</div>
		</div>
	</div>

	<div class="how_it_works">
		<img src="img/ourservices/pinkappoint/pinkAppoint.png" style="max-width: 100%;">
	</div>

	<!-- <div class="work-bg work-pink-appoint"></div> -->

	<div class="row-before-video row-bg-2">
		SEE HOW <i>pink</i>Appoint<br>CAN HELP YOU WHEN YOU NEED IT MOST

	</div>

	<div class="work-bg video-bg video-image">
		<iframe width="100%" src="//www.youtube.com/embed/sV6ff8UbjHk?list=PLu4bXNfdFdSxW0UGWQwSMkNBCOAorfB1E" frameborder="0" allowfullscreen></iframe>		
	</div>

	<div class="form-block col-md-12 row-bg-3">
		<div class="content-area form-contents">
			<div class="form-header">
				<img src="img/Subscribe1_icon_Apply.png">
				<span class="form-tagline">Register with pinkWhale Today!</span>
			</div>
				<div class="form-left col-xs-12 col-sm-8">								
				<div class="pw-form-container">					
					<form class="form-horizontal pw-form" role="form" name="register" id="register">
						<div class="form-group">
							<label for="fname" class="col-xs-5 control-label">Your Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="name" name="name" >
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="nameErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV --> 
						<!--div class="form-group">
							<label for="lname" class="col-xs-5 control-label">Last Name:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="lname" name="lname" >
							</div>
						</div-->	
						<!--    ERROR DIV -->						
						<!--div  align="right" style="margin-right:70px;">
						<div id="lname_ErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div-->
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="age" class="col-xs-5 control-label">Age:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="age" name="age" onkeypress="return isNumberKey(event);" value="">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="ageErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="gender" class="col-xs-5 control-label">Gender:*</label>
							<div class="col-sm-7">
								<!--input type="textbox" class="form-control" id="gender" name="gender"-->
								<select name="gender" id="gender">
                                            <option value="">Select Gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="genderErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="cardno" class="col-xs-5 control-label">Card Number:</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="regcard" name="regcard" value=""  autocomplete="off" onblur="return card_check(this.value)">
							</div>
						</div>		
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="cardErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="email" class="col-xs-5 control-label">Email*</label>
							<div class="col-xs-7">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="emailErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="password" class="col-xs-5 control-label">Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="password" name="password" value="" onkeyup="valid_pswd(form)" autocomplete="off">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="passwordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="cpassword" class="col-xs-5 control-label">Confirm Password:*</label>
							<div class="col-xs-7">
								<input type="password" class="form-control" id="cpassword" name="cpassword" value="" onkeyup="valid_pswd(form)" autocomplete="off">
							</div>
						</div>	
						<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="repasswordErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group">
							<label for="mobile" class="col-xs-5 control-label">Mobile:*</label>
							<div class="col-xs-7">
								<input type="textbox" class="form-control" id="mobile" name="mobile"  value="" onkeypress="return isNumberKey(event);" autocomplete="off">
							</div>
						</div>
							<!--    ERROR DIV -->						
						<div  align="right" style="margin-right:70px;">
						<div id="mobileErrDiv" class="error" style="color: #F33;font-family:verdana;font-size:10px; margin-left:8px"></div></td>
						</div>
					   <!--  END ERROR DIV -->
						<div class="form-group text-left">
							<div class="col-xs-offset-5 col-xs-7">
								<input type="submit" class="pw-btn" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return register_pink(reg_form)"/>
							</div>
						</div>												
					</form>
				</div>				
			</div>	
			<div class="message form-right col-sm-4 hidden-xs">

				No need to wait on the phone to <br>
				schedule an appointment with <br>
				your doctor.<br><br>
				Schedule an in-clinic visit here to <br>
				become eligible for our other <br>
				virtual health services.<br>
			</div>
		</div>
	</div>
	

	<?php include "includes/footer.php" ?>
	<?php include "includes/include-js.php" ?>
<?php include "includes/end.php" ?>
