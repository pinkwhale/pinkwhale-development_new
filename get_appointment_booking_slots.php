<?php
ob_start();
error_reporting(E_PARSE);
session_start();
date_default_timezone_set('Asia/Calcutta');
if(!isset($_SESSION['username']) ||  $_SESSION['login']!='doctor')
{
        header("Location: index.php");
        exit();
}

include ("db_connect.php");

$doc=$_SESSION['doctor_id'];

$doctor_details = explode("|", $doc);
$doc_id = $doctor_details[0];
$doctor_name = $doctor_details[1];

$date = urldecode($_GET['date']);

$d_day = strftime ( '%a', strtotime($date) );

$week = get_week($d_day);

$clinic = urldecode($_GET['clinic_id']);

$clinic_details = explode("|", $clinic);
$clinic_id = $clinic_details[0];
$clinic_name = $clinic_details[1];

$time_slot = "<table border='0' cellpadding='2' cellspacing='1' width='500' align='center'><tr>";

//$time_slot .= "<td><input type='checkbox' name='time_slot' id='time_slot' onclick='checkall()' value='all'>All</input></td></tr><tr>";

$i=0;
$count_1 = 0;

$qry = "select substr(from_time,12) from_time,DATE_FORMAT(from_time,'%h:%i %p') from doctor_time_slots where substr(from_time,12) not in (select from_time from doctor_appointment_cancel where clinic_id='$clinic_id' and doc_id='$doc_id' and cancel_date='$date')  and substr(from_time,12) not in (select  substr(from_time,12) from_time from Appointment_book_details where  clinic_id='$clinic_id' and doc_id='$doc_id' and date(from_time)='$date' and status=2) and clinic_id='$clinic_id' and doc_id='$doc_id' and Day='$week' order by from_time";
$res = mysql_query($qry);
$num = mysql_num_rows($res);
if($num>0){
    while($data = mysql_fetch_array($res)){
        //$time_slot .= "<tr><td><a href='#' onclick='proceed_details(\"$data[0]\",\"$data[1]\")' >$data[1]</a></td></tr>";
        if((strtotime(date('Y-m-d H:i:s')) < strtotime($date."".$data[0])) ){
            $count_1 +=1;
            if($i<=3){
                $time_slot .= "<td><a href='#' onclick='proceed_detail(\"$data[0]\",\"$data[1]\")' >$data[1]</a></td>";
                $i++;
            }else{
                $i=1;
                $time_slot .= "</tr><tr>";
                $time_slot .= "<td><a href='#' onclick='proceed_detail(\"$data[0]\",\"$data[1]\")' >$data[1]</a></td>";
            }
        }

    }
    
    if($count_1==0){
        $time_slot .= "<td align='center'><font color='red'>No Appointments</font></td>";
    }

    
}else{
    $time_slot .= "<td align='center'><font color='red'>No Appointments</font></td>";
}

$time_slot .= "</tr></table>";
echo $time_slot;



ob_end_flush();



function get_week($d){
    $day=0;
    
    switch($d){
        case "Mon" : $day = 1;break;
        case "Tue" : $day = 2;break;
        case "Wed" : $day = 3;break;
        case "Thu" : $day = 4;break;
        case "Fri" : $day = 5;break;
        case "Sat" : $day = 6;break;
        case "Sun" : $day = 7;break;
    }
    
    return $day;
    
}
?>

